import 'reflect-metadata';

import { PrismaClient } from '@prisma/client';

import { App } from './app';
import { container } from './dependency-injection';
import { errorToString } from './utils/conversion';
import { LoggerFactory } from './utils/logger';

async function bootstrap(): Promise<void> {
    const prisma = container.resolve(PrismaClient);
    const logger = container.resolve(LoggerFactory).get('Index');
    const app = container.resolve(App);

    try {
        await app.start();
    } catch (error) {
        logger.error(errorToString(error));
        throw error;
    } finally {
        await prisma.$disconnect();
    }
}

void bootstrap();
