import { singleton } from 'tsyringe';

import { RpcMonitorBlockHeader } from '../rpc/rpc-monitor-block-header';
import { TezosRpcClient } from '../rpc/tezos-rpc-client';
import { asIterable } from '../utils/async-iterator-utils';
import { BackgroundWorker } from '../utils/background-worker';
import { Logger, LoggerFactory } from '../utils/logger';
import { BlockConverter } from './converters/block-converter';
import { Triggers, TypedPubSub } from './typed-pub-sub';

/** Publishes actual RPC block and GraphQL operations based on a notification from Tezos node monitor. */
@singleton()
export class NotificationsPublisher implements BackgroundWorker {
    private readonly logger: Logger;
    private monitorIterator: AsyncIterator<RpcMonitorBlockHeader> | undefined;

    constructor(
        private readonly rpcClient: TezosRpcClient,
        private readonly pubSub: TypedPubSub,
        private readonly blockConverter: BlockConverter,
        loggerFactory: LoggerFactory,
    ) {
        this.logger = loggerFactory.get(NotificationsPublisher);
    }

    start(): void {
        if (this.monitorIterator) {
            throw new Error(`This can't be started because it's already running.`);
        }

        this.logger.info('Starting.');
        void this.processMonitorBlocks();
    }

    stop(): void {
        if (!this.monitorIterator) {
            throw new Error(`This can't be stopped because it's NOT running.`);
        }
        if (!this.monitorIterator.return) {
            throw new Error(`Iterator is missing return therefore can't be stopped.`);
        }

        this.logger.info('Stopping.');
        void this.monitorIterator.return('stop signal');
        this.monitorIterator = undefined;
    }

    private async processMonitorBlocks(): Promise<void> {
        this.monitorIterator = this.pubSub.iterate(Triggers.monitorBlockHeaders);
        for await (const monitorBlock of asIterable(this.monitorIterator)) {
            try {
                const rpcBlock = await this.rpcClient.getBlock(monitorBlock.hash);

                // Convert only once regardless number of subscriptions.
                const block = this.blockConverter.convert(rpcBlock);
                this.logger.info(`Publishing block ${block.hash} with ${block.operations.length} operations.`);

                this.pubSub.publish(Triggers.blocks, block);
                block.operations.forEach(o => this.pubSub.publish(Triggers.operations[o.kind], o));
            } catch (error) {
                const message = `Failed to download and process block ${monitorBlock.hash} from Tezos RPC. Related subscriptions may not be pushed.`;
                this.logger.error(message, error);
            }
        }
    }
}
