import { BlockResponse } from '@taquito/rpc';
import { UserInputError } from 'apollo-server';
import { singleton } from 'tsyringe';

import { OperationNotification } from '../entity/subscriptions/operation-notification';
import { TezosRpcClient } from '../rpc/tezos-rpc-client';
import { asIterable } from '../utils/async-iterator-utils';
import { EnvConfig } from '../utils/env-config';
import { Logger, LoggerFactory } from '../utils/logger';
import { DefaultConstructor } from '../utils/reflection';
import { BlockConverter } from './converters/block-converter';
import { HeadBlockProvider } from './head-block-provider';
import { Triggers, TypedPubSub } from './typed-pub-sub';

@singleton()
export class OperationsProvider {
    private readonly logger: Logger;

    constructor(
        private readonly pubSub: TypedPubSub,
        private readonly rpcClient: TezosRpcClient,
        private readonly blockConverter: BlockConverter,
        private readonly headBlockProvider: HeadBlockProvider,
        private readonly config: EnvConfig,
        loggerFactory: LoggerFactory,
    ) {
        this.logger = loggerFactory.get(OperationsProvider);
    }

    iterate<TOperation extends OperationNotification>(
        operationType: DefaultConstructor<TOperation>,
        startBlockLevel: number | undefined,
    ): AsyncIterator<TOperation> {
        return startBlockLevel !== undefined
            ? this.iterateWithHistory(operationType, startBlockLevel)
            : this.iterateFresh(operationType);
    }

    private iterateFresh<TOperation extends OperationNotification>(operationType: DefaultConstructor<TOperation>): AsyncIterator<TOperation> {
        const kind = new operationType().kind;
        return <AsyncIterator<TOperation>> this.pubSub.iterate(Triggers.operations[kind]);
    }

    private async *iterateWithHistory<TOperation extends OperationNotification>(
        operationType: DefaultConstructor<TOperation>,
        startBlockLevel: number,
    ): AsyncIterator<TOperation> {
        let headBlockLevel = await this.getHeadBlockLevel();
        if ((headBlockLevel - startBlockLevel) > this.config.startBlockLevelMaxFromHead) {
            throw new UserInputError(`StartBlockLevel can go only ${this.config.startBlockLevelMaxFromHead} levels to the past`
                + ` but specified #${startBlockLevel} is further from head #${headBlockLevel}.`);
        }

        let blockLevel: number | undefined = startBlockLevel;
        this.logger.info(`Serving historical operations from block #${blockLevel} for particular client.`);

        /* eslint-disable no-await-in-loop */
        while (blockLevel !== undefined) {
            try {
                this.logger.debug(`Serving historical block #${blockLevel}. Head is #${headBlockLevel}.`);
                const rpcBlock: BlockResponse = await this.rpcClient.getBlock(blockLevel);

                const block = this.blockConverter.convert(rpcBlock);
                const operations = <TOperation[]>block.operations.filter(o => o instanceof operationType);
                this.logger.debug(`Serving ${operations.length} operations.`);
                yield* operations;
            } catch (error) {
                this.logger.error(`Failed retrieving historical block. Switching to head. ${<string>error}`);
            }
            headBlockLevel = await this.getHeadBlockLevel(); // New block could have been baked in the meantime.
            const blockLevelIncrement = 1;
            blockLevel = blockLevel < headBlockLevel ? blockLevel + blockLevelIncrement : undefined;
        }
        /* eslint-enable no-await-in-loop */

        this.logger.debug(`Switching to new operations from head #${headBlockLevel}.`);
        yield* asIterable(this.iterateFresh(operationType));
    }

    private async getHeadBlockLevel(): Promise<number> {
        const header = await this.headBlockProvider.getHeader();
        return header.level;
    }
}
