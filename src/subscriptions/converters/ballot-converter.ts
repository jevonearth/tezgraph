import * as rpc from '@taquito/rpc';
import { singleton } from 'tsyringe';

import { BallotNotification, BallotVote } from '../../entity/subscriptions/ballot-notification';
import { OperationKind } from '../../entity/subscriptions/operation-notification';
import { create } from '../../utils/creation';
import { BaseOperationProperties } from './block-converter';

type RpcBallot = rpc.OperationContentsBallot | rpc.OperationContentsAndResultBallot;

@singleton()
export class BallotConverter {
    convert(rpcOperation: RpcBallot, baseProperties: BaseOperationProperties): BallotNotification {
        return create(BallotNotification, {
            ...baseProperties,
            kind: OperationKind[rpcOperation.kind],
            source: rpcOperation.source,
            period: rpcOperation.period,
            proposal: rpcOperation.proposal,
            ballot: BallotVote[rpcOperation.ballot],
        });
    }
}
