import * as rpc from '@taquito/rpc';
import { singleton } from 'tsyringe';

import {
    DoubleEndorsementEvidenceNotification,
    InlinedEndorsement,
    InlinedEndorsementKind,
} from '../../entity/subscriptions/double-endorsement-evidence-notification';
import { OperationKind } from '../../entity/subscriptions/operation-notification';
import { create } from '../../utils/creation';
import { BaseOperationProperties } from './block-converter';
import { OperationMetadataConverter } from './common/operation-metadata-converter';

@singleton()
export class DoubleEndorsementEvidenceConverter {
    constructor(private readonly metadataConverter: OperationMetadataConverter) {}

    convert(
        rpcOperation: rpc.OperationContentsAndResultDoubleEndorsement,
        baseProperties: BaseOperationProperties,
    ): DoubleEndorsementEvidenceNotification {
        return create(DoubleEndorsementEvidenceNotification, {
            ...baseProperties,
            kind: OperationKind[rpcOperation.kind],
            op1: this.convertEndorsement(rpcOperation.op1),
            op2: this.convertEndorsement(rpcOperation.op2),
            metadata: this.metadataConverter.convertSimple(rpcOperation),
        });
    }

    private convertEndorsement(rpcEndorsement: rpc.InlinedEndorsement): InlinedEndorsement {
        return {
            branch: rpcEndorsement.branch,
            operations: {
                kind: InlinedEndorsementKind[rpcEndorsement.operations.kind],
                level: rpcEndorsement.operations.level,
            },
            signature: rpcEndorsement.signature,
        };
    }
}
