import * as rpc from '@taquito/rpc';
import { singleton } from 'tsyringe';

import { OperationKind } from '../../entity/subscriptions/operation-notification';
import { OriginationNotification } from '../../entity/subscriptions/origination-notification';
import { create } from '../../utils/creation';
import { BaseOperationProperties } from './block-converter';
import { convertBigNumber } from './common/big-number-converter';
import { OperationMetadataConverter } from './common/operation-metadata-converter';
import { OriginationResultConverter } from './operation-results/origination-result-converter';

@singleton()
export class OriginationConverter {
    constructor(
        private readonly metadataConverter: OperationMetadataConverter,
        private readonly resultConverter: OriginationResultConverter,
    ) {}

    convert(rpcOperation: rpc.OperationContentsAndResultOrigination, baseProperties: BaseOperationProperties): OriginationNotification {
        return create(OriginationNotification, {
            ...baseProperties,
            kind: OperationKind[rpcOperation.kind],
            source: rpcOperation.source,
            fee: convertBigNumber(rpcOperation.fee),
            counter: convertBigNumber(rpcOperation.counter),
            gas_limit: convertBigNumber(rpcOperation.gas_limit),
            storage_limit: convertBigNumber(rpcOperation.storage_limit),
            balance: convertBigNumber(rpcOperation.balance),
            delegate: rpcOperation.delegate,
            script: rpcOperation.script,
            metadata: this.metadataConverter.convert(rpcOperation, this.resultConverter),
        });
    }
}
