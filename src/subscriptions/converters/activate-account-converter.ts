import * as rpc from '@taquito/rpc';
import { singleton } from 'tsyringe';

import { ActivateAccountNotification } from '../../entity/subscriptions/activate-account-notification';
import { OperationKind } from '../../entity/subscriptions/operation-notification';
import { create } from '../../utils/creation';
import { BaseOperationProperties } from './block-converter';
import { OperationMetadataConverter } from './common/operation-metadata-converter';

@singleton()
export class ActivateAccountConverter {
    constructor(private readonly metadataConverter: OperationMetadataConverter) {}

    convert(rpcOperation: rpc.OperationContentsAndResultActivateAccount, baseProperties: BaseOperationProperties): ActivateAccountNotification {
        return create(ActivateAccountNotification, {
            ...baseProperties,
            kind: OperationKind[rpcOperation.kind],
            pkh: rpcOperation.pkh,
            secret: rpcOperation.secret,
            metadata: this.metadataConverter.convertSimple(rpcOperation),
        });
    }
}
