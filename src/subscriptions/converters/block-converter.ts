import * as rpc from '@taquito/rpc';
import { singleton } from 'tsyringe';

import { BlockNotification } from '../../entity/subscriptions/block-notification';
import { OperationNotification, OperationNotificationInfo } from '../../entity/subscriptions/operation-notification';
import { Logger, LoggerFactory } from '../../utils/logger';
import { OperationConverter } from './operation-converter';

export interface BaseOperationProperties {
    readonly info: OperationNotificationInfo;
    readonly block: BlockNotification;
}

/** Converts a block from Tezos RPC to a GraphQL object. */
@singleton()
export class BlockConverter {
    private readonly logger: Logger;

    constructor(
        private readonly operationConverter: OperationConverter,
        loggerFactory: LoggerFactory,
    ) {
        this.logger = loggerFactory.get(BlockConverter);
    }

    convert(rpcBlock: rpc.BlockResponse): BlockNotification {
        const block = {
            hash: rpcBlock.hash,
            protocol: rpcBlock.protocol,
            chain_id: rpcBlock.chain_id,
            header: {
                level: rpcBlock.header.level,
                proto: rpcBlock.header.proto,
                predecessor: rpcBlock.header.predecessor,
                timestamp: rpcBlock.header.timestamp,
                signature: rpcBlock.header.signature,
            },
            operations: Array<OperationNotification>(),
        };

        for (const rpcGroup of rpcBlock.operations.flat()) {
            for (const rpcOperation of <rpc.OperationContentsAndResult[]>rpcGroup.contents) {
                const baseProperties = this.createBaseProperties(block, rpcGroup);
                const operation = this.operationConverter.convert(rpcOperation, baseProperties);

                this.logger.debug(`Converted operation kind ${rpcOperation.kind} in group ${rpcGroup.hash}.`);
                block.operations.push(operation);
            }
        }
        return block;
    }

    private createBaseProperties(block: BlockNotification, rpcGroup: rpc.OperationEntry): BaseOperationProperties {
        return {
            info: {
                protocol: rpcGroup.protocol,
                chain_id: rpcGroup.chain_id,
                hash: rpcGroup.hash,
                branch: rpcGroup.branch,
                signature: rpcGroup.signature,
            },
            block,
        };
    }
}
