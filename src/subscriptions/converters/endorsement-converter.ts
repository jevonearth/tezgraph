import * as rpc from '@taquito/rpc';
import { singleton } from 'tsyringe';

import { EndorsementNotification } from '../../entity/subscriptions/endorsement-notification';
import { OperationKind } from '../../entity/subscriptions/operation-notification';
import { create } from '../../utils/creation';
import { BaseOperationProperties } from './block-converter';
import { BalanceUpdateConverter } from './common/balance-update-converter';

@singleton()
export class EndorsementConverter {
    constructor(private readonly balanceUpdateConverter: BalanceUpdateConverter) {}

    convert(rpcOperation: rpc.OperationContentsAndResultEndorsement, baseProperties: BaseOperationProperties): EndorsementNotification {
        return create(EndorsementNotification, {
            ...baseProperties,
            kind: OperationKind[rpcOperation.kind],
            level: rpcOperation.level,
            metadata: {
                balance_updates: this.balanceUpdateConverter.convert(rpcOperation.metadata.balance_updates),
                delegate: rpcOperation.metadata.delegate,
                slots: rpcOperation.metadata.slots,
            },
        });
    }
}
