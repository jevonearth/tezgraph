import * as rpc from '@taquito/rpc';
import { singleton } from 'tsyringe';

import { OperationKind } from '../../entity/subscriptions/operation-notification';
import { RevealNotification } from '../../entity/subscriptions/reveal-notification';
import { create } from '../../utils/creation';
import { BaseOperationProperties } from './block-converter';
import { convertBigNumber } from './common/big-number-converter';
import { OperationMetadataConverter } from './common/operation-metadata-converter';
import { RevealResultConverter } from './operation-results/reveal-result-converter';

@singleton()
export class RevealConverter {
    constructor(
        private readonly metadataConverter: OperationMetadataConverter,
        private readonly resultConverter: RevealResultConverter,
    ) {}

    convert(rpcOperation: rpc.OperationContentsAndResultReveal, baseProperties: BaseOperationProperties): RevealNotification {
        return create(RevealNotification, {
            ...baseProperties,
            kind: OperationKind[rpcOperation.kind],
            source: rpcOperation.source,
            fee: convertBigNumber(rpcOperation.fee),
            counter: convertBigNumber(rpcOperation.counter),
            gas_limit: convertBigNumber(rpcOperation.gas_limit),
            storage_limit: convertBigNumber(rpcOperation.storage_limit),
            public_key: rpcOperation.public_key,
            metadata: this.metadataConverter.convert(rpcOperation, this.resultConverter),
        });
    }
}
