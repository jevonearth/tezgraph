import * as rpc from '@taquito/rpc';
import { singleton } from 'tsyringe';

import { AbstractOperationResult, InternalOperationResult, InternalOperationResultKind } from '../../../entity/subscriptions/operation-result';
import { convertBigNumber } from '../common/big-number-converter';
import { DelegationResultConverter } from './delegation-result-converter';
import { OriginationResultConverter } from './origination-result-converter';
import { RevealResultConverter } from './reveal-result-converter';
import { TransactionResultConverter } from './transaction-result-converter';

@singleton()
export class InternalOperationResultConverter {
    constructor(
        private readonly delegationResultConverter: DelegationResultConverter,
        private readonly originationResultConverter: OriginationResultConverter,
        private readonly revealResultConverter: RevealResultConverter,
        private readonly transactionResultConverter: TransactionResultConverter,
    ) {}

    convert(rpcResult: rpc.InternalOperationResult): InternalOperationResult {
        return {
            kind: InternalOperationResultKind[rpcResult.kind],
            source: rpcResult.source,
            nonce: rpcResult.nonce,
            amount: convertBigNumber(rpcResult.amount),
            destination: rpcResult.destination,
            parameters: rpcResult.parameters,
            public_key: rpcResult.public_key,
            balance: convertBigNumber(rpcResult.balance),
            delegate: rpcResult.delegate,
            script: rpcResult.script,
            result: this.convertResult(rpcResult),
        };
    }

    /* eslint-disable-next-line consistent-return */
    private convertResult(rpcResult: rpc.InternalOperationResult): AbstractOperationResult {
        switch (rpcResult.kind) {
            case rpc.OpKind.TRANSACTION:
                return this.transactionResultConverter.convert(rpcResult.result);
            case rpc.OpKind.REVEAL:
                return this.revealResultConverter.convert(rpcResult.result);
            case rpc.OpKind.DELEGATION:
                return this.delegationResultConverter.convert(rpcResult.result);
            case rpc.OpKind.ORIGINATION:
                return this.originationResultConverter.convert(rpcResult.result);
        }
    }
}
