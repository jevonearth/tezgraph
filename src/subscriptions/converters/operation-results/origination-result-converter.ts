import * as rpc from '@taquito/rpc';
import { singleton } from 'tsyringe';

import { OriginationResult } from '../../../entity/subscriptions/origination-notification';
import { create } from '../../../utils/creation';
import { BalanceUpdateConverter } from '../common/balance-update-converter';
import { convertBigNumber } from '../common/big-number-converter';
import { getBaseProperties } from './base-operation-result-converter';

@singleton()
export class OriginationResultConverter {
    constructor(private readonly balanceUpdateConverter: BalanceUpdateConverter) {}

    convert(rpcResult: rpc.OperationResultOrigination): OriginationResult {
        return create(OriginationResult, {
            ...getBaseProperties(rpcResult),
            balance_updates: this.balanceUpdateConverter.convert(rpcResult.balance_updates),
            originated_contracts: rpcResult.originated_contracts,
            storage_size: convertBigNumber(rpcResult.storage_size),
            paid_storage_size_diff: convertBigNumber(rpcResult.paid_storage_size_diff),
        });
    }
}
