import * as rpc from '@taquito/rpc';
import { singleton } from 'tsyringe';

import { DelegationResult } from '../../../entity/subscriptions/delegation-notification';
import { create } from '../../../utils/creation';
import { getBaseProperties } from './base-operation-result-converter';

@singleton()
export class DelegationResultConverter {
    convert(rpcResult: rpc.OperationResultDelegation): DelegationResult {
        return create(DelegationResult, getBaseProperties(rpcResult));
    }
}
