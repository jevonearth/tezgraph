import * as rpc from '@taquito/rpc';
import { singleton } from 'tsyringe';

import { TransactionResult } from '../../../entity/subscriptions/transaction-notification';
import { create } from '../../../utils/creation';
import { BalanceUpdateConverter } from '../common/balance-update-converter';
import { BigMapDiffConverter } from '../common/big-map-diff-converter';
import { convertBigNumber } from '../common/big-number-converter';
import { getBaseProperties } from './base-operation-result-converter';

@singleton()
export class TransactionResultConverter {
    constructor(
        private readonly balanceUpdateConverter: BalanceUpdateConverter,
        private readonly bigMapDiffConverter: BigMapDiffConverter,
    ) {}

    convert(rpcResult: rpc.OperationResultTransaction): TransactionResult {
        return create(TransactionResult, {
            ...getBaseProperties(rpcResult),
            storage: rpcResult.storage,
            big_map_diff: this.bigMapDiffConverter.convert(rpcResult.big_map_diff),
            balance_updates: this.balanceUpdateConverter.convert(rpcResult.balance_updates),
            originated_contracts: rpcResult.originated_contracts,
            storage_size: convertBigNumber(rpcResult.storage_size),
            paid_storage_size_diff: convertBigNumber(rpcResult.paid_storage_size_diff),
            allocated_destination_contract: rpcResult.allocated_destination_contract,
        });
    }
}
