import * as rpc from '@taquito/rpc';

import { AbstractOperationResult, OperationResultStatus } from '../../../entity/subscriptions/operation-result';
import { convertBigNumber } from '../common/big-number-converter';

interface OperationResultBase {
    status: rpc.OperationResultStatusEnum;
    consumed_gas?: string;
    errors?: rpc.TezosGenericOperationError[];
}

export function getBaseProperties(rpcResult: OperationResultBase): AbstractOperationResult {
    return {
        status: OperationResultStatus[rpcResult.status],
        consumed_gas: convertBigNumber(rpcResult.consumed_gas),
        errors: rpcResult.errors,
    };
}
