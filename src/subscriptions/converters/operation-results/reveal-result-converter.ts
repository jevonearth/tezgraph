import * as rpc from '@taquito/rpc';
import { singleton } from 'tsyringe';

import { RevealResult } from '../../../entity/subscriptions/reveal-notification';
import { create } from '../../../utils/creation';
import { getBaseProperties } from './base-operation-result-converter';

@singleton()
export class RevealResultConverter {
    convert(rpcResult: rpc.OperationResultReveal): RevealResult {
        return create(RevealResult, getBaseProperties(rpcResult));
    }
}
