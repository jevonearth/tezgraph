import * as rpc from '@taquito/rpc';
import { singleton } from 'tsyringe';

import { OperationKind } from '../../entity/subscriptions/operation-notification';
import { ProposalsNotification } from '../../entity/subscriptions/proposals-notification';
import { create } from '../../utils/creation';
import { BaseOperationProperties } from './block-converter';

type RpcProposals = rpc.OperationContentsProposals | rpc.OperationContentsAndResultProposals;

@singleton()
export class ProposalsConverter {
    convert(rpcOperation: RpcProposals, baseProperties: BaseOperationProperties): ProposalsNotification {
        return create(ProposalsNotification, {
            ...baseProperties,
            kind: OperationKind[rpcOperation.kind],
            source: rpcOperation.source,
            period: rpcOperation.period,
            proposals: rpcOperation.proposals,
        });
    }
}
