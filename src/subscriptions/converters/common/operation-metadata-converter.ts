import * as rpc from '@taquito/rpc';
import { singleton } from 'tsyringe';

import { BalanceUpdate } from '../../../entity/subscriptions/balance-update';
import { SimpleOperationMetadata } from '../../../entity/subscriptions/operation-metadata';
import { InternalOperationResult } from '../../../entity/subscriptions/operation-result';
import { InternalOperationResultConverter } from '../operation-results/internal-operation-result-converter';
import { BalanceUpdateConverter } from './balance-update-converter';

interface RpcMetadataWithResult<TRpcResult> {
    balance_updates: rpc.OperationMetadataBalanceUpdates[];
    operation_result: TRpcResult;
    internal_operation_results?: rpc.InternalOperationResult[];
}

interface MetadataWithResult<TRpcResult> {
    balance_updates: readonly BalanceUpdate[];
    operation_result: TRpcResult;
    internal_operation_results: readonly InternalOperationResult[] | undefined;
}

interface OperationResultConverter<TRpcResult, TResult> {
    convert(rpcResult: TRpcResult): TResult;
}

@singleton()
export class OperationMetadataConverter {
    constructor(
        private readonly balanceUpdateConverter: BalanceUpdateConverter,
        private readonly internalOperationResultConverter: InternalOperationResultConverter,
    ) {}

    convertSimple(rpcOperation: { metadata: rpc.OperationContentsAndResultMetadata }): SimpleOperationMetadata {
        return {
            balance_updates: this.balanceUpdateConverter.convert(rpcOperation.metadata.balance_updates),
        };
    }

    convert<TRpcResult, TResult>(
        rpcOperation: { metadata: RpcMetadataWithResult<TRpcResult> },
        resultConverter: OperationResultConverter<TRpcResult, TResult>,
    ): MetadataWithResult<TResult> {
        return {
            ...this.convertSimple(rpcOperation),
            operation_result: resultConverter.convert(rpcOperation.metadata.operation_result),
            internal_operation_results: rpcOperation.metadata.internal_operation_results?.map(r => this.internalOperationResultConverter.convert(r)),
        };
    }
}
