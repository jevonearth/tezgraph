import * as rpc from '@taquito/rpc';
import { singleton } from 'tsyringe';

import { BigMapDiff } from '../../../entity/subscriptions/big-map-diff';

@singleton()
export class BigMapDiffConverter {
    convert(rpcDiffs: rpc.ContractBigMapDiffItem[] | undefined): readonly BigMapDiff[] | undefined {
        return rpcDiffs?.map(d => ({
            key_hash: d.key_hash,
            key: d.key,
            value: d.value,
        }));
    }
}
