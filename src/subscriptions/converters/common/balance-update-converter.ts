import * as rpc from '@taquito/rpc';
import { singleton } from 'tsyringe';

import { BalanceUpdate, BalanceUpdateCategory, BalanceUpdateKind } from '../../../entity/subscriptions/balance-update';
import { convertBigNumber } from './big-number-converter';

type RpcBalanceUpdate = rpc.OperationMetadataBalanceUpdates | rpc.OperationBalanceUpdatesItem;

@singleton()
export class BalanceUpdateConverter {
    convert(rpcUpdates: RpcBalanceUpdate[]): readonly BalanceUpdate[];
    convert(rpcUpdates: RpcBalanceUpdate[] | undefined): readonly BalanceUpdate[] | undefined;
    convert(rpcUpdates: RpcBalanceUpdate[] | undefined): readonly BalanceUpdate[] | undefined {
        return rpcUpdates?.map(u => ({
            kind: BalanceUpdateKind[u.kind],
            category: u.category ? BalanceUpdateCategory[u.category] : undefined,
            contract: u.contract,
            delegate: u.delegate,
            cycle: u.cycle,
            change: convertBigNumber(u.change),
        }));
    }
}
