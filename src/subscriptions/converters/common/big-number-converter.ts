import { BigNumber } from 'bignumber.js';

export function convertBigNumber(rawValue: string): BigNumber;
export function convertBigNumber(rawValue: string | undefined): BigNumber | undefined;
export function convertBigNumber(rawValue: string | undefined): BigNumber | undefined {
    if (!rawValue) {
        return undefined;
    }

    const value = new BigNumber(rawValue);
    if (value.isNaN()) {
        throw new Error(`Received invalid big number "${rawValue}" from RPC.`);
    }
    return value;
}
