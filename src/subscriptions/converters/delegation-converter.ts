import * as rpc from '@taquito/rpc';
import { singleton } from 'tsyringe';

import { DelegationNotification } from '../../entity/subscriptions/delegation-notification';
import { OperationKind } from '../../entity/subscriptions/operation-notification';
import { create } from '../../utils/creation';
import { BaseOperationProperties } from './block-converter';
import { convertBigNumber } from './common/big-number-converter';
import { OperationMetadataConverter } from './common/operation-metadata-converter';
import { DelegationResultConverter } from './operation-results/delegation-result-converter';

@singleton()
export class DelegationConverter {
    constructor(
        private readonly metadataConverter: OperationMetadataConverter,
        private readonly resultConverter: DelegationResultConverter,
    ) {}

    convert(rpcOperation: rpc.OperationContentsAndResultDelegation, baseProperties: BaseOperationProperties): DelegationNotification {
        return create(DelegationNotification, {
            ...baseProperties,
            kind: OperationKind[rpcOperation.kind],
            source: rpcOperation.source,
            fee: convertBigNumber(rpcOperation.fee),
            counter: convertBigNumber(rpcOperation.counter),
            gas_limit: convertBigNumber(rpcOperation.gas_limit),
            storage_limit: convertBigNumber(rpcOperation.storage_limit),
            delegate: rpcOperation.delegate,
            metadata: this.metadataConverter.convert(rpcOperation, this.resultConverter),
        });
    }
}
