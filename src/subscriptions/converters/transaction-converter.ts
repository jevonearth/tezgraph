import * as rpc from '@taquito/rpc';
import { singleton } from 'tsyringe';

import { OperationKind } from '../../entity/subscriptions/operation-notification';
import { TransactionNotification } from '../../entity/subscriptions/transaction-notification';
import { create } from '../../utils/creation';
import { BaseOperationProperties } from './block-converter';
import { convertBigNumber } from './common/big-number-converter';
import { OperationMetadataConverter } from './common/operation-metadata-converter';
import { TransactionResultConverter } from './operation-results/transaction-result-converter';

@singleton()
export class TransactionConverter {
    constructor(
        private readonly metadataConverter: OperationMetadataConverter,
        private readonly resultConverter: TransactionResultConverter,
    ) {}

    convert(rpcOperation: rpc.OperationContentsAndResultTransaction, baseProperties: BaseOperationProperties): TransactionNotification {
        return create(TransactionNotification, {
            ...baseProperties,
            kind: OperationKind[rpcOperation.kind],
            source: rpcOperation.source,
            fee: convertBigNumber(rpcOperation.fee),
            counter: convertBigNumber(rpcOperation.counter),
            gas_limit: convertBigNumber(rpcOperation.gas_limit),
            storage_limit: convertBigNumber(rpcOperation.storage_limit),
            amount: convertBigNumber(rpcOperation.amount),
            destination: rpcOperation.destination,
            parameters: rpcOperation.parameters,
            metadata: this.metadataConverter.convert(rpcOperation, this.resultConverter),
        });
    }
}
