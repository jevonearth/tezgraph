import * as rpc from '@taquito/rpc';
import { singleton } from 'tsyringe';

import { OperationKind } from '../../entity/subscriptions/operation-notification';
import { SeedNonceRevelationNotification } from '../../entity/subscriptions/seed-nonce-revelation-notification';
import { create } from '../../utils/creation';
import { BaseOperationProperties } from './block-converter';
import { OperationMetadataConverter } from './common/operation-metadata-converter';

@singleton()
export class SeedNonceRevelationConverter {
    constructor(private readonly metadataConverter: OperationMetadataConverter) {}

    convert(rpcOperation: rpc.OperationContentsAndResultRevelation, baseProperties: BaseOperationProperties): SeedNonceRevelationNotification {
        return create(SeedNonceRevelationNotification, {
            ...baseProperties,
            kind: OperationKind[rpcOperation.kind],
            level: rpcOperation.level,
            nonce: rpcOperation.nonce,
            metadata: this.metadataConverter.convertSimple(rpcOperation),
        });
    }
}
