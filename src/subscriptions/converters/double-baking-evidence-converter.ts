import * as rpc from '@taquito/rpc';
import { singleton } from 'tsyringe';

import {
    DoubleBakingEvidenceBlockHeader,
    DoubleBakingEvidenceNotification,
} from '../../entity/subscriptions/double-baking-evidence-notification';
import { OperationKind } from '../../entity/subscriptions/operation-notification';
import { create } from '../../utils/creation';
import { BaseOperationProperties } from './block-converter';
import { OperationMetadataConverter } from './common/operation-metadata-converter';

@singleton()
export class DoubleBakingEvidenceConverter {
    constructor(private readonly metadataConverter: OperationMetadataConverter) {}

    convert(rpcOperation: rpc.OperationContentsAndResultDoubleBaking, baseProperties: BaseOperationProperties): DoubleBakingEvidenceNotification {
        return create(DoubleBakingEvidenceNotification, {
            ...baseProperties,
            kind: OperationKind[rpcOperation.kind],
            bh1: this.convertHeader(rpcOperation.bh1),
            bh2: this.convertHeader(rpcOperation.bh2),
            metadata: this.metadataConverter.convertSimple(rpcOperation),
        });
    }

    private convertHeader(rpcHeader: rpc.BlockFullHeader): DoubleBakingEvidenceBlockHeader {
        return {
            level: rpcHeader.level,
            proto: rpcHeader.proto,
            predecessor: rpcHeader.predecessor,
            timestamp: rpcHeader.timestamp,
            validation_pass: rpcHeader.validation_pass,
            operations_hash: rpcHeader.operations_hash,
            fitness: rpcHeader.fitness,
            context: rpcHeader.context,
            priority: rpcHeader.priority,
            proof_of_work_nonce: rpcHeader.proof_of_work_nonce,
            seed_nonce_hash: rpcHeader.seed_nonce_hash,
            signature: rpcHeader.signature,
        };
    }
}
