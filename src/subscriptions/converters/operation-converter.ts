import * as rpc from '@taquito/rpc';
import { singleton } from 'tsyringe';

import { OperationNotification } from '../../entity/subscriptions/operation-notification';
import { ActivateAccountConverter } from './activate-account-converter';
import { BallotConverter } from './ballot-converter';
import { BaseOperationProperties } from './block-converter';
import { DelegationConverter } from './delegation-converter';
import { DoubleBakingEvidenceConverter } from './double-baking-evidence-converter';
import { DoubleEndorsementEvidenceConverter } from './double-endorsement-evidence-converter';
import { EndorsementConverter } from './endorsement-converter';
import { OriginationConverter } from './origination-converter';
import { ProposalsConverter } from './proposals-converter';
import { RevealConverter } from './reveal-converter';
import { SeedNonceRevelationConverter } from './seed-nonce-revelation-converter';
import { TransactionConverter } from './transaction-converter';

/** Converts an operation from Tezos RPC format to a GraphQL object. */
@singleton()
export class OperationConverter {
    /* eslint-disable-next-line max-params */
    constructor(
        private readonly activateAccountConverter: ActivateAccountConverter,
        private readonly ballotConverter: BallotConverter,
        private readonly delegationConverter: DelegationConverter,
        private readonly doubleBakingEvidenceConverter: DoubleBakingEvidenceConverter,
        private readonly doubleEndorsementEvidenceConverter: DoubleEndorsementEvidenceConverter,
        private readonly endorsementConverter: EndorsementConverter,
        private readonly originationConverter: OriginationConverter,
        private readonly proposalsConverter: ProposalsConverter,
        private readonly revealConverter: RevealConverter,
        private readonly seedNonceRevelationConverter: SeedNonceRevelationConverter,
        private readonly transactionConverter: TransactionConverter,
    ) {}

    /* eslint-disable-next-line consistent-return */
    convert(rpcOperation: rpc.OperationContentsAndResult, baseProperties: BaseOperationProperties): OperationNotification {
        switch (rpcOperation.kind) {
            case rpc.OpKind.ACTIVATION:
                return this.activateAccountConverter.convert(rpcOperation, baseProperties);
            case rpc.OpKind.BALLOT:
                return this.ballotConverter.convert(rpcOperation, baseProperties);
            case rpc.OpKind.DELEGATION:
                return this.delegationConverter.convert(rpcOperation, baseProperties);
            case rpc.OpKind.DOUBLE_BAKING_EVIDENCE:
                return this.doubleBakingEvidenceConverter.convert(rpcOperation, baseProperties);
            case rpc.OpKind.DOUBLE_ENDORSEMENT_EVIDENCE:
                return this.doubleEndorsementEvidenceConverter.convert(rpcOperation, baseProperties);
            case rpc.OpKind.ENDORSEMENT:
                return this.endorsementConverter.convert(rpcOperation, baseProperties);
            case rpc.OpKind.ORIGINATION:
                return this.originationConverter.convert(rpcOperation, baseProperties);
            case rpc.OpKind.PROPOSALS:
                return this.proposalsConverter.convert(rpcOperation, baseProperties);
            case rpc.OpKind.REVEAL:
                return this.revealConverter.convert(rpcOperation, baseProperties);
            case rpc.OpKind.SEED_NONCE_REVELATION:
                return this.seedNonceRevelationConverter.convert(rpcOperation, baseProperties);
            case rpc.OpKind.TRANSACTION:
                return this.transactionConverter.convert(rpcOperation, baseProperties);
        }
    }
}
