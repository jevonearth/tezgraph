import { PubSub } from 'graphql-subscriptions';
import { singleton } from 'tsyringe';

import { BlockNotification } from '../entity/subscriptions/block-notification';
import { OperationKind, OperationNotification } from '../entity/subscriptions/operation-notification';
import { RpcMonitorBlockHeader } from '../rpc/rpc-monitor-block-header';
import { asReadonly } from '../utils/conversion';
import { Logger, LoggerFactory } from '../utils/logger';

class PubSubTrigger<_TPayload> { /* eslint-disable-line @typescript-eslint/no-unused-vars */
    constructor(readonly name: string) {}
}

/** Strongly-typed triggers for particular payload. */
export const Triggers = asReadonly({
    monitorBlockHeaders: new PubSubTrigger<RpcMonitorBlockHeader>('MONITOR_BLOCK_HEADERS'),
    blocks: new PubSubTrigger<BlockNotification>('BLOCKS'),
    operations: asReadonly(Object.fromEntries(Object.keys(OperationKind)
        .map(k => [<OperationKind>k, new PubSubTrigger<OperationNotification>(`OPERATIONS:${k}`)]))),
});

/** Strongly-typed PubSub. */
@singleton()
export class TypedPubSub {
    private readonly logger: Logger;

    constructor(
        private readonly pubSub: PubSub,
        loggerFactory: LoggerFactory,
    ) {
        this.logger = loggerFactory.get(TypedPubSub);
    }

    publish<TPayload>(trigger: PubSubTrigger<TPayload>, payload: TPayload): void {
        this.logger.debug(`Publishing a payload for trigger ${trigger.name}.`, { payload });
        void this.pubSub.publish(trigger.name, payload);
    }

    subscribe<TPayload>(trigger: PubSubTrigger<TPayload>, onMessage: (p: TPayload) => void): void {
        void this.pubSub.subscribe(trigger.name, onMessage);
    }

    iterate<TPayload>(trigger: PubSubTrigger<TPayload>): AsyncIterator<TPayload> {
        return this.pubSub.asyncIterator(trigger.name);
    }
}
