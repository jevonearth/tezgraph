import { singleton } from 'tsyringe';

import { RpcMonitorBlockHeader } from '../rpc/rpc-monitor-block-header';
import { TezosRpcClient } from '../rpc/tezos-rpc-client';
import { Triggers, TypedPubSub } from './typed-pub-sub';

/** Provides the header of the head block. Usually it comes via PubSub from monitor. */
@singleton()
export class HeadBlockProvider {
    private header: RpcMonitorBlockHeader | undefined;

    constructor(
        pubSub: TypedPubSub,
        private readonly rpcClient: TezosRpcClient,
    ) {
        pubSub.subscribe(Triggers.monitorBlockHeaders, header => {
            this.header = header;
        });
    }

    async getHeader(): Promise<RpcMonitorBlockHeader> {
        if (!this.header) {
            this.header = await this.rpcClient.getHeadBlockHeader();
        }
        return this.header;
    }
}
