-- // This endorsement function is the old one, performs a little faster however it does not get the delegate data. Unless the delegate data is for sure the same as the sender, we need to use the other function.
-- CREATE OR REPLACE FUNCTION get_endorsement (address varchar)
-- RETURNS TABLE(
-- kind text,
-- id bigint,
-- level int,
-- "timestamp" timestamp,
-- block char,
-- hash char,
-- source char,
-- fee bigint,
-- counter char,
-- gas_limit char,
-- storage_limit char,
-- op_id smallint,
-- public_key char,
-- amount bigint,
-- destination char,
-- "parameters" char,
-- "slots" char,
-- entrypoint char,
-- contract_address char,
-- delegate char
-- )
-- AS $$
-- select  
-- 'endorsement' as kind, 
-- op.autoid::bigint as id,
-- b.level as level,
-- b.timestamp as timestamp,
-- b.hash as block,
-- oa.hash as hash,
-- oa.sender as source,
-- cast(null as bigint) as fee,
-- cast(null as char) as counter,
-- cast(null as char) as gas_limit,
-- cast(null as char) as storage_limit,
-- oa.id as op_id,
-- cast(null as char) as public_key,
-- cast(null as bigint) as amount,
-- cast(null as char) as destination,
-- cast(null as char) as parameters,
-- cast(null as char) as slots,
-- cast(null as char) as entrypoint,
-- cast(null as char) as contact_address,
-- cast(null as char) as de,legate
-- from operation_alpha oa
-- join operation op on 
-- oa.hash = op.hash
-- join block b on
-- op.block_hash = b.hash
-- where oa.operation_kind = 0
-- and oa.sender = address
-- order by oa.autoid
-- $$ LANGUAGE SQL;

CREATE OR REPLACE FUNCTION get_endorsement (address varchar)
RETURNS TABLE(
kind text,
id bigint,
level int,
"timestamp" timestamp,
block char,
hash char,
source char,
fee bigint,
counter char,
gas_limit char,
storage_limit char,
op_id smallint,
public_key char,
amount bigint,
destination char,
"parameters" char,
"slots" char,
entrypoint char,
contract_address char,
delegate char
)
AS $$
select  
'endorsement' as kind, 
op.autoid::bigint as id,
b.level as level,
b.timestamp as timestamp,
b.hash as block,
oa.hash as hash,
oa.sender as source,
cast(null as bigint) as fee,
cast(null as char) as counter,
cast(null as char) as gas_limit,
cast(null as char) as storage_limit,
oa.id as op_id,
cast(null as char) as public_key,
cast(null as bigint) as amount,
oa.receiver as destination,
cast(null as char) as parameters,
e.slots as slots,
cast(null as char) as entrypoint,
cast(null as char) as contact_address,
e.delegate as delegate
from endorsement e
join operation_alpha oa on
e.operation_hash = oa.hash and e.op_id = oa.id
join operation op on 
oa.hash = op.hash
join block b on
op.block_hash = b.hash
where oa.sender = address
order by oa.autoid
$$ LANGUAGE SQL;

CREATE OR REPLACE FUNCTION get_transaction (address varchar)
RETURNS TABLE(
kind text,
id bigint,
level int,
"timestamp" timestamp,
block char,
hash char,
source char,
fee bigint,
counter char,
gas_limit char,
storage_limit char,
op_id smallint,
public_key char,
amount bigint,
destination char,
"parameters" char,
"slots" char,
entrypoint char,
contract_address char,
delegate char
)
AS $$
select 'transaction', op.autoid::bigint, t.level, t.timestamp, t.block_hash, oa.hash, t.source, t.fee,
       m.counter, m.gas_limit, m.storage_limit, t.op_id,
       null,  -- address (reveal)
       t.amount, -- amount (tx)
       t.destination, -- destination (tx)
       t."parameters", -- parameters (tx)
       null, --slots(endorsement)
       t.entrypoint, -- entrypoint (tx)
       null, -- contract_address (origination)
       null -- delegate (delegation)
from operation_alpha oa, tx_full t, manager_numbers m
,
   operation op
where op.hash = oa.hash AND
 (address = t.destination or address = t.source)
and oa.operation_kind = 8 AND oa.hash = t.operation_hash AND m.hash = oa.hash
$$ LANGUAGE SQL;

CREATE OR REPLACE FUNCTION get_origination (address varchar)
RETURNS TABLE(
kind text,
id bigint,
level int,
"timestamp" timestamp,
block char,
hash char,
source char,
fee bigint,
counter char,
gas_limit char,
storage_limit char,
op_id smallint,
public_key char,
amount bigint,
destination char,
"parameters" char,
"slots" char,
entrypoint char,
contract_address char,
delegate char
)
AS $$
select 'origination', op.autoid::bigint, b.level, b.timestamp, b.hash, oa.hash,
       o.source,
       -- cast(null as bigint), -- bal.diff,
       (select bal.diff from balance bal where bal.operation_hash = op.hash and bal.op_id = oa.id and bal.balance_kind = 2)::bigint,
       m.counter, m.gas_limit, m.storage_limit, o.op_id,
       null,
       cast(null as bigint), -- amount (tx)
       null, -- destination (tx)
       null, -- parameters (tx)
       null, --slots(endorsement)
       null, -- entrypoint (tx)
       o.k, -- contract_address (origination)
       null -- delegate (delegation)
from operation_alpha oa, manager_numbers m, origination o, block b, operation op
--     , balance bal
where (address = o.source or address = o.k) and o.operation_hash = oa.hash
and oa.operation_kind = 9 AND oa.hash = op.hash AND m.hash = oa.hash
and op.block_hash = b.hash
-- and bal.operation_hash = op.hash and bal.balance_kind = 2\
limit 200
$$ LANGUAGE SQL;

CREATE OR REPLACE FUNCTION get_reveal (address varchar)
RETURNS TABLE(
kind text,
id bigint,
level int,
"timestamp" timestamp,
block char,
hash char,
source char,
fee bigint,
counter char,
gas_limit char,
storage_limit char,
op_id smallint,
public_key char,
amount bigint,
destination char,
"parameters" char,
"slots" char,
entrypoint char,
contract_address char,
delegate char
)
AS $$
select 'reveal', op.autoid::bigint, b.level, b.timestamp, b.hash, oa.hash, address, bal.diff,
       m.counter, m.gas_limit, m.storage_limit, oa.id,
       i.pk, -- public_key (reveal)
       cast(null as bigint), -- amount (tx)
       null, -- destination (tx)
       null, -- parameters (tx)
       null, --slots(endorsement)
       null, -- entrypoint (tx)
       null, -- contract_address (origination)
       null  -- delegate (delegation)
from
   operation_alpha oa,
   block b,
   manager_numbers m,
   implicit i,
   operation op,
   -- tx t,
   balance bal
where
i.pkh = address
AND i.revealed = op.hash
AND oa.operation_kind = 7
AND op.hash = oa.hash
AND b.hash = op.block_hash
AND m.hash = oa.hash
-- and t.source = address -- removed this because turns reveals(operation_kind = 7) do not appear in tx_full so this would only work when reveals are part of batch operations
-- and t.operation_hash = op.hash 
and bal.operation_hash = op.hash
and bal.op_id = oa.id
and bal.balance_kind = 2
-- and bal.contract_address = address
group by op.autoid::bigint, b.level, b.timestamp, b.hash, oa.hash, bal.diff,
       m.counter, m.gas_limit, m.storage_limit, oa.id,
       i.pk
limit 200
$$ LANGUAGE SQL;

CREATE OR REPLACE FUNCTION get_delegation (address varchar)
RETURNS TABLE(
kind text,
id bigint,
level int,
"timestamp" timestamp,
block char,
hash char,
source char,
fee bigint,
counter char,
gas_limit char,
storage_limit char,
op_id smallint,
public_key char,
amount bigint,
destination char,
"parameters" char,
"slots" char,
entrypoint char,
contract_address char,
delegate char
)
AS $$
select 'delegation', op.autoid::bigint, b.level, b.timestamp, b.hash, oa.hash,
       d.source,
      --  bal.diff, -- cast(null as bigint),
      (select bal.diff from balance bal where bal.operation_hash = op.hash and bal.op_id = oa.id and bal.balance_kind = 2)::bigint,-- fees
       m.counter, m.gas_limit, m.storage_limit, d.op_id,
       null, -- public_key (revelation)
       cast(null as bigint), --amount (tx)
       null, -- destination (tx)
       null, -- parameters (tx)
       null, --slots(endorsement)
       null, -- entrypoint (tx)
       null, -- contract_address (origination)
       pkh -- delegate (delegation)
from operation_alpha oa, manager_numbers m, delegation d, block b, operation op, balance bal
where
 (address = d.pkh or address = d.source) and d.operation_hash = oa.hash
AND m.hash = oa.hash and oa.hash = op.hash and op.block_hash = b.hash
and oa.operation_kind = 10
and bal.operation_hash = op.hash and bal.balance_kind = 2
limit 200
$$ LANGUAGE SQL;

CREATE OR REPLACE FUNCTION get_seed_nonce_revelation (address varchar)
RETURNS TABLE(
kind text,
id bigint,
level int,
"timestamp" timestamp,
block char,
hash char,
source char,
fee bigint,
counter char,
gas_limit char,
storage_limit char,
op_id smallint,
public_key char,
amount bigint,
destination char,
"parameters" char,
"slots" char,
entrypoint char,
contract_address char,
delegate char
)
AS $$
select  
'seed_nonce_revelation' as kind, 
op.autoid::bigint as id,
b.level as level,
b.timestamp as timestamp,
b.hash as block,
oa.hash as hash,
cast(null as char) as source,
cast(null as bigint) as fee,
cast(null as char) as counter,
cast(null as char) as gas_limit,
cast(null as char) as storage_limit,
oa.id as op_id,
cast(null as char) as public_key,
cast(null as bigint) as amount,
oa.receiver as destination,
cast(null as char) as parameters,
cast(null as char) as slots,
cast(null as char) as entrypoint,
cast(null as char) as contact_address,
cast(null as char) as delegate
from operation_alpha oa
join operation op on 
oa.hash = op.hash
join block b on
op.block_hash = b.hash
where oa.operation_kind = 1
and oa.receiver = address
limit 200
$$ LANGUAGE SQL;

CREATE OR REPLACE FUNCTION get_double_endorsement_evidence (address varchar)
RETURNS TABLE(
kind text,
id bigint,
level int,
"timestamp" timestamp,
block char,
hash char,
source char,
fee bigint,
counter char,
gas_limit char,
storage_limit char,
op_id smallint,
public_key char,
amount bigint,
destination char,
"parameters" char,
"slots" char,
entrypoint char,
contract_address char,
delegate char
)
AS $$
select  
'double_endorsement_evidence' as kind, 
op.autoid::bigint as id,
b.level as level,
b.timestamp as timestamp,
b.hash as block,
oa.hash as hash,
cast(null as char) as source,
cast(null as bigint) as fee,
cast(null as char) as counter,
cast(null as char) as gas_limit,
cast(null as char) as storage_limit,
oa.id as op_id,
cast(null as char) as public_key,
cast(null as bigint) as amount,
oa.receiver as destination,
cast(null as char) as parameters,
cast(null as char) as slots,
cast(null as char) as entrypoint,
cast(null as char) as contact_address,
cast(null as char) as delegate
from operation_alpha oa
join operation op on 
oa.hash = op.hash
join block b on
op.block_hash = b.hash
where oa.operation_kind = 2
and oa.receiver = address
limit 200
$$ LANGUAGE SQL;

CREATE OR REPLACE FUNCTION get_double_baking_evidence (address varchar)
RETURNS TABLE(
kind text,
id bigint,
level int,
"timestamp" timestamp,
block char,
hash char,
source char,
fee bigint,
counter char,
gas_limit char,
storage_limit char,
op_id smallint,
public_key char,
amount bigint,
destination char,
"parameters" char,
"slots" char,
entrypoint char,
contract_address char,
delegate char
)
AS $$
select  
'double_baking_evidence' as kind, 
op.autoid::bigint as id,
b.level as level,
b.timestamp as timestamp,
b.hash as block,
oa.hash as hash,
cast(null as char) as source,
cast(null as bigint) as fee,
cast(null as char) as counter,
cast(null as char) as gas_limit,
cast(null as char) as storage_limit,
oa.id as op_id,
cast(null as char) as public_key,
cast(null as bigint) as amount,
oa.receiver as destination,
cast(null as char) as parameters,
cast(null as char) as slots,
cast(null as char) as entrypoint,
cast(null as char) as contact_address,
cast(null as char) as delegate
from operation_alpha oa
join operation op on 
oa.hash = op.hash
join block b on
op.block_hash = b.hash
where oa.operation_kind = 3
and oa.receiver = address
limit 200
$$ LANGUAGE SQL;

CREATE OR REPLACE FUNCTION get_activate_account (address varchar)
RETURNS TABLE(
kind text,
id bigint,
level int,
"timestamp" timestamp,
block char,
hash char,
source char,
fee bigint,
counter char,
gas_limit char,
storage_limit char,
op_id smallint,
public_key char,
amount bigint,
destination char,
"parameters" char,
"slots" char,
entrypoint char,
contract_address char,
delegate char
)
AS $$
select  
'activate_account' as kind, 
op.autoid::bigint as id,
b.level as level,
b.timestamp as timestamp,
b.hash as block,
oa.hash as hash,
oa.sender as source,
cast(null as bigint) as fee,
cast(null as char) as counter,
cast(null as char) as gas_limit,
cast(null as char) as storage_limit,
oa.id as op_id,
cast(null as char) as public_key,
cast(null as bigint) as amount,
oa.receiver as destination,
cast(null as char) as parameters,
cast(null as char) as slots,
cast(null as char) as entrypoint,
cast(null as char) as contact_address,
cast(null as char) as delegate
from operation_alpha oa
join operation op on 
oa.hash = op.hash
join block b on
op.block_hash = b.hash
where oa.operation_kind = 4
and oa.receiver = address
limit 200
$$ LANGUAGE SQL;

create or replace function get_operations (address varchar)
RETURNS TABLE(
kind text,
id bigint,
level int,
"timestamp" timestamp,
block char,
hash char,
source char,
fee bigint,
counter char,
gas_limit char,
storage_limit char,
op_id smallint,
public_key char,
amount bigint,
destination char,
"parameters" char,
"slots" char,
entrypoint char,
contract_address char,
delegate char
)
AS $$
((select * from get_delegation(address))
union
(select * from get_origination(address))
union
(select * from get_transaction(address))
union
(select * from get_reveal(address))
union
(select * from get_endorsement(address) limit 200)
union
(select * from get_seed_nonce_revelation(address))
union
(select * from get_double_endorsement_evidence(address))
union
(select * from get_double_baking_evidence(address))
union
(select * from get_activate_account(address))
order by id)
$$ language sql;

CREATE OR REPLACE FUNCTION get_ops_transaction(address varchar, lastid integer, lim integer)
RETURNS TABLE(
kind text,
id bigint,
level int,
"timestamp" timestamp,
block char,
hash char,
source char,
fee bigint,
counter char,
gas_limit char,
storage_limit char,
op_id smallint,
public_key char,
amount bigint,
destination char,
"parameters" char,
"slots" char,
entrypoint char,
contract_address char,
delegate char
)
AS $$
((select * from get_transaction(address) where id < lastid order by id desc limit lim)
)
$$ language sql;

CREATE OR REPLACE FUNCTION get_ops_reveal(address varchar, lastid integer, lim integer)
RETURNS TABLE(
kind text,
id bigint,
level int,
"timestamp" timestamp,
block char,
hash char,
source char,
fee bigint,
counter char,
gas_limit char,
storage_limit char,
op_id smallint,
public_key char,
amount bigint,
destination char,
"parameters" char,
"slots" char,
entrypoint char,
contract_address char,
delegate char
)
AS $$
((select * from get_reveal(address) where id < lastid order by id desc limit lim)
)
$$ language sql;

CREATE OR REPLACE FUNCTION get_ops_delegation(address varchar, lastid integer, lim integer)
RETURNS TABLE(
kind text,
id bigint,
level int,
"timestamp" timestamp,
block char,
hash char,
source char,
fee bigint,
counter char,
gas_limit char,
storage_limit char,
op_id smallint,
public_key char,
amount bigint,
destination char,
"parameters" char,
"slots" char,
entrypoint char,
contract_address char,
delegate char
)
AS $$
((select * from get_delegation(address) where id < lastid order by id desc limit lim)
)
$$ language sql;

CREATE OR REPLACE FUNCTION get_ops_origination(address varchar, lastid integer, lim integer)
RETURNS TABLE(
kind text,
id bigint,
level int,
"timestamp" timestamp,
block char,
hash char,
source char,
fee bigint,
counter char,
gas_limit char,
storage_limit char,
op_id smallint,
public_key char,
amount bigint,
destination char,
"parameters" char,
"slots" char,
entrypoint char,
contract_address char,
delegate char
)
AS $$
((select * from get_origination(address) where id < lastid order by id desc limit lim)
)
$$ language sql;

CREATE OR REPLACE FUNCTION get_ops_operations(address varchar, lastid integer, lim integer)
RETURNS TABLE(
kind text,
id bigint,
level int,
"timestamp" timestamp,
block char,
hash char,
source char,
fee bigint,
counter char,
gas_limit char,
storage_limit char,
op_id smallint,
public_key char,
amount bigint,
destination char,
"parameters" char,
"slots" char,
entrypoint char,
contract_address char,
delegate char
)
AS $$
((select  
kind, 
id,
level,
timestamp,
block,
hash,
source,
fee,
counter,
gas_limit,
storage_limit,
op_id,
public_key,
amount,
destination,
parameters,
slots,
entrypoint,
contract_address,
delegate
from get_operations(address) where id < lastid order by id desc limit lim)
)
$$ language sql;