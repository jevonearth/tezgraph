import { createLogger, format, transports } from 'winston';

const { combine, timestamp, simple } = format;

interface Logger {
    debug(message: string): void;
    info(message: string, parameters: { status: string; uuid: string; duration?: string; query?: string }): void;
    warn(message: string): void;
    error(message: string, parameters: { status: string; uuid: string; type: string; error: any; query?: string }): void;

}

const loggerConfig = {
    format: combine(timestamp(), simple()),
    transports: [
        new transports.File({ filename: `/var/logs/${new Date().toJSON()
            .substring(0, 10)}.log` }),
        new transports.Console(),
    ],
};

export const logger: Logger = createLogger(loggerConfig);
