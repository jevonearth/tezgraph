import * as prom from 'prom-client';

export const query_counter = new prom.Counter({ name: 'query_counter', help: 'query_counter', labelNames: ['status', 'resolver'] });

/*
 * Export const blocks_query_histogram = new prom.Histogram({name: 'blocks_query_historgram', help: 'query_historgram', buckets: [0.01, 0.02, 0.03, 0.04, 0.05, 0.06, 0.07, 0.08, 0.09, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1, 1.1, 1.2, 1.3, 1.4, 1.5]});
 * export const block_query_histogram = new prom.Histogram({name: 'block_query_historgram', help: 'query_historgram', buckets: [0.01, 0.02, 0.03, 0.04, 0.05, 0.06, 0.07, 0.08, 0.09, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1, 1.1, 1.2, 1.3, 1.4, 1.5]});
 */

export const account_query_histogram = new prom.Histogram({ name: 'account_query_histogram', help: 'query_historgram', buckets: [0.01, 0.02, 0.03, 0.04, 0.05, 0.06, 0.07, 0.08, 0.09, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1, 1.1, 1.2, 1.3, 1.4, 1.5] });
export const account_transactions_query_histogram = new prom.Histogram({ name: 'account_transactions_query_histogram', help: 'query_historgram', buckets: [0.01, 0.02, 0.03, 0.04, 0.05, 0.06, 0.07, 0.08, 0.09, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1, 1.1, 1.2, 1.3, 1.4, 1.5] });

/*
 * Export const txs_query_histogram = new prom.Histogram({name: 'txs_query_histogram', help: 'query_historgram', buckets: [0.01, 0.02, 0.03, 0.04, 0.05, 0.06, 0.07, 0.08, 0.09, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1, 1.1, 1.2, 1.3, 1.4, 1.5]});
 * export const txs_involves_query_histogram = new prom.Histogram({name: 'txs_involves_query_histogram', help: 'query_historgram', buckets: [0.01, 0.02, 0.03, 0.04, 0.05, 0.06, 0.07, 0.08, 0.09, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1, 1.1, 1.2, 1.3, 1.4, 1.5]});
 * export const txs_source_query_histogram = new prom.Histogram({name: 'txs_source_query_histogram', help: 'query_historgram', buckets: [0.01, 0.02, 0.03, 0.04, 0.05, 0.06, 0.07, 0.08, 0.09, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1, 1.1, 1.2, 1.3, 1.4, 1.5]});
 * export const txs_destination_query_histogram = new prom.Histogram({name: 'txs_destination_query_histogram', help: 'query_historgram', buckets: [0.01, 0.02, 0.03, 0.04, 0.05, 0.06, 0.07, 0.08, 0.09, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1, 1.1, 1.2, 1.3, 1.4, 1.5]});
 * export const tx_query_histogram = new prom.Histogram({name: 'tx_query_histogram', help: 'query_historgram', buckets: [0.01, 0.02, 0.03, 0.04, 0.05, 0.06, 0.07, 0.08, 0.09, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1, 1.1, 1.2, 1.3, 1.4, 1.5]});
 */

/*
 * Export const contracts_query_histogram = new prom.Histogram({name: 'contracts_query_histogram', help: 'query_historgram', buckets: [0.01, 0.02, 0.03, 0.04, 0.05, 0.06, 0.07, 0.08, 0.09, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1, 1.1, 1.2, 1.3, 1.4, 1.5]});
 * export const contract_query_histogram = new prom.Histogram({name: 'contract_query_histogram', help: 'query_historgram', buckets: [0.01, 0.02, 0.03, 0.04, 0.05, 0.06, 0.07, 0.08, 0.09, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1, 1.1, 1.2, 1.3, 1.4, 1.5]});
 */

// Export const delegate_query_histogram = new prom.Histogram({name: 'delegate_query_histogram', help: 'query_historgram', buckets: [0.01, 0.02, 0.03, 0.04, 0.05, 0.06, 0.07, 0.08, 0.09, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1, 1.1, 1.2, 1.3, 1.4, 1.5]});

export const account_delegations_query_histogram = new prom.Histogram({ name: 'account_delegations_query_histogram', help: 'query_historgram', buckets: [0.01, 0.02, 0.03, 0.04, 0.05, 0.06, 0.07, 0.08, 0.09, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1, 1.1, 1.2, 1.3, 1.4, 1.5] });
export const account_operations_query_histogram = new prom.Histogram({ name: 'account_operations_query_histogram', help: 'query_historgram', buckets: [0.01, 0.02, 0.03, 0.04, 0.05, 0.06, 0.07, 0.08, 0.09, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1, 1.1, 1.2, 1.3, 1.4, 1.5] });

export const account_reveals_query_histogram = new prom.Histogram({ name: 'account_reveals_query_histogram', help: 'query_historgram', buckets: [0.01, 0.02, 0.03, 0.04, 0.05, 0.06, 0.07, 0.08, 0.09, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1, 1.1, 1.2, 1.3, 1.4, 1.5] });

export const account_originations_query_histogram = new prom.Histogram({ name: 'account_originations_query_histogram', help: 'query_historgram', buckets: [0.01, 0.02, 0.03, 0.04, 0.05, 0.06, 0.07, 0.08, 0.09, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1, 1.1, 1.2, 1.3, 1.4, 1.5] });

export const account_endorsements_query_histogram = new prom.Histogram({ name: 'account_endorsements_query_histogram', help: 'query_historgram', buckets: [0.01, 0.02, 0.03, 0.04, 0.05, 0.06, 0.07, 0.08, 0.09, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1, 1.1, 1.2, 1.3, 1.4, 1.5] });
