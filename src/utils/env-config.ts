import dotenv from 'dotenv';

import { errorToString } from './conversion';

export enum Names {
    Host = 'HOST',
    Port = 'PORT',
    TezosNode = 'TEZOS_NODE',
    Env = 'ENV',
    StartBlockLevelMaxFromHead = 'START_BLOCK_LEVEL_MAX_FROM_HEAD',
    RpcRetryDelaysMillis = 'RPC_RETRY_DELAYS_MILLIS',
    SubscriptionKeepAliveMillis = 'SUBSCRIPTION_KEEP_ALIVE_MILLIS',
    GraphQL = 'GRAPH_QL',
}

export class EnvConfig {
    readonly host: string;
    readonly port: number;
    readonly tezosNodeUrl: string;
    readonly environment: string;
    readonly graphQL: { readonly queriesEnabled: boolean; readonly subscriptionsEnabled: boolean };
    readonly startBlockLevelMaxFromHead: number;
    readonly rpcRetryDelaysMillis: readonly number[];
    readonly subscriptionKeepAliveMillis: number;

    constructor(env: Readonly<Record<string, string | undefined>>) {
        this.host = getVar(Names.Host, s => s);
        this.port = getVar(Names.Port, parsePositiveInteger);
        this.tezosNodeUrl = getVar(Names.TezosNode, parseAbsoluteUrl);
        this.environment = getVar(Names.Env, s => s);
        this.startBlockLevelMaxFromHead = getVar(Names.StartBlockLevelMaxFromHead, parsePositiveInteger);
        this.rpcRetryDelaysMillis = getVar(Names.RpcRetryDelaysMillis, parseArrayOfPositiveIntegers);
        this.subscriptionKeepAliveMillis = getVar(Names.SubscriptionKeepAliveMillis, parsePositiveInteger);

        this.graphQL = getVar(Names.GraphQL, rawValue => {
            const supported = new Map([
                ['all', { queriesEnabled: true, subscriptionsEnabled: true }],
                ['queries', { queriesEnabled: true, subscriptionsEnabled: false }],
                ['subscriptions', { queriesEnabled: false, subscriptionsEnabled: true }],
            ]);
            const value = supported.get(rawValue);
            if (!value) {
                throw new Error(`Supported values are only: ${Array.from(supported.keys()).join()}.`);
            }
            return value;
        });

        function getVar<T>(name: string, parseFn: (rawValue: string) => T): T {
            const rawValue = env[name]?.trim();
            try {
                if (!rawValue) {
                    throw new Error('Variable or its value is missing.');
                }
                return parseFn(rawValue);
            } catch (error) {
                throw new Error(`Failed parsing value "${rawValue ?? ''}" of environment variable "${name}". ${errorToString(error)}`);
            }
        }
    }
}

// Actual config initalization. Here -> standalone -> no circular imports -> can be used anywhere e.g. decorators.
const envFile = process.env.NODE_ENV ? `.env.${process.env.NODE_ENV.trim()}` : '.env';
dotenv.config({ path: envFile });
export const config = new EnvConfig(process.env);

function parsePositiveInteger(rawValue: string): number {
    const value = parseInt(rawValue, 10); /* eslint-disable-line @typescript-eslint/no-magic-numbers */
    if (isNaN(value) || value <= 0) { /* eslint-disable-line @typescript-eslint/no-magic-numbers */
        throw new Error('Value must be an integer greater than zero.');
    }
    return value;
}

function parseArrayOfPositiveIntegers(rawValue: string): number[] {
    return rawValue.split(',').map(n => parsePositiveInteger(n));
}

function parseAbsoluteUrl(value: string): string {
    try {
        return new URL(value).toString();
    } catch {
        throw new Error('Value must be an absolute URL.');
    }
}
