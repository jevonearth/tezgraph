import { DependencyContainer } from 'tsyringe';
import { ContainerType, ResolverData } from 'type-graphql';

export class GraphQLContainer implements ContainerType {
    constructor(private readonly container: DependencyContainer) {}

    /* eslint-disable @typescript-eslint/explicit-module-boundary-types, @typescript-eslint/no-explicit-any, @typescript-eslint/no-unsafe-return */
    get(someClass: any, _resolverData: ResolverData<any>): any | Promise<any> {
        return this.container.resolve(someClass);
    }
}
