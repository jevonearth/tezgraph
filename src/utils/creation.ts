import { DefaultConstructor } from './reflection';

export function create<T>(targetType: DefaultConstructor<T>, properties: T): T {
    return Object.assign(new targetType(), properties);
}
