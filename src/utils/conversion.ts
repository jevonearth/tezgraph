export function asReadonly<T>(items: T[]): readonly T[];
export function asReadonly<T>(obj: T): Readonly<T>;
export function asReadonly<T>(obj: T[] | T): readonly T[] | Readonly<T> {
    return obj;
}

export function errorToString(error: unknown): string {
    if (error instanceof Error) {
        return error.toString();
    }
    switch (typeof error) {
        case 'string':
            return error;
        case 'undefined':
            return 'undefined';
        default:
            return JSON.stringify(error);
    }
}
