import { validateAddress } from '@taquito/utils';
import { UserInputError } from 'apollo-server-express';
import { performance } from 'perf_hooks';
import { Field, InputType, registerEnumType } from 'type-graphql';
import { prisma } from '../database/prisma';
import { DelegationConnection, DelegationEdge } from '../entity/delegation';
import { OperationsConnection, OperationsEdge } from '../entity/operations';
import { OriginationConnection, OriginationEdge } from '../entity/origination';
import { EndorsementConnection, EndorsementEdge } from '../entity/endorsement';

import { PageInfo } from '../entity/pageInfo';
import { RevealConnection, RevealEdge } from '../entity/reveal';
import { TransactionConnection, TransactionEdge } from '../entity/transaction';
import { logger } from '../monitoring/logger';
import { query_counter } from '../monitoring/prometheus';
import { format } from 'fecha';

// Server Default Record Limit
export const pageSizeLimit: number = process.env.PAGE_SIZE_LIMIT ? parseInt(process.env.PAGE_SIZE_LIMIT) : 200;

// Address Validator
export const addressValid = function (address: string): boolean {
    const validation = validateAddress(address);
    switch (validation) {
        case 0:
            throw new UserInputError('Unknown Address: The provided address must be in a valid tz1, tz2, tz3, or KT1 address format.');
        case 1:
            throw new UserInputError('Invalid Address: The provided address must be a valid tz1, tz2, tz3, or KT1 address.');
        case 2:
            throw new UserInputError('Unknown Address: The provided address must be in a valid tz1, tz2, tz3, or KT1 address format.');
        case 3:
            return true;
        default:
            return false;
    }
};

// DateRange Argument Fields
@InputType()
export class DateRange {
    @Field({ nullable: true })
    gte?: string;

    @Field({ nullable: true })
    lte?: string;
}

// DateRange Argument Validator
export const dateRangeValid = function (dateObj: DateRange): boolean {
    if (dateObj.lte && dateObj.gte) {
        if (!(new Date(dateObj.gte) < new Date(dateObj.lte))) {
            throw new UserInputError('Argument "date" of type "DateRange?" must have the "lte" value greater than the "gte" value.');
        }
    }

    return true;
};

// Cursor Validator
export const cursorValid = function (cursor: string): boolean {
    const cursorSplit = cursor.split(':');
    if (!cursorSplit[0].startsWith('oo') && !cursorSplit[0].startsWith('op') && !cursorSplit[0].startsWith('on')) {
        throw new UserInputError('Argument "cursor" of type "string?" contains a invalid operation hash. The valid cursor format is "operation_hash:operation_id".');
    }
    if (cursorSplit[0].length !== 51 || isNaN(parseInt(cursorSplit[1]))) {
        throw new UserInputError('Argument "cursor" of type "string?" is in a invalid format. The valid cursor format is "operation_hash:operation_id".');
    }
    return true;
};

// OrderBy Direction Enums
export enum OrderByDirection {
    ASC = 'ASC',
    DESC = 'DESC',
}

registerEnumType(OrderByDirection, {
    name: 'OrderByDirection',
    description: 'The available options for the order_by.direction field value.',
});

// OrderBy Field Enums
export enum OrderByField {
    TIMESTAMP = 'timestamp',
    // KIND = "kind", - only for account.operations
    HASH = 'hash',
    LEVEL = 'level',
    BLOCK = 'block',
    ID = 'id',
}

registerEnumType(OrderByField, {
    name: 'OrderByField',
    description: 'The available options for the order_by.field field value.',
});

@InputType()
export class OrderBy {
    @Field(() => OrderByField, { nullable: true })
    field!: OrderByField;

    @Field(() => OrderByDirection, { nullable: true })
    direction!: OrderByDirection;
}

export const createOrderBy = function (field: OrderByField, direction: OrderByDirection): OrderBy {
    const orderBy = Object.create(OrderBy);
    orderBy.field = field;
    orderBy.direction = direction;

    return orderBy;
};

export const createBeforeOrderBy = function (_first: number | undefined, last: number | undefined, before: string, orderBy: OrderBy): OrderBy {
    if (!orderBy.field || !orderBy.direction) {
        throw new UserInputError('Argument Error - order_by: Unable to process order_by argument. Missing order_by.field and/or order_by.direction.');
    }

    const beforeOrderBy = createOrderBy(orderBy.field, orderBy.direction);

    if (last && orderBy && before && orderBy.direction == 'DESC') {
        beforeOrderBy.direction = OrderByDirection.ASC;
    }

    if (last && orderBy && before && orderBy.direction == 'ASC') {
        beforeOrderBy.direction = OrderByDirection.DESC;
    }

    return beforeOrderBy;
};

export const createOrderByClause = function (first: number | undefined, last: number | undefined, orderBy: OrderBy | undefined): string {
    let orderByClause = 'ORDER BY id DESC, hash ASC, op_id ASC';

    if (orderBy) {
        orderByClause = `ORDER BY ${orderBy.field} ${orderBy.direction || 'ASC'}  NULLS LAST, hash ASC, op_id ASC`;

        if (first && orderBy.direction == 'ASC') {
            orderByClause = `ORDER BY ${orderBy.field} ASC NULLS LAST, hash ASC, op_id ASC`;
        }

        if (last && orderBy.direction == 'DESC') {
            orderByClause = `ORDER BY ${orderBy.field} ASC NULLS LAST, hash ASC, op_id ASC`;
        }

        if (last && orderBy.direction == 'ASC') {
            orderByClause = `ORDER BY ${orderBy.field} DESC NULLS LAST, hash ASC, op_id ASC`;
        }
    }

    return orderByClause;
};

const dateRangeTimestampCleanUp = function (timestamp: string): string {
    /*
     * This is used to handle the case in which the user only enters the year.
     * Depending on the timezone, the date may be changed to something different.
     * This way we ensure that we use the 01/01 of the year the user entered.
     */
    if (timestamp.length == 7 || timestamp.length == 4) {
        timestamp += '/01';
    }

    /*
     * This is used to handle the case in which the user only enters a date without time.
     * Depending on the timezone, the date may be changed to something different.
     * This way we ensure that we use the 00:00:00 of the date the user entered.
     */
    if (timestamp.length < 11) {
        timestamp = format(new Date(timestamp), 'shortDate');
    }

    switch (true) {
        case timestamp.includes('PM'):
            timestamp = timestamp.replace('PM', ' PM');
            break;
        case timestamp.includes('pm'):
            timestamp = timestamp.replace('pm', ' pm');
            break;
        case timestamp.includes('AM'):
            timestamp = timestamp.replace('AM', ' AM');
            break;
        case timestamp.includes('am'):
            timestamp = timestamp.replace('am', ' am');
            break;
    }

    switch (true) {
        case timestamp.includes('Z'):
            timestamp = timestamp.replace('Z', '');
            break;
    }
    return timestamp;
};

export const dateRangeFormatter = function (dateRange: DateRange): DateRange {
    try {
        if (dateRange.gte) {
            dateRange.gte = dateRangeTimestampCleanUp(dateRange.gte);
            let isoDateTime = format(new Date(dateRange.gte), 'isoDateTime');
            isoDateTime = isoDateTime.substring(0, 19);
            dateRange.gte = isoDateTime;
        }

        if (dateRange.lte) {
            dateRange.lte = dateRangeTimestampCleanUp(dateRange.lte);
            let isoDateTime = format(new Date(dateRange.lte), 'isoDateTime');
            isoDateTime = isoDateTime.substring(0, 19);
            const dateEG = isoDateTime;
            dateRange.lte = dateEG;
        }
        return dateRange;
    } catch (err) {
        throw new UserInputError(`Argument Error - date_range:  ${err}`);
    }
};

export const createDateRangeClause = function (dateRange: DateRange | undefined): string {
    if (dateRange) {
        dateRange = dateRangeFormatter(dateRange);
        dateRangeValid(dateRange);
        return getDateRangeClause(dateRange);
    }

    return '';
};

export const setTakeLimit = function (first: number | undefined, last: number | undefined): number {
    let takeLimit = pageSizeLimit;

    if (first && last) {
        throw new Error('cant have both first and last');
    } else if (first) {
        takeLimit = first;
    } else if (last) {
        takeLimit = last;
    }

    limitValid(takeLimit);
    return takeLimit;
};

export class RelayPageData {
    direction?: string | undefined;
    order?: string;
}

export const createRelayPageData = function (_first: number | undefined, last: number | undefined, before: string | undefined, _after: string | undefined): RelayPageData {
    const relayPageDataData = Object.create(RelayPageData);

    if (last) {
        relayPageDataData.direction = 'last';
    } else {
        relayPageDataData.direction = 'first';
    }

    relayPageDataData.order = before ? 'before' : 'after';
    return relayPageDataData;
};

export const applyRelayOrder = function (first: number | undefined, last: number | undefined, before: string | undefined, after: string | undefined, orderBy: OrderBy, operationQuery: string): string {
    if (first && orderBy && !after && orderBy.direction != 'ASC') {
        operationQuery = `SELECT * FROM (${operationQuery}) AS result ORDER BY ${orderBy.field} DESC`;
    }

    if (last && orderBy && !before && !after && orderBy.direction == 'DESC') {
        operationQuery = `SELECT * FROM (${operationQuery}) AS result ORDER BY ${orderBy.field} DESC`;
    }

    if (last && orderBy && before && orderBy.direction == 'DESC') {
        operationQuery = `SELECT * FROM (${operationQuery}) AS result ORDER BY ${orderBy.field} DESC`;
    }

    if (last && orderBy && before && orderBy.direction == 'ASC') {
        operationQuery = `SELECT * FROM (${operationQuery}) AS result ORDER BY ${orderBy.field} ASC`;
    }

    if (last && !before && !after && orderBy && orderBy.direction == 'ASC') {
        operationQuery = `SELECT * FROM (${operationQuery}) AS result ORDER BY ${orderBy.field} ASC`;
    }

    if (last && !before && after && orderBy && orderBy.direction == 'DESC') {
        operationQuery = `SELECT * FROM (${operationQuery}) AS result ORDER BY ${orderBy.field} DESC`;
    }

    if (last && !before && after && orderBy && orderBy.direction == 'ASC') {
        operationQuery = `SELECT * FROM (${operationQuery}) AS result ORDER BY ${orderBy.field} ASC`;
    }

    return operationQuery;
};

// Error Type Checker Logger
export const errorCheck = function (err: UserInputError, resolver: string, uuid: string): UserInputError {
    if (err.extensions && err.extensions.code && err.extensions.code == 'BAD_USER_INPUT') {
        query_counter.labels('input_error', resolver).inc(1);
        logger.error(`${resolver} resolver`, { status: 'error', uuid, type: 'input_error', error: { message: err.message, code: err.extensions.code } });
        return err;
    }

    query_counter.labels('unexpected_error', resolver).inc(1);
    logger.error(`${resolver} resolver`, { status: 'error', uuid, type: 'unexpected_error', error: err });
    return err;
};

// Limit Validator
export const limitValid = function (limit: number | undefined): boolean {
    if (limit && limit > pageSizeLimit) {
        throw new UserInputError(`Argument "limit" of type "Float?" cannot be greater than ${pageSizeLimit}.`);
    }
    return true;
};

// Result Validator
export const queryResultsFound = function (results: any[]): boolean {
    if (results.length === 0) {
        throw new UserInputError('Unknown Account: No results found for the provided address.');
    }
    return true;
};

// Date Range Query
export const getDateRangeClause = function (dateRange: DateRange): string {
    let dateRangeClause: string;

    if (dateRange.lte && dateRange.gte) {
        dateRangeClause = `WHERE (timestamp >= '${dateRange.gte}' AND timestamp <= '${dateRange.lte}')`;
    } else if (dateRange.lte) {
        dateRangeClause = `WHERE (timestamp <= '${dateRange.lte}')`;
    } else if (dateRange.gte) {
        dateRangeClause = `WHERE (timestamp >= '${dateRange.gte}')`;
    } else {
        throw 'Argument "date" of type "dateArg?" must be in a valid format.';
    }

    return dateRangeClause;
};

// Cursor Transaction Query
export const getCursorRecord = async function (operation: string, pkh: string, opHash: string, opID: string, uuid: string) {
    logger.info(`account.getCursorRecord(${operation}) resolver query`, { status: 'start', uuid });
    const performanceStart = performance.now();
    const cursorRecordQuery
        = `SELECT * FROM get_${operation}('${pkh}') WHERE hash = '${opHash}' and op_id = '${opID}' limit 1`;

    const cursorRecord = await prisma.$queryRaw(cursorRecordQuery);
    const performanceEnd = performance.now();
    logger.info(`account.getCursorRecord(${operation}) resolver query`, { status: 'end', uuid, duration: `${performanceEnd - performanceStart} ms`, query: cursorRecordQuery });

    return cursorRecord;
};

// Cursor Query
export const getCursorQuery = async function (operation: string, cursor: string, relayPageData: any, pkh: string, takeLimit: number, uuid: string, orderBy: OrderBy, dateRange?: DateRange): Promise<string> {
    const cursorSplit = cursor.split(':');
    const cursorRecord = await getCursorRecord(operation, pkh, cursorSplit[0], cursorSplit[1], uuid);
    let selectStatement;

    switch (operation) {
        case 'transaction':
            selectStatement = `SELECT id, hash, op_id as batch_position, source, destination, kind, level, block, counter, gas_limit, storage_limit, entrypoint, fee as gas, amount, parameters, timestamp, CONCAT(hash, CONCAT(':', op_id)) AS cursor`;
            break;
        case 'delegation':
            selectStatement = `SELECT DISTINCT id, hash, op_id as batch_position, source, delegate, kind, timestamp, level, block, counter, gas_limit, storage_limit, public_key, amount, parameters, contract_address, fee as gas, CONCAT(hash, CONCAT(':', op_id)) AS cursor`;
            break;
        case 'operations':
            selectStatement = `SELECT id, hash, op_id as batch_position, source, destination, delegate, kind, timestamp, level, block, counter, gas_limit, storage_limit, public_key, amount, parameters, contract_address, fee as gas, CONCAT(hash, CONCAT(':', op_id)) AS cursor`;
            break;
        case 'reveal':
            selectStatement = `SELECT id, hash, op_id as batch_position, source, destination, delegate, kind, timestamp, level, block, counter, gas_limit, storage_limit, public_key, amount, parameters, contract_address, fee as gas, CONCAT(hash, CONCAT(':', op_id)) AS cursor`;
            break;
        case 'origination':
            selectStatement = `SELECT id, hash, op_id as batch_position, source, destination, delegate, kind, timestamp, level, block, counter, gas_limit, storage_limit, public_key, amount, parameters, contract_address, fee as gas, CONCAT(hash, CONCAT(':', op_id)) AS cursor`;
            break;
        case 'endorsement':
            selectStatement = `SELECT id, hash, op_id as batch_position, source, destination, delegate, kind, timestamp, level, block, counter, gas_limit, storage_limit, public_key, amount, parameters, slots, contract_address, fee as gas, CONCAT(hash, CONCAT(':', op_id)) AS cursor`;
            break;
        default:
            throw new UserInputError('Operation Error: Cursor query operation not recognized.');
    }

    if (cursorRecord.length === 0) {
        throw new UserInputError('Unknown Transaction: No results found for the provided cursor.');
    }

    let orderByClause = `ORDER BY id DESC, hash ASC, op_id ASC`;

    let cursorQuery; let orderByWhereClause; let whereClause; let dateRangeWhereClause; let direction;

    let orderByDirection = orderBy.direction;
    direction = orderByDirection == 'DESC' ? '<' : '>';

    if (relayPageData.direction == 'first' && relayPageData.order == 'before' && orderBy.direction == 'DESC') {
        // OrderByDirection = OrderByDirection.ASC
        direction = '>';
    } else if (relayPageData.direction == 'first' && relayPageData.order == 'before' && orderBy.direction == 'ASC') {
        direction = '<';
    } else if (relayPageData.direction == 'last' && relayPageData.order == 'after' && orderBy.direction == 'DESC') {
        orderByDirection = OrderByDirection.ASC;
    } else if (relayPageData.direction == 'last' && relayPageData.order == 'after' && orderBy.direction == 'ASC') {
        orderByDirection = OrderByDirection.DESC;
    }

    orderByClause = `ORDER BY ${orderBy.field} ${orderByDirection}, hash ASC, op_id ASC`;

    orderByWhereClause = `WHERE (((${orderBy.field} ${direction}= '${cursorRecord[0][orderBy.field]}') AND (hash >= '${cursorRecord[0].hash}' AND op_id > '${cursorRecord[0].op_id}')) OR ((${orderBy.field} ${direction}= '${cursorRecord[0][orderBy.field]}') AND (hash != '${cursorRecord[0].hash}')))`;
    // }

    if (dateRange) {
        dateRangeWhereClause = getDateRangeClause(dateRange);
    }

    if (dateRangeWhereClause && orderByWhereClause) {
        whereClause = `${dateRangeWhereClause} AND${orderByWhereClause.slice(5)}`;
    } else {
        whereClause = dateRangeWhereClause || orderByWhereClause;
    }

    if (whereClause == undefined) {
        whereClause = '';
    }

    cursorQuery = `${selectStatement} FROM get_${operation}('${pkh}') ${whereClause} ${orderByClause} LIMIT ${takeLimit}`;

    return cursorQuery;
};

/*
 * Cursor Query
 * checking for prev/next page info requires for the relayPageData to be changed from the original relayPageData, relayAfter is a boolean that tells us whether the original relayPageData.order was 'after' or not.
 */
export const getRelayPageInfo = async function (operation: string, cursor: string, relayPageData: any, pkh: string, takeLimit: number, uuid: string, relayAfter: boolean, orderBy?: OrderBy, dateRange?: DateRange): Promise<string> {
    const cursorSplit = cursor.split(':');
    const cursorRecord = await getCursorRecord(operation, pkh, cursorSplit[0], cursorSplit[1], uuid);
    let selectStatement;

    selectStatement = `SELECT id, hash`;
    if (cursorRecord.length === 0) {
        throw new UserInputError('Unknown Transaction: No results found for the provided cursor.');
    }

    let orderByClause = `ORDER BY id DESC, hash ASC, op_id ASC`;

    let cursorQuery; let orderByWhereClause; let whereClause; let dateRangeWhereClause; let direction;

    if (orderBy && orderBy.field != undefined) {
        let orderByDirection = orderBy.direction || 'DESC';
        direction = orderByDirection == 'DESC' ? '<' : '>';

        if (relayPageData.direction == 'first' && relayPageData.order == 'before' && orderBy.direction == 'DESC') {
            orderByDirection = OrderByDirection.ASC;
            direction = '>';
        } else if (relayPageData.direction == 'first' && relayPageData.order == 'before' && orderBy.direction == 'ASC') {
            direction = '<';
        } else if (relayPageData.direction == 'last' && relayPageData.order == 'before' && orderBy.direction == 'ASC') {
            direction = '<';
        } else if (relayPageData.direction == undefined && relayPageData.order == 'before' && orderBy.direction == 'ASC') {
            direction = '<';
        } else if (relayPageData.direction == undefined && relayPageData.order == 'before' && orderBy.direction == 'DESC') {
            direction = '>';
        } else if (relayPageData.direction == 'last' && relayPageData.order == 'before' && orderBy.direction == 'DESC' && relayAfter) {
            direction = '>';
        } else if (relayPageData.direction == 'last' && relayPageData.order == 'before' && orderBy.direction == 'DESC' && !relayAfter) {
            direction = '>';
        }

        orderByClause = `ORDER BY ${orderBy.field} ${orderByDirection}, hash ASC, op_id ASC`;

        orderByWhereClause = `WHERE (((${orderBy.field} ${direction}= '${cursorRecord[0][orderBy.field]}') AND (hash >= '${cursorRecord[0].hash}' AND op_id > '${cursorRecord[0].op_id}')) OR ((${orderBy.field} ${direction}= '${cursorRecord[0][orderBy.field]}') AND (hash != '${cursorRecord[0].hash}')))`;
        // }
    }

    if (dateRange) {
        dateRangeWhereClause = getDateRangeClause(dateRange);
    }

    if (dateRangeWhereClause && orderByWhereClause) {
        whereClause = `${dateRangeWhereClause} AND${orderByWhereClause.slice(5)}`;
    } else {
        whereClause = dateRangeWhereClause || orderByWhereClause;
    }

    if (whereClause == undefined) {
        whereClause = '';
    }

    cursorQuery = `${selectStatement} FROM get_${operation}('${pkh}') ${whereClause} ${orderByClause} LIMIT ${takeLimit}`;

    return cursorQuery;
};

// Relay Connections Helper
export const connectionBuilder = async function (operation: string, results: any[], relayPageData: any, pkh: string, uuid: string, orderBy?: OrderBy, dateRange?: DateRange): Promise<TransactionConnection> {
    let operationConnection;
    const operationEdgeArray: any[] = [];
    let operationEdgeType: any;

    switch (operation) {
        case 'transaction':
            operationConnection = Object.create(TransactionConnection);
            operationEdgeType = TransactionEdge;
            break;
        case 'delegation':
            operationConnection = Object.create(DelegationConnection);
            operationEdgeType = DelegationEdge;
            break;
        case 'operations':
            operationConnection = Object.create(OperationsConnection);
            operationEdgeType = OperationsEdge;
            break;
        case 'origination':
            operationConnection = Object.create(OriginationConnection);
            operationEdgeType = OriginationEdge;
            break;
        case 'reveal':
            operationConnection = Object.create(RevealConnection);
            operationEdgeType = RevealEdge;
            break;
        case 'endorsement':
            operationConnection = Object.create(EndorsementConnection);
            operationEdgeType = EndorsementEdge;
            break;
        default:
            throw new UserInputError('Operation Error: Operation not recognized.');
    }

    results.forEach((operation) => {
        const operationEdge = Object.create(operationEdgeType);
        operationEdge.cursor = `${operation.hash}:${operation.batch_position}`,
        operationEdge.node = operation;
        operationEdgeArray.push(operationEdge);
    });

    const pageInfo = Object.create(PageInfo);

    pageInfo.start_cursor = operationEdgeArray[0].cursor;
    pageInfo.end_cursor = operationEdgeArray[operationEdgeArray.length - 1].cursor;

    operationConnection.edges = operationEdgeArray;
    operationConnection.page_info = pageInfo;

    if (orderBy == undefined) {
        orderBy = Object.create(OrderBy);
        if (orderBy != undefined) {
            orderBy.field = OrderByField.ID;
            orderBy.direction = OrderByDirection.DESC;
        }
    }

    const nextRelayPageData = Object.create(RelayPageData);
    nextRelayPageData.direction = relayPageData.direction;
    nextRelayPageData.order = 'after';

    const hasNextPage = await getRelayPageInfo(operation, pageInfo.end_cursor, nextRelayPageData, pkh, 1, uuid, relayPageData.order == 'after', orderBy, dateRange);

    const prevRelayPageData = Object.create(RelayPageData);
    prevRelayPageData.direction = relayPageData.direction;
    prevRelayPageData.order = 'before';

    const hasPrevPage = await getRelayPageInfo(operation, pageInfo.start_cursor, prevRelayPageData, pkh, 1, uuid, relayPageData.order == 'after', orderBy, dateRange);

    const nextPageBool = await prisma.$queryRaw(hasNextPage);
    if (nextPageBool.length != 0) {
        operationConnection.page_info.has_next_page = true;
    } else {
        operationConnection.page_info.has_next_page = false;
    }

    const prevPageBool = await prisma.$queryRaw(hasPrevPage);
    if (prevPageBool.length != 0) {
        operationConnection.page_info.has_previous_page = true;
    } else {
        operationConnection.page_info.has_previous_page = false;
    }

    return operationConnection;
};
