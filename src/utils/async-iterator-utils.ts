export async function *filterIterator<T>(iterator: AsyncIterator<T>, predicate: (value: T) => boolean): AsyncIterator<T> {
    for await (const value of asIterable(iterator)) {
        if (predicate(value)) {
            yield value;
        }
    }
}

export function asIterable<T>(iterator: AsyncIterator<T>): AsyncIterable<T> {
    return {
        [Symbol.asyncIterator](): AsyncIterator<T> {
            return iterator;
        },
    };
}

export async function iteratorToArray<T>(iterator: AsyncIterator<T>): Promise<T[]> {
    const result = [];
    for await (const item of asIterable(iterator)) {
        result.push(item);
    }
    return result;
}

export async function *arrayToIterator<T>(array: readonly T[]): AsyncIterator<T> {
    await Promise.resolve();
    yield* array;
}
