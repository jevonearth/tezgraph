export interface BackgroundWorker {
    start(): void;
    stop(): void;
}
