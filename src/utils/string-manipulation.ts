export function removeSuffix(str: string, suffixToRemove: string): string {
    if (!str.endsWith(suffixToRemove)) {
        throw new Error(`Given string "${str}" doesn't end with suffix "${suffixToRemove}" which should be removed.`);
    }
    return str.slice(undefined, str.length - suffixToRemove.length);
}
