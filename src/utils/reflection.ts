export type Constructor<T> = new(...args: any[]) => T; /* eslint-disable-line @typescript-eslint/no-explicit-any */

export type DefaultConstructor<T> = new() => T;

export interface NonNullValue {} /* eslint-disable-line @typescript-eslint/no-empty-interface */

export function nameof<T>(name: keyof T): string {
    return name.toString();
}

export type KeyOfType<TObject, TProperty> = {
    [P in keyof TObject]: TObject[P] extends TProperty ? P : never
} [keyof TObject];

export function isNullish<TValue>(value: TValue | null | undefined): value is null | undefined {
    return value === undefined || value === null;
}

export function isNotNullish<TValue>(value: TValue | null | undefined): value is TValue {
    return !isNullish(value);
}
