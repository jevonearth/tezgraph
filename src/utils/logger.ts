import winston from 'winston';

import { Constructor } from './reflection';

export enum LogLevel {
    Error = 'error',
    Warn = 'warn',
    Info = 'info',
    Debug = 'debug',
}

/* eslint-disable @typescript-eslint/no-explicit-any */
export class Logger {
    constructor(
        private readonly category: string,
        private readonly rootLogger: winston.Logger,
    ) {}

    isEnabled(level: LogLevel): boolean {
        return this.rootLogger.isLevelEnabled(level);
    }

    log(level: LogLevel, message: string, ...meta: readonly any[]): void {
        this.rootLogger.log(level, `[${this.category}] ${message}`, ...meta);
    }

    get isErrorEnabled(): boolean {
        return this.isEnabled(LogLevel.Error);
    }

    error(message: string, ...meta: readonly any[]): void {
        this.log(LogLevel.Error, message, ...meta);
    }

    get isWarnEnabled(): boolean {
        return this.isEnabled(LogLevel.Warn);
    }

    warn(message: string, ...meta: readonly any[]): void {
        this.log(LogLevel.Warn, message, ...meta);
    }

    get isInfoEnabled(): boolean {
        return this.isEnabled(LogLevel.Info);
    }

    info(message: string, ...meta: readonly any[]): void {
        this.log(LogLevel.Info, message, ...meta);
    }

    get isDebugEnabled(): boolean {
        return this.isEnabled(LogLevel.Debug);
    }

    debug(message: string, ...meta: readonly any[]): void {
        this.log(LogLevel.Debug, message, ...meta);
    }
}

export class LoggerFactory {
    constructor(private readonly rootLogger: winston.Logger) {}

    get(categoryOrType: string | Constructor<any>): Logger {
        const category = typeof categoryOrType === 'string' ? categoryOrType : categoryOrType.name;
        return new Logger(category, this.rootLogger);
    }
}
