import { UserInputError } from 'apollo-server-express';
import { BigNumber } from 'bignumber.js';
import { GraphQLScalarType, GraphQLScalarTypeConfig } from 'graphql';
import { GraphQLDateTime } from 'graphql-iso-date';
import GraphQLJSONObject from 'graphql-type-json';

/* eslint-disable */
const bs58check: (s: string) => void = require('bs58check').decode;
/* eslint-enable */

export const scalars = {
    Address: createBase58HashScalar({
        name: 'Address',
        description: 'Tezos address. Represented as public key hash (Base58Check-encoded) prefixed with tz1, tz2, tz3 or KT1',
        supportedPrefixes: [
            { prefix: 'tz1', length: 36 },
            { prefix: 'KT1', length: 36 },
            { prefix: 'tz2', length: 36 },
            { prefix: 'tz3', length: 36 },
        ],
    }),
    BigNumber: createScalar({
        name: 'BigNumber',
        description: 'Arbitrary precision number represented as string in JSON.',
        parseValue: parseBigNumber,
    }),
    Mutez: createScalar({
        name: 'Mutez',
        description: 'Micro tez. Positive bignumber. 1 tez = 1,000,000 micro tez.',
        parseValue: strValue => {
            const value = parseBigNumber(strValue);
            if (value.isNegative()) {
                throw new Error('Mutez amount must be positive but it is a negative number.');
            }
            return value;
        },
    }),
    OperationHash: createBase58HashScalar({
        name: 'OperationHash',
        description: 'Operation identifier (Base58Check-encoded) prefixed with o.Operation identifier (Base58Check-encoded) prefixed with o.',
        supportedPrefixes: [{ prefix: 'o', length: 51 }],
    }),
    BlockHash: createBase58HashScalar({
        name: 'BlockHash',
        description: 'Block identifier (Base58Check-encoded) prefixed with B.',
        supportedPrefixes: [{ prefix: 'B', length: 51 }],
    }),
    ProtocolHash: createBase58HashScalar({
        name: 'ProtocolHash',
        description: 'Protocol identifier (Base58Check-encoded) prefixed with P.',
        supportedPrefixes: [{ prefix: 'P', length: 51 }],
    }),
    ContextHash: createBase58HashScalar({
        name: 'ContextHash',
        description: 'ContextHash identifier (Base58Check-encoded) prefixed with Co.',
        supportedPrefixes: [{ prefix: 'Co', length: 52 }],
    }),
    OperationsHash: createBase58HashScalar({
        name: 'OperationsHash',
        description: 'OperationsHash identifier (Base58Check-encoded) prefixed with LLo.',
        supportedPrefixes: [{ prefix: 'LLo', length: 53 }],
    }),
    ChainId: createBase58HashScalar({
        name: 'ChainId',
        description: 'Chain identifier (Base58Check-encoded) prefixed with Net.',
        supportedPrefixes: [{ prefix: 'Net', length: 15 }],
    }),
    Signature: createBase58HashScalar({
        name: 'Signature',
        description: 'Generic signature (Base58Check-encoded) prefixed with sig.',
        supportedPrefixes: [{ prefix: 'sig', length: 96 }],
    }),
    PublicKey: createBase58HashScalar({
        name: 'PublicKey',
        description: 'Public key (Base58Check-encoded) prefixed with edpk, sppk or p2pk.',
        supportedPrefixes: [
            { prefix: 'edpk', length: 54 },
            { prefix: 'p2pk', length: 55 },
            { prefix: 'sppk', length: 55 },
        ],
    }),
    NonceHash: createBase58HashScalar({
        name: 'NonceHash',
        description: 'Nonce hash (Base58Check-encoded).',
    }),
    DateTime: GraphQLDateTime,
    JSON: GraphQLJSONObject,
    MichelsonJson: new GraphQLScalarType({
        name: 'MichelsonJson',
        description: 'Raw Michelson expression represented as JSON.',
    }),
};

interface Base58HashScalarConfig {
    readonly name: string;
    readonly description: string;
    supportedPrefixes?: readonly { readonly prefix: string; readonly length: number }[];
}

function createBase58HashScalar(config: Base58HashScalarConfig): GraphQLScalarType {
    const prefixes = config.supportedPrefixes;
    return createScalar({
        name: config.name,
        description: config.description,
        parseValue: value => {
            if (prefixes && !prefixes.some(p => value.startsWith(p.prefix) && value.length === p.length)) {
                throw new Error(`Invalid hash prefix. It must be one of: ${prefixes.map(p => `"${p.prefix}" with length ${p.length}`).join()}.`);
            }
            try {
                bs58check(value);
            } catch {
                throw new Error('Given string is not a valid Base58Check encoded hash.');
            }
            return value;
        },
    });
}

interface ScalarConfig<T> {
    name: string;
    description: string;
    parseValue(value: string): T;
}

function createScalar<T extends { toString(): string }>(config: ScalarConfig<T>): GraphQLScalarType {
    const scalarConfig: GraphQLScalarTypeConfig<T, string> = {
        name: config.name,
        description: config.name,
        parseValue: value => parse(typeof value === 'string' ? value : null, typeof value),
        serialize: (value: T) => value.toString(),
        parseLiteral: node => parse(node.kind === 'StringValue' ? node.value : null, node.kind),
    };
    return new GraphQLScalarType(scalarConfig);

    function parse(value: string | null, valueType: string): T {
        try {
            if (value === null) {
                throw new Error(`Expected a string but there is a ${valueType}.`);
            }
            return config.parseValue(value);
        } catch (error) {
            throw new UserInputError(error, {
                actualValue: value,
                scalarType: config.name,
            });
        }
    }
}

function parseBigNumber(strValue: string): BigNumber {
    const value = new BigNumber(strValue);
    if (value.isNaN()) {
        throw new Error('Given string is not a valid number.');
    }
    return value;
}
