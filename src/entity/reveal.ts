import { Int, ObjectType } from 'type-graphql';
import { scalars } from './scalars';
import { GraphField } from './graph-field';
import { PageInfo } from './pageInfo';

@ObjectType()
export class Reveal {
    @GraphField(scalars.OperationHash, { description: 'The hash of the operation.' })
    hash!: string;

    @GraphField(Int, { description: 'The position of the operation in the operation batch.' })
    batch_position?: number;

    @GraphField(scalars.Address, { description: 'The unique identifier of an implicit account for the source of the operation. An Implicit account starts with the letters tz followed by 1, 2 or 3 and the hash of the public key.' })
    source!: string;

    @GraphField(String, { description: 'The kind of operation.' })
    kind!: string;

    @GraphField(scalars.DateTime, { description: 'The timestamp for when the block pushed to the block chain.' })
    timestamp!: Date;

    @GraphField(Int, { description: 'The level of the block in the chain.' })
    level!: number;

    @GraphField(scalars.BlockHash, { description: 'The hash of the block.' })
    block!: string;

    @GraphField(Int, { description: 'A measure of the number of elementary operations performed during the execution of a smart contract.' })
    gas!: number;

    @GraphField(String)
    counter!: string;

    @GraphField(String)
    gas_limit!: string;

    @GraphField(scalars.PublicKey)
    public_key!: string;

    @GraphField(Int)
    id!: number;

    /*
     * @GraphField(scalars.Address, {nullable: true})
     * Destination?: string;
     */

    /*
     * @GraphField({nullable: true})
     * Storage_limit?: string;
     */

    /*
     * @GraphField(scalars.Mutez, {nullable: true})
     * Amount?: number;
     */

    /*
     * @GraphField({nullable: true})
     * Parameters?: string;
     */

    /*
     * @GraphField({nullable: true})
     * Entrypoint?: string;
     */

    /*
     * @GraphField({nullable: true})
     * Contract_address?: string;
     */

    /*
     * @GraphField(scalars.Address)
     * Delegate!: string;
     */
}

@ObjectType()
export class RevealEdge {
    @GraphField(String, { description: 'A string build from the operation hash and the batch position. Used for pagination.' })
    cursor!: string;

    @GraphField(Reveal, { description: 'An object containing operation data.' })
    node?: Reveal;
}

@ObjectType()
export class RevealConnection {
    @GraphField(PageInfo, { description: 'An object containing the details of the current page.' })
    page_info!: PageInfo;

    @GraphField([RevealEdge], { description: 'A list containing the operation data.' })
    edges?: RevealEdge[];
}
