import { Field, ObjectType } from 'type-graphql';

@ObjectType()
export class PageInfo {
    @Field({ description: 'The hash of the operation.' })
    has_next_page!: boolean;

    @Field({ description: 'The position of the operation in the operation batch.' })
    has_previous_page!: boolean;

    @Field({ description: 'The hash of the operation.' })
    start_cursor!: string;

    @Field({ description: 'The position of the operation in the operation batch.' })
    end_cursor!: string;
}
