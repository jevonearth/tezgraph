import { Int, ObjectType } from 'type-graphql';
import { scalars } from './scalars';
import { GraphField } from './graph-field';
import { PageInfo } from './pageInfo';

@ObjectType()
export class Operations {
    @GraphField(scalars.OperationHash, { description: 'The hash of the operation.' })
    hash!: string;

    @GraphField(Int, { description: 'The position of the operation in the operation batch.' })
    batch_position!: number;

    @GraphField(String, { description: 'The kind of operation.' })
    kind!: string;

    @GraphField(scalars.DateTime, { description: 'The timestamp for when the block pushed to the block chain.' })
    timestamp!: Date;

    @GraphField(Int, { description: 'The level of the block in the chain.' })
    level!: number;

    @GraphField(scalars.BlockHash, { description: 'The hash of the block.' })
    block!: string;

    @GraphField(Int)
    id!: number;

    @GraphField(scalars.Address, { nullable: true, description: 'The unique identifier of an implicit account for the source of the operation. An Implicit account starts with the letters tz followed by 1, 2 or 3 and the hash of the public key.' })
    source?: string;

    @GraphField(Int, { nullable: true, description: 'A measure of the number of elementary operations performed during the execution of a smart contract.' })
    gas?: number;

    @GraphField(String, { nullable: true })
    counter?: string;

    @GraphField(String, { nullable: true })
    gas_limit?: string;

    @GraphField(String, { nullable: true })
    storage_limit?: string;

    @GraphField(scalars.PublicKey, { nullable: true })
    public_key?: string;

    @GraphField(scalars.Mutez, { nullable: true, description: 'The amount of tz transfered.' })
    amount?: number;

    @GraphField(String, { nullable: true })
    parameters?: string;

    @GraphField(String, { nullable: true })
    entrypoint?: string;

    @GraphField(String, { nullable: true, description: 'The address of an originated account or an implicit account.' })
    contract_address?: string;

    @GraphField(scalars.Address, { nullable: true })
    destination?: string;

    @GraphField(scalars.Address, { description: 'An Implicit account to which an account has delegated their baking and endorsement rights.' })
    delegate!: string;
}

@ObjectType()
export class OperationsEdge {
    @GraphField(String, { description: 'A string build from the operation hash and the batch position. Used for pagination.' })
    cursor!: string;

    @GraphField(Operations, { description: 'An object containing operation data.' })
    node?: Operations;
}

@ObjectType()
export class OperationsConnection {
    @GraphField(PageInfo, { description: 'An object containing the details of the current page.' })
    page_info!: PageInfo;

    @GraphField([OperationsEdge], { description: 'A list containing the operation data.' })
    edges?: OperationsEdge[];
}
