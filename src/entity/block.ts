import { Int, ObjectType } from 'type-graphql';

import { scalars } from './scalars';
import { GraphField } from './graph-field';

@ObjectType()
export class Block {
    @GraphField(scalars.BlockHash)
    hash!: string;

    @GraphField(Int)
    level!: number;

    @GraphField(Int)
    proto!: number;

    @GraphField(scalars.BlockHash)
    predecessor!: string;

    @GraphField(scalars.DateTime)
    timestamp!: Date;

    @GraphField(Int)
    validation_passes!: number;

    @GraphField(String)
    merkle_root!: string;

    @GraphField(String)
    fitness!: string;

    @GraphField(scalars.ContextHash)
    context_hash!: string;
}
