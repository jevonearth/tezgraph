import { Int, ObjectType } from 'type-graphql';

import { GraphField } from '../graph-field';
import { SimpleOperationMetadata } from './operation-metadata';
import { OperationKind, OperationNotification } from './operation-notification';

@ObjectType({
    implements: OperationNotification,
    description: 'Corresponds to RPC -> `$operation.alpha.operation_contents_and_result` -> `[@kind = seed_nonce_revelation]`.',
})
export class SeedNonceRevelationNotification extends OperationNotification {
    @GraphField(OperationKind, {
        description: `It always equals to \`${OperationKind.seed_nonce_revelation}\`.`,
    })
    readonly kind = OperationKind.seed_nonce_revelation;

    @GraphField(Int)
    readonly level!: number;

    @GraphField(String)
    readonly nonce!: string;

    @GraphField(SimpleOperationMetadata)
    readonly metadata!: SimpleOperationMetadata;
}
