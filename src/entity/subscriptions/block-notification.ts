import { Int, ObjectType } from 'type-graphql';

import { GraphField } from '../graph-field';
import { scalars } from '../scalars';
import { OperationNotification } from './operation-notification';

@ObjectType()
export class BlockHeader {
    @GraphField(Int)
    readonly level!: number;

    @GraphField(Int)
    readonly proto!: number;

    @GraphField(scalars.BlockHash)
    readonly predecessor!: string;

    @GraphField(scalars.DateTime)
    readonly timestamp!: string | Date;

    @GraphField(scalars.Signature)
    readonly signature!: string;
}

@ObjectType()
export class BlockNotification {
    @GraphField(scalars.BlockHash)
    readonly hash!: string;

    @GraphField(BlockHeader)
    readonly header!: BlockHeader;

    @GraphField(scalars.ProtocolHash)
    readonly protocol!: string;

    @GraphField(scalars.ChainId)
    readonly chain_id!: string;

    // Intentionally not exposed yet.
    readonly operations!: readonly OperationNotification[];
}
