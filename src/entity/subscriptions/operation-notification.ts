import { Field, InterfaceType, ObjectType, registerEnumType } from 'type-graphql';

import { GraphField } from '../graph-field';
import { scalars } from '../scalars';
import { BlockNotification } from './block-notification';

@ObjectType({ description: 'Corresponds to RPC -> `$operation` but its `contents` is the parent object.' })
export class OperationNotificationInfo {
    @GraphField(scalars.ProtocolHash)
    readonly protocol!: string;

    @GraphField(scalars.ChainId)
    readonly chain_id!: string;

    @GraphField(scalars.OperationHash)
    readonly hash!: string;

    @GraphField(scalars.BlockHash)
    readonly branch!: string;

    @GraphField(scalars.Signature, { nullable: true })
    readonly signature!: string | undefined;
}

export enum OperationKind {
    origination = 'origination',
    delegation = 'delegation',
    reveal = 'reveal',
    transaction = 'transaction',
    activate_account = 'activate_account',
    endorsement = 'endorsement',
    seed_nonce_revelation = 'seed_nonce_revelation',
    double_endorsement_evidence = 'double_endorsement_evidence',
    double_baking_evidence = 'double_baking_evidence',
    proposals = 'proposals',
    ballot = 'ballot',
}

registerEnumType(OperationKind, {
    name: 'OperationKind',
    description: 'The kind of an operation.',
});

@InterfaceType({ description: 'Corresponds to RPC -> `$operation.alpha.operation_contents_and_result`.' })
export abstract class OperationNotification {
    @Field()
    readonly info!: OperationNotificationInfo;

    @GraphField(OperationKind, {
        description: 'The kind of the operation. It is constant for a conrete *Operation* type'
            + ' e.g. it is always `transaction` for a *Transaction Operation*',
    })
    readonly abstract kind: OperationKind;

    @GraphField(BlockNotification)
    readonly block!: BlockNotification;
}
