import { Int, ObjectType, registerEnumType } from 'type-graphql';

import { GraphField } from '../graph-field';
import { scalars } from '../scalars';
import { SimpleOperationMetadata } from './operation-metadata';
import { OperationKind, OperationNotification } from './operation-notification';

export enum InlinedEndorsementKind {
    endorsement = 'endorsement',
}

registerEnumType(InlinedEndorsementKind, {
    name: 'InlinedEndorsementKindEnum',
    description: 'Inlined endorsement kind.',
});

@ObjectType({ description: 'Corresponds to RPC -> `$inlined.endorsement.contents`.' })
export class InlinedEndorsementContents {
    @GraphField(InlinedEndorsementKind)
    readonly kind!: InlinedEndorsementKind;

    @GraphField(Int)
    readonly level!: number;
}

@ObjectType({ description: 'Corresponds to RPC -> `$inlined.endorsement`.' })
export class InlinedEndorsement {
    @GraphField(scalars.BlockHash)
    readonly branch!: string;

    @GraphField(InlinedEndorsementContents)
    readonly operations!: InlinedEndorsementContents;

    @GraphField(scalars.Signature, { nullable: true })
    readonly signature: string | undefined;
}

@ObjectType({
    implements: OperationNotification,
    description: 'Corresponds to RPC -> `$operation.alpha.operation_contents_and_result` -> `[@kind = double_endorsement_evidence]`.',
})
export class DoubleEndorsementEvidenceNotification extends OperationNotification {
    @GraphField(OperationKind, {
        description: `It always equals to \`${OperationKind.double_endorsement_evidence}\`.`,
    })
    readonly kind = OperationKind.double_endorsement_evidence;

    @GraphField(InlinedEndorsement)
    readonly op1!: InlinedEndorsement;

    @GraphField(InlinedEndorsement)
    readonly op2!: InlinedEndorsement;

    @GraphField(SimpleOperationMetadata)
    readonly metadata!: SimpleOperationMetadata;
}
