import { Int, ObjectType } from 'type-graphql';

import { GraphField } from '../graph-field';
import { scalars } from '../scalars';
import { SimpleOperationMetadata } from './operation-metadata';
import { OperationKind, OperationNotification } from './operation-notification';

@ObjectType({ description: 'Corresponds to RPC -> `$block_header.alpha.full_header`.' })
export class DoubleBakingEvidenceBlockHeader {
    @GraphField(Int)
    readonly level!: number;

    @GraphField(Int)
    readonly proto!: number;

    @GraphField(scalars.BlockHash)
    readonly predecessor!: string;

    @GraphField(scalars.DateTime)
    readonly timestamp!: Date | string;

    @GraphField(Int)
    readonly validation_pass!: number;

    @GraphField(scalars.OperationHash)
    readonly operations_hash: string | undefined;

    @GraphField([String])
    readonly fitness!: string[];

    @GraphField(scalars.ContextHash)
    readonly context!: string;

    @GraphField(Int)
    readonly priority!: number;

    @GraphField(String)
    readonly proof_of_work_nonce: string | undefined;

    @GraphField(scalars.NonceHash, { nullable: true })
    readonly seed_nonce_hash?: string | undefined;

    @GraphField(scalars.Signature)
    readonly signature!: string;
}

@ObjectType({
    implements: OperationNotification,
    description: 'Corresponds to RPC -> `$operation.alpha.operation_contents_and_result` -> `[@kind = double_baking_evidence]`.',
})
export class DoubleBakingEvidenceNotification extends OperationNotification {
    @GraphField(OperationKind, {
        description: `It always equals to \`${OperationKind.double_baking_evidence}\`.`,
    })
    readonly kind = OperationKind.double_baking_evidence;

    @GraphField(DoubleBakingEvidenceBlockHeader)
    readonly bh1!: DoubleBakingEvidenceBlockHeader;

    @GraphField(DoubleBakingEvidenceBlockHeader)
    readonly bh2!: DoubleBakingEvidenceBlockHeader;

    @GraphField(SimpleOperationMetadata)
    readonly metadata!: SimpleOperationMetadata;
}
