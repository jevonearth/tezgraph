import { MichelsonV1Expression } from '@taquito/rpc';
import { ObjectType } from 'type-graphql';

import { GraphField } from '../graph-field';
import { scalars } from '../scalars';

@ObjectType({ description: 'The code and the storage a contract. Corresponds to RPC -> `$scripted.contracts`.' })
export class ScriptedContracts {
    @GraphField([scalars.MichelsonJson], { description: 'The code of the script.' })
    readonly code!: readonly MichelsonV1Expression[];

    @GraphField(scalars.MichelsonJson, { description: 'The current storage value in Michelson format.' })
    readonly storage!: MichelsonV1Expression;
}
