import { ClassType, InputType } from 'type-graphql';
import { ReturnTypeFuncValue } from 'type-graphql/dist/decorators/types';

import { isNullish, NonNullValue } from '../../../utils/reflection';
import { GraphField } from '../../graph-field';
import { Filter } from './filter';

export function ArrayFilter<TItemScalar extends NonNullValue>(itemScalarType: ReturnTypeFuncValue): ClassType<Filter<readonly TItemScalar[]>> {
    @InputType({ isAbstract: true })
    class ArrayFilterClass implements Filter<TItemScalar[]> {
        @GraphField(itemScalarType, { nullable: true })
        includes: TItemScalar | undefined;

        @GraphField(itemScalarType, { nullable: true })
        notIncludes: TItemScalar | undefined;

        passes(value: TItemScalar[]): boolean {
            // Check undefined instead of ! operator b/c value can be empty string or zero.
            return (this.includes === undefined || (!isNullish(value) && value.includes(this.includes)))
                && (this.notIncludes === undefined || isNullish(value) || !value.includes(this.notIncludes));
        }
    }
    return ArrayFilterClass;
}
