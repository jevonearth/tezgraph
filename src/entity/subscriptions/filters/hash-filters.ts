import { InputType } from 'type-graphql';

import { scalars } from '../../scalars';
import { ArrayFilter } from './array-filter';
import { EqualityFilter } from './equality-filter';
import { Nullable } from './nullable-filter';

@InputType()
export class ProtocolHashFilter extends EqualityFilter<string>(scalars.ProtocolHash) {}

@InputType()
export class NullableProtocolHashArrayFilter extends Nullable(ArrayFilter<string>(scalars.ProtocolHash)) {}

@InputType()
export class BlockHashFilter extends EqualityFilter<string>(scalars.BlockHash) {}

@InputType()
export class OperationHashFilter extends EqualityFilter<string>(scalars.OperationHash) {}
