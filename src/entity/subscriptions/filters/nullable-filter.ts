import { ClassType, InputType } from 'type-graphql';

import { isNullish } from '../../../utils/reflection';
import { GraphField } from '../../graph-field';
import { Filter } from './filter';

export function Nullable<TValue>(baseFilterType: ClassType<Filter<TValue>>): ClassType<Filter<TValue | undefined | null>> {
    @InputType({ isAbstract: true })
    class NullableFilterClass extends baseFilterType {
        @GraphField(Boolean, { nullable: true })
        isNull: boolean | undefined;

        passes(value: TValue): boolean {
            return super.passes(value)
                && (this.isNull === undefined || this.isNull === isNullish(value));
        }
    }
    return NullableFilterClass;
}
