import { InputType } from 'type-graphql';

import { scalars } from '../../scalars';
import { ArrayFilter } from './array-filter';
import { EqualityFilter } from './equality-filter';
import { Nullable } from './nullable-filter';

@InputType()
export class AddressFilter extends EqualityFilter<string>(scalars.Address) {}

@InputType()
export class NullableAddressFilter extends Nullable(AddressFilter) {}

@InputType()
export class AddressArrayFilter extends ArrayFilter<string>(scalars.Address) {}

@InputType()
export class NullableAddressArrayFilter extends Nullable(AddressArrayFilter) {}
