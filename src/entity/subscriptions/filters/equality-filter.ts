import { ClassType, InputType } from 'type-graphql';
import { ReturnTypeFuncValue } from 'type-graphql/dist/decorators/types';

import { GraphField } from '../../graph-field';
import { Filter } from './filter';

export function EqualityFilter<TScalar>(scalarType: ReturnTypeFuncValue): ClassType<Filter<TScalar>> {
    @InputType({ isAbstract: true })
    class EqualityFilterClass implements Filter<TScalar> {
        @GraphField(scalarType, { nullable: true })
        equalTo: TScalar | undefined;

        @GraphField(scalarType, { nullable: true })
        notEqualTo: TScalar | undefined;

        @GraphField([scalarType], { nullable: true })
        in: TScalar[] | undefined;

        @GraphField([String], { nullable: true })
        notIn: TScalar[] | undefined;

        passes(value: TScalar): boolean {
            // Check undefined instead of ! operator b/c value can be empty string or zero.
            return (this.equalTo === undefined || this.equalTo === value)
                && (this.notEqualTo === undefined || this.notEqualTo !== value)
                && (!this.in || this.in.includes(value))
                && (!this.notIn || !this.notIn.includes(value));
        }
    }
    return EqualityFilterClass;
}
