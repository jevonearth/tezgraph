import { InputType } from 'type-graphql';

import { OperationResultStatus } from '../operation-result';
import { EqualityFilter } from './equality-filter';

@InputType()
export class OperationResultStatusFilter extends EqualityFilter<OperationResultStatus>(OperationResultStatus) {}
