import { BigNumber } from 'bignumber.js';
import { ObjectType } from 'type-graphql';

import { GraphField } from '../graph-field';
import { scalars } from '../scalars';
import { AbstractOperationMetadataWithInternalResults } from './operation-metadata';
import { OperationKind, OperationNotification } from './operation-notification';
import { AbstractOperationResult } from './operation-result';

@ObjectType({ description: 'Corresponds to RPC -> `$operation.alpha.operation_result.reveal`.' })
export class RevealResult extends AbstractOperationResult {}

@ObjectType({ description: 'Corresponds to RPC -> `$operation.alpha.operation_contents_and_result` -> `[@kind = reveal]` -> `metadata`.' })
export class RevealMetadata extends AbstractOperationMetadataWithInternalResults {
    @GraphField(RevealResult)
    readonly operation_result!: RevealResult;
}

@ObjectType({
    implements: OperationNotification,
    description: 'Corresponds to RPC -> `$operation.alpha.operation_contents_and_result` -> `[@kind = reveal]`.',
})
export class RevealNotification extends OperationNotification {
    @GraphField(OperationKind, {
        description: `It always equals to \`${OperationKind.reveal}\`.`,
    })
    readonly kind = OperationKind.reveal;

    @GraphField(scalars.Address)
    readonly source!: string;

    @GraphField(scalars.Mutez)
    readonly fee!: BigNumber;

    @GraphField(scalars.BigNumber)
    readonly counter!: BigNumber;

    @GraphField(scalars.BigNumber)
    readonly gas_limit!: BigNumber;

    @GraphField(scalars.BigNumber)
    readonly storage_limit!: BigNumber;

    @GraphField(scalars.PublicKey)
    readonly public_key!: string;

    @GraphField(RevealMetadata)
    readonly metadata!: RevealMetadata;
}
