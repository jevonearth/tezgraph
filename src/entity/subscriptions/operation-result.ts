import { MichelsonV1Expression } from '@taquito/rpc';
import { BigNumber } from 'bignumber.js';
import { Field, Int, ObjectType, registerEnumType } from 'type-graphql';

import { GraphField } from '../graph-field';
import { scalars } from '../scalars';
import { ScriptedContracts } from './scripted-contracts';

export enum OperationResultStatus {
    applied = 'applied',
    failed = 'failed',
    skipped = 'skipped',
    backtracked = 'backtracked',
}

registerEnumType(OperationResultStatus, {
    name: 'OperationResultStatus',
    description: 'Operation result status.',
});

@ObjectType({ description: 'Corresponds to $error.' })
export class OperationError {
    @GraphField(String)
    readonly kind!: string;

    @GraphField(String)
    readonly id!: string;
}

@ObjectType({ isAbstract: true })
export abstract class AbstractOperationResult {
    @GraphField(OperationResultStatus)
    readonly status!: OperationResultStatus;

    @GraphField(scalars.BigNumber, { nullable: true })
    readonly consumed_gas: BigNumber | undefined;

    @GraphField([OperationError], { nullable: true })
    readonly errors: readonly OperationError[] | undefined;
}

export enum InternalOperationResultKind {
    reveal = 'reveal',
    transaction = 'transaction',
    origination = 'origination',
    delegation = 'delegation',
}

registerEnumType(InternalOperationResultKind, {
    name: 'InternalOperationResultKind',
    description: 'The kind of an internal operation result.',
});

@ObjectType({
    description: 'Corresponds to RPC -> `$operation.alpha.operation_contents_and_result` -> `[@kind = transaction]` -> `parameters`'
        + ' or RPC -> `$operation.alpha.internal_operation_result` -> `parameters`.',
})
export class TransactionOperationParameter {
    @Field()
    readonly entrypoint!: string;

    @GraphField(scalars.MichelsonJson)
    readonly value!: MichelsonV1Expression;
}

@ObjectType({ description: 'Corresponds to RPC -> `$operation.alpha.internal_operation_result`.' })
export class InternalOperationResult {
    @GraphField(InternalOperationResultKind)
    readonly kind!: InternalOperationResultKind;

    @GraphField(scalars.Address)
    readonly source!: string;

    @GraphField(Int)
    readonly nonce!: number;

    @GraphField(scalars.Mutez, { nullable: true })
    readonly amount: BigNumber | undefined;

    @GraphField(scalars.Address, { nullable: true })
    readonly destination!: string | undefined;

    @GraphField(TransactionOperationParameter, { nullable: true })
    readonly parameters: TransactionOperationParameter | undefined;

    @GraphField(scalars.PublicKey, { nullable: true })
    readonly public_key: string | undefined;

    @GraphField(scalars.Mutez, { nullable: true })
    readonly balance: BigNumber | undefined;

    @GraphField(scalars.Address, { nullable: true })
    readonly delegate: string | undefined;

    @GraphField(ScriptedContracts, { nullable: true })
    readonly script: ScriptedContracts | undefined;

    @GraphField(AbstractOperationResult)
    readonly result!: AbstractOperationResult;
}
