import { ObjectType } from 'type-graphql';

import { GraphField } from '../graph-field';
import { BalanceUpdate } from './balance-update';
import { InternalOperationResult } from './operation-result';

@ObjectType({ isAbstract: true })
export abstract class AbstractOperationMetadata {
    @GraphField([BalanceUpdate])
    readonly balance_updates!: readonly BalanceUpdate[];
}

@ObjectType({ isAbstract: true })
export abstract class AbstractOperationMetadataWithInternalResults extends AbstractOperationMetadata {
    @GraphField([InternalOperationResult], { nullable: true })
    readonly internal_operation_results: readonly InternalOperationResult[] | undefined;
}

@ObjectType({
    description: 'Corresponds to RPC -> `$operation.alpha.operation_contents_and_result` -> `metadata`'
        + ' of simple operations where there are only `balance_updates`.',
})
export class SimpleOperationMetadata extends AbstractOperationMetadata {}
