import { Int, ObjectType, registerEnumType } from 'type-graphql';

import { GraphField } from '../graph-field';
import { scalars } from '../scalars';
import { OperationKind, OperationNotification } from './operation-notification';

export enum BallotVote {
    nay = 'nay',
    pass = 'pass',
    yay = 'yay',
}

registerEnumType(BallotVote, {
    name: 'BallotVote',
    description: 'The ballot vote.',
});

@ObjectType({
    implements: OperationNotification,
    description: 'Corresponds to RPC -> `$operation.alpha.operation_contents_and_result` -> `[@kind = ballot]`.',
})
export class BallotNotification extends OperationNotification {
    @GraphField(OperationKind, {
        description: `It always equals to \`${OperationKind.ballot}\`.`,
    })
    readonly kind = OperationKind.ballot;

    @GraphField(scalars.Address)
    readonly source!: string;

    @GraphField(Int)
    readonly period!: number;

    @GraphField(scalars.ProtocolHash)
    readonly proposal!: string;

    @GraphField(BallotVote)
    readonly ballot!: BallotVote;
}
