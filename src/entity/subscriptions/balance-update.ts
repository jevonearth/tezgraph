import { BigNumber } from 'bignumber.js';
import { Int, ObjectType, registerEnumType } from 'type-graphql';

import { GraphField } from '../graph-field';
import { scalars } from '../scalars';

export enum BalanceUpdateKind {
    contract = 'contract',
    freezer = 'freezer',
}

registerEnumType(BalanceUpdateKind, {
    name: 'BalanceUpdateKind',
    description: 'The kind of a balance update.',
});

export enum BalanceUpdateCategory {
    rewards = 'rewards',
    fees = 'fees',
    deposits = 'deposits',
}

registerEnumType(BalanceUpdateCategory, {
    name: 'BalanceUpdateCategory',
    description: 'The category of a balance update.',
});

@ObjectType({ description: 'Corresponds to RPC -> `$operation_metadata.alpha.balance_updates`.' })
export class BalanceUpdate {
    @GraphField(BalanceUpdateKind)
    readonly kind!: BalanceUpdateKind;

    @GraphField(BalanceUpdateCategory, { nullable: true })
    readonly category!: BalanceUpdateCategory | undefined;

    @GraphField(scalars.Address, { nullable: true })
    readonly contract!: string | undefined;

    @GraphField(scalars.Address, { nullable: true })
    readonly delegate!: string | undefined;

    @GraphField(Int, { nullable: true })
    readonly cycle!: number | undefined;

    @GraphField(scalars.BigNumber)
    readonly change!: BigNumber;
}
