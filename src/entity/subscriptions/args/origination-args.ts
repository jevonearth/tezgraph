import { ArgsType } from 'type-graphql';

import { GraphField } from '../../graph-field';
import { AddressFilter, NullableAddressArrayFilter, NullableAddressFilter } from '../filters/address-filters';
import { OperationResultStatusFilter } from '../filters/operation-result-status-filter';
import { OriginationNotification } from '../origination-notification';
import { OperationArgs } from './operation-args';

@ArgsType()
export class OriginationArgs extends OperationArgs<OriginationNotification> {
    @GraphField(AddressFilter, { nullable: true })
    source: AddressFilter | undefined;

    @GraphField(NullableAddressFilter, { nullable: true })
    delegate: NullableAddressFilter | undefined;

    @GraphField(NullableAddressArrayFilter, { nullable: true })
    originated_contract: NullableAddressArrayFilter | undefined;

    @GraphField(OperationResultStatusFilter, { nullable: true })
    status: OperationResultStatusFilter | undefined;

    passesInternal(operation: OriginationNotification): boolean {
        return (!this.source || this.source.passes(operation.source))
            && (!this.delegate || this.delegate.passes(operation.delegate))
            && (!this.originated_contract || this.originated_contract.passes(operation.metadata.operation_result.originated_contracts))
            && (!this.status || this.status.passes(operation.metadata.operation_result.status));
    }
}
