import { ArgsType, InputType } from 'type-graphql';

import { GraphField } from '../../graph-field';
import { BallotNotification, BallotVote } from '../ballot-notification';
import { AddressFilter } from '../filters/address-filters';
import { EqualityFilter } from '../filters/equality-filter';
import { ProtocolHashFilter } from '../filters/hash-filters';
import { OperationArgs } from './operation-args';

@InputType()
export class BallotVoteFilter extends EqualityFilter<BallotVote>(BallotVote) {}

@ArgsType()
export class BallotArgs extends OperationArgs<BallotNotification> {
    @GraphField(AddressFilter, { nullable: true })
    source: AddressFilter | undefined;

    @GraphField(ProtocolHashFilter, { nullable: true })
    proposal: ProtocolHashFilter | undefined;

    @GraphField(BallotVoteFilter, { nullable: true })
    ballot: BallotVoteFilter | undefined;

    passesInternal(operation: BallotNotification): boolean {
        return (!this.source || this.source.passes(operation.source))
            && (!this.proposal || this.proposal.passes(operation.proposal))
            && (!this.ballot || this.ballot.passes(operation.ballot));
    }
}
