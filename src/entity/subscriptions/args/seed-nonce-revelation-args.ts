import { ArgsType } from 'type-graphql';

import { SeedNonceRevelationNotification } from '../seed-nonce-revelation-notification';
import { OperationArgs } from './operation-args';

@ArgsType()
export class SeedNonceRevelationArgs extends OperationArgs<SeedNonceRevelationNotification> {
    passesInternal(): boolean {
        return true;
    }
}
