import { ArgsType } from 'type-graphql';

import { GraphField } from '../../graph-field';
import { AddressFilter } from '../filters/address-filters';
import { NullableProtocolHashArrayFilter } from '../filters/hash-filters';
import { ProposalsNotification } from '../proposals-notification';
import { OperationArgs } from './operation-args';

@ArgsType()
export class ProposalsArgs extends OperationArgs<ProposalsNotification> {
    @GraphField(AddressFilter, { nullable: true })
    source: AddressFilter | undefined;

    @GraphField(NullableProtocolHashArrayFilter, { nullable: true })
    proposals: NullableProtocolHashArrayFilter | undefined;

    passesInternal(operation: ProposalsNotification): boolean {
        return (!this.source || this.source.passes(operation.source))
            && (!this.proposals || this.proposals.passes(operation.proposals));
    }
}
