import { ArgsType } from 'type-graphql';

import { GraphField } from '../../graph-field';
import { DelegationNotification } from '../delegation-notification';
import { AddressFilter, NullableAddressFilter } from '../filters/address-filters';
import { OperationResultStatusFilter } from '../filters/operation-result-status-filter';
import { OperationArgs } from './operation-args';

@ArgsType()
export class DelegationArgs extends OperationArgs<DelegationNotification> {
    @GraphField(AddressFilter, { nullable: true })
    source: AddressFilter | undefined;

    @GraphField(NullableAddressFilter, { nullable: true })
    delegate: NullableAddressFilter | undefined;

    @GraphField(OperationResultStatusFilter, { nullable: true })
    status: OperationResultStatusFilter | undefined;

    passesInternal(operation: DelegationNotification): boolean {
        return (!this.source || this.source.passes(operation.source))
            && (!this.delegate || this.delegate.passes(operation.delegate))
            && (!this.status || this.status.passes(operation.metadata.operation_result.status));
    }
}
