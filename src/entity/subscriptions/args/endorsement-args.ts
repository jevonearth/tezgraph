import { ArgsType } from 'type-graphql';

import { GraphField } from '../../graph-field';
import { EndorsementNotification } from '../endorsement-notification';
import { AddressFilter } from '../filters/address-filters';
import { OperationArgs } from './operation-args';

@ArgsType()
export class EndorsementArgs extends OperationArgs<EndorsementNotification> {
    @GraphField(AddressFilter, { nullable: true })
    delegate: AddressFilter | undefined;

    passesInternal(operation: EndorsementNotification): boolean {
        return !this.delegate || this.delegate.passes(operation.metadata.delegate);
    }
}
