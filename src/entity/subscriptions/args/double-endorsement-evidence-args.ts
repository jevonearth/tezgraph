import { ArgsType } from 'type-graphql';

import { isNotNullish } from '../../../utils/reflection';
import { GraphField } from '../../graph-field';
import { DoubleEndorsementEvidenceNotification } from '../double-endorsement-evidence-notification';
import { AddressArrayFilter } from '../filters/address-filters';
import { OperationArgs } from './operation-args';

@ArgsType()
export class DoubleEndorsementEvidenceArgs extends OperationArgs<DoubleEndorsementEvidenceNotification> {
    @GraphField(AddressArrayFilter, { nullable: true })
    delegate: AddressArrayFilter | undefined;

    passesInternal(operation: DoubleEndorsementEvidenceNotification): boolean {
        return !this.delegate || this.delegate.passes(operation.metadata.balance_updates.map(u => u.delegate).filter(isNotNullish));
    }
}
