import { ArgsType } from 'type-graphql';

import { GraphField } from '../../graph-field';
import { ActivateAccountNotification } from '../activate-account-notification';
import { AddressFilter } from '../filters/address-filters';
import { OperationArgs } from './operation-args';

@ArgsType()
export class ActivateAccountArgs extends OperationArgs<ActivateAccountNotification> {
    @GraphField(AddressFilter, { nullable: true })
    pkh: AddressFilter | undefined;

    passesInternal(operation: ActivateAccountNotification): boolean {
        return !this.pkh || this.pkh.passes(operation.pkh);
    }
}
