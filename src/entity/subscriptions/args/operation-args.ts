import { ArgsType, Int } from 'type-graphql';

import { config } from '../../../utils/env-config';
import { GraphField } from '../../graph-field';
import { BlockHashFilter, OperationHashFilter, ProtocolHashFilter } from '../filters/hash-filters';
import { OperationNotification } from '../operation-notification';

@ArgsType()
export abstract class OperationArgs<TOperation extends OperationNotification> {
    @GraphField(Int, {
        nullable: true,
        description: 'Specifies a historical block level to start to retrieve operations. It seamlessly switches to fresh new operations.'
            + ` The value can go only ${config.startBlockLevelMaxFromHead} levels back from the level of the head block.`,
    })
    startBlockLevel: number | undefined;

    @GraphField(OperationHashFilter, { nullable: true })
    hash: OperationHashFilter | undefined;

    @GraphField(ProtocolHashFilter, { nullable: true })
    protocol: ProtocolHashFilter | undefined;

    @GraphField(BlockHashFilter, { nullable: true })
    branch: BlockHashFilter | undefined;

    passes(operation: TOperation): boolean {
        return (!this.hash || this.hash.passes(operation.info.hash))
            && (!this.protocol || this.protocol.passes(operation.info.protocol))
            && (!this.branch || this.branch.passes(operation.info.branch))
            && this.passesInternal(operation);
    }

    protected abstract passesInternal(operation: TOperation): boolean;
}
