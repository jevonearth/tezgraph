import { ArgsType } from 'type-graphql';

import { GraphField } from '../../graph-field';
import { AddressFilter } from '../filters/address-filters';
import { OperationResultStatusFilter } from '../filters/operation-result-status-filter';
import { TransactionNotification } from '../transaction-notification';
import { OperationArgs } from './operation-args';

@ArgsType()
export class TransactionArgs extends OperationArgs<TransactionNotification> {
    @GraphField(AddressFilter, { nullable: true })
    source: AddressFilter | undefined;

    @GraphField(AddressFilter, { nullable: true })
    destination: AddressFilter | undefined;

    @GraphField(OperationResultStatusFilter, { nullable: true })
    status: OperationResultStatusFilter | undefined;

    passesInternal(operation: TransactionNotification): boolean {
        return (!this.source || this.source.passes(operation.source))
            && (!this.destination || this.destination.passes(operation.destination))
            && (!this.status || this.status.passes(operation.metadata.operation_result.status));
    }
}
