import { ArgsType } from 'type-graphql';

import { GraphField } from '../../graph-field';
import { AddressFilter } from '../filters/address-filters';
import { OperationResultStatusFilter } from '../filters/operation-result-status-filter';
import { RevealNotification } from '../reveal-notification';
import { OperationArgs } from './operation-args';

@ArgsType()
export class RevealArgs extends OperationArgs<RevealNotification> {
    @GraphField(AddressFilter, { nullable: true })
    source: AddressFilter | undefined;

    @GraphField(OperationResultStatusFilter, { nullable: true })
    status: OperationResultStatusFilter | undefined;

    passesInternal(operation: RevealNotification): boolean {
        return (!this.source || this.source.passes(operation.source))
            && (!this.status || this.status.passes(operation.metadata.operation_result.status));
    }
}
