import { BigNumber } from 'bignumber.js';
import { ObjectType } from 'type-graphql';

import { GraphField } from '../graph-field';
import { scalars } from '../scalars';
import { BalanceUpdate } from './balance-update';
import { AbstractOperationMetadataWithInternalResults } from './operation-metadata';
import { OperationKind, OperationNotification } from './operation-notification';
import { AbstractOperationResult } from './operation-result';
import { ScriptedContracts } from './scripted-contracts';

@ObjectType({ description: 'Corresponds to RPC -> `$operation.alpha.operation_result.origination`.' })
export class OriginationResult extends AbstractOperationResult {
    @GraphField([BalanceUpdate], { nullable: true })
    readonly balance_updates: readonly BalanceUpdate[] | undefined;

    @GraphField([scalars.Address], { nullable: true })
    readonly originated_contracts!: readonly string[] | undefined;

    @GraphField(scalars.BigNumber, { nullable: true })
    readonly storage_size: BigNumber | undefined;

    @GraphField(scalars.BigNumber, { nullable: true })
    readonly paid_storage_size_diff: BigNumber | undefined;
}

@ObjectType({ description: 'Corresponds to RPC -> `$operation.alpha.operation_contents_and_result` -> `[@kind = origination]` -> `metadata`.' })
export class OriginationMetadata extends AbstractOperationMetadataWithInternalResults {
    @GraphField(OriginationResult)
    readonly operation_result!: OriginationResult;
}

@ObjectType({
    implements: OperationNotification,
    description: 'Corresponds to RPC -> `$operation.alpha.operation_contents_and_result` -> `[@kind = origination]`.',
})
export class OriginationNotification extends OperationNotification {
    @GraphField(OperationKind, {
        description: `It always equals to \`${OperationKind.origination}\`.`,
    })
    readonly kind = OperationKind.origination;

    @GraphField(scalars.Address)
    readonly source!: string;

    @GraphField(scalars.Mutez)
    readonly fee!: BigNumber;

    @GraphField(scalars.BigNumber)
    readonly counter!: BigNumber;

    @GraphField(scalars.BigNumber)
    readonly gas_limit!: BigNumber;

    @GraphField(scalars.BigNumber)
    readonly storage_limit!: BigNumber;

    @GraphField(scalars.Mutez)
    readonly balance!: BigNumber;

    @GraphField(scalars.Address, { nullable: true })
    readonly delegate: string | undefined;

    @GraphField(ScriptedContracts, { nullable: true })
    readonly script: ScriptedContracts | undefined;

    @GraphField(OriginationMetadata)
    readonly metadata!: OriginationMetadata;
}
