import { Int, ObjectType } from 'type-graphql';

import { GraphField } from '../graph-field';
import { scalars } from '../scalars';
import { AbstractOperationMetadata } from './operation-metadata';
import { OperationKind, OperationNotification } from './operation-notification';

@ObjectType({ description: 'Corresponds to RPC -> `$operation.alpha.operation_contents_and_result` -> `[@kind = endorsement]` -> `metadata`.' })
export class EndorsementMetadata extends AbstractOperationMetadata {
    @GraphField(scalars.Address)
    readonly delegate!: string;

    @GraphField([Int])
    readonly slots!: readonly number[];
}

@ObjectType({
    implements: OperationNotification,
    description: 'Corresponds to RPC -> `$operation.alpha.operation_contents_and_result` -> `[@kind = endorsement]`.',
})
export class EndorsementNotification extends OperationNotification {
    @GraphField(OperationKind, {
        description: `It always equals to \`${OperationKind.endorsement}\`.`,
    })
    readonly kind = OperationKind.endorsement;

    @GraphField(Int)
    readonly level!: number;

    @GraphField(EndorsementMetadata)
    readonly metadata!: EndorsementMetadata;
}
