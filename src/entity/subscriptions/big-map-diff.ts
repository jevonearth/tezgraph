import { MichelsonV1Expression } from '@taquito/rpc';
import { ObjectType } from 'type-graphql';

import { GraphField } from '../graph-field';
import { scalars } from '../scalars';

@ObjectType({ description: 'Corresponds to RPC -> `$contract.big_map_diff`.' })
export class BigMapDiff {
    @GraphField(String)
    readonly key_hash!: string;

    @GraphField(scalars.MichelsonJson)
    readonly key!: MichelsonV1Expression;

    @GraphField(scalars.MichelsonJson, { nullable: true })
    readonly value: MichelsonV1Expression | undefined;
}
