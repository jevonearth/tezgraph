import { MichelsonV1Expression } from '@taquito/rpc';
import { BigNumber } from 'bignumber.js';
import { ObjectType } from 'type-graphql';

import { GraphField } from '../graph-field';
import { scalars } from '../scalars';
import { BalanceUpdate } from './balance-update';
import { BigMapDiff } from './big-map-diff';
import { AbstractOperationMetadataWithInternalResults } from './operation-metadata';
import { OperationKind, OperationNotification } from './operation-notification';
import { AbstractOperationResult, TransactionOperationParameter } from './operation-result';

@ObjectType({ description: 'Corresponds to RPC -> `$operation.alpha.operation_result.transaction`.' })
export class TransactionResult extends AbstractOperationResult {
    @GraphField(scalars.MichelsonJson, { nullable: true })
    readonly storage: MichelsonV1Expression | undefined;

    @GraphField([BigMapDiff], { nullable: true })
    readonly big_map_diff: readonly BigMapDiff[] | undefined;

    @GraphField([BalanceUpdate], { nullable: true })
    readonly balance_updates: readonly BalanceUpdate[] | undefined;

    @GraphField([String], { nullable: true })
    readonly originated_contracts: readonly string[] | undefined;

    @GraphField(scalars.BigNumber, { nullable: true })
    readonly storage_size: BigNumber | undefined;

    @GraphField(scalars.BigNumber, { nullable: true })
    readonly paid_storage_size_diff: BigNumber | undefined;

    @GraphField(Boolean, { nullable: true })
    readonly allocated_destination_contract: boolean | undefined;
}

@ObjectType({ description: 'Corresponds to RPC -> `$operation.alpha.operation_contents_and_result` -> `[@kind = transaction]` -> `metadata`.' })
export class TransactionMetadata extends AbstractOperationMetadataWithInternalResults {
    @GraphField(TransactionResult)
    readonly operation_result!: TransactionResult;
}

@ObjectType({
    implements: OperationNotification,
    description: 'Corresponds to RPC -> `$operation.alpha.operation_contents_and_result` -> `[@kind = transaction]`.',
})
export class TransactionNotification extends OperationNotification {
    @GraphField(OperationKind, {
        description: `It always equals to \`${OperationKind.transaction}\`.`,
    })
    readonly kind = OperationKind.transaction;

    @GraphField(scalars.Address)
    readonly source!: string;

    @GraphField(scalars.Mutez)
    readonly fee!: BigNumber;

    @GraphField(scalars.BigNumber)
    readonly counter!: BigNumber;

    @GraphField(scalars.BigNumber)
    readonly gas_limit!: BigNumber;

    @GraphField(scalars.BigNumber)
    readonly storage_limit!: BigNumber;

    @GraphField(scalars.Mutez)
    readonly amount!: BigNumber;

    @GraphField(scalars.Address)
    readonly destination!: string;

    @GraphField(TransactionOperationParameter, { nullable: true })
    readonly parameters: TransactionOperationParameter | undefined;

    @GraphField(TransactionMetadata)
    readonly metadata!: TransactionMetadata;
}
