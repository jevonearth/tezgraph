import { BigNumber } from 'bignumber.js';
import { ObjectType } from 'type-graphql';

import { GraphField } from '../graph-field';
import { scalars } from '../scalars';
import { AbstractOperationMetadataWithInternalResults } from './operation-metadata';
import { OperationKind, OperationNotification } from './operation-notification';
import { AbstractOperationResult } from './operation-result';

@ObjectType({ description: 'Corresponds to RPC -> `$operation.alpha.operation_result.delegation`.' })
export class DelegationResult extends AbstractOperationResult {}

@ObjectType({ description: 'Corresponds to RPC -> `$operation.alpha.operation_contents_and_result` -> `[@kind = delegation]` -> `metadata`.' })
export class DelegationMetadata extends AbstractOperationMetadataWithInternalResults {
    @GraphField(DelegationResult)
    readonly operation_result!: DelegationResult;
}

@ObjectType({
    implements: OperationNotification,
    description: 'Corresponds to RPC -> `$operation.alpha.operation_contents_and_result` -> `[@kind = delegation]`.',
})
export class DelegationNotification extends OperationNotification {
    @GraphField(OperationKind, {
        description: `It always equals to \`${OperationKind.delegation}\`.`,
    })
    readonly kind = OperationKind.delegation;

    @GraphField(scalars.Address)
    readonly source!: string;

    @GraphField(scalars.Mutez)
    readonly fee!: BigNumber;

    @GraphField(scalars.BigNumber)
    readonly counter!: BigNumber;

    @GraphField(scalars.BigNumber)
    readonly gas_limit!: BigNumber;

    @GraphField(scalars.BigNumber)
    readonly storage_limit!: BigNumber;

    @GraphField(scalars.Address, { nullable: true })
    readonly delegate: string | undefined;

    @GraphField(DelegationMetadata)
    readonly metadata!: DelegationMetadata;
}
