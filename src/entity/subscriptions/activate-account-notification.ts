import { ObjectType } from 'type-graphql';

import { GraphField } from '../graph-field';
import { scalars } from '../scalars';
import { SimpleOperationMetadata } from './operation-metadata';
import { OperationKind, OperationNotification } from './operation-notification';

@ObjectType({
    implements: OperationNotification,
    description: 'Corresponds to RPC -> `$operation.alpha.operation_contents_and_result` -> `[@kind = activate_account]`.',
})
export class ActivateAccountNotification extends OperationNotification {
    @GraphField(OperationKind, {
        description: `It always equals to \`${OperationKind.activate_account}\`.`,
    })
    readonly kind = OperationKind.activate_account;

    @GraphField(scalars.Address)
    readonly pkh!: string;

    @GraphField(String)
    readonly secret!: string;

    @GraphField(SimpleOperationMetadata)
    readonly metadata!: SimpleOperationMetadata;
}
