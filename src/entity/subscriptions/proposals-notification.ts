import { Int, ObjectType } from 'type-graphql';

import { GraphField } from '../graph-field';
import { scalars } from '../scalars';
import { OperationKind, OperationNotification } from './operation-notification';

@ObjectType({
    implements: OperationNotification,
    description: 'Corresponds to RPC -> `$operation.alpha.operation_contents_and_result` -> `[@kind = proposals]`.',
})
export class ProposalsNotification extends OperationNotification {
    @GraphField(OperationKind, {
        description: `It always equals to \`${OperationKind.proposals}\`.`,
    })
    readonly kind = OperationKind.proposals;

    @GraphField(scalars.Address)
    readonly source!: string;

    @GraphField(Int)
    readonly period!: number;

    @GraphField([scalars.ProtocolHash], { nullable: true })
    readonly proposals: readonly string[] | undefined;
}
