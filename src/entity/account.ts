import { ObjectType } from 'type-graphql';
import { scalars } from './scalars';
import { GraphField } from './graph-field';
import { TransactionConnection } from './transaction';
import { DelegationConnection } from './delegation';
import { OperationsConnection } from './operations';
import { RevealConnection } from './reveal';
import { OriginationConnection } from './origination';

@ObjectType()
export class Account {
    @GraphField(scalars.Address)
    address!: string;

    @GraphField(TransactionConnection)
    transactions?: TransactionConnection;

    @GraphField(DelegationConnection)
    delegations?: DelegationConnection;

    @GraphField(OperationsConnection)
    operations?: OperationsConnection;

    @GraphField(OriginationConnection)
    originations?: OriginationConnection;

    @GraphField(RevealConnection)
    reveals?: RevealConnection;

    @GraphField(scalars.DateTime, { nullable: true })
    activated?: Date;

    @GraphField(scalars.DateTime, { nullable: true })
    first_seen?: Date;

    @GraphField(scalars.PublicKey, { nullable: true })
    public_key?: string;
}
