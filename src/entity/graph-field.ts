import { Field, FieldOptions } from 'type-graphql';
import { MethodAndPropDecorator, ReturnTypeFuncValue } from 'type-graphql/dist/decorators/types';

/**
 * Compared to regular Field():
 * - Takes field type directly which make it declarative code -> not showing up in test coverage report.
 * - Make type mandatory -> explicit and solves ambiguous situations.
 */
export function GraphField(type: ReturnTypeFuncValue, options?: FieldOptions): MethodAndPropDecorator {
    return Field(() => type, options);
}
