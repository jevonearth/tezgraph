// Data downloaded from Tezos node monitor.
export interface RpcMonitorBlockHeader {
    readonly hash: string;
    readonly level: number;
    readonly proto: number;
    readonly predecessor: string;
    readonly timestamp: string;
    readonly validation_pass: number;
    readonly operations_hash: string;
    readonly fitness: string[];
    readonly context: string;

    /*
     * This is not exposed because it's not present on taquito.BlockHeaderResponse.
     * readonly protocol_data: string;
     */
}
