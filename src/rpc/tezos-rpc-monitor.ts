import fetch from 'node-fetch';
import { singleton } from 'tsyringe';
import { URL } from 'url';

import { Triggers, TypedPubSub } from '../subscriptions/typed-pub-sub';
import { BackgroundWorker } from '../utils/background-worker';
import { errorToString } from '../utils/conversion';
import { EnvConfig } from '../utils/env-config';
import { Logger, LoggerFactory } from '../utils/logger';
import { RpcMonitorBlockHeader } from './rpc-monitor-block-header';

enum TezosMonitorState {
    Inactive = 'Inactive',
    Connecting = 'Connecting',
    Running = 'Running',
}

const OkStatusCode = 200;

/** Monitors Tezos node for new blocks. Then publishes a new header to PubSub. */
@singleton()
export class TezosRpcMonitor implements BackgroundWorker {
    private readonly logger: Logger;
    private readonly url: string;
    private state = TezosMonitorState.Inactive;
    private error: string | undefined;

    constructor(
        envConfig: EnvConfig,
        private readonly pubSub: TypedPubSub,
        loggerFactory: LoggerFactory,
    ) {
        this.logger = loggerFactory.get(TezosRpcMonitor);
        this.url = new URL('/monitor/heads/main', envConfig.tezosNodeUrl).toString();
    }

    get status(): string {
        const stateStr = TezosMonitorState[this.state];
        return this.error ? `${stateStr} with last error: ${this.error}` : stateStr;
    }

    start(): void {
        this.logger.info('Starting.', this.url);
        void this.establishConnection();
    }

    stop(): void {
        this.logger.info('Stopping.');
        this.state = TezosMonitorState.Inactive;
    }

    private async establishConnection(): Promise<void> {
        this.state = TezosMonitorState.Connecting;
        /* eslint-disable no-await-in-loop */
        while (<TezosMonitorState> this.state !== TezosMonitorState.Inactive) {
            try {
                this.state = TezosMonitorState.Connecting;
                const response = await fetch(this.url);
                if (response.status !== OkStatusCode) {
                    throw new Error(`Failed to connect ${response.status} ${response.statusText}`);
                }

                this.logger.info('Established the connection.');
                this.state = TezosMonitorState.Running;
                this.error = undefined;

                for await (const chunk of response.body) {
                    const dataStr = typeof chunk !== 'string' ? chunk.toString() : chunk;
                    const block = <RpcMonitorBlockHeader>JSON.parse(dataStr);

                    this.logger.info(`Publishing a new block header: { level: ${block.level}, hash: "${block.hash}" }`);
                    this.pubSub.publish(Triggers.monitorBlockHeaders, block);

                    if (<TezosMonitorState> this.state === TezosMonitorState.Inactive) {
                        this.logger.info('Exiting tezos monitor');
                        return;
                    }
                }
            } catch (error) {
                this.error = errorToString(error);
                this.logger.error(`Failed listening at: ${this.url}. It will reconnect. ${this.error}`);
            }
        }
        /* eslint-enable no-await-in-loop */

        this.logger.info('Stopped.');
    }
}
