import { singleton } from 'tsyringe';

import { EnvConfig } from '../utils/env-config';

@singleton()
export class RpcRetryHelper {
    constructor(private readonly config: EnvConfig) {}

    async retry<TResponse>(func: () => Promise<TResponse>): Promise<TResponse> {
        return this.retryRecursive(func, this.config.rpcRetryDelaysMillis);
    }

    private async retryRecursive<TResponse>(
        func: () => Promise<TResponse>,
        delays: readonly number[],
    ): Promise<TResponse> {
        try {
            return await func();
        } catch (error) {
            if (delays.length) {
                const [delay, ...remainingDelays] = delays;
                await this.sleep(delay);
                return await this.retryRecursive(func, remainingDelays);
            }
            throw error;
        }
    }

    private async sleep(millis: number): Promise<void> {
        return new Promise(resolve => {
            setTimeout(resolve, millis);
        });
    }
}
