import { BlockHeaderResponse, BlockResponse, RpcClient } from '@taquito/rpc';
import { singleton } from 'tsyringe';

import { Logger, LoggerFactory } from '../utils/logger';
import { RpcRetryHelper } from './rpc-retry-helper';

@singleton()
export class TezosRpcClient {
    private readonly logger: Logger;

    constructor(
        private readonly rpcClient: RpcClient,
        private readonly retryHelper: RpcRetryHelper,
        loggerFactory: LoggerFactory,
    ) {
        this.logger = loggerFactory.get(TezosRpcClient);
    }

    async getBlock(hashOrLevel: string | number): Promise<BlockResponse> {
        return this.retryHelper.retry(async () => {
            this.logger.debug(`Calling getBlock(hashOrLevel: ${hashOrLevel}).`);
            return this.rpcClient.getBlock({ block: hashOrLevel.toString() });
        });
    }

    async getHeadBlockHeader(): Promise<BlockHeaderResponse> {
        return this.retryHelper.retry(async () => {
            this.logger.debug('Calling getHeadBlockHeader().');
            return this.rpcClient.getBlockHeader();
        });
    }
}
