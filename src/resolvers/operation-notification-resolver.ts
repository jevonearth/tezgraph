import { Args, getMetadataStorage, Resolver, Root, Subscription } from 'type-graphql';
import { ArgsParamMetadata } from 'type-graphql/dist/metadata/definitions';
import { convertArgsToInstance } from 'type-graphql/dist/resolvers/convert-args';

import { ActivateAccountNotification } from '../entity/subscriptions/activate-account-notification';
import { ActivateAccountArgs } from '../entity/subscriptions/args/activate-account-args';
import { BallotArgs } from '../entity/subscriptions/args/ballot-args';
import { DelegationArgs } from '../entity/subscriptions/args/delegation-args';
import { DoubleBakingEvidenceArgs } from '../entity/subscriptions/args/double-baking-evidence-args';
import { DoubleEndorsementEvidenceArgs } from '../entity/subscriptions/args/double-endorsement-evidence-args';
import { EndorsementArgs } from '../entity/subscriptions/args/endorsement-args';
import { OperationArgs } from '../entity/subscriptions/args/operation-args';
import { OriginationArgs } from '../entity/subscriptions/args/origination-args';
import { ProposalsArgs } from '../entity/subscriptions/args/proposals-args';
import { RevealArgs } from '../entity/subscriptions/args/reveal-args';
import { SeedNonceRevelationArgs } from '../entity/subscriptions/args/seed-nonce-revelation-args';
import { TransactionArgs } from '../entity/subscriptions/args/transaction-args';
import { BallotNotification } from '../entity/subscriptions/ballot-notification';
import { DelegationNotification } from '../entity/subscriptions/delegation-notification';
import { DoubleBakingEvidenceNotification } from '../entity/subscriptions/double-baking-evidence-notification';
import { DoubleEndorsementEvidenceNotification } from '../entity/subscriptions/double-endorsement-evidence-notification';
import { EndorsementNotification } from '../entity/subscriptions/endorsement-notification';
import { OperationNotification } from '../entity/subscriptions/operation-notification';
import { OriginationNotification } from '../entity/subscriptions/origination-notification';
import { ProposalsNotification } from '../entity/subscriptions/proposals-notification';
import { RevealNotification } from '../entity/subscriptions/reveal-notification';
import { SeedNonceRevelationNotification } from '../entity/subscriptions/seed-nonce-revelation-notification';
import { TransactionNotification } from '../entity/subscriptions/transaction-notification';
import { OperationsProvider } from '../subscriptions/operations-provider';
import { filterIterator } from '../utils/async-iterator-utils';
import { DefaultConstructor } from '../utils/reflection';
import { removeSuffix } from '../utils/string-manipulation';
import { ResolverContext } from './resolver-context';

/** Helper decoration for operation subscriptions. */
function OperationSubscription<
    TOperation extends OperationNotification,
    TArgs extends OperationArgs<TOperation>,
>(
    operationType: DefaultConstructor<TOperation>,
    argsType: DefaultConstructor<TArgs>,
): MethodDecorator {
    return Subscription({
        description: `Subscribes to *${removeSuffix(argsType.name, 'Args')} Operation*-s from newly baked *Block*-s on Tezos network.`,
        subscribe: (_root, argsDictionary, context: ResolverContext) => {
            const metadataStorage = getMetadataStorage();
            const argsMetadata = <ArgsParamMetadata>metadataStorage.params.find(p => p.kind === 'args' && p.getType() === argsType);
            const args = <TArgs>convertArgsToInstance(argsMetadata, argsDictionary);

            return filterIterator(
                context.container.resolve(OperationsProvider).iterate(operationType, args.startBlockLevel),
                o => args.passes(o),
            );
        },
    });
}

/** Resolver for various operation subscriptions. */
@Resolver()
export class OperationNotificationResolver {
    @OperationSubscription(ActivateAccountNotification, ActivateAccountArgs)
    activateAccountAdded(
        @Root() operation: ActivateAccountNotification,
        @Args() _args: ActivateAccountArgs, /* eslint-disable-line @typescript-eslint/indent */
    ): ActivateAccountNotification {
        return operation;
    }

    @OperationSubscription(BallotNotification, BallotArgs)
    ballotAdded(
        @Root() operation: BallotNotification,
        @Args() _args: BallotArgs, /* eslint-disable-line @typescript-eslint/indent */
    ): BallotNotification {
        return operation;
    }

    @OperationSubscription(DelegationNotification, DelegationArgs)
    delegationAdded(
        @Root() operation: DelegationNotification,
        @Args() _args: DelegationArgs, /* eslint-disable-line @typescript-eslint/indent */
    ): DelegationNotification {
        return operation;
    }

    @OperationSubscription(DoubleBakingEvidenceNotification, DoubleBakingEvidenceArgs)
    doubleBakingEvidenceAdded(
        @Root() operation: DoubleBakingEvidenceNotification,
        @Args() _args: DoubleBakingEvidenceArgs, /* eslint-disable-line @typescript-eslint/indent */
    ): DoubleBakingEvidenceNotification {
        return operation;
    }

    @OperationSubscription(DoubleEndorsementEvidenceNotification, DoubleEndorsementEvidenceArgs)
    doubleEndorsementEvidenceAdded(
        @Root() operation: DoubleEndorsementEvidenceNotification,
        @Args() _args: DoubleEndorsementEvidenceArgs, /* eslint-disable-line @typescript-eslint/indent */
    ): DoubleEndorsementEvidenceNotification {
        return operation;
    }

    @OperationSubscription(EndorsementNotification, EndorsementArgs)
    endorsementAdded(
        @Root() operation: EndorsementNotification,
        @Args() _args: EndorsementArgs, /* eslint-disable-line @typescript-eslint/indent */
    ): EndorsementNotification {
        return operation;
    }

    @OperationSubscription(OriginationNotification, OriginationArgs)
    originationAdded(
        @Root() operation: OriginationNotification,
        @Args() _args: OriginationArgs, /* eslint-disable-line @typescript-eslint/indent */
    ): OriginationNotification {
        return operation;
    }

    @OperationSubscription(ProposalsNotification, ProposalsArgs)
    proposalsAdded(
        @Root() operation: ProposalsNotification,
        @Args() _args: ProposalsArgs, /* eslint-disable-line @typescript-eslint/indent */
    ): ProposalsNotification {
        return operation;
    }

    @OperationSubscription(RevealNotification, RevealArgs)
    revealAdded(
        @Root() operation: RevealNotification,
        @Args() _args: RevealArgs, /* eslint-disable-line @typescript-eslint/indent */
    ): RevealNotification {
        return operation;
    }

    @OperationSubscription(SeedNonceRevelationNotification, SeedNonceRevelationArgs)
    seedNonceRevelationAdded(
        @Root() operation: SeedNonceRevelationNotification,
        @Args() _args: SeedNonceRevelationArgs, /* eslint-disable-line @typescript-eslint/indent */
    ): SeedNonceRevelationNotification {
        return operation;
    }

    @OperationSubscription(TransactionNotification, TransactionArgs)
    transactionAdded(
        @Root() operation: TransactionNotification,
        @Args() _args: TransactionArgs, /* eslint-disable-line @typescript-eslint/indent */
    ): TransactionNotification {
        return operation;
    }
}
