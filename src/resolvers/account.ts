import { Resolver, Query, Arg, FieldResolver } from 'type-graphql';
import { Account } from '../entity/account';
import { prisma } from '../database/prisma';
import { getCursorQuery, getDateRangeClause, addressValid, dateRangeValid, DateRange, OrderBy, errorCheck, queryResultsFound, connectionBuilder, OrderByDirection, OrderByField, createOrderBy, createBeforeOrderBy, setTakeLimit, createRelayPageData, applyRelayOrder, createOrderByClause, createDateRangeClause } from '../utils/utils';
import { query_counter } from '../monitoring/prometheus';
import { account_query_histogram } from '../monitoring/prometheus';
import { account_endorsements_query_histogram, account_transactions_query_histogram, account_delegations_query_histogram, account_operations_query_histogram, account_reveals_query_histogram, account_originations_query_histogram } from '../monitoring/prometheus';
import { logger } from '../monitoring/logger';
import { v4 as uuidv4 } from 'uuid';
import { performance } from 'perf_hooks';
import { TransactionConnection } from '../entity/transaction';
import { DelegationConnection } from '../entity/delegation';
import { OperationsConnection } from '../entity/operations';
import { RevealConnection } from '../entity/reveal';
import { OriginationConnection } from '../entity/origination';
import { EndorsementConnection } from '../entity/endorsement';
import { UserInputError } from 'apollo-server';

// First Seen Timestamp Query
const getFirstSeen = async function (pkh: string, uuid: string) {
    logger.info('account.getFirstSeen resolver query', { status: 'start', uuid });
    const performanceStart = performance.now();
    const firstTransactionQuery
        = `SELECT timestamp FROM get_transaction('${pkh}') ORDER BY timestamp LIMIT 1;`;
    const firstTransaction = await prisma.$queryRaw(firstTransactionQuery);
    const performanceEnd = performance.now();
    logger.info('account.getFirstSeen resolver query', { status: 'end', uuid, duration: `${performanceEnd - performanceStart} ms`, query: firstTransactionQuery });

    if (firstTransaction.length == 0) {
        return null;
    }

    return firstTransaction[0].timestamp;
};

// Activated Timestamp Query
const getActivated = async function (pkh: string, uuid: string) {
    logger.info('account.getActivated resolver query', { status: 'start', uuid });
    const performanceStart = performance.now();
    const activatedQuery
        = `SELECT timestamp FROM block WHERE hash = (select block_hash from operation where hash = (select activated from implicit where pkh = '${pkh}'));`;
    const activated = await prisma.$queryRaw(activatedQuery);
    const performanceEnd = performance.now();
    logger.info('account.getActivated resolver query', { status: 'end', uuid, duration: `${performanceEnd - performanceStart} ms`, query: activatedQuery });

    if (activated.length == 0) {
        return null;
    }

    return new Date(activated[0].timestamp);
};

// Activated Timestamp Query
const getRevealedPublicKey = async function (pkh: string, uuid: string) {
    logger.info('account.getRevealedPublicKey resolver query', { status: 'start', uuid });
    const performanceStart = performance.now();
    const revealedQuery
        = `SELECT public_key FROM get_reveal('${pkh}') order by id desc limit 1;`;
    const revealed = await prisma.$queryRaw(revealedQuery);
    const performanceEnd = performance.now();
    logger.info('account.getRevealedPublicKey resolver query', { status: 'end', uuid, duration: `${performanceEnd - performanceStart} ms`, query: revealedQuery });

    if (revealed.length == 0) {
        return null;
    }

    return revealed[0].public_key;
};

let accountPKH: string;
let uuid: string;

@Resolver(() => Account)
export class AccountResolver {
    @Query(() => Account)
    async account(@Arg('address', { nullable: false }) pkh: string) {
        uuid = uuidv4();
        logger.info('account resolver', { status: 'start', uuid });
        const performanceStart = performance.now();

        try {
            query_counter.labels('request', 'account').inc(1);
            const timer = account_query_histogram.startTimer();

            addressValid(pkh);
            accountPKH = pkh;

            // Account Response Body
            const account = Object.create(Account);
            account.address = pkh;
            account.activated = await getActivated(pkh, uuid);
            account.first_seen = await getFirstSeen(pkh, uuid);
            account.public_key = await getRevealedPublicKey(pkh, uuid);

            timer();
            query_counter.labels('success', 'account').inc(1);
            const performanceEnd = performance.now();
            logger.info('account resolver', { status: 'end', uuid, duration: `${performanceEnd - performanceStart} ms` });

            return account;
        } catch (err) {
            return errorCheck(err, 'account', uuid);
        }
    }

    @FieldResolver(() => OriginationConnection)
    async originations(
    @Arg('first', { nullable: true, description: `Sets the number of records retrieved from the beginning. Example: "originations (first: 50)"` }) first?: number,
        @Arg('last', { nullable: true, description: `Sets the number of records retrieved from the end. (last: 50)"` }) last?: number,
        @Arg('before', { nullable: true, description: `Used with the argument "first" or "last" for backwards pagination. Example: "originations (before: "tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6:0")"` }) before?: string,
        @Arg('after', { nullable: true, description: `Used with the argument "first" or "last" for forward pagination.. Example: "originations (after: "tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6:0")"` }) after?: string,
        @Arg('date_range', { nullable: true, description: `Sets the query to retrieve records based on the given date(s). Available options are "gte", "lte". Example: "originations (date: {lte: "2018-09-13T16:23:20Z", gte: "2018-09-13T14:51:20Z"})"` }) dateRange?: DateRange,
        @Arg('order_by', { nullable: true, description: `Sets how the query will order the retrieved records. Provide a field and a direction of ASC or DESC. Example: "originations (order_by:{field: TIMESTAMP, direction: DESC}"` }) orderBy?: OrderBy,
    ) {
        logger.info('account.originations resolver', { status: 'start', uuid });
        const performanceStart = performance.now();

        try {
            query_counter.labels('request', 'account.originations').inc(1);
            const timer = account_originations_query_histogram.startTimer();

            if (orderBy == undefined) {
                orderBy = createOrderBy(OrderByField.ID, OrderByDirection.DESC);
            }

            const takeLimit = setTakeLimit(first, last);
            const dateRangeClause = createDateRangeClause(dateRange);
            const orderByClause = createOrderByClause(first, last, orderBy);
            let originationsQuery: string;
            const relayPageData = createRelayPageData(first, last, before, after);

            if (before || after) {
                if (before && after) {
                    throw new UserInputError(`Argument Error - before/after: Cannot use both "before" and "after" arguments. Please use "before" to paginate backwards and "after" to paginate forwards.`);
                }

                if (before) {
                    const beforeOrderBy = createBeforeOrderBy(first, last, before, orderBy);
                    originationsQuery = await getCursorQuery('origination', before, relayPageData, accountPKH, takeLimit, uuid, beforeOrderBy, dateRange);
                } else if (after) {
                    originationsQuery = await getCursorQuery('origination', after, relayPageData, accountPKH, takeLimit, uuid, orderBy, dateRange);
                } else {
                    throw new UserInputError(`Argument Error - before/after: Pagination argument "before" or "after" detected but unable to be processed.`);
                }
            } else {
                originationsQuery
                    = `SELECT id, hash, op_id as batch_position, source, destination, delegate, kind, timestamp, level, block, counter, gas_limit, storage_limit, public_key, amount, parameters, contract_address, fee as gas, CONCAT(hash, CONCAT(':', op_id)) AS cursor FROM get_origination('${accountPKH}')${dateRangeClause} ${orderByClause} LIMIT ${takeLimit}`;
            }

            originationsQuery = applyRelayOrder(first, last, before, after, orderBy, originationsQuery);

            const originations = await prisma.$queryRaw(originationsQuery);
            queryResultsFound(originations);

            timer();
            query_counter.labels('success', 'account.originations').inc(1);
            const performanceEnd = performance.now();
            logger.info('account.originations resolver', { status: 'end', uuid, duration: `${performanceEnd - performanceStart} ms`, query: originationsQuery });

            // Return originations
            return await connectionBuilder('origination', originations, relayPageData, accountPKH, uuid, orderBy, dateRange);
        } catch (err) {
            return errorCheck(err, 'account.originations', uuid);
        }
    }

    @FieldResolver(() => RevealConnection)
    async reveals(
    @Arg('first', { nullable: true, description: `Sets the number of records retrieved from the beginning. Example: "reveals (first: 50)"` }) first?: number,
        @Arg('last', { nullable: true, description: `Sets the number of records retrieved from the end. (last: 50)"` }) last?: number,
        @Arg('before', { nullable: true, description: `Used with the argument "first" or "last" for backwards pagination. Example: "reveals (before: "tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6:0")"` }) before?: string,
        @Arg('after', { nullable: true, description: `Used with the argument "first" or "last" for forward pagination.. Example: "reveals (after: "tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6:0")"` }) after?: string,
        @Arg('date_range', { nullable: true, description: `Sets the query to retrieve records based on the given date(s). Available options are "gte", "lte". Example: "reveals (date: {lte: "2018-09-13T16:23:20Z", gte: "2018-09-13T14:51:20Z"})"` }) dateRange?: DateRange,
        @Arg('order_by', { nullable: true, description: `Sets how the query will order the retrieved records. Provide a field and a direction of ASC or DESC. Example: "reveals (order_by:{field: TIMESTAMP, direction: DESC}"` }) orderBy?: OrderBy,
    ) {
        logger.info('account.reveals resolver', { status: 'start', uuid });
        const performanceStart = performance.now();

        try {
            query_counter.labels('request', 'account.reveals').inc(1);
            const timer = account_reveals_query_histogram.startTimer();

            if (orderBy == undefined) {
                orderBy = createOrderBy(OrderByField.ID, OrderByDirection.DESC);
            }

            const takeLimit = setTakeLimit(first, last);
            const dateRangeClause = createDateRangeClause(dateRange);
            const orderByClause = createOrderByClause(first, last, orderBy);
            let revealsQuery: string;
            const relayPageData = createRelayPageData(first, last, before, after);

            if (before || after) {
                if (before && after) {
                    throw new UserInputError(`Argument Error - before/after: Cannot use both "before" and "after" arguments. Please use "before" to paginate backwards and "after" to paginate forwards.`);
                }

                if (before) {
                    const beforeOrderBy = createBeforeOrderBy(first, last, before, orderBy);
                    revealsQuery = await getCursorQuery('reveal', before, relayPageData, accountPKH, takeLimit, uuid, beforeOrderBy, dateRange);
                } else if (after) {
                    revealsQuery = await getCursorQuery('reveal', after, relayPageData, accountPKH, takeLimit, uuid, orderBy, dateRange);
                } else {
                    throw new UserInputError(`Argument Error - before/after: Pagination argument "before" or "after" detected but unable to be processed.`);
                }
            } else {
                revealsQuery
                    = `SELECT id, hash, op_id as batch_position, source, destination, delegate, kind, timestamp, level, block, counter, gas_limit, storage_limit, public_key, amount, parameters, contract_address, fee as gas, CONCAT(hash, CONCAT(':', op_id)) AS cursor FROM get_reveal('${accountPKH}')${dateRangeClause} ${orderByClause} LIMIT ${takeLimit}`;
            }

            revealsQuery = applyRelayOrder(first, last, before, after, orderBy, revealsQuery);

            const reveals = await prisma.$queryRaw(revealsQuery);
            queryResultsFound(reveals);

            timer();
            query_counter.labels('success', 'account.reveals').inc(1);
            const performanceEnd = performance.now();
            logger.info('account.reveals resolver', { status: 'end', uuid, duration: `${performanceEnd - performanceStart} ms`, query: revealsQuery });

            return await connectionBuilder('reveal', reveals, relayPageData, accountPKH, uuid, orderBy, dateRange);
        } catch (err) {
            return errorCheck(err, 'account.reveals', uuid);
        }
    }

    @FieldResolver(() => OperationsConnection)
    async operations(
    @Arg('first', { nullable: true, description: `Sets the number of records retrieved from the beginning. Example: "operations (first: 50)"` }) first?: number,
        @Arg('last', { nullable: true, description: `Sets the number of records retrieved from the end. "operations (last: 50)"` }) last?: number,
        @Arg('before', { nullable: true, description: `Used with the argument "first" or "last" for backwards pagination. Example: "operations (before: "tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6:0")"` }) before?: string,
        @Arg('after', { nullable: true, description: `Used with the argument "first" or "last" for forward pagination.. Example: "operations (after: "tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6:0")"` }) after?: string,
        @Arg('date_range', { nullable: true, description: `Sets the query to retrieve records based on the given date(s). Available options are "gte", "lte". Example: "operations (date: {lte: "2018-09-13T16:23:20Z", gte: "2018-09-13T14:51:20Z"})"` }) dateRange?: DateRange,
        @Arg('order_by', { nullable: true, description: `Sets how the query will order the retrieved records. Provide a field and a direction of ASC or DESC. Example: "operations (order_by:{field: TIMESTAMP, direction: DESC}"` }) orderBy?: OrderBy,
    ) {
        logger.info('account.operations resolver', { status: 'start', uuid });
        const performanceStart = performance.now();

        try {
            query_counter.labels('request', 'account.operations').inc(1);
            const timer = account_operations_query_histogram.startTimer();

            if (orderBy == undefined) {
                orderBy = createOrderBy(OrderByField.ID, OrderByDirection.DESC);
            }

            const takeLimit = setTakeLimit(first, last);
            const dateRangeClause = createDateRangeClause(dateRange);
            const orderByClause = createOrderByClause(first, last, orderBy);
            let operationsQuery: string;
            const relayPageData = createRelayPageData(first, last, before, after);

            if (before || after) {
                if (before && after) {
                    throw new UserInputError(`Argument Error - before/after: Cannot use both "before" and "after" arguments. Please use "before" to paginate backwards and "after" to paginate forwards.`);
                }

                if (before) {
                    const beforeOrderBy = createBeforeOrderBy(first, last, before, orderBy);
                    operationsQuery = await getCursorQuery('operations', before, relayPageData, accountPKH, takeLimit, uuid, beforeOrderBy, dateRange);
                } else if (after) {
                    operationsQuery = await getCursorQuery('operations', after, relayPageData, accountPKH, takeLimit, uuid, orderBy, dateRange);
                } else {
                    throw new UserInputError(`Argument Error - before/after: Pagination argument "before" or "after" detected but unable to be processed.`);
                }
            } else {
                operationsQuery
                    = `SELECT id,hash, op_id as batch_position, source, destination, delegate, kind, timestamp, level, block, counter, gas_limit, storage_limit, public_key, amount, parameters, contract_address, fee as gas, CONCAT(hash, CONCAT(':', op_id)) AS cursor FROM get_operations('${accountPKH}')${dateRangeClause} ${orderByClause} LIMIT ${takeLimit}`;
            }

            operationsQuery = applyRelayOrder(first, last, before, after, orderBy, operationsQuery);

            const operations = await prisma.$queryRaw(operationsQuery);
            queryResultsFound(operations);

            timer();
            query_counter.labels('success', 'account.operations').inc(1);
            const performanceEnd = performance.now();
            logger.info('account.operations resolver', { status: 'end', uuid, duration: `${performanceEnd - performanceStart} ms`, query: operationsQuery });

            return await connectionBuilder('operations', operations, relayPageData, accountPKH, uuid, orderBy, dateRange);
        } catch (err) {
            return errorCheck(err, 'account.operations', uuid);
        }
    }

    @FieldResolver(() => TransactionConnection)
    async transactions(
    @Arg('first', { nullable: true, description: `Sets the number of records retrieved from the beginning. Example: "transactions (first: 50)"` }) first?: number,
        @Arg('last', { nullable: true, description: `Sets the number of records retrieved from the end. "transactions (last: 50)"` }) last?: number,
        @Arg('before', { nullable: true, description: `Used with the argument "first" or "last" for backwards pagination. Example: "transactions (before: "tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6:0")"` }) before?: string,
        @Arg('after', { nullable: true, description: `Used with the argument "first" or "last" for forward pagination.. Example: "transactions (after: "tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6:0")"` }) after?: string,
        @Arg('date_range', { nullable: true, description: `Sets the query to retrieve records based on the given date(s). Available options are "gte", "lte", "lt", and "gt". Example: "transactions (date: {lte: "2018-09-13T16:23:20Z", gte: "2018-09-13T14:51:20Z"})"` }) dateRange?: DateRange,
        @Arg('order_by', { nullable: true, description: `Sets how the query will order the retrieved records. Provide a field and a direction of ASC or DESC. Example: "transactions (order_by:{field: TIMESTAMP, direction: DESC}"` }) orderBy?: OrderBy,
    ) {
        logger.info('account.transactions resolver', { status: 'start', uuid });
        const performanceStart = performance.now();

        try {
            query_counter.labels('request', 'account.transactions').inc(1);
            const timer = account_transactions_query_histogram.startTimer();

            if (orderBy == undefined) {
                orderBy = createOrderBy(OrderByField.ID, OrderByDirection.DESC);
            }

            const takeLimit = setTakeLimit(first, last);
            const dateRangeClause = createDateRangeClause(dateRange);
            const orderByClause = createOrderByClause(first, last, orderBy);
            let transactionsQuery: string;
            const relayPageData = createRelayPageData(first, last, before, after);

            if (before || after) {
                if (before && after) {
                    throw new UserInputError(`Argument Error - before/after: Cannot use both "before" and "after" arguments. Please use "before" to paginate backwards and "after" to paginate forwards.`);
                }

                if (before) {
                    const beforeOrderBy = createBeforeOrderBy(first, last, before, orderBy);
                    transactionsQuery = await getCursorQuery('transaction', before, relayPageData, accountPKH, takeLimit, uuid, beforeOrderBy, dateRange);
                } else if (after) {
                    transactionsQuery = await getCursorQuery('transaction', after, relayPageData, accountPKH, takeLimit, uuid, orderBy, dateRange);
                } else {
                    throw new UserInputError(`Argument Error - before/after: Pagination argument "before" or "after" detected but unable to be processed.`);
                }
            } else {
                transactionsQuery
                    = `SELECT id,  hash, op_id as batch_position, source, destination, kind, level, block, counter, gas_limit, storage_limit, entrypoint, fee as gas, amount, parameters, timestamp, CONCAT(hash, CONCAT(':', op_id)) AS cursor FROM get_transaction('${accountPKH}') ${dateRangeClause} ${orderByClause} LIMIT ${takeLimit}`;
            }

            transactionsQuery = applyRelayOrder(first, last, before, after, orderBy, transactionsQuery);

            const transactions = await prisma.$queryRaw(transactionsQuery);
            queryResultsFound(transactions);

            timer();
            query_counter.labels('success', 'account.transactions').inc(1);
            const performanceEnd = performance.now();
            logger.info('account.transactions resolver', { status: 'end', uuid, duration: `${performanceEnd - performanceStart} ms`, query: transactionsQuery });

            return await connectionBuilder('transaction', transactions, relayPageData, accountPKH, uuid, orderBy, dateRange);
        } catch (err) {
            return errorCheck(err, 'account.transactions', uuid);
        }
    }

    @FieldResolver(() => DelegationConnection)
    async delegations(
    @Arg('first', { nullable: true, description: `Sets the number of records retrieved from the beginning. Example: "delegations (first: 50)"` }) first?: number,
        @Arg('last', { nullable: true, description: `Sets the number of records retrieved from the end. "delegations (last: 50)"` }) last?: number,
        @Arg('before', { nullable: true, description: `Used with the argument "first" or "last" for backwards pagination. Example: "delegations (before: "tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6:0")"` }) before?: string,
        @Arg('after', { nullable: true, description: `Used with the argument "first" or "last" for forward pagination.. Example: "delegations (after: "tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6:0")"` }) after?: string,
        @Arg('date_range', { nullable: true, description: `Sets the query to retrieve records based on the given date(s). Available options are "gte", "lte". Example: "delegations (date: {lte: "2018-09-13T16:23:20Z", gte: "2018-09-13T14:51:20Z"})"` }) dateRange?: DateRange,
        @Arg('order_by', { nullable: true, description: `Sets how the query will order the retrieved records. Provide a field and a direction of ASC or DESC. Example: "delegations (order_by:{field: TIMESTAMP, direction: DESC}"` }) orderBy?: OrderBy,
    ) {
        logger.info('account.delegations resolver', { status: 'start', uuid });
        const performanceStart = performance.now();

        try {
            query_counter.labels('request', 'account.delegations').inc(1);
            const timer = account_delegations_query_histogram.startTimer();

            if (orderBy == undefined) {
                orderBy = createOrderBy(OrderByField.ID, OrderByDirection.DESC);
            }

            const takeLimit = setTakeLimit(first, last);
            let dateRangeClause = createDateRangeClause(dateRange);
            const orderByClause = createOrderByClause(first, last, orderBy);
            let delegationsQuery: string;
            const relayPageData = createRelayPageData(first, last, before, after);

            if (before || after) {
                if (before && after) {
                    throw new UserInputError(`Argument Error - before/after: Cannot use both "before" and "after" arguments. Please use "before" to paginate backwards and "after" to paginate forwards.`);
                }

                if (before) {
                    const beforeOrderBy = createBeforeOrderBy(first, last, before, orderBy);
                    delegationsQuery = await getCursorQuery('delegation', before, relayPageData, accountPKH, takeLimit, uuid, beforeOrderBy, dateRange);
                } else if (after) {
                    delegationsQuery = await getCursorQuery('delegation', after, relayPageData, accountPKH, takeLimit, uuid, orderBy, dateRange);
                } else {
                    throw new UserInputError(`Argument Error - before/after: Pagination argument "before" or "after" detected but unable to be processed.`);
                }
            } else {
                if (dateRange) {
                    dateRangeValid(dateRange);
                    dateRangeClause = getDateRangeClause(dateRange);
                }

                delegationsQuery
                    = `SELECT DISTINCT id,hash, op_id as batch_position, source, delegate, kind, timestamp, level, block, counter, gas_limit, storage_limit, public_key, amount, parameters, contract_address, fee as gas, CONCAT(hash, CONCAT(':', op_id)) AS cursor FROM get_delegation('${accountPKH}')${dateRangeClause} ${orderByClause} LIMIT ${takeLimit}`;
            }

            delegationsQuery = applyRelayOrder(first, last, before, after, orderBy, delegationsQuery);
            const delegations = await prisma.$queryRaw(delegationsQuery);
            queryResultsFound(delegations);

            timer();
            query_counter.labels('success', 'account.delegations').inc(1);
            const performanceEnd = performance.now();
            logger.info('account.delegations resolver', { status: 'end', uuid, duration: `${performanceEnd - performanceStart} ms`, query: delegationsQuery });

            // Return delegations
            return await connectionBuilder('delegation', delegations, relayPageData, accountPKH, uuid, orderBy, dateRange);
        } catch (err) {
            return errorCheck(err, 'account.delegations', uuid);
        }
    }

    @FieldResolver(() => EndorsementConnection)
    async endorsements(
    @Arg('first', { nullable: true, description: `Sets the number of records retrieved from the beginning. Example: "endorsements (first: 50)"` }) first?: number,
        @Arg('last', { nullable: true, description: `Sets the number of records retrieved from the end. "endorsements (last: 50)"` }) last?: number,
        @Arg('before', { nullable: true, description: `Used with the argument "first" or "last" for backwards pagination. Example: "endorsements (before: "tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6:0")"` }) before?: string,
        @Arg('after', { nullable: true, description: `Used with the argument "first" or "last" for forward pagination.. Example: "endorsements (after: "tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6:0")"` }) after?: string,
        @Arg('date_range', { nullable: true, description: `Sets the query to retrieve records based on the given date(s). Available options are "gte", "lte". Example: "endorsements (date: {lte: "2018-09-13T16:23:20Z", gte: "2018-09-13T14:51:20Z"})"` }) dateRange?: DateRange,
        @Arg('order_by', { nullable: true, description: `Sets how the query will order the retrieved records. Provide a field and a direction of ASC or DESC. Example: "endorsements (order_by:{field: TIMESTAMP, direction: DESC}"` }) orderBy?: OrderBy,
    ) {
        logger.info('account.endorsements resolver', { status: 'start', uuid });
        const performanceStart = performance.now();

        try {
            query_counter.labels('request', 'account.endorsements').inc(1);
            const timer = account_endorsements_query_histogram.startTimer();

            if (orderBy == undefined) {
                orderBy = createOrderBy(OrderByField.ID, OrderByDirection.DESC);
            }

            const takeLimit = setTakeLimit(first, last);
            let dateRangeClause = createDateRangeClause(dateRange);
            const orderByClause = createOrderByClause(first, last, orderBy);
            let endorsementsQuery: string;
            const relayPageData = createRelayPageData(first, last, before, after);

            if (before || after) {
                if (before && after) {
                    throw new UserInputError(`Argument Error - before/after: Cannot use both "before" and "after" arguments. Please use "before" to paginate backwards and "after" to paginate forwards.`);
                }

                if (before) {
                    const beforeOrderBy = createBeforeOrderBy(first, last, before, orderBy);
                    endorsementsQuery = await getCursorQuery('endorsement', before, relayPageData, accountPKH, takeLimit, uuid, beforeOrderBy, dateRange);
                } else if (after) {
                    endorsementsQuery = await getCursorQuery('endorsement', after, relayPageData, accountPKH, takeLimit, uuid, orderBy, dateRange);
                } else {
                    throw new UserInputError(`Argument Error - before/after: Pagination argument "before" or "after" detected but unable to be processed.`);
                }
            } else {
                if (dateRange) {
                    dateRangeValid(dateRange);
                    dateRangeClause = getDateRangeClause(dateRange);
                }

                endorsementsQuery
                    = `SELECT DISTINCT id, hash, op_id as batch_position, source, delegate, slots, kind, timestamp, level, block, counter, gas_limit, storage_limit, public_key, amount, parameters, contract_address, fee as gas, CONCAT(hash, CONCAT(':', op_id)) AS cursor FROM get_endorsement('${accountPKH}')${dateRangeClause} ${orderByClause} LIMIT ${takeLimit}`;
            }

            endorsementsQuery = applyRelayOrder(first, last, before, after, orderBy, endorsementsQuery);
            const endorsements = await prisma.$queryRaw(endorsementsQuery);
            queryResultsFound(endorsements);

            timer();
            query_counter.labels('success', 'account.endorsements').inc(1);
            const performanceEnd = performance.now();
            logger.info('account.endorsements resolver', { status: 'end', uuid, duration: `${performanceEnd - performanceStart} ms`, query: endorsementsQuery });

            // Return endorsements
            return await connectionBuilder('endorsement', endorsements, relayPageData, accountPKH, uuid, orderBy, dateRange);
        } catch (err) {
            return errorCheck(err, 'account.endorsements', uuid);
        }
    }
}
