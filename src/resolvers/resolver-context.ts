import express from 'express';
import { DependencyContainer } from 'tsyringe';

/** Enhanced context used for processing requests. */
export interface ResolverContext {
    req: express.Request;
    res: express.Response;
    container: DependencyContainer;
}
