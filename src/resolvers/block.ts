import { Arg, Query, Resolver } from 'type-graphql';

import { prisma } from '../database/prisma';
import { Block } from '../entity/block';

@Resolver()
export class BlockResolver {
    @Query(() => [Block])
    async blocks(@Arg('limit', { nullable: true }) limit?: number) {
        if (limit && limit > 200) {
            throw new Error('TAKE_ARGUMENT_MUST_BE_LESS_THAN_200');
        }
        const takeLimit = limit || 200;

        const blocks = await prisma.block.findMany({
            take: takeLimit,
            orderBy: [
                {
                    timestamp: 'asc',
                },
            ],
        });

        return blocks;
    }
}
