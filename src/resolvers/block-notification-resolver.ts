import { Resolver, Root, Subscription } from 'type-graphql';

import { BlockNotification } from '../entity/subscriptions/block-notification';
import { Triggers, TypedPubSub } from '../subscriptions/typed-pub-sub';
import { ResolverContext } from './resolver-context';

@Resolver()
export class BlockNotificationResolver {
    @Subscription({
        subscribe: (_x, _args, context: ResolverContext) => context.container.resolve(TypedPubSub).iterate(Triggers.blocks),
    })
    blockAdded(@Root() block: BlockNotification): BlockNotification {
        return block;
    }
}
