import { injectable } from 'tsyringe';
import { Query, Resolver } from 'type-graphql';

import { TezosRpcMonitor } from '../rpc/tezos-rpc-monitor';

@Resolver()
@injectable()
export class TezosNodeMonitorStatusResolver {
    constructor(private readonly tezosMonitor: TezosRpcMonitor) {}

    @Query()
    tezosNodeMonitorStatus(): string {
        return this.tezosMonitor.status;
    }
}
