import {
  DMMF,
  DMMFClass,
  Engine,
  PrismaClientKnownRequestError,
  PrismaClientUnknownRequestError,
  PrismaClientRustPanicError,
  PrismaClientInitializationError,
  PrismaClientValidationError,
  sqltag as sql,
  empty,
  join,
  raw,
} from './runtime';

export { PrismaClientKnownRequestError }
export { PrismaClientUnknownRequestError }
export { PrismaClientRustPanicError }
export { PrismaClientInitializationError }
export { PrismaClientValidationError }

/**
 * Re-export of sql-template-tag
 */
export { sql, empty, join, raw }

/**
 * Prisma Client JS version: 2.6.2
 * Query Engine version: 6a8054bb549e4cc23f157b0010cb2e95cb2637fb
 */
export declare type PrismaVersion = {
  client: string
}

export declare const prismaVersion: PrismaVersion 

/**
 * Utility Types
 */

/**
 * From https://github.com/sindresorhus/type-fest/
 * Matches a JSON object.
 * This type can be useful to enforce some input to be JSON-compatible or as a super-type to be extended from. 
 */
export declare type JsonObject = {[Key in string]?: JsonValue}
 
/**
 * From https://github.com/sindresorhus/type-fest/
 * Matches a JSON array.
 */
export declare interface JsonArray extends Array<JsonValue> {}
 
/**
 * From https://github.com/sindresorhus/type-fest/
 * Matches any valid JSON value.
 */
export declare type JsonValue = string | number | boolean | null | JsonObject | JsonArray

/**
 * Same as JsonObject, but allows undefined
 */
export declare type InputJsonObject = {[Key in string]?: JsonValue}
 
export declare interface InputJsonArray extends Array<JsonValue> {}
 
export declare type InputJsonValue = undefined |  string | number | boolean | null | InputJsonObject | InputJsonArray

declare type SelectAndInclude = {
  select: any
  include: any
}

declare type HasSelect = {
  select: any
}

declare type HasInclude = {
  include: any
}

declare type CheckSelect<T, S, U> = T extends SelectAndInclude
  ? 'Please either choose `select` or `include`'
  : T extends HasSelect
  ? U
  : T extends HasInclude
  ? U
  : S

/**
 * Get the type of the value, that the Promise holds.
 */
export declare type PromiseType<T extends PromiseLike<any>> = T extends PromiseLike<infer U> ? U : T;

/**
 * Get the return type of a function which returns a Promise.
 */
export declare type PromiseReturnType<T extends (...args: any) => Promise<any>> = PromiseType<ReturnType<T>>


export declare type Enumerable<T> = T | Array<T>;

export type RequiredKeys<T> = {
  [K in keyof T]-?: {} extends Pick<T, K> ? never : K
}[keyof T]

export declare type TruthyKeys<T> = {
  [key in keyof T]: T[key] extends false | undefined | null ? never : key
}[keyof T]

export declare type TrueKeys<T> = TruthyKeys<Pick<T, RequiredKeys<T>>>

/**
 * Subset
 * @desc From `T` pick properties that exist in `U`. Simple version of Intersection
 */
export declare type Subset<T, U> = {
  [key in keyof T]: key extends keyof U ? T[key] : never;
};
declare class PrismaClientFetcher {
  private readonly prisma;
  private readonly debug;
  private readonly hooks?;
  constructor(prisma: PrismaClient<any, any>, debug?: boolean, hooks?: Hooks | undefined);
  request<T>(document: any, dataPath?: string[], rootField?: string, typeName?: string, isList?: boolean, callsite?: string): Promise<T>;
  sanitizeMessage(message: string): string;
  protected unpack(document: any, data: any, path: string[], rootField?: string, isList?: boolean): any;
}


/**
 * Client
**/

export declare type Datasource = {
  url?: string
}

export type Datasources = {
  db?: Datasource
}

export type ErrorFormat = 'pretty' | 'colorless' | 'minimal'

export interface PrismaClientOptions {
  /**
   * Overwrites the datasource url from your prisma.schema file
   */
  datasources?: Datasources

  /**
   * @default "colorless"
   */
  errorFormat?: ErrorFormat

  /**
   * @example
   * ```
   * // Defaults to stdout
   * log: ['query', 'info', 'warn', 'error']
   * 
   * // Emit as events
   * log: [
   *  { emit: 'stdout', level: 'query' },
   *  { emit: 'stdout', level: 'info' },
   *  { emit: 'stdout', level: 'warn' }
   *  { emit: 'stdout', level: 'error' }
   * ]
   * ```
   * Read more in our [docs](https://www.prisma.io/docs/reference/tools-and-interfaces/prisma-client/logging#the-log-option).
   */
  log?: Array<LogLevel | LogDefinition>
}

export type Hooks = {
  beforeRequest?: (options: {query: string, path: string[], rootField?: string, typeName?: string, document: any}) => any
}

/* Types for Logging */
export type LogLevel = 'info' | 'query' | 'warn' | 'error'
export type LogDefinition = {
  level: LogLevel
  emit: 'stdout' | 'event'
}

export type GetLogType<T extends LogLevel | LogDefinition> = T extends LogDefinition ? T['emit'] extends 'event' ? T['level'] : never : never
export type GetEvents<T extends any> = T extends Array<LogLevel | LogDefinition> ?
  GetLogType<T[0]> | GetLogType<T[1]> | GetLogType<T[2]> | GetLogType<T[3]>
  : never

export type QueryEvent = {
  timestamp: Date
  query: string
  params: string
  duration: number
  target: string
}

export type LogEvent = {
  timestamp: Date
  message: string
  target: string
}
/* End Types for Logging */


export type PrismaAction =
  | 'findOne'
  | 'findMany'
  | 'create'
  | 'update'
  | 'updateMany'
  | 'upsert'
  | 'delete'
  | 'deleteMany'
  | 'executeRaw'
  | 'queryRaw'
  | 'aggregate'

/**
 * These options are being passed in to the middleware as "params"
 */
export type MiddlewareParams = {
  model?: string
  action: PrismaAction
  args: any
  dataPath: string[]
  runInTransaction: boolean
}

/**
 * The `T` type makes sure, that the `return proceed` is not forgotten in the middleware implementation
 */
export type Middleware<T = any> = (
  params: MiddlewareParams,
  next: (params: MiddlewareParams) => Promise<T>,
) => Promise<T>

// tested in getLogLevel.test.ts
export declare function getLogLevel(log: Array<LogLevel | LogDefinition>): LogLevel | undefined;

/**
 * ##  Prisma Client ʲˢ
 * 
 * Type-safe database client for TypeScript & Node.js (ORM replacement)
 * @example
 * ```
 * const prisma = new PrismaClient()
 * // Fetch zero or more Blocks
 * const blocks = await prisma.block.findMany()
 * ```
 *
 * 
 * Read more in our [docs](https://www.prisma.io/docs/reference/tools-and-interfaces/prisma-client).
 */
export declare class PrismaClient<
  T extends PrismaClientOptions = PrismaClientOptions,
  U = 'log' extends keyof T ? T['log'] extends Array<LogLevel | LogDefinition> ? GetEvents<T['log']> : never : never
> {
  /**
   * @private
   */
  private fetcher;
  /**
   * @private
   */
  private readonly dmmf;
  /**
   * @private
   */
  private connectionPromise?;
  /**
   * @private
   */
  private disconnectionPromise?;
  /**
   * @private
   */
  private readonly engineConfig;
  /**
   * @private
   */
  private readonly measurePerformance;
  /**
   * @private
   */
  private engine: Engine;
  /**
   * @private
   */
  private errorFormat: ErrorFormat;

  /**
   * ##  Prisma Client ʲˢ
   * 
   * Type-safe database client for TypeScript & Node.js (ORM replacement)
   * @example
   * ```
   * const prisma = new PrismaClient()
   * // Fetch zero or more Blocks
   * const blocks = await prisma.block.findMany()
   * ```
   *
   * 
   * Read more in our [docs](https://www.prisma.io/docs/reference/tools-and-interfaces/prisma-client).
   */
  constructor(optionsArg?: T);
  $on<V extends U>(eventType: V, callback: (event: V extends 'query' ? QueryEvent : LogEvent) => void): void;
  /**
   * @deprecated renamed to `$on`
   */
  on<V extends U>(eventType: V, callback: (event: V extends 'query' ? QueryEvent : LogEvent) => void): void;
  /**
   * Connect with the database
   */
  $connect(): Promise<void>;
  /**
   * @deprecated renamed to `$connect`
   */
  connect(): Promise<void>;

  /**
   * Disconnect from the database
   */
  $disconnect(): Promise<any>;
  /**
   * @deprecated renamed to `$disconnect`
   */
  disconnect(): Promise<any>;

  /**
   * Add a middleware
   */
  $use(cb: Middleware): void

  /**
   * Executes a raw query and returns the number of affected rows
   * @example
   * ```
   * // With parameters use prisma.executeRaw``, values will be escaped automatically
   * const result = await prisma.executeRaw`UPDATE User SET cool = ${true} WHERE id = ${1};`
   * // Or
   * const result = await prisma.executeRaw('UPDATE User SET cool = $1 WHERE id = $2 ;', true, 1)
  * ```
  * 
  * Read more in our [docs](https://www.prisma.io/docs/reference/tools-and-interfaces/prisma-client/raw-database-access).
  */
  $executeRaw<T = any>(query: string | TemplateStringsArray, ...values: any[]): Promise<number>;

  /**
   * @deprecated renamed to `$executeRaw`
   */
  executeRaw<T = any>(query: string | TemplateStringsArray, ...values: any[]): Promise<number>;

  /**
   * Performs a raw query and returns the SELECT data
   * @example
   * ```
   * // With parameters use prisma.queryRaw``, values will be escaped automatically
   * const result = await prisma.queryRaw`SELECT * FROM User WHERE id = ${1} OR email = ${'ema.il'};`
   * // Or
   * const result = await prisma.queryRaw('SELECT * FROM User WHERE id = $1 OR email = $2;', 1, 'ema.il')
  * ```
  * 
  * Read more in our [docs](https://www.prisma.io/docs/reference/tools-and-interfaces/prisma-client/raw-database-access).
  */
  $queryRaw<T = any>(query: string | TemplateStringsArray, ...values: any[]): Promise<T>;
 
  /**
   * @deprecated renamed to `$executeRaw`
   */
  queryRaw<T = any>(query: string | TemplateStringsArray, ...values: any[]): Promise<T>;

  /**
   * `prisma.block`: Exposes CRUD operations for the **block** model.
    * Example usage:
    * ```ts
    * // Fetch zero or more Blocks
    * const blocks = await prisma.block.findMany()
    * ```
    */
  get block(): blockDelegate;

  /**
   * `prisma.block_alpha`: Exposes CRUD operations for the **block_alpha** model.
    * Example usage:
    * ```ts
    * // Fetch zero or more Block_alphas
    * const block_alphas = await prisma.block_alpha.findMany()
    * ```
    */
  get block_alpha(): block_alphaDelegate;

  /**
   * `prisma.chain`: Exposes CRUD operations for the **chain** model.
    * Example usage:
    * ```ts
    * // Fetch zero or more Chains
    * const chains = await prisma.chain.findMany()
    * ```
    */
  get chain(): chainDelegate;

  /**
   * `prisma.contract`: Exposes CRUD operations for the **contract** model.
    * Example usage:
    * ```ts
    * // Fetch zero or more Contracts
    * const contracts = await prisma.contract.findMany()
    * ```
    */
  get contract(): contractDelegate;

  /**
   * `prisma.deactivated`: Exposes CRUD operations for the **deactivated** model.
    * Example usage:
    * ```ts
    * // Fetch zero or more Deactivateds
    * const deactivateds = await prisma.deactivated.findMany()
    * ```
    */
  get deactivated(): deactivatedDelegate;

  /**
   * `prisma.delegated_contract`: Exposes CRUD operations for the **delegated_contract** model.
    * Example usage:
    * ```ts
    * // Fetch zero or more Delegated_contracts
    * const delegated_contracts = await prisma.delegated_contract.findMany()
    * ```
    */
  get delegated_contract(): delegated_contractDelegate;

  /**
   * `prisma.delegation`: Exposes CRUD operations for the **delegation** model.
    * Example usage:
    * ```ts
    * // Fetch zero or more Delegations
    * const delegations = await prisma.delegation.findMany()
    * ```
    */
  get delegation(): delegationDelegate;

  /**
   * `prisma.endorsement`: Exposes CRUD operations for the **endorsement** model.
    * Example usage:
    * ```ts
    * // Fetch zero or more Endorsements
    * const endorsements = await prisma.endorsement.findMany()
    * ```
    */
  get endorsement(): endorsementDelegate;

  /**
   * `prisma.implicit`: Exposes CRUD operations for the **implicit** model.
    * Example usage:
    * ```ts
    * // Fetch zero or more Implicits
    * const implicits = await prisma.implicit.findMany()
    * ```
    */
  get implicit(): implicitDelegate;

  /**
   * `prisma.operation`: Exposes CRUD operations for the **operation** model.
    * Example usage:
    * ```ts
    * // Fetch zero or more Operations
    * const operations = await prisma.operation.findMany()
    * ```
    */
  get operation(): operationDelegate;

  /**
   * `prisma.operation_alpha`: Exposes CRUD operations for the **operation_alpha** model.
    * Example usage:
    * ```ts
    * // Fetch zero or more Operation_alphas
    * const operation_alphas = await prisma.operation_alpha.findMany()
    * ```
    */
  get operation_alpha(): operation_alphaDelegate;

  /**
   * `prisma.origination`: Exposes CRUD operations for the **origination** model.
    * Example usage:
    * ```ts
    * // Fetch zero or more Originations
    * const originations = await prisma.origination.findMany()
    * ```
    */
  get origination(): originationDelegate;

  /**
   * `prisma.snapshot`: Exposes CRUD operations for the **snapshot** model.
    * Example usage:
    * ```ts
    * // Fetch zero or more Snapshots
    * const snapshots = await prisma.snapshot.findMany()
    * ```
    */
  get snapshot(): snapshotDelegate;

  /**
   * `prisma.tx`: Exposes CRUD operations for the **tx** model.
    * Example usage:
    * ```ts
    * // Fetch zero or more Txes
    * const txes = await prisma.tx.findMany()
    * ```
    */
  get tx(): txDelegate;

  /**
   * `prisma.addresses`: Exposes CRUD operations for the **addresses** model.
    * Example usage:
    * ```ts
    * // Fetch zero or more Addresses
    * const addresses = await prisma.addresses.findMany()
    * ```
    */
  get addresses(): addressesDelegate;

  /**
   * `prisma.manager_numbers`: Exposes CRUD operations for the **manager_numbers** model.
    * Example usage:
    * ```ts
    * // Fetch zero or more Manager_numbers
    * const manager_numbers = await prisma.manager_numbers.findMany()
    * ```
    */
  get manager_numbers(): manager_numbersDelegate;
}



/**
 * Enums
 */

// Based on
// https://github.com/microsoft/TypeScript/issues/3192#issuecomment-261720275

export declare const BlockDistinctFieldEnum: {
  hash: 'hash',
  level: 'level',
  proto: 'proto',
  predecessor: 'predecessor',
  timestamp: 'timestamp',
  validation_passes: 'validation_passes',
  merkle_root: 'merkle_root',
  fitness: 'fitness',
  context_hash: 'context_hash',
  sterile: 'sterile'
};

export declare type BlockDistinctFieldEnum = (typeof BlockDistinctFieldEnum)[keyof typeof BlockDistinctFieldEnum]


export declare const Block_alphaDistinctFieldEnum: {
  hash: 'hash',
  baker: 'baker',
  level_position: 'level_position',
  cycle: 'cycle',
  cycle_position: 'cycle_position',
  voting_period: 'voting_period',
  voting_period_position: 'voting_period_position',
  voting_period_kind: 'voting_period_kind',
  consumed_gas: 'consumed_gas'
};

export declare type Block_alphaDistinctFieldEnum = (typeof Block_alphaDistinctFieldEnum)[keyof typeof Block_alphaDistinctFieldEnum]


export declare const ChainDistinctFieldEnum: {
  hash: 'hash'
};

export declare type ChainDistinctFieldEnum = (typeof ChainDistinctFieldEnum)[keyof typeof ChainDistinctFieldEnum]


export declare const ContractDistinctFieldEnum: {
  address: 'address',
  block_hash: 'block_hash',
  mgr: 'mgr',
  delegate: 'delegate',
  spendable: 'spendable',
  delegatable: 'delegatable',
  credit: 'credit',
  preorig: 'preorig',
  script: 'script'
};

export declare type ContractDistinctFieldEnum = (typeof ContractDistinctFieldEnum)[keyof typeof ContractDistinctFieldEnum]


export declare const DeactivatedDistinctFieldEnum: {
  pkh: 'pkh',
  block_hash: 'block_hash'
};

export declare type DeactivatedDistinctFieldEnum = (typeof DeactivatedDistinctFieldEnum)[keyof typeof DeactivatedDistinctFieldEnum]


export declare const Delegated_contractDistinctFieldEnum: {
  delegate: 'delegate',
  delegator: 'delegator',
  cycle: 'cycle',
  level: 'level'
};

export declare type Delegated_contractDistinctFieldEnum = (typeof Delegated_contractDistinctFieldEnum)[keyof typeof Delegated_contractDistinctFieldEnum]


export declare const DelegationDistinctFieldEnum: {
  operation_hash: 'operation_hash',
  op_id: 'op_id',
  source: 'source',
  pkh: 'pkh',
  autoid: 'autoid'
};

export declare type DelegationDistinctFieldEnum = (typeof DelegationDistinctFieldEnum)[keyof typeof DelegationDistinctFieldEnum]


export declare const EndorsementDistinctFieldEnum: {
  block_hash: 'block_hash',
  op: 'op',
  id: 'id',
  level: 'level',
  pkh: 'pkh',
  slot: 'slot'
};

export declare type EndorsementDistinctFieldEnum = (typeof EndorsementDistinctFieldEnum)[keyof typeof EndorsementDistinctFieldEnum]


export declare const ImplicitDistinctFieldEnum: {
  pkh: 'pkh',
  activated: 'activated',
  revealed: 'revealed',
  pk: 'pk',
  autoid: 'autoid'
};

export declare type ImplicitDistinctFieldEnum = (typeof ImplicitDistinctFieldEnum)[keyof typeof ImplicitDistinctFieldEnum]


export declare const OperationDistinctFieldEnum: {
  hash: 'hash',
  chain: 'chain',
  block_hash: 'block_hash',
  autoid: 'autoid'
};

export declare type OperationDistinctFieldEnum = (typeof OperationDistinctFieldEnum)[keyof typeof OperationDistinctFieldEnum]


export declare const Operation_alphaDistinctFieldEnum: {
  hash: 'hash',
  id: 'id',
  operation_kind: 'operation_kind',
  sender: 'sender',
  receiver: 'receiver',
  autoid: 'autoid'
};

export declare type Operation_alphaDistinctFieldEnum = (typeof Operation_alphaDistinctFieldEnum)[keyof typeof Operation_alphaDistinctFieldEnum]


export declare const OriginationDistinctFieldEnum: {
  operation_hash: 'operation_hash',
  op_id: 'op_id',
  source: 'source',
  k: 'k',
  autoid: 'autoid'
};

export declare type OriginationDistinctFieldEnum = (typeof OriginationDistinctFieldEnum)[keyof typeof OriginationDistinctFieldEnum]


export declare const SnapshotDistinctFieldEnum: {
  cycle: 'cycle',
  level: 'level'
};

export declare type SnapshotDistinctFieldEnum = (typeof SnapshotDistinctFieldEnum)[keyof typeof SnapshotDistinctFieldEnum]


export declare const TxDistinctFieldEnum: {
  operation_hash: 'operation_hash',
  op_id: 'op_id',
  source: 'source',
  destination: 'destination',
  fee: 'fee',
  amount: 'amount',
  parameters: 'parameters',
  storage: 'storage',
  consumed_gas: 'consumed_gas',
  storage_size: 'storage_size',
  paid_storage_size_diff: 'paid_storage_size_diff',
  entrypoint: 'entrypoint',
  bigmapdiff: 'bigmapdiff',
  autoid: 'autoid'
};

export declare type TxDistinctFieldEnum = (typeof TxDistinctFieldEnum)[keyof typeof TxDistinctFieldEnum]


export declare const AddressesDistinctFieldEnum: {
  address: 'address'
};

export declare type AddressesDistinctFieldEnum = (typeof AddressesDistinctFieldEnum)[keyof typeof AddressesDistinctFieldEnum]


export declare const Manager_numbersDistinctFieldEnum: {
  hash: 'hash',
  counter: 'counter',
  gas_limit: 'gas_limit',
  storage_limit: 'storage_limit'
};

export declare type Manager_numbersDistinctFieldEnum = (typeof Manager_numbersDistinctFieldEnum)[keyof typeof Manager_numbersDistinctFieldEnum]


export declare const SortOrder: {
  asc: 'asc',
  desc: 'desc'
};

export declare type SortOrder = (typeof SortOrder)[keyof typeof SortOrder]



/**
 * Model block
 */

export type block = {
  hash: string
  level: number
  proto: number
  predecessor: string
  timestamp: Date
  validation_passes: number
  merkle_root: string
  fitness: string
  context_hash: string
  sterile: boolean | null
}


export type AggregateBlock = {
  count: number
  avg: BlockAvgAggregateOutputType | null
  sum: BlockSumAggregateOutputType | null
  min: BlockMinAggregateOutputType | null
  max: BlockMaxAggregateOutputType | null
}

export type BlockAvgAggregateOutputType = {
  level: number
  proto: number
  validation_passes: number
}

export type BlockSumAggregateOutputType = {
  level: number
  proto: number
  validation_passes: number
}

export type BlockMinAggregateOutputType = {
  level: number
  proto: number
  validation_passes: number
}

export type BlockMaxAggregateOutputType = {
  level: number
  proto: number
  validation_passes: number
}


export type BlockAvgAggregateInputType = {
  level?: true
  proto?: true
  validation_passes?: true
}

export type BlockSumAggregateInputType = {
  level?: true
  proto?: true
  validation_passes?: true
}

export type BlockMinAggregateInputType = {
  level?: true
  proto?: true
  validation_passes?: true
}

export type BlockMaxAggregateInputType = {
  level?: true
  proto?: true
  validation_passes?: true
}

export type AggregateBlockArgs = {
  where?: blockWhereInput
  orderBy?: Enumerable<blockOrderByInput>
  cursor?: blockWhereUniqueInput
  take?: number
  skip?: number
  distinct?: Enumerable<BlockDistinctFieldEnum>
  count?: true
  avg?: BlockAvgAggregateInputType
  sum?: BlockSumAggregateInputType
  min?: BlockMinAggregateInputType
  max?: BlockMaxAggregateInputType
}

export type GetBlockAggregateType<T extends AggregateBlockArgs> = {
  [P in keyof T]: P extends 'count' ? number : GetBlockAggregateScalarType<T[P]>
}

export type GetBlockAggregateScalarType<T extends any> = {
  [P in keyof T]: P extends keyof BlockAvgAggregateOutputType ? BlockAvgAggregateOutputType[P] : never
}
    
    

export type blockSelect = {
  hash?: boolean
  level?: boolean
  proto?: boolean
  predecessor?: boolean
  timestamp?: boolean
  validation_passes?: boolean
  merkle_root?: boolean
  fitness?: boolean
  context_hash?: boolean
  sterile?: boolean
  block_alpha?: boolean | FindManyblock_alphaArgs
  contract?: boolean | FindManycontractArgs
  deactivated?: boolean | FindManydeactivatedArgs
  endorsement?: boolean | FindManyendorsementArgs
  operation?: boolean | FindManyoperationArgs
}

export type blockInclude = {
  block_alpha?: boolean | FindManyblock_alphaArgs
  contract?: boolean | FindManycontractArgs
  deactivated?: boolean | FindManydeactivatedArgs
  endorsement?: boolean | FindManyendorsementArgs
  operation?: boolean | FindManyoperationArgs
}

export type blockGetPayload<
  S extends boolean | null | undefined | blockArgs,
  U = keyof S
> = S extends true
  ? block
  : S extends undefined
  ? never
  : S extends blockArgs | FindManyblockArgs
  ? 'include' extends U
    ? block  & {
      [P in TrueKeys<S['include']>]:
      P extends 'block_alpha'
      ? Array<block_alphaGetPayload<S['include'][P]>> :
      P extends 'contract'
      ? Array<contractGetPayload<S['include'][P]>> :
      P extends 'deactivated'
      ? Array<deactivatedGetPayload<S['include'][P]>> :
      P extends 'endorsement'
      ? Array<endorsementGetPayload<S['include'][P]>> :
      P extends 'operation'
      ? Array<operationGetPayload<S['include'][P]>> : never
    }
  : 'select' extends U
    ? {
      [P in TrueKeys<S['select']>]:P extends keyof block ? block[P]
: 
      P extends 'block_alpha'
      ? Array<block_alphaGetPayload<S['select'][P]>> :
      P extends 'contract'
      ? Array<contractGetPayload<S['select'][P]>> :
      P extends 'deactivated'
      ? Array<deactivatedGetPayload<S['select'][P]>> :
      P extends 'endorsement'
      ? Array<endorsementGetPayload<S['select'][P]>> :
      P extends 'operation'
      ? Array<operationGetPayload<S['select'][P]>> : never
    }
  : block
: block


export interface blockDelegate {
  /**
   * Find zero or one Block.
   * @param {FindOneblockArgs} args - Arguments to find a Block
   * @example
   * // Get one Block
   * const block = await prisma.block.findOne({
   *   where: {
   *     // ... provide filter here
   *   }
   * })
  **/
  findOne<T extends FindOneblockArgs>(
    args: Subset<T, FindOneblockArgs>
  ): CheckSelect<T, Prisma__blockClient<block | null>, Prisma__blockClient<blockGetPayload<T> | null>>
  /**
   * Find zero or more Blocks.
   * @param {FindManyblockArgs=} args - Arguments to filter and select certain fields only.
   * @example
   * // Get all Blocks
   * const blocks = await prisma.block.findMany()
   * 
   * // Get first 10 Blocks
   * const blocks = await prisma.block.findMany({ take: 10 })
   * 
   * // Only select the `hash`
   * const blockWithHashOnly = await prisma.block.findMany({ select: { hash: true } })
   * 
  **/
  findMany<T extends FindManyblockArgs>(
    args?: Subset<T, FindManyblockArgs>
  ): CheckSelect<T, Promise<Array<block>>, Promise<Array<blockGetPayload<T>>>>
  /**
   * Create a Block.
   * @param {blockCreateArgs} args - Arguments to create a Block.
   * @example
   * // Create one Block
   * const Block = await prisma.block.create({
   *   data: {
   *     // ... data to create a Block
   *   }
   * })
   * 
  **/
  create<T extends blockCreateArgs>(
    args: Subset<T, blockCreateArgs>
  ): CheckSelect<T, Prisma__blockClient<block>, Prisma__blockClient<blockGetPayload<T>>>
  /**
   * Delete a Block.
   * @param {blockDeleteArgs} args - Arguments to delete one Block.
   * @example
   * // Delete one Block
   * const Block = await prisma.block.delete({
   *   where: {
   *     // ... filter to delete one Block
   *   }
   * })
   * 
  **/
  delete<T extends blockDeleteArgs>(
    args: Subset<T, blockDeleteArgs>
  ): CheckSelect<T, Prisma__blockClient<block>, Prisma__blockClient<blockGetPayload<T>>>
  /**
   * Update one Block.
   * @param {blockUpdateArgs} args - Arguments to update one Block.
   * @example
   * // Update one Block
   * const block = await prisma.block.update({
   *   where: {
   *     // ... provide filter here
   *   },
   *   data: {
   *     // ... provide data here
   *   }
   * })
   * 
  **/
  update<T extends blockUpdateArgs>(
    args: Subset<T, blockUpdateArgs>
  ): CheckSelect<T, Prisma__blockClient<block>, Prisma__blockClient<blockGetPayload<T>>>
  /**
   * Delete zero or more Blocks.
   * @param {blockDeleteManyArgs} args - Arguments to filter Blocks to delete.
   * @example
   * // Delete a few Blocks
   * const { count } = await prisma.block.deleteMany({
   *   where: {
   *     // ... provide filter here
   *   }
   * })
   * 
  **/
  deleteMany<T extends blockDeleteManyArgs>(
    args: Subset<T, blockDeleteManyArgs>
  ): Promise<BatchPayload>
  /**
   * Update zero or more Blocks.
   * @param {blockUpdateManyArgs} args - Arguments to update one or more rows.
   * @example
   * // Update many Blocks
   * const block = await prisma.block.updateMany({
   *   where: {
   *     // ... provide filter here
   *   },
   *   data: {
   *     // ... provide data here
   *   }
   * })
   * 
  **/
  updateMany<T extends blockUpdateManyArgs>(
    args: Subset<T, blockUpdateManyArgs>
  ): Promise<BatchPayload>
  /**
   * Create or update one Block.
   * @param {blockUpsertArgs} args - Arguments to update or create a Block.
   * @example
   * // Update or create a Block
   * const block = await prisma.block.upsert({
   *   create: {
   *     // ... data to create a Block
   *   },
   *   update: {
   *     // ... in case it already exists, update
   *   },
   *   where: {
   *     // ... the filter for the Block we want to update
   *   }
   * })
  **/
  upsert<T extends blockUpsertArgs>(
    args: Subset<T, blockUpsertArgs>
  ): CheckSelect<T, Prisma__blockClient<block>, Prisma__blockClient<blockGetPayload<T>>>
  /**
   * Count
   */
  count(args?: Omit<FindManyblockArgs, 'select' | 'include'>): Promise<number>

  /**
   * Aggregate
   */
  aggregate<T extends AggregateBlockArgs>(args: Subset<T, AggregateBlockArgs>): Promise<GetBlockAggregateType<T>>
}

/**
 * The delegate class that acts as a "Promise-like" for block.
 * Why is this prefixed with `Prisma__`?
 * Because we want to prevent naming conflicts as mentioned in 
 * https://github.com/prisma/prisma-client-js/issues/707
 */
export declare class Prisma__blockClient<T> implements Promise<T> {
  private readonly _dmmf;
  private readonly _fetcher;
  private readonly _queryType;
  private readonly _rootField;
  private readonly _clientMethod;
  private readonly _args;
  private readonly _dataPath;
  private readonly _errorFormat;
  private readonly _measurePerformance?;
  private _isList;
  private _callsite;
  private _requestPromise?;
  constructor(_dmmf: DMMFClass, _fetcher: PrismaClientFetcher, _queryType: 'query' | 'mutation', _rootField: string, _clientMethod: string, _args: any, _dataPath: string[], _errorFormat: ErrorFormat, _measurePerformance?: boolean | undefined, _isList?: boolean);
  readonly [Symbol.toStringTag]: 'PrismaClientPromise';

  block_alpha<T extends FindManyblock_alphaArgs = {}>(args?: Subset<T, FindManyblock_alphaArgs>): CheckSelect<T, Promise<Array<block_alpha>>, Promise<Array<block_alphaGetPayload<T>>>>;

  contract<T extends FindManycontractArgs = {}>(args?: Subset<T, FindManycontractArgs>): CheckSelect<T, Promise<Array<contract>>, Promise<Array<contractGetPayload<T>>>>;

  deactivated<T extends FindManydeactivatedArgs = {}>(args?: Subset<T, FindManydeactivatedArgs>): CheckSelect<T, Promise<Array<deactivated>>, Promise<Array<deactivatedGetPayload<T>>>>;

  endorsement<T extends FindManyendorsementArgs = {}>(args?: Subset<T, FindManyendorsementArgs>): CheckSelect<T, Promise<Array<endorsement>>, Promise<Array<endorsementGetPayload<T>>>>;

  operation<T extends FindManyoperationArgs = {}>(args?: Subset<T, FindManyoperationArgs>): CheckSelect<T, Promise<Array<operation>>, Promise<Array<operationGetPayload<T>>>>;

  private get _document();
  /**
   * Attaches callbacks for the resolution and/or rejection of the Promise.
   * @param onfulfilled The callback to execute when the Promise is resolved.
   * @param onrejected The callback to execute when the Promise is rejected.
   * @returns A Promise for the completion of which ever callback is executed.
   */
  then<TResult1 = T, TResult2 = never>(onfulfilled?: ((value: T) => TResult1 | Promise<TResult1>) | undefined | null, onrejected?: ((reason: any) => TResult2 | Promise<TResult2>) | undefined | null): Promise<TResult1 | TResult2>;
  /**
   * Attaches a callback for only the rejection of the Promise.
   * @param onrejected The callback to execute when the Promise is rejected.
   * @returns A Promise for the completion of the callback.
   */
  catch<TResult = never>(onrejected?: ((reason: any) => TResult | Promise<TResult>) | undefined | null): Promise<T | TResult>;
  /**
   * Attaches a callback that is invoked when the Promise is settled (fulfilled or rejected). The
   * resolved value cannot be modified from the callback.
   * @param onfinally The callback to execute when the Promise is settled (fulfilled or rejected).
   * @returns A Promise for the completion of the callback.
   */
  finally(onfinally?: (() => void) | undefined | null): Promise<T>;
}

// Custom InputTypes

/**
 * block findOne
 */
export type FindOneblockArgs = {
  /**
   * Select specific fields to fetch from the block
  **/
  select?: blockSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: blockInclude | null
  /**
   * Filter, which block to fetch.
  **/
  where: blockWhereUniqueInput
}


/**
 * block findMany
 */
export type FindManyblockArgs = {
  /**
   * Select specific fields to fetch from the block
  **/
  select?: blockSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: blockInclude | null
  /**
   * Filter, which blocks to fetch.
  **/
  where?: blockWhereInput
  /**
   * Determine the order of the blocks to fetch.
  **/
  orderBy?: Enumerable<blockOrderByInput>
  /**
   * Sets the position for listing blocks.
  **/
  cursor?: blockWhereUniqueInput
  /**
   * The number of blocks to fetch. If negative number, it will take blocks before the `cursor`.
  **/
  take?: number
  /**
   * Skip the first `n` blocks.
  **/
  skip?: number
  distinct?: Enumerable<BlockDistinctFieldEnum>
}


/**
 * block create
 */
export type blockCreateArgs = {
  /**
   * Select specific fields to fetch from the block
  **/
  select?: blockSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: blockInclude | null
  /**
   * The data needed to create a block.
  **/
  data: blockCreateInput
}


/**
 * block update
 */
export type blockUpdateArgs = {
  /**
   * Select specific fields to fetch from the block
  **/
  select?: blockSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: blockInclude | null
  /**
   * The data needed to update a block.
  **/
  data: blockUpdateInput
  /**
   * Choose, which block to update.
  **/
  where: blockWhereUniqueInput
}


/**
 * block updateMany
 */
export type blockUpdateManyArgs = {
  data: blockUpdateManyMutationInput
  where?: blockWhereInput
}


/**
 * block upsert
 */
export type blockUpsertArgs = {
  /**
   * Select specific fields to fetch from the block
  **/
  select?: blockSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: blockInclude | null
  /**
   * The filter to search for the block to update in case it exists.
  **/
  where: blockWhereUniqueInput
  /**
   * In case the block found by the `where` argument doesn't exist, create a new block with this data.
  **/
  create: blockCreateInput
  /**
   * In case the block was found with the provided `where` argument, update it with this data.
  **/
  update: blockUpdateInput
}


/**
 * block delete
 */
export type blockDeleteArgs = {
  /**
   * Select specific fields to fetch from the block
  **/
  select?: blockSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: blockInclude | null
  /**
   * Filter which block to delete.
  **/
  where: blockWhereUniqueInput
}


/**
 * block deleteMany
 */
export type blockDeleteManyArgs = {
  where?: blockWhereInput
}


/**
 * block without action
 */
export type blockArgs = {
  /**
   * Select specific fields to fetch from the block
  **/
  select?: blockSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: blockInclude | null
}



/**
 * Model block_alpha
 */

export type block_alpha = {
  hash: string
  baker: string
  level_position: number
  cycle: number
  cycle_position: number
  voting_period: number
  voting_period_position: number
  voting_period_kind: number
  consumed_gas: string
}


export type AggregateBlock_alpha = {
  count: number
  avg: Block_alphaAvgAggregateOutputType | null
  sum: Block_alphaSumAggregateOutputType | null
  min: Block_alphaMinAggregateOutputType | null
  max: Block_alphaMaxAggregateOutputType | null
}

export type Block_alphaAvgAggregateOutputType = {
  level_position: number
  cycle: number
  cycle_position: number
  voting_period: number
  voting_period_position: number
  voting_period_kind: number
}

export type Block_alphaSumAggregateOutputType = {
  level_position: number
  cycle: number
  cycle_position: number
  voting_period: number
  voting_period_position: number
  voting_period_kind: number
}

export type Block_alphaMinAggregateOutputType = {
  level_position: number
  cycle: number
  cycle_position: number
  voting_period: number
  voting_period_position: number
  voting_period_kind: number
}

export type Block_alphaMaxAggregateOutputType = {
  level_position: number
  cycle: number
  cycle_position: number
  voting_period: number
  voting_period_position: number
  voting_period_kind: number
}


export type Block_alphaAvgAggregateInputType = {
  level_position?: true
  cycle?: true
  cycle_position?: true
  voting_period?: true
  voting_period_position?: true
  voting_period_kind?: true
}

export type Block_alphaSumAggregateInputType = {
  level_position?: true
  cycle?: true
  cycle_position?: true
  voting_period?: true
  voting_period_position?: true
  voting_period_kind?: true
}

export type Block_alphaMinAggregateInputType = {
  level_position?: true
  cycle?: true
  cycle_position?: true
  voting_period?: true
  voting_period_position?: true
  voting_period_kind?: true
}

export type Block_alphaMaxAggregateInputType = {
  level_position?: true
  cycle?: true
  cycle_position?: true
  voting_period?: true
  voting_period_position?: true
  voting_period_kind?: true
}

export type AggregateBlock_alphaArgs = {
  where?: block_alphaWhereInput
  orderBy?: Enumerable<block_alphaOrderByInput>
  cursor?: block_alphaWhereUniqueInput
  take?: number
  skip?: number
  distinct?: Enumerable<Block_alphaDistinctFieldEnum>
  count?: true
  avg?: Block_alphaAvgAggregateInputType
  sum?: Block_alphaSumAggregateInputType
  min?: Block_alphaMinAggregateInputType
  max?: Block_alphaMaxAggregateInputType
}

export type GetBlock_alphaAggregateType<T extends AggregateBlock_alphaArgs> = {
  [P in keyof T]: P extends 'count' ? number : GetBlock_alphaAggregateScalarType<T[P]>
}

export type GetBlock_alphaAggregateScalarType<T extends any> = {
  [P in keyof T]: P extends keyof Block_alphaAvgAggregateOutputType ? Block_alphaAvgAggregateOutputType[P] : never
}
    
    

export type block_alphaSelect = {
  hash?: boolean
  baker?: boolean
  level_position?: boolean
  cycle?: boolean
  cycle_position?: boolean
  voting_period?: boolean
  voting_period_position?: boolean
  voting_period_kind?: boolean
  consumed_gas?: boolean
  block?: boolean | blockArgs
}

export type block_alphaInclude = {
  block?: boolean | blockArgs
}

export type block_alphaGetPayload<
  S extends boolean | null | undefined | block_alphaArgs,
  U = keyof S
> = S extends true
  ? block_alpha
  : S extends undefined
  ? never
  : S extends block_alphaArgs | FindManyblock_alphaArgs
  ? 'include' extends U
    ? block_alpha  & {
      [P in TrueKeys<S['include']>]:
      P extends 'block'
      ? blockGetPayload<S['include'][P]> : never
    }
  : 'select' extends U
    ? {
      [P in TrueKeys<S['select']>]:P extends keyof block_alpha ? block_alpha[P]
: 
      P extends 'block'
      ? blockGetPayload<S['select'][P]> : never
    }
  : block_alpha
: block_alpha


export interface block_alphaDelegate {
  /**
   * Find zero or one Block_alpha.
   * @param {FindOneblock_alphaArgs} args - Arguments to find a Block_alpha
   * @example
   * // Get one Block_alpha
   * const block_alpha = await prisma.block_alpha.findOne({
   *   where: {
   *     // ... provide filter here
   *   }
   * })
  **/
  findOne<T extends FindOneblock_alphaArgs>(
    args: Subset<T, FindOneblock_alphaArgs>
  ): CheckSelect<T, Prisma__block_alphaClient<block_alpha | null>, Prisma__block_alphaClient<block_alphaGetPayload<T> | null>>
  /**
   * Find zero or more Block_alphas.
   * @param {FindManyblock_alphaArgs=} args - Arguments to filter and select certain fields only.
   * @example
   * // Get all Block_alphas
   * const block_alphas = await prisma.block_alpha.findMany()
   * 
   * // Get first 10 Block_alphas
   * const block_alphas = await prisma.block_alpha.findMany({ take: 10 })
   * 
   * // Only select the `hash`
   * const block_alphaWithHashOnly = await prisma.block_alpha.findMany({ select: { hash: true } })
   * 
  **/
  findMany<T extends FindManyblock_alphaArgs>(
    args?: Subset<T, FindManyblock_alphaArgs>
  ): CheckSelect<T, Promise<Array<block_alpha>>, Promise<Array<block_alphaGetPayload<T>>>>
  /**
   * Create a Block_alpha.
   * @param {block_alphaCreateArgs} args - Arguments to create a Block_alpha.
   * @example
   * // Create one Block_alpha
   * const Block_alpha = await prisma.block_alpha.create({
   *   data: {
   *     // ... data to create a Block_alpha
   *   }
   * })
   * 
  **/
  create<T extends block_alphaCreateArgs>(
    args: Subset<T, block_alphaCreateArgs>
  ): CheckSelect<T, Prisma__block_alphaClient<block_alpha>, Prisma__block_alphaClient<block_alphaGetPayload<T>>>
  /**
   * Delete a Block_alpha.
   * @param {block_alphaDeleteArgs} args - Arguments to delete one Block_alpha.
   * @example
   * // Delete one Block_alpha
   * const Block_alpha = await prisma.block_alpha.delete({
   *   where: {
   *     // ... filter to delete one Block_alpha
   *   }
   * })
   * 
  **/
  delete<T extends block_alphaDeleteArgs>(
    args: Subset<T, block_alphaDeleteArgs>
  ): CheckSelect<T, Prisma__block_alphaClient<block_alpha>, Prisma__block_alphaClient<block_alphaGetPayload<T>>>
  /**
   * Update one Block_alpha.
   * @param {block_alphaUpdateArgs} args - Arguments to update one Block_alpha.
   * @example
   * // Update one Block_alpha
   * const block_alpha = await prisma.block_alpha.update({
   *   where: {
   *     // ... provide filter here
   *   },
   *   data: {
   *     // ... provide data here
   *   }
   * })
   * 
  **/
  update<T extends block_alphaUpdateArgs>(
    args: Subset<T, block_alphaUpdateArgs>
  ): CheckSelect<T, Prisma__block_alphaClient<block_alpha>, Prisma__block_alphaClient<block_alphaGetPayload<T>>>
  /**
   * Delete zero or more Block_alphas.
   * @param {block_alphaDeleteManyArgs} args - Arguments to filter Block_alphas to delete.
   * @example
   * // Delete a few Block_alphas
   * const { count } = await prisma.block_alpha.deleteMany({
   *   where: {
   *     // ... provide filter here
   *   }
   * })
   * 
  **/
  deleteMany<T extends block_alphaDeleteManyArgs>(
    args: Subset<T, block_alphaDeleteManyArgs>
  ): Promise<BatchPayload>
  /**
   * Update zero or more Block_alphas.
   * @param {block_alphaUpdateManyArgs} args - Arguments to update one or more rows.
   * @example
   * // Update many Block_alphas
   * const block_alpha = await prisma.block_alpha.updateMany({
   *   where: {
   *     // ... provide filter here
   *   },
   *   data: {
   *     // ... provide data here
   *   }
   * })
   * 
  **/
  updateMany<T extends block_alphaUpdateManyArgs>(
    args: Subset<T, block_alphaUpdateManyArgs>
  ): Promise<BatchPayload>
  /**
   * Create or update one Block_alpha.
   * @param {block_alphaUpsertArgs} args - Arguments to update or create a Block_alpha.
   * @example
   * // Update or create a Block_alpha
   * const block_alpha = await prisma.block_alpha.upsert({
   *   create: {
   *     // ... data to create a Block_alpha
   *   },
   *   update: {
   *     // ... in case it already exists, update
   *   },
   *   where: {
   *     // ... the filter for the Block_alpha we want to update
   *   }
   * })
  **/
  upsert<T extends block_alphaUpsertArgs>(
    args: Subset<T, block_alphaUpsertArgs>
  ): CheckSelect<T, Prisma__block_alphaClient<block_alpha>, Prisma__block_alphaClient<block_alphaGetPayload<T>>>
  /**
   * Count
   */
  count(args?: Omit<FindManyblock_alphaArgs, 'select' | 'include'>): Promise<number>

  /**
   * Aggregate
   */
  aggregate<T extends AggregateBlock_alphaArgs>(args: Subset<T, AggregateBlock_alphaArgs>): Promise<GetBlock_alphaAggregateType<T>>
}

/**
 * The delegate class that acts as a "Promise-like" for block_alpha.
 * Why is this prefixed with `Prisma__`?
 * Because we want to prevent naming conflicts as mentioned in 
 * https://github.com/prisma/prisma-client-js/issues/707
 */
export declare class Prisma__block_alphaClient<T> implements Promise<T> {
  private readonly _dmmf;
  private readonly _fetcher;
  private readonly _queryType;
  private readonly _rootField;
  private readonly _clientMethod;
  private readonly _args;
  private readonly _dataPath;
  private readonly _errorFormat;
  private readonly _measurePerformance?;
  private _isList;
  private _callsite;
  private _requestPromise?;
  constructor(_dmmf: DMMFClass, _fetcher: PrismaClientFetcher, _queryType: 'query' | 'mutation', _rootField: string, _clientMethod: string, _args: any, _dataPath: string[], _errorFormat: ErrorFormat, _measurePerformance?: boolean | undefined, _isList?: boolean);
  readonly [Symbol.toStringTag]: 'PrismaClientPromise';

  block<T extends blockArgs = {}>(args?: Subset<T, blockArgs>): CheckSelect<T, Prisma__blockClient<block | null>, Prisma__blockClient<blockGetPayload<T> | null>>;

  private get _document();
  /**
   * Attaches callbacks for the resolution and/or rejection of the Promise.
   * @param onfulfilled The callback to execute when the Promise is resolved.
   * @param onrejected The callback to execute when the Promise is rejected.
   * @returns A Promise for the completion of which ever callback is executed.
   */
  then<TResult1 = T, TResult2 = never>(onfulfilled?: ((value: T) => TResult1 | Promise<TResult1>) | undefined | null, onrejected?: ((reason: any) => TResult2 | Promise<TResult2>) | undefined | null): Promise<TResult1 | TResult2>;
  /**
   * Attaches a callback for only the rejection of the Promise.
   * @param onrejected The callback to execute when the Promise is rejected.
   * @returns A Promise for the completion of the callback.
   */
  catch<TResult = never>(onrejected?: ((reason: any) => TResult | Promise<TResult>) | undefined | null): Promise<T | TResult>;
  /**
   * Attaches a callback that is invoked when the Promise is settled (fulfilled or rejected). The
   * resolved value cannot be modified from the callback.
   * @param onfinally The callback to execute when the Promise is settled (fulfilled or rejected).
   * @returns A Promise for the completion of the callback.
   */
  finally(onfinally?: (() => void) | undefined | null): Promise<T>;
}

// Custom InputTypes

/**
 * block_alpha findOne
 */
export type FindOneblock_alphaArgs = {
  /**
   * Select specific fields to fetch from the block_alpha
  **/
  select?: block_alphaSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: block_alphaInclude | null
  /**
   * Filter, which block_alpha to fetch.
  **/
  where: block_alphaWhereUniqueInput
}


/**
 * block_alpha findMany
 */
export type FindManyblock_alphaArgs = {
  /**
   * Select specific fields to fetch from the block_alpha
  **/
  select?: block_alphaSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: block_alphaInclude | null
  /**
   * Filter, which block_alphas to fetch.
  **/
  where?: block_alphaWhereInput
  /**
   * Determine the order of the block_alphas to fetch.
  **/
  orderBy?: Enumerable<block_alphaOrderByInput>
  /**
   * Sets the position for listing block_alphas.
  **/
  cursor?: block_alphaWhereUniqueInput
  /**
   * The number of block_alphas to fetch. If negative number, it will take block_alphas before the `cursor`.
  **/
  take?: number
  /**
   * Skip the first `n` block_alphas.
  **/
  skip?: number
  distinct?: Enumerable<Block_alphaDistinctFieldEnum>
}


/**
 * block_alpha create
 */
export type block_alphaCreateArgs = {
  /**
   * Select specific fields to fetch from the block_alpha
  **/
  select?: block_alphaSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: block_alphaInclude | null
  /**
   * The data needed to create a block_alpha.
  **/
  data: block_alphaCreateInput
}


/**
 * block_alpha update
 */
export type block_alphaUpdateArgs = {
  /**
   * Select specific fields to fetch from the block_alpha
  **/
  select?: block_alphaSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: block_alphaInclude | null
  /**
   * The data needed to update a block_alpha.
  **/
  data: block_alphaUpdateInput
  /**
   * Choose, which block_alpha to update.
  **/
  where: block_alphaWhereUniqueInput
}


/**
 * block_alpha updateMany
 */
export type block_alphaUpdateManyArgs = {
  data: block_alphaUpdateManyMutationInput
  where?: block_alphaWhereInput
}


/**
 * block_alpha upsert
 */
export type block_alphaUpsertArgs = {
  /**
   * Select specific fields to fetch from the block_alpha
  **/
  select?: block_alphaSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: block_alphaInclude | null
  /**
   * The filter to search for the block_alpha to update in case it exists.
  **/
  where: block_alphaWhereUniqueInput
  /**
   * In case the block_alpha found by the `where` argument doesn't exist, create a new block_alpha with this data.
  **/
  create: block_alphaCreateInput
  /**
   * In case the block_alpha was found with the provided `where` argument, update it with this data.
  **/
  update: block_alphaUpdateInput
}


/**
 * block_alpha delete
 */
export type block_alphaDeleteArgs = {
  /**
   * Select specific fields to fetch from the block_alpha
  **/
  select?: block_alphaSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: block_alphaInclude | null
  /**
   * Filter which block_alpha to delete.
  **/
  where: block_alphaWhereUniqueInput
}


/**
 * block_alpha deleteMany
 */
export type block_alphaDeleteManyArgs = {
  where?: block_alphaWhereInput
}


/**
 * block_alpha without action
 */
export type block_alphaArgs = {
  /**
   * Select specific fields to fetch from the block_alpha
  **/
  select?: block_alphaSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: block_alphaInclude | null
}



/**
 * Model chain
 */

export type chain = {
  hash: string
}


export type AggregateChain = {
  count: number
}



export type AggregateChainArgs = {
  where?: chainWhereInput
  orderBy?: Enumerable<chainOrderByInput>
  cursor?: chainWhereUniqueInput
  take?: number
  skip?: number
  distinct?: Enumerable<ChainDistinctFieldEnum>
  count?: true
}

export type GetChainAggregateType<T extends AggregateChainArgs> = {
  [P in keyof T]: P extends 'count' ? number : never
}


    
    

export type chainSelect = {
  hash?: boolean
  operation?: boolean | FindManyoperationArgs
}

export type chainInclude = {
  operation?: boolean | FindManyoperationArgs
}

export type chainGetPayload<
  S extends boolean | null | undefined | chainArgs,
  U = keyof S
> = S extends true
  ? chain
  : S extends undefined
  ? never
  : S extends chainArgs | FindManychainArgs
  ? 'include' extends U
    ? chain  & {
      [P in TrueKeys<S['include']>]:
      P extends 'operation'
      ? Array<operationGetPayload<S['include'][P]>> : never
    }
  : 'select' extends U
    ? {
      [P in TrueKeys<S['select']>]:P extends keyof chain ? chain[P]
: 
      P extends 'operation'
      ? Array<operationGetPayload<S['select'][P]>> : never
    }
  : chain
: chain


export interface chainDelegate {
  /**
   * Find zero or one Chain.
   * @param {FindOnechainArgs} args - Arguments to find a Chain
   * @example
   * // Get one Chain
   * const chain = await prisma.chain.findOne({
   *   where: {
   *     // ... provide filter here
   *   }
   * })
  **/
  findOne<T extends FindOnechainArgs>(
    args: Subset<T, FindOnechainArgs>
  ): CheckSelect<T, Prisma__chainClient<chain | null>, Prisma__chainClient<chainGetPayload<T> | null>>
  /**
   * Find zero or more Chains.
   * @param {FindManychainArgs=} args - Arguments to filter and select certain fields only.
   * @example
   * // Get all Chains
   * const chains = await prisma.chain.findMany()
   * 
   * // Get first 10 Chains
   * const chains = await prisma.chain.findMany({ take: 10 })
   * 
   * // Only select the `hash`
   * const chainWithHashOnly = await prisma.chain.findMany({ select: { hash: true } })
   * 
  **/
  findMany<T extends FindManychainArgs>(
    args?: Subset<T, FindManychainArgs>
  ): CheckSelect<T, Promise<Array<chain>>, Promise<Array<chainGetPayload<T>>>>
  /**
   * Create a Chain.
   * @param {chainCreateArgs} args - Arguments to create a Chain.
   * @example
   * // Create one Chain
   * const Chain = await prisma.chain.create({
   *   data: {
   *     // ... data to create a Chain
   *   }
   * })
   * 
  **/
  create<T extends chainCreateArgs>(
    args: Subset<T, chainCreateArgs>
  ): CheckSelect<T, Prisma__chainClient<chain>, Prisma__chainClient<chainGetPayload<T>>>
  /**
   * Delete a Chain.
   * @param {chainDeleteArgs} args - Arguments to delete one Chain.
   * @example
   * // Delete one Chain
   * const Chain = await prisma.chain.delete({
   *   where: {
   *     // ... filter to delete one Chain
   *   }
   * })
   * 
  **/
  delete<T extends chainDeleteArgs>(
    args: Subset<T, chainDeleteArgs>
  ): CheckSelect<T, Prisma__chainClient<chain>, Prisma__chainClient<chainGetPayload<T>>>
  /**
   * Update one Chain.
   * @param {chainUpdateArgs} args - Arguments to update one Chain.
   * @example
   * // Update one Chain
   * const chain = await prisma.chain.update({
   *   where: {
   *     // ... provide filter here
   *   },
   *   data: {
   *     // ... provide data here
   *   }
   * })
   * 
  **/
  update<T extends chainUpdateArgs>(
    args: Subset<T, chainUpdateArgs>
  ): CheckSelect<T, Prisma__chainClient<chain>, Prisma__chainClient<chainGetPayload<T>>>
  /**
   * Delete zero or more Chains.
   * @param {chainDeleteManyArgs} args - Arguments to filter Chains to delete.
   * @example
   * // Delete a few Chains
   * const { count } = await prisma.chain.deleteMany({
   *   where: {
   *     // ... provide filter here
   *   }
   * })
   * 
  **/
  deleteMany<T extends chainDeleteManyArgs>(
    args: Subset<T, chainDeleteManyArgs>
  ): Promise<BatchPayload>
  /**
   * Update zero or more Chains.
   * @param {chainUpdateManyArgs} args - Arguments to update one or more rows.
   * @example
   * // Update many Chains
   * const chain = await prisma.chain.updateMany({
   *   where: {
   *     // ... provide filter here
   *   },
   *   data: {
   *     // ... provide data here
   *   }
   * })
   * 
  **/
  updateMany<T extends chainUpdateManyArgs>(
    args: Subset<T, chainUpdateManyArgs>
  ): Promise<BatchPayload>
  /**
   * Create or update one Chain.
   * @param {chainUpsertArgs} args - Arguments to update or create a Chain.
   * @example
   * // Update or create a Chain
   * const chain = await prisma.chain.upsert({
   *   create: {
   *     // ... data to create a Chain
   *   },
   *   update: {
   *     // ... in case it already exists, update
   *   },
   *   where: {
   *     // ... the filter for the Chain we want to update
   *   }
   * })
  **/
  upsert<T extends chainUpsertArgs>(
    args: Subset<T, chainUpsertArgs>
  ): CheckSelect<T, Prisma__chainClient<chain>, Prisma__chainClient<chainGetPayload<T>>>
  /**
   * Count
   */
  count(args?: Omit<FindManychainArgs, 'select' | 'include'>): Promise<number>

  /**
   * Aggregate
   */
  aggregate<T extends AggregateChainArgs>(args: Subset<T, AggregateChainArgs>): Promise<GetChainAggregateType<T>>
}

/**
 * The delegate class that acts as a "Promise-like" for chain.
 * Why is this prefixed with `Prisma__`?
 * Because we want to prevent naming conflicts as mentioned in 
 * https://github.com/prisma/prisma-client-js/issues/707
 */
export declare class Prisma__chainClient<T> implements Promise<T> {
  private readonly _dmmf;
  private readonly _fetcher;
  private readonly _queryType;
  private readonly _rootField;
  private readonly _clientMethod;
  private readonly _args;
  private readonly _dataPath;
  private readonly _errorFormat;
  private readonly _measurePerformance?;
  private _isList;
  private _callsite;
  private _requestPromise?;
  constructor(_dmmf: DMMFClass, _fetcher: PrismaClientFetcher, _queryType: 'query' | 'mutation', _rootField: string, _clientMethod: string, _args: any, _dataPath: string[], _errorFormat: ErrorFormat, _measurePerformance?: boolean | undefined, _isList?: boolean);
  readonly [Symbol.toStringTag]: 'PrismaClientPromise';

  operation<T extends FindManyoperationArgs = {}>(args?: Subset<T, FindManyoperationArgs>): CheckSelect<T, Promise<Array<operation>>, Promise<Array<operationGetPayload<T>>>>;

  private get _document();
  /**
   * Attaches callbacks for the resolution and/or rejection of the Promise.
   * @param onfulfilled The callback to execute when the Promise is resolved.
   * @param onrejected The callback to execute when the Promise is rejected.
   * @returns A Promise for the completion of which ever callback is executed.
   */
  then<TResult1 = T, TResult2 = never>(onfulfilled?: ((value: T) => TResult1 | Promise<TResult1>) | undefined | null, onrejected?: ((reason: any) => TResult2 | Promise<TResult2>) | undefined | null): Promise<TResult1 | TResult2>;
  /**
   * Attaches a callback for only the rejection of the Promise.
   * @param onrejected The callback to execute when the Promise is rejected.
   * @returns A Promise for the completion of the callback.
   */
  catch<TResult = never>(onrejected?: ((reason: any) => TResult | Promise<TResult>) | undefined | null): Promise<T | TResult>;
  /**
   * Attaches a callback that is invoked when the Promise is settled (fulfilled or rejected). The
   * resolved value cannot be modified from the callback.
   * @param onfinally The callback to execute when the Promise is settled (fulfilled or rejected).
   * @returns A Promise for the completion of the callback.
   */
  finally(onfinally?: (() => void) | undefined | null): Promise<T>;
}

// Custom InputTypes

/**
 * chain findOne
 */
export type FindOnechainArgs = {
  /**
   * Select specific fields to fetch from the chain
  **/
  select?: chainSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: chainInclude | null
  /**
   * Filter, which chain to fetch.
  **/
  where: chainWhereUniqueInput
}


/**
 * chain findMany
 */
export type FindManychainArgs = {
  /**
   * Select specific fields to fetch from the chain
  **/
  select?: chainSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: chainInclude | null
  /**
   * Filter, which chains to fetch.
  **/
  where?: chainWhereInput
  /**
   * Determine the order of the chains to fetch.
  **/
  orderBy?: Enumerable<chainOrderByInput>
  /**
   * Sets the position for listing chains.
  **/
  cursor?: chainWhereUniqueInput
  /**
   * The number of chains to fetch. If negative number, it will take chains before the `cursor`.
  **/
  take?: number
  /**
   * Skip the first `n` chains.
  **/
  skip?: number
  distinct?: Enumerable<ChainDistinctFieldEnum>
}


/**
 * chain create
 */
export type chainCreateArgs = {
  /**
   * Select specific fields to fetch from the chain
  **/
  select?: chainSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: chainInclude | null
  /**
   * The data needed to create a chain.
  **/
  data: chainCreateInput
}


/**
 * chain update
 */
export type chainUpdateArgs = {
  /**
   * Select specific fields to fetch from the chain
  **/
  select?: chainSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: chainInclude | null
  /**
   * The data needed to update a chain.
  **/
  data: chainUpdateInput
  /**
   * Choose, which chain to update.
  **/
  where: chainWhereUniqueInput
}


/**
 * chain updateMany
 */
export type chainUpdateManyArgs = {
  data: chainUpdateManyMutationInput
  where?: chainWhereInput
}


/**
 * chain upsert
 */
export type chainUpsertArgs = {
  /**
   * Select specific fields to fetch from the chain
  **/
  select?: chainSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: chainInclude | null
  /**
   * The filter to search for the chain to update in case it exists.
  **/
  where: chainWhereUniqueInput
  /**
   * In case the chain found by the `where` argument doesn't exist, create a new chain with this data.
  **/
  create: chainCreateInput
  /**
   * In case the chain was found with the provided `where` argument, update it with this data.
  **/
  update: chainUpdateInput
}


/**
 * chain delete
 */
export type chainDeleteArgs = {
  /**
   * Select specific fields to fetch from the chain
  **/
  select?: chainSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: chainInclude | null
  /**
   * Filter which chain to delete.
  **/
  where: chainWhereUniqueInput
}


/**
 * chain deleteMany
 */
export type chainDeleteManyArgs = {
  where?: chainWhereInput
}


/**
 * chain without action
 */
export type chainArgs = {
  /**
   * Select specific fields to fetch from the chain
  **/
  select?: chainSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: chainInclude | null
}



/**
 * Model contract
 */

export type contract = {
  address: string
  block_hash: string
  mgr: string | null
  delegate: string | null
  spendable: boolean
  delegatable: boolean
  credit: number | null
  preorig: string | null
  script: string | null
}


export type AggregateContract = {
  count: number
  avg: ContractAvgAggregateOutputType | null
  sum: ContractSumAggregateOutputType | null
  min: ContractMinAggregateOutputType | null
  max: ContractMaxAggregateOutputType | null
}

export type ContractAvgAggregateOutputType = {
  credit: number
}

export type ContractSumAggregateOutputType = {
  credit: number | null
}

export type ContractMinAggregateOutputType = {
  credit: number | null
}

export type ContractMaxAggregateOutputType = {
  credit: number | null
}


export type ContractAvgAggregateInputType = {
  credit?: true
}

export type ContractSumAggregateInputType = {
  credit?: true
}

export type ContractMinAggregateInputType = {
  credit?: true
}

export type ContractMaxAggregateInputType = {
  credit?: true
}

export type AggregateContractArgs = {
  where?: contractWhereInput
  orderBy?: Enumerable<contractOrderByInput>
  cursor?: contractWhereUniqueInput
  take?: number
  skip?: number
  distinct?: Enumerable<ContractDistinctFieldEnum>
  count?: true
  avg?: ContractAvgAggregateInputType
  sum?: ContractSumAggregateInputType
  min?: ContractMinAggregateInputType
  max?: ContractMaxAggregateInputType
}

export type GetContractAggregateType<T extends AggregateContractArgs> = {
  [P in keyof T]: P extends 'count' ? number : GetContractAggregateScalarType<T[P]>
}

export type GetContractAggregateScalarType<T extends any> = {
  [P in keyof T]: P extends keyof ContractAvgAggregateOutputType ? ContractAvgAggregateOutputType[P] : never
}
    
    

export type contractSelect = {
  address?: boolean
  block_hash?: boolean
  mgr?: boolean
  delegate?: boolean
  spendable?: boolean
  delegatable?: boolean
  credit?: boolean
  preorig?: boolean
  script?: boolean
  addresses_addressesTocontract_address?: boolean | addressesArgs
  block?: boolean | blockArgs
  addresses_addressesTocontract_delegate?: boolean | addressesArgs
  addresses_addressesTocontract_mgr?: boolean | addressesArgs
  implicit?: boolean | implicitArgs
  addresses_addressesTocontract_preorig?: boolean | addressesArgs
  contract?: boolean | contractArgs
  other_contract?: boolean | FindManycontractArgs
  delegated_contract?: boolean | FindManydelegated_contractArgs
  delegation?: boolean | FindManydelegationArgs
  origination_contractToorigination_k?: boolean | FindManyoriginationArgs
  origination_contractToorigination_source?: boolean | FindManyoriginationArgs
  tx_contractTotx_destination?: boolean | FindManytxArgs
  tx_contractTotx_source?: boolean | FindManytxArgs
}

export type contractInclude = {
  addresses_addressesTocontract_address?: boolean | addressesArgs
  block?: boolean | blockArgs
  addresses_addressesTocontract_delegate?: boolean | addressesArgs
  addresses_addressesTocontract_mgr?: boolean | addressesArgs
  implicit?: boolean | implicitArgs
  addresses_addressesTocontract_preorig?: boolean | addressesArgs
  contract?: boolean | contractArgs
  other_contract?: boolean | FindManycontractArgs
  delegated_contract?: boolean | FindManydelegated_contractArgs
  delegation?: boolean | FindManydelegationArgs
  origination_contractToorigination_k?: boolean | FindManyoriginationArgs
  origination_contractToorigination_source?: boolean | FindManyoriginationArgs
  tx_contractTotx_destination?: boolean | FindManytxArgs
  tx_contractTotx_source?: boolean | FindManytxArgs
}

export type contractGetPayload<
  S extends boolean | null | undefined | contractArgs,
  U = keyof S
> = S extends true
  ? contract
  : S extends undefined
  ? never
  : S extends contractArgs | FindManycontractArgs
  ? 'include' extends U
    ? contract  & {
      [P in TrueKeys<S['include']>]:
      P extends 'addresses_addressesTocontract_address'
      ? addressesGetPayload<S['include'][P]> :
      P extends 'block'
      ? blockGetPayload<S['include'][P]> :
      P extends 'addresses_addressesTocontract_delegate'
      ? addressesGetPayload<S['include'][P]> | null :
      P extends 'addresses_addressesTocontract_mgr'
      ? addressesGetPayload<S['include'][P]> | null :
      P extends 'implicit'
      ? implicitGetPayload<S['include'][P]> | null :
      P extends 'addresses_addressesTocontract_preorig'
      ? addressesGetPayload<S['include'][P]> | null :
      P extends 'contract'
      ? contractGetPayload<S['include'][P]> | null :
      P extends 'other_contract'
      ? Array<contractGetPayload<S['include'][P]>> :
      P extends 'delegated_contract'
      ? Array<delegated_contractGetPayload<S['include'][P]>> :
      P extends 'delegation'
      ? Array<delegationGetPayload<S['include'][P]>> :
      P extends 'origination_contractToorigination_k'
      ? Array<originationGetPayload<S['include'][P]>> :
      P extends 'origination_contractToorigination_source'
      ? Array<originationGetPayload<S['include'][P]>> :
      P extends 'tx_contractTotx_destination'
      ? Array<txGetPayload<S['include'][P]>> :
      P extends 'tx_contractTotx_source'
      ? Array<txGetPayload<S['include'][P]>> : never
    }
  : 'select' extends U
    ? {
      [P in TrueKeys<S['select']>]:P extends keyof contract ? contract[P]
: 
      P extends 'addresses_addressesTocontract_address'
      ? addressesGetPayload<S['select'][P]> :
      P extends 'block'
      ? blockGetPayload<S['select'][P]> :
      P extends 'addresses_addressesTocontract_delegate'
      ? addressesGetPayload<S['select'][P]> | null :
      P extends 'addresses_addressesTocontract_mgr'
      ? addressesGetPayload<S['select'][P]> | null :
      P extends 'implicit'
      ? implicitGetPayload<S['select'][P]> | null :
      P extends 'addresses_addressesTocontract_preorig'
      ? addressesGetPayload<S['select'][P]> | null :
      P extends 'contract'
      ? contractGetPayload<S['select'][P]> | null :
      P extends 'other_contract'
      ? Array<contractGetPayload<S['select'][P]>> :
      P extends 'delegated_contract'
      ? Array<delegated_contractGetPayload<S['select'][P]>> :
      P extends 'delegation'
      ? Array<delegationGetPayload<S['select'][P]>> :
      P extends 'origination_contractToorigination_k'
      ? Array<originationGetPayload<S['select'][P]>> :
      P extends 'origination_contractToorigination_source'
      ? Array<originationGetPayload<S['select'][P]>> :
      P extends 'tx_contractTotx_destination'
      ? Array<txGetPayload<S['select'][P]>> :
      P extends 'tx_contractTotx_source'
      ? Array<txGetPayload<S['select'][P]>> : never
    }
  : contract
: contract


export interface contractDelegate {
  /**
   * Find zero or one Contract.
   * @param {FindOnecontractArgs} args - Arguments to find a Contract
   * @example
   * // Get one Contract
   * const contract = await prisma.contract.findOne({
   *   where: {
   *     // ... provide filter here
   *   }
   * })
  **/
  findOne<T extends FindOnecontractArgs>(
    args: Subset<T, FindOnecontractArgs>
  ): CheckSelect<T, Prisma__contractClient<contract | null>, Prisma__contractClient<contractGetPayload<T> | null>>
  /**
   * Find zero or more Contracts.
   * @param {FindManycontractArgs=} args - Arguments to filter and select certain fields only.
   * @example
   * // Get all Contracts
   * const contracts = await prisma.contract.findMany()
   * 
   * // Get first 10 Contracts
   * const contracts = await prisma.contract.findMany({ take: 10 })
   * 
   * // Only select the `address`
   * const contractWithAddressOnly = await prisma.contract.findMany({ select: { address: true } })
   * 
  **/
  findMany<T extends FindManycontractArgs>(
    args?: Subset<T, FindManycontractArgs>
  ): CheckSelect<T, Promise<Array<contract>>, Promise<Array<contractGetPayload<T>>>>
  /**
   * Create a Contract.
   * @param {contractCreateArgs} args - Arguments to create a Contract.
   * @example
   * // Create one Contract
   * const Contract = await prisma.contract.create({
   *   data: {
   *     // ... data to create a Contract
   *   }
   * })
   * 
  **/
  create<T extends contractCreateArgs>(
    args: Subset<T, contractCreateArgs>
  ): CheckSelect<T, Prisma__contractClient<contract>, Prisma__contractClient<contractGetPayload<T>>>
  /**
   * Delete a Contract.
   * @param {contractDeleteArgs} args - Arguments to delete one Contract.
   * @example
   * // Delete one Contract
   * const Contract = await prisma.contract.delete({
   *   where: {
   *     // ... filter to delete one Contract
   *   }
   * })
   * 
  **/
  delete<T extends contractDeleteArgs>(
    args: Subset<T, contractDeleteArgs>
  ): CheckSelect<T, Prisma__contractClient<contract>, Prisma__contractClient<contractGetPayload<T>>>
  /**
   * Update one Contract.
   * @param {contractUpdateArgs} args - Arguments to update one Contract.
   * @example
   * // Update one Contract
   * const contract = await prisma.contract.update({
   *   where: {
   *     // ... provide filter here
   *   },
   *   data: {
   *     // ... provide data here
   *   }
   * })
   * 
  **/
  update<T extends contractUpdateArgs>(
    args: Subset<T, contractUpdateArgs>
  ): CheckSelect<T, Prisma__contractClient<contract>, Prisma__contractClient<contractGetPayload<T>>>
  /**
   * Delete zero or more Contracts.
   * @param {contractDeleteManyArgs} args - Arguments to filter Contracts to delete.
   * @example
   * // Delete a few Contracts
   * const { count } = await prisma.contract.deleteMany({
   *   where: {
   *     // ... provide filter here
   *   }
   * })
   * 
  **/
  deleteMany<T extends contractDeleteManyArgs>(
    args: Subset<T, contractDeleteManyArgs>
  ): Promise<BatchPayload>
  /**
   * Update zero or more Contracts.
   * @param {contractUpdateManyArgs} args - Arguments to update one or more rows.
   * @example
   * // Update many Contracts
   * const contract = await prisma.contract.updateMany({
   *   where: {
   *     // ... provide filter here
   *   },
   *   data: {
   *     // ... provide data here
   *   }
   * })
   * 
  **/
  updateMany<T extends contractUpdateManyArgs>(
    args: Subset<T, contractUpdateManyArgs>
  ): Promise<BatchPayload>
  /**
   * Create or update one Contract.
   * @param {contractUpsertArgs} args - Arguments to update or create a Contract.
   * @example
   * // Update or create a Contract
   * const contract = await prisma.contract.upsert({
   *   create: {
   *     // ... data to create a Contract
   *   },
   *   update: {
   *     // ... in case it already exists, update
   *   },
   *   where: {
   *     // ... the filter for the Contract we want to update
   *   }
   * })
  **/
  upsert<T extends contractUpsertArgs>(
    args: Subset<T, contractUpsertArgs>
  ): CheckSelect<T, Prisma__contractClient<contract>, Prisma__contractClient<contractGetPayload<T>>>
  /**
   * Count
   */
  count(args?: Omit<FindManycontractArgs, 'select' | 'include'>): Promise<number>

  /**
   * Aggregate
   */
  aggregate<T extends AggregateContractArgs>(args: Subset<T, AggregateContractArgs>): Promise<GetContractAggregateType<T>>
}

/**
 * The delegate class that acts as a "Promise-like" for contract.
 * Why is this prefixed with `Prisma__`?
 * Because we want to prevent naming conflicts as mentioned in 
 * https://github.com/prisma/prisma-client-js/issues/707
 */
export declare class Prisma__contractClient<T> implements Promise<T> {
  private readonly _dmmf;
  private readonly _fetcher;
  private readonly _queryType;
  private readonly _rootField;
  private readonly _clientMethod;
  private readonly _args;
  private readonly _dataPath;
  private readonly _errorFormat;
  private readonly _measurePerformance?;
  private _isList;
  private _callsite;
  private _requestPromise?;
  constructor(_dmmf: DMMFClass, _fetcher: PrismaClientFetcher, _queryType: 'query' | 'mutation', _rootField: string, _clientMethod: string, _args: any, _dataPath: string[], _errorFormat: ErrorFormat, _measurePerformance?: boolean | undefined, _isList?: boolean);
  readonly [Symbol.toStringTag]: 'PrismaClientPromise';

  addresses_addressesTocontract_address<T extends addressesArgs = {}>(args?: Subset<T, addressesArgs>): CheckSelect<T, Prisma__addressesClient<addresses | null>, Prisma__addressesClient<addressesGetPayload<T> | null>>;

  block<T extends blockArgs = {}>(args?: Subset<T, blockArgs>): CheckSelect<T, Prisma__blockClient<block | null>, Prisma__blockClient<blockGetPayload<T> | null>>;

  addresses_addressesTocontract_delegate<T extends addressesArgs = {}>(args?: Subset<T, addressesArgs>): CheckSelect<T, Prisma__addressesClient<addresses | null>, Prisma__addressesClient<addressesGetPayload<T> | null>>;

  addresses_addressesTocontract_mgr<T extends addressesArgs = {}>(args?: Subset<T, addressesArgs>): CheckSelect<T, Prisma__addressesClient<addresses | null>, Prisma__addressesClient<addressesGetPayload<T> | null>>;

  implicit<T extends implicitArgs = {}>(args?: Subset<T, implicitArgs>): CheckSelect<T, Prisma__implicitClient<implicit | null>, Prisma__implicitClient<implicitGetPayload<T> | null>>;

  addresses_addressesTocontract_preorig<T extends addressesArgs = {}>(args?: Subset<T, addressesArgs>): CheckSelect<T, Prisma__addressesClient<addresses | null>, Prisma__addressesClient<addressesGetPayload<T> | null>>;

  contract<T extends contractArgs = {}>(args?: Subset<T, contractArgs>): CheckSelect<T, Prisma__contractClient<contract | null>, Prisma__contractClient<contractGetPayload<T> | null>>;

  other_contract<T extends FindManycontractArgs = {}>(args?: Subset<T, FindManycontractArgs>): CheckSelect<T, Promise<Array<contract>>, Promise<Array<contractGetPayload<T>>>>;

  delegated_contract<T extends FindManydelegated_contractArgs = {}>(args?: Subset<T, FindManydelegated_contractArgs>): CheckSelect<T, Promise<Array<delegated_contract>>, Promise<Array<delegated_contractGetPayload<T>>>>;

  delegation<T extends FindManydelegationArgs = {}>(args?: Subset<T, FindManydelegationArgs>): CheckSelect<T, Promise<Array<delegation>>, Promise<Array<delegationGetPayload<T>>>>;

  origination_contractToorigination_k<T extends FindManyoriginationArgs = {}>(args?: Subset<T, FindManyoriginationArgs>): CheckSelect<T, Promise<Array<origination>>, Promise<Array<originationGetPayload<T>>>>;

  origination_contractToorigination_source<T extends FindManyoriginationArgs = {}>(args?: Subset<T, FindManyoriginationArgs>): CheckSelect<T, Promise<Array<origination>>, Promise<Array<originationGetPayload<T>>>>;

  tx_contractTotx_destination<T extends FindManytxArgs = {}>(args?: Subset<T, FindManytxArgs>): CheckSelect<T, Promise<Array<tx>>, Promise<Array<txGetPayload<T>>>>;

  tx_contractTotx_source<T extends FindManytxArgs = {}>(args?: Subset<T, FindManytxArgs>): CheckSelect<T, Promise<Array<tx>>, Promise<Array<txGetPayload<T>>>>;

  private get _document();
  /**
   * Attaches callbacks for the resolution and/or rejection of the Promise.
   * @param onfulfilled The callback to execute when the Promise is resolved.
   * @param onrejected The callback to execute when the Promise is rejected.
   * @returns A Promise for the completion of which ever callback is executed.
   */
  then<TResult1 = T, TResult2 = never>(onfulfilled?: ((value: T) => TResult1 | Promise<TResult1>) | undefined | null, onrejected?: ((reason: any) => TResult2 | Promise<TResult2>) | undefined | null): Promise<TResult1 | TResult2>;
  /**
   * Attaches a callback for only the rejection of the Promise.
   * @param onrejected The callback to execute when the Promise is rejected.
   * @returns A Promise for the completion of the callback.
   */
  catch<TResult = never>(onrejected?: ((reason: any) => TResult | Promise<TResult>) | undefined | null): Promise<T | TResult>;
  /**
   * Attaches a callback that is invoked when the Promise is settled (fulfilled or rejected). The
   * resolved value cannot be modified from the callback.
   * @param onfinally The callback to execute when the Promise is settled (fulfilled or rejected).
   * @returns A Promise for the completion of the callback.
   */
  finally(onfinally?: (() => void) | undefined | null): Promise<T>;
}

// Custom InputTypes

/**
 * contract findOne
 */
export type FindOnecontractArgs = {
  /**
   * Select specific fields to fetch from the contract
  **/
  select?: contractSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: contractInclude | null
  /**
   * Filter, which contract to fetch.
  **/
  where: contractWhereUniqueInput
}


/**
 * contract findMany
 */
export type FindManycontractArgs = {
  /**
   * Select specific fields to fetch from the contract
  **/
  select?: contractSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: contractInclude | null
  /**
   * Filter, which contracts to fetch.
  **/
  where?: contractWhereInput
  /**
   * Determine the order of the contracts to fetch.
  **/
  orderBy?: Enumerable<contractOrderByInput>
  /**
   * Sets the position for listing contracts.
  **/
  cursor?: contractWhereUniqueInput
  /**
   * The number of contracts to fetch. If negative number, it will take contracts before the `cursor`.
  **/
  take?: number
  /**
   * Skip the first `n` contracts.
  **/
  skip?: number
  distinct?: Enumerable<ContractDistinctFieldEnum>
}


/**
 * contract create
 */
export type contractCreateArgs = {
  /**
   * Select specific fields to fetch from the contract
  **/
  select?: contractSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: contractInclude | null
  /**
   * The data needed to create a contract.
  **/
  data: contractCreateInput
}


/**
 * contract update
 */
export type contractUpdateArgs = {
  /**
   * Select specific fields to fetch from the contract
  **/
  select?: contractSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: contractInclude | null
  /**
   * The data needed to update a contract.
  **/
  data: contractUpdateInput
  /**
   * Choose, which contract to update.
  **/
  where: contractWhereUniqueInput
}


/**
 * contract updateMany
 */
export type contractUpdateManyArgs = {
  data: contractUpdateManyMutationInput
  where?: contractWhereInput
}


/**
 * contract upsert
 */
export type contractUpsertArgs = {
  /**
   * Select specific fields to fetch from the contract
  **/
  select?: contractSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: contractInclude | null
  /**
   * The filter to search for the contract to update in case it exists.
  **/
  where: contractWhereUniqueInput
  /**
   * In case the contract found by the `where` argument doesn't exist, create a new contract with this data.
  **/
  create: contractCreateInput
  /**
   * In case the contract was found with the provided `where` argument, update it with this data.
  **/
  update: contractUpdateInput
}


/**
 * contract delete
 */
export type contractDeleteArgs = {
  /**
   * Select specific fields to fetch from the contract
  **/
  select?: contractSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: contractInclude | null
  /**
   * Filter which contract to delete.
  **/
  where: contractWhereUniqueInput
}


/**
 * contract deleteMany
 */
export type contractDeleteManyArgs = {
  where?: contractWhereInput
}


/**
 * contract without action
 */
export type contractArgs = {
  /**
   * Select specific fields to fetch from the contract
  **/
  select?: contractSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: contractInclude | null
}



/**
 * Model deactivated
 */

export type deactivated = {
  pkh: string
  block_hash: string
}


export type AggregateDeactivated = {
  count: number
}



export type AggregateDeactivatedArgs = {
  where?: deactivatedWhereInput
  orderBy?: Enumerable<deactivatedOrderByInput>
  cursor?: deactivatedWhereUniqueInput
  take?: number
  skip?: number
  distinct?: Enumerable<DeactivatedDistinctFieldEnum>
  count?: true
}

export type GetDeactivatedAggregateType<T extends AggregateDeactivatedArgs> = {
  [P in keyof T]: P extends 'count' ? number : never
}


    
    

export type deactivatedSelect = {
  pkh?: boolean
  block_hash?: boolean
  block?: boolean | blockArgs
  implicit?: boolean | implicitArgs
}

export type deactivatedInclude = {
  block?: boolean | blockArgs
  implicit?: boolean | implicitArgs
}

export type deactivatedGetPayload<
  S extends boolean | null | undefined | deactivatedArgs,
  U = keyof S
> = S extends true
  ? deactivated
  : S extends undefined
  ? never
  : S extends deactivatedArgs | FindManydeactivatedArgs
  ? 'include' extends U
    ? deactivated  & {
      [P in TrueKeys<S['include']>]:
      P extends 'block'
      ? blockGetPayload<S['include'][P]> :
      P extends 'implicit'
      ? implicitGetPayload<S['include'][P]> : never
    }
  : 'select' extends U
    ? {
      [P in TrueKeys<S['select']>]:P extends keyof deactivated ? deactivated[P]
: 
      P extends 'block'
      ? blockGetPayload<S['select'][P]> :
      P extends 'implicit'
      ? implicitGetPayload<S['select'][P]> : never
    }
  : deactivated
: deactivated


export interface deactivatedDelegate {
  /**
   * Find zero or one Deactivated.
   * @param {FindOnedeactivatedArgs} args - Arguments to find a Deactivated
   * @example
   * // Get one Deactivated
   * const deactivated = await prisma.deactivated.findOne({
   *   where: {
   *     // ... provide filter here
   *   }
   * })
  **/
  findOne<T extends FindOnedeactivatedArgs>(
    args: Subset<T, FindOnedeactivatedArgs>
  ): CheckSelect<T, Prisma__deactivatedClient<deactivated | null>, Prisma__deactivatedClient<deactivatedGetPayload<T> | null>>
  /**
   * Find zero or more Deactivateds.
   * @param {FindManydeactivatedArgs=} args - Arguments to filter and select certain fields only.
   * @example
   * // Get all Deactivateds
   * const deactivateds = await prisma.deactivated.findMany()
   * 
   * // Get first 10 Deactivateds
   * const deactivateds = await prisma.deactivated.findMany({ take: 10 })
   * 
   * // Only select the `pkh`
   * const deactivatedWithPkhOnly = await prisma.deactivated.findMany({ select: { pkh: true } })
   * 
  **/
  findMany<T extends FindManydeactivatedArgs>(
    args?: Subset<T, FindManydeactivatedArgs>
  ): CheckSelect<T, Promise<Array<deactivated>>, Promise<Array<deactivatedGetPayload<T>>>>
  /**
   * Create a Deactivated.
   * @param {deactivatedCreateArgs} args - Arguments to create a Deactivated.
   * @example
   * // Create one Deactivated
   * const Deactivated = await prisma.deactivated.create({
   *   data: {
   *     // ... data to create a Deactivated
   *   }
   * })
   * 
  **/
  create<T extends deactivatedCreateArgs>(
    args: Subset<T, deactivatedCreateArgs>
  ): CheckSelect<T, Prisma__deactivatedClient<deactivated>, Prisma__deactivatedClient<deactivatedGetPayload<T>>>
  /**
   * Delete a Deactivated.
   * @param {deactivatedDeleteArgs} args - Arguments to delete one Deactivated.
   * @example
   * // Delete one Deactivated
   * const Deactivated = await prisma.deactivated.delete({
   *   where: {
   *     // ... filter to delete one Deactivated
   *   }
   * })
   * 
  **/
  delete<T extends deactivatedDeleteArgs>(
    args: Subset<T, deactivatedDeleteArgs>
  ): CheckSelect<T, Prisma__deactivatedClient<deactivated>, Prisma__deactivatedClient<deactivatedGetPayload<T>>>
  /**
   * Update one Deactivated.
   * @param {deactivatedUpdateArgs} args - Arguments to update one Deactivated.
   * @example
   * // Update one Deactivated
   * const deactivated = await prisma.deactivated.update({
   *   where: {
   *     // ... provide filter here
   *   },
   *   data: {
   *     // ... provide data here
   *   }
   * })
   * 
  **/
  update<T extends deactivatedUpdateArgs>(
    args: Subset<T, deactivatedUpdateArgs>
  ): CheckSelect<T, Prisma__deactivatedClient<deactivated>, Prisma__deactivatedClient<deactivatedGetPayload<T>>>
  /**
   * Delete zero or more Deactivateds.
   * @param {deactivatedDeleteManyArgs} args - Arguments to filter Deactivateds to delete.
   * @example
   * // Delete a few Deactivateds
   * const { count } = await prisma.deactivated.deleteMany({
   *   where: {
   *     // ... provide filter here
   *   }
   * })
   * 
  **/
  deleteMany<T extends deactivatedDeleteManyArgs>(
    args: Subset<T, deactivatedDeleteManyArgs>
  ): Promise<BatchPayload>
  /**
   * Update zero or more Deactivateds.
   * @param {deactivatedUpdateManyArgs} args - Arguments to update one or more rows.
   * @example
   * // Update many Deactivateds
   * const deactivated = await prisma.deactivated.updateMany({
   *   where: {
   *     // ... provide filter here
   *   },
   *   data: {
   *     // ... provide data here
   *   }
   * })
   * 
  **/
  updateMany<T extends deactivatedUpdateManyArgs>(
    args: Subset<T, deactivatedUpdateManyArgs>
  ): Promise<BatchPayload>
  /**
   * Create or update one Deactivated.
   * @param {deactivatedUpsertArgs} args - Arguments to update or create a Deactivated.
   * @example
   * // Update or create a Deactivated
   * const deactivated = await prisma.deactivated.upsert({
   *   create: {
   *     // ... data to create a Deactivated
   *   },
   *   update: {
   *     // ... in case it already exists, update
   *   },
   *   where: {
   *     // ... the filter for the Deactivated we want to update
   *   }
   * })
  **/
  upsert<T extends deactivatedUpsertArgs>(
    args: Subset<T, deactivatedUpsertArgs>
  ): CheckSelect<T, Prisma__deactivatedClient<deactivated>, Prisma__deactivatedClient<deactivatedGetPayload<T>>>
  /**
   * Count
   */
  count(args?: Omit<FindManydeactivatedArgs, 'select' | 'include'>): Promise<number>

  /**
   * Aggregate
   */
  aggregate<T extends AggregateDeactivatedArgs>(args: Subset<T, AggregateDeactivatedArgs>): Promise<GetDeactivatedAggregateType<T>>
}

/**
 * The delegate class that acts as a "Promise-like" for deactivated.
 * Why is this prefixed with `Prisma__`?
 * Because we want to prevent naming conflicts as mentioned in 
 * https://github.com/prisma/prisma-client-js/issues/707
 */
export declare class Prisma__deactivatedClient<T> implements Promise<T> {
  private readonly _dmmf;
  private readonly _fetcher;
  private readonly _queryType;
  private readonly _rootField;
  private readonly _clientMethod;
  private readonly _args;
  private readonly _dataPath;
  private readonly _errorFormat;
  private readonly _measurePerformance?;
  private _isList;
  private _callsite;
  private _requestPromise?;
  constructor(_dmmf: DMMFClass, _fetcher: PrismaClientFetcher, _queryType: 'query' | 'mutation', _rootField: string, _clientMethod: string, _args: any, _dataPath: string[], _errorFormat: ErrorFormat, _measurePerformance?: boolean | undefined, _isList?: boolean);
  readonly [Symbol.toStringTag]: 'PrismaClientPromise';

  block<T extends blockArgs = {}>(args?: Subset<T, blockArgs>): CheckSelect<T, Prisma__blockClient<block | null>, Prisma__blockClient<blockGetPayload<T> | null>>;

  implicit<T extends implicitArgs = {}>(args?: Subset<T, implicitArgs>): CheckSelect<T, Prisma__implicitClient<implicit | null>, Prisma__implicitClient<implicitGetPayload<T> | null>>;

  private get _document();
  /**
   * Attaches callbacks for the resolution and/or rejection of the Promise.
   * @param onfulfilled The callback to execute when the Promise is resolved.
   * @param onrejected The callback to execute when the Promise is rejected.
   * @returns A Promise for the completion of which ever callback is executed.
   */
  then<TResult1 = T, TResult2 = never>(onfulfilled?: ((value: T) => TResult1 | Promise<TResult1>) | undefined | null, onrejected?: ((reason: any) => TResult2 | Promise<TResult2>) | undefined | null): Promise<TResult1 | TResult2>;
  /**
   * Attaches a callback for only the rejection of the Promise.
   * @param onrejected The callback to execute when the Promise is rejected.
   * @returns A Promise for the completion of the callback.
   */
  catch<TResult = never>(onrejected?: ((reason: any) => TResult | Promise<TResult>) | undefined | null): Promise<T | TResult>;
  /**
   * Attaches a callback that is invoked when the Promise is settled (fulfilled or rejected). The
   * resolved value cannot be modified from the callback.
   * @param onfinally The callback to execute when the Promise is settled (fulfilled or rejected).
   * @returns A Promise for the completion of the callback.
   */
  finally(onfinally?: (() => void) | undefined | null): Promise<T>;
}

// Custom InputTypes

/**
 * deactivated findOne
 */
export type FindOnedeactivatedArgs = {
  /**
   * Select specific fields to fetch from the deactivated
  **/
  select?: deactivatedSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: deactivatedInclude | null
  /**
   * Filter, which deactivated to fetch.
  **/
  where: deactivatedWhereUniqueInput
}


/**
 * deactivated findMany
 */
export type FindManydeactivatedArgs = {
  /**
   * Select specific fields to fetch from the deactivated
  **/
  select?: deactivatedSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: deactivatedInclude | null
  /**
   * Filter, which deactivateds to fetch.
  **/
  where?: deactivatedWhereInput
  /**
   * Determine the order of the deactivateds to fetch.
  **/
  orderBy?: Enumerable<deactivatedOrderByInput>
  /**
   * Sets the position for listing deactivateds.
  **/
  cursor?: deactivatedWhereUniqueInput
  /**
   * The number of deactivateds to fetch. If negative number, it will take deactivateds before the `cursor`.
  **/
  take?: number
  /**
   * Skip the first `n` deactivateds.
  **/
  skip?: number
  distinct?: Enumerable<DeactivatedDistinctFieldEnum>
}


/**
 * deactivated create
 */
export type deactivatedCreateArgs = {
  /**
   * Select specific fields to fetch from the deactivated
  **/
  select?: deactivatedSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: deactivatedInclude | null
  /**
   * The data needed to create a deactivated.
  **/
  data: deactivatedCreateInput
}


/**
 * deactivated update
 */
export type deactivatedUpdateArgs = {
  /**
   * Select specific fields to fetch from the deactivated
  **/
  select?: deactivatedSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: deactivatedInclude | null
  /**
   * The data needed to update a deactivated.
  **/
  data: deactivatedUpdateInput
  /**
   * Choose, which deactivated to update.
  **/
  where: deactivatedWhereUniqueInput
}


/**
 * deactivated updateMany
 */
export type deactivatedUpdateManyArgs = {
  data: deactivatedUpdateManyMutationInput
  where?: deactivatedWhereInput
}


/**
 * deactivated upsert
 */
export type deactivatedUpsertArgs = {
  /**
   * Select specific fields to fetch from the deactivated
  **/
  select?: deactivatedSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: deactivatedInclude | null
  /**
   * The filter to search for the deactivated to update in case it exists.
  **/
  where: deactivatedWhereUniqueInput
  /**
   * In case the deactivated found by the `where` argument doesn't exist, create a new deactivated with this data.
  **/
  create: deactivatedCreateInput
  /**
   * In case the deactivated was found with the provided `where` argument, update it with this data.
  **/
  update: deactivatedUpdateInput
}


/**
 * deactivated delete
 */
export type deactivatedDeleteArgs = {
  /**
   * Select specific fields to fetch from the deactivated
  **/
  select?: deactivatedSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: deactivatedInclude | null
  /**
   * Filter which deactivated to delete.
  **/
  where: deactivatedWhereUniqueInput
}


/**
 * deactivated deleteMany
 */
export type deactivatedDeleteManyArgs = {
  where?: deactivatedWhereInput
}


/**
 * deactivated without action
 */
export type deactivatedArgs = {
  /**
   * Select specific fields to fetch from the deactivated
  **/
  select?: deactivatedSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: deactivatedInclude | null
}



/**
 * Model delegated_contract
 */

export type delegated_contract = {
  delegate: string
  delegator: string
  cycle: number
  level: number
}


export type AggregateDelegated_contract = {
  count: number
  avg: Delegated_contractAvgAggregateOutputType | null
  sum: Delegated_contractSumAggregateOutputType | null
  min: Delegated_contractMinAggregateOutputType | null
  max: Delegated_contractMaxAggregateOutputType | null
}

export type Delegated_contractAvgAggregateOutputType = {
  cycle: number
  level: number
}

export type Delegated_contractSumAggregateOutputType = {
  cycle: number
  level: number
}

export type Delegated_contractMinAggregateOutputType = {
  cycle: number
  level: number
}

export type Delegated_contractMaxAggregateOutputType = {
  cycle: number
  level: number
}


export type Delegated_contractAvgAggregateInputType = {
  cycle?: true
  level?: true
}

export type Delegated_contractSumAggregateInputType = {
  cycle?: true
  level?: true
}

export type Delegated_contractMinAggregateInputType = {
  cycle?: true
  level?: true
}

export type Delegated_contractMaxAggregateInputType = {
  cycle?: true
  level?: true
}

export type AggregateDelegated_contractArgs = {
  where?: delegated_contractWhereInput
  orderBy?: Enumerable<delegated_contractOrderByInput>
  cursor?: delegated_contractWhereUniqueInput
  take?: number
  skip?: number
  distinct?: Enumerable<Delegated_contractDistinctFieldEnum>
  count?: true
  avg?: Delegated_contractAvgAggregateInputType
  sum?: Delegated_contractSumAggregateInputType
  min?: Delegated_contractMinAggregateInputType
  max?: Delegated_contractMaxAggregateInputType
}

export type GetDelegated_contractAggregateType<T extends AggregateDelegated_contractArgs> = {
  [P in keyof T]: P extends 'count' ? number : GetDelegated_contractAggregateScalarType<T[P]>
}

export type GetDelegated_contractAggregateScalarType<T extends any> = {
  [P in keyof T]: P extends keyof Delegated_contractAvgAggregateOutputType ? Delegated_contractAvgAggregateOutputType[P] : never
}
    
    

export type delegated_contractSelect = {
  delegate?: boolean
  delegator?: boolean
  cycle?: boolean
  level?: boolean
  snapshot?: boolean | snapshotArgs
  implicit?: boolean | implicitArgs
  contract?: boolean | contractArgs
}

export type delegated_contractInclude = {
  snapshot?: boolean | snapshotArgs
  implicit?: boolean | implicitArgs
  contract?: boolean | contractArgs
}

export type delegated_contractGetPayload<
  S extends boolean | null | undefined | delegated_contractArgs,
  U = keyof S
> = S extends true
  ? delegated_contract
  : S extends undefined
  ? never
  : S extends delegated_contractArgs | FindManydelegated_contractArgs
  ? 'include' extends U
    ? delegated_contract  & {
      [P in TrueKeys<S['include']>]:
      P extends 'snapshot'
      ? snapshotGetPayload<S['include'][P]> :
      P extends 'implicit'
      ? implicitGetPayload<S['include'][P]> :
      P extends 'contract'
      ? contractGetPayload<S['include'][P]> : never
    }
  : 'select' extends U
    ? {
      [P in TrueKeys<S['select']>]:P extends keyof delegated_contract ? delegated_contract[P]
: 
      P extends 'snapshot'
      ? snapshotGetPayload<S['select'][P]> :
      P extends 'implicit'
      ? implicitGetPayload<S['select'][P]> :
      P extends 'contract'
      ? contractGetPayload<S['select'][P]> : never
    }
  : delegated_contract
: delegated_contract


export interface delegated_contractDelegate {
  /**
   * Find zero or one Delegated_contract.
   * @param {FindOnedelegated_contractArgs} args - Arguments to find a Delegated_contract
   * @example
   * // Get one Delegated_contract
   * const delegated_contract = await prisma.delegated_contract.findOne({
   *   where: {
   *     // ... provide filter here
   *   }
   * })
  **/
  findOne<T extends FindOnedelegated_contractArgs>(
    args: Subset<T, FindOnedelegated_contractArgs>
  ): CheckSelect<T, Prisma__delegated_contractClient<delegated_contract | null>, Prisma__delegated_contractClient<delegated_contractGetPayload<T> | null>>
  /**
   * Find zero or more Delegated_contracts.
   * @param {FindManydelegated_contractArgs=} args - Arguments to filter and select certain fields only.
   * @example
   * // Get all Delegated_contracts
   * const delegated_contracts = await prisma.delegated_contract.findMany()
   * 
   * // Get first 10 Delegated_contracts
   * const delegated_contracts = await prisma.delegated_contract.findMany({ take: 10 })
   * 
   * // Only select the `delegate`
   * const delegated_contractWithDelegateOnly = await prisma.delegated_contract.findMany({ select: { delegate: true } })
   * 
  **/
  findMany<T extends FindManydelegated_contractArgs>(
    args?: Subset<T, FindManydelegated_contractArgs>
  ): CheckSelect<T, Promise<Array<delegated_contract>>, Promise<Array<delegated_contractGetPayload<T>>>>
  /**
   * Create a Delegated_contract.
   * @param {delegated_contractCreateArgs} args - Arguments to create a Delegated_contract.
   * @example
   * // Create one Delegated_contract
   * const Delegated_contract = await prisma.delegated_contract.create({
   *   data: {
   *     // ... data to create a Delegated_contract
   *   }
   * })
   * 
  **/
  create<T extends delegated_contractCreateArgs>(
    args: Subset<T, delegated_contractCreateArgs>
  ): CheckSelect<T, Prisma__delegated_contractClient<delegated_contract>, Prisma__delegated_contractClient<delegated_contractGetPayload<T>>>
  /**
   * Delete a Delegated_contract.
   * @param {delegated_contractDeleteArgs} args - Arguments to delete one Delegated_contract.
   * @example
   * // Delete one Delegated_contract
   * const Delegated_contract = await prisma.delegated_contract.delete({
   *   where: {
   *     // ... filter to delete one Delegated_contract
   *   }
   * })
   * 
  **/
  delete<T extends delegated_contractDeleteArgs>(
    args: Subset<T, delegated_contractDeleteArgs>
  ): CheckSelect<T, Prisma__delegated_contractClient<delegated_contract>, Prisma__delegated_contractClient<delegated_contractGetPayload<T>>>
  /**
   * Update one Delegated_contract.
   * @param {delegated_contractUpdateArgs} args - Arguments to update one Delegated_contract.
   * @example
   * // Update one Delegated_contract
   * const delegated_contract = await prisma.delegated_contract.update({
   *   where: {
   *     // ... provide filter here
   *   },
   *   data: {
   *     // ... provide data here
   *   }
   * })
   * 
  **/
  update<T extends delegated_contractUpdateArgs>(
    args: Subset<T, delegated_contractUpdateArgs>
  ): CheckSelect<T, Prisma__delegated_contractClient<delegated_contract>, Prisma__delegated_contractClient<delegated_contractGetPayload<T>>>
  /**
   * Delete zero or more Delegated_contracts.
   * @param {delegated_contractDeleteManyArgs} args - Arguments to filter Delegated_contracts to delete.
   * @example
   * // Delete a few Delegated_contracts
   * const { count } = await prisma.delegated_contract.deleteMany({
   *   where: {
   *     // ... provide filter here
   *   }
   * })
   * 
  **/
  deleteMany<T extends delegated_contractDeleteManyArgs>(
    args: Subset<T, delegated_contractDeleteManyArgs>
  ): Promise<BatchPayload>
  /**
   * Update zero or more Delegated_contracts.
   * @param {delegated_contractUpdateManyArgs} args - Arguments to update one or more rows.
   * @example
   * // Update many Delegated_contracts
   * const delegated_contract = await prisma.delegated_contract.updateMany({
   *   where: {
   *     // ... provide filter here
   *   },
   *   data: {
   *     // ... provide data here
   *   }
   * })
   * 
  **/
  updateMany<T extends delegated_contractUpdateManyArgs>(
    args: Subset<T, delegated_contractUpdateManyArgs>
  ): Promise<BatchPayload>
  /**
   * Create or update one Delegated_contract.
   * @param {delegated_contractUpsertArgs} args - Arguments to update or create a Delegated_contract.
   * @example
   * // Update or create a Delegated_contract
   * const delegated_contract = await prisma.delegated_contract.upsert({
   *   create: {
   *     // ... data to create a Delegated_contract
   *   },
   *   update: {
   *     // ... in case it already exists, update
   *   },
   *   where: {
   *     // ... the filter for the Delegated_contract we want to update
   *   }
   * })
  **/
  upsert<T extends delegated_contractUpsertArgs>(
    args: Subset<T, delegated_contractUpsertArgs>
  ): CheckSelect<T, Prisma__delegated_contractClient<delegated_contract>, Prisma__delegated_contractClient<delegated_contractGetPayload<T>>>
  /**
   * Count
   */
  count(args?: Omit<FindManydelegated_contractArgs, 'select' | 'include'>): Promise<number>

  /**
   * Aggregate
   */
  aggregate<T extends AggregateDelegated_contractArgs>(args: Subset<T, AggregateDelegated_contractArgs>): Promise<GetDelegated_contractAggregateType<T>>
}

/**
 * The delegate class that acts as a "Promise-like" for delegated_contract.
 * Why is this prefixed with `Prisma__`?
 * Because we want to prevent naming conflicts as mentioned in 
 * https://github.com/prisma/prisma-client-js/issues/707
 */
export declare class Prisma__delegated_contractClient<T> implements Promise<T> {
  private readonly _dmmf;
  private readonly _fetcher;
  private readonly _queryType;
  private readonly _rootField;
  private readonly _clientMethod;
  private readonly _args;
  private readonly _dataPath;
  private readonly _errorFormat;
  private readonly _measurePerformance?;
  private _isList;
  private _callsite;
  private _requestPromise?;
  constructor(_dmmf: DMMFClass, _fetcher: PrismaClientFetcher, _queryType: 'query' | 'mutation', _rootField: string, _clientMethod: string, _args: any, _dataPath: string[], _errorFormat: ErrorFormat, _measurePerformance?: boolean | undefined, _isList?: boolean);
  readonly [Symbol.toStringTag]: 'PrismaClientPromise';

  snapshot<T extends snapshotArgs = {}>(args?: Subset<T, snapshotArgs>): CheckSelect<T, Prisma__snapshotClient<snapshot | null>, Prisma__snapshotClient<snapshotGetPayload<T> | null>>;

  implicit<T extends implicitArgs = {}>(args?: Subset<T, implicitArgs>): CheckSelect<T, Prisma__implicitClient<implicit | null>, Prisma__implicitClient<implicitGetPayload<T> | null>>;

  contract<T extends contractArgs = {}>(args?: Subset<T, contractArgs>): CheckSelect<T, Prisma__contractClient<contract | null>, Prisma__contractClient<contractGetPayload<T> | null>>;

  private get _document();
  /**
   * Attaches callbacks for the resolution and/or rejection of the Promise.
   * @param onfulfilled The callback to execute when the Promise is resolved.
   * @param onrejected The callback to execute when the Promise is rejected.
   * @returns A Promise for the completion of which ever callback is executed.
   */
  then<TResult1 = T, TResult2 = never>(onfulfilled?: ((value: T) => TResult1 | Promise<TResult1>) | undefined | null, onrejected?: ((reason: any) => TResult2 | Promise<TResult2>) | undefined | null): Promise<TResult1 | TResult2>;
  /**
   * Attaches a callback for only the rejection of the Promise.
   * @param onrejected The callback to execute when the Promise is rejected.
   * @returns A Promise for the completion of the callback.
   */
  catch<TResult = never>(onrejected?: ((reason: any) => TResult | Promise<TResult>) | undefined | null): Promise<T | TResult>;
  /**
   * Attaches a callback that is invoked when the Promise is settled (fulfilled or rejected). The
   * resolved value cannot be modified from the callback.
   * @param onfinally The callback to execute when the Promise is settled (fulfilled or rejected).
   * @returns A Promise for the completion of the callback.
   */
  finally(onfinally?: (() => void) | undefined | null): Promise<T>;
}

// Custom InputTypes

/**
 * delegated_contract findOne
 */
export type FindOnedelegated_contractArgs = {
  /**
   * Select specific fields to fetch from the delegated_contract
  **/
  select?: delegated_contractSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: delegated_contractInclude | null
  /**
   * Filter, which delegated_contract to fetch.
  **/
  where: delegated_contractWhereUniqueInput
}


/**
 * delegated_contract findMany
 */
export type FindManydelegated_contractArgs = {
  /**
   * Select specific fields to fetch from the delegated_contract
  **/
  select?: delegated_contractSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: delegated_contractInclude | null
  /**
   * Filter, which delegated_contracts to fetch.
  **/
  where?: delegated_contractWhereInput
  /**
   * Determine the order of the delegated_contracts to fetch.
  **/
  orderBy?: Enumerable<delegated_contractOrderByInput>
  /**
   * Sets the position for listing delegated_contracts.
  **/
  cursor?: delegated_contractWhereUniqueInput
  /**
   * The number of delegated_contracts to fetch. If negative number, it will take delegated_contracts before the `cursor`.
  **/
  take?: number
  /**
   * Skip the first `n` delegated_contracts.
  **/
  skip?: number
  distinct?: Enumerable<Delegated_contractDistinctFieldEnum>
}


/**
 * delegated_contract create
 */
export type delegated_contractCreateArgs = {
  /**
   * Select specific fields to fetch from the delegated_contract
  **/
  select?: delegated_contractSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: delegated_contractInclude | null
  /**
   * The data needed to create a delegated_contract.
  **/
  data: delegated_contractCreateInput
}


/**
 * delegated_contract update
 */
export type delegated_contractUpdateArgs = {
  /**
   * Select specific fields to fetch from the delegated_contract
  **/
  select?: delegated_contractSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: delegated_contractInclude | null
  /**
   * The data needed to update a delegated_contract.
  **/
  data: delegated_contractUpdateInput
  /**
   * Choose, which delegated_contract to update.
  **/
  where: delegated_contractWhereUniqueInput
}


/**
 * delegated_contract updateMany
 */
export type delegated_contractUpdateManyArgs = {
  data: delegated_contractUpdateManyMutationInput
  where?: delegated_contractWhereInput
}


/**
 * delegated_contract upsert
 */
export type delegated_contractUpsertArgs = {
  /**
   * Select specific fields to fetch from the delegated_contract
  **/
  select?: delegated_contractSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: delegated_contractInclude | null
  /**
   * The filter to search for the delegated_contract to update in case it exists.
  **/
  where: delegated_contractWhereUniqueInput
  /**
   * In case the delegated_contract found by the `where` argument doesn't exist, create a new delegated_contract with this data.
  **/
  create: delegated_contractCreateInput
  /**
   * In case the delegated_contract was found with the provided `where` argument, update it with this data.
  **/
  update: delegated_contractUpdateInput
}


/**
 * delegated_contract delete
 */
export type delegated_contractDeleteArgs = {
  /**
   * Select specific fields to fetch from the delegated_contract
  **/
  select?: delegated_contractSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: delegated_contractInclude | null
  /**
   * Filter which delegated_contract to delete.
  **/
  where: delegated_contractWhereUniqueInput
}


/**
 * delegated_contract deleteMany
 */
export type delegated_contractDeleteManyArgs = {
  where?: delegated_contractWhereInput
}


/**
 * delegated_contract without action
 */
export type delegated_contractArgs = {
  /**
   * Select specific fields to fetch from the delegated_contract
  **/
  select?: delegated_contractSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: delegated_contractInclude | null
}



/**
 * Model delegation
 */

export type delegation = {
  operation_hash: string
  op_id: number
  source: string
  pkh: string | null
  autoid: number
}


export type AggregateDelegation = {
  count: number
  avg: DelegationAvgAggregateOutputType | null
  sum: DelegationSumAggregateOutputType | null
  min: DelegationMinAggregateOutputType | null
  max: DelegationMaxAggregateOutputType | null
}

export type DelegationAvgAggregateOutputType = {
  op_id: number
  autoid: number
}

export type DelegationSumAggregateOutputType = {
  op_id: number
  autoid: number
}

export type DelegationMinAggregateOutputType = {
  op_id: number
  autoid: number
}

export type DelegationMaxAggregateOutputType = {
  op_id: number
  autoid: number
}


export type DelegationAvgAggregateInputType = {
  op_id?: true
  autoid?: true
}

export type DelegationSumAggregateInputType = {
  op_id?: true
  autoid?: true
}

export type DelegationMinAggregateInputType = {
  op_id?: true
  autoid?: true
}

export type DelegationMaxAggregateInputType = {
  op_id?: true
  autoid?: true
}

export type AggregateDelegationArgs = {
  where?: delegationWhereInput
  orderBy?: Enumerable<delegationOrderByInput>
  cursor?: delegationWhereUniqueInput
  take?: number
  skip?: number
  distinct?: Enumerable<DelegationDistinctFieldEnum>
  count?: true
  avg?: DelegationAvgAggregateInputType
  sum?: DelegationSumAggregateInputType
  min?: DelegationMinAggregateInputType
  max?: DelegationMaxAggregateInputType
}

export type GetDelegationAggregateType<T extends AggregateDelegationArgs> = {
  [P in keyof T]: P extends 'count' ? number : GetDelegationAggregateScalarType<T[P]>
}

export type GetDelegationAggregateScalarType<T extends any> = {
  [P in keyof T]: P extends keyof DelegationAvgAggregateOutputType ? DelegationAvgAggregateOutputType[P] : never
}
    
    

export type delegationSelect = {
  operation_hash?: boolean
  op_id?: boolean
  source?: boolean
  pkh?: boolean
  autoid?: boolean
  operation_alpha?: boolean | operation_alphaArgs
  addresses?: boolean | addressesArgs
  contract?: boolean | contractArgs
}

export type delegationInclude = {
  operation_alpha?: boolean | operation_alphaArgs
  addresses?: boolean | addressesArgs
  contract?: boolean | contractArgs
}

export type delegationGetPayload<
  S extends boolean | null | undefined | delegationArgs,
  U = keyof S
> = S extends true
  ? delegation
  : S extends undefined
  ? never
  : S extends delegationArgs | FindManydelegationArgs
  ? 'include' extends U
    ? delegation  & {
      [P in TrueKeys<S['include']>]:
      P extends 'operation_alpha'
      ? operation_alphaGetPayload<S['include'][P]> :
      P extends 'addresses'
      ? addressesGetPayload<S['include'][P]> | null :
      P extends 'contract'
      ? contractGetPayload<S['include'][P]> : never
    }
  : 'select' extends U
    ? {
      [P in TrueKeys<S['select']>]:P extends keyof delegation ? delegation[P]
: 
      P extends 'operation_alpha'
      ? operation_alphaGetPayload<S['select'][P]> :
      P extends 'addresses'
      ? addressesGetPayload<S['select'][P]> | null :
      P extends 'contract'
      ? contractGetPayload<S['select'][P]> : never
    }
  : delegation
: delegation


export interface delegationDelegate {
  /**
   * Find zero or one Delegation.
   * @param {FindOnedelegationArgs} args - Arguments to find a Delegation
   * @example
   * // Get one Delegation
   * const delegation = await prisma.delegation.findOne({
   *   where: {
   *     // ... provide filter here
   *   }
   * })
  **/
  findOne<T extends FindOnedelegationArgs>(
    args: Subset<T, FindOnedelegationArgs>
  ): CheckSelect<T, Prisma__delegationClient<delegation | null>, Prisma__delegationClient<delegationGetPayload<T> | null>>
  /**
   * Find zero or more Delegations.
   * @param {FindManydelegationArgs=} args - Arguments to filter and select certain fields only.
   * @example
   * // Get all Delegations
   * const delegations = await prisma.delegation.findMany()
   * 
   * // Get first 10 Delegations
   * const delegations = await prisma.delegation.findMany({ take: 10 })
   * 
   * // Only select the `operation_hash`
   * const delegationWithOperation_hashOnly = await prisma.delegation.findMany({ select: { operation_hash: true } })
   * 
  **/
  findMany<T extends FindManydelegationArgs>(
    args?: Subset<T, FindManydelegationArgs>
  ): CheckSelect<T, Promise<Array<delegation>>, Promise<Array<delegationGetPayload<T>>>>
  /**
   * Create a Delegation.
   * @param {delegationCreateArgs} args - Arguments to create a Delegation.
   * @example
   * // Create one Delegation
   * const Delegation = await prisma.delegation.create({
   *   data: {
   *     // ... data to create a Delegation
   *   }
   * })
   * 
  **/
  create<T extends delegationCreateArgs>(
    args: Subset<T, delegationCreateArgs>
  ): CheckSelect<T, Prisma__delegationClient<delegation>, Prisma__delegationClient<delegationGetPayload<T>>>
  /**
   * Delete a Delegation.
   * @param {delegationDeleteArgs} args - Arguments to delete one Delegation.
   * @example
   * // Delete one Delegation
   * const Delegation = await prisma.delegation.delete({
   *   where: {
   *     // ... filter to delete one Delegation
   *   }
   * })
   * 
  **/
  delete<T extends delegationDeleteArgs>(
    args: Subset<T, delegationDeleteArgs>
  ): CheckSelect<T, Prisma__delegationClient<delegation>, Prisma__delegationClient<delegationGetPayload<T>>>
  /**
   * Update one Delegation.
   * @param {delegationUpdateArgs} args - Arguments to update one Delegation.
   * @example
   * // Update one Delegation
   * const delegation = await prisma.delegation.update({
   *   where: {
   *     // ... provide filter here
   *   },
   *   data: {
   *     // ... provide data here
   *   }
   * })
   * 
  **/
  update<T extends delegationUpdateArgs>(
    args: Subset<T, delegationUpdateArgs>
  ): CheckSelect<T, Prisma__delegationClient<delegation>, Prisma__delegationClient<delegationGetPayload<T>>>
  /**
   * Delete zero or more Delegations.
   * @param {delegationDeleteManyArgs} args - Arguments to filter Delegations to delete.
   * @example
   * // Delete a few Delegations
   * const { count } = await prisma.delegation.deleteMany({
   *   where: {
   *     // ... provide filter here
   *   }
   * })
   * 
  **/
  deleteMany<T extends delegationDeleteManyArgs>(
    args: Subset<T, delegationDeleteManyArgs>
  ): Promise<BatchPayload>
  /**
   * Update zero or more Delegations.
   * @param {delegationUpdateManyArgs} args - Arguments to update one or more rows.
   * @example
   * // Update many Delegations
   * const delegation = await prisma.delegation.updateMany({
   *   where: {
   *     // ... provide filter here
   *   },
   *   data: {
   *     // ... provide data here
   *   }
   * })
   * 
  **/
  updateMany<T extends delegationUpdateManyArgs>(
    args: Subset<T, delegationUpdateManyArgs>
  ): Promise<BatchPayload>
  /**
   * Create or update one Delegation.
   * @param {delegationUpsertArgs} args - Arguments to update or create a Delegation.
   * @example
   * // Update or create a Delegation
   * const delegation = await prisma.delegation.upsert({
   *   create: {
   *     // ... data to create a Delegation
   *   },
   *   update: {
   *     // ... in case it already exists, update
   *   },
   *   where: {
   *     // ... the filter for the Delegation we want to update
   *   }
   * })
  **/
  upsert<T extends delegationUpsertArgs>(
    args: Subset<T, delegationUpsertArgs>
  ): CheckSelect<T, Prisma__delegationClient<delegation>, Prisma__delegationClient<delegationGetPayload<T>>>
  /**
   * Count
   */
  count(args?: Omit<FindManydelegationArgs, 'select' | 'include'>): Promise<number>

  /**
   * Aggregate
   */
  aggregate<T extends AggregateDelegationArgs>(args: Subset<T, AggregateDelegationArgs>): Promise<GetDelegationAggregateType<T>>
}

/**
 * The delegate class that acts as a "Promise-like" for delegation.
 * Why is this prefixed with `Prisma__`?
 * Because we want to prevent naming conflicts as mentioned in 
 * https://github.com/prisma/prisma-client-js/issues/707
 */
export declare class Prisma__delegationClient<T> implements Promise<T> {
  private readonly _dmmf;
  private readonly _fetcher;
  private readonly _queryType;
  private readonly _rootField;
  private readonly _clientMethod;
  private readonly _args;
  private readonly _dataPath;
  private readonly _errorFormat;
  private readonly _measurePerformance?;
  private _isList;
  private _callsite;
  private _requestPromise?;
  constructor(_dmmf: DMMFClass, _fetcher: PrismaClientFetcher, _queryType: 'query' | 'mutation', _rootField: string, _clientMethod: string, _args: any, _dataPath: string[], _errorFormat: ErrorFormat, _measurePerformance?: boolean | undefined, _isList?: boolean);
  readonly [Symbol.toStringTag]: 'PrismaClientPromise';

  operation_alpha<T extends operation_alphaArgs = {}>(args?: Subset<T, operation_alphaArgs>): CheckSelect<T, Prisma__operation_alphaClient<operation_alpha | null>, Prisma__operation_alphaClient<operation_alphaGetPayload<T> | null>>;

  addresses<T extends addressesArgs = {}>(args?: Subset<T, addressesArgs>): CheckSelect<T, Prisma__addressesClient<addresses | null>, Prisma__addressesClient<addressesGetPayload<T> | null>>;

  contract<T extends contractArgs = {}>(args?: Subset<T, contractArgs>): CheckSelect<T, Prisma__contractClient<contract | null>, Prisma__contractClient<contractGetPayload<T> | null>>;

  private get _document();
  /**
   * Attaches callbacks for the resolution and/or rejection of the Promise.
   * @param onfulfilled The callback to execute when the Promise is resolved.
   * @param onrejected The callback to execute when the Promise is rejected.
   * @returns A Promise for the completion of which ever callback is executed.
   */
  then<TResult1 = T, TResult2 = never>(onfulfilled?: ((value: T) => TResult1 | Promise<TResult1>) | undefined | null, onrejected?: ((reason: any) => TResult2 | Promise<TResult2>) | undefined | null): Promise<TResult1 | TResult2>;
  /**
   * Attaches a callback for only the rejection of the Promise.
   * @param onrejected The callback to execute when the Promise is rejected.
   * @returns A Promise for the completion of the callback.
   */
  catch<TResult = never>(onrejected?: ((reason: any) => TResult | Promise<TResult>) | undefined | null): Promise<T | TResult>;
  /**
   * Attaches a callback that is invoked when the Promise is settled (fulfilled or rejected). The
   * resolved value cannot be modified from the callback.
   * @param onfinally The callback to execute when the Promise is settled (fulfilled or rejected).
   * @returns A Promise for the completion of the callback.
   */
  finally(onfinally?: (() => void) | undefined | null): Promise<T>;
}

// Custom InputTypes

/**
 * delegation findOne
 */
export type FindOnedelegationArgs = {
  /**
   * Select specific fields to fetch from the delegation
  **/
  select?: delegationSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: delegationInclude | null
  /**
   * Filter, which delegation to fetch.
  **/
  where: delegationWhereUniqueInput
}


/**
 * delegation findMany
 */
export type FindManydelegationArgs = {
  /**
   * Select specific fields to fetch from the delegation
  **/
  select?: delegationSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: delegationInclude | null
  /**
   * Filter, which delegations to fetch.
  **/
  where?: delegationWhereInput
  /**
   * Determine the order of the delegations to fetch.
  **/
  orderBy?: Enumerable<delegationOrderByInput>
  /**
   * Sets the position for listing delegations.
  **/
  cursor?: delegationWhereUniqueInput
  /**
   * The number of delegations to fetch. If negative number, it will take delegations before the `cursor`.
  **/
  take?: number
  /**
   * Skip the first `n` delegations.
  **/
  skip?: number
  distinct?: Enumerable<DelegationDistinctFieldEnum>
}


/**
 * delegation create
 */
export type delegationCreateArgs = {
  /**
   * Select specific fields to fetch from the delegation
  **/
  select?: delegationSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: delegationInclude | null
  /**
   * The data needed to create a delegation.
  **/
  data: delegationCreateInput
}


/**
 * delegation update
 */
export type delegationUpdateArgs = {
  /**
   * Select specific fields to fetch from the delegation
  **/
  select?: delegationSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: delegationInclude | null
  /**
   * The data needed to update a delegation.
  **/
  data: delegationUpdateInput
  /**
   * Choose, which delegation to update.
  **/
  where: delegationWhereUniqueInput
}


/**
 * delegation updateMany
 */
export type delegationUpdateManyArgs = {
  data: delegationUpdateManyMutationInput
  where?: delegationWhereInput
}


/**
 * delegation upsert
 */
export type delegationUpsertArgs = {
  /**
   * Select specific fields to fetch from the delegation
  **/
  select?: delegationSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: delegationInclude | null
  /**
   * The filter to search for the delegation to update in case it exists.
  **/
  where: delegationWhereUniqueInput
  /**
   * In case the delegation found by the `where` argument doesn't exist, create a new delegation with this data.
  **/
  create: delegationCreateInput
  /**
   * In case the delegation was found with the provided `where` argument, update it with this data.
  **/
  update: delegationUpdateInput
}


/**
 * delegation delete
 */
export type delegationDeleteArgs = {
  /**
   * Select specific fields to fetch from the delegation
  **/
  select?: delegationSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: delegationInclude | null
  /**
   * Filter which delegation to delete.
  **/
  where: delegationWhereUniqueInput
}


/**
 * delegation deleteMany
 */
export type delegationDeleteManyArgs = {
  where?: delegationWhereInput
}


/**
 * delegation without action
 */
export type delegationArgs = {
  /**
   * Select specific fields to fetch from the delegation
  **/
  select?: delegationSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: delegationInclude | null
}



/**
 * Model endorsement
 */

export type endorsement = {
  block_hash: string
  op: string
  id: number
  level: number
  pkh: string
  slot: number
}


export type AggregateEndorsement = {
  count: number
  avg: EndorsementAvgAggregateOutputType | null
  sum: EndorsementSumAggregateOutputType | null
  min: EndorsementMinAggregateOutputType | null
  max: EndorsementMaxAggregateOutputType | null
}

export type EndorsementAvgAggregateOutputType = {
  id: number
  level: number
  slot: number
}

export type EndorsementSumAggregateOutputType = {
  id: number
  level: number
  slot: number
}

export type EndorsementMinAggregateOutputType = {
  id: number
  level: number
  slot: number
}

export type EndorsementMaxAggregateOutputType = {
  id: number
  level: number
  slot: number
}


export type EndorsementAvgAggregateInputType = {
  id?: true
  level?: true
  slot?: true
}

export type EndorsementSumAggregateInputType = {
  id?: true
  level?: true
  slot?: true
}

export type EndorsementMinAggregateInputType = {
  id?: true
  level?: true
  slot?: true
}

export type EndorsementMaxAggregateInputType = {
  id?: true
  level?: true
  slot?: true
}

export type AggregateEndorsementArgs = {
  where?: endorsementWhereInput
  orderBy?: Enumerable<endorsementOrderByInput>
  cursor?: endorsementWhereUniqueInput
  take?: number
  skip?: number
  distinct?: Enumerable<EndorsementDistinctFieldEnum>
  count?: true
  avg?: EndorsementAvgAggregateInputType
  sum?: EndorsementSumAggregateInputType
  min?: EndorsementMinAggregateInputType
  max?: EndorsementMaxAggregateInputType
}

export type GetEndorsementAggregateType<T extends AggregateEndorsementArgs> = {
  [P in keyof T]: P extends 'count' ? number : GetEndorsementAggregateScalarType<T[P]>
}

export type GetEndorsementAggregateScalarType<T extends any> = {
  [P in keyof T]: P extends keyof EndorsementAvgAggregateOutputType ? EndorsementAvgAggregateOutputType[P] : never
}
    
    

export type endorsementSelect = {
  block_hash?: boolean
  op?: boolean
  id?: boolean
  level?: boolean
  pkh?: boolean
  slot?: boolean
  block?: boolean | blockArgs
  operation?: boolean | operationArgs
  implicit?: boolean | implicitArgs
}

export type endorsementInclude = {
  block?: boolean | blockArgs
  operation?: boolean | operationArgs
  implicit?: boolean | implicitArgs
}

export type endorsementGetPayload<
  S extends boolean | null | undefined | endorsementArgs,
  U = keyof S
> = S extends true
  ? endorsement
  : S extends undefined
  ? never
  : S extends endorsementArgs | FindManyendorsementArgs
  ? 'include' extends U
    ? endorsement  & {
      [P in TrueKeys<S['include']>]:
      P extends 'block'
      ? blockGetPayload<S['include'][P]> :
      P extends 'operation'
      ? operationGetPayload<S['include'][P]> :
      P extends 'implicit'
      ? implicitGetPayload<S['include'][P]> : never
    }
  : 'select' extends U
    ? {
      [P in TrueKeys<S['select']>]:P extends keyof endorsement ? endorsement[P]
: 
      P extends 'block'
      ? blockGetPayload<S['select'][P]> :
      P extends 'operation'
      ? operationGetPayload<S['select'][P]> :
      P extends 'implicit'
      ? implicitGetPayload<S['select'][P]> : never
    }
  : endorsement
: endorsement


export interface endorsementDelegate {
  /**
   * Find zero or one Endorsement.
   * @param {FindOneendorsementArgs} args - Arguments to find a Endorsement
   * @example
   * // Get one Endorsement
   * const endorsement = await prisma.endorsement.findOne({
   *   where: {
   *     // ... provide filter here
   *   }
   * })
  **/
  findOne<T extends FindOneendorsementArgs>(
    args: Subset<T, FindOneendorsementArgs>
  ): CheckSelect<T, Prisma__endorsementClient<endorsement | null>, Prisma__endorsementClient<endorsementGetPayload<T> | null>>
  /**
   * Find zero or more Endorsements.
   * @param {FindManyendorsementArgs=} args - Arguments to filter and select certain fields only.
   * @example
   * // Get all Endorsements
   * const endorsements = await prisma.endorsement.findMany()
   * 
   * // Get first 10 Endorsements
   * const endorsements = await prisma.endorsement.findMany({ take: 10 })
   * 
   * // Only select the `block_hash`
   * const endorsementWithBlock_hashOnly = await prisma.endorsement.findMany({ select: { block_hash: true } })
   * 
  **/
  findMany<T extends FindManyendorsementArgs>(
    args?: Subset<T, FindManyendorsementArgs>
  ): CheckSelect<T, Promise<Array<endorsement>>, Promise<Array<endorsementGetPayload<T>>>>
  /**
   * Create a Endorsement.
   * @param {endorsementCreateArgs} args - Arguments to create a Endorsement.
   * @example
   * // Create one Endorsement
   * const Endorsement = await prisma.endorsement.create({
   *   data: {
   *     // ... data to create a Endorsement
   *   }
   * })
   * 
  **/
  create<T extends endorsementCreateArgs>(
    args: Subset<T, endorsementCreateArgs>
  ): CheckSelect<T, Prisma__endorsementClient<endorsement>, Prisma__endorsementClient<endorsementGetPayload<T>>>
  /**
   * Delete a Endorsement.
   * @param {endorsementDeleteArgs} args - Arguments to delete one Endorsement.
   * @example
   * // Delete one Endorsement
   * const Endorsement = await prisma.endorsement.delete({
   *   where: {
   *     // ... filter to delete one Endorsement
   *   }
   * })
   * 
  **/
  delete<T extends endorsementDeleteArgs>(
    args: Subset<T, endorsementDeleteArgs>
  ): CheckSelect<T, Prisma__endorsementClient<endorsement>, Prisma__endorsementClient<endorsementGetPayload<T>>>
  /**
   * Update one Endorsement.
   * @param {endorsementUpdateArgs} args - Arguments to update one Endorsement.
   * @example
   * // Update one Endorsement
   * const endorsement = await prisma.endorsement.update({
   *   where: {
   *     // ... provide filter here
   *   },
   *   data: {
   *     // ... provide data here
   *   }
   * })
   * 
  **/
  update<T extends endorsementUpdateArgs>(
    args: Subset<T, endorsementUpdateArgs>
  ): CheckSelect<T, Prisma__endorsementClient<endorsement>, Prisma__endorsementClient<endorsementGetPayload<T>>>
  /**
   * Delete zero or more Endorsements.
   * @param {endorsementDeleteManyArgs} args - Arguments to filter Endorsements to delete.
   * @example
   * // Delete a few Endorsements
   * const { count } = await prisma.endorsement.deleteMany({
   *   where: {
   *     // ... provide filter here
   *   }
   * })
   * 
  **/
  deleteMany<T extends endorsementDeleteManyArgs>(
    args: Subset<T, endorsementDeleteManyArgs>
  ): Promise<BatchPayload>
  /**
   * Update zero or more Endorsements.
   * @param {endorsementUpdateManyArgs} args - Arguments to update one or more rows.
   * @example
   * // Update many Endorsements
   * const endorsement = await prisma.endorsement.updateMany({
   *   where: {
   *     // ... provide filter here
   *   },
   *   data: {
   *     // ... provide data here
   *   }
   * })
   * 
  **/
  updateMany<T extends endorsementUpdateManyArgs>(
    args: Subset<T, endorsementUpdateManyArgs>
  ): Promise<BatchPayload>
  /**
   * Create or update one Endorsement.
   * @param {endorsementUpsertArgs} args - Arguments to update or create a Endorsement.
   * @example
   * // Update or create a Endorsement
   * const endorsement = await prisma.endorsement.upsert({
   *   create: {
   *     // ... data to create a Endorsement
   *   },
   *   update: {
   *     // ... in case it already exists, update
   *   },
   *   where: {
   *     // ... the filter for the Endorsement we want to update
   *   }
   * })
  **/
  upsert<T extends endorsementUpsertArgs>(
    args: Subset<T, endorsementUpsertArgs>
  ): CheckSelect<T, Prisma__endorsementClient<endorsement>, Prisma__endorsementClient<endorsementGetPayload<T>>>
  /**
   * Count
   */
  count(args?: Omit<FindManyendorsementArgs, 'select' | 'include'>): Promise<number>

  /**
   * Aggregate
   */
  aggregate<T extends AggregateEndorsementArgs>(args: Subset<T, AggregateEndorsementArgs>): Promise<GetEndorsementAggregateType<T>>
}

/**
 * The delegate class that acts as a "Promise-like" for endorsement.
 * Why is this prefixed with `Prisma__`?
 * Because we want to prevent naming conflicts as mentioned in 
 * https://github.com/prisma/prisma-client-js/issues/707
 */
export declare class Prisma__endorsementClient<T> implements Promise<T> {
  private readonly _dmmf;
  private readonly _fetcher;
  private readonly _queryType;
  private readonly _rootField;
  private readonly _clientMethod;
  private readonly _args;
  private readonly _dataPath;
  private readonly _errorFormat;
  private readonly _measurePerformance?;
  private _isList;
  private _callsite;
  private _requestPromise?;
  constructor(_dmmf: DMMFClass, _fetcher: PrismaClientFetcher, _queryType: 'query' | 'mutation', _rootField: string, _clientMethod: string, _args: any, _dataPath: string[], _errorFormat: ErrorFormat, _measurePerformance?: boolean | undefined, _isList?: boolean);
  readonly [Symbol.toStringTag]: 'PrismaClientPromise';

  block<T extends blockArgs = {}>(args?: Subset<T, blockArgs>): CheckSelect<T, Prisma__blockClient<block | null>, Prisma__blockClient<blockGetPayload<T> | null>>;

  operation<T extends operationArgs = {}>(args?: Subset<T, operationArgs>): CheckSelect<T, Prisma__operationClient<operation | null>, Prisma__operationClient<operationGetPayload<T> | null>>;

  implicit<T extends implicitArgs = {}>(args?: Subset<T, implicitArgs>): CheckSelect<T, Prisma__implicitClient<implicit | null>, Prisma__implicitClient<implicitGetPayload<T> | null>>;

  private get _document();
  /**
   * Attaches callbacks for the resolution and/or rejection of the Promise.
   * @param onfulfilled The callback to execute when the Promise is resolved.
   * @param onrejected The callback to execute when the Promise is rejected.
   * @returns A Promise for the completion of which ever callback is executed.
   */
  then<TResult1 = T, TResult2 = never>(onfulfilled?: ((value: T) => TResult1 | Promise<TResult1>) | undefined | null, onrejected?: ((reason: any) => TResult2 | Promise<TResult2>) | undefined | null): Promise<TResult1 | TResult2>;
  /**
   * Attaches a callback for only the rejection of the Promise.
   * @param onrejected The callback to execute when the Promise is rejected.
   * @returns A Promise for the completion of the callback.
   */
  catch<TResult = never>(onrejected?: ((reason: any) => TResult | Promise<TResult>) | undefined | null): Promise<T | TResult>;
  /**
   * Attaches a callback that is invoked when the Promise is settled (fulfilled or rejected). The
   * resolved value cannot be modified from the callback.
   * @param onfinally The callback to execute when the Promise is settled (fulfilled or rejected).
   * @returns A Promise for the completion of the callback.
   */
  finally(onfinally?: (() => void) | undefined | null): Promise<T>;
}

// Custom InputTypes

/**
 * endorsement findOne
 */
export type FindOneendorsementArgs = {
  /**
   * Select specific fields to fetch from the endorsement
  **/
  select?: endorsementSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: endorsementInclude | null
  /**
   * Filter, which endorsement to fetch.
  **/
  where: endorsementWhereUniqueInput
}


/**
 * endorsement findMany
 */
export type FindManyendorsementArgs = {
  /**
   * Select specific fields to fetch from the endorsement
  **/
  select?: endorsementSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: endorsementInclude | null
  /**
   * Filter, which endorsements to fetch.
  **/
  where?: endorsementWhereInput
  /**
   * Determine the order of the endorsements to fetch.
  **/
  orderBy?: Enumerable<endorsementOrderByInput>
  /**
   * Sets the position for listing endorsements.
  **/
  cursor?: endorsementWhereUniqueInput
  /**
   * The number of endorsements to fetch. If negative number, it will take endorsements before the `cursor`.
  **/
  take?: number
  /**
   * Skip the first `n` endorsements.
  **/
  skip?: number
  distinct?: Enumerable<EndorsementDistinctFieldEnum>
}


/**
 * endorsement create
 */
export type endorsementCreateArgs = {
  /**
   * Select specific fields to fetch from the endorsement
  **/
  select?: endorsementSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: endorsementInclude | null
  /**
   * The data needed to create a endorsement.
  **/
  data: endorsementCreateInput
}


/**
 * endorsement update
 */
export type endorsementUpdateArgs = {
  /**
   * Select specific fields to fetch from the endorsement
  **/
  select?: endorsementSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: endorsementInclude | null
  /**
   * The data needed to update a endorsement.
  **/
  data: endorsementUpdateInput
  /**
   * Choose, which endorsement to update.
  **/
  where: endorsementWhereUniqueInput
}


/**
 * endorsement updateMany
 */
export type endorsementUpdateManyArgs = {
  data: endorsementUpdateManyMutationInput
  where?: endorsementWhereInput
}


/**
 * endorsement upsert
 */
export type endorsementUpsertArgs = {
  /**
   * Select specific fields to fetch from the endorsement
  **/
  select?: endorsementSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: endorsementInclude | null
  /**
   * The filter to search for the endorsement to update in case it exists.
  **/
  where: endorsementWhereUniqueInput
  /**
   * In case the endorsement found by the `where` argument doesn't exist, create a new endorsement with this data.
  **/
  create: endorsementCreateInput
  /**
   * In case the endorsement was found with the provided `where` argument, update it with this data.
  **/
  update: endorsementUpdateInput
}


/**
 * endorsement delete
 */
export type endorsementDeleteArgs = {
  /**
   * Select specific fields to fetch from the endorsement
  **/
  select?: endorsementSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: endorsementInclude | null
  /**
   * Filter which endorsement to delete.
  **/
  where: endorsementWhereUniqueInput
}


/**
 * endorsement deleteMany
 */
export type endorsementDeleteManyArgs = {
  where?: endorsementWhereInput
}


/**
 * endorsement without action
 */
export type endorsementArgs = {
  /**
   * Select specific fields to fetch from the endorsement
  **/
  select?: endorsementSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: endorsementInclude | null
}



/**
 * Model implicit
 */

export type implicit = {
  pkh: string
  activated: string | null
  revealed: string | null
  pk: string | null
  autoid: number
}


export type AggregateImplicit = {
  count: number
  avg: ImplicitAvgAggregateOutputType | null
  sum: ImplicitSumAggregateOutputType | null
  min: ImplicitMinAggregateOutputType | null
  max: ImplicitMaxAggregateOutputType | null
}

export type ImplicitAvgAggregateOutputType = {
  autoid: number
}

export type ImplicitSumAggregateOutputType = {
  autoid: number
}

export type ImplicitMinAggregateOutputType = {
  autoid: number
}

export type ImplicitMaxAggregateOutputType = {
  autoid: number
}


export type ImplicitAvgAggregateInputType = {
  autoid?: true
}

export type ImplicitSumAggregateInputType = {
  autoid?: true
}

export type ImplicitMinAggregateInputType = {
  autoid?: true
}

export type ImplicitMaxAggregateInputType = {
  autoid?: true
}

export type AggregateImplicitArgs = {
  where?: implicitWhereInput
  orderBy?: Enumerable<implicitOrderByInput>
  cursor?: implicitWhereUniqueInput
  take?: number
  skip?: number
  distinct?: Enumerable<ImplicitDistinctFieldEnum>
  count?: true
  avg?: ImplicitAvgAggregateInputType
  sum?: ImplicitSumAggregateInputType
  min?: ImplicitMinAggregateInputType
  max?: ImplicitMaxAggregateInputType
}

export type GetImplicitAggregateType<T extends AggregateImplicitArgs> = {
  [P in keyof T]: P extends 'count' ? number : GetImplicitAggregateScalarType<T[P]>
}

export type GetImplicitAggregateScalarType<T extends any> = {
  [P in keyof T]: P extends keyof ImplicitAvgAggregateOutputType ? ImplicitAvgAggregateOutputType[P] : never
}
    
    

export type implicitSelect = {
  pkh?: boolean
  activated?: boolean
  revealed?: boolean
  pk?: boolean
  autoid?: boolean
  operation_implicit_activatedTooperation?: boolean | operationArgs
  addresses?: boolean | addressesArgs
  operation_implicit_revealedTooperation?: boolean | operationArgs
  contract?: boolean | FindManycontractArgs
  deactivated?: boolean | FindManydeactivatedArgs
  delegated_contract?: boolean | FindManydelegated_contractArgs
  endorsement?: boolean | FindManyendorsementArgs
}

export type implicitInclude = {
  operation_implicit_activatedTooperation?: boolean | operationArgs
  addresses?: boolean | addressesArgs
  operation_implicit_revealedTooperation?: boolean | operationArgs
  contract?: boolean | FindManycontractArgs
  deactivated?: boolean | FindManydeactivatedArgs
  delegated_contract?: boolean | FindManydelegated_contractArgs
  endorsement?: boolean | FindManyendorsementArgs
}

export type implicitGetPayload<
  S extends boolean | null | undefined | implicitArgs,
  U = keyof S
> = S extends true
  ? implicit
  : S extends undefined
  ? never
  : S extends implicitArgs | FindManyimplicitArgs
  ? 'include' extends U
    ? implicit  & {
      [P in TrueKeys<S['include']>]:
      P extends 'operation_implicit_activatedTooperation'
      ? operationGetPayload<S['include'][P]> | null :
      P extends 'addresses'
      ? addressesGetPayload<S['include'][P]> :
      P extends 'operation_implicit_revealedTooperation'
      ? operationGetPayload<S['include'][P]> | null :
      P extends 'contract'
      ? Array<contractGetPayload<S['include'][P]>> :
      P extends 'deactivated'
      ? Array<deactivatedGetPayload<S['include'][P]>> :
      P extends 'delegated_contract'
      ? Array<delegated_contractGetPayload<S['include'][P]>> :
      P extends 'endorsement'
      ? Array<endorsementGetPayload<S['include'][P]>> : never
    }
  : 'select' extends U
    ? {
      [P in TrueKeys<S['select']>]:P extends keyof implicit ? implicit[P]
: 
      P extends 'operation_implicit_activatedTooperation'
      ? operationGetPayload<S['select'][P]> | null :
      P extends 'addresses'
      ? addressesGetPayload<S['select'][P]> :
      P extends 'operation_implicit_revealedTooperation'
      ? operationGetPayload<S['select'][P]> | null :
      P extends 'contract'
      ? Array<contractGetPayload<S['select'][P]>> :
      P extends 'deactivated'
      ? Array<deactivatedGetPayload<S['select'][P]>> :
      P extends 'delegated_contract'
      ? Array<delegated_contractGetPayload<S['select'][P]>> :
      P extends 'endorsement'
      ? Array<endorsementGetPayload<S['select'][P]>> : never
    }
  : implicit
: implicit


export interface implicitDelegate {
  /**
   * Find zero or one Implicit.
   * @param {FindOneimplicitArgs} args - Arguments to find a Implicit
   * @example
   * // Get one Implicit
   * const implicit = await prisma.implicit.findOne({
   *   where: {
   *     // ... provide filter here
   *   }
   * })
  **/
  findOne<T extends FindOneimplicitArgs>(
    args: Subset<T, FindOneimplicitArgs>
  ): CheckSelect<T, Prisma__implicitClient<implicit | null>, Prisma__implicitClient<implicitGetPayload<T> | null>>
  /**
   * Find zero or more Implicits.
   * @param {FindManyimplicitArgs=} args - Arguments to filter and select certain fields only.
   * @example
   * // Get all Implicits
   * const implicits = await prisma.implicit.findMany()
   * 
   * // Get first 10 Implicits
   * const implicits = await prisma.implicit.findMany({ take: 10 })
   * 
   * // Only select the `pkh`
   * const implicitWithPkhOnly = await prisma.implicit.findMany({ select: { pkh: true } })
   * 
  **/
  findMany<T extends FindManyimplicitArgs>(
    args?: Subset<T, FindManyimplicitArgs>
  ): CheckSelect<T, Promise<Array<implicit>>, Promise<Array<implicitGetPayload<T>>>>
  /**
   * Create a Implicit.
   * @param {implicitCreateArgs} args - Arguments to create a Implicit.
   * @example
   * // Create one Implicit
   * const Implicit = await prisma.implicit.create({
   *   data: {
   *     // ... data to create a Implicit
   *   }
   * })
   * 
  **/
  create<T extends implicitCreateArgs>(
    args: Subset<T, implicitCreateArgs>
  ): CheckSelect<T, Prisma__implicitClient<implicit>, Prisma__implicitClient<implicitGetPayload<T>>>
  /**
   * Delete a Implicit.
   * @param {implicitDeleteArgs} args - Arguments to delete one Implicit.
   * @example
   * // Delete one Implicit
   * const Implicit = await prisma.implicit.delete({
   *   where: {
   *     // ... filter to delete one Implicit
   *   }
   * })
   * 
  **/
  delete<T extends implicitDeleteArgs>(
    args: Subset<T, implicitDeleteArgs>
  ): CheckSelect<T, Prisma__implicitClient<implicit>, Prisma__implicitClient<implicitGetPayload<T>>>
  /**
   * Update one Implicit.
   * @param {implicitUpdateArgs} args - Arguments to update one Implicit.
   * @example
   * // Update one Implicit
   * const implicit = await prisma.implicit.update({
   *   where: {
   *     // ... provide filter here
   *   },
   *   data: {
   *     // ... provide data here
   *   }
   * })
   * 
  **/
  update<T extends implicitUpdateArgs>(
    args: Subset<T, implicitUpdateArgs>
  ): CheckSelect<T, Prisma__implicitClient<implicit>, Prisma__implicitClient<implicitGetPayload<T>>>
  /**
   * Delete zero or more Implicits.
   * @param {implicitDeleteManyArgs} args - Arguments to filter Implicits to delete.
   * @example
   * // Delete a few Implicits
   * const { count } = await prisma.implicit.deleteMany({
   *   where: {
   *     // ... provide filter here
   *   }
   * })
   * 
  **/
  deleteMany<T extends implicitDeleteManyArgs>(
    args: Subset<T, implicitDeleteManyArgs>
  ): Promise<BatchPayload>
  /**
   * Update zero or more Implicits.
   * @param {implicitUpdateManyArgs} args - Arguments to update one or more rows.
   * @example
   * // Update many Implicits
   * const implicit = await prisma.implicit.updateMany({
   *   where: {
   *     // ... provide filter here
   *   },
   *   data: {
   *     // ... provide data here
   *   }
   * })
   * 
  **/
  updateMany<T extends implicitUpdateManyArgs>(
    args: Subset<T, implicitUpdateManyArgs>
  ): Promise<BatchPayload>
  /**
   * Create or update one Implicit.
   * @param {implicitUpsertArgs} args - Arguments to update or create a Implicit.
   * @example
   * // Update or create a Implicit
   * const implicit = await prisma.implicit.upsert({
   *   create: {
   *     // ... data to create a Implicit
   *   },
   *   update: {
   *     // ... in case it already exists, update
   *   },
   *   where: {
   *     // ... the filter for the Implicit we want to update
   *   }
   * })
  **/
  upsert<T extends implicitUpsertArgs>(
    args: Subset<T, implicitUpsertArgs>
  ): CheckSelect<T, Prisma__implicitClient<implicit>, Prisma__implicitClient<implicitGetPayload<T>>>
  /**
   * Count
   */
  count(args?: Omit<FindManyimplicitArgs, 'select' | 'include'>): Promise<number>

  /**
   * Aggregate
   */
  aggregate<T extends AggregateImplicitArgs>(args: Subset<T, AggregateImplicitArgs>): Promise<GetImplicitAggregateType<T>>
}

/**
 * The delegate class that acts as a "Promise-like" for implicit.
 * Why is this prefixed with `Prisma__`?
 * Because we want to prevent naming conflicts as mentioned in 
 * https://github.com/prisma/prisma-client-js/issues/707
 */
export declare class Prisma__implicitClient<T> implements Promise<T> {
  private readonly _dmmf;
  private readonly _fetcher;
  private readonly _queryType;
  private readonly _rootField;
  private readonly _clientMethod;
  private readonly _args;
  private readonly _dataPath;
  private readonly _errorFormat;
  private readonly _measurePerformance?;
  private _isList;
  private _callsite;
  private _requestPromise?;
  constructor(_dmmf: DMMFClass, _fetcher: PrismaClientFetcher, _queryType: 'query' | 'mutation', _rootField: string, _clientMethod: string, _args: any, _dataPath: string[], _errorFormat: ErrorFormat, _measurePerformance?: boolean | undefined, _isList?: boolean);
  readonly [Symbol.toStringTag]: 'PrismaClientPromise';

  operation_implicit_activatedTooperation<T extends operationArgs = {}>(args?: Subset<T, operationArgs>): CheckSelect<T, Prisma__operationClient<operation | null>, Prisma__operationClient<operationGetPayload<T> | null>>;

  addresses<T extends addressesArgs = {}>(args?: Subset<T, addressesArgs>): CheckSelect<T, Prisma__addressesClient<addresses | null>, Prisma__addressesClient<addressesGetPayload<T> | null>>;

  operation_implicit_revealedTooperation<T extends operationArgs = {}>(args?: Subset<T, operationArgs>): CheckSelect<T, Prisma__operationClient<operation | null>, Prisma__operationClient<operationGetPayload<T> | null>>;

  contract<T extends FindManycontractArgs = {}>(args?: Subset<T, FindManycontractArgs>): CheckSelect<T, Promise<Array<contract>>, Promise<Array<contractGetPayload<T>>>>;

  deactivated<T extends FindManydeactivatedArgs = {}>(args?: Subset<T, FindManydeactivatedArgs>): CheckSelect<T, Promise<Array<deactivated>>, Promise<Array<deactivatedGetPayload<T>>>>;

  delegated_contract<T extends FindManydelegated_contractArgs = {}>(args?: Subset<T, FindManydelegated_contractArgs>): CheckSelect<T, Promise<Array<delegated_contract>>, Promise<Array<delegated_contractGetPayload<T>>>>;

  endorsement<T extends FindManyendorsementArgs = {}>(args?: Subset<T, FindManyendorsementArgs>): CheckSelect<T, Promise<Array<endorsement>>, Promise<Array<endorsementGetPayload<T>>>>;

  private get _document();
  /**
   * Attaches callbacks for the resolution and/or rejection of the Promise.
   * @param onfulfilled The callback to execute when the Promise is resolved.
   * @param onrejected The callback to execute when the Promise is rejected.
   * @returns A Promise for the completion of which ever callback is executed.
   */
  then<TResult1 = T, TResult2 = never>(onfulfilled?: ((value: T) => TResult1 | Promise<TResult1>) | undefined | null, onrejected?: ((reason: any) => TResult2 | Promise<TResult2>) | undefined | null): Promise<TResult1 | TResult2>;
  /**
   * Attaches a callback for only the rejection of the Promise.
   * @param onrejected The callback to execute when the Promise is rejected.
   * @returns A Promise for the completion of the callback.
   */
  catch<TResult = never>(onrejected?: ((reason: any) => TResult | Promise<TResult>) | undefined | null): Promise<T | TResult>;
  /**
   * Attaches a callback that is invoked when the Promise is settled (fulfilled or rejected). The
   * resolved value cannot be modified from the callback.
   * @param onfinally The callback to execute when the Promise is settled (fulfilled or rejected).
   * @returns A Promise for the completion of the callback.
   */
  finally(onfinally?: (() => void) | undefined | null): Promise<T>;
}

// Custom InputTypes

/**
 * implicit findOne
 */
export type FindOneimplicitArgs = {
  /**
   * Select specific fields to fetch from the implicit
  **/
  select?: implicitSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: implicitInclude | null
  /**
   * Filter, which implicit to fetch.
  **/
  where: implicitWhereUniqueInput
}


/**
 * implicit findMany
 */
export type FindManyimplicitArgs = {
  /**
   * Select specific fields to fetch from the implicit
  **/
  select?: implicitSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: implicitInclude | null
  /**
   * Filter, which implicits to fetch.
  **/
  where?: implicitWhereInput
  /**
   * Determine the order of the implicits to fetch.
  **/
  orderBy?: Enumerable<implicitOrderByInput>
  /**
   * Sets the position for listing implicits.
  **/
  cursor?: implicitWhereUniqueInput
  /**
   * The number of implicits to fetch. If negative number, it will take implicits before the `cursor`.
  **/
  take?: number
  /**
   * Skip the first `n` implicits.
  **/
  skip?: number
  distinct?: Enumerable<ImplicitDistinctFieldEnum>
}


/**
 * implicit create
 */
export type implicitCreateArgs = {
  /**
   * Select specific fields to fetch from the implicit
  **/
  select?: implicitSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: implicitInclude | null
  /**
   * The data needed to create a implicit.
  **/
  data: implicitCreateInput
}


/**
 * implicit update
 */
export type implicitUpdateArgs = {
  /**
   * Select specific fields to fetch from the implicit
  **/
  select?: implicitSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: implicitInclude | null
  /**
   * The data needed to update a implicit.
  **/
  data: implicitUpdateInput
  /**
   * Choose, which implicit to update.
  **/
  where: implicitWhereUniqueInput
}


/**
 * implicit updateMany
 */
export type implicitUpdateManyArgs = {
  data: implicitUpdateManyMutationInput
  where?: implicitWhereInput
}


/**
 * implicit upsert
 */
export type implicitUpsertArgs = {
  /**
   * Select specific fields to fetch from the implicit
  **/
  select?: implicitSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: implicitInclude | null
  /**
   * The filter to search for the implicit to update in case it exists.
  **/
  where: implicitWhereUniqueInput
  /**
   * In case the implicit found by the `where` argument doesn't exist, create a new implicit with this data.
  **/
  create: implicitCreateInput
  /**
   * In case the implicit was found with the provided `where` argument, update it with this data.
  **/
  update: implicitUpdateInput
}


/**
 * implicit delete
 */
export type implicitDeleteArgs = {
  /**
   * Select specific fields to fetch from the implicit
  **/
  select?: implicitSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: implicitInclude | null
  /**
   * Filter which implicit to delete.
  **/
  where: implicitWhereUniqueInput
}


/**
 * implicit deleteMany
 */
export type implicitDeleteManyArgs = {
  where?: implicitWhereInput
}


/**
 * implicit without action
 */
export type implicitArgs = {
  /**
   * Select specific fields to fetch from the implicit
  **/
  select?: implicitSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: implicitInclude | null
}



/**
 * Model operation
 */

export type operation = {
  hash: string
  chain: string
  block_hash: string
  autoid: number
}


export type AggregateOperation = {
  count: number
  avg: OperationAvgAggregateOutputType | null
  sum: OperationSumAggregateOutputType | null
  min: OperationMinAggregateOutputType | null
  max: OperationMaxAggregateOutputType | null
}

export type OperationAvgAggregateOutputType = {
  autoid: number
}

export type OperationSumAggregateOutputType = {
  autoid: number
}

export type OperationMinAggregateOutputType = {
  autoid: number
}

export type OperationMaxAggregateOutputType = {
  autoid: number
}


export type OperationAvgAggregateInputType = {
  autoid?: true
}

export type OperationSumAggregateInputType = {
  autoid?: true
}

export type OperationMinAggregateInputType = {
  autoid?: true
}

export type OperationMaxAggregateInputType = {
  autoid?: true
}

export type AggregateOperationArgs = {
  where?: operationWhereInput
  orderBy?: Enumerable<operationOrderByInput>
  cursor?: operationWhereUniqueInput
  take?: number
  skip?: number
  distinct?: Enumerable<OperationDistinctFieldEnum>
  count?: true
  avg?: OperationAvgAggregateInputType
  sum?: OperationSumAggregateInputType
  min?: OperationMinAggregateInputType
  max?: OperationMaxAggregateInputType
}

export type GetOperationAggregateType<T extends AggregateOperationArgs> = {
  [P in keyof T]: P extends 'count' ? number : GetOperationAggregateScalarType<T[P]>
}

export type GetOperationAggregateScalarType<T extends any> = {
  [P in keyof T]: P extends keyof OperationAvgAggregateOutputType ? OperationAvgAggregateOutputType[P] : never
}
    
    

export type operationSelect = {
  hash?: boolean
  chain?: boolean
  block_hash?: boolean
  autoid?: boolean
  block?: boolean | blockArgs
  chain_chainTooperation?: boolean | chainArgs
  endorsement?: boolean | FindManyendorsementArgs
  implicit_implicit_activatedTooperation?: boolean | FindManyimplicitArgs
  implicit_implicit_revealedTooperation?: boolean | FindManyimplicitArgs
  manager_numbers?: boolean | FindManymanager_numbersArgs
  operation_alpha?: boolean | FindManyoperation_alphaArgs
}

export type operationInclude = {
  block?: boolean | blockArgs
  chain_chainTooperation?: boolean | chainArgs
  endorsement?: boolean | FindManyendorsementArgs
  implicit_implicit_activatedTooperation?: boolean | FindManyimplicitArgs
  implicit_implicit_revealedTooperation?: boolean | FindManyimplicitArgs
  manager_numbers?: boolean | FindManymanager_numbersArgs
  operation_alpha?: boolean | FindManyoperation_alphaArgs
}

export type operationGetPayload<
  S extends boolean | null | undefined | operationArgs,
  U = keyof S
> = S extends true
  ? operation
  : S extends undefined
  ? never
  : S extends operationArgs | FindManyoperationArgs
  ? 'include' extends U
    ? operation  & {
      [P in TrueKeys<S['include']>]:
      P extends 'block'
      ? blockGetPayload<S['include'][P]> :
      P extends 'chain_chainTooperation'
      ? chainGetPayload<S['include'][P]> :
      P extends 'endorsement'
      ? Array<endorsementGetPayload<S['include'][P]>> :
      P extends 'implicit_implicit_activatedTooperation'
      ? Array<implicitGetPayload<S['include'][P]>> :
      P extends 'implicit_implicit_revealedTooperation'
      ? Array<implicitGetPayload<S['include'][P]>> :
      P extends 'manager_numbers'
      ? Array<manager_numbersGetPayload<S['include'][P]>> :
      P extends 'operation_alpha'
      ? Array<operation_alphaGetPayload<S['include'][P]>> : never
    }
  : 'select' extends U
    ? {
      [P in TrueKeys<S['select']>]:P extends keyof operation ? operation[P]
: 
      P extends 'block'
      ? blockGetPayload<S['select'][P]> :
      P extends 'chain_chainTooperation'
      ? chainGetPayload<S['select'][P]> :
      P extends 'endorsement'
      ? Array<endorsementGetPayload<S['select'][P]>> :
      P extends 'implicit_implicit_activatedTooperation'
      ? Array<implicitGetPayload<S['select'][P]>> :
      P extends 'implicit_implicit_revealedTooperation'
      ? Array<implicitGetPayload<S['select'][P]>> :
      P extends 'manager_numbers'
      ? Array<manager_numbersGetPayload<S['select'][P]>> :
      P extends 'operation_alpha'
      ? Array<operation_alphaGetPayload<S['select'][P]>> : never
    }
  : operation
: operation


export interface operationDelegate {
  /**
   * Find zero or one Operation.
   * @param {FindOneoperationArgs} args - Arguments to find a Operation
   * @example
   * // Get one Operation
   * const operation = await prisma.operation.findOne({
   *   where: {
   *     // ... provide filter here
   *   }
   * })
  **/
  findOne<T extends FindOneoperationArgs>(
    args: Subset<T, FindOneoperationArgs>
  ): CheckSelect<T, Prisma__operationClient<operation | null>, Prisma__operationClient<operationGetPayload<T> | null>>
  /**
   * Find zero or more Operations.
   * @param {FindManyoperationArgs=} args - Arguments to filter and select certain fields only.
   * @example
   * // Get all Operations
   * const operations = await prisma.operation.findMany()
   * 
   * // Get first 10 Operations
   * const operations = await prisma.operation.findMany({ take: 10 })
   * 
   * // Only select the `hash`
   * const operationWithHashOnly = await prisma.operation.findMany({ select: { hash: true } })
   * 
  **/
  findMany<T extends FindManyoperationArgs>(
    args?: Subset<T, FindManyoperationArgs>
  ): CheckSelect<T, Promise<Array<operation>>, Promise<Array<operationGetPayload<T>>>>
  /**
   * Create a Operation.
   * @param {operationCreateArgs} args - Arguments to create a Operation.
   * @example
   * // Create one Operation
   * const Operation = await prisma.operation.create({
   *   data: {
   *     // ... data to create a Operation
   *   }
   * })
   * 
  **/
  create<T extends operationCreateArgs>(
    args: Subset<T, operationCreateArgs>
  ): CheckSelect<T, Prisma__operationClient<operation>, Prisma__operationClient<operationGetPayload<T>>>
  /**
   * Delete a Operation.
   * @param {operationDeleteArgs} args - Arguments to delete one Operation.
   * @example
   * // Delete one Operation
   * const Operation = await prisma.operation.delete({
   *   where: {
   *     // ... filter to delete one Operation
   *   }
   * })
   * 
  **/
  delete<T extends operationDeleteArgs>(
    args: Subset<T, operationDeleteArgs>
  ): CheckSelect<T, Prisma__operationClient<operation>, Prisma__operationClient<operationGetPayload<T>>>
  /**
   * Update one Operation.
   * @param {operationUpdateArgs} args - Arguments to update one Operation.
   * @example
   * // Update one Operation
   * const operation = await prisma.operation.update({
   *   where: {
   *     // ... provide filter here
   *   },
   *   data: {
   *     // ... provide data here
   *   }
   * })
   * 
  **/
  update<T extends operationUpdateArgs>(
    args: Subset<T, operationUpdateArgs>
  ): CheckSelect<T, Prisma__operationClient<operation>, Prisma__operationClient<operationGetPayload<T>>>
  /**
   * Delete zero or more Operations.
   * @param {operationDeleteManyArgs} args - Arguments to filter Operations to delete.
   * @example
   * // Delete a few Operations
   * const { count } = await prisma.operation.deleteMany({
   *   where: {
   *     // ... provide filter here
   *   }
   * })
   * 
  **/
  deleteMany<T extends operationDeleteManyArgs>(
    args: Subset<T, operationDeleteManyArgs>
  ): Promise<BatchPayload>
  /**
   * Update zero or more Operations.
   * @param {operationUpdateManyArgs} args - Arguments to update one or more rows.
   * @example
   * // Update many Operations
   * const operation = await prisma.operation.updateMany({
   *   where: {
   *     // ... provide filter here
   *   },
   *   data: {
   *     // ... provide data here
   *   }
   * })
   * 
  **/
  updateMany<T extends operationUpdateManyArgs>(
    args: Subset<T, operationUpdateManyArgs>
  ): Promise<BatchPayload>
  /**
   * Create or update one Operation.
   * @param {operationUpsertArgs} args - Arguments to update or create a Operation.
   * @example
   * // Update or create a Operation
   * const operation = await prisma.operation.upsert({
   *   create: {
   *     // ... data to create a Operation
   *   },
   *   update: {
   *     // ... in case it already exists, update
   *   },
   *   where: {
   *     // ... the filter for the Operation we want to update
   *   }
   * })
  **/
  upsert<T extends operationUpsertArgs>(
    args: Subset<T, operationUpsertArgs>
  ): CheckSelect<T, Prisma__operationClient<operation>, Prisma__operationClient<operationGetPayload<T>>>
  /**
   * Count
   */
  count(args?: Omit<FindManyoperationArgs, 'select' | 'include'>): Promise<number>

  /**
   * Aggregate
   */
  aggregate<T extends AggregateOperationArgs>(args: Subset<T, AggregateOperationArgs>): Promise<GetOperationAggregateType<T>>
}

/**
 * The delegate class that acts as a "Promise-like" for operation.
 * Why is this prefixed with `Prisma__`?
 * Because we want to prevent naming conflicts as mentioned in 
 * https://github.com/prisma/prisma-client-js/issues/707
 */
export declare class Prisma__operationClient<T> implements Promise<T> {
  private readonly _dmmf;
  private readonly _fetcher;
  private readonly _queryType;
  private readonly _rootField;
  private readonly _clientMethod;
  private readonly _args;
  private readonly _dataPath;
  private readonly _errorFormat;
  private readonly _measurePerformance?;
  private _isList;
  private _callsite;
  private _requestPromise?;
  constructor(_dmmf: DMMFClass, _fetcher: PrismaClientFetcher, _queryType: 'query' | 'mutation', _rootField: string, _clientMethod: string, _args: any, _dataPath: string[], _errorFormat: ErrorFormat, _measurePerformance?: boolean | undefined, _isList?: boolean);
  readonly [Symbol.toStringTag]: 'PrismaClientPromise';

  block<T extends blockArgs = {}>(args?: Subset<T, blockArgs>): CheckSelect<T, Prisma__blockClient<block | null>, Prisma__blockClient<blockGetPayload<T> | null>>;

  chain_chainTooperation<T extends chainArgs = {}>(args?: Subset<T, chainArgs>): CheckSelect<T, Prisma__chainClient<chain | null>, Prisma__chainClient<chainGetPayload<T> | null>>;

  endorsement<T extends FindManyendorsementArgs = {}>(args?: Subset<T, FindManyendorsementArgs>): CheckSelect<T, Promise<Array<endorsement>>, Promise<Array<endorsementGetPayload<T>>>>;

  implicit_implicit_activatedTooperation<T extends FindManyimplicitArgs = {}>(args?: Subset<T, FindManyimplicitArgs>): CheckSelect<T, Promise<Array<implicit>>, Promise<Array<implicitGetPayload<T>>>>;

  implicit_implicit_revealedTooperation<T extends FindManyimplicitArgs = {}>(args?: Subset<T, FindManyimplicitArgs>): CheckSelect<T, Promise<Array<implicit>>, Promise<Array<implicitGetPayload<T>>>>;

  manager_numbers<T extends FindManymanager_numbersArgs = {}>(args?: Subset<T, FindManymanager_numbersArgs>): CheckSelect<T, Promise<Array<manager_numbers>>, Promise<Array<manager_numbersGetPayload<T>>>>;

  operation_alpha<T extends FindManyoperation_alphaArgs = {}>(args?: Subset<T, FindManyoperation_alphaArgs>): CheckSelect<T, Promise<Array<operation_alpha>>, Promise<Array<operation_alphaGetPayload<T>>>>;

  private get _document();
  /**
   * Attaches callbacks for the resolution and/or rejection of the Promise.
   * @param onfulfilled The callback to execute when the Promise is resolved.
   * @param onrejected The callback to execute when the Promise is rejected.
   * @returns A Promise for the completion of which ever callback is executed.
   */
  then<TResult1 = T, TResult2 = never>(onfulfilled?: ((value: T) => TResult1 | Promise<TResult1>) | undefined | null, onrejected?: ((reason: any) => TResult2 | Promise<TResult2>) | undefined | null): Promise<TResult1 | TResult2>;
  /**
   * Attaches a callback for only the rejection of the Promise.
   * @param onrejected The callback to execute when the Promise is rejected.
   * @returns A Promise for the completion of the callback.
   */
  catch<TResult = never>(onrejected?: ((reason: any) => TResult | Promise<TResult>) | undefined | null): Promise<T | TResult>;
  /**
   * Attaches a callback that is invoked when the Promise is settled (fulfilled or rejected). The
   * resolved value cannot be modified from the callback.
   * @param onfinally The callback to execute when the Promise is settled (fulfilled or rejected).
   * @returns A Promise for the completion of the callback.
   */
  finally(onfinally?: (() => void) | undefined | null): Promise<T>;
}

// Custom InputTypes

/**
 * operation findOne
 */
export type FindOneoperationArgs = {
  /**
   * Select specific fields to fetch from the operation
  **/
  select?: operationSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: operationInclude | null
  /**
   * Filter, which operation to fetch.
  **/
  where: operationWhereUniqueInput
}


/**
 * operation findMany
 */
export type FindManyoperationArgs = {
  /**
   * Select specific fields to fetch from the operation
  **/
  select?: operationSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: operationInclude | null
  /**
   * Filter, which operations to fetch.
  **/
  where?: operationWhereInput
  /**
   * Determine the order of the operations to fetch.
  **/
  orderBy?: Enumerable<operationOrderByInput>
  /**
   * Sets the position for listing operations.
  **/
  cursor?: operationWhereUniqueInput
  /**
   * The number of operations to fetch. If negative number, it will take operations before the `cursor`.
  **/
  take?: number
  /**
   * Skip the first `n` operations.
  **/
  skip?: number
  distinct?: Enumerable<OperationDistinctFieldEnum>
}


/**
 * operation create
 */
export type operationCreateArgs = {
  /**
   * Select specific fields to fetch from the operation
  **/
  select?: operationSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: operationInclude | null
  /**
   * The data needed to create a operation.
  **/
  data: operationCreateInput
}


/**
 * operation update
 */
export type operationUpdateArgs = {
  /**
   * Select specific fields to fetch from the operation
  **/
  select?: operationSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: operationInclude | null
  /**
   * The data needed to update a operation.
  **/
  data: operationUpdateInput
  /**
   * Choose, which operation to update.
  **/
  where: operationWhereUniqueInput
}


/**
 * operation updateMany
 */
export type operationUpdateManyArgs = {
  data: operationUpdateManyMutationInput
  where?: operationWhereInput
}


/**
 * operation upsert
 */
export type operationUpsertArgs = {
  /**
   * Select specific fields to fetch from the operation
  **/
  select?: operationSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: operationInclude | null
  /**
   * The filter to search for the operation to update in case it exists.
  **/
  where: operationWhereUniqueInput
  /**
   * In case the operation found by the `where` argument doesn't exist, create a new operation with this data.
  **/
  create: operationCreateInput
  /**
   * In case the operation was found with the provided `where` argument, update it with this data.
  **/
  update: operationUpdateInput
}


/**
 * operation delete
 */
export type operationDeleteArgs = {
  /**
   * Select specific fields to fetch from the operation
  **/
  select?: operationSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: operationInclude | null
  /**
   * Filter which operation to delete.
  **/
  where: operationWhereUniqueInput
}


/**
 * operation deleteMany
 */
export type operationDeleteManyArgs = {
  where?: operationWhereInput
}


/**
 * operation without action
 */
export type operationArgs = {
  /**
   * Select specific fields to fetch from the operation
  **/
  select?: operationSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: operationInclude | null
}



/**
 * Model operation_alpha
 */

export type operation_alpha = {
  hash: string
  id: number
  operation_kind: number
  sender: string | null
  receiver: string | null
  autoid: number
}


export type AggregateOperation_alpha = {
  count: number
  avg: Operation_alphaAvgAggregateOutputType | null
  sum: Operation_alphaSumAggregateOutputType | null
  min: Operation_alphaMinAggregateOutputType | null
  max: Operation_alphaMaxAggregateOutputType | null
}

export type Operation_alphaAvgAggregateOutputType = {
  id: number
  operation_kind: number
  autoid: number
}

export type Operation_alphaSumAggregateOutputType = {
  id: number
  operation_kind: number
  autoid: number
}

export type Operation_alphaMinAggregateOutputType = {
  id: number
  operation_kind: number
  autoid: number
}

export type Operation_alphaMaxAggregateOutputType = {
  id: number
  operation_kind: number
  autoid: number
}


export type Operation_alphaAvgAggregateInputType = {
  id?: true
  operation_kind?: true
  autoid?: true
}

export type Operation_alphaSumAggregateInputType = {
  id?: true
  operation_kind?: true
  autoid?: true
}

export type Operation_alphaMinAggregateInputType = {
  id?: true
  operation_kind?: true
  autoid?: true
}

export type Operation_alphaMaxAggregateInputType = {
  id?: true
  operation_kind?: true
  autoid?: true
}

export type AggregateOperation_alphaArgs = {
  where?: operation_alphaWhereInput
  orderBy?: Enumerable<operation_alphaOrderByInput>
  cursor?: operation_alphaWhereUniqueInput
  take?: number
  skip?: number
  distinct?: Enumerable<Operation_alphaDistinctFieldEnum>
  count?: true
  avg?: Operation_alphaAvgAggregateInputType
  sum?: Operation_alphaSumAggregateInputType
  min?: Operation_alphaMinAggregateInputType
  max?: Operation_alphaMaxAggregateInputType
}

export type GetOperation_alphaAggregateType<T extends AggregateOperation_alphaArgs> = {
  [P in keyof T]: P extends 'count' ? number : GetOperation_alphaAggregateScalarType<T[P]>
}

export type GetOperation_alphaAggregateScalarType<T extends any> = {
  [P in keyof T]: P extends keyof Operation_alphaAvgAggregateOutputType ? Operation_alphaAvgAggregateOutputType[P] : never
}
    
    

export type operation_alphaSelect = {
  hash?: boolean
  id?: boolean
  operation_kind?: boolean
  sender?: boolean
  receiver?: boolean
  autoid?: boolean
  operation?: boolean | operationArgs
  addresses_addressesTooperation_alpha_receiver?: boolean | addressesArgs
  addresses_addressesTooperation_alpha_sender?: boolean | addressesArgs
  delegation?: boolean | FindManydelegationArgs
  origination?: boolean | FindManyoriginationArgs
  tx?: boolean | FindManytxArgs
}

export type operation_alphaInclude = {
  operation?: boolean | operationArgs
  addresses_addressesTooperation_alpha_receiver?: boolean | addressesArgs
  addresses_addressesTooperation_alpha_sender?: boolean | addressesArgs
  delegation?: boolean | FindManydelegationArgs
  origination?: boolean | FindManyoriginationArgs
  tx?: boolean | FindManytxArgs
}

export type operation_alphaGetPayload<
  S extends boolean | null | undefined | operation_alphaArgs,
  U = keyof S
> = S extends true
  ? operation_alpha
  : S extends undefined
  ? never
  : S extends operation_alphaArgs | FindManyoperation_alphaArgs
  ? 'include' extends U
    ? operation_alpha  & {
      [P in TrueKeys<S['include']>]:
      P extends 'operation'
      ? operationGetPayload<S['include'][P]> :
      P extends 'addresses_addressesTooperation_alpha_receiver'
      ? addressesGetPayload<S['include'][P]> | null :
      P extends 'addresses_addressesTooperation_alpha_sender'
      ? addressesGetPayload<S['include'][P]> | null :
      P extends 'delegation'
      ? Array<delegationGetPayload<S['include'][P]>> :
      P extends 'origination'
      ? Array<originationGetPayload<S['include'][P]>> :
      P extends 'tx'
      ? Array<txGetPayload<S['include'][P]>> : never
    }
  : 'select' extends U
    ? {
      [P in TrueKeys<S['select']>]:P extends keyof operation_alpha ? operation_alpha[P]
: 
      P extends 'operation'
      ? operationGetPayload<S['select'][P]> :
      P extends 'addresses_addressesTooperation_alpha_receiver'
      ? addressesGetPayload<S['select'][P]> | null :
      P extends 'addresses_addressesTooperation_alpha_sender'
      ? addressesGetPayload<S['select'][P]> | null :
      P extends 'delegation'
      ? Array<delegationGetPayload<S['select'][P]>> :
      P extends 'origination'
      ? Array<originationGetPayload<S['select'][P]>> :
      P extends 'tx'
      ? Array<txGetPayload<S['select'][P]>> : never
    }
  : operation_alpha
: operation_alpha


export interface operation_alphaDelegate {
  /**
   * Find zero or one Operation_alpha.
   * @param {FindOneoperation_alphaArgs} args - Arguments to find a Operation_alpha
   * @example
   * // Get one Operation_alpha
   * const operation_alpha = await prisma.operation_alpha.findOne({
   *   where: {
   *     // ... provide filter here
   *   }
   * })
  **/
  findOne<T extends FindOneoperation_alphaArgs>(
    args: Subset<T, FindOneoperation_alphaArgs>
  ): CheckSelect<T, Prisma__operation_alphaClient<operation_alpha | null>, Prisma__operation_alphaClient<operation_alphaGetPayload<T> | null>>
  /**
   * Find zero or more Operation_alphas.
   * @param {FindManyoperation_alphaArgs=} args - Arguments to filter and select certain fields only.
   * @example
   * // Get all Operation_alphas
   * const operation_alphas = await prisma.operation_alpha.findMany()
   * 
   * // Get first 10 Operation_alphas
   * const operation_alphas = await prisma.operation_alpha.findMany({ take: 10 })
   * 
   * // Only select the `hash`
   * const operation_alphaWithHashOnly = await prisma.operation_alpha.findMany({ select: { hash: true } })
   * 
  **/
  findMany<T extends FindManyoperation_alphaArgs>(
    args?: Subset<T, FindManyoperation_alphaArgs>
  ): CheckSelect<T, Promise<Array<operation_alpha>>, Promise<Array<operation_alphaGetPayload<T>>>>
  /**
   * Create a Operation_alpha.
   * @param {operation_alphaCreateArgs} args - Arguments to create a Operation_alpha.
   * @example
   * // Create one Operation_alpha
   * const Operation_alpha = await prisma.operation_alpha.create({
   *   data: {
   *     // ... data to create a Operation_alpha
   *   }
   * })
   * 
  **/
  create<T extends operation_alphaCreateArgs>(
    args: Subset<T, operation_alphaCreateArgs>
  ): CheckSelect<T, Prisma__operation_alphaClient<operation_alpha>, Prisma__operation_alphaClient<operation_alphaGetPayload<T>>>
  /**
   * Delete a Operation_alpha.
   * @param {operation_alphaDeleteArgs} args - Arguments to delete one Operation_alpha.
   * @example
   * // Delete one Operation_alpha
   * const Operation_alpha = await prisma.operation_alpha.delete({
   *   where: {
   *     // ... filter to delete one Operation_alpha
   *   }
   * })
   * 
  **/
  delete<T extends operation_alphaDeleteArgs>(
    args: Subset<T, operation_alphaDeleteArgs>
  ): CheckSelect<T, Prisma__operation_alphaClient<operation_alpha>, Prisma__operation_alphaClient<operation_alphaGetPayload<T>>>
  /**
   * Update one Operation_alpha.
   * @param {operation_alphaUpdateArgs} args - Arguments to update one Operation_alpha.
   * @example
   * // Update one Operation_alpha
   * const operation_alpha = await prisma.operation_alpha.update({
   *   where: {
   *     // ... provide filter here
   *   },
   *   data: {
   *     // ... provide data here
   *   }
   * })
   * 
  **/
  update<T extends operation_alphaUpdateArgs>(
    args: Subset<T, operation_alphaUpdateArgs>
  ): CheckSelect<T, Prisma__operation_alphaClient<operation_alpha>, Prisma__operation_alphaClient<operation_alphaGetPayload<T>>>
  /**
   * Delete zero or more Operation_alphas.
   * @param {operation_alphaDeleteManyArgs} args - Arguments to filter Operation_alphas to delete.
   * @example
   * // Delete a few Operation_alphas
   * const { count } = await prisma.operation_alpha.deleteMany({
   *   where: {
   *     // ... provide filter here
   *   }
   * })
   * 
  **/
  deleteMany<T extends operation_alphaDeleteManyArgs>(
    args: Subset<T, operation_alphaDeleteManyArgs>
  ): Promise<BatchPayload>
  /**
   * Update zero or more Operation_alphas.
   * @param {operation_alphaUpdateManyArgs} args - Arguments to update one or more rows.
   * @example
   * // Update many Operation_alphas
   * const operation_alpha = await prisma.operation_alpha.updateMany({
   *   where: {
   *     // ... provide filter here
   *   },
   *   data: {
   *     // ... provide data here
   *   }
   * })
   * 
  **/
  updateMany<T extends operation_alphaUpdateManyArgs>(
    args: Subset<T, operation_alphaUpdateManyArgs>
  ): Promise<BatchPayload>
  /**
   * Create or update one Operation_alpha.
   * @param {operation_alphaUpsertArgs} args - Arguments to update or create a Operation_alpha.
   * @example
   * // Update or create a Operation_alpha
   * const operation_alpha = await prisma.operation_alpha.upsert({
   *   create: {
   *     // ... data to create a Operation_alpha
   *   },
   *   update: {
   *     // ... in case it already exists, update
   *   },
   *   where: {
   *     // ... the filter for the Operation_alpha we want to update
   *   }
   * })
  **/
  upsert<T extends operation_alphaUpsertArgs>(
    args: Subset<T, operation_alphaUpsertArgs>
  ): CheckSelect<T, Prisma__operation_alphaClient<operation_alpha>, Prisma__operation_alphaClient<operation_alphaGetPayload<T>>>
  /**
   * Count
   */
  count(args?: Omit<FindManyoperation_alphaArgs, 'select' | 'include'>): Promise<number>

  /**
   * Aggregate
   */
  aggregate<T extends AggregateOperation_alphaArgs>(args: Subset<T, AggregateOperation_alphaArgs>): Promise<GetOperation_alphaAggregateType<T>>
}

/**
 * The delegate class that acts as a "Promise-like" for operation_alpha.
 * Why is this prefixed with `Prisma__`?
 * Because we want to prevent naming conflicts as mentioned in 
 * https://github.com/prisma/prisma-client-js/issues/707
 */
export declare class Prisma__operation_alphaClient<T> implements Promise<T> {
  private readonly _dmmf;
  private readonly _fetcher;
  private readonly _queryType;
  private readonly _rootField;
  private readonly _clientMethod;
  private readonly _args;
  private readonly _dataPath;
  private readonly _errorFormat;
  private readonly _measurePerformance?;
  private _isList;
  private _callsite;
  private _requestPromise?;
  constructor(_dmmf: DMMFClass, _fetcher: PrismaClientFetcher, _queryType: 'query' | 'mutation', _rootField: string, _clientMethod: string, _args: any, _dataPath: string[], _errorFormat: ErrorFormat, _measurePerformance?: boolean | undefined, _isList?: boolean);
  readonly [Symbol.toStringTag]: 'PrismaClientPromise';

  operation<T extends operationArgs = {}>(args?: Subset<T, operationArgs>): CheckSelect<T, Prisma__operationClient<operation | null>, Prisma__operationClient<operationGetPayload<T> | null>>;

  addresses_addressesTooperation_alpha_receiver<T extends addressesArgs = {}>(args?: Subset<T, addressesArgs>): CheckSelect<T, Prisma__addressesClient<addresses | null>, Prisma__addressesClient<addressesGetPayload<T> | null>>;

  addresses_addressesTooperation_alpha_sender<T extends addressesArgs = {}>(args?: Subset<T, addressesArgs>): CheckSelect<T, Prisma__addressesClient<addresses | null>, Prisma__addressesClient<addressesGetPayload<T> | null>>;

  delegation<T extends FindManydelegationArgs = {}>(args?: Subset<T, FindManydelegationArgs>): CheckSelect<T, Promise<Array<delegation>>, Promise<Array<delegationGetPayload<T>>>>;

  origination<T extends FindManyoriginationArgs = {}>(args?: Subset<T, FindManyoriginationArgs>): CheckSelect<T, Promise<Array<origination>>, Promise<Array<originationGetPayload<T>>>>;

  tx<T extends FindManytxArgs = {}>(args?: Subset<T, FindManytxArgs>): CheckSelect<T, Promise<Array<tx>>, Promise<Array<txGetPayload<T>>>>;

  private get _document();
  /**
   * Attaches callbacks for the resolution and/or rejection of the Promise.
   * @param onfulfilled The callback to execute when the Promise is resolved.
   * @param onrejected The callback to execute when the Promise is rejected.
   * @returns A Promise for the completion of which ever callback is executed.
   */
  then<TResult1 = T, TResult2 = never>(onfulfilled?: ((value: T) => TResult1 | Promise<TResult1>) | undefined | null, onrejected?: ((reason: any) => TResult2 | Promise<TResult2>) | undefined | null): Promise<TResult1 | TResult2>;
  /**
   * Attaches a callback for only the rejection of the Promise.
   * @param onrejected The callback to execute when the Promise is rejected.
   * @returns A Promise for the completion of the callback.
   */
  catch<TResult = never>(onrejected?: ((reason: any) => TResult | Promise<TResult>) | undefined | null): Promise<T | TResult>;
  /**
   * Attaches a callback that is invoked when the Promise is settled (fulfilled or rejected). The
   * resolved value cannot be modified from the callback.
   * @param onfinally The callback to execute when the Promise is settled (fulfilled or rejected).
   * @returns A Promise for the completion of the callback.
   */
  finally(onfinally?: (() => void) | undefined | null): Promise<T>;
}

// Custom InputTypes

/**
 * operation_alpha findOne
 */
export type FindOneoperation_alphaArgs = {
  /**
   * Select specific fields to fetch from the operation_alpha
  **/
  select?: operation_alphaSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: operation_alphaInclude | null
  /**
   * Filter, which operation_alpha to fetch.
  **/
  where: operation_alphaWhereUniqueInput
}


/**
 * operation_alpha findMany
 */
export type FindManyoperation_alphaArgs = {
  /**
   * Select specific fields to fetch from the operation_alpha
  **/
  select?: operation_alphaSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: operation_alphaInclude | null
  /**
   * Filter, which operation_alphas to fetch.
  **/
  where?: operation_alphaWhereInput
  /**
   * Determine the order of the operation_alphas to fetch.
  **/
  orderBy?: Enumerable<operation_alphaOrderByInput>
  /**
   * Sets the position for listing operation_alphas.
  **/
  cursor?: operation_alphaWhereUniqueInput
  /**
   * The number of operation_alphas to fetch. If negative number, it will take operation_alphas before the `cursor`.
  **/
  take?: number
  /**
   * Skip the first `n` operation_alphas.
  **/
  skip?: number
  distinct?: Enumerable<Operation_alphaDistinctFieldEnum>
}


/**
 * operation_alpha create
 */
export type operation_alphaCreateArgs = {
  /**
   * Select specific fields to fetch from the operation_alpha
  **/
  select?: operation_alphaSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: operation_alphaInclude | null
  /**
   * The data needed to create a operation_alpha.
  **/
  data: operation_alphaCreateInput
}


/**
 * operation_alpha update
 */
export type operation_alphaUpdateArgs = {
  /**
   * Select specific fields to fetch from the operation_alpha
  **/
  select?: operation_alphaSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: operation_alphaInclude | null
  /**
   * The data needed to update a operation_alpha.
  **/
  data: operation_alphaUpdateInput
  /**
   * Choose, which operation_alpha to update.
  **/
  where: operation_alphaWhereUniqueInput
}


/**
 * operation_alpha updateMany
 */
export type operation_alphaUpdateManyArgs = {
  data: operation_alphaUpdateManyMutationInput
  where?: operation_alphaWhereInput
}


/**
 * operation_alpha upsert
 */
export type operation_alphaUpsertArgs = {
  /**
   * Select specific fields to fetch from the operation_alpha
  **/
  select?: operation_alphaSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: operation_alphaInclude | null
  /**
   * The filter to search for the operation_alpha to update in case it exists.
  **/
  where: operation_alphaWhereUniqueInput
  /**
   * In case the operation_alpha found by the `where` argument doesn't exist, create a new operation_alpha with this data.
  **/
  create: operation_alphaCreateInput
  /**
   * In case the operation_alpha was found with the provided `where` argument, update it with this data.
  **/
  update: operation_alphaUpdateInput
}


/**
 * operation_alpha delete
 */
export type operation_alphaDeleteArgs = {
  /**
   * Select specific fields to fetch from the operation_alpha
  **/
  select?: operation_alphaSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: operation_alphaInclude | null
  /**
   * Filter which operation_alpha to delete.
  **/
  where: operation_alphaWhereUniqueInput
}


/**
 * operation_alpha deleteMany
 */
export type operation_alphaDeleteManyArgs = {
  where?: operation_alphaWhereInput
}


/**
 * operation_alpha without action
 */
export type operation_alphaArgs = {
  /**
   * Select specific fields to fetch from the operation_alpha
  **/
  select?: operation_alphaSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: operation_alphaInclude | null
}



/**
 * Model origination
 */

export type origination = {
  operation_hash: string
  op_id: number
  source: string
  k: string
  autoid: number
}


export type AggregateOrigination = {
  count: number
  avg: OriginationAvgAggregateOutputType | null
  sum: OriginationSumAggregateOutputType | null
  min: OriginationMinAggregateOutputType | null
  max: OriginationMaxAggregateOutputType | null
}

export type OriginationAvgAggregateOutputType = {
  op_id: number
  autoid: number
}

export type OriginationSumAggregateOutputType = {
  op_id: number
  autoid: number
}

export type OriginationMinAggregateOutputType = {
  op_id: number
  autoid: number
}

export type OriginationMaxAggregateOutputType = {
  op_id: number
  autoid: number
}


export type OriginationAvgAggregateInputType = {
  op_id?: true
  autoid?: true
}

export type OriginationSumAggregateInputType = {
  op_id?: true
  autoid?: true
}

export type OriginationMinAggregateInputType = {
  op_id?: true
  autoid?: true
}

export type OriginationMaxAggregateInputType = {
  op_id?: true
  autoid?: true
}

export type AggregateOriginationArgs = {
  where?: originationWhereInput
  orderBy?: Enumerable<originationOrderByInput>
  cursor?: originationWhereUniqueInput
  take?: number
  skip?: number
  distinct?: Enumerable<OriginationDistinctFieldEnum>
  count?: true
  avg?: OriginationAvgAggregateInputType
  sum?: OriginationSumAggregateInputType
  min?: OriginationMinAggregateInputType
  max?: OriginationMaxAggregateInputType
}

export type GetOriginationAggregateType<T extends AggregateOriginationArgs> = {
  [P in keyof T]: P extends 'count' ? number : GetOriginationAggregateScalarType<T[P]>
}

export type GetOriginationAggregateScalarType<T extends any> = {
  [P in keyof T]: P extends keyof OriginationAvgAggregateOutputType ? OriginationAvgAggregateOutputType[P] : never
}
    
    

export type originationSelect = {
  operation_hash?: boolean
  op_id?: boolean
  source?: boolean
  k?: boolean
  autoid?: boolean
  contract_contractToorigination_k?: boolean | contractArgs
  operation_alpha?: boolean | operation_alphaArgs
  contract_contractToorigination_source?: boolean | contractArgs
}

export type originationInclude = {
  contract_contractToorigination_k?: boolean | contractArgs
  operation_alpha?: boolean | operation_alphaArgs
  contract_contractToorigination_source?: boolean | contractArgs
}

export type originationGetPayload<
  S extends boolean | null | undefined | originationArgs,
  U = keyof S
> = S extends true
  ? origination
  : S extends undefined
  ? never
  : S extends originationArgs | FindManyoriginationArgs
  ? 'include' extends U
    ? origination  & {
      [P in TrueKeys<S['include']>]:
      P extends 'contract_contractToorigination_k'
      ? contractGetPayload<S['include'][P]> :
      P extends 'operation_alpha'
      ? operation_alphaGetPayload<S['include'][P]> :
      P extends 'contract_contractToorigination_source'
      ? contractGetPayload<S['include'][P]> : never
    }
  : 'select' extends U
    ? {
      [P in TrueKeys<S['select']>]:P extends keyof origination ? origination[P]
: 
      P extends 'contract_contractToorigination_k'
      ? contractGetPayload<S['select'][P]> :
      P extends 'operation_alpha'
      ? operation_alphaGetPayload<S['select'][P]> :
      P extends 'contract_contractToorigination_source'
      ? contractGetPayload<S['select'][P]> : never
    }
  : origination
: origination


export interface originationDelegate {
  /**
   * Find zero or one Origination.
   * @param {FindOneoriginationArgs} args - Arguments to find a Origination
   * @example
   * // Get one Origination
   * const origination = await prisma.origination.findOne({
   *   where: {
   *     // ... provide filter here
   *   }
   * })
  **/
  findOne<T extends FindOneoriginationArgs>(
    args: Subset<T, FindOneoriginationArgs>
  ): CheckSelect<T, Prisma__originationClient<origination | null>, Prisma__originationClient<originationGetPayload<T> | null>>
  /**
   * Find zero or more Originations.
   * @param {FindManyoriginationArgs=} args - Arguments to filter and select certain fields only.
   * @example
   * // Get all Originations
   * const originations = await prisma.origination.findMany()
   * 
   * // Get first 10 Originations
   * const originations = await prisma.origination.findMany({ take: 10 })
   * 
   * // Only select the `operation_hash`
   * const originationWithOperation_hashOnly = await prisma.origination.findMany({ select: { operation_hash: true } })
   * 
  **/
  findMany<T extends FindManyoriginationArgs>(
    args?: Subset<T, FindManyoriginationArgs>
  ): CheckSelect<T, Promise<Array<origination>>, Promise<Array<originationGetPayload<T>>>>
  /**
   * Create a Origination.
   * @param {originationCreateArgs} args - Arguments to create a Origination.
   * @example
   * // Create one Origination
   * const Origination = await prisma.origination.create({
   *   data: {
   *     // ... data to create a Origination
   *   }
   * })
   * 
  **/
  create<T extends originationCreateArgs>(
    args: Subset<T, originationCreateArgs>
  ): CheckSelect<T, Prisma__originationClient<origination>, Prisma__originationClient<originationGetPayload<T>>>
  /**
   * Delete a Origination.
   * @param {originationDeleteArgs} args - Arguments to delete one Origination.
   * @example
   * // Delete one Origination
   * const Origination = await prisma.origination.delete({
   *   where: {
   *     // ... filter to delete one Origination
   *   }
   * })
   * 
  **/
  delete<T extends originationDeleteArgs>(
    args: Subset<T, originationDeleteArgs>
  ): CheckSelect<T, Prisma__originationClient<origination>, Prisma__originationClient<originationGetPayload<T>>>
  /**
   * Update one Origination.
   * @param {originationUpdateArgs} args - Arguments to update one Origination.
   * @example
   * // Update one Origination
   * const origination = await prisma.origination.update({
   *   where: {
   *     // ... provide filter here
   *   },
   *   data: {
   *     // ... provide data here
   *   }
   * })
   * 
  **/
  update<T extends originationUpdateArgs>(
    args: Subset<T, originationUpdateArgs>
  ): CheckSelect<T, Prisma__originationClient<origination>, Prisma__originationClient<originationGetPayload<T>>>
  /**
   * Delete zero or more Originations.
   * @param {originationDeleteManyArgs} args - Arguments to filter Originations to delete.
   * @example
   * // Delete a few Originations
   * const { count } = await prisma.origination.deleteMany({
   *   where: {
   *     // ... provide filter here
   *   }
   * })
   * 
  **/
  deleteMany<T extends originationDeleteManyArgs>(
    args: Subset<T, originationDeleteManyArgs>
  ): Promise<BatchPayload>
  /**
   * Update zero or more Originations.
   * @param {originationUpdateManyArgs} args - Arguments to update one or more rows.
   * @example
   * // Update many Originations
   * const origination = await prisma.origination.updateMany({
   *   where: {
   *     // ... provide filter here
   *   },
   *   data: {
   *     // ... provide data here
   *   }
   * })
   * 
  **/
  updateMany<T extends originationUpdateManyArgs>(
    args: Subset<T, originationUpdateManyArgs>
  ): Promise<BatchPayload>
  /**
   * Create or update one Origination.
   * @param {originationUpsertArgs} args - Arguments to update or create a Origination.
   * @example
   * // Update or create a Origination
   * const origination = await prisma.origination.upsert({
   *   create: {
   *     // ... data to create a Origination
   *   },
   *   update: {
   *     // ... in case it already exists, update
   *   },
   *   where: {
   *     // ... the filter for the Origination we want to update
   *   }
   * })
  **/
  upsert<T extends originationUpsertArgs>(
    args: Subset<T, originationUpsertArgs>
  ): CheckSelect<T, Prisma__originationClient<origination>, Prisma__originationClient<originationGetPayload<T>>>
  /**
   * Count
   */
  count(args?: Omit<FindManyoriginationArgs, 'select' | 'include'>): Promise<number>

  /**
   * Aggregate
   */
  aggregate<T extends AggregateOriginationArgs>(args: Subset<T, AggregateOriginationArgs>): Promise<GetOriginationAggregateType<T>>
}

/**
 * The delegate class that acts as a "Promise-like" for origination.
 * Why is this prefixed with `Prisma__`?
 * Because we want to prevent naming conflicts as mentioned in 
 * https://github.com/prisma/prisma-client-js/issues/707
 */
export declare class Prisma__originationClient<T> implements Promise<T> {
  private readonly _dmmf;
  private readonly _fetcher;
  private readonly _queryType;
  private readonly _rootField;
  private readonly _clientMethod;
  private readonly _args;
  private readonly _dataPath;
  private readonly _errorFormat;
  private readonly _measurePerformance?;
  private _isList;
  private _callsite;
  private _requestPromise?;
  constructor(_dmmf: DMMFClass, _fetcher: PrismaClientFetcher, _queryType: 'query' | 'mutation', _rootField: string, _clientMethod: string, _args: any, _dataPath: string[], _errorFormat: ErrorFormat, _measurePerformance?: boolean | undefined, _isList?: boolean);
  readonly [Symbol.toStringTag]: 'PrismaClientPromise';

  contract_contractToorigination_k<T extends contractArgs = {}>(args?: Subset<T, contractArgs>): CheckSelect<T, Prisma__contractClient<contract | null>, Prisma__contractClient<contractGetPayload<T> | null>>;

  operation_alpha<T extends operation_alphaArgs = {}>(args?: Subset<T, operation_alphaArgs>): CheckSelect<T, Prisma__operation_alphaClient<operation_alpha | null>, Prisma__operation_alphaClient<operation_alphaGetPayload<T> | null>>;

  contract_contractToorigination_source<T extends contractArgs = {}>(args?: Subset<T, contractArgs>): CheckSelect<T, Prisma__contractClient<contract | null>, Prisma__contractClient<contractGetPayload<T> | null>>;

  private get _document();
  /**
   * Attaches callbacks for the resolution and/or rejection of the Promise.
   * @param onfulfilled The callback to execute when the Promise is resolved.
   * @param onrejected The callback to execute when the Promise is rejected.
   * @returns A Promise for the completion of which ever callback is executed.
   */
  then<TResult1 = T, TResult2 = never>(onfulfilled?: ((value: T) => TResult1 | Promise<TResult1>) | undefined | null, onrejected?: ((reason: any) => TResult2 | Promise<TResult2>) | undefined | null): Promise<TResult1 | TResult2>;
  /**
   * Attaches a callback for only the rejection of the Promise.
   * @param onrejected The callback to execute when the Promise is rejected.
   * @returns A Promise for the completion of the callback.
   */
  catch<TResult = never>(onrejected?: ((reason: any) => TResult | Promise<TResult>) | undefined | null): Promise<T | TResult>;
  /**
   * Attaches a callback that is invoked when the Promise is settled (fulfilled or rejected). The
   * resolved value cannot be modified from the callback.
   * @param onfinally The callback to execute when the Promise is settled (fulfilled or rejected).
   * @returns A Promise for the completion of the callback.
   */
  finally(onfinally?: (() => void) | undefined | null): Promise<T>;
}

// Custom InputTypes

/**
 * origination findOne
 */
export type FindOneoriginationArgs = {
  /**
   * Select specific fields to fetch from the origination
  **/
  select?: originationSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: originationInclude | null
  /**
   * Filter, which origination to fetch.
  **/
  where: originationWhereUniqueInput
}


/**
 * origination findMany
 */
export type FindManyoriginationArgs = {
  /**
   * Select specific fields to fetch from the origination
  **/
  select?: originationSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: originationInclude | null
  /**
   * Filter, which originations to fetch.
  **/
  where?: originationWhereInput
  /**
   * Determine the order of the originations to fetch.
  **/
  orderBy?: Enumerable<originationOrderByInput>
  /**
   * Sets the position for listing originations.
  **/
  cursor?: originationWhereUniqueInput
  /**
   * The number of originations to fetch. If negative number, it will take originations before the `cursor`.
  **/
  take?: number
  /**
   * Skip the first `n` originations.
  **/
  skip?: number
  distinct?: Enumerable<OriginationDistinctFieldEnum>
}


/**
 * origination create
 */
export type originationCreateArgs = {
  /**
   * Select specific fields to fetch from the origination
  **/
  select?: originationSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: originationInclude | null
  /**
   * The data needed to create a origination.
  **/
  data: originationCreateInput
}


/**
 * origination update
 */
export type originationUpdateArgs = {
  /**
   * Select specific fields to fetch from the origination
  **/
  select?: originationSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: originationInclude | null
  /**
   * The data needed to update a origination.
  **/
  data: originationUpdateInput
  /**
   * Choose, which origination to update.
  **/
  where: originationWhereUniqueInput
}


/**
 * origination updateMany
 */
export type originationUpdateManyArgs = {
  data: originationUpdateManyMutationInput
  where?: originationWhereInput
}


/**
 * origination upsert
 */
export type originationUpsertArgs = {
  /**
   * Select specific fields to fetch from the origination
  **/
  select?: originationSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: originationInclude | null
  /**
   * The filter to search for the origination to update in case it exists.
  **/
  where: originationWhereUniqueInput
  /**
   * In case the origination found by the `where` argument doesn't exist, create a new origination with this data.
  **/
  create: originationCreateInput
  /**
   * In case the origination was found with the provided `where` argument, update it with this data.
  **/
  update: originationUpdateInput
}


/**
 * origination delete
 */
export type originationDeleteArgs = {
  /**
   * Select specific fields to fetch from the origination
  **/
  select?: originationSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: originationInclude | null
  /**
   * Filter which origination to delete.
  **/
  where: originationWhereUniqueInput
}


/**
 * origination deleteMany
 */
export type originationDeleteManyArgs = {
  where?: originationWhereInput
}


/**
 * origination without action
 */
export type originationArgs = {
  /**
   * Select specific fields to fetch from the origination
  **/
  select?: originationSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: originationInclude | null
}



/**
 * Model snapshot
 */

export type snapshot = {
  cycle: number
  level: number
}


export type AggregateSnapshot = {
  count: number
  avg: SnapshotAvgAggregateOutputType | null
  sum: SnapshotSumAggregateOutputType | null
  min: SnapshotMinAggregateOutputType | null
  max: SnapshotMaxAggregateOutputType | null
}

export type SnapshotAvgAggregateOutputType = {
  cycle: number
  level: number
}

export type SnapshotSumAggregateOutputType = {
  cycle: number
  level: number
}

export type SnapshotMinAggregateOutputType = {
  cycle: number
  level: number
}

export type SnapshotMaxAggregateOutputType = {
  cycle: number
  level: number
}


export type SnapshotAvgAggregateInputType = {
  cycle?: true
  level?: true
}

export type SnapshotSumAggregateInputType = {
  cycle?: true
  level?: true
}

export type SnapshotMinAggregateInputType = {
  cycle?: true
  level?: true
}

export type SnapshotMaxAggregateInputType = {
  cycle?: true
  level?: true
}

export type AggregateSnapshotArgs = {
  where?: snapshotWhereInput
  orderBy?: Enumerable<snapshotOrderByInput>
  cursor?: snapshotWhereUniqueInput
  take?: number
  skip?: number
  distinct?: Enumerable<SnapshotDistinctFieldEnum>
  count?: true
  avg?: SnapshotAvgAggregateInputType
  sum?: SnapshotSumAggregateInputType
  min?: SnapshotMinAggregateInputType
  max?: SnapshotMaxAggregateInputType
}

export type GetSnapshotAggregateType<T extends AggregateSnapshotArgs> = {
  [P in keyof T]: P extends 'count' ? number : GetSnapshotAggregateScalarType<T[P]>
}

export type GetSnapshotAggregateScalarType<T extends any> = {
  [P in keyof T]: P extends keyof SnapshotAvgAggregateOutputType ? SnapshotAvgAggregateOutputType[P] : never
}
    
    

export type snapshotSelect = {
  cycle?: boolean
  level?: boolean
  delegated_contract?: boolean | FindManydelegated_contractArgs
}

export type snapshotInclude = {
  delegated_contract?: boolean | FindManydelegated_contractArgs
}

export type snapshotGetPayload<
  S extends boolean | null | undefined | snapshotArgs,
  U = keyof S
> = S extends true
  ? snapshot
  : S extends undefined
  ? never
  : S extends snapshotArgs | FindManysnapshotArgs
  ? 'include' extends U
    ? snapshot  & {
      [P in TrueKeys<S['include']>]:
      P extends 'delegated_contract'
      ? Array<delegated_contractGetPayload<S['include'][P]>> : never
    }
  : 'select' extends U
    ? {
      [P in TrueKeys<S['select']>]:P extends keyof snapshot ? snapshot[P]
: 
      P extends 'delegated_contract'
      ? Array<delegated_contractGetPayload<S['select'][P]>> : never
    }
  : snapshot
: snapshot


export interface snapshotDelegate {
  /**
   * Find zero or one Snapshot.
   * @param {FindOnesnapshotArgs} args - Arguments to find a Snapshot
   * @example
   * // Get one Snapshot
   * const snapshot = await prisma.snapshot.findOne({
   *   where: {
   *     // ... provide filter here
   *   }
   * })
  **/
  findOne<T extends FindOnesnapshotArgs>(
    args: Subset<T, FindOnesnapshotArgs>
  ): CheckSelect<T, Prisma__snapshotClient<snapshot | null>, Prisma__snapshotClient<snapshotGetPayload<T> | null>>
  /**
   * Find zero or more Snapshots.
   * @param {FindManysnapshotArgs=} args - Arguments to filter and select certain fields only.
   * @example
   * // Get all Snapshots
   * const snapshots = await prisma.snapshot.findMany()
   * 
   * // Get first 10 Snapshots
   * const snapshots = await prisma.snapshot.findMany({ take: 10 })
   * 
   * // Only select the `cycle`
   * const snapshotWithCycleOnly = await prisma.snapshot.findMany({ select: { cycle: true } })
   * 
  **/
  findMany<T extends FindManysnapshotArgs>(
    args?: Subset<T, FindManysnapshotArgs>
  ): CheckSelect<T, Promise<Array<snapshot>>, Promise<Array<snapshotGetPayload<T>>>>
  /**
   * Create a Snapshot.
   * @param {snapshotCreateArgs} args - Arguments to create a Snapshot.
   * @example
   * // Create one Snapshot
   * const Snapshot = await prisma.snapshot.create({
   *   data: {
   *     // ... data to create a Snapshot
   *   }
   * })
   * 
  **/
  create<T extends snapshotCreateArgs>(
    args: Subset<T, snapshotCreateArgs>
  ): CheckSelect<T, Prisma__snapshotClient<snapshot>, Prisma__snapshotClient<snapshotGetPayload<T>>>
  /**
   * Delete a Snapshot.
   * @param {snapshotDeleteArgs} args - Arguments to delete one Snapshot.
   * @example
   * // Delete one Snapshot
   * const Snapshot = await prisma.snapshot.delete({
   *   where: {
   *     // ... filter to delete one Snapshot
   *   }
   * })
   * 
  **/
  delete<T extends snapshotDeleteArgs>(
    args: Subset<T, snapshotDeleteArgs>
  ): CheckSelect<T, Prisma__snapshotClient<snapshot>, Prisma__snapshotClient<snapshotGetPayload<T>>>
  /**
   * Update one Snapshot.
   * @param {snapshotUpdateArgs} args - Arguments to update one Snapshot.
   * @example
   * // Update one Snapshot
   * const snapshot = await prisma.snapshot.update({
   *   where: {
   *     // ... provide filter here
   *   },
   *   data: {
   *     // ... provide data here
   *   }
   * })
   * 
  **/
  update<T extends snapshotUpdateArgs>(
    args: Subset<T, snapshotUpdateArgs>
  ): CheckSelect<T, Prisma__snapshotClient<snapshot>, Prisma__snapshotClient<snapshotGetPayload<T>>>
  /**
   * Delete zero or more Snapshots.
   * @param {snapshotDeleteManyArgs} args - Arguments to filter Snapshots to delete.
   * @example
   * // Delete a few Snapshots
   * const { count } = await prisma.snapshot.deleteMany({
   *   where: {
   *     // ... provide filter here
   *   }
   * })
   * 
  **/
  deleteMany<T extends snapshotDeleteManyArgs>(
    args: Subset<T, snapshotDeleteManyArgs>
  ): Promise<BatchPayload>
  /**
   * Update zero or more Snapshots.
   * @param {snapshotUpdateManyArgs} args - Arguments to update one or more rows.
   * @example
   * // Update many Snapshots
   * const snapshot = await prisma.snapshot.updateMany({
   *   where: {
   *     // ... provide filter here
   *   },
   *   data: {
   *     // ... provide data here
   *   }
   * })
   * 
  **/
  updateMany<T extends snapshotUpdateManyArgs>(
    args: Subset<T, snapshotUpdateManyArgs>
  ): Promise<BatchPayload>
  /**
   * Create or update one Snapshot.
   * @param {snapshotUpsertArgs} args - Arguments to update or create a Snapshot.
   * @example
   * // Update or create a Snapshot
   * const snapshot = await prisma.snapshot.upsert({
   *   create: {
   *     // ... data to create a Snapshot
   *   },
   *   update: {
   *     // ... in case it already exists, update
   *   },
   *   where: {
   *     // ... the filter for the Snapshot we want to update
   *   }
   * })
  **/
  upsert<T extends snapshotUpsertArgs>(
    args: Subset<T, snapshotUpsertArgs>
  ): CheckSelect<T, Prisma__snapshotClient<snapshot>, Prisma__snapshotClient<snapshotGetPayload<T>>>
  /**
   * Count
   */
  count(args?: Omit<FindManysnapshotArgs, 'select' | 'include'>): Promise<number>

  /**
   * Aggregate
   */
  aggregate<T extends AggregateSnapshotArgs>(args: Subset<T, AggregateSnapshotArgs>): Promise<GetSnapshotAggregateType<T>>
}

/**
 * The delegate class that acts as a "Promise-like" for snapshot.
 * Why is this prefixed with `Prisma__`?
 * Because we want to prevent naming conflicts as mentioned in 
 * https://github.com/prisma/prisma-client-js/issues/707
 */
export declare class Prisma__snapshotClient<T> implements Promise<T> {
  private readonly _dmmf;
  private readonly _fetcher;
  private readonly _queryType;
  private readonly _rootField;
  private readonly _clientMethod;
  private readonly _args;
  private readonly _dataPath;
  private readonly _errorFormat;
  private readonly _measurePerformance?;
  private _isList;
  private _callsite;
  private _requestPromise?;
  constructor(_dmmf: DMMFClass, _fetcher: PrismaClientFetcher, _queryType: 'query' | 'mutation', _rootField: string, _clientMethod: string, _args: any, _dataPath: string[], _errorFormat: ErrorFormat, _measurePerformance?: boolean | undefined, _isList?: boolean);
  readonly [Symbol.toStringTag]: 'PrismaClientPromise';

  delegated_contract<T extends FindManydelegated_contractArgs = {}>(args?: Subset<T, FindManydelegated_contractArgs>): CheckSelect<T, Promise<Array<delegated_contract>>, Promise<Array<delegated_contractGetPayload<T>>>>;

  private get _document();
  /**
   * Attaches callbacks for the resolution and/or rejection of the Promise.
   * @param onfulfilled The callback to execute when the Promise is resolved.
   * @param onrejected The callback to execute when the Promise is rejected.
   * @returns A Promise for the completion of which ever callback is executed.
   */
  then<TResult1 = T, TResult2 = never>(onfulfilled?: ((value: T) => TResult1 | Promise<TResult1>) | undefined | null, onrejected?: ((reason: any) => TResult2 | Promise<TResult2>) | undefined | null): Promise<TResult1 | TResult2>;
  /**
   * Attaches a callback for only the rejection of the Promise.
   * @param onrejected The callback to execute when the Promise is rejected.
   * @returns A Promise for the completion of the callback.
   */
  catch<TResult = never>(onrejected?: ((reason: any) => TResult | Promise<TResult>) | undefined | null): Promise<T | TResult>;
  /**
   * Attaches a callback that is invoked when the Promise is settled (fulfilled or rejected). The
   * resolved value cannot be modified from the callback.
   * @param onfinally The callback to execute when the Promise is settled (fulfilled or rejected).
   * @returns A Promise for the completion of the callback.
   */
  finally(onfinally?: (() => void) | undefined | null): Promise<T>;
}

// Custom InputTypes

/**
 * snapshot findOne
 */
export type FindOnesnapshotArgs = {
  /**
   * Select specific fields to fetch from the snapshot
  **/
  select?: snapshotSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: snapshotInclude | null
  /**
   * Filter, which snapshot to fetch.
  **/
  where: snapshotWhereUniqueInput
}


/**
 * snapshot findMany
 */
export type FindManysnapshotArgs = {
  /**
   * Select specific fields to fetch from the snapshot
  **/
  select?: snapshotSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: snapshotInclude | null
  /**
   * Filter, which snapshots to fetch.
  **/
  where?: snapshotWhereInput
  /**
   * Determine the order of the snapshots to fetch.
  **/
  orderBy?: Enumerable<snapshotOrderByInput>
  /**
   * Sets the position for listing snapshots.
  **/
  cursor?: snapshotWhereUniqueInput
  /**
   * The number of snapshots to fetch. If negative number, it will take snapshots before the `cursor`.
  **/
  take?: number
  /**
   * Skip the first `n` snapshots.
  **/
  skip?: number
  distinct?: Enumerable<SnapshotDistinctFieldEnum>
}


/**
 * snapshot create
 */
export type snapshotCreateArgs = {
  /**
   * Select specific fields to fetch from the snapshot
  **/
  select?: snapshotSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: snapshotInclude | null
  /**
   * The data needed to create a snapshot.
  **/
  data: snapshotCreateInput
}


/**
 * snapshot update
 */
export type snapshotUpdateArgs = {
  /**
   * Select specific fields to fetch from the snapshot
  **/
  select?: snapshotSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: snapshotInclude | null
  /**
   * The data needed to update a snapshot.
  **/
  data: snapshotUpdateInput
  /**
   * Choose, which snapshot to update.
  **/
  where: snapshotWhereUniqueInput
}


/**
 * snapshot updateMany
 */
export type snapshotUpdateManyArgs = {
  data: snapshotUpdateManyMutationInput
  where?: snapshotWhereInput
}


/**
 * snapshot upsert
 */
export type snapshotUpsertArgs = {
  /**
   * Select specific fields to fetch from the snapshot
  **/
  select?: snapshotSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: snapshotInclude | null
  /**
   * The filter to search for the snapshot to update in case it exists.
  **/
  where: snapshotWhereUniqueInput
  /**
   * In case the snapshot found by the `where` argument doesn't exist, create a new snapshot with this data.
  **/
  create: snapshotCreateInput
  /**
   * In case the snapshot was found with the provided `where` argument, update it with this data.
  **/
  update: snapshotUpdateInput
}


/**
 * snapshot delete
 */
export type snapshotDeleteArgs = {
  /**
   * Select specific fields to fetch from the snapshot
  **/
  select?: snapshotSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: snapshotInclude | null
  /**
   * Filter which snapshot to delete.
  **/
  where: snapshotWhereUniqueInput
}


/**
 * snapshot deleteMany
 */
export type snapshotDeleteManyArgs = {
  where?: snapshotWhereInput
}


/**
 * snapshot without action
 */
export type snapshotArgs = {
  /**
   * Select specific fields to fetch from the snapshot
  **/
  select?: snapshotSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: snapshotInclude | null
}



/**
 * Model tx
 */

export type tx = {
  operation_hash: string
  op_id: number
  source: string
  destination: string
  fee: number
  amount: number
  parameters: string | null
  storage: string | null
  consumed_gas: string
  storage_size: string
  paid_storage_size_diff: string
  entrypoint: string | null
  bigmapdiff: string | null
  autoid: number
}


export type AggregateTx = {
  count: number
  avg: TxAvgAggregateOutputType | null
  sum: TxSumAggregateOutputType | null
  min: TxMinAggregateOutputType | null
  max: TxMaxAggregateOutputType | null
}

export type TxAvgAggregateOutputType = {
  op_id: number
  fee: number
  amount: number
  autoid: number
}

export type TxSumAggregateOutputType = {
  op_id: number
  fee: number
  amount: number
  autoid: number
}

export type TxMinAggregateOutputType = {
  op_id: number
  fee: number
  amount: number
  autoid: number
}

export type TxMaxAggregateOutputType = {
  op_id: number
  fee: number
  amount: number
  autoid: number
}


export type TxAvgAggregateInputType = {
  op_id?: true
  fee?: true
  amount?: true
  autoid?: true
}

export type TxSumAggregateInputType = {
  op_id?: true
  fee?: true
  amount?: true
  autoid?: true
}

export type TxMinAggregateInputType = {
  op_id?: true
  fee?: true
  amount?: true
  autoid?: true
}

export type TxMaxAggregateInputType = {
  op_id?: true
  fee?: true
  amount?: true
  autoid?: true
}

export type AggregateTxArgs = {
  where?: txWhereInput
  orderBy?: Enumerable<txOrderByInput>
  cursor?: txWhereUniqueInput
  take?: number
  skip?: number
  distinct?: Enumerable<TxDistinctFieldEnum>
  count?: true
  avg?: TxAvgAggregateInputType
  sum?: TxSumAggregateInputType
  min?: TxMinAggregateInputType
  max?: TxMaxAggregateInputType
}

export type GetTxAggregateType<T extends AggregateTxArgs> = {
  [P in keyof T]: P extends 'count' ? number : GetTxAggregateScalarType<T[P]>
}

export type GetTxAggregateScalarType<T extends any> = {
  [P in keyof T]: P extends keyof TxAvgAggregateOutputType ? TxAvgAggregateOutputType[P] : never
}
    
    

export type txSelect = {
  operation_hash?: boolean
  op_id?: boolean
  source?: boolean
  destination?: boolean
  fee?: boolean
  amount?: boolean
  parameters?: boolean
  storage?: boolean
  consumed_gas?: boolean
  storage_size?: boolean
  paid_storage_size_diff?: boolean
  entrypoint?: boolean
  bigmapdiff?: boolean
  autoid?: boolean
  contract_contractTotx_destination?: boolean | contractArgs
  operation_alpha?: boolean | operation_alphaArgs
  contract_contractTotx_source?: boolean | contractArgs
}

export type txInclude = {
  contract_contractTotx_destination?: boolean | contractArgs
  operation_alpha?: boolean | operation_alphaArgs
  contract_contractTotx_source?: boolean | contractArgs
}

export type txGetPayload<
  S extends boolean | null | undefined | txArgs,
  U = keyof S
> = S extends true
  ? tx
  : S extends undefined
  ? never
  : S extends txArgs | FindManytxArgs
  ? 'include' extends U
    ? tx  & {
      [P in TrueKeys<S['include']>]:
      P extends 'contract_contractTotx_destination'
      ? contractGetPayload<S['include'][P]> :
      P extends 'operation_alpha'
      ? operation_alphaGetPayload<S['include'][P]> :
      P extends 'contract_contractTotx_source'
      ? contractGetPayload<S['include'][P]> : never
    }
  : 'select' extends U
    ? {
      [P in TrueKeys<S['select']>]:P extends keyof tx ? tx[P]
: 
      P extends 'contract_contractTotx_destination'
      ? contractGetPayload<S['select'][P]> :
      P extends 'operation_alpha'
      ? operation_alphaGetPayload<S['select'][P]> :
      P extends 'contract_contractTotx_source'
      ? contractGetPayload<S['select'][P]> : never
    }
  : tx
: tx


export interface txDelegate {
  /**
   * Find zero or one Tx.
   * @param {FindOnetxArgs} args - Arguments to find a Tx
   * @example
   * // Get one Tx
   * const tx = await prisma.tx.findOne({
   *   where: {
   *     // ... provide filter here
   *   }
   * })
  **/
  findOne<T extends FindOnetxArgs>(
    args: Subset<T, FindOnetxArgs>
  ): CheckSelect<T, Prisma__txClient<tx | null>, Prisma__txClient<txGetPayload<T> | null>>
  /**
   * Find zero or more Txes.
   * @param {FindManytxArgs=} args - Arguments to filter and select certain fields only.
   * @example
   * // Get all Txes
   * const txes = await prisma.tx.findMany()
   * 
   * // Get first 10 Txes
   * const txes = await prisma.tx.findMany({ take: 10 })
   * 
   * // Only select the `operation_hash`
   * const txWithOperation_hashOnly = await prisma.tx.findMany({ select: { operation_hash: true } })
   * 
  **/
  findMany<T extends FindManytxArgs>(
    args?: Subset<T, FindManytxArgs>
  ): CheckSelect<T, Promise<Array<tx>>, Promise<Array<txGetPayload<T>>>>
  /**
   * Create a Tx.
   * @param {txCreateArgs} args - Arguments to create a Tx.
   * @example
   * // Create one Tx
   * const Tx = await prisma.tx.create({
   *   data: {
   *     // ... data to create a Tx
   *   }
   * })
   * 
  **/
  create<T extends txCreateArgs>(
    args: Subset<T, txCreateArgs>
  ): CheckSelect<T, Prisma__txClient<tx>, Prisma__txClient<txGetPayload<T>>>
  /**
   * Delete a Tx.
   * @param {txDeleteArgs} args - Arguments to delete one Tx.
   * @example
   * // Delete one Tx
   * const Tx = await prisma.tx.delete({
   *   where: {
   *     // ... filter to delete one Tx
   *   }
   * })
   * 
  **/
  delete<T extends txDeleteArgs>(
    args: Subset<T, txDeleteArgs>
  ): CheckSelect<T, Prisma__txClient<tx>, Prisma__txClient<txGetPayload<T>>>
  /**
   * Update one Tx.
   * @param {txUpdateArgs} args - Arguments to update one Tx.
   * @example
   * // Update one Tx
   * const tx = await prisma.tx.update({
   *   where: {
   *     // ... provide filter here
   *   },
   *   data: {
   *     // ... provide data here
   *   }
   * })
   * 
  **/
  update<T extends txUpdateArgs>(
    args: Subset<T, txUpdateArgs>
  ): CheckSelect<T, Prisma__txClient<tx>, Prisma__txClient<txGetPayload<T>>>
  /**
   * Delete zero or more Txes.
   * @param {txDeleteManyArgs} args - Arguments to filter Txes to delete.
   * @example
   * // Delete a few Txes
   * const { count } = await prisma.tx.deleteMany({
   *   where: {
   *     // ... provide filter here
   *   }
   * })
   * 
  **/
  deleteMany<T extends txDeleteManyArgs>(
    args: Subset<T, txDeleteManyArgs>
  ): Promise<BatchPayload>
  /**
   * Update zero or more Txes.
   * @param {txUpdateManyArgs} args - Arguments to update one or more rows.
   * @example
   * // Update many Txes
   * const tx = await prisma.tx.updateMany({
   *   where: {
   *     // ... provide filter here
   *   },
   *   data: {
   *     // ... provide data here
   *   }
   * })
   * 
  **/
  updateMany<T extends txUpdateManyArgs>(
    args: Subset<T, txUpdateManyArgs>
  ): Promise<BatchPayload>
  /**
   * Create or update one Tx.
   * @param {txUpsertArgs} args - Arguments to update or create a Tx.
   * @example
   * // Update or create a Tx
   * const tx = await prisma.tx.upsert({
   *   create: {
   *     // ... data to create a Tx
   *   },
   *   update: {
   *     // ... in case it already exists, update
   *   },
   *   where: {
   *     // ... the filter for the Tx we want to update
   *   }
   * })
  **/
  upsert<T extends txUpsertArgs>(
    args: Subset<T, txUpsertArgs>
  ): CheckSelect<T, Prisma__txClient<tx>, Prisma__txClient<txGetPayload<T>>>
  /**
   * Count
   */
  count(args?: Omit<FindManytxArgs, 'select' | 'include'>): Promise<number>

  /**
   * Aggregate
   */
  aggregate<T extends AggregateTxArgs>(args: Subset<T, AggregateTxArgs>): Promise<GetTxAggregateType<T>>
}

/**
 * The delegate class that acts as a "Promise-like" for tx.
 * Why is this prefixed with `Prisma__`?
 * Because we want to prevent naming conflicts as mentioned in 
 * https://github.com/prisma/prisma-client-js/issues/707
 */
export declare class Prisma__txClient<T> implements Promise<T> {
  private readonly _dmmf;
  private readonly _fetcher;
  private readonly _queryType;
  private readonly _rootField;
  private readonly _clientMethod;
  private readonly _args;
  private readonly _dataPath;
  private readonly _errorFormat;
  private readonly _measurePerformance?;
  private _isList;
  private _callsite;
  private _requestPromise?;
  constructor(_dmmf: DMMFClass, _fetcher: PrismaClientFetcher, _queryType: 'query' | 'mutation', _rootField: string, _clientMethod: string, _args: any, _dataPath: string[], _errorFormat: ErrorFormat, _measurePerformance?: boolean | undefined, _isList?: boolean);
  readonly [Symbol.toStringTag]: 'PrismaClientPromise';

  contract_contractTotx_destination<T extends contractArgs = {}>(args?: Subset<T, contractArgs>): CheckSelect<T, Prisma__contractClient<contract | null>, Prisma__contractClient<contractGetPayload<T> | null>>;

  operation_alpha<T extends operation_alphaArgs = {}>(args?: Subset<T, operation_alphaArgs>): CheckSelect<T, Prisma__operation_alphaClient<operation_alpha | null>, Prisma__operation_alphaClient<operation_alphaGetPayload<T> | null>>;

  contract_contractTotx_source<T extends contractArgs = {}>(args?: Subset<T, contractArgs>): CheckSelect<T, Prisma__contractClient<contract | null>, Prisma__contractClient<contractGetPayload<T> | null>>;

  private get _document();
  /**
   * Attaches callbacks for the resolution and/or rejection of the Promise.
   * @param onfulfilled The callback to execute when the Promise is resolved.
   * @param onrejected The callback to execute when the Promise is rejected.
   * @returns A Promise for the completion of which ever callback is executed.
   */
  then<TResult1 = T, TResult2 = never>(onfulfilled?: ((value: T) => TResult1 | Promise<TResult1>) | undefined | null, onrejected?: ((reason: any) => TResult2 | Promise<TResult2>) | undefined | null): Promise<TResult1 | TResult2>;
  /**
   * Attaches a callback for only the rejection of the Promise.
   * @param onrejected The callback to execute when the Promise is rejected.
   * @returns A Promise for the completion of the callback.
   */
  catch<TResult = never>(onrejected?: ((reason: any) => TResult | Promise<TResult>) | undefined | null): Promise<T | TResult>;
  /**
   * Attaches a callback that is invoked when the Promise is settled (fulfilled or rejected). The
   * resolved value cannot be modified from the callback.
   * @param onfinally The callback to execute when the Promise is settled (fulfilled or rejected).
   * @returns A Promise for the completion of the callback.
   */
  finally(onfinally?: (() => void) | undefined | null): Promise<T>;
}

// Custom InputTypes

/**
 * tx findOne
 */
export type FindOnetxArgs = {
  /**
   * Select specific fields to fetch from the tx
  **/
  select?: txSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: txInclude | null
  /**
   * Filter, which tx to fetch.
  **/
  where: txWhereUniqueInput
}


/**
 * tx findMany
 */
export type FindManytxArgs = {
  /**
   * Select specific fields to fetch from the tx
  **/
  select?: txSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: txInclude | null
  /**
   * Filter, which txes to fetch.
  **/
  where?: txWhereInput
  /**
   * Determine the order of the txes to fetch.
  **/
  orderBy?: Enumerable<txOrderByInput>
  /**
   * Sets the position for listing txes.
  **/
  cursor?: txWhereUniqueInput
  /**
   * The number of txes to fetch. If negative number, it will take txes before the `cursor`.
  **/
  take?: number
  /**
   * Skip the first `n` txes.
  **/
  skip?: number
  distinct?: Enumerable<TxDistinctFieldEnum>
}


/**
 * tx create
 */
export type txCreateArgs = {
  /**
   * Select specific fields to fetch from the tx
  **/
  select?: txSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: txInclude | null
  /**
   * The data needed to create a tx.
  **/
  data: txCreateInput
}


/**
 * tx update
 */
export type txUpdateArgs = {
  /**
   * Select specific fields to fetch from the tx
  **/
  select?: txSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: txInclude | null
  /**
   * The data needed to update a tx.
  **/
  data: txUpdateInput
  /**
   * Choose, which tx to update.
  **/
  where: txWhereUniqueInput
}


/**
 * tx updateMany
 */
export type txUpdateManyArgs = {
  data: txUpdateManyMutationInput
  where?: txWhereInput
}


/**
 * tx upsert
 */
export type txUpsertArgs = {
  /**
   * Select specific fields to fetch from the tx
  **/
  select?: txSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: txInclude | null
  /**
   * The filter to search for the tx to update in case it exists.
  **/
  where: txWhereUniqueInput
  /**
   * In case the tx found by the `where` argument doesn't exist, create a new tx with this data.
  **/
  create: txCreateInput
  /**
   * In case the tx was found with the provided `where` argument, update it with this data.
  **/
  update: txUpdateInput
}


/**
 * tx delete
 */
export type txDeleteArgs = {
  /**
   * Select specific fields to fetch from the tx
  **/
  select?: txSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: txInclude | null
  /**
   * Filter which tx to delete.
  **/
  where: txWhereUniqueInput
}


/**
 * tx deleteMany
 */
export type txDeleteManyArgs = {
  where?: txWhereInput
}


/**
 * tx without action
 */
export type txArgs = {
  /**
   * Select specific fields to fetch from the tx
  **/
  select?: txSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: txInclude | null
}



/**
 * Model addresses
 */

export type addresses = {
  address: string
}


export type AggregateAddresses = {
  count: number
}



export type AggregateAddressesArgs = {
  where?: addressesWhereInput
  orderBy?: Enumerable<addressesOrderByInput>
  cursor?: addressesWhereUniqueInput
  take?: number
  skip?: number
  distinct?: Enumerable<AddressesDistinctFieldEnum>
  count?: true
}

export type GetAddressesAggregateType<T extends AggregateAddressesArgs> = {
  [P in keyof T]: P extends 'count' ? number : never
}


    
    

export type addressesSelect = {
  address?: boolean
  contract_addressesTocontract_address?: boolean | FindManycontractArgs
  contract_addressesTocontract_delegate?: boolean | FindManycontractArgs
  contract_addressesTocontract_mgr?: boolean | FindManycontractArgs
  contract_addressesTocontract_preorig?: boolean | FindManycontractArgs
  delegation?: boolean | FindManydelegationArgs
  implicit?: boolean | FindManyimplicitArgs
  operation_alpha_addressesTooperation_alpha_receiver?: boolean | FindManyoperation_alphaArgs
  operation_alpha_addressesTooperation_alpha_sender?: boolean | FindManyoperation_alphaArgs
}

export type addressesInclude = {
  contract_addressesTocontract_address?: boolean | FindManycontractArgs
  contract_addressesTocontract_delegate?: boolean | FindManycontractArgs
  contract_addressesTocontract_mgr?: boolean | FindManycontractArgs
  contract_addressesTocontract_preorig?: boolean | FindManycontractArgs
  delegation?: boolean | FindManydelegationArgs
  implicit?: boolean | FindManyimplicitArgs
  operation_alpha_addressesTooperation_alpha_receiver?: boolean | FindManyoperation_alphaArgs
  operation_alpha_addressesTooperation_alpha_sender?: boolean | FindManyoperation_alphaArgs
}

export type addressesGetPayload<
  S extends boolean | null | undefined | addressesArgs,
  U = keyof S
> = S extends true
  ? addresses
  : S extends undefined
  ? never
  : S extends addressesArgs | FindManyaddressesArgs
  ? 'include' extends U
    ? addresses  & {
      [P in TrueKeys<S['include']>]:
      P extends 'contract_addressesTocontract_address'
      ? Array<contractGetPayload<S['include'][P]>> :
      P extends 'contract_addressesTocontract_delegate'
      ? Array<contractGetPayload<S['include'][P]>> :
      P extends 'contract_addressesTocontract_mgr'
      ? Array<contractGetPayload<S['include'][P]>> :
      P extends 'contract_addressesTocontract_preorig'
      ? Array<contractGetPayload<S['include'][P]>> :
      P extends 'delegation'
      ? Array<delegationGetPayload<S['include'][P]>> :
      P extends 'implicit'
      ? Array<implicitGetPayload<S['include'][P]>> :
      P extends 'operation_alpha_addressesTooperation_alpha_receiver'
      ? Array<operation_alphaGetPayload<S['include'][P]>> :
      P extends 'operation_alpha_addressesTooperation_alpha_sender'
      ? Array<operation_alphaGetPayload<S['include'][P]>> : never
    }
  : 'select' extends U
    ? {
      [P in TrueKeys<S['select']>]:P extends keyof addresses ? addresses[P]
: 
      P extends 'contract_addressesTocontract_address'
      ? Array<contractGetPayload<S['select'][P]>> :
      P extends 'contract_addressesTocontract_delegate'
      ? Array<contractGetPayload<S['select'][P]>> :
      P extends 'contract_addressesTocontract_mgr'
      ? Array<contractGetPayload<S['select'][P]>> :
      P extends 'contract_addressesTocontract_preorig'
      ? Array<contractGetPayload<S['select'][P]>> :
      P extends 'delegation'
      ? Array<delegationGetPayload<S['select'][P]>> :
      P extends 'implicit'
      ? Array<implicitGetPayload<S['select'][P]>> :
      P extends 'operation_alpha_addressesTooperation_alpha_receiver'
      ? Array<operation_alphaGetPayload<S['select'][P]>> :
      P extends 'operation_alpha_addressesTooperation_alpha_sender'
      ? Array<operation_alphaGetPayload<S['select'][P]>> : never
    }
  : addresses
: addresses


export interface addressesDelegate {
  /**
   * Find zero or one Addresses.
   * @param {FindOneaddressesArgs} args - Arguments to find a Addresses
   * @example
   * // Get one Addresses
   * const addresses = await prisma.addresses.findOne({
   *   where: {
   *     // ... provide filter here
   *   }
   * })
  **/
  findOne<T extends FindOneaddressesArgs>(
    args: Subset<T, FindOneaddressesArgs>
  ): CheckSelect<T, Prisma__addressesClient<addresses | null>, Prisma__addressesClient<addressesGetPayload<T> | null>>
  /**
   * Find zero or more Addresses.
   * @param {FindManyaddressesArgs=} args - Arguments to filter and select certain fields only.
   * @example
   * // Get all Addresses
   * const addresses = await prisma.addresses.findMany()
   * 
   * // Get first 10 Addresses
   * const addresses = await prisma.addresses.findMany({ take: 10 })
   * 
   * // Only select the `address`
   * const addressesWithAddressOnly = await prisma.addresses.findMany({ select: { address: true } })
   * 
  **/
  findMany<T extends FindManyaddressesArgs>(
    args?: Subset<T, FindManyaddressesArgs>
  ): CheckSelect<T, Promise<Array<addresses>>, Promise<Array<addressesGetPayload<T>>>>
  /**
   * Create a Addresses.
   * @param {addressesCreateArgs} args - Arguments to create a Addresses.
   * @example
   * // Create one Addresses
   * const Addresses = await prisma.addresses.create({
   *   data: {
   *     // ... data to create a Addresses
   *   }
   * })
   * 
  **/
  create<T extends addressesCreateArgs>(
    args: Subset<T, addressesCreateArgs>
  ): CheckSelect<T, Prisma__addressesClient<addresses>, Prisma__addressesClient<addressesGetPayload<T>>>
  /**
   * Delete a Addresses.
   * @param {addressesDeleteArgs} args - Arguments to delete one Addresses.
   * @example
   * // Delete one Addresses
   * const Addresses = await prisma.addresses.delete({
   *   where: {
   *     // ... filter to delete one Addresses
   *   }
   * })
   * 
  **/
  delete<T extends addressesDeleteArgs>(
    args: Subset<T, addressesDeleteArgs>
  ): CheckSelect<T, Prisma__addressesClient<addresses>, Prisma__addressesClient<addressesGetPayload<T>>>
  /**
   * Update one Addresses.
   * @param {addressesUpdateArgs} args - Arguments to update one Addresses.
   * @example
   * // Update one Addresses
   * const addresses = await prisma.addresses.update({
   *   where: {
   *     // ... provide filter here
   *   },
   *   data: {
   *     // ... provide data here
   *   }
   * })
   * 
  **/
  update<T extends addressesUpdateArgs>(
    args: Subset<T, addressesUpdateArgs>
  ): CheckSelect<T, Prisma__addressesClient<addresses>, Prisma__addressesClient<addressesGetPayload<T>>>
  /**
   * Delete zero or more Addresses.
   * @param {addressesDeleteManyArgs} args - Arguments to filter Addresses to delete.
   * @example
   * // Delete a few Addresses
   * const { count } = await prisma.addresses.deleteMany({
   *   where: {
   *     // ... provide filter here
   *   }
   * })
   * 
  **/
  deleteMany<T extends addressesDeleteManyArgs>(
    args: Subset<T, addressesDeleteManyArgs>
  ): Promise<BatchPayload>
  /**
   * Update zero or more Addresses.
   * @param {addressesUpdateManyArgs} args - Arguments to update one or more rows.
   * @example
   * // Update many Addresses
   * const addresses = await prisma.addresses.updateMany({
   *   where: {
   *     // ... provide filter here
   *   },
   *   data: {
   *     // ... provide data here
   *   }
   * })
   * 
  **/
  updateMany<T extends addressesUpdateManyArgs>(
    args: Subset<T, addressesUpdateManyArgs>
  ): Promise<BatchPayload>
  /**
   * Create or update one Addresses.
   * @param {addressesUpsertArgs} args - Arguments to update or create a Addresses.
   * @example
   * // Update or create a Addresses
   * const addresses = await prisma.addresses.upsert({
   *   create: {
   *     // ... data to create a Addresses
   *   },
   *   update: {
   *     // ... in case it already exists, update
   *   },
   *   where: {
   *     // ... the filter for the Addresses we want to update
   *   }
   * })
  **/
  upsert<T extends addressesUpsertArgs>(
    args: Subset<T, addressesUpsertArgs>
  ): CheckSelect<T, Prisma__addressesClient<addresses>, Prisma__addressesClient<addressesGetPayload<T>>>
  /**
   * Count
   */
  count(args?: Omit<FindManyaddressesArgs, 'select' | 'include'>): Promise<number>

  /**
   * Aggregate
   */
  aggregate<T extends AggregateAddressesArgs>(args: Subset<T, AggregateAddressesArgs>): Promise<GetAddressesAggregateType<T>>
}

/**
 * The delegate class that acts as a "Promise-like" for addresses.
 * Why is this prefixed with `Prisma__`?
 * Because we want to prevent naming conflicts as mentioned in 
 * https://github.com/prisma/prisma-client-js/issues/707
 */
export declare class Prisma__addressesClient<T> implements Promise<T> {
  private readonly _dmmf;
  private readonly _fetcher;
  private readonly _queryType;
  private readonly _rootField;
  private readonly _clientMethod;
  private readonly _args;
  private readonly _dataPath;
  private readonly _errorFormat;
  private readonly _measurePerformance?;
  private _isList;
  private _callsite;
  private _requestPromise?;
  constructor(_dmmf: DMMFClass, _fetcher: PrismaClientFetcher, _queryType: 'query' | 'mutation', _rootField: string, _clientMethod: string, _args: any, _dataPath: string[], _errorFormat: ErrorFormat, _measurePerformance?: boolean | undefined, _isList?: boolean);
  readonly [Symbol.toStringTag]: 'PrismaClientPromise';

  contract_addressesTocontract_address<T extends FindManycontractArgs = {}>(args?: Subset<T, FindManycontractArgs>): CheckSelect<T, Promise<Array<contract>>, Promise<Array<contractGetPayload<T>>>>;

  contract_addressesTocontract_delegate<T extends FindManycontractArgs = {}>(args?: Subset<T, FindManycontractArgs>): CheckSelect<T, Promise<Array<contract>>, Promise<Array<contractGetPayload<T>>>>;

  contract_addressesTocontract_mgr<T extends FindManycontractArgs = {}>(args?: Subset<T, FindManycontractArgs>): CheckSelect<T, Promise<Array<contract>>, Promise<Array<contractGetPayload<T>>>>;

  contract_addressesTocontract_preorig<T extends FindManycontractArgs = {}>(args?: Subset<T, FindManycontractArgs>): CheckSelect<T, Promise<Array<contract>>, Promise<Array<contractGetPayload<T>>>>;

  delegation<T extends FindManydelegationArgs = {}>(args?: Subset<T, FindManydelegationArgs>): CheckSelect<T, Promise<Array<delegation>>, Promise<Array<delegationGetPayload<T>>>>;

  implicit<T extends FindManyimplicitArgs = {}>(args?: Subset<T, FindManyimplicitArgs>): CheckSelect<T, Promise<Array<implicit>>, Promise<Array<implicitGetPayload<T>>>>;

  operation_alpha_addressesTooperation_alpha_receiver<T extends FindManyoperation_alphaArgs = {}>(args?: Subset<T, FindManyoperation_alphaArgs>): CheckSelect<T, Promise<Array<operation_alpha>>, Promise<Array<operation_alphaGetPayload<T>>>>;

  operation_alpha_addressesTooperation_alpha_sender<T extends FindManyoperation_alphaArgs = {}>(args?: Subset<T, FindManyoperation_alphaArgs>): CheckSelect<T, Promise<Array<operation_alpha>>, Promise<Array<operation_alphaGetPayload<T>>>>;

  private get _document();
  /**
   * Attaches callbacks for the resolution and/or rejection of the Promise.
   * @param onfulfilled The callback to execute when the Promise is resolved.
   * @param onrejected The callback to execute when the Promise is rejected.
   * @returns A Promise for the completion of which ever callback is executed.
   */
  then<TResult1 = T, TResult2 = never>(onfulfilled?: ((value: T) => TResult1 | Promise<TResult1>) | undefined | null, onrejected?: ((reason: any) => TResult2 | Promise<TResult2>) | undefined | null): Promise<TResult1 | TResult2>;
  /**
   * Attaches a callback for only the rejection of the Promise.
   * @param onrejected The callback to execute when the Promise is rejected.
   * @returns A Promise for the completion of the callback.
   */
  catch<TResult = never>(onrejected?: ((reason: any) => TResult | Promise<TResult>) | undefined | null): Promise<T | TResult>;
  /**
   * Attaches a callback that is invoked when the Promise is settled (fulfilled or rejected). The
   * resolved value cannot be modified from the callback.
   * @param onfinally The callback to execute when the Promise is settled (fulfilled or rejected).
   * @returns A Promise for the completion of the callback.
   */
  finally(onfinally?: (() => void) | undefined | null): Promise<T>;
}

// Custom InputTypes

/**
 * addresses findOne
 */
export type FindOneaddressesArgs = {
  /**
   * Select specific fields to fetch from the addresses
  **/
  select?: addressesSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: addressesInclude | null
  /**
   * Filter, which addresses to fetch.
  **/
  where: addressesWhereUniqueInput
}


/**
 * addresses findMany
 */
export type FindManyaddressesArgs = {
  /**
   * Select specific fields to fetch from the addresses
  **/
  select?: addressesSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: addressesInclude | null
  /**
   * Filter, which addresses to fetch.
  **/
  where?: addressesWhereInput
  /**
   * Determine the order of the addresses to fetch.
  **/
  orderBy?: Enumerable<addressesOrderByInput>
  /**
   * Sets the position for listing addresses.
  **/
  cursor?: addressesWhereUniqueInput
  /**
   * The number of addresses to fetch. If negative number, it will take addresses before the `cursor`.
  **/
  take?: number
  /**
   * Skip the first `n` addresses.
  **/
  skip?: number
  distinct?: Enumerable<AddressesDistinctFieldEnum>
}


/**
 * addresses create
 */
export type addressesCreateArgs = {
  /**
   * Select specific fields to fetch from the addresses
  **/
  select?: addressesSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: addressesInclude | null
  /**
   * The data needed to create a addresses.
  **/
  data: addressesCreateInput
}


/**
 * addresses update
 */
export type addressesUpdateArgs = {
  /**
   * Select specific fields to fetch from the addresses
  **/
  select?: addressesSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: addressesInclude | null
  /**
   * The data needed to update a addresses.
  **/
  data: addressesUpdateInput
  /**
   * Choose, which addresses to update.
  **/
  where: addressesWhereUniqueInput
}


/**
 * addresses updateMany
 */
export type addressesUpdateManyArgs = {
  data: addressesUpdateManyMutationInput
  where?: addressesWhereInput
}


/**
 * addresses upsert
 */
export type addressesUpsertArgs = {
  /**
   * Select specific fields to fetch from the addresses
  **/
  select?: addressesSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: addressesInclude | null
  /**
   * The filter to search for the addresses to update in case it exists.
  **/
  where: addressesWhereUniqueInput
  /**
   * In case the addresses found by the `where` argument doesn't exist, create a new addresses with this data.
  **/
  create: addressesCreateInput
  /**
   * In case the addresses was found with the provided `where` argument, update it with this data.
  **/
  update: addressesUpdateInput
}


/**
 * addresses delete
 */
export type addressesDeleteArgs = {
  /**
   * Select specific fields to fetch from the addresses
  **/
  select?: addressesSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: addressesInclude | null
  /**
   * Filter which addresses to delete.
  **/
  where: addressesWhereUniqueInput
}


/**
 * addresses deleteMany
 */
export type addressesDeleteManyArgs = {
  where?: addressesWhereInput
}


/**
 * addresses without action
 */
export type addressesArgs = {
  /**
   * Select specific fields to fetch from the addresses
  **/
  select?: addressesSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: addressesInclude | null
}



/**
 * Model manager_numbers
 */

export type manager_numbers = {
  hash: string
  counter: string | null
  gas_limit: string | null
  storage_limit: string | null
}


export type AggregateManager_numbers = {
  count: number
}



export type AggregateManager_numbersArgs = {
  where?: manager_numbersWhereInput
  orderBy?: Enumerable<manager_numbersOrderByInput>
  cursor?: manager_numbersWhereUniqueInput
  take?: number
  skip?: number
  distinct?: Enumerable<Manager_numbersDistinctFieldEnum>
  count?: true
}

export type GetManager_numbersAggregateType<T extends AggregateManager_numbersArgs> = {
  [P in keyof T]: P extends 'count' ? number : never
}


    
    

export type manager_numbersSelect = {
  hash?: boolean
  counter?: boolean
  gas_limit?: boolean
  storage_limit?: boolean
  operation?: boolean | operationArgs
}

export type manager_numbersInclude = {
  operation?: boolean | operationArgs
}

export type manager_numbersGetPayload<
  S extends boolean | null | undefined | manager_numbersArgs,
  U = keyof S
> = S extends true
  ? manager_numbers
  : S extends undefined
  ? never
  : S extends manager_numbersArgs | FindManymanager_numbersArgs
  ? 'include' extends U
    ? manager_numbers  & {
      [P in TrueKeys<S['include']>]:
      P extends 'operation'
      ? operationGetPayload<S['include'][P]> : never
    }
  : 'select' extends U
    ? {
      [P in TrueKeys<S['select']>]:P extends keyof manager_numbers ? manager_numbers[P]
: 
      P extends 'operation'
      ? operationGetPayload<S['select'][P]> : never
    }
  : manager_numbers
: manager_numbers


export interface manager_numbersDelegate {
  /**
   * Find zero or one Manager_numbers.
   * @param {FindOnemanager_numbersArgs} args - Arguments to find a Manager_numbers
   * @example
   * // Get one Manager_numbers
   * const manager_numbers = await prisma.manager_numbers.findOne({
   *   where: {
   *     // ... provide filter here
   *   }
   * })
  **/
  findOne<T extends FindOnemanager_numbersArgs>(
    args: Subset<T, FindOnemanager_numbersArgs>
  ): CheckSelect<T, Prisma__manager_numbersClient<manager_numbers | null>, Prisma__manager_numbersClient<manager_numbersGetPayload<T> | null>>
  /**
   * Find zero or more Manager_numbers.
   * @param {FindManymanager_numbersArgs=} args - Arguments to filter and select certain fields only.
   * @example
   * // Get all Manager_numbers
   * const manager_numbers = await prisma.manager_numbers.findMany()
   * 
   * // Get first 10 Manager_numbers
   * const manager_numbers = await prisma.manager_numbers.findMany({ take: 10 })
   * 
   * // Only select the `hash`
   * const manager_numbersWithHashOnly = await prisma.manager_numbers.findMany({ select: { hash: true } })
   * 
  **/
  findMany<T extends FindManymanager_numbersArgs>(
    args?: Subset<T, FindManymanager_numbersArgs>
  ): CheckSelect<T, Promise<Array<manager_numbers>>, Promise<Array<manager_numbersGetPayload<T>>>>
  /**
   * Create a Manager_numbers.
   * @param {manager_numbersCreateArgs} args - Arguments to create a Manager_numbers.
   * @example
   * // Create one Manager_numbers
   * const Manager_numbers = await prisma.manager_numbers.create({
   *   data: {
   *     // ... data to create a Manager_numbers
   *   }
   * })
   * 
  **/
  create<T extends manager_numbersCreateArgs>(
    args: Subset<T, manager_numbersCreateArgs>
  ): CheckSelect<T, Prisma__manager_numbersClient<manager_numbers>, Prisma__manager_numbersClient<manager_numbersGetPayload<T>>>
  /**
   * Delete a Manager_numbers.
   * @param {manager_numbersDeleteArgs} args - Arguments to delete one Manager_numbers.
   * @example
   * // Delete one Manager_numbers
   * const Manager_numbers = await prisma.manager_numbers.delete({
   *   where: {
   *     // ... filter to delete one Manager_numbers
   *   }
   * })
   * 
  **/
  delete<T extends manager_numbersDeleteArgs>(
    args: Subset<T, manager_numbersDeleteArgs>
  ): CheckSelect<T, Prisma__manager_numbersClient<manager_numbers>, Prisma__manager_numbersClient<manager_numbersGetPayload<T>>>
  /**
   * Update one Manager_numbers.
   * @param {manager_numbersUpdateArgs} args - Arguments to update one Manager_numbers.
   * @example
   * // Update one Manager_numbers
   * const manager_numbers = await prisma.manager_numbers.update({
   *   where: {
   *     // ... provide filter here
   *   },
   *   data: {
   *     // ... provide data here
   *   }
   * })
   * 
  **/
  update<T extends manager_numbersUpdateArgs>(
    args: Subset<T, manager_numbersUpdateArgs>
  ): CheckSelect<T, Prisma__manager_numbersClient<manager_numbers>, Prisma__manager_numbersClient<manager_numbersGetPayload<T>>>
  /**
   * Delete zero or more Manager_numbers.
   * @param {manager_numbersDeleteManyArgs} args - Arguments to filter Manager_numbers to delete.
   * @example
   * // Delete a few Manager_numbers
   * const { count } = await prisma.manager_numbers.deleteMany({
   *   where: {
   *     // ... provide filter here
   *   }
   * })
   * 
  **/
  deleteMany<T extends manager_numbersDeleteManyArgs>(
    args: Subset<T, manager_numbersDeleteManyArgs>
  ): Promise<BatchPayload>
  /**
   * Update zero or more Manager_numbers.
   * @param {manager_numbersUpdateManyArgs} args - Arguments to update one or more rows.
   * @example
   * // Update many Manager_numbers
   * const manager_numbers = await prisma.manager_numbers.updateMany({
   *   where: {
   *     // ... provide filter here
   *   },
   *   data: {
   *     // ... provide data here
   *   }
   * })
   * 
  **/
  updateMany<T extends manager_numbersUpdateManyArgs>(
    args: Subset<T, manager_numbersUpdateManyArgs>
  ): Promise<BatchPayload>
  /**
   * Create or update one Manager_numbers.
   * @param {manager_numbersUpsertArgs} args - Arguments to update or create a Manager_numbers.
   * @example
   * // Update or create a Manager_numbers
   * const manager_numbers = await prisma.manager_numbers.upsert({
   *   create: {
   *     // ... data to create a Manager_numbers
   *   },
   *   update: {
   *     // ... in case it already exists, update
   *   },
   *   where: {
   *     // ... the filter for the Manager_numbers we want to update
   *   }
   * })
  **/
  upsert<T extends manager_numbersUpsertArgs>(
    args: Subset<T, manager_numbersUpsertArgs>
  ): CheckSelect<T, Prisma__manager_numbersClient<manager_numbers>, Prisma__manager_numbersClient<manager_numbersGetPayload<T>>>
  /**
   * Count
   */
  count(args?: Omit<FindManymanager_numbersArgs, 'select' | 'include'>): Promise<number>

  /**
   * Aggregate
   */
  aggregate<T extends AggregateManager_numbersArgs>(args: Subset<T, AggregateManager_numbersArgs>): Promise<GetManager_numbersAggregateType<T>>
}

/**
 * The delegate class that acts as a "Promise-like" for manager_numbers.
 * Why is this prefixed with `Prisma__`?
 * Because we want to prevent naming conflicts as mentioned in 
 * https://github.com/prisma/prisma-client-js/issues/707
 */
export declare class Prisma__manager_numbersClient<T> implements Promise<T> {
  private readonly _dmmf;
  private readonly _fetcher;
  private readonly _queryType;
  private readonly _rootField;
  private readonly _clientMethod;
  private readonly _args;
  private readonly _dataPath;
  private readonly _errorFormat;
  private readonly _measurePerformance?;
  private _isList;
  private _callsite;
  private _requestPromise?;
  constructor(_dmmf: DMMFClass, _fetcher: PrismaClientFetcher, _queryType: 'query' | 'mutation', _rootField: string, _clientMethod: string, _args: any, _dataPath: string[], _errorFormat: ErrorFormat, _measurePerformance?: boolean | undefined, _isList?: boolean);
  readonly [Symbol.toStringTag]: 'PrismaClientPromise';

  operation<T extends operationArgs = {}>(args?: Subset<T, operationArgs>): CheckSelect<T, Prisma__operationClient<operation | null>, Prisma__operationClient<operationGetPayload<T> | null>>;

  private get _document();
  /**
   * Attaches callbacks for the resolution and/or rejection of the Promise.
   * @param onfulfilled The callback to execute when the Promise is resolved.
   * @param onrejected The callback to execute when the Promise is rejected.
   * @returns A Promise for the completion of which ever callback is executed.
   */
  then<TResult1 = T, TResult2 = never>(onfulfilled?: ((value: T) => TResult1 | Promise<TResult1>) | undefined | null, onrejected?: ((reason: any) => TResult2 | Promise<TResult2>) | undefined | null): Promise<TResult1 | TResult2>;
  /**
   * Attaches a callback for only the rejection of the Promise.
   * @param onrejected The callback to execute when the Promise is rejected.
   * @returns A Promise for the completion of the callback.
   */
  catch<TResult = never>(onrejected?: ((reason: any) => TResult | Promise<TResult>) | undefined | null): Promise<T | TResult>;
  /**
   * Attaches a callback that is invoked when the Promise is settled (fulfilled or rejected). The
   * resolved value cannot be modified from the callback.
   * @param onfinally The callback to execute when the Promise is settled (fulfilled or rejected).
   * @returns A Promise for the completion of the callback.
   */
  finally(onfinally?: (() => void) | undefined | null): Promise<T>;
}

// Custom InputTypes

/**
 * manager_numbers findOne
 */
export type FindOnemanager_numbersArgs = {
  /**
   * Select specific fields to fetch from the manager_numbers
  **/
  select?: manager_numbersSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: manager_numbersInclude | null
  /**
   * Filter, which manager_numbers to fetch.
  **/
  where: manager_numbersWhereUniqueInput
}


/**
 * manager_numbers findMany
 */
export type FindManymanager_numbersArgs = {
  /**
   * Select specific fields to fetch from the manager_numbers
  **/
  select?: manager_numbersSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: manager_numbersInclude | null
  /**
   * Filter, which manager_numbers to fetch.
  **/
  where?: manager_numbersWhereInput
  /**
   * Determine the order of the manager_numbers to fetch.
  **/
  orderBy?: Enumerable<manager_numbersOrderByInput>
  /**
   * Sets the position for listing manager_numbers.
  **/
  cursor?: manager_numbersWhereUniqueInput
  /**
   * The number of manager_numbers to fetch. If negative number, it will take manager_numbers before the `cursor`.
  **/
  take?: number
  /**
   * Skip the first `n` manager_numbers.
  **/
  skip?: number
  distinct?: Enumerable<Manager_numbersDistinctFieldEnum>
}


/**
 * manager_numbers create
 */
export type manager_numbersCreateArgs = {
  /**
   * Select specific fields to fetch from the manager_numbers
  **/
  select?: manager_numbersSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: manager_numbersInclude | null
  /**
   * The data needed to create a manager_numbers.
  **/
  data: manager_numbersCreateInput
}


/**
 * manager_numbers update
 */
export type manager_numbersUpdateArgs = {
  /**
   * Select specific fields to fetch from the manager_numbers
  **/
  select?: manager_numbersSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: manager_numbersInclude | null
  /**
   * The data needed to update a manager_numbers.
  **/
  data: manager_numbersUpdateInput
  /**
   * Choose, which manager_numbers to update.
  **/
  where: manager_numbersWhereUniqueInput
}


/**
 * manager_numbers updateMany
 */
export type manager_numbersUpdateManyArgs = {
  data: manager_numbersUpdateManyMutationInput
  where?: manager_numbersWhereInput
}


/**
 * manager_numbers upsert
 */
export type manager_numbersUpsertArgs = {
  /**
   * Select specific fields to fetch from the manager_numbers
  **/
  select?: manager_numbersSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: manager_numbersInclude | null
  /**
   * The filter to search for the manager_numbers to update in case it exists.
  **/
  where: manager_numbersWhereUniqueInput
  /**
   * In case the manager_numbers found by the `where` argument doesn't exist, create a new manager_numbers with this data.
  **/
  create: manager_numbersCreateInput
  /**
   * In case the manager_numbers was found with the provided `where` argument, update it with this data.
  **/
  update: manager_numbersUpdateInput
}


/**
 * manager_numbers delete
 */
export type manager_numbersDeleteArgs = {
  /**
   * Select specific fields to fetch from the manager_numbers
  **/
  select?: manager_numbersSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: manager_numbersInclude | null
  /**
   * Filter which manager_numbers to delete.
  **/
  where: manager_numbersWhereUniqueInput
}


/**
 * manager_numbers deleteMany
 */
export type manager_numbersDeleteManyArgs = {
  where?: manager_numbersWhereInput
}


/**
 * manager_numbers without action
 */
export type manager_numbersArgs = {
  /**
   * Select specific fields to fetch from the manager_numbers
  **/
  select?: manager_numbersSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: manager_numbersInclude | null
}



/**
 * Deep Input Types
 */


export type blockWhereInput = {
  AND?: Enumerable<blockWhereInput>
  OR?: Array<blockWhereInput>
  NOT?: Enumerable<blockWhereInput>
  hash?: string | StringFilter
  level?: number | IntFilter
  proto?: number | IntFilter
  predecessor?: string | StringFilter
  timestamp?: Date | string | DateTimeFilter
  validation_passes?: number | IntFilter
  merkle_root?: string | StringFilter
  fitness?: string | StringFilter
  context_hash?: string | StringFilter
  sterile?: boolean | BoolNullableFilter | null
  block_alpha?: Block_alphaListRelationFilter
  contract?: ContractListRelationFilter
  deactivated?: DeactivatedListRelationFilter
  endorsement?: EndorsementListRelationFilter
  operation?: OperationListRelationFilter
}

export type blockOrderByInput = {
  hash?: SortOrder
  level?: SortOrder
  proto?: SortOrder
  predecessor?: SortOrder
  timestamp?: SortOrder
  validation_passes?: SortOrder
  merkle_root?: SortOrder
  fitness?: SortOrder
  context_hash?: SortOrder
  sterile?: SortOrder
}

export type blockWhereUniqueInput = {
  hash?: string
  level?: number
  sterile?: boolean | null
  hash_level?: HashLevelCompoundUniqueInput
}

export type block_alphaWhereInput = {
  AND?: Enumerable<block_alphaWhereInput>
  OR?: Array<block_alphaWhereInput>
  NOT?: Enumerable<block_alphaWhereInput>
  hash?: string | StringFilter
  baker?: string | StringFilter
  level_position?: number | IntFilter
  cycle?: number | IntFilter
  cycle_position?: number | IntFilter
  voting_period?: number | IntFilter
  voting_period_position?: number | IntFilter
  voting_period_kind?: number | IntFilter
  consumed_gas?: string | StringFilter
  block?: blockWhereInput | null
}

export type block_alphaOrderByInput = {
  hash?: SortOrder
  baker?: SortOrder
  level_position?: SortOrder
  cycle?: SortOrder
  cycle_position?: SortOrder
  voting_period?: SortOrder
  voting_period_position?: SortOrder
  voting_period_kind?: SortOrder
  consumed_gas?: SortOrder
}

export type block_alphaWhereUniqueInput = {
  hash?: string
  baker?: string
  level_position?: number
  cycle?: number
  cycle_position?: number
}

export type chainWhereInput = {
  AND?: Enumerable<chainWhereInput>
  OR?: Array<chainWhereInput>
  NOT?: Enumerable<chainWhereInput>
  hash?: string | StringFilter
  operation?: OperationListRelationFilter
}

export type chainOrderByInput = {
  hash?: SortOrder
}

export type chainWhereUniqueInput = {
  hash?: string
}

export type contractWhereInput = {
  AND?: Enumerable<contractWhereInput>
  OR?: Array<contractWhereInput>
  NOT?: Enumerable<contractWhereInput>
  address?: string | StringFilter
  block_hash?: string | StringFilter
  mgr?: string | StringNullableFilter | null
  delegate?: string | StringNullableFilter | null
  spendable?: boolean | BoolFilter
  delegatable?: boolean | BoolFilter
  credit?: number | IntNullableFilter | null
  preorig?: string | StringNullableFilter | null
  script?: string | StringNullableFilter | null
  addresses_addressesTocontract_address?: addressesWhereInput | null
  block?: blockWhereInput | null
  addresses_addressesTocontract_delegate?: addressesWhereInput | null
  addresses_addressesTocontract_mgr?: addressesWhereInput | null
  implicit?: implicitWhereInput | null
  addresses_addressesTocontract_preorig?: addressesWhereInput | null
  contract?: contractWhereInput | null
  other_contract?: ContractListRelationFilter
  delegated_contract?: Delegated_contractListRelationFilter
  delegation?: DelegationListRelationFilter
  origination_contractToorigination_k?: OriginationListRelationFilter
  origination_contractToorigination_source?: OriginationListRelationFilter
  tx_contractTotx_destination?: TxListRelationFilter
  tx_contractTotx_source?: TxListRelationFilter
}

export type contractOrderByInput = {
  address?: SortOrder
  block_hash?: SortOrder
  mgr?: SortOrder
  delegate?: SortOrder
  spendable?: SortOrder
  delegatable?: SortOrder
  credit?: SortOrder
  preorig?: SortOrder
  script?: SortOrder
}

export type contractWhereUniqueInput = {
  address?: string
  block_hash?: string
  mgr?: string | null
  delegate?: string | null
  preorig?: string | null
}

export type deactivatedWhereInput = {
  AND?: Enumerable<deactivatedWhereInput>
  OR?: Array<deactivatedWhereInput>
  NOT?: Enumerable<deactivatedWhereInput>
  pkh?: string | StringFilter
  block_hash?: string | StringFilter
  block?: blockWhereInput | null
  implicit?: implicitWhereInput | null
}

export type deactivatedOrderByInput = {
  pkh?: SortOrder
  block_hash?: SortOrder
}

export type deactivatedWhereUniqueInput = {
  pkh?: string
  block_hash?: string
  pkh_block_hash?: PkhBlock_hashCompoundUniqueInput
}

export type delegated_contractWhereInput = {
  AND?: Enumerable<delegated_contractWhereInput>
  OR?: Array<delegated_contractWhereInput>
  NOT?: Enumerable<delegated_contractWhereInput>
  delegate?: string | StringFilter
  delegator?: string | StringFilter
  cycle?: number | IntFilter
  level?: number | IntFilter
  snapshot?: snapshotWhereInput | null
  implicit?: implicitWhereInput | null
  contract?: contractWhereInput | null
}

export type delegated_contractOrderByInput = {
  delegate?: SortOrder
  delegator?: SortOrder
  cycle?: SortOrder
  level?: SortOrder
}

export type delegated_contractWhereUniqueInput = {
  delegate?: string
  delegator?: string
  cycle?: number
  level?: number
  delegate_delegator_cycle_level?: DelegateDelegatorCycleLevelCompoundUniqueInput
}

export type delegationWhereInput = {
  AND?: Enumerable<delegationWhereInput>
  OR?: Array<delegationWhereInput>
  NOT?: Enumerable<delegationWhereInput>
  operation_hash?: string | StringFilter
  op_id?: number | IntFilter
  source?: string | StringFilter
  pkh?: string | StringNullableFilter | null
  autoid?: number | IntFilter
  operation_alpha?: operation_alphaWhereInput | null
  addresses?: addressesWhereInput | null
  contract?: contractWhereInput | null
}

export type delegationOrderByInput = {
  operation_hash?: SortOrder
  op_id?: SortOrder
  source?: SortOrder
  pkh?: SortOrder
  autoid?: SortOrder
}

export type delegationWhereUniqueInput = {
  operation_hash?: string
  source?: string
  pkh?: string | null
  operation_hash_op_id?: Operation_hashOp_idCompoundUniqueInput
}

export type endorsementWhereInput = {
  AND?: Enumerable<endorsementWhereInput>
  OR?: Array<endorsementWhereInput>
  NOT?: Enumerable<endorsementWhereInput>
  block_hash?: string | StringFilter
  op?: string | StringFilter
  id?: number | IntFilter
  level?: number | IntFilter
  pkh?: string | StringFilter
  slot?: number | IntFilter
  block?: blockWhereInput | null
  operation?: operationWhereInput | null
  implicit?: implicitWhereInput | null
}

export type endorsementOrderByInput = {
  block_hash?: SortOrder
  op?: SortOrder
  id?: SortOrder
  level?: SortOrder
  pkh?: SortOrder
  slot?: SortOrder
}

export type endorsementWhereUniqueInput = {
  block_hash_op_id_level_pkh_slot?: Block_hashOpIdLevelPkhSlotCompoundUniqueInput
}

export type implicitWhereInput = {
  AND?: Enumerable<implicitWhereInput>
  OR?: Array<implicitWhereInput>
  NOT?: Enumerable<implicitWhereInput>
  pkh?: string | StringFilter
  activated?: string | StringNullableFilter | null
  revealed?: string | StringNullableFilter | null
  pk?: string | StringNullableFilter | null
  autoid?: number | IntFilter
  operation_implicit_activatedTooperation?: operationWhereInput | null
  addresses?: addressesWhereInput | null
  operation_implicit_revealedTooperation?: operationWhereInput | null
  contract?: ContractListRelationFilter
  deactivated?: DeactivatedListRelationFilter
  delegated_contract?: Delegated_contractListRelationFilter
  endorsement?: EndorsementListRelationFilter
}

export type implicitOrderByInput = {
  pkh?: SortOrder
  activated?: SortOrder
  revealed?: SortOrder
  pk?: SortOrder
  autoid?: SortOrder
}

export type implicitWhereUniqueInput = {
  pkh?: string
  activated?: string | null
  revealed?: string | null
}

export type operationWhereInput = {
  AND?: Enumerable<operationWhereInput>
  OR?: Array<operationWhereInput>
  NOT?: Enumerable<operationWhereInput>
  hash?: string | StringFilter
  chain?: string | StringFilter
  block_hash?: string | StringFilter
  autoid?: number | IntFilter
  block?: blockWhereInput | null
  chain_chainTooperation?: chainWhereInput | null
  endorsement?: EndorsementListRelationFilter
  implicit_implicit_activatedTooperation?: ImplicitListRelationFilter
  implicit_implicit_revealedTooperation?: ImplicitListRelationFilter
  manager_numbers?: Manager_numbersListRelationFilter
  operation_alpha?: Operation_alphaListRelationFilter
}

export type operationOrderByInput = {
  hash?: SortOrder
  chain?: SortOrder
  block_hash?: SortOrder
  autoid?: SortOrder
}

export type operationWhereUniqueInput = {
  hash?: string
  chain?: string
  block_hash?: string
}

export type operation_alphaWhereInput = {
  AND?: Enumerable<operation_alphaWhereInput>
  OR?: Array<operation_alphaWhereInput>
  NOT?: Enumerable<operation_alphaWhereInput>
  hash?: string | StringFilter
  id?: number | IntFilter
  operation_kind?: number | IntFilter
  sender?: string | StringNullableFilter | null
  receiver?: string | StringNullableFilter | null
  autoid?: number | IntFilter
  operation?: operationWhereInput | null
  addresses_addressesTooperation_alpha_receiver?: addressesWhereInput | null
  addresses_addressesTooperation_alpha_sender?: addressesWhereInput | null
  delegation?: DelegationListRelationFilter
  origination?: OriginationListRelationFilter
  tx?: TxListRelationFilter
}

export type operation_alphaOrderByInput = {
  hash?: SortOrder
  id?: SortOrder
  operation_kind?: SortOrder
  sender?: SortOrder
  receiver?: SortOrder
  autoid?: SortOrder
}

export type operation_alphaWhereUniqueInput = {
  hash?: string
  operation_kind?: number
  hash_id?: HashIdCompoundUniqueInput
}

export type originationWhereInput = {
  AND?: Enumerable<originationWhereInput>
  OR?: Array<originationWhereInput>
  NOT?: Enumerable<originationWhereInput>
  operation_hash?: string | StringFilter
  op_id?: number | IntFilter
  source?: string | StringFilter
  k?: string | StringFilter
  autoid?: number | IntFilter
  contract_contractToorigination_k?: contractWhereInput | null
  operation_alpha?: operation_alphaWhereInput | null
  contract_contractToorigination_source?: contractWhereInput | null
}

export type originationOrderByInput = {
  operation_hash?: SortOrder
  op_id?: SortOrder
  source?: SortOrder
  k?: SortOrder
  autoid?: SortOrder
}

export type originationWhereUniqueInput = {
  operation_hash?: string
  source?: string
  k?: string
  operation_hash_op_id?: Operation_hashOp_idCompoundUniqueInput
}

export type snapshotWhereInput = {
  AND?: Enumerable<snapshotWhereInput>
  OR?: Array<snapshotWhereInput>
  NOT?: Enumerable<snapshotWhereInput>
  cycle?: number | IntFilter
  level?: number | IntFilter
  delegated_contract?: Delegated_contractListRelationFilter
}

export type snapshotOrderByInput = {
  cycle?: SortOrder
  level?: SortOrder
}

export type snapshotWhereUniqueInput = {
  cycle_level?: CycleLevelCompoundUniqueInput
}

export type txWhereInput = {
  AND?: Enumerable<txWhereInput>
  OR?: Array<txWhereInput>
  NOT?: Enumerable<txWhereInput>
  operation_hash?: string | StringFilter
  op_id?: number | IntFilter
  source?: string | StringFilter
  destination?: string | StringFilter
  fee?: number | IntFilter
  amount?: number | IntFilter
  parameters?: string | StringNullableFilter | null
  storage?: string | StringNullableFilter | null
  consumed_gas?: string | StringFilter
  storage_size?: string | StringFilter
  paid_storage_size_diff?: string | StringFilter
  entrypoint?: string | StringNullableFilter | null
  bigmapdiff?: string | StringNullableFilter | null
  autoid?: number | IntFilter
  contract_contractTotx_destination?: contractWhereInput | null
  operation_alpha?: operation_alphaWhereInput | null
  contract_contractTotx_source?: contractWhereInput | null
}

export type txOrderByInput = {
  operation_hash?: SortOrder
  op_id?: SortOrder
  source?: SortOrder
  destination?: SortOrder
  fee?: SortOrder
  amount?: SortOrder
  parameters?: SortOrder
  storage?: SortOrder
  consumed_gas?: SortOrder
  storage_size?: SortOrder
  paid_storage_size_diff?: SortOrder
  entrypoint?: SortOrder
  bigmapdiff?: SortOrder
  autoid?: SortOrder
}

export type txWhereUniqueInput = {
  operation_hash?: string
  source?: string
  destination?: string
  operation_hash_op_id?: Operation_hashOp_idCompoundUniqueInput
}

export type addressesWhereInput = {
  AND?: Enumerable<addressesWhereInput>
  OR?: Array<addressesWhereInput>
  NOT?: Enumerable<addressesWhereInput>
  address?: string | StringFilter
  contract_addressesTocontract_address?: ContractListRelationFilter
  contract_addressesTocontract_delegate?: ContractListRelationFilter
  contract_addressesTocontract_mgr?: ContractListRelationFilter
  contract_addressesTocontract_preorig?: ContractListRelationFilter
  delegation?: DelegationListRelationFilter
  implicit?: ImplicitListRelationFilter
  operation_alpha_addressesTooperation_alpha_receiver?: Operation_alphaListRelationFilter
  operation_alpha_addressesTooperation_alpha_sender?: Operation_alphaListRelationFilter
}

export type addressesOrderByInput = {
  address?: SortOrder
}

export type addressesWhereUniqueInput = {
  address?: string
}

export type manager_numbersWhereInput = {
  AND?: Enumerable<manager_numbersWhereInput>
  OR?: Array<manager_numbersWhereInput>
  NOT?: Enumerable<manager_numbersWhereInput>
  hash?: string | StringFilter
  counter?: string | StringNullableFilter | null
  gas_limit?: string | StringNullableFilter | null
  storage_limit?: string | StringNullableFilter | null
  operation?: operationWhereInput | null
}

export type manager_numbersOrderByInput = {
  hash?: SortOrder
  counter?: SortOrder
  gas_limit?: SortOrder
  storage_limit?: SortOrder
}

export type manager_numbersWhereUniqueInput = {
  hash?: string
}

export type blockCreateInput = {
  hash: string
  level: number
  proto: number
  predecessor: string
  timestamp: Date | string
  validation_passes: number
  merkle_root: string
  fitness: string
  context_hash: string
  sterile?: boolean | null
  block_alpha?: block_alphaCreateManyWithoutBlockInput
  contract?: contractCreateManyWithoutBlockInput
  deactivated?: deactivatedCreateManyWithoutBlockInput
  endorsement?: endorsementCreateManyWithoutBlockInput
  operation?: operationCreateManyWithoutBlockInput
}

export type blockUpdateInput = {
  hash?: string | StringFieldUpdateOperationsInput
  level?: number | IntFieldUpdateOperationsInput
  proto?: number | IntFieldUpdateOperationsInput
  predecessor?: string | StringFieldUpdateOperationsInput
  timestamp?: Date | string | DateTimeFieldUpdateOperationsInput
  validation_passes?: number | IntFieldUpdateOperationsInput
  merkle_root?: string | StringFieldUpdateOperationsInput
  fitness?: string | StringFieldUpdateOperationsInput
  context_hash?: string | StringFieldUpdateOperationsInput
  sterile?: boolean | NullableBoolFieldUpdateOperationsInput | null
  block_alpha?: block_alphaUpdateManyWithoutBlockInput
  contract?: contractUpdateManyWithoutBlockInput
  deactivated?: deactivatedUpdateManyWithoutBlockInput
  endorsement?: endorsementUpdateManyWithoutBlockInput
  operation?: operationUpdateManyWithoutBlockInput
}

export type blockUpdateManyMutationInput = {
  hash?: string | StringFieldUpdateOperationsInput
  level?: number | IntFieldUpdateOperationsInput
  proto?: number | IntFieldUpdateOperationsInput
  predecessor?: string | StringFieldUpdateOperationsInput
  timestamp?: Date | string | DateTimeFieldUpdateOperationsInput
  validation_passes?: number | IntFieldUpdateOperationsInput
  merkle_root?: string | StringFieldUpdateOperationsInput
  fitness?: string | StringFieldUpdateOperationsInput
  context_hash?: string | StringFieldUpdateOperationsInput
  sterile?: boolean | NullableBoolFieldUpdateOperationsInput | null
}

export type block_alphaCreateInput = {
  baker: string
  level_position: number
  cycle: number
  cycle_position: number
  voting_period: number
  voting_period_position: number
  voting_period_kind: number
  consumed_gas: string
  block: blockCreateOneWithoutBlock_alphaInput
}

export type block_alphaUpdateInput = {
  baker?: string | StringFieldUpdateOperationsInput
  level_position?: number | IntFieldUpdateOperationsInput
  cycle?: number | IntFieldUpdateOperationsInput
  cycle_position?: number | IntFieldUpdateOperationsInput
  voting_period?: number | IntFieldUpdateOperationsInput
  voting_period_position?: number | IntFieldUpdateOperationsInput
  voting_period_kind?: number | IntFieldUpdateOperationsInput
  consumed_gas?: string | StringFieldUpdateOperationsInput
  block?: blockUpdateOneRequiredWithoutBlock_alphaInput
}

export type block_alphaUpdateManyMutationInput = {
  baker?: string | StringFieldUpdateOperationsInput
  level_position?: number | IntFieldUpdateOperationsInput
  cycle?: number | IntFieldUpdateOperationsInput
  cycle_position?: number | IntFieldUpdateOperationsInput
  voting_period?: number | IntFieldUpdateOperationsInput
  voting_period_position?: number | IntFieldUpdateOperationsInput
  voting_period_kind?: number | IntFieldUpdateOperationsInput
  consumed_gas?: string | StringFieldUpdateOperationsInput
}

export type chainCreateInput = {
  hash: string
  operation?: operationCreateManyWithoutChain_chainTooperationInput
}

export type chainUpdateInput = {
  hash?: string | StringFieldUpdateOperationsInput
  operation?: operationUpdateManyWithoutChain_chainTooperationInput
}

export type chainUpdateManyMutationInput = {
  hash?: string | StringFieldUpdateOperationsInput
}

export type contractCreateInput = {
  spendable: boolean
  delegatable: boolean
  credit?: number | null
  script?: string | null
  addresses_addressesTocontract_address: addressesCreateOneWithoutContract_addressesTocontract_addressInput
  block: blockCreateOneWithoutContractInput
  addresses_addressesTocontract_delegate?: addressesCreateOneWithoutContract_addressesTocontract_delegateInput
  addresses_addressesTocontract_mgr?: addressesCreateOneWithoutContract_addressesTocontract_mgrInput
  implicit?: implicitCreateOneWithoutContractInput
  addresses_addressesTocontract_preorig?: addressesCreateOneWithoutContract_addressesTocontract_preorigInput
  contract?: contractCreateOneWithoutOther_contractInput
  other_contract?: contractCreateManyWithoutContractInput
  delegated_contract?: delegated_contractCreateManyWithoutContractInput
  delegation?: delegationCreateManyWithoutContractInput
  origination_contractToorigination_k?: originationCreateManyWithoutContract_contractToorigination_kInput
  origination_contractToorigination_source?: originationCreateManyWithoutContract_contractToorigination_sourceInput
  tx_contractTotx_destination?: txCreateManyWithoutContract_contractTotx_destinationInput
  tx_contractTotx_source?: txCreateManyWithoutContract_contractTotx_sourceInput
}

export type contractUpdateInput = {
  spendable?: boolean | BoolFieldUpdateOperationsInput
  delegatable?: boolean | BoolFieldUpdateOperationsInput
  credit?: number | NullableIntFieldUpdateOperationsInput | null
  script?: string | NullableStringFieldUpdateOperationsInput | null
  addresses_addressesTocontract_address?: addressesUpdateOneRequiredWithoutContract_addressesTocontract_addressInput
  block?: blockUpdateOneRequiredWithoutContractInput
  addresses_addressesTocontract_delegate?: addressesUpdateOneWithoutContract_addressesTocontract_delegateInput
  addresses_addressesTocontract_mgr?: addressesUpdateOneWithoutContract_addressesTocontract_mgrInput
  implicit?: implicitUpdateOneWithoutContractInput
  addresses_addressesTocontract_preorig?: addressesUpdateOneWithoutContract_addressesTocontract_preorigInput
  contract?: contractUpdateOneWithoutOther_contractInput
  other_contract?: contractUpdateManyWithoutContractInput
  delegated_contract?: delegated_contractUpdateManyWithoutContractInput
  delegation?: delegationUpdateManyWithoutContractInput
  origination_contractToorigination_k?: originationUpdateManyWithoutContract_contractToorigination_kInput
  origination_contractToorigination_source?: originationUpdateManyWithoutContract_contractToorigination_sourceInput
  tx_contractTotx_destination?: txUpdateManyWithoutContract_contractTotx_destinationInput
  tx_contractTotx_source?: txUpdateManyWithoutContract_contractTotx_sourceInput
}

export type contractUpdateManyMutationInput = {
  spendable?: boolean | BoolFieldUpdateOperationsInput
  delegatable?: boolean | BoolFieldUpdateOperationsInput
  credit?: number | NullableIntFieldUpdateOperationsInput | null
  script?: string | NullableStringFieldUpdateOperationsInput | null
}

export type deactivatedCreateInput = {
  block: blockCreateOneWithoutDeactivatedInput
  implicit: implicitCreateOneWithoutDeactivatedInput
}

export type deactivatedUpdateInput = {
  block?: blockUpdateOneRequiredWithoutDeactivatedInput
  implicit?: implicitUpdateOneRequiredWithoutDeactivatedInput
}

export type deactivatedUpdateManyMutationInput = {

}

export type delegated_contractCreateInput = {
  snapshot: snapshotCreateOneWithoutDelegated_contractInput
  implicit: implicitCreateOneWithoutDelegated_contractInput
  contract: contractCreateOneWithoutDelegated_contractInput
}

export type delegated_contractUpdateInput = {
  snapshot?: snapshotUpdateOneRequiredWithoutDelegated_contractInput
  implicit?: implicitUpdateOneRequiredWithoutDelegated_contractInput
  contract?: contractUpdateOneRequiredWithoutDelegated_contractInput
}

export type delegated_contractUpdateManyMutationInput = {

}

export type delegationCreateInput = {
  autoid?: number
  operation_alpha: operation_alphaCreateOneWithoutDelegationInput
  addresses?: addressesCreateOneWithoutDelegationInput
  contract: contractCreateOneWithoutDelegationInput
}

export type delegationUpdateInput = {
  autoid?: number | IntFieldUpdateOperationsInput
  operation_alpha?: operation_alphaUpdateOneRequiredWithoutDelegationInput
  addresses?: addressesUpdateOneWithoutDelegationInput
  contract?: contractUpdateOneRequiredWithoutDelegationInput
}

export type delegationUpdateManyMutationInput = {
  autoid?: number | IntFieldUpdateOperationsInput
}

export type endorsementCreateInput = {
  id: number
  level: number
  slot: number
  block: blockCreateOneWithoutEndorsementInput
  operation: operationCreateOneWithoutEndorsementInput
  implicit: implicitCreateOneWithoutEndorsementInput
}

export type endorsementUpdateInput = {
  id?: number | IntFieldUpdateOperationsInput
  level?: number | IntFieldUpdateOperationsInput
  slot?: number | IntFieldUpdateOperationsInput
  block?: blockUpdateOneRequiredWithoutEndorsementInput
  operation?: operationUpdateOneRequiredWithoutEndorsementInput
  implicit?: implicitUpdateOneRequiredWithoutEndorsementInput
}

export type endorsementUpdateManyMutationInput = {
  id?: number | IntFieldUpdateOperationsInput
  level?: number | IntFieldUpdateOperationsInput
  slot?: number | IntFieldUpdateOperationsInput
}

export type implicitCreateInput = {
  pk?: string | null
  autoid?: number
  operation_implicit_activatedTooperation?: operationCreateOneWithoutImplicit_implicit_activatedTooperationInput
  addresses: addressesCreateOneWithoutImplicitInput
  operation_implicit_revealedTooperation?: operationCreateOneWithoutImplicit_implicit_revealedTooperationInput
  contract?: contractCreateManyWithoutImplicitInput
  deactivated?: deactivatedCreateManyWithoutImplicitInput
  delegated_contract?: delegated_contractCreateManyWithoutImplicitInput
  endorsement?: endorsementCreateManyWithoutImplicitInput
}

export type implicitUpdateInput = {
  pk?: string | NullableStringFieldUpdateOperationsInput | null
  autoid?: number | IntFieldUpdateOperationsInput
  operation_implicit_activatedTooperation?: operationUpdateOneWithoutImplicit_implicit_activatedTooperationInput
  addresses?: addressesUpdateOneRequiredWithoutImplicitInput
  operation_implicit_revealedTooperation?: operationUpdateOneWithoutImplicit_implicit_revealedTooperationInput
  contract?: contractUpdateManyWithoutImplicitInput
  deactivated?: deactivatedUpdateManyWithoutImplicitInput
  delegated_contract?: delegated_contractUpdateManyWithoutImplicitInput
  endorsement?: endorsementUpdateManyWithoutImplicitInput
}

export type implicitUpdateManyMutationInput = {
  pk?: string | NullableStringFieldUpdateOperationsInput | null
  autoid?: number | IntFieldUpdateOperationsInput
}

export type operationCreateInput = {
  hash: string
  autoid?: number
  block: blockCreateOneWithoutOperationInput
  chain_chainTooperation: chainCreateOneWithoutOperationInput
  endorsement?: endorsementCreateManyWithoutOperationInput
  implicit_implicit_activatedTooperation?: implicitCreateManyWithoutOperation_implicit_activatedTooperationInput
  implicit_implicit_revealedTooperation?: implicitCreateManyWithoutOperation_implicit_revealedTooperationInput
  manager_numbers?: manager_numbersCreateManyWithoutOperationInput
  operation_alpha?: operation_alphaCreateManyWithoutOperationInput
}

export type operationUpdateInput = {
  hash?: string | StringFieldUpdateOperationsInput
  autoid?: number | IntFieldUpdateOperationsInput
  block?: blockUpdateOneRequiredWithoutOperationInput
  chain_chainTooperation?: chainUpdateOneRequiredWithoutOperationInput
  endorsement?: endorsementUpdateManyWithoutOperationInput
  implicit_implicit_activatedTooperation?: implicitUpdateManyWithoutOperation_implicit_activatedTooperationInput
  implicit_implicit_revealedTooperation?: implicitUpdateManyWithoutOperation_implicit_revealedTooperationInput
  manager_numbers?: manager_numbersUpdateManyWithoutOperationInput
  operation_alpha?: operation_alphaUpdateManyWithoutOperationInput
}

export type operationUpdateManyMutationInput = {
  hash?: string | StringFieldUpdateOperationsInput
  autoid?: number | IntFieldUpdateOperationsInput
}

export type operation_alphaCreateInput = {
  id: number
  operation_kind: number
  autoid?: number
  operation: operationCreateOneWithoutOperation_alphaInput
  addresses_addressesTooperation_alpha_receiver?: addressesCreateOneWithoutOperation_alpha_addressesTooperation_alpha_receiverInput
  addresses_addressesTooperation_alpha_sender?: addressesCreateOneWithoutOperation_alpha_addressesTooperation_alpha_senderInput
  delegation?: delegationCreateManyWithoutOperation_alphaInput
  origination?: originationCreateManyWithoutOperation_alphaInput
  tx?: txCreateManyWithoutOperation_alphaInput
}

export type operation_alphaUpdateInput = {
  id?: number | IntFieldUpdateOperationsInput
  operation_kind?: number | IntFieldUpdateOperationsInput
  autoid?: number | IntFieldUpdateOperationsInput
  operation?: operationUpdateOneRequiredWithoutOperation_alphaInput
  addresses_addressesTooperation_alpha_receiver?: addressesUpdateOneWithoutOperation_alpha_addressesTooperation_alpha_receiverInput
  addresses_addressesTooperation_alpha_sender?: addressesUpdateOneWithoutOperation_alpha_addressesTooperation_alpha_senderInput
  delegation?: delegationUpdateManyWithoutOperation_alphaInput
  origination?: originationUpdateManyWithoutOperation_alphaInput
  tx?: txUpdateManyWithoutOperation_alphaInput
}

export type operation_alphaUpdateManyMutationInput = {
  id?: number | IntFieldUpdateOperationsInput
  operation_kind?: number | IntFieldUpdateOperationsInput
  autoid?: number | IntFieldUpdateOperationsInput
}

export type originationCreateInput = {
  autoid?: number
  contract_contractToorigination_k: contractCreateOneWithoutOrigination_contractToorigination_kInput
  operation_alpha: operation_alphaCreateOneWithoutOriginationInput
  contract_contractToorigination_source: contractCreateOneWithoutOrigination_contractToorigination_sourceInput
}

export type originationUpdateInput = {
  autoid?: number | IntFieldUpdateOperationsInput
  contract_contractToorigination_k?: contractUpdateOneRequiredWithoutOrigination_contractToorigination_kInput
  operation_alpha?: operation_alphaUpdateOneRequiredWithoutOriginationInput
  contract_contractToorigination_source?: contractUpdateOneRequiredWithoutOrigination_contractToorigination_sourceInput
}

export type originationUpdateManyMutationInput = {
  autoid?: number | IntFieldUpdateOperationsInput
}

export type snapshotCreateInput = {
  cycle: number
  level: number
  delegated_contract?: delegated_contractCreateManyWithoutSnapshotInput
}

export type snapshotUpdateInput = {
  cycle?: number | IntFieldUpdateOperationsInput
  level?: number | IntFieldUpdateOperationsInput
  delegated_contract?: delegated_contractUpdateManyWithoutSnapshotInput
}

export type snapshotUpdateManyMutationInput = {
  cycle?: number | IntFieldUpdateOperationsInput
  level?: number | IntFieldUpdateOperationsInput
}

export type txCreateInput = {
  fee: number
  amount: number
  parameters?: string | null
  storage?: string | null
  consumed_gas: string
  storage_size: string
  paid_storage_size_diff: string
  entrypoint?: string | null
  bigmapdiff?: string | null
  autoid?: number
  contract_contractTotx_destination: contractCreateOneWithoutTx_contractTotx_destinationInput
  operation_alpha: operation_alphaCreateOneWithoutTxInput
  contract_contractTotx_source: contractCreateOneWithoutTx_contractTotx_sourceInput
}

export type txUpdateInput = {
  fee?: number | IntFieldUpdateOperationsInput
  amount?: number | IntFieldUpdateOperationsInput
  parameters?: string | NullableStringFieldUpdateOperationsInput | null
  storage?: string | NullableStringFieldUpdateOperationsInput | null
  consumed_gas?: string | StringFieldUpdateOperationsInput
  storage_size?: string | StringFieldUpdateOperationsInput
  paid_storage_size_diff?: string | StringFieldUpdateOperationsInput
  entrypoint?: string | NullableStringFieldUpdateOperationsInput | null
  bigmapdiff?: string | NullableStringFieldUpdateOperationsInput | null
  autoid?: number | IntFieldUpdateOperationsInput
  contract_contractTotx_destination?: contractUpdateOneRequiredWithoutTx_contractTotx_destinationInput
  operation_alpha?: operation_alphaUpdateOneRequiredWithoutTxInput
  contract_contractTotx_source?: contractUpdateOneRequiredWithoutTx_contractTotx_sourceInput
}

export type txUpdateManyMutationInput = {
  fee?: number | IntFieldUpdateOperationsInput
  amount?: number | IntFieldUpdateOperationsInput
  parameters?: string | NullableStringFieldUpdateOperationsInput | null
  storage?: string | NullableStringFieldUpdateOperationsInput | null
  consumed_gas?: string | StringFieldUpdateOperationsInput
  storage_size?: string | StringFieldUpdateOperationsInput
  paid_storage_size_diff?: string | StringFieldUpdateOperationsInput
  entrypoint?: string | NullableStringFieldUpdateOperationsInput | null
  bigmapdiff?: string | NullableStringFieldUpdateOperationsInput | null
  autoid?: number | IntFieldUpdateOperationsInput
}

export type addressesCreateInput = {
  address: string
  contract_addressesTocontract_address?: contractCreateManyWithoutAddresses_addressesTocontract_addressInput
  contract_addressesTocontract_delegate?: contractCreateManyWithoutAddresses_addressesTocontract_delegateInput
  contract_addressesTocontract_mgr?: contractCreateManyWithoutAddresses_addressesTocontract_mgrInput
  contract_addressesTocontract_preorig?: contractCreateManyWithoutAddresses_addressesTocontract_preorigInput
  delegation?: delegationCreateManyWithoutAddressesInput
  implicit?: implicitCreateManyWithoutAddressesInput
  operation_alpha_addressesTooperation_alpha_receiver?: operation_alphaCreateManyWithoutAddresses_addressesTooperation_alpha_receiverInput
  operation_alpha_addressesTooperation_alpha_sender?: operation_alphaCreateManyWithoutAddresses_addressesTooperation_alpha_senderInput
}

export type addressesUpdateInput = {
  address?: string | StringFieldUpdateOperationsInput
  contract_addressesTocontract_address?: contractUpdateManyWithoutAddresses_addressesTocontract_addressInput
  contract_addressesTocontract_delegate?: contractUpdateManyWithoutAddresses_addressesTocontract_delegateInput
  contract_addressesTocontract_mgr?: contractUpdateManyWithoutAddresses_addressesTocontract_mgrInput
  contract_addressesTocontract_preorig?: contractUpdateManyWithoutAddresses_addressesTocontract_preorigInput
  delegation?: delegationUpdateManyWithoutAddressesInput
  implicit?: implicitUpdateManyWithoutAddressesInput
  operation_alpha_addressesTooperation_alpha_receiver?: operation_alphaUpdateManyWithoutAddresses_addressesTooperation_alpha_receiverInput
  operation_alpha_addressesTooperation_alpha_sender?: operation_alphaUpdateManyWithoutAddresses_addressesTooperation_alpha_senderInput
}

export type addressesUpdateManyMutationInput = {
  address?: string | StringFieldUpdateOperationsInput
}

export type manager_numbersCreateInput = {
  counter?: string | null
  gas_limit?: string | null
  storage_limit?: string | null
  operation: operationCreateOneWithoutManager_numbersInput
}

export type manager_numbersUpdateInput = {
  counter?: string | NullableStringFieldUpdateOperationsInput | null
  gas_limit?: string | NullableStringFieldUpdateOperationsInput | null
  storage_limit?: string | NullableStringFieldUpdateOperationsInput | null
  operation?: operationUpdateOneRequiredWithoutManager_numbersInput
}

export type manager_numbersUpdateManyMutationInput = {
  counter?: string | NullableStringFieldUpdateOperationsInput | null
  gas_limit?: string | NullableStringFieldUpdateOperationsInput | null
  storage_limit?: string | NullableStringFieldUpdateOperationsInput | null
}

export type StringFilter = {
  equals?: string
  in?: Enumerable<string>
  notIn?: Enumerable<string>
  lt?: string
  lte?: string
  gt?: string
  gte?: string
  contains?: string
  startsWith?: string
  endsWith?: string
  not?: string | NestedStringFilter
}

export type IntFilter = {
  equals?: number
  in?: Enumerable<number>
  notIn?: Enumerable<number>
  lt?: number
  lte?: number
  gt?: number
  gte?: number
  not?: number | NestedIntFilter
}

export type DateTimeFilter = {
  equals?: Date | string
  in?: Enumerable<Date | string>
  notIn?: Enumerable<Date | string>
  lt?: Date | string
  lte?: Date | string
  gt?: Date | string
  gte?: Date | string
  not?: Date | string | NestedDateTimeFilter
}

export type BoolNullableFilter = {
  equals?: boolean | null
  not?: boolean | NestedBoolNullableFilter | null
}

export type Block_alphaListRelationFilter = {
  every?: block_alphaWhereInput
  some?: block_alphaWhereInput
  none?: block_alphaWhereInput
}

export type ContractListRelationFilter = {
  every?: contractWhereInput
  some?: contractWhereInput
  none?: contractWhereInput
}

export type DeactivatedListRelationFilter = {
  every?: deactivatedWhereInput
  some?: deactivatedWhereInput
  none?: deactivatedWhereInput
}

export type EndorsementListRelationFilter = {
  every?: endorsementWhereInput
  some?: endorsementWhereInput
  none?: endorsementWhereInput
}

export type OperationListRelationFilter = {
  every?: operationWhereInput
  some?: operationWhereInput
  none?: operationWhereInput
}

export type HashLevelCompoundUniqueInput = {
  hash: string
  level: number
}

export type BlockRelationFilter = {
  is?: blockWhereInput | null
  isNot?: blockWhereInput | null
}

export type StringNullableFilter = {
  equals?: string | null
  in?: Enumerable<string> | null
  notIn?: Enumerable<string> | null
  lt?: string | null
  lte?: string | null
  gt?: string | null
  gte?: string | null
  contains?: string | null
  startsWith?: string | null
  endsWith?: string | null
  not?: string | NestedStringNullableFilter | null
}

export type BoolFilter = {
  equals?: boolean
  not?: boolean | NestedBoolFilter
}

export type IntNullableFilter = {
  equals?: number | null
  in?: Enumerable<number> | null
  notIn?: Enumerable<number> | null
  lt?: number | null
  lte?: number | null
  gt?: number | null
  gte?: number | null
  not?: number | NestedIntNullableFilter | null
}

export type AddressesRelationFilter = {
  is?: addressesWhereInput | null
  isNot?: addressesWhereInput | null
}

export type ImplicitRelationFilter = {
  is?: implicitWhereInput | null
  isNot?: implicitWhereInput | null
}

export type ContractRelationFilter = {
  is?: contractWhereInput | null
  isNot?: contractWhereInput | null
}

export type Delegated_contractListRelationFilter = {
  every?: delegated_contractWhereInput
  some?: delegated_contractWhereInput
  none?: delegated_contractWhereInput
}

export type DelegationListRelationFilter = {
  every?: delegationWhereInput
  some?: delegationWhereInput
  none?: delegationWhereInput
}

export type OriginationListRelationFilter = {
  every?: originationWhereInput
  some?: originationWhereInput
  none?: originationWhereInput
}

export type TxListRelationFilter = {
  every?: txWhereInput
  some?: txWhereInput
  none?: txWhereInput
}

export type PkhBlock_hashCompoundUniqueInput = {
  pkh: string
  block_hash: string
}

export type SnapshotRelationFilter = {
  is?: snapshotWhereInput | null
  isNot?: snapshotWhereInput | null
}

export type DelegateDelegatorCycleLevelCompoundUniqueInput = {
  delegate: string
  delegator: string
  cycle: number
  level: number
}

export type Operation_alphaRelationFilter = {
  is?: operation_alphaWhereInput | null
  isNot?: operation_alphaWhereInput | null
}

export type Operation_hashOp_idCompoundUniqueInput = {
  operation_hash: string
  op_id: number
}

export type OperationRelationFilter = {
  is?: operationWhereInput | null
  isNot?: operationWhereInput | null
}

export type Block_hashOpIdLevelPkhSlotCompoundUniqueInput = {
  block_hash: string
  op: string
  id: number
  level: number
  pkh: string
  slot: number
}

export type ChainRelationFilter = {
  is?: chainWhereInput | null
  isNot?: chainWhereInput | null
}

export type ImplicitListRelationFilter = {
  every?: implicitWhereInput
  some?: implicitWhereInput
  none?: implicitWhereInput
}

export type Manager_numbersListRelationFilter = {
  every?: manager_numbersWhereInput
  some?: manager_numbersWhereInput
  none?: manager_numbersWhereInput
}

export type Operation_alphaListRelationFilter = {
  every?: operation_alphaWhereInput
  some?: operation_alphaWhereInput
  none?: operation_alphaWhereInput
}

export type HashIdCompoundUniqueInput = {
  hash: string
  id: number
}

export type CycleLevelCompoundUniqueInput = {
  cycle: number
  level: number
}

export type block_alphaCreateManyWithoutBlockInput = {
  create?: Enumerable<block_alphaCreateWithoutBlockInput>
  connect?: Enumerable<block_alphaWhereUniqueInput>
}

export type contractCreateManyWithoutBlockInput = {
  create?: Enumerable<contractCreateWithoutBlockInput>
  connect?: Enumerable<contractWhereUniqueInput>
}

export type deactivatedCreateManyWithoutBlockInput = {
  create?: Enumerable<deactivatedCreateWithoutBlockInput>
  connect?: Enumerable<deactivatedWhereUniqueInput>
}

export type endorsementCreateManyWithoutBlockInput = {
  create?: Enumerable<endorsementCreateWithoutBlockInput>
  connect?: Enumerable<endorsementWhereUniqueInput>
}

export type operationCreateManyWithoutBlockInput = {
  create?: Enumerable<operationCreateWithoutBlockInput>
  connect?: Enumerable<operationWhereUniqueInput>
}

export type StringFieldUpdateOperationsInput = {
  set?: string
}

export type IntFieldUpdateOperationsInput = {
  set?: number
}

export type DateTimeFieldUpdateOperationsInput = {
  set?: Date | string
}

export type NullableBoolFieldUpdateOperationsInput = {
  set?: boolean | null
}

export type block_alphaUpdateManyWithoutBlockInput = {
  create?: Enumerable<block_alphaCreateWithoutBlockInput>
  connect?: Enumerable<block_alphaWhereUniqueInput>
  set?: Enumerable<block_alphaWhereUniqueInput>
  disconnect?: Enumerable<block_alphaWhereUniqueInput>
  delete?: Enumerable<block_alphaWhereUniqueInput>
  update?: Enumerable<block_alphaUpdateWithWhereUniqueWithoutBlockInput>
  updateMany?: Enumerable<block_alphaUpdateManyWithWhereNestedInput> | null
  deleteMany?: Enumerable<block_alphaScalarWhereInput>
  upsert?: Enumerable<block_alphaUpsertWithWhereUniqueWithoutBlockInput>
}

export type contractUpdateManyWithoutBlockInput = {
  create?: Enumerable<contractCreateWithoutBlockInput>
  connect?: Enumerable<contractWhereUniqueInput>
  set?: Enumerable<contractWhereUniqueInput>
  disconnect?: Enumerable<contractWhereUniqueInput>
  delete?: Enumerable<contractWhereUniqueInput>
  update?: Enumerable<contractUpdateWithWhereUniqueWithoutBlockInput>
  updateMany?: Enumerable<contractUpdateManyWithWhereNestedInput> | null
  deleteMany?: Enumerable<contractScalarWhereInput>
  upsert?: Enumerable<contractUpsertWithWhereUniqueWithoutBlockInput>
}

export type deactivatedUpdateManyWithoutBlockInput = {
  create?: Enumerable<deactivatedCreateWithoutBlockInput>
  connect?: Enumerable<deactivatedWhereUniqueInput>
  set?: Enumerable<deactivatedWhereUniqueInput>
  disconnect?: Enumerable<deactivatedWhereUniqueInput>
  delete?: Enumerable<deactivatedWhereUniqueInput>
  update?: Enumerable<deactivatedUpdateWithWhereUniqueWithoutBlockInput>
  updateMany?: Enumerable<deactivatedUpdateManyWithWhereNestedInput> | null
  deleteMany?: Enumerable<deactivatedScalarWhereInput>
  upsert?: Enumerable<deactivatedUpsertWithWhereUniqueWithoutBlockInput>
}

export type endorsementUpdateManyWithoutBlockInput = {
  create?: Enumerable<endorsementCreateWithoutBlockInput>
  connect?: Enumerable<endorsementWhereUniqueInput>
  set?: Enumerable<endorsementWhereUniqueInput>
  disconnect?: Enumerable<endorsementWhereUniqueInput>
  delete?: Enumerable<endorsementWhereUniqueInput>
  update?: Enumerable<endorsementUpdateWithWhereUniqueWithoutBlockInput>
  updateMany?: Enumerable<endorsementUpdateManyWithWhereNestedInput> | null
  deleteMany?: Enumerable<endorsementScalarWhereInput>
  upsert?: Enumerable<endorsementUpsertWithWhereUniqueWithoutBlockInput>
}

export type operationUpdateManyWithoutBlockInput = {
  create?: Enumerable<operationCreateWithoutBlockInput>
  connect?: Enumerable<operationWhereUniqueInput>
  set?: Enumerable<operationWhereUniqueInput>
  disconnect?: Enumerable<operationWhereUniqueInput>
  delete?: Enumerable<operationWhereUniqueInput>
  update?: Enumerable<operationUpdateWithWhereUniqueWithoutBlockInput>
  updateMany?: Enumerable<operationUpdateManyWithWhereNestedInput> | null
  deleteMany?: Enumerable<operationScalarWhereInput>
  upsert?: Enumerable<operationUpsertWithWhereUniqueWithoutBlockInput>
}

export type blockCreateOneWithoutBlock_alphaInput = {
  create?: blockCreateWithoutBlock_alphaInput
  connect?: blockWhereUniqueInput
}

export type blockUpdateOneRequiredWithoutBlock_alphaInput = {
  create?: blockCreateWithoutBlock_alphaInput
  connect?: blockWhereUniqueInput
  update?: blockUpdateWithoutBlock_alphaDataInput
  upsert?: blockUpsertWithoutBlock_alphaInput
}

export type operationCreateManyWithoutChain_chainTooperationInput = {
  create?: Enumerable<operationCreateWithoutChain_chainTooperationInput>
  connect?: Enumerable<operationWhereUniqueInput>
}

export type operationUpdateManyWithoutChain_chainTooperationInput = {
  create?: Enumerable<operationCreateWithoutChain_chainTooperationInput>
  connect?: Enumerable<operationWhereUniqueInput>
  set?: Enumerable<operationWhereUniqueInput>
  disconnect?: Enumerable<operationWhereUniqueInput>
  delete?: Enumerable<operationWhereUniqueInput>
  update?: Enumerable<operationUpdateWithWhereUniqueWithoutChain_chainTooperationInput>
  updateMany?: Enumerable<operationUpdateManyWithWhereNestedInput> | null
  deleteMany?: Enumerable<operationScalarWhereInput>
  upsert?: Enumerable<operationUpsertWithWhereUniqueWithoutChain_chainTooperationInput>
}

export type addressesCreateOneWithoutContract_addressesTocontract_addressInput = {
  create?: addressesCreateWithoutContract_addressesTocontract_addressInput
  connect?: addressesWhereUniqueInput
}

export type blockCreateOneWithoutContractInput = {
  create?: blockCreateWithoutContractInput
  connect?: blockWhereUniqueInput
}

export type addressesCreateOneWithoutContract_addressesTocontract_delegateInput = {
  create?: addressesCreateWithoutContract_addressesTocontract_delegateInput
  connect?: addressesWhereUniqueInput
}

export type addressesCreateOneWithoutContract_addressesTocontract_mgrInput = {
  create?: addressesCreateWithoutContract_addressesTocontract_mgrInput
  connect?: addressesWhereUniqueInput
}

export type implicitCreateOneWithoutContractInput = {
  create?: implicitCreateWithoutContractInput
  connect?: implicitWhereUniqueInput
}

export type addressesCreateOneWithoutContract_addressesTocontract_preorigInput = {
  create?: addressesCreateWithoutContract_addressesTocontract_preorigInput
  connect?: addressesWhereUniqueInput
}

export type contractCreateOneWithoutOther_contractInput = {
  create?: contractCreateWithoutOther_contractInput
  connect?: contractWhereUniqueInput
}

export type contractCreateManyWithoutContractInput = {
  create?: Enumerable<contractCreateWithoutContractInput>
  connect?: Enumerable<contractWhereUniqueInput>
}

export type delegated_contractCreateManyWithoutContractInput = {
  create?: Enumerable<delegated_contractCreateWithoutContractInput>
  connect?: Enumerable<delegated_contractWhereUniqueInput>
}

export type delegationCreateManyWithoutContractInput = {
  create?: Enumerable<delegationCreateWithoutContractInput>
  connect?: Enumerable<delegationWhereUniqueInput>
}

export type originationCreateManyWithoutContract_contractToorigination_kInput = {
  create?: Enumerable<originationCreateWithoutContract_contractToorigination_kInput>
  connect?: Enumerable<originationWhereUniqueInput>
}

export type originationCreateManyWithoutContract_contractToorigination_sourceInput = {
  create?: Enumerable<originationCreateWithoutContract_contractToorigination_sourceInput>
  connect?: Enumerable<originationWhereUniqueInput>
}

export type txCreateManyWithoutContract_contractTotx_destinationInput = {
  create?: Enumerable<txCreateWithoutContract_contractTotx_destinationInput>
  connect?: Enumerable<txWhereUniqueInput>
}

export type txCreateManyWithoutContract_contractTotx_sourceInput = {
  create?: Enumerable<txCreateWithoutContract_contractTotx_sourceInput>
  connect?: Enumerable<txWhereUniqueInput>
}

export type BoolFieldUpdateOperationsInput = {
  set?: boolean
}

export type NullableIntFieldUpdateOperationsInput = {
  set?: number | null
}

export type NullableStringFieldUpdateOperationsInput = {
  set?: string | null
}

export type addressesUpdateOneRequiredWithoutContract_addressesTocontract_addressInput = {
  create?: addressesCreateWithoutContract_addressesTocontract_addressInput
  connect?: addressesWhereUniqueInput
  update?: addressesUpdateWithoutContract_addressesTocontract_addressDataInput
  upsert?: addressesUpsertWithoutContract_addressesTocontract_addressInput
}

export type blockUpdateOneRequiredWithoutContractInput = {
  create?: blockCreateWithoutContractInput
  connect?: blockWhereUniqueInput
  update?: blockUpdateWithoutContractDataInput
  upsert?: blockUpsertWithoutContractInput
}

export type addressesUpdateOneWithoutContract_addressesTocontract_delegateInput = {
  create?: addressesCreateWithoutContract_addressesTocontract_delegateInput
  connect?: addressesWhereUniqueInput
  disconnect?: boolean
  delete?: boolean
  update?: addressesUpdateWithoutContract_addressesTocontract_delegateDataInput
  upsert?: addressesUpsertWithoutContract_addressesTocontract_delegateInput
}

export type addressesUpdateOneWithoutContract_addressesTocontract_mgrInput = {
  create?: addressesCreateWithoutContract_addressesTocontract_mgrInput
  connect?: addressesWhereUniqueInput
  disconnect?: boolean
  delete?: boolean
  update?: addressesUpdateWithoutContract_addressesTocontract_mgrDataInput
  upsert?: addressesUpsertWithoutContract_addressesTocontract_mgrInput
}

export type implicitUpdateOneWithoutContractInput = {
  create?: implicitCreateWithoutContractInput
  connect?: implicitWhereUniqueInput
  disconnect?: boolean
  delete?: boolean
  update?: implicitUpdateWithoutContractDataInput
  upsert?: implicitUpsertWithoutContractInput
}

export type addressesUpdateOneWithoutContract_addressesTocontract_preorigInput = {
  create?: addressesCreateWithoutContract_addressesTocontract_preorigInput
  connect?: addressesWhereUniqueInput
  disconnect?: boolean
  delete?: boolean
  update?: addressesUpdateWithoutContract_addressesTocontract_preorigDataInput
  upsert?: addressesUpsertWithoutContract_addressesTocontract_preorigInput
}

export type contractUpdateOneWithoutOther_contractInput = {
  create?: contractCreateWithoutOther_contractInput
  connect?: contractWhereUniqueInput
  disconnect?: boolean
  delete?: boolean
  update?: contractUpdateWithoutOther_contractDataInput
  upsert?: contractUpsertWithoutOther_contractInput
}

export type contractUpdateManyWithoutContractInput = {
  create?: Enumerable<contractCreateWithoutContractInput>
  connect?: Enumerable<contractWhereUniqueInput>
  set?: Enumerable<contractWhereUniqueInput>
  disconnect?: Enumerable<contractWhereUniqueInput>
  delete?: Enumerable<contractWhereUniqueInput>
  update?: Enumerable<contractUpdateWithWhereUniqueWithoutContractInput>
  updateMany?: Enumerable<contractUpdateManyWithWhereNestedInput> | null
  deleteMany?: Enumerable<contractScalarWhereInput>
  upsert?: Enumerable<contractUpsertWithWhereUniqueWithoutContractInput>
}

export type delegated_contractUpdateManyWithoutContractInput = {
  create?: Enumerable<delegated_contractCreateWithoutContractInput>
  connect?: Enumerable<delegated_contractWhereUniqueInput>
  set?: Enumerable<delegated_contractWhereUniqueInput>
  disconnect?: Enumerable<delegated_contractWhereUniqueInput>
  delete?: Enumerable<delegated_contractWhereUniqueInput>
  update?: Enumerable<delegated_contractUpdateWithWhereUniqueWithoutContractInput>
  updateMany?: Enumerable<delegated_contractUpdateManyWithWhereNestedInput> | null
  deleteMany?: Enumerable<delegated_contractScalarWhereInput>
  upsert?: Enumerable<delegated_contractUpsertWithWhereUniqueWithoutContractInput>
}

export type delegationUpdateManyWithoutContractInput = {
  create?: Enumerable<delegationCreateWithoutContractInput>
  connect?: Enumerable<delegationWhereUniqueInput>
  set?: Enumerable<delegationWhereUniqueInput>
  disconnect?: Enumerable<delegationWhereUniqueInput>
  delete?: Enumerable<delegationWhereUniqueInput>
  update?: Enumerable<delegationUpdateWithWhereUniqueWithoutContractInput>
  updateMany?: Enumerable<delegationUpdateManyWithWhereNestedInput> | null
  deleteMany?: Enumerable<delegationScalarWhereInput>
  upsert?: Enumerable<delegationUpsertWithWhereUniqueWithoutContractInput>
}

export type originationUpdateManyWithoutContract_contractToorigination_kInput = {
  create?: Enumerable<originationCreateWithoutContract_contractToorigination_kInput>
  connect?: Enumerable<originationWhereUniqueInput>
  set?: Enumerable<originationWhereUniqueInput>
  disconnect?: Enumerable<originationWhereUniqueInput>
  delete?: Enumerable<originationWhereUniqueInput>
  update?: Enumerable<originationUpdateWithWhereUniqueWithoutContract_contractToorigination_kInput>
  updateMany?: Enumerable<originationUpdateManyWithWhereNestedInput> | null
  deleteMany?: Enumerable<originationScalarWhereInput>
  upsert?: Enumerable<originationUpsertWithWhereUniqueWithoutContract_contractToorigination_kInput>
}

export type originationUpdateManyWithoutContract_contractToorigination_sourceInput = {
  create?: Enumerable<originationCreateWithoutContract_contractToorigination_sourceInput>
  connect?: Enumerable<originationWhereUniqueInput>
  set?: Enumerable<originationWhereUniqueInput>
  disconnect?: Enumerable<originationWhereUniqueInput>
  delete?: Enumerable<originationWhereUniqueInput>
  update?: Enumerable<originationUpdateWithWhereUniqueWithoutContract_contractToorigination_sourceInput>
  updateMany?: Enumerable<originationUpdateManyWithWhereNestedInput> | null
  deleteMany?: Enumerable<originationScalarWhereInput>
  upsert?: Enumerable<originationUpsertWithWhereUniqueWithoutContract_contractToorigination_sourceInput>
}

export type txUpdateManyWithoutContract_contractTotx_destinationInput = {
  create?: Enumerable<txCreateWithoutContract_contractTotx_destinationInput>
  connect?: Enumerable<txWhereUniqueInput>
  set?: Enumerable<txWhereUniqueInput>
  disconnect?: Enumerable<txWhereUniqueInput>
  delete?: Enumerable<txWhereUniqueInput>
  update?: Enumerable<txUpdateWithWhereUniqueWithoutContract_contractTotx_destinationInput>
  updateMany?: Enumerable<txUpdateManyWithWhereNestedInput> | null
  deleteMany?: Enumerable<txScalarWhereInput>
  upsert?: Enumerable<txUpsertWithWhereUniqueWithoutContract_contractTotx_destinationInput>
}

export type txUpdateManyWithoutContract_contractTotx_sourceInput = {
  create?: Enumerable<txCreateWithoutContract_contractTotx_sourceInput>
  connect?: Enumerable<txWhereUniqueInput>
  set?: Enumerable<txWhereUniqueInput>
  disconnect?: Enumerable<txWhereUniqueInput>
  delete?: Enumerable<txWhereUniqueInput>
  update?: Enumerable<txUpdateWithWhereUniqueWithoutContract_contractTotx_sourceInput>
  updateMany?: Enumerable<txUpdateManyWithWhereNestedInput> | null
  deleteMany?: Enumerable<txScalarWhereInput>
  upsert?: Enumerable<txUpsertWithWhereUniqueWithoutContract_contractTotx_sourceInput>
}

export type blockCreateOneWithoutDeactivatedInput = {
  create?: blockCreateWithoutDeactivatedInput
  connect?: blockWhereUniqueInput
}

export type implicitCreateOneWithoutDeactivatedInput = {
  create?: implicitCreateWithoutDeactivatedInput
  connect?: implicitWhereUniqueInput
}

export type blockUpdateOneRequiredWithoutDeactivatedInput = {
  create?: blockCreateWithoutDeactivatedInput
  connect?: blockWhereUniqueInput
  update?: blockUpdateWithoutDeactivatedDataInput
  upsert?: blockUpsertWithoutDeactivatedInput
}

export type implicitUpdateOneRequiredWithoutDeactivatedInput = {
  create?: implicitCreateWithoutDeactivatedInput
  connect?: implicitWhereUniqueInput
  update?: implicitUpdateWithoutDeactivatedDataInput
  upsert?: implicitUpsertWithoutDeactivatedInput
}

export type snapshotCreateOneWithoutDelegated_contractInput = {
  create?: snapshotCreateWithoutDelegated_contractInput
  connect?: snapshotWhereUniqueInput
}

export type implicitCreateOneWithoutDelegated_contractInput = {
  create?: implicitCreateWithoutDelegated_contractInput
  connect?: implicitWhereUniqueInput
}

export type contractCreateOneWithoutDelegated_contractInput = {
  create?: contractCreateWithoutDelegated_contractInput
  connect?: contractWhereUniqueInput
}

export type snapshotUpdateOneRequiredWithoutDelegated_contractInput = {
  create?: snapshotCreateWithoutDelegated_contractInput
  connect?: snapshotWhereUniqueInput
  update?: snapshotUpdateWithoutDelegated_contractDataInput
  upsert?: snapshotUpsertWithoutDelegated_contractInput
}

export type implicitUpdateOneRequiredWithoutDelegated_contractInput = {
  create?: implicitCreateWithoutDelegated_contractInput
  connect?: implicitWhereUniqueInput
  update?: implicitUpdateWithoutDelegated_contractDataInput
  upsert?: implicitUpsertWithoutDelegated_contractInput
}

export type contractUpdateOneRequiredWithoutDelegated_contractInput = {
  create?: contractCreateWithoutDelegated_contractInput
  connect?: contractWhereUniqueInput
  update?: contractUpdateWithoutDelegated_contractDataInput
  upsert?: contractUpsertWithoutDelegated_contractInput
}

export type operation_alphaCreateOneWithoutDelegationInput = {
  create?: operation_alphaCreateWithoutDelegationInput
  connect?: operation_alphaWhereUniqueInput
}

export type addressesCreateOneWithoutDelegationInput = {
  create?: addressesCreateWithoutDelegationInput
  connect?: addressesWhereUniqueInput
}

export type contractCreateOneWithoutDelegationInput = {
  create?: contractCreateWithoutDelegationInput
  connect?: contractWhereUniqueInput
}

export type operation_alphaUpdateOneRequiredWithoutDelegationInput = {
  create?: operation_alphaCreateWithoutDelegationInput
  connect?: operation_alphaWhereUniqueInput
  update?: operation_alphaUpdateWithoutDelegationDataInput
  upsert?: operation_alphaUpsertWithoutDelegationInput
}

export type addressesUpdateOneWithoutDelegationInput = {
  create?: addressesCreateWithoutDelegationInput
  connect?: addressesWhereUniqueInput
  disconnect?: boolean
  delete?: boolean
  update?: addressesUpdateWithoutDelegationDataInput
  upsert?: addressesUpsertWithoutDelegationInput
}

export type contractUpdateOneRequiredWithoutDelegationInput = {
  create?: contractCreateWithoutDelegationInput
  connect?: contractWhereUniqueInput
  update?: contractUpdateWithoutDelegationDataInput
  upsert?: contractUpsertWithoutDelegationInput
}

export type blockCreateOneWithoutEndorsementInput = {
  create?: blockCreateWithoutEndorsementInput
  connect?: blockWhereUniqueInput
}

export type operationCreateOneWithoutEndorsementInput = {
  create?: operationCreateWithoutEndorsementInput
  connect?: operationWhereUniqueInput
}

export type implicitCreateOneWithoutEndorsementInput = {
  create?: implicitCreateWithoutEndorsementInput
  connect?: implicitWhereUniqueInput
}

export type blockUpdateOneRequiredWithoutEndorsementInput = {
  create?: blockCreateWithoutEndorsementInput
  connect?: blockWhereUniqueInput
  update?: blockUpdateWithoutEndorsementDataInput
  upsert?: blockUpsertWithoutEndorsementInput
}

export type operationUpdateOneRequiredWithoutEndorsementInput = {
  create?: operationCreateWithoutEndorsementInput
  connect?: operationWhereUniqueInput
  update?: operationUpdateWithoutEndorsementDataInput
  upsert?: operationUpsertWithoutEndorsementInput
}

export type implicitUpdateOneRequiredWithoutEndorsementInput = {
  create?: implicitCreateWithoutEndorsementInput
  connect?: implicitWhereUniqueInput
  update?: implicitUpdateWithoutEndorsementDataInput
  upsert?: implicitUpsertWithoutEndorsementInput
}

export type operationCreateOneWithoutImplicit_implicit_activatedTooperationInput = {
  create?: operationCreateWithoutImplicit_implicit_activatedTooperationInput
  connect?: operationWhereUniqueInput
}

export type addressesCreateOneWithoutImplicitInput = {
  create?: addressesCreateWithoutImplicitInput
  connect?: addressesWhereUniqueInput
}

export type operationCreateOneWithoutImplicit_implicit_revealedTooperationInput = {
  create?: operationCreateWithoutImplicit_implicit_revealedTooperationInput
  connect?: operationWhereUniqueInput
}

export type contractCreateManyWithoutImplicitInput = {
  create?: Enumerable<contractCreateWithoutImplicitInput>
  connect?: Enumerable<contractWhereUniqueInput>
}

export type deactivatedCreateManyWithoutImplicitInput = {
  create?: Enumerable<deactivatedCreateWithoutImplicitInput>
  connect?: Enumerable<deactivatedWhereUniqueInput>
}

export type delegated_contractCreateManyWithoutImplicitInput = {
  create?: Enumerable<delegated_contractCreateWithoutImplicitInput>
  connect?: Enumerable<delegated_contractWhereUniqueInput>
}

export type endorsementCreateManyWithoutImplicitInput = {
  create?: Enumerable<endorsementCreateWithoutImplicitInput>
  connect?: Enumerable<endorsementWhereUniqueInput>
}

export type operationUpdateOneWithoutImplicit_implicit_activatedTooperationInput = {
  create?: operationCreateWithoutImplicit_implicit_activatedTooperationInput
  connect?: operationWhereUniqueInput
  disconnect?: boolean
  delete?: boolean
  update?: operationUpdateWithoutImplicit_implicit_activatedTooperationDataInput
  upsert?: operationUpsertWithoutImplicit_implicit_activatedTooperationInput
}

export type addressesUpdateOneRequiredWithoutImplicitInput = {
  create?: addressesCreateWithoutImplicitInput
  connect?: addressesWhereUniqueInput
  update?: addressesUpdateWithoutImplicitDataInput
  upsert?: addressesUpsertWithoutImplicitInput
}

export type operationUpdateOneWithoutImplicit_implicit_revealedTooperationInput = {
  create?: operationCreateWithoutImplicit_implicit_revealedTooperationInput
  connect?: operationWhereUniqueInput
  disconnect?: boolean
  delete?: boolean
  update?: operationUpdateWithoutImplicit_implicit_revealedTooperationDataInput
  upsert?: operationUpsertWithoutImplicit_implicit_revealedTooperationInput
}

export type contractUpdateManyWithoutImplicitInput = {
  create?: Enumerable<contractCreateWithoutImplicitInput>
  connect?: Enumerable<contractWhereUniqueInput>
  set?: Enumerable<contractWhereUniqueInput>
  disconnect?: Enumerable<contractWhereUniqueInput>
  delete?: Enumerable<contractWhereUniqueInput>
  update?: Enumerable<contractUpdateWithWhereUniqueWithoutImplicitInput>
  updateMany?: Enumerable<contractUpdateManyWithWhereNestedInput> | null
  deleteMany?: Enumerable<contractScalarWhereInput>
  upsert?: Enumerable<contractUpsertWithWhereUniqueWithoutImplicitInput>
}

export type deactivatedUpdateManyWithoutImplicitInput = {
  create?: Enumerable<deactivatedCreateWithoutImplicitInput>
  connect?: Enumerable<deactivatedWhereUniqueInput>
  set?: Enumerable<deactivatedWhereUniqueInput>
  disconnect?: Enumerable<deactivatedWhereUniqueInput>
  delete?: Enumerable<deactivatedWhereUniqueInput>
  update?: Enumerable<deactivatedUpdateWithWhereUniqueWithoutImplicitInput>
  updateMany?: Enumerable<deactivatedUpdateManyWithWhereNestedInput> | null
  deleteMany?: Enumerable<deactivatedScalarWhereInput>
  upsert?: Enumerable<deactivatedUpsertWithWhereUniqueWithoutImplicitInput>
}

export type delegated_contractUpdateManyWithoutImplicitInput = {
  create?: Enumerable<delegated_contractCreateWithoutImplicitInput>
  connect?: Enumerable<delegated_contractWhereUniqueInput>
  set?: Enumerable<delegated_contractWhereUniqueInput>
  disconnect?: Enumerable<delegated_contractWhereUniqueInput>
  delete?: Enumerable<delegated_contractWhereUniqueInput>
  update?: Enumerable<delegated_contractUpdateWithWhereUniqueWithoutImplicitInput>
  updateMany?: Enumerable<delegated_contractUpdateManyWithWhereNestedInput> | null
  deleteMany?: Enumerable<delegated_contractScalarWhereInput>
  upsert?: Enumerable<delegated_contractUpsertWithWhereUniqueWithoutImplicitInput>
}

export type endorsementUpdateManyWithoutImplicitInput = {
  create?: Enumerable<endorsementCreateWithoutImplicitInput>
  connect?: Enumerable<endorsementWhereUniqueInput>
  set?: Enumerable<endorsementWhereUniqueInput>
  disconnect?: Enumerable<endorsementWhereUniqueInput>
  delete?: Enumerable<endorsementWhereUniqueInput>
  update?: Enumerable<endorsementUpdateWithWhereUniqueWithoutImplicitInput>
  updateMany?: Enumerable<endorsementUpdateManyWithWhereNestedInput> | null
  deleteMany?: Enumerable<endorsementScalarWhereInput>
  upsert?: Enumerable<endorsementUpsertWithWhereUniqueWithoutImplicitInput>
}

export type blockCreateOneWithoutOperationInput = {
  create?: blockCreateWithoutOperationInput
  connect?: blockWhereUniqueInput
}

export type chainCreateOneWithoutOperationInput = {
  create?: chainCreateWithoutOperationInput
  connect?: chainWhereUniqueInput
}

export type endorsementCreateManyWithoutOperationInput = {
  create?: Enumerable<endorsementCreateWithoutOperationInput>
  connect?: Enumerable<endorsementWhereUniqueInput>
}

export type implicitCreateManyWithoutOperation_implicit_activatedTooperationInput = {
  create?: Enumerable<implicitCreateWithoutOperation_implicit_activatedTooperationInput>
  connect?: Enumerable<implicitWhereUniqueInput>
}

export type implicitCreateManyWithoutOperation_implicit_revealedTooperationInput = {
  create?: Enumerable<implicitCreateWithoutOperation_implicit_revealedTooperationInput>
  connect?: Enumerable<implicitWhereUniqueInput>
}

export type manager_numbersCreateManyWithoutOperationInput = {
  create?: Enumerable<manager_numbersCreateWithoutOperationInput>
  connect?: Enumerable<manager_numbersWhereUniqueInput>
}

export type operation_alphaCreateManyWithoutOperationInput = {
  create?: Enumerable<operation_alphaCreateWithoutOperationInput>
  connect?: Enumerable<operation_alphaWhereUniqueInput>
}

export type blockUpdateOneRequiredWithoutOperationInput = {
  create?: blockCreateWithoutOperationInput
  connect?: blockWhereUniqueInput
  update?: blockUpdateWithoutOperationDataInput
  upsert?: blockUpsertWithoutOperationInput
}

export type chainUpdateOneRequiredWithoutOperationInput = {
  create?: chainCreateWithoutOperationInput
  connect?: chainWhereUniqueInput
  update?: chainUpdateWithoutOperationDataInput
  upsert?: chainUpsertWithoutOperationInput
}

export type endorsementUpdateManyWithoutOperationInput = {
  create?: Enumerable<endorsementCreateWithoutOperationInput>
  connect?: Enumerable<endorsementWhereUniqueInput>
  set?: Enumerable<endorsementWhereUniqueInput>
  disconnect?: Enumerable<endorsementWhereUniqueInput>
  delete?: Enumerable<endorsementWhereUniqueInput>
  update?: Enumerable<endorsementUpdateWithWhereUniqueWithoutOperationInput>
  updateMany?: Enumerable<endorsementUpdateManyWithWhereNestedInput> | null
  deleteMany?: Enumerable<endorsementScalarWhereInput>
  upsert?: Enumerable<endorsementUpsertWithWhereUniqueWithoutOperationInput>
}

export type implicitUpdateManyWithoutOperation_implicit_activatedTooperationInput = {
  create?: Enumerable<implicitCreateWithoutOperation_implicit_activatedTooperationInput>
  connect?: Enumerable<implicitWhereUniqueInput>
  set?: Enumerable<implicitWhereUniqueInput>
  disconnect?: Enumerable<implicitWhereUniqueInput>
  delete?: Enumerable<implicitWhereUniqueInput>
  update?: Enumerable<implicitUpdateWithWhereUniqueWithoutOperation_implicit_activatedTooperationInput>
  updateMany?: Enumerable<implicitUpdateManyWithWhereNestedInput> | null
  deleteMany?: Enumerable<implicitScalarWhereInput>
  upsert?: Enumerable<implicitUpsertWithWhereUniqueWithoutOperation_implicit_activatedTooperationInput>
}

export type implicitUpdateManyWithoutOperation_implicit_revealedTooperationInput = {
  create?: Enumerable<implicitCreateWithoutOperation_implicit_revealedTooperationInput>
  connect?: Enumerable<implicitWhereUniqueInput>
  set?: Enumerable<implicitWhereUniqueInput>
  disconnect?: Enumerable<implicitWhereUniqueInput>
  delete?: Enumerable<implicitWhereUniqueInput>
  update?: Enumerable<implicitUpdateWithWhereUniqueWithoutOperation_implicit_revealedTooperationInput>
  updateMany?: Enumerable<implicitUpdateManyWithWhereNestedInput> | null
  deleteMany?: Enumerable<implicitScalarWhereInput>
  upsert?: Enumerable<implicitUpsertWithWhereUniqueWithoutOperation_implicit_revealedTooperationInput>
}

export type manager_numbersUpdateManyWithoutOperationInput = {
  create?: Enumerable<manager_numbersCreateWithoutOperationInput>
  connect?: Enumerable<manager_numbersWhereUniqueInput>
  set?: Enumerable<manager_numbersWhereUniqueInput>
  disconnect?: Enumerable<manager_numbersWhereUniqueInput>
  delete?: Enumerable<manager_numbersWhereUniqueInput>
  update?: Enumerable<manager_numbersUpdateWithWhereUniqueWithoutOperationInput>
  updateMany?: Enumerable<manager_numbersUpdateManyWithWhereNestedInput> | null
  deleteMany?: Enumerable<manager_numbersScalarWhereInput>
  upsert?: Enumerable<manager_numbersUpsertWithWhereUniqueWithoutOperationInput>
}

export type operation_alphaUpdateManyWithoutOperationInput = {
  create?: Enumerable<operation_alphaCreateWithoutOperationInput>
  connect?: Enumerable<operation_alphaWhereUniqueInput>
  set?: Enumerable<operation_alphaWhereUniqueInput>
  disconnect?: Enumerable<operation_alphaWhereUniqueInput>
  delete?: Enumerable<operation_alphaWhereUniqueInput>
  update?: Enumerable<operation_alphaUpdateWithWhereUniqueWithoutOperationInput>
  updateMany?: Enumerable<operation_alphaUpdateManyWithWhereNestedInput> | null
  deleteMany?: Enumerable<operation_alphaScalarWhereInput>
  upsert?: Enumerable<operation_alphaUpsertWithWhereUniqueWithoutOperationInput>
}

export type operationCreateOneWithoutOperation_alphaInput = {
  create?: operationCreateWithoutOperation_alphaInput
  connect?: operationWhereUniqueInput
}

export type addressesCreateOneWithoutOperation_alpha_addressesTooperation_alpha_receiverInput = {
  create?: addressesCreateWithoutOperation_alpha_addressesTooperation_alpha_receiverInput
  connect?: addressesWhereUniqueInput
}

export type addressesCreateOneWithoutOperation_alpha_addressesTooperation_alpha_senderInput = {
  create?: addressesCreateWithoutOperation_alpha_addressesTooperation_alpha_senderInput
  connect?: addressesWhereUniqueInput
}

export type delegationCreateManyWithoutOperation_alphaInput = {
  create?: Enumerable<delegationCreateWithoutOperation_alphaInput>
  connect?: Enumerable<delegationWhereUniqueInput>
}

export type originationCreateManyWithoutOperation_alphaInput = {
  create?: Enumerable<originationCreateWithoutOperation_alphaInput>
  connect?: Enumerable<originationWhereUniqueInput>
}

export type txCreateManyWithoutOperation_alphaInput = {
  create?: Enumerable<txCreateWithoutOperation_alphaInput>
  connect?: Enumerable<txWhereUniqueInput>
}

export type operationUpdateOneRequiredWithoutOperation_alphaInput = {
  create?: operationCreateWithoutOperation_alphaInput
  connect?: operationWhereUniqueInput
  update?: operationUpdateWithoutOperation_alphaDataInput
  upsert?: operationUpsertWithoutOperation_alphaInput
}

export type addressesUpdateOneWithoutOperation_alpha_addressesTooperation_alpha_receiverInput = {
  create?: addressesCreateWithoutOperation_alpha_addressesTooperation_alpha_receiverInput
  connect?: addressesWhereUniqueInput
  disconnect?: boolean
  delete?: boolean
  update?: addressesUpdateWithoutOperation_alpha_addressesTooperation_alpha_receiverDataInput
  upsert?: addressesUpsertWithoutOperation_alpha_addressesTooperation_alpha_receiverInput
}

export type addressesUpdateOneWithoutOperation_alpha_addressesTooperation_alpha_senderInput = {
  create?: addressesCreateWithoutOperation_alpha_addressesTooperation_alpha_senderInput
  connect?: addressesWhereUniqueInput
  disconnect?: boolean
  delete?: boolean
  update?: addressesUpdateWithoutOperation_alpha_addressesTooperation_alpha_senderDataInput
  upsert?: addressesUpsertWithoutOperation_alpha_addressesTooperation_alpha_senderInput
}

export type delegationUpdateManyWithoutOperation_alphaInput = {
  create?: Enumerable<delegationCreateWithoutOperation_alphaInput>
  connect?: Enumerable<delegationWhereUniqueInput>
  set?: Enumerable<delegationWhereUniqueInput>
  disconnect?: Enumerable<delegationWhereUniqueInput>
  delete?: Enumerable<delegationWhereUniqueInput>
  update?: Enumerable<delegationUpdateWithWhereUniqueWithoutOperation_alphaInput>
  updateMany?: Enumerable<delegationUpdateManyWithWhereNestedInput> | null
  deleteMany?: Enumerable<delegationScalarWhereInput>
  upsert?: Enumerable<delegationUpsertWithWhereUniqueWithoutOperation_alphaInput>
}

export type originationUpdateManyWithoutOperation_alphaInput = {
  create?: Enumerable<originationCreateWithoutOperation_alphaInput>
  connect?: Enumerable<originationWhereUniqueInput>
  set?: Enumerable<originationWhereUniqueInput>
  disconnect?: Enumerable<originationWhereUniqueInput>
  delete?: Enumerable<originationWhereUniqueInput>
  update?: Enumerable<originationUpdateWithWhereUniqueWithoutOperation_alphaInput>
  updateMany?: Enumerable<originationUpdateManyWithWhereNestedInput> | null
  deleteMany?: Enumerable<originationScalarWhereInput>
  upsert?: Enumerable<originationUpsertWithWhereUniqueWithoutOperation_alphaInput>
}

export type txUpdateManyWithoutOperation_alphaInput = {
  create?: Enumerable<txCreateWithoutOperation_alphaInput>
  connect?: Enumerable<txWhereUniqueInput>
  set?: Enumerable<txWhereUniqueInput>
  disconnect?: Enumerable<txWhereUniqueInput>
  delete?: Enumerable<txWhereUniqueInput>
  update?: Enumerable<txUpdateWithWhereUniqueWithoutOperation_alphaInput>
  updateMany?: Enumerable<txUpdateManyWithWhereNestedInput> | null
  deleteMany?: Enumerable<txScalarWhereInput>
  upsert?: Enumerable<txUpsertWithWhereUniqueWithoutOperation_alphaInput>
}

export type contractCreateOneWithoutOrigination_contractToorigination_kInput = {
  create?: contractCreateWithoutOrigination_contractToorigination_kInput
  connect?: contractWhereUniqueInput
}

export type operation_alphaCreateOneWithoutOriginationInput = {
  create?: operation_alphaCreateWithoutOriginationInput
  connect?: operation_alphaWhereUniqueInput
}

export type contractCreateOneWithoutOrigination_contractToorigination_sourceInput = {
  create?: contractCreateWithoutOrigination_contractToorigination_sourceInput
  connect?: contractWhereUniqueInput
}

export type contractUpdateOneRequiredWithoutOrigination_contractToorigination_kInput = {
  create?: contractCreateWithoutOrigination_contractToorigination_kInput
  connect?: contractWhereUniqueInput
  update?: contractUpdateWithoutOrigination_contractToorigination_kDataInput
  upsert?: contractUpsertWithoutOrigination_contractToorigination_kInput
}

export type operation_alphaUpdateOneRequiredWithoutOriginationInput = {
  create?: operation_alphaCreateWithoutOriginationInput
  connect?: operation_alphaWhereUniqueInput
  update?: operation_alphaUpdateWithoutOriginationDataInput
  upsert?: operation_alphaUpsertWithoutOriginationInput
}

export type contractUpdateOneRequiredWithoutOrigination_contractToorigination_sourceInput = {
  create?: contractCreateWithoutOrigination_contractToorigination_sourceInput
  connect?: contractWhereUniqueInput
  update?: contractUpdateWithoutOrigination_contractToorigination_sourceDataInput
  upsert?: contractUpsertWithoutOrigination_contractToorigination_sourceInput
}

export type delegated_contractCreateManyWithoutSnapshotInput = {
  create?: Enumerable<delegated_contractCreateWithoutSnapshotInput>
  connect?: Enumerable<delegated_contractWhereUniqueInput>
}

export type delegated_contractUpdateManyWithoutSnapshotInput = {
  create?: Enumerable<delegated_contractCreateWithoutSnapshotInput>
  connect?: Enumerable<delegated_contractWhereUniqueInput>
  set?: Enumerable<delegated_contractWhereUniqueInput>
  disconnect?: Enumerable<delegated_contractWhereUniqueInput>
  delete?: Enumerable<delegated_contractWhereUniqueInput>
  update?: Enumerable<delegated_contractUpdateWithWhereUniqueWithoutSnapshotInput>
  updateMany?: Enumerable<delegated_contractUpdateManyWithWhereNestedInput> | null
  deleteMany?: Enumerable<delegated_contractScalarWhereInput>
  upsert?: Enumerable<delegated_contractUpsertWithWhereUniqueWithoutSnapshotInput>
}

export type contractCreateOneWithoutTx_contractTotx_destinationInput = {
  create?: contractCreateWithoutTx_contractTotx_destinationInput
  connect?: contractWhereUniqueInput
}

export type operation_alphaCreateOneWithoutTxInput = {
  create?: operation_alphaCreateWithoutTxInput
  connect?: operation_alphaWhereUniqueInput
}

export type contractCreateOneWithoutTx_contractTotx_sourceInput = {
  create?: contractCreateWithoutTx_contractTotx_sourceInput
  connect?: contractWhereUniqueInput
}

export type contractUpdateOneRequiredWithoutTx_contractTotx_destinationInput = {
  create?: contractCreateWithoutTx_contractTotx_destinationInput
  connect?: contractWhereUniqueInput
  update?: contractUpdateWithoutTx_contractTotx_destinationDataInput
  upsert?: contractUpsertWithoutTx_contractTotx_destinationInput
}

export type operation_alphaUpdateOneRequiredWithoutTxInput = {
  create?: operation_alphaCreateWithoutTxInput
  connect?: operation_alphaWhereUniqueInput
  update?: operation_alphaUpdateWithoutTxDataInput
  upsert?: operation_alphaUpsertWithoutTxInput
}

export type contractUpdateOneRequiredWithoutTx_contractTotx_sourceInput = {
  create?: contractCreateWithoutTx_contractTotx_sourceInput
  connect?: contractWhereUniqueInput
  update?: contractUpdateWithoutTx_contractTotx_sourceDataInput
  upsert?: contractUpsertWithoutTx_contractTotx_sourceInput
}

export type contractCreateManyWithoutAddresses_addressesTocontract_addressInput = {
  create?: Enumerable<contractCreateWithoutAddresses_addressesTocontract_addressInput>
  connect?: Enumerable<contractWhereUniqueInput>
}

export type contractCreateManyWithoutAddresses_addressesTocontract_delegateInput = {
  create?: Enumerable<contractCreateWithoutAddresses_addressesTocontract_delegateInput>
  connect?: Enumerable<contractWhereUniqueInput>
}

export type contractCreateManyWithoutAddresses_addressesTocontract_mgrInput = {
  create?: Enumerable<contractCreateWithoutAddresses_addressesTocontract_mgrInput>
  connect?: Enumerable<contractWhereUniqueInput>
}

export type contractCreateManyWithoutAddresses_addressesTocontract_preorigInput = {
  create?: Enumerable<contractCreateWithoutAddresses_addressesTocontract_preorigInput>
  connect?: Enumerable<contractWhereUniqueInput>
}

export type delegationCreateManyWithoutAddressesInput = {
  create?: Enumerable<delegationCreateWithoutAddressesInput>
  connect?: Enumerable<delegationWhereUniqueInput>
}

export type implicitCreateManyWithoutAddressesInput = {
  create?: Enumerable<implicitCreateWithoutAddressesInput>
  connect?: Enumerable<implicitWhereUniqueInput>
}

export type operation_alphaCreateManyWithoutAddresses_addressesTooperation_alpha_receiverInput = {
  create?: Enumerable<operation_alphaCreateWithoutAddresses_addressesTooperation_alpha_receiverInput>
  connect?: Enumerable<operation_alphaWhereUniqueInput>
}

export type operation_alphaCreateManyWithoutAddresses_addressesTooperation_alpha_senderInput = {
  create?: Enumerable<operation_alphaCreateWithoutAddresses_addressesTooperation_alpha_senderInput>
  connect?: Enumerable<operation_alphaWhereUniqueInput>
}

export type contractUpdateManyWithoutAddresses_addressesTocontract_addressInput = {
  create?: Enumerable<contractCreateWithoutAddresses_addressesTocontract_addressInput>
  connect?: Enumerable<contractWhereUniqueInput>
  set?: Enumerable<contractWhereUniqueInput>
  disconnect?: Enumerable<contractWhereUniqueInput>
  delete?: Enumerable<contractWhereUniqueInput>
  update?: Enumerable<contractUpdateWithWhereUniqueWithoutAddresses_addressesTocontract_addressInput>
  updateMany?: Enumerable<contractUpdateManyWithWhereNestedInput> | null
  deleteMany?: Enumerable<contractScalarWhereInput>
  upsert?: Enumerable<contractUpsertWithWhereUniqueWithoutAddresses_addressesTocontract_addressInput>
}

export type contractUpdateManyWithoutAddresses_addressesTocontract_delegateInput = {
  create?: Enumerable<contractCreateWithoutAddresses_addressesTocontract_delegateInput>
  connect?: Enumerable<contractWhereUniqueInput>
  set?: Enumerable<contractWhereUniqueInput>
  disconnect?: Enumerable<contractWhereUniqueInput>
  delete?: Enumerable<contractWhereUniqueInput>
  update?: Enumerable<contractUpdateWithWhereUniqueWithoutAddresses_addressesTocontract_delegateInput>
  updateMany?: Enumerable<contractUpdateManyWithWhereNestedInput> | null
  deleteMany?: Enumerable<contractScalarWhereInput>
  upsert?: Enumerable<contractUpsertWithWhereUniqueWithoutAddresses_addressesTocontract_delegateInput>
}

export type contractUpdateManyWithoutAddresses_addressesTocontract_mgrInput = {
  create?: Enumerable<contractCreateWithoutAddresses_addressesTocontract_mgrInput>
  connect?: Enumerable<contractWhereUniqueInput>
  set?: Enumerable<contractWhereUniqueInput>
  disconnect?: Enumerable<contractWhereUniqueInput>
  delete?: Enumerable<contractWhereUniqueInput>
  update?: Enumerable<contractUpdateWithWhereUniqueWithoutAddresses_addressesTocontract_mgrInput>
  updateMany?: Enumerable<contractUpdateManyWithWhereNestedInput> | null
  deleteMany?: Enumerable<contractScalarWhereInput>
  upsert?: Enumerable<contractUpsertWithWhereUniqueWithoutAddresses_addressesTocontract_mgrInput>
}

export type contractUpdateManyWithoutAddresses_addressesTocontract_preorigInput = {
  create?: Enumerable<contractCreateWithoutAddresses_addressesTocontract_preorigInput>
  connect?: Enumerable<contractWhereUniqueInput>
  set?: Enumerable<contractWhereUniqueInput>
  disconnect?: Enumerable<contractWhereUniqueInput>
  delete?: Enumerable<contractWhereUniqueInput>
  update?: Enumerable<contractUpdateWithWhereUniqueWithoutAddresses_addressesTocontract_preorigInput>
  updateMany?: Enumerable<contractUpdateManyWithWhereNestedInput> | null
  deleteMany?: Enumerable<contractScalarWhereInput>
  upsert?: Enumerable<contractUpsertWithWhereUniqueWithoutAddresses_addressesTocontract_preorigInput>
}

export type delegationUpdateManyWithoutAddressesInput = {
  create?: Enumerable<delegationCreateWithoutAddressesInput>
  connect?: Enumerable<delegationWhereUniqueInput>
  set?: Enumerable<delegationWhereUniqueInput>
  disconnect?: Enumerable<delegationWhereUniqueInput>
  delete?: Enumerable<delegationWhereUniqueInput>
  update?: Enumerable<delegationUpdateWithWhereUniqueWithoutAddressesInput>
  updateMany?: Enumerable<delegationUpdateManyWithWhereNestedInput> | null
  deleteMany?: Enumerable<delegationScalarWhereInput>
  upsert?: Enumerable<delegationUpsertWithWhereUniqueWithoutAddressesInput>
}

export type implicitUpdateManyWithoutAddressesInput = {
  create?: Enumerable<implicitCreateWithoutAddressesInput>
  connect?: Enumerable<implicitWhereUniqueInput>
  set?: Enumerable<implicitWhereUniqueInput>
  disconnect?: Enumerable<implicitWhereUniqueInput>
  delete?: Enumerable<implicitWhereUniqueInput>
  update?: Enumerable<implicitUpdateWithWhereUniqueWithoutAddressesInput>
  updateMany?: Enumerable<implicitUpdateManyWithWhereNestedInput> | null
  deleteMany?: Enumerable<implicitScalarWhereInput>
  upsert?: Enumerable<implicitUpsertWithWhereUniqueWithoutAddressesInput>
}

export type operation_alphaUpdateManyWithoutAddresses_addressesTooperation_alpha_receiverInput = {
  create?: Enumerable<operation_alphaCreateWithoutAddresses_addressesTooperation_alpha_receiverInput>
  connect?: Enumerable<operation_alphaWhereUniqueInput>
  set?: Enumerable<operation_alphaWhereUniqueInput>
  disconnect?: Enumerable<operation_alphaWhereUniqueInput>
  delete?: Enumerable<operation_alphaWhereUniqueInput>
  update?: Enumerable<operation_alphaUpdateWithWhereUniqueWithoutAddresses_addressesTooperation_alpha_receiverInput>
  updateMany?: Enumerable<operation_alphaUpdateManyWithWhereNestedInput> | null
  deleteMany?: Enumerable<operation_alphaScalarWhereInput>
  upsert?: Enumerable<operation_alphaUpsertWithWhereUniqueWithoutAddresses_addressesTooperation_alpha_receiverInput>
}

export type operation_alphaUpdateManyWithoutAddresses_addressesTooperation_alpha_senderInput = {
  create?: Enumerable<operation_alphaCreateWithoutAddresses_addressesTooperation_alpha_senderInput>
  connect?: Enumerable<operation_alphaWhereUniqueInput>
  set?: Enumerable<operation_alphaWhereUniqueInput>
  disconnect?: Enumerable<operation_alphaWhereUniqueInput>
  delete?: Enumerable<operation_alphaWhereUniqueInput>
  update?: Enumerable<operation_alphaUpdateWithWhereUniqueWithoutAddresses_addressesTooperation_alpha_senderInput>
  updateMany?: Enumerable<operation_alphaUpdateManyWithWhereNestedInput> | null
  deleteMany?: Enumerable<operation_alphaScalarWhereInput>
  upsert?: Enumerable<operation_alphaUpsertWithWhereUniqueWithoutAddresses_addressesTooperation_alpha_senderInput>
}

export type operationCreateOneWithoutManager_numbersInput = {
  create?: operationCreateWithoutManager_numbersInput
  connect?: operationWhereUniqueInput
}

export type operationUpdateOneRequiredWithoutManager_numbersInput = {
  create?: operationCreateWithoutManager_numbersInput
  connect?: operationWhereUniqueInput
  update?: operationUpdateWithoutManager_numbersDataInput
  upsert?: operationUpsertWithoutManager_numbersInput
}

export type NestedStringFilter = {
  equals?: string
  in?: Enumerable<string>
  notIn?: Enumerable<string>
  lt?: string
  lte?: string
  gt?: string
  gte?: string
  contains?: string
  startsWith?: string
  endsWith?: string
  not?: NestedStringFilter | null
}

export type NestedIntFilter = {
  equals?: number
  in?: Enumerable<number>
  notIn?: Enumerable<number>
  lt?: number
  lte?: number
  gt?: number
  gte?: number
  not?: NestedIntFilter | null
}

export type NestedDateTimeFilter = {
  equals?: Date | string
  in?: Enumerable<Date | string>
  notIn?: Enumerable<Date | string>
  lt?: Date | string
  lte?: Date | string
  gt?: Date | string
  gte?: Date | string
  not?: NestedDateTimeFilter | null
}

export type NestedBoolNullableFilter = {
  equals?: boolean | null
  not?: NestedBoolNullableFilter | null
}

export type NestedStringNullableFilter = {
  equals?: string | null
  in?: Enumerable<string> | null
  notIn?: Enumerable<string> | null
  lt?: string | null
  lte?: string | null
  gt?: string | null
  gte?: string | null
  contains?: string | null
  startsWith?: string | null
  endsWith?: string | null
  not?: NestedStringNullableFilter | null
}

export type NestedBoolFilter = {
  equals?: boolean
  not?: NestedBoolFilter | null
}

export type NestedIntNullableFilter = {
  equals?: number | null
  in?: Enumerable<number> | null
  notIn?: Enumerable<number> | null
  lt?: number | null
  lte?: number | null
  gt?: number | null
  gte?: number | null
  not?: NestedIntNullableFilter | null
}

export type block_alphaCreateWithoutBlockInput = {
  baker: string
  level_position: number
  cycle: number
  cycle_position: number
  voting_period: number
  voting_period_position: number
  voting_period_kind: number
  consumed_gas: string
}

export type contractCreateWithoutBlockInput = {
  spendable: boolean
  delegatable: boolean
  credit?: number | null
  script?: string | null
  addresses_addressesTocontract_address: addressesCreateOneWithoutContract_addressesTocontract_addressInput
  addresses_addressesTocontract_delegate?: addressesCreateOneWithoutContract_addressesTocontract_delegateInput
  addresses_addressesTocontract_mgr?: addressesCreateOneWithoutContract_addressesTocontract_mgrInput
  implicit?: implicitCreateOneWithoutContractInput
  addresses_addressesTocontract_preorig?: addressesCreateOneWithoutContract_addressesTocontract_preorigInput
  contract?: contractCreateOneWithoutOther_contractInput
  other_contract?: contractCreateManyWithoutContractInput
  delegated_contract?: delegated_contractCreateManyWithoutContractInput
  delegation?: delegationCreateManyWithoutContractInput
  origination_contractToorigination_k?: originationCreateManyWithoutContract_contractToorigination_kInput
  origination_contractToorigination_source?: originationCreateManyWithoutContract_contractToorigination_sourceInput
  tx_contractTotx_destination?: txCreateManyWithoutContract_contractTotx_destinationInput
  tx_contractTotx_source?: txCreateManyWithoutContract_contractTotx_sourceInput
}

export type deactivatedCreateWithoutBlockInput = {
  implicit: implicitCreateOneWithoutDeactivatedInput
}

export type endorsementCreateWithoutBlockInput = {
  id: number
  level: number
  slot: number
  operation: operationCreateOneWithoutEndorsementInput
  implicit: implicitCreateOneWithoutEndorsementInput
}

export type operationCreateWithoutBlockInput = {
  hash: string
  autoid?: number
  chain_chainTooperation: chainCreateOneWithoutOperationInput
  endorsement?: endorsementCreateManyWithoutOperationInput
  implicit_implicit_activatedTooperation?: implicitCreateManyWithoutOperation_implicit_activatedTooperationInput
  implicit_implicit_revealedTooperation?: implicitCreateManyWithoutOperation_implicit_revealedTooperationInput
  manager_numbers?: manager_numbersCreateManyWithoutOperationInput
  operation_alpha?: operation_alphaCreateManyWithoutOperationInput
}

export type block_alphaUpdateWithWhereUniqueWithoutBlockInput = {
  where: block_alphaWhereUniqueInput
  data: block_alphaUpdateWithoutBlockDataInput
}

export type block_alphaUpdateManyWithWhereNestedInput = {
  where: block_alphaScalarWhereInput
  data: block_alphaUpdateManyDataInput
}

export type block_alphaScalarWhereInput = {
  AND?: Enumerable<block_alphaScalarWhereInput>
  OR?: Array<block_alphaScalarWhereInput>
  NOT?: Enumerable<block_alphaScalarWhereInput>
  hash?: string | StringFilter
  baker?: string | StringFilter
  level_position?: number | IntFilter
  cycle?: number | IntFilter
  cycle_position?: number | IntFilter
  voting_period?: number | IntFilter
  voting_period_position?: number | IntFilter
  voting_period_kind?: number | IntFilter
  consumed_gas?: string | StringFilter
}

export type block_alphaUpsertWithWhereUniqueWithoutBlockInput = {
  where: block_alphaWhereUniqueInput
  update: block_alphaUpdateWithoutBlockDataInput
  create: block_alphaCreateWithoutBlockInput
}

export type contractUpdateWithWhereUniqueWithoutBlockInput = {
  where: contractWhereUniqueInput
  data: contractUpdateWithoutBlockDataInput
}

export type contractUpdateManyWithWhereNestedInput = {
  where: contractScalarWhereInput
  data: contractUpdateManyDataInput
}

export type contractScalarWhereInput = {
  AND?: Enumerable<contractScalarWhereInput>
  OR?: Array<contractScalarWhereInput>
  NOT?: Enumerable<contractScalarWhereInput>
  address?: string | StringFilter
  block_hash?: string | StringFilter
  mgr?: string | StringNullableFilter | null
  delegate?: string | StringNullableFilter | null
  spendable?: boolean | BoolFilter
  delegatable?: boolean | BoolFilter
  credit?: number | IntNullableFilter | null
  preorig?: string | StringNullableFilter | null
  script?: string | StringNullableFilter | null
}

export type contractUpsertWithWhereUniqueWithoutBlockInput = {
  where: contractWhereUniqueInput
  update: contractUpdateWithoutBlockDataInput
  create: contractCreateWithoutBlockInput
}

export type deactivatedUpdateWithWhereUniqueWithoutBlockInput = {
  where: deactivatedWhereUniqueInput
  data: deactivatedUpdateWithoutBlockDataInput
}

export type deactivatedUpdateManyWithWhereNestedInput = {
  where: deactivatedScalarWhereInput
  data: deactivatedUpdateManyDataInput
}

export type deactivatedScalarWhereInput = {
  AND?: Enumerable<deactivatedScalarWhereInput>
  OR?: Array<deactivatedScalarWhereInput>
  NOT?: Enumerable<deactivatedScalarWhereInput>
  pkh?: string | StringFilter
  block_hash?: string | StringFilter
}

export type deactivatedUpsertWithWhereUniqueWithoutBlockInput = {
  where: deactivatedWhereUniqueInput
  update: deactivatedUpdateWithoutBlockDataInput
  create: deactivatedCreateWithoutBlockInput
}

export type endorsementUpdateWithWhereUniqueWithoutBlockInput = {
  where: endorsementWhereUniqueInput
  data: endorsementUpdateWithoutBlockDataInput
}

export type endorsementUpdateManyWithWhereNestedInput = {
  where: endorsementScalarWhereInput
  data: endorsementUpdateManyDataInput
}

export type endorsementScalarWhereInput = {
  AND?: Enumerable<endorsementScalarWhereInput>
  OR?: Array<endorsementScalarWhereInput>
  NOT?: Enumerable<endorsementScalarWhereInput>
  block_hash?: string | StringFilter
  op?: string | StringFilter
  id?: number | IntFilter
  level?: number | IntFilter
  pkh?: string | StringFilter
  slot?: number | IntFilter
}

export type endorsementUpsertWithWhereUniqueWithoutBlockInput = {
  where: endorsementWhereUniqueInput
  update: endorsementUpdateWithoutBlockDataInput
  create: endorsementCreateWithoutBlockInput
}

export type operationUpdateWithWhereUniqueWithoutBlockInput = {
  where: operationWhereUniqueInput
  data: operationUpdateWithoutBlockDataInput
}

export type operationUpdateManyWithWhereNestedInput = {
  where: operationScalarWhereInput
  data: operationUpdateManyDataInput
}

export type operationScalarWhereInput = {
  AND?: Enumerable<operationScalarWhereInput>
  OR?: Array<operationScalarWhereInput>
  NOT?: Enumerable<operationScalarWhereInput>
  hash?: string | StringFilter
  chain?: string | StringFilter
  block_hash?: string | StringFilter
  autoid?: number | IntFilter
}

export type operationUpsertWithWhereUniqueWithoutBlockInput = {
  where: operationWhereUniqueInput
  update: operationUpdateWithoutBlockDataInput
  create: operationCreateWithoutBlockInput
}

export type blockCreateWithoutBlock_alphaInput = {
  hash: string
  level: number
  proto: number
  predecessor: string
  timestamp: Date | string
  validation_passes: number
  merkle_root: string
  fitness: string
  context_hash: string
  sterile?: boolean | null
  contract?: contractCreateManyWithoutBlockInput
  deactivated?: deactivatedCreateManyWithoutBlockInput
  endorsement?: endorsementCreateManyWithoutBlockInput
  operation?: operationCreateManyWithoutBlockInput
}

export type blockUpdateWithoutBlock_alphaDataInput = {
  hash?: string | StringFieldUpdateOperationsInput
  level?: number | IntFieldUpdateOperationsInput
  proto?: number | IntFieldUpdateOperationsInput
  predecessor?: string | StringFieldUpdateOperationsInput
  timestamp?: Date | string | DateTimeFieldUpdateOperationsInput
  validation_passes?: number | IntFieldUpdateOperationsInput
  merkle_root?: string | StringFieldUpdateOperationsInput
  fitness?: string | StringFieldUpdateOperationsInput
  context_hash?: string | StringFieldUpdateOperationsInput
  sterile?: boolean | NullableBoolFieldUpdateOperationsInput | null
  contract?: contractUpdateManyWithoutBlockInput
  deactivated?: deactivatedUpdateManyWithoutBlockInput
  endorsement?: endorsementUpdateManyWithoutBlockInput
  operation?: operationUpdateManyWithoutBlockInput
}

export type blockUpsertWithoutBlock_alphaInput = {
  update: blockUpdateWithoutBlock_alphaDataInput
  create: blockCreateWithoutBlock_alphaInput
}

export type operationCreateWithoutChain_chainTooperationInput = {
  hash: string
  autoid?: number
  block: blockCreateOneWithoutOperationInput
  endorsement?: endorsementCreateManyWithoutOperationInput
  implicit_implicit_activatedTooperation?: implicitCreateManyWithoutOperation_implicit_activatedTooperationInput
  implicit_implicit_revealedTooperation?: implicitCreateManyWithoutOperation_implicit_revealedTooperationInput
  manager_numbers?: manager_numbersCreateManyWithoutOperationInput
  operation_alpha?: operation_alphaCreateManyWithoutOperationInput
}

export type operationUpdateWithWhereUniqueWithoutChain_chainTooperationInput = {
  where: operationWhereUniqueInput
  data: operationUpdateWithoutChain_chainTooperationDataInput
}

export type operationUpsertWithWhereUniqueWithoutChain_chainTooperationInput = {
  where: operationWhereUniqueInput
  update: operationUpdateWithoutChain_chainTooperationDataInput
  create: operationCreateWithoutChain_chainTooperationInput
}

export type addressesCreateWithoutContract_addressesTocontract_addressInput = {
  address: string
  contract_addressesTocontract_delegate?: contractCreateManyWithoutAddresses_addressesTocontract_delegateInput
  contract_addressesTocontract_mgr?: contractCreateManyWithoutAddresses_addressesTocontract_mgrInput
  contract_addressesTocontract_preorig?: contractCreateManyWithoutAddresses_addressesTocontract_preorigInput
  delegation?: delegationCreateManyWithoutAddressesInput
  implicit?: implicitCreateManyWithoutAddressesInput
  operation_alpha_addressesTooperation_alpha_receiver?: operation_alphaCreateManyWithoutAddresses_addressesTooperation_alpha_receiverInput
  operation_alpha_addressesTooperation_alpha_sender?: operation_alphaCreateManyWithoutAddresses_addressesTooperation_alpha_senderInput
}

export type blockCreateWithoutContractInput = {
  hash: string
  level: number
  proto: number
  predecessor: string
  timestamp: Date | string
  validation_passes: number
  merkle_root: string
  fitness: string
  context_hash: string
  sterile?: boolean | null
  block_alpha?: block_alphaCreateManyWithoutBlockInput
  deactivated?: deactivatedCreateManyWithoutBlockInput
  endorsement?: endorsementCreateManyWithoutBlockInput
  operation?: operationCreateManyWithoutBlockInput
}

export type addressesCreateWithoutContract_addressesTocontract_delegateInput = {
  address: string
  contract_addressesTocontract_address?: contractCreateManyWithoutAddresses_addressesTocontract_addressInput
  contract_addressesTocontract_mgr?: contractCreateManyWithoutAddresses_addressesTocontract_mgrInput
  contract_addressesTocontract_preorig?: contractCreateManyWithoutAddresses_addressesTocontract_preorigInput
  delegation?: delegationCreateManyWithoutAddressesInput
  implicit?: implicitCreateManyWithoutAddressesInput
  operation_alpha_addressesTooperation_alpha_receiver?: operation_alphaCreateManyWithoutAddresses_addressesTooperation_alpha_receiverInput
  operation_alpha_addressesTooperation_alpha_sender?: operation_alphaCreateManyWithoutAddresses_addressesTooperation_alpha_senderInput
}

export type addressesCreateWithoutContract_addressesTocontract_mgrInput = {
  address: string
  contract_addressesTocontract_address?: contractCreateManyWithoutAddresses_addressesTocontract_addressInput
  contract_addressesTocontract_delegate?: contractCreateManyWithoutAddresses_addressesTocontract_delegateInput
  contract_addressesTocontract_preorig?: contractCreateManyWithoutAddresses_addressesTocontract_preorigInput
  delegation?: delegationCreateManyWithoutAddressesInput
  implicit?: implicitCreateManyWithoutAddressesInput
  operation_alpha_addressesTooperation_alpha_receiver?: operation_alphaCreateManyWithoutAddresses_addressesTooperation_alpha_receiverInput
  operation_alpha_addressesTooperation_alpha_sender?: operation_alphaCreateManyWithoutAddresses_addressesTooperation_alpha_senderInput
}

export type implicitCreateWithoutContractInput = {
  pk?: string | null
  autoid?: number
  operation_implicit_activatedTooperation?: operationCreateOneWithoutImplicit_implicit_activatedTooperationInput
  addresses: addressesCreateOneWithoutImplicitInput
  operation_implicit_revealedTooperation?: operationCreateOneWithoutImplicit_implicit_revealedTooperationInput
  deactivated?: deactivatedCreateManyWithoutImplicitInput
  delegated_contract?: delegated_contractCreateManyWithoutImplicitInput
  endorsement?: endorsementCreateManyWithoutImplicitInput
}

export type addressesCreateWithoutContract_addressesTocontract_preorigInput = {
  address: string
  contract_addressesTocontract_address?: contractCreateManyWithoutAddresses_addressesTocontract_addressInput
  contract_addressesTocontract_delegate?: contractCreateManyWithoutAddresses_addressesTocontract_delegateInput
  contract_addressesTocontract_mgr?: contractCreateManyWithoutAddresses_addressesTocontract_mgrInput
  delegation?: delegationCreateManyWithoutAddressesInput
  implicit?: implicitCreateManyWithoutAddressesInput
  operation_alpha_addressesTooperation_alpha_receiver?: operation_alphaCreateManyWithoutAddresses_addressesTooperation_alpha_receiverInput
  operation_alpha_addressesTooperation_alpha_sender?: operation_alphaCreateManyWithoutAddresses_addressesTooperation_alpha_senderInput
}

export type contractCreateWithoutOther_contractInput = {
  spendable: boolean
  delegatable: boolean
  credit?: number | null
  script?: string | null
  addresses_addressesTocontract_address: addressesCreateOneWithoutContract_addressesTocontract_addressInput
  block: blockCreateOneWithoutContractInput
  addresses_addressesTocontract_delegate?: addressesCreateOneWithoutContract_addressesTocontract_delegateInput
  addresses_addressesTocontract_mgr?: addressesCreateOneWithoutContract_addressesTocontract_mgrInput
  implicit?: implicitCreateOneWithoutContractInput
  addresses_addressesTocontract_preorig?: addressesCreateOneWithoutContract_addressesTocontract_preorigInput
  contract?: contractCreateOneWithoutOther_contractInput
  delegated_contract?: delegated_contractCreateManyWithoutContractInput
  delegation?: delegationCreateManyWithoutContractInput
  origination_contractToorigination_k?: originationCreateManyWithoutContract_contractToorigination_kInput
  origination_contractToorigination_source?: originationCreateManyWithoutContract_contractToorigination_sourceInput
  tx_contractTotx_destination?: txCreateManyWithoutContract_contractTotx_destinationInput
  tx_contractTotx_source?: txCreateManyWithoutContract_contractTotx_sourceInput
}

export type contractCreateWithoutContractInput = {
  spendable: boolean
  delegatable: boolean
  credit?: number | null
  script?: string | null
  addresses_addressesTocontract_address: addressesCreateOneWithoutContract_addressesTocontract_addressInput
  block: blockCreateOneWithoutContractInput
  addresses_addressesTocontract_delegate?: addressesCreateOneWithoutContract_addressesTocontract_delegateInput
  addresses_addressesTocontract_mgr?: addressesCreateOneWithoutContract_addressesTocontract_mgrInput
  implicit?: implicitCreateOneWithoutContractInput
  addresses_addressesTocontract_preorig?: addressesCreateOneWithoutContract_addressesTocontract_preorigInput
  other_contract?: contractCreateManyWithoutContractInput
  delegated_contract?: delegated_contractCreateManyWithoutContractInput
  delegation?: delegationCreateManyWithoutContractInput
  origination_contractToorigination_k?: originationCreateManyWithoutContract_contractToorigination_kInput
  origination_contractToorigination_source?: originationCreateManyWithoutContract_contractToorigination_sourceInput
  tx_contractTotx_destination?: txCreateManyWithoutContract_contractTotx_destinationInput
  tx_contractTotx_source?: txCreateManyWithoutContract_contractTotx_sourceInput
}

export type delegated_contractCreateWithoutContractInput = {
  snapshot: snapshotCreateOneWithoutDelegated_contractInput
  implicit: implicitCreateOneWithoutDelegated_contractInput
}

export type delegationCreateWithoutContractInput = {
  autoid?: number
  operation_alpha: operation_alphaCreateOneWithoutDelegationInput
  addresses?: addressesCreateOneWithoutDelegationInput
}

export type originationCreateWithoutContract_contractToorigination_kInput = {
  autoid?: number
  operation_alpha: operation_alphaCreateOneWithoutOriginationInput
  contract_contractToorigination_source: contractCreateOneWithoutOrigination_contractToorigination_sourceInput
}

export type originationCreateWithoutContract_contractToorigination_sourceInput = {
  autoid?: number
  contract_contractToorigination_k: contractCreateOneWithoutOrigination_contractToorigination_kInput
  operation_alpha: operation_alphaCreateOneWithoutOriginationInput
}

export type txCreateWithoutContract_contractTotx_destinationInput = {
  fee: number
  amount: number
  parameters?: string | null
  storage?: string | null
  consumed_gas: string
  storage_size: string
  paid_storage_size_diff: string
  entrypoint?: string | null
  bigmapdiff?: string | null
  autoid?: number
  operation_alpha: operation_alphaCreateOneWithoutTxInput
  contract_contractTotx_source: contractCreateOneWithoutTx_contractTotx_sourceInput
}

export type txCreateWithoutContract_contractTotx_sourceInput = {
  fee: number
  amount: number
  parameters?: string | null
  storage?: string | null
  consumed_gas: string
  storage_size: string
  paid_storage_size_diff: string
  entrypoint?: string | null
  bigmapdiff?: string | null
  autoid?: number
  contract_contractTotx_destination: contractCreateOneWithoutTx_contractTotx_destinationInput
  operation_alpha: operation_alphaCreateOneWithoutTxInput
}

export type addressesUpdateWithoutContract_addressesTocontract_addressDataInput = {
  address?: string | StringFieldUpdateOperationsInput
  contract_addressesTocontract_delegate?: contractUpdateManyWithoutAddresses_addressesTocontract_delegateInput
  contract_addressesTocontract_mgr?: contractUpdateManyWithoutAddresses_addressesTocontract_mgrInput
  contract_addressesTocontract_preorig?: contractUpdateManyWithoutAddresses_addressesTocontract_preorigInput
  delegation?: delegationUpdateManyWithoutAddressesInput
  implicit?: implicitUpdateManyWithoutAddressesInput
  operation_alpha_addressesTooperation_alpha_receiver?: operation_alphaUpdateManyWithoutAddresses_addressesTooperation_alpha_receiverInput
  operation_alpha_addressesTooperation_alpha_sender?: operation_alphaUpdateManyWithoutAddresses_addressesTooperation_alpha_senderInput
}

export type addressesUpsertWithoutContract_addressesTocontract_addressInput = {
  update: addressesUpdateWithoutContract_addressesTocontract_addressDataInput
  create: addressesCreateWithoutContract_addressesTocontract_addressInput
}

export type blockUpdateWithoutContractDataInput = {
  hash?: string | StringFieldUpdateOperationsInput
  level?: number | IntFieldUpdateOperationsInput
  proto?: number | IntFieldUpdateOperationsInput
  predecessor?: string | StringFieldUpdateOperationsInput
  timestamp?: Date | string | DateTimeFieldUpdateOperationsInput
  validation_passes?: number | IntFieldUpdateOperationsInput
  merkle_root?: string | StringFieldUpdateOperationsInput
  fitness?: string | StringFieldUpdateOperationsInput
  context_hash?: string | StringFieldUpdateOperationsInput
  sterile?: boolean | NullableBoolFieldUpdateOperationsInput | null
  block_alpha?: block_alphaUpdateManyWithoutBlockInput
  deactivated?: deactivatedUpdateManyWithoutBlockInput
  endorsement?: endorsementUpdateManyWithoutBlockInput
  operation?: operationUpdateManyWithoutBlockInput
}

export type blockUpsertWithoutContractInput = {
  update: blockUpdateWithoutContractDataInput
  create: blockCreateWithoutContractInput
}

export type addressesUpdateWithoutContract_addressesTocontract_delegateDataInput = {
  address?: string | StringFieldUpdateOperationsInput
  contract_addressesTocontract_address?: contractUpdateManyWithoutAddresses_addressesTocontract_addressInput
  contract_addressesTocontract_mgr?: contractUpdateManyWithoutAddresses_addressesTocontract_mgrInput
  contract_addressesTocontract_preorig?: contractUpdateManyWithoutAddresses_addressesTocontract_preorigInput
  delegation?: delegationUpdateManyWithoutAddressesInput
  implicit?: implicitUpdateManyWithoutAddressesInput
  operation_alpha_addressesTooperation_alpha_receiver?: operation_alphaUpdateManyWithoutAddresses_addressesTooperation_alpha_receiverInput
  operation_alpha_addressesTooperation_alpha_sender?: operation_alphaUpdateManyWithoutAddresses_addressesTooperation_alpha_senderInput
}

export type addressesUpsertWithoutContract_addressesTocontract_delegateInput = {
  update: addressesUpdateWithoutContract_addressesTocontract_delegateDataInput
  create: addressesCreateWithoutContract_addressesTocontract_delegateInput
}

export type addressesUpdateWithoutContract_addressesTocontract_mgrDataInput = {
  address?: string | StringFieldUpdateOperationsInput
  contract_addressesTocontract_address?: contractUpdateManyWithoutAddresses_addressesTocontract_addressInput
  contract_addressesTocontract_delegate?: contractUpdateManyWithoutAddresses_addressesTocontract_delegateInput
  contract_addressesTocontract_preorig?: contractUpdateManyWithoutAddresses_addressesTocontract_preorigInput
  delegation?: delegationUpdateManyWithoutAddressesInput
  implicit?: implicitUpdateManyWithoutAddressesInput
  operation_alpha_addressesTooperation_alpha_receiver?: operation_alphaUpdateManyWithoutAddresses_addressesTooperation_alpha_receiverInput
  operation_alpha_addressesTooperation_alpha_sender?: operation_alphaUpdateManyWithoutAddresses_addressesTooperation_alpha_senderInput
}

export type addressesUpsertWithoutContract_addressesTocontract_mgrInput = {
  update: addressesUpdateWithoutContract_addressesTocontract_mgrDataInput
  create: addressesCreateWithoutContract_addressesTocontract_mgrInput
}

export type implicitUpdateWithoutContractDataInput = {
  pk?: string | NullableStringFieldUpdateOperationsInput | null
  autoid?: number | IntFieldUpdateOperationsInput
  operation_implicit_activatedTooperation?: operationUpdateOneWithoutImplicit_implicit_activatedTooperationInput
  addresses?: addressesUpdateOneRequiredWithoutImplicitInput
  operation_implicit_revealedTooperation?: operationUpdateOneWithoutImplicit_implicit_revealedTooperationInput
  deactivated?: deactivatedUpdateManyWithoutImplicitInput
  delegated_contract?: delegated_contractUpdateManyWithoutImplicitInput
  endorsement?: endorsementUpdateManyWithoutImplicitInput
}

export type implicitUpsertWithoutContractInput = {
  update: implicitUpdateWithoutContractDataInput
  create: implicitCreateWithoutContractInput
}

export type addressesUpdateWithoutContract_addressesTocontract_preorigDataInput = {
  address?: string | StringFieldUpdateOperationsInput
  contract_addressesTocontract_address?: contractUpdateManyWithoutAddresses_addressesTocontract_addressInput
  contract_addressesTocontract_delegate?: contractUpdateManyWithoutAddresses_addressesTocontract_delegateInput
  contract_addressesTocontract_mgr?: contractUpdateManyWithoutAddresses_addressesTocontract_mgrInput
  delegation?: delegationUpdateManyWithoutAddressesInput
  implicit?: implicitUpdateManyWithoutAddressesInput
  operation_alpha_addressesTooperation_alpha_receiver?: operation_alphaUpdateManyWithoutAddresses_addressesTooperation_alpha_receiverInput
  operation_alpha_addressesTooperation_alpha_sender?: operation_alphaUpdateManyWithoutAddresses_addressesTooperation_alpha_senderInput
}

export type addressesUpsertWithoutContract_addressesTocontract_preorigInput = {
  update: addressesUpdateWithoutContract_addressesTocontract_preorigDataInput
  create: addressesCreateWithoutContract_addressesTocontract_preorigInput
}

export type contractUpdateWithoutOther_contractDataInput = {
  spendable?: boolean | BoolFieldUpdateOperationsInput
  delegatable?: boolean | BoolFieldUpdateOperationsInput
  credit?: number | NullableIntFieldUpdateOperationsInput | null
  script?: string | NullableStringFieldUpdateOperationsInput | null
  addresses_addressesTocontract_address?: addressesUpdateOneRequiredWithoutContract_addressesTocontract_addressInput
  block?: blockUpdateOneRequiredWithoutContractInput
  addresses_addressesTocontract_delegate?: addressesUpdateOneWithoutContract_addressesTocontract_delegateInput
  addresses_addressesTocontract_mgr?: addressesUpdateOneWithoutContract_addressesTocontract_mgrInput
  implicit?: implicitUpdateOneWithoutContractInput
  addresses_addressesTocontract_preorig?: addressesUpdateOneWithoutContract_addressesTocontract_preorigInput
  contract?: contractUpdateOneWithoutOther_contractInput
  delegated_contract?: delegated_contractUpdateManyWithoutContractInput
  delegation?: delegationUpdateManyWithoutContractInput
  origination_contractToorigination_k?: originationUpdateManyWithoutContract_contractToorigination_kInput
  origination_contractToorigination_source?: originationUpdateManyWithoutContract_contractToorigination_sourceInput
  tx_contractTotx_destination?: txUpdateManyWithoutContract_contractTotx_destinationInput
  tx_contractTotx_source?: txUpdateManyWithoutContract_contractTotx_sourceInput
}

export type contractUpsertWithoutOther_contractInput = {
  update: contractUpdateWithoutOther_contractDataInput
  create: contractCreateWithoutOther_contractInput
}

export type contractUpdateWithWhereUniqueWithoutContractInput = {
  where: contractWhereUniqueInput
  data: contractUpdateWithoutContractDataInput
}

export type contractUpsertWithWhereUniqueWithoutContractInput = {
  where: contractWhereUniqueInput
  update: contractUpdateWithoutContractDataInput
  create: contractCreateWithoutContractInput
}

export type delegated_contractUpdateWithWhereUniqueWithoutContractInput = {
  where: delegated_contractWhereUniqueInput
  data: delegated_contractUpdateWithoutContractDataInput
}

export type delegated_contractUpdateManyWithWhereNestedInput = {
  where: delegated_contractScalarWhereInput
  data: delegated_contractUpdateManyDataInput
}

export type delegated_contractScalarWhereInput = {
  AND?: Enumerable<delegated_contractScalarWhereInput>
  OR?: Array<delegated_contractScalarWhereInput>
  NOT?: Enumerable<delegated_contractScalarWhereInput>
  delegate?: string | StringFilter
  delegator?: string | StringFilter
  cycle?: number | IntFilter
  level?: number | IntFilter
}

export type delegated_contractUpsertWithWhereUniqueWithoutContractInput = {
  where: delegated_contractWhereUniqueInput
  update: delegated_contractUpdateWithoutContractDataInput
  create: delegated_contractCreateWithoutContractInput
}

export type delegationUpdateWithWhereUniqueWithoutContractInput = {
  where: delegationWhereUniqueInput
  data: delegationUpdateWithoutContractDataInput
}

export type delegationUpdateManyWithWhereNestedInput = {
  where: delegationScalarWhereInput
  data: delegationUpdateManyDataInput
}

export type delegationScalarWhereInput = {
  AND?: Enumerable<delegationScalarWhereInput>
  OR?: Array<delegationScalarWhereInput>
  NOT?: Enumerable<delegationScalarWhereInput>
  operation_hash?: string | StringFilter
  op_id?: number | IntFilter
  source?: string | StringFilter
  pkh?: string | StringNullableFilter | null
  autoid?: number | IntFilter
}

export type delegationUpsertWithWhereUniqueWithoutContractInput = {
  where: delegationWhereUniqueInput
  update: delegationUpdateWithoutContractDataInput
  create: delegationCreateWithoutContractInput
}

export type originationUpdateWithWhereUniqueWithoutContract_contractToorigination_kInput = {
  where: originationWhereUniqueInput
  data: originationUpdateWithoutContract_contractToorigination_kDataInput
}

export type originationUpdateManyWithWhereNestedInput = {
  where: originationScalarWhereInput
  data: originationUpdateManyDataInput
}

export type originationScalarWhereInput = {
  AND?: Enumerable<originationScalarWhereInput>
  OR?: Array<originationScalarWhereInput>
  NOT?: Enumerable<originationScalarWhereInput>
  operation_hash?: string | StringFilter
  op_id?: number | IntFilter
  source?: string | StringFilter
  k?: string | StringFilter
  autoid?: number | IntFilter
}

export type originationUpsertWithWhereUniqueWithoutContract_contractToorigination_kInput = {
  where: originationWhereUniqueInput
  update: originationUpdateWithoutContract_contractToorigination_kDataInput
  create: originationCreateWithoutContract_contractToorigination_kInput
}

export type originationUpdateWithWhereUniqueWithoutContract_contractToorigination_sourceInput = {
  where: originationWhereUniqueInput
  data: originationUpdateWithoutContract_contractToorigination_sourceDataInput
}

export type originationUpsertWithWhereUniqueWithoutContract_contractToorigination_sourceInput = {
  where: originationWhereUniqueInput
  update: originationUpdateWithoutContract_contractToorigination_sourceDataInput
  create: originationCreateWithoutContract_contractToorigination_sourceInput
}

export type txUpdateWithWhereUniqueWithoutContract_contractTotx_destinationInput = {
  where: txWhereUniqueInput
  data: txUpdateWithoutContract_contractTotx_destinationDataInput
}

export type txUpdateManyWithWhereNestedInput = {
  where: txScalarWhereInput
  data: txUpdateManyDataInput
}

export type txScalarWhereInput = {
  AND?: Enumerable<txScalarWhereInput>
  OR?: Array<txScalarWhereInput>
  NOT?: Enumerable<txScalarWhereInput>
  operation_hash?: string | StringFilter
  op_id?: number | IntFilter
  source?: string | StringFilter
  destination?: string | StringFilter
  fee?: number | IntFilter
  amount?: number | IntFilter
  parameters?: string | StringNullableFilter | null
  storage?: string | StringNullableFilter | null
  consumed_gas?: string | StringFilter
  storage_size?: string | StringFilter
  paid_storage_size_diff?: string | StringFilter
  entrypoint?: string | StringNullableFilter | null
  bigmapdiff?: string | StringNullableFilter | null
  autoid?: number | IntFilter
}

export type txUpsertWithWhereUniqueWithoutContract_contractTotx_destinationInput = {
  where: txWhereUniqueInput
  update: txUpdateWithoutContract_contractTotx_destinationDataInput
  create: txCreateWithoutContract_contractTotx_destinationInput
}

export type txUpdateWithWhereUniqueWithoutContract_contractTotx_sourceInput = {
  where: txWhereUniqueInput
  data: txUpdateWithoutContract_contractTotx_sourceDataInput
}

export type txUpsertWithWhereUniqueWithoutContract_contractTotx_sourceInput = {
  where: txWhereUniqueInput
  update: txUpdateWithoutContract_contractTotx_sourceDataInput
  create: txCreateWithoutContract_contractTotx_sourceInput
}

export type blockCreateWithoutDeactivatedInput = {
  hash: string
  level: number
  proto: number
  predecessor: string
  timestamp: Date | string
  validation_passes: number
  merkle_root: string
  fitness: string
  context_hash: string
  sterile?: boolean | null
  block_alpha?: block_alphaCreateManyWithoutBlockInput
  contract?: contractCreateManyWithoutBlockInput
  endorsement?: endorsementCreateManyWithoutBlockInput
  operation?: operationCreateManyWithoutBlockInput
}

export type implicitCreateWithoutDeactivatedInput = {
  pk?: string | null
  autoid?: number
  operation_implicit_activatedTooperation?: operationCreateOneWithoutImplicit_implicit_activatedTooperationInput
  addresses: addressesCreateOneWithoutImplicitInput
  operation_implicit_revealedTooperation?: operationCreateOneWithoutImplicit_implicit_revealedTooperationInput
  contract?: contractCreateManyWithoutImplicitInput
  delegated_contract?: delegated_contractCreateManyWithoutImplicitInput
  endorsement?: endorsementCreateManyWithoutImplicitInput
}

export type blockUpdateWithoutDeactivatedDataInput = {
  hash?: string | StringFieldUpdateOperationsInput
  level?: number | IntFieldUpdateOperationsInput
  proto?: number | IntFieldUpdateOperationsInput
  predecessor?: string | StringFieldUpdateOperationsInput
  timestamp?: Date | string | DateTimeFieldUpdateOperationsInput
  validation_passes?: number | IntFieldUpdateOperationsInput
  merkle_root?: string | StringFieldUpdateOperationsInput
  fitness?: string | StringFieldUpdateOperationsInput
  context_hash?: string | StringFieldUpdateOperationsInput
  sterile?: boolean | NullableBoolFieldUpdateOperationsInput | null
  block_alpha?: block_alphaUpdateManyWithoutBlockInput
  contract?: contractUpdateManyWithoutBlockInput
  endorsement?: endorsementUpdateManyWithoutBlockInput
  operation?: operationUpdateManyWithoutBlockInput
}

export type blockUpsertWithoutDeactivatedInput = {
  update: blockUpdateWithoutDeactivatedDataInput
  create: blockCreateWithoutDeactivatedInput
}

export type implicitUpdateWithoutDeactivatedDataInput = {
  pk?: string | NullableStringFieldUpdateOperationsInput | null
  autoid?: number | IntFieldUpdateOperationsInput
  operation_implicit_activatedTooperation?: operationUpdateOneWithoutImplicit_implicit_activatedTooperationInput
  addresses?: addressesUpdateOneRequiredWithoutImplicitInput
  operation_implicit_revealedTooperation?: operationUpdateOneWithoutImplicit_implicit_revealedTooperationInput
  contract?: contractUpdateManyWithoutImplicitInput
  delegated_contract?: delegated_contractUpdateManyWithoutImplicitInput
  endorsement?: endorsementUpdateManyWithoutImplicitInput
}

export type implicitUpsertWithoutDeactivatedInput = {
  update: implicitUpdateWithoutDeactivatedDataInput
  create: implicitCreateWithoutDeactivatedInput
}

export type snapshotCreateWithoutDelegated_contractInput = {
  cycle: number
  level: number
}

export type implicitCreateWithoutDelegated_contractInput = {
  pk?: string | null
  autoid?: number
  operation_implicit_activatedTooperation?: operationCreateOneWithoutImplicit_implicit_activatedTooperationInput
  addresses: addressesCreateOneWithoutImplicitInput
  operation_implicit_revealedTooperation?: operationCreateOneWithoutImplicit_implicit_revealedTooperationInput
  contract?: contractCreateManyWithoutImplicitInput
  deactivated?: deactivatedCreateManyWithoutImplicitInput
  endorsement?: endorsementCreateManyWithoutImplicitInput
}

export type contractCreateWithoutDelegated_contractInput = {
  spendable: boolean
  delegatable: boolean
  credit?: number | null
  script?: string | null
  addresses_addressesTocontract_address: addressesCreateOneWithoutContract_addressesTocontract_addressInput
  block: blockCreateOneWithoutContractInput
  addresses_addressesTocontract_delegate?: addressesCreateOneWithoutContract_addressesTocontract_delegateInput
  addresses_addressesTocontract_mgr?: addressesCreateOneWithoutContract_addressesTocontract_mgrInput
  implicit?: implicitCreateOneWithoutContractInput
  addresses_addressesTocontract_preorig?: addressesCreateOneWithoutContract_addressesTocontract_preorigInput
  contract?: contractCreateOneWithoutOther_contractInput
  other_contract?: contractCreateManyWithoutContractInput
  delegation?: delegationCreateManyWithoutContractInput
  origination_contractToorigination_k?: originationCreateManyWithoutContract_contractToorigination_kInput
  origination_contractToorigination_source?: originationCreateManyWithoutContract_contractToorigination_sourceInput
  tx_contractTotx_destination?: txCreateManyWithoutContract_contractTotx_destinationInput
  tx_contractTotx_source?: txCreateManyWithoutContract_contractTotx_sourceInput
}

export type snapshotUpdateWithoutDelegated_contractDataInput = {
  cycle?: number | IntFieldUpdateOperationsInput
  level?: number | IntFieldUpdateOperationsInput
}

export type snapshotUpsertWithoutDelegated_contractInput = {
  update: snapshotUpdateWithoutDelegated_contractDataInput
  create: snapshotCreateWithoutDelegated_contractInput
}

export type implicitUpdateWithoutDelegated_contractDataInput = {
  pk?: string | NullableStringFieldUpdateOperationsInput | null
  autoid?: number | IntFieldUpdateOperationsInput
  operation_implicit_activatedTooperation?: operationUpdateOneWithoutImplicit_implicit_activatedTooperationInput
  addresses?: addressesUpdateOneRequiredWithoutImplicitInput
  operation_implicit_revealedTooperation?: operationUpdateOneWithoutImplicit_implicit_revealedTooperationInput
  contract?: contractUpdateManyWithoutImplicitInput
  deactivated?: deactivatedUpdateManyWithoutImplicitInput
  endorsement?: endorsementUpdateManyWithoutImplicitInput
}

export type implicitUpsertWithoutDelegated_contractInput = {
  update: implicitUpdateWithoutDelegated_contractDataInput
  create: implicitCreateWithoutDelegated_contractInput
}

export type contractUpdateWithoutDelegated_contractDataInput = {
  spendable?: boolean | BoolFieldUpdateOperationsInput
  delegatable?: boolean | BoolFieldUpdateOperationsInput
  credit?: number | NullableIntFieldUpdateOperationsInput | null
  script?: string | NullableStringFieldUpdateOperationsInput | null
  addresses_addressesTocontract_address?: addressesUpdateOneRequiredWithoutContract_addressesTocontract_addressInput
  block?: blockUpdateOneRequiredWithoutContractInput
  addresses_addressesTocontract_delegate?: addressesUpdateOneWithoutContract_addressesTocontract_delegateInput
  addresses_addressesTocontract_mgr?: addressesUpdateOneWithoutContract_addressesTocontract_mgrInput
  implicit?: implicitUpdateOneWithoutContractInput
  addresses_addressesTocontract_preorig?: addressesUpdateOneWithoutContract_addressesTocontract_preorigInput
  contract?: contractUpdateOneWithoutOther_contractInput
  other_contract?: contractUpdateManyWithoutContractInput
  delegation?: delegationUpdateManyWithoutContractInput
  origination_contractToorigination_k?: originationUpdateManyWithoutContract_contractToorigination_kInput
  origination_contractToorigination_source?: originationUpdateManyWithoutContract_contractToorigination_sourceInput
  tx_contractTotx_destination?: txUpdateManyWithoutContract_contractTotx_destinationInput
  tx_contractTotx_source?: txUpdateManyWithoutContract_contractTotx_sourceInput
}

export type contractUpsertWithoutDelegated_contractInput = {
  update: contractUpdateWithoutDelegated_contractDataInput
  create: contractCreateWithoutDelegated_contractInput
}

export type operation_alphaCreateWithoutDelegationInput = {
  id: number
  operation_kind: number
  autoid?: number
  operation: operationCreateOneWithoutOperation_alphaInput
  addresses_addressesTooperation_alpha_receiver?: addressesCreateOneWithoutOperation_alpha_addressesTooperation_alpha_receiverInput
  addresses_addressesTooperation_alpha_sender?: addressesCreateOneWithoutOperation_alpha_addressesTooperation_alpha_senderInput
  origination?: originationCreateManyWithoutOperation_alphaInput
  tx?: txCreateManyWithoutOperation_alphaInput
}

export type addressesCreateWithoutDelegationInput = {
  address: string
  contract_addressesTocontract_address?: contractCreateManyWithoutAddresses_addressesTocontract_addressInput
  contract_addressesTocontract_delegate?: contractCreateManyWithoutAddresses_addressesTocontract_delegateInput
  contract_addressesTocontract_mgr?: contractCreateManyWithoutAddresses_addressesTocontract_mgrInput
  contract_addressesTocontract_preorig?: contractCreateManyWithoutAddresses_addressesTocontract_preorigInput
  implicit?: implicitCreateManyWithoutAddressesInput
  operation_alpha_addressesTooperation_alpha_receiver?: operation_alphaCreateManyWithoutAddresses_addressesTooperation_alpha_receiverInput
  operation_alpha_addressesTooperation_alpha_sender?: operation_alphaCreateManyWithoutAddresses_addressesTooperation_alpha_senderInput
}

export type contractCreateWithoutDelegationInput = {
  spendable: boolean
  delegatable: boolean
  credit?: number | null
  script?: string | null
  addresses_addressesTocontract_address: addressesCreateOneWithoutContract_addressesTocontract_addressInput
  block: blockCreateOneWithoutContractInput
  addresses_addressesTocontract_delegate?: addressesCreateOneWithoutContract_addressesTocontract_delegateInput
  addresses_addressesTocontract_mgr?: addressesCreateOneWithoutContract_addressesTocontract_mgrInput
  implicit?: implicitCreateOneWithoutContractInput
  addresses_addressesTocontract_preorig?: addressesCreateOneWithoutContract_addressesTocontract_preorigInput
  contract?: contractCreateOneWithoutOther_contractInput
  other_contract?: contractCreateManyWithoutContractInput
  delegated_contract?: delegated_contractCreateManyWithoutContractInput
  origination_contractToorigination_k?: originationCreateManyWithoutContract_contractToorigination_kInput
  origination_contractToorigination_source?: originationCreateManyWithoutContract_contractToorigination_sourceInput
  tx_contractTotx_destination?: txCreateManyWithoutContract_contractTotx_destinationInput
  tx_contractTotx_source?: txCreateManyWithoutContract_contractTotx_sourceInput
}

export type operation_alphaUpdateWithoutDelegationDataInput = {
  id?: number | IntFieldUpdateOperationsInput
  operation_kind?: number | IntFieldUpdateOperationsInput
  autoid?: number | IntFieldUpdateOperationsInput
  operation?: operationUpdateOneRequiredWithoutOperation_alphaInput
  addresses_addressesTooperation_alpha_receiver?: addressesUpdateOneWithoutOperation_alpha_addressesTooperation_alpha_receiverInput
  addresses_addressesTooperation_alpha_sender?: addressesUpdateOneWithoutOperation_alpha_addressesTooperation_alpha_senderInput
  origination?: originationUpdateManyWithoutOperation_alphaInput
  tx?: txUpdateManyWithoutOperation_alphaInput
}

export type operation_alphaUpsertWithoutDelegationInput = {
  update: operation_alphaUpdateWithoutDelegationDataInput
  create: operation_alphaCreateWithoutDelegationInput
}

export type addressesUpdateWithoutDelegationDataInput = {
  address?: string | StringFieldUpdateOperationsInput
  contract_addressesTocontract_address?: contractUpdateManyWithoutAddresses_addressesTocontract_addressInput
  contract_addressesTocontract_delegate?: contractUpdateManyWithoutAddresses_addressesTocontract_delegateInput
  contract_addressesTocontract_mgr?: contractUpdateManyWithoutAddresses_addressesTocontract_mgrInput
  contract_addressesTocontract_preorig?: contractUpdateManyWithoutAddresses_addressesTocontract_preorigInput
  implicit?: implicitUpdateManyWithoutAddressesInput
  operation_alpha_addressesTooperation_alpha_receiver?: operation_alphaUpdateManyWithoutAddresses_addressesTooperation_alpha_receiverInput
  operation_alpha_addressesTooperation_alpha_sender?: operation_alphaUpdateManyWithoutAddresses_addressesTooperation_alpha_senderInput
}

export type addressesUpsertWithoutDelegationInput = {
  update: addressesUpdateWithoutDelegationDataInput
  create: addressesCreateWithoutDelegationInput
}

export type contractUpdateWithoutDelegationDataInput = {
  spendable?: boolean | BoolFieldUpdateOperationsInput
  delegatable?: boolean | BoolFieldUpdateOperationsInput
  credit?: number | NullableIntFieldUpdateOperationsInput | null
  script?: string | NullableStringFieldUpdateOperationsInput | null
  addresses_addressesTocontract_address?: addressesUpdateOneRequiredWithoutContract_addressesTocontract_addressInput
  block?: blockUpdateOneRequiredWithoutContractInput
  addresses_addressesTocontract_delegate?: addressesUpdateOneWithoutContract_addressesTocontract_delegateInput
  addresses_addressesTocontract_mgr?: addressesUpdateOneWithoutContract_addressesTocontract_mgrInput
  implicit?: implicitUpdateOneWithoutContractInput
  addresses_addressesTocontract_preorig?: addressesUpdateOneWithoutContract_addressesTocontract_preorigInput
  contract?: contractUpdateOneWithoutOther_contractInput
  other_contract?: contractUpdateManyWithoutContractInput
  delegated_contract?: delegated_contractUpdateManyWithoutContractInput
  origination_contractToorigination_k?: originationUpdateManyWithoutContract_contractToorigination_kInput
  origination_contractToorigination_source?: originationUpdateManyWithoutContract_contractToorigination_sourceInput
  tx_contractTotx_destination?: txUpdateManyWithoutContract_contractTotx_destinationInput
  tx_contractTotx_source?: txUpdateManyWithoutContract_contractTotx_sourceInput
}

export type contractUpsertWithoutDelegationInput = {
  update: contractUpdateWithoutDelegationDataInput
  create: contractCreateWithoutDelegationInput
}

export type blockCreateWithoutEndorsementInput = {
  hash: string
  level: number
  proto: number
  predecessor: string
  timestamp: Date | string
  validation_passes: number
  merkle_root: string
  fitness: string
  context_hash: string
  sterile?: boolean | null
  block_alpha?: block_alphaCreateManyWithoutBlockInput
  contract?: contractCreateManyWithoutBlockInput
  deactivated?: deactivatedCreateManyWithoutBlockInput
  operation?: operationCreateManyWithoutBlockInput
}

export type operationCreateWithoutEndorsementInput = {
  hash: string
  autoid?: number
  block: blockCreateOneWithoutOperationInput
  chain_chainTooperation: chainCreateOneWithoutOperationInput
  implicit_implicit_activatedTooperation?: implicitCreateManyWithoutOperation_implicit_activatedTooperationInput
  implicit_implicit_revealedTooperation?: implicitCreateManyWithoutOperation_implicit_revealedTooperationInput
  manager_numbers?: manager_numbersCreateManyWithoutOperationInput
  operation_alpha?: operation_alphaCreateManyWithoutOperationInput
}

export type implicitCreateWithoutEndorsementInput = {
  pk?: string | null
  autoid?: number
  operation_implicit_activatedTooperation?: operationCreateOneWithoutImplicit_implicit_activatedTooperationInput
  addresses: addressesCreateOneWithoutImplicitInput
  operation_implicit_revealedTooperation?: operationCreateOneWithoutImplicit_implicit_revealedTooperationInput
  contract?: contractCreateManyWithoutImplicitInput
  deactivated?: deactivatedCreateManyWithoutImplicitInput
  delegated_contract?: delegated_contractCreateManyWithoutImplicitInput
}

export type blockUpdateWithoutEndorsementDataInput = {
  hash?: string | StringFieldUpdateOperationsInput
  level?: number | IntFieldUpdateOperationsInput
  proto?: number | IntFieldUpdateOperationsInput
  predecessor?: string | StringFieldUpdateOperationsInput
  timestamp?: Date | string | DateTimeFieldUpdateOperationsInput
  validation_passes?: number | IntFieldUpdateOperationsInput
  merkle_root?: string | StringFieldUpdateOperationsInput
  fitness?: string | StringFieldUpdateOperationsInput
  context_hash?: string | StringFieldUpdateOperationsInput
  sterile?: boolean | NullableBoolFieldUpdateOperationsInput | null
  block_alpha?: block_alphaUpdateManyWithoutBlockInput
  contract?: contractUpdateManyWithoutBlockInput
  deactivated?: deactivatedUpdateManyWithoutBlockInput
  operation?: operationUpdateManyWithoutBlockInput
}

export type blockUpsertWithoutEndorsementInput = {
  update: blockUpdateWithoutEndorsementDataInput
  create: blockCreateWithoutEndorsementInput
}

export type operationUpdateWithoutEndorsementDataInput = {
  hash?: string | StringFieldUpdateOperationsInput
  autoid?: number | IntFieldUpdateOperationsInput
  block?: blockUpdateOneRequiredWithoutOperationInput
  chain_chainTooperation?: chainUpdateOneRequiredWithoutOperationInput
  implicit_implicit_activatedTooperation?: implicitUpdateManyWithoutOperation_implicit_activatedTooperationInput
  implicit_implicit_revealedTooperation?: implicitUpdateManyWithoutOperation_implicit_revealedTooperationInput
  manager_numbers?: manager_numbersUpdateManyWithoutOperationInput
  operation_alpha?: operation_alphaUpdateManyWithoutOperationInput
}

export type operationUpsertWithoutEndorsementInput = {
  update: operationUpdateWithoutEndorsementDataInput
  create: operationCreateWithoutEndorsementInput
}

export type implicitUpdateWithoutEndorsementDataInput = {
  pk?: string | NullableStringFieldUpdateOperationsInput | null
  autoid?: number | IntFieldUpdateOperationsInput
  operation_implicit_activatedTooperation?: operationUpdateOneWithoutImplicit_implicit_activatedTooperationInput
  addresses?: addressesUpdateOneRequiredWithoutImplicitInput
  operation_implicit_revealedTooperation?: operationUpdateOneWithoutImplicit_implicit_revealedTooperationInput
  contract?: contractUpdateManyWithoutImplicitInput
  deactivated?: deactivatedUpdateManyWithoutImplicitInput
  delegated_contract?: delegated_contractUpdateManyWithoutImplicitInput
}

export type implicitUpsertWithoutEndorsementInput = {
  update: implicitUpdateWithoutEndorsementDataInput
  create: implicitCreateWithoutEndorsementInput
}

export type operationCreateWithoutImplicit_implicit_activatedTooperationInput = {
  hash: string
  autoid?: number
  block: blockCreateOneWithoutOperationInput
  chain_chainTooperation: chainCreateOneWithoutOperationInput
  endorsement?: endorsementCreateManyWithoutOperationInput
  implicit_implicit_revealedTooperation?: implicitCreateManyWithoutOperation_implicit_revealedTooperationInput
  manager_numbers?: manager_numbersCreateManyWithoutOperationInput
  operation_alpha?: operation_alphaCreateManyWithoutOperationInput
}

export type addressesCreateWithoutImplicitInput = {
  address: string
  contract_addressesTocontract_address?: contractCreateManyWithoutAddresses_addressesTocontract_addressInput
  contract_addressesTocontract_delegate?: contractCreateManyWithoutAddresses_addressesTocontract_delegateInput
  contract_addressesTocontract_mgr?: contractCreateManyWithoutAddresses_addressesTocontract_mgrInput
  contract_addressesTocontract_preorig?: contractCreateManyWithoutAddresses_addressesTocontract_preorigInput
  delegation?: delegationCreateManyWithoutAddressesInput
  operation_alpha_addressesTooperation_alpha_receiver?: operation_alphaCreateManyWithoutAddresses_addressesTooperation_alpha_receiverInput
  operation_alpha_addressesTooperation_alpha_sender?: operation_alphaCreateManyWithoutAddresses_addressesTooperation_alpha_senderInput
}

export type operationCreateWithoutImplicit_implicit_revealedTooperationInput = {
  hash: string
  autoid?: number
  block: blockCreateOneWithoutOperationInput
  chain_chainTooperation: chainCreateOneWithoutOperationInput
  endorsement?: endorsementCreateManyWithoutOperationInput
  implicit_implicit_activatedTooperation?: implicitCreateManyWithoutOperation_implicit_activatedTooperationInput
  manager_numbers?: manager_numbersCreateManyWithoutOperationInput
  operation_alpha?: operation_alphaCreateManyWithoutOperationInput
}

export type contractCreateWithoutImplicitInput = {
  spendable: boolean
  delegatable: boolean
  credit?: number | null
  script?: string | null
  addresses_addressesTocontract_address: addressesCreateOneWithoutContract_addressesTocontract_addressInput
  block: blockCreateOneWithoutContractInput
  addresses_addressesTocontract_delegate?: addressesCreateOneWithoutContract_addressesTocontract_delegateInput
  addresses_addressesTocontract_mgr?: addressesCreateOneWithoutContract_addressesTocontract_mgrInput
  addresses_addressesTocontract_preorig?: addressesCreateOneWithoutContract_addressesTocontract_preorigInput
  contract?: contractCreateOneWithoutOther_contractInput
  other_contract?: contractCreateManyWithoutContractInput
  delegated_contract?: delegated_contractCreateManyWithoutContractInput
  delegation?: delegationCreateManyWithoutContractInput
  origination_contractToorigination_k?: originationCreateManyWithoutContract_contractToorigination_kInput
  origination_contractToorigination_source?: originationCreateManyWithoutContract_contractToorigination_sourceInput
  tx_contractTotx_destination?: txCreateManyWithoutContract_contractTotx_destinationInput
  tx_contractTotx_source?: txCreateManyWithoutContract_contractTotx_sourceInput
}

export type deactivatedCreateWithoutImplicitInput = {
  block: blockCreateOneWithoutDeactivatedInput
}

export type delegated_contractCreateWithoutImplicitInput = {
  snapshot: snapshotCreateOneWithoutDelegated_contractInput
  contract: contractCreateOneWithoutDelegated_contractInput
}

export type endorsementCreateWithoutImplicitInput = {
  id: number
  level: number
  slot: number
  block: blockCreateOneWithoutEndorsementInput
  operation: operationCreateOneWithoutEndorsementInput
}

export type operationUpdateWithoutImplicit_implicit_activatedTooperationDataInput = {
  hash?: string | StringFieldUpdateOperationsInput
  autoid?: number | IntFieldUpdateOperationsInput
  block?: blockUpdateOneRequiredWithoutOperationInput
  chain_chainTooperation?: chainUpdateOneRequiredWithoutOperationInput
  endorsement?: endorsementUpdateManyWithoutOperationInput
  implicit_implicit_revealedTooperation?: implicitUpdateManyWithoutOperation_implicit_revealedTooperationInput
  manager_numbers?: manager_numbersUpdateManyWithoutOperationInput
  operation_alpha?: operation_alphaUpdateManyWithoutOperationInput
}

export type operationUpsertWithoutImplicit_implicit_activatedTooperationInput = {
  update: operationUpdateWithoutImplicit_implicit_activatedTooperationDataInput
  create: operationCreateWithoutImplicit_implicit_activatedTooperationInput
}

export type addressesUpdateWithoutImplicitDataInput = {
  address?: string | StringFieldUpdateOperationsInput
  contract_addressesTocontract_address?: contractUpdateManyWithoutAddresses_addressesTocontract_addressInput
  contract_addressesTocontract_delegate?: contractUpdateManyWithoutAddresses_addressesTocontract_delegateInput
  contract_addressesTocontract_mgr?: contractUpdateManyWithoutAddresses_addressesTocontract_mgrInput
  contract_addressesTocontract_preorig?: contractUpdateManyWithoutAddresses_addressesTocontract_preorigInput
  delegation?: delegationUpdateManyWithoutAddressesInput
  operation_alpha_addressesTooperation_alpha_receiver?: operation_alphaUpdateManyWithoutAddresses_addressesTooperation_alpha_receiverInput
  operation_alpha_addressesTooperation_alpha_sender?: operation_alphaUpdateManyWithoutAddresses_addressesTooperation_alpha_senderInput
}

export type addressesUpsertWithoutImplicitInput = {
  update: addressesUpdateWithoutImplicitDataInput
  create: addressesCreateWithoutImplicitInput
}

export type operationUpdateWithoutImplicit_implicit_revealedTooperationDataInput = {
  hash?: string | StringFieldUpdateOperationsInput
  autoid?: number | IntFieldUpdateOperationsInput
  block?: blockUpdateOneRequiredWithoutOperationInput
  chain_chainTooperation?: chainUpdateOneRequiredWithoutOperationInput
  endorsement?: endorsementUpdateManyWithoutOperationInput
  implicit_implicit_activatedTooperation?: implicitUpdateManyWithoutOperation_implicit_activatedTooperationInput
  manager_numbers?: manager_numbersUpdateManyWithoutOperationInput
  operation_alpha?: operation_alphaUpdateManyWithoutOperationInput
}

export type operationUpsertWithoutImplicit_implicit_revealedTooperationInput = {
  update: operationUpdateWithoutImplicit_implicit_revealedTooperationDataInput
  create: operationCreateWithoutImplicit_implicit_revealedTooperationInput
}

export type contractUpdateWithWhereUniqueWithoutImplicitInput = {
  where: contractWhereUniqueInput
  data: contractUpdateWithoutImplicitDataInput
}

export type contractUpsertWithWhereUniqueWithoutImplicitInput = {
  where: contractWhereUniqueInput
  update: contractUpdateWithoutImplicitDataInput
  create: contractCreateWithoutImplicitInput
}

export type deactivatedUpdateWithWhereUniqueWithoutImplicitInput = {
  where: deactivatedWhereUniqueInput
  data: deactivatedUpdateWithoutImplicitDataInput
}

export type deactivatedUpsertWithWhereUniqueWithoutImplicitInput = {
  where: deactivatedWhereUniqueInput
  update: deactivatedUpdateWithoutImplicitDataInput
  create: deactivatedCreateWithoutImplicitInput
}

export type delegated_contractUpdateWithWhereUniqueWithoutImplicitInput = {
  where: delegated_contractWhereUniqueInput
  data: delegated_contractUpdateWithoutImplicitDataInput
}

export type delegated_contractUpsertWithWhereUniqueWithoutImplicitInput = {
  where: delegated_contractWhereUniqueInput
  update: delegated_contractUpdateWithoutImplicitDataInput
  create: delegated_contractCreateWithoutImplicitInput
}

export type endorsementUpdateWithWhereUniqueWithoutImplicitInput = {
  where: endorsementWhereUniqueInput
  data: endorsementUpdateWithoutImplicitDataInput
}

export type endorsementUpsertWithWhereUniqueWithoutImplicitInput = {
  where: endorsementWhereUniqueInput
  update: endorsementUpdateWithoutImplicitDataInput
  create: endorsementCreateWithoutImplicitInput
}

export type blockCreateWithoutOperationInput = {
  hash: string
  level: number
  proto: number
  predecessor: string
  timestamp: Date | string
  validation_passes: number
  merkle_root: string
  fitness: string
  context_hash: string
  sterile?: boolean | null
  block_alpha?: block_alphaCreateManyWithoutBlockInput
  contract?: contractCreateManyWithoutBlockInput
  deactivated?: deactivatedCreateManyWithoutBlockInput
  endorsement?: endorsementCreateManyWithoutBlockInput
}

export type chainCreateWithoutOperationInput = {
  hash: string
}

export type endorsementCreateWithoutOperationInput = {
  id: number
  level: number
  slot: number
  block: blockCreateOneWithoutEndorsementInput
  implicit: implicitCreateOneWithoutEndorsementInput
}

export type implicitCreateWithoutOperation_implicit_activatedTooperationInput = {
  pk?: string | null
  autoid?: number
  addresses: addressesCreateOneWithoutImplicitInput
  operation_implicit_revealedTooperation?: operationCreateOneWithoutImplicit_implicit_revealedTooperationInput
  contract?: contractCreateManyWithoutImplicitInput
  deactivated?: deactivatedCreateManyWithoutImplicitInput
  delegated_contract?: delegated_contractCreateManyWithoutImplicitInput
  endorsement?: endorsementCreateManyWithoutImplicitInput
}

export type implicitCreateWithoutOperation_implicit_revealedTooperationInput = {
  pk?: string | null
  autoid?: number
  operation_implicit_activatedTooperation?: operationCreateOneWithoutImplicit_implicit_activatedTooperationInput
  addresses: addressesCreateOneWithoutImplicitInput
  contract?: contractCreateManyWithoutImplicitInput
  deactivated?: deactivatedCreateManyWithoutImplicitInput
  delegated_contract?: delegated_contractCreateManyWithoutImplicitInput
  endorsement?: endorsementCreateManyWithoutImplicitInput
}

export type manager_numbersCreateWithoutOperationInput = {
  counter?: string | null
  gas_limit?: string | null
  storage_limit?: string | null
}

export type operation_alphaCreateWithoutOperationInput = {
  id: number
  operation_kind: number
  autoid?: number
  addresses_addressesTooperation_alpha_receiver?: addressesCreateOneWithoutOperation_alpha_addressesTooperation_alpha_receiverInput
  addresses_addressesTooperation_alpha_sender?: addressesCreateOneWithoutOperation_alpha_addressesTooperation_alpha_senderInput
  delegation?: delegationCreateManyWithoutOperation_alphaInput
  origination?: originationCreateManyWithoutOperation_alphaInput
  tx?: txCreateManyWithoutOperation_alphaInput
}

export type blockUpdateWithoutOperationDataInput = {
  hash?: string | StringFieldUpdateOperationsInput
  level?: number | IntFieldUpdateOperationsInput
  proto?: number | IntFieldUpdateOperationsInput
  predecessor?: string | StringFieldUpdateOperationsInput
  timestamp?: Date | string | DateTimeFieldUpdateOperationsInput
  validation_passes?: number | IntFieldUpdateOperationsInput
  merkle_root?: string | StringFieldUpdateOperationsInput
  fitness?: string | StringFieldUpdateOperationsInput
  context_hash?: string | StringFieldUpdateOperationsInput
  sterile?: boolean | NullableBoolFieldUpdateOperationsInput | null
  block_alpha?: block_alphaUpdateManyWithoutBlockInput
  contract?: contractUpdateManyWithoutBlockInput
  deactivated?: deactivatedUpdateManyWithoutBlockInput
  endorsement?: endorsementUpdateManyWithoutBlockInput
}

export type blockUpsertWithoutOperationInput = {
  update: blockUpdateWithoutOperationDataInput
  create: blockCreateWithoutOperationInput
}

export type chainUpdateWithoutOperationDataInput = {
  hash?: string | StringFieldUpdateOperationsInput
}

export type chainUpsertWithoutOperationInput = {
  update: chainUpdateWithoutOperationDataInput
  create: chainCreateWithoutOperationInput
}

export type endorsementUpdateWithWhereUniqueWithoutOperationInput = {
  where: endorsementWhereUniqueInput
  data: endorsementUpdateWithoutOperationDataInput
}

export type endorsementUpsertWithWhereUniqueWithoutOperationInput = {
  where: endorsementWhereUniqueInput
  update: endorsementUpdateWithoutOperationDataInput
  create: endorsementCreateWithoutOperationInput
}

export type implicitUpdateWithWhereUniqueWithoutOperation_implicit_activatedTooperationInput = {
  where: implicitWhereUniqueInput
  data: implicitUpdateWithoutOperation_implicit_activatedTooperationDataInput
}

export type implicitUpdateManyWithWhereNestedInput = {
  where: implicitScalarWhereInput
  data: implicitUpdateManyDataInput
}

export type implicitScalarWhereInput = {
  AND?: Enumerable<implicitScalarWhereInput>
  OR?: Array<implicitScalarWhereInput>
  NOT?: Enumerable<implicitScalarWhereInput>
  pkh?: string | StringFilter
  activated?: string | StringNullableFilter | null
  revealed?: string | StringNullableFilter | null
  pk?: string | StringNullableFilter | null
  autoid?: number | IntFilter
}

export type implicitUpsertWithWhereUniqueWithoutOperation_implicit_activatedTooperationInput = {
  where: implicitWhereUniqueInput
  update: implicitUpdateWithoutOperation_implicit_activatedTooperationDataInput
  create: implicitCreateWithoutOperation_implicit_activatedTooperationInput
}

export type implicitUpdateWithWhereUniqueWithoutOperation_implicit_revealedTooperationInput = {
  where: implicitWhereUniqueInput
  data: implicitUpdateWithoutOperation_implicit_revealedTooperationDataInput
}

export type implicitUpsertWithWhereUniqueWithoutOperation_implicit_revealedTooperationInput = {
  where: implicitWhereUniqueInput
  update: implicitUpdateWithoutOperation_implicit_revealedTooperationDataInput
  create: implicitCreateWithoutOperation_implicit_revealedTooperationInput
}

export type manager_numbersUpdateWithWhereUniqueWithoutOperationInput = {
  where: manager_numbersWhereUniqueInput
  data: manager_numbersUpdateWithoutOperationDataInput
}

export type manager_numbersUpdateManyWithWhereNestedInput = {
  where: manager_numbersScalarWhereInput
  data: manager_numbersUpdateManyDataInput
}

export type manager_numbersScalarWhereInput = {
  AND?: Enumerable<manager_numbersScalarWhereInput>
  OR?: Array<manager_numbersScalarWhereInput>
  NOT?: Enumerable<manager_numbersScalarWhereInput>
  hash?: string | StringFilter
  counter?: string | StringNullableFilter | null
  gas_limit?: string | StringNullableFilter | null
  storage_limit?: string | StringNullableFilter | null
}

export type manager_numbersUpsertWithWhereUniqueWithoutOperationInput = {
  where: manager_numbersWhereUniqueInput
  update: manager_numbersUpdateWithoutOperationDataInput
  create: manager_numbersCreateWithoutOperationInput
}

export type operation_alphaUpdateWithWhereUniqueWithoutOperationInput = {
  where: operation_alphaWhereUniqueInput
  data: operation_alphaUpdateWithoutOperationDataInput
}

export type operation_alphaUpdateManyWithWhereNestedInput = {
  where: operation_alphaScalarWhereInput
  data: operation_alphaUpdateManyDataInput
}

export type operation_alphaScalarWhereInput = {
  AND?: Enumerable<operation_alphaScalarWhereInput>
  OR?: Array<operation_alphaScalarWhereInput>
  NOT?: Enumerable<operation_alphaScalarWhereInput>
  hash?: string | StringFilter
  id?: number | IntFilter
  operation_kind?: number | IntFilter
  sender?: string | StringNullableFilter | null
  receiver?: string | StringNullableFilter | null
  autoid?: number | IntFilter
}

export type operation_alphaUpsertWithWhereUniqueWithoutOperationInput = {
  where: operation_alphaWhereUniqueInput
  update: operation_alphaUpdateWithoutOperationDataInput
  create: operation_alphaCreateWithoutOperationInput
}

export type operationCreateWithoutOperation_alphaInput = {
  hash: string
  autoid?: number
  block: blockCreateOneWithoutOperationInput
  chain_chainTooperation: chainCreateOneWithoutOperationInput
  endorsement?: endorsementCreateManyWithoutOperationInput
  implicit_implicit_activatedTooperation?: implicitCreateManyWithoutOperation_implicit_activatedTooperationInput
  implicit_implicit_revealedTooperation?: implicitCreateManyWithoutOperation_implicit_revealedTooperationInput
  manager_numbers?: manager_numbersCreateManyWithoutOperationInput
}

export type addressesCreateWithoutOperation_alpha_addressesTooperation_alpha_receiverInput = {
  address: string
  contract_addressesTocontract_address?: contractCreateManyWithoutAddresses_addressesTocontract_addressInput
  contract_addressesTocontract_delegate?: contractCreateManyWithoutAddresses_addressesTocontract_delegateInput
  contract_addressesTocontract_mgr?: contractCreateManyWithoutAddresses_addressesTocontract_mgrInput
  contract_addressesTocontract_preorig?: contractCreateManyWithoutAddresses_addressesTocontract_preorigInput
  delegation?: delegationCreateManyWithoutAddressesInput
  implicit?: implicitCreateManyWithoutAddressesInput
  operation_alpha_addressesTooperation_alpha_sender?: operation_alphaCreateManyWithoutAddresses_addressesTooperation_alpha_senderInput
}

export type addressesCreateWithoutOperation_alpha_addressesTooperation_alpha_senderInput = {
  address: string
  contract_addressesTocontract_address?: contractCreateManyWithoutAddresses_addressesTocontract_addressInput
  contract_addressesTocontract_delegate?: contractCreateManyWithoutAddresses_addressesTocontract_delegateInput
  contract_addressesTocontract_mgr?: contractCreateManyWithoutAddresses_addressesTocontract_mgrInput
  contract_addressesTocontract_preorig?: contractCreateManyWithoutAddresses_addressesTocontract_preorigInput
  delegation?: delegationCreateManyWithoutAddressesInput
  implicit?: implicitCreateManyWithoutAddressesInput
  operation_alpha_addressesTooperation_alpha_receiver?: operation_alphaCreateManyWithoutAddresses_addressesTooperation_alpha_receiverInput
}

export type delegationCreateWithoutOperation_alphaInput = {
  autoid?: number
  addresses?: addressesCreateOneWithoutDelegationInput
  contract: contractCreateOneWithoutDelegationInput
}

export type originationCreateWithoutOperation_alphaInput = {
  autoid?: number
  contract_contractToorigination_k: contractCreateOneWithoutOrigination_contractToorigination_kInput
  contract_contractToorigination_source: contractCreateOneWithoutOrigination_contractToorigination_sourceInput
}

export type txCreateWithoutOperation_alphaInput = {
  fee: number
  amount: number
  parameters?: string | null
  storage?: string | null
  consumed_gas: string
  storage_size: string
  paid_storage_size_diff: string
  entrypoint?: string | null
  bigmapdiff?: string | null
  autoid?: number
  contract_contractTotx_destination: contractCreateOneWithoutTx_contractTotx_destinationInput
  contract_contractTotx_source: contractCreateOneWithoutTx_contractTotx_sourceInput
}

export type operationUpdateWithoutOperation_alphaDataInput = {
  hash?: string | StringFieldUpdateOperationsInput
  autoid?: number | IntFieldUpdateOperationsInput
  block?: blockUpdateOneRequiredWithoutOperationInput
  chain_chainTooperation?: chainUpdateOneRequiredWithoutOperationInput
  endorsement?: endorsementUpdateManyWithoutOperationInput
  implicit_implicit_activatedTooperation?: implicitUpdateManyWithoutOperation_implicit_activatedTooperationInput
  implicit_implicit_revealedTooperation?: implicitUpdateManyWithoutOperation_implicit_revealedTooperationInput
  manager_numbers?: manager_numbersUpdateManyWithoutOperationInput
}

export type operationUpsertWithoutOperation_alphaInput = {
  update: operationUpdateWithoutOperation_alphaDataInput
  create: operationCreateWithoutOperation_alphaInput
}

export type addressesUpdateWithoutOperation_alpha_addressesTooperation_alpha_receiverDataInput = {
  address?: string | StringFieldUpdateOperationsInput
  contract_addressesTocontract_address?: contractUpdateManyWithoutAddresses_addressesTocontract_addressInput
  contract_addressesTocontract_delegate?: contractUpdateManyWithoutAddresses_addressesTocontract_delegateInput
  contract_addressesTocontract_mgr?: contractUpdateManyWithoutAddresses_addressesTocontract_mgrInput
  contract_addressesTocontract_preorig?: contractUpdateManyWithoutAddresses_addressesTocontract_preorigInput
  delegation?: delegationUpdateManyWithoutAddressesInput
  implicit?: implicitUpdateManyWithoutAddressesInput
  operation_alpha_addressesTooperation_alpha_sender?: operation_alphaUpdateManyWithoutAddresses_addressesTooperation_alpha_senderInput
}

export type addressesUpsertWithoutOperation_alpha_addressesTooperation_alpha_receiverInput = {
  update: addressesUpdateWithoutOperation_alpha_addressesTooperation_alpha_receiverDataInput
  create: addressesCreateWithoutOperation_alpha_addressesTooperation_alpha_receiverInput
}

export type addressesUpdateWithoutOperation_alpha_addressesTooperation_alpha_senderDataInput = {
  address?: string | StringFieldUpdateOperationsInput
  contract_addressesTocontract_address?: contractUpdateManyWithoutAddresses_addressesTocontract_addressInput
  contract_addressesTocontract_delegate?: contractUpdateManyWithoutAddresses_addressesTocontract_delegateInput
  contract_addressesTocontract_mgr?: contractUpdateManyWithoutAddresses_addressesTocontract_mgrInput
  contract_addressesTocontract_preorig?: contractUpdateManyWithoutAddresses_addressesTocontract_preorigInput
  delegation?: delegationUpdateManyWithoutAddressesInput
  implicit?: implicitUpdateManyWithoutAddressesInput
  operation_alpha_addressesTooperation_alpha_receiver?: operation_alphaUpdateManyWithoutAddresses_addressesTooperation_alpha_receiverInput
}

export type addressesUpsertWithoutOperation_alpha_addressesTooperation_alpha_senderInput = {
  update: addressesUpdateWithoutOperation_alpha_addressesTooperation_alpha_senderDataInput
  create: addressesCreateWithoutOperation_alpha_addressesTooperation_alpha_senderInput
}

export type delegationUpdateWithWhereUniqueWithoutOperation_alphaInput = {
  where: delegationWhereUniqueInput
  data: delegationUpdateWithoutOperation_alphaDataInput
}

export type delegationUpsertWithWhereUniqueWithoutOperation_alphaInput = {
  where: delegationWhereUniqueInput
  update: delegationUpdateWithoutOperation_alphaDataInput
  create: delegationCreateWithoutOperation_alphaInput
}

export type originationUpdateWithWhereUniqueWithoutOperation_alphaInput = {
  where: originationWhereUniqueInput
  data: originationUpdateWithoutOperation_alphaDataInput
}

export type originationUpsertWithWhereUniqueWithoutOperation_alphaInput = {
  where: originationWhereUniqueInput
  update: originationUpdateWithoutOperation_alphaDataInput
  create: originationCreateWithoutOperation_alphaInput
}

export type txUpdateWithWhereUniqueWithoutOperation_alphaInput = {
  where: txWhereUniqueInput
  data: txUpdateWithoutOperation_alphaDataInput
}

export type txUpsertWithWhereUniqueWithoutOperation_alphaInput = {
  where: txWhereUniqueInput
  update: txUpdateWithoutOperation_alphaDataInput
  create: txCreateWithoutOperation_alphaInput
}

export type contractCreateWithoutOrigination_contractToorigination_kInput = {
  spendable: boolean
  delegatable: boolean
  credit?: number | null
  script?: string | null
  addresses_addressesTocontract_address: addressesCreateOneWithoutContract_addressesTocontract_addressInput
  block: blockCreateOneWithoutContractInput
  addresses_addressesTocontract_delegate?: addressesCreateOneWithoutContract_addressesTocontract_delegateInput
  addresses_addressesTocontract_mgr?: addressesCreateOneWithoutContract_addressesTocontract_mgrInput
  implicit?: implicitCreateOneWithoutContractInput
  addresses_addressesTocontract_preorig?: addressesCreateOneWithoutContract_addressesTocontract_preorigInput
  contract?: contractCreateOneWithoutOther_contractInput
  other_contract?: contractCreateManyWithoutContractInput
  delegated_contract?: delegated_contractCreateManyWithoutContractInput
  delegation?: delegationCreateManyWithoutContractInput
  origination_contractToorigination_source?: originationCreateManyWithoutContract_contractToorigination_sourceInput
  tx_contractTotx_destination?: txCreateManyWithoutContract_contractTotx_destinationInput
  tx_contractTotx_source?: txCreateManyWithoutContract_contractTotx_sourceInput
}

export type operation_alphaCreateWithoutOriginationInput = {
  id: number
  operation_kind: number
  autoid?: number
  operation: operationCreateOneWithoutOperation_alphaInput
  addresses_addressesTooperation_alpha_receiver?: addressesCreateOneWithoutOperation_alpha_addressesTooperation_alpha_receiverInput
  addresses_addressesTooperation_alpha_sender?: addressesCreateOneWithoutOperation_alpha_addressesTooperation_alpha_senderInput
  delegation?: delegationCreateManyWithoutOperation_alphaInput
  tx?: txCreateManyWithoutOperation_alphaInput
}

export type contractCreateWithoutOrigination_contractToorigination_sourceInput = {
  spendable: boolean
  delegatable: boolean
  credit?: number | null
  script?: string | null
  addresses_addressesTocontract_address: addressesCreateOneWithoutContract_addressesTocontract_addressInput
  block: blockCreateOneWithoutContractInput
  addresses_addressesTocontract_delegate?: addressesCreateOneWithoutContract_addressesTocontract_delegateInput
  addresses_addressesTocontract_mgr?: addressesCreateOneWithoutContract_addressesTocontract_mgrInput
  implicit?: implicitCreateOneWithoutContractInput
  addresses_addressesTocontract_preorig?: addressesCreateOneWithoutContract_addressesTocontract_preorigInput
  contract?: contractCreateOneWithoutOther_contractInput
  other_contract?: contractCreateManyWithoutContractInput
  delegated_contract?: delegated_contractCreateManyWithoutContractInput
  delegation?: delegationCreateManyWithoutContractInput
  origination_contractToorigination_k?: originationCreateManyWithoutContract_contractToorigination_kInput
  tx_contractTotx_destination?: txCreateManyWithoutContract_contractTotx_destinationInput
  tx_contractTotx_source?: txCreateManyWithoutContract_contractTotx_sourceInput
}

export type contractUpdateWithoutOrigination_contractToorigination_kDataInput = {
  spendable?: boolean | BoolFieldUpdateOperationsInput
  delegatable?: boolean | BoolFieldUpdateOperationsInput
  credit?: number | NullableIntFieldUpdateOperationsInput | null
  script?: string | NullableStringFieldUpdateOperationsInput | null
  addresses_addressesTocontract_address?: addressesUpdateOneRequiredWithoutContract_addressesTocontract_addressInput
  block?: blockUpdateOneRequiredWithoutContractInput
  addresses_addressesTocontract_delegate?: addressesUpdateOneWithoutContract_addressesTocontract_delegateInput
  addresses_addressesTocontract_mgr?: addressesUpdateOneWithoutContract_addressesTocontract_mgrInput
  implicit?: implicitUpdateOneWithoutContractInput
  addresses_addressesTocontract_preorig?: addressesUpdateOneWithoutContract_addressesTocontract_preorigInput
  contract?: contractUpdateOneWithoutOther_contractInput
  other_contract?: contractUpdateManyWithoutContractInput
  delegated_contract?: delegated_contractUpdateManyWithoutContractInput
  delegation?: delegationUpdateManyWithoutContractInput
  origination_contractToorigination_source?: originationUpdateManyWithoutContract_contractToorigination_sourceInput
  tx_contractTotx_destination?: txUpdateManyWithoutContract_contractTotx_destinationInput
  tx_contractTotx_source?: txUpdateManyWithoutContract_contractTotx_sourceInput
}

export type contractUpsertWithoutOrigination_contractToorigination_kInput = {
  update: contractUpdateWithoutOrigination_contractToorigination_kDataInput
  create: contractCreateWithoutOrigination_contractToorigination_kInput
}

export type operation_alphaUpdateWithoutOriginationDataInput = {
  id?: number | IntFieldUpdateOperationsInput
  operation_kind?: number | IntFieldUpdateOperationsInput
  autoid?: number | IntFieldUpdateOperationsInput
  operation?: operationUpdateOneRequiredWithoutOperation_alphaInput
  addresses_addressesTooperation_alpha_receiver?: addressesUpdateOneWithoutOperation_alpha_addressesTooperation_alpha_receiverInput
  addresses_addressesTooperation_alpha_sender?: addressesUpdateOneWithoutOperation_alpha_addressesTooperation_alpha_senderInput
  delegation?: delegationUpdateManyWithoutOperation_alphaInput
  tx?: txUpdateManyWithoutOperation_alphaInput
}

export type operation_alphaUpsertWithoutOriginationInput = {
  update: operation_alphaUpdateWithoutOriginationDataInput
  create: operation_alphaCreateWithoutOriginationInput
}

export type contractUpdateWithoutOrigination_contractToorigination_sourceDataInput = {
  spendable?: boolean | BoolFieldUpdateOperationsInput
  delegatable?: boolean | BoolFieldUpdateOperationsInput
  credit?: number | NullableIntFieldUpdateOperationsInput | null
  script?: string | NullableStringFieldUpdateOperationsInput | null
  addresses_addressesTocontract_address?: addressesUpdateOneRequiredWithoutContract_addressesTocontract_addressInput
  block?: blockUpdateOneRequiredWithoutContractInput
  addresses_addressesTocontract_delegate?: addressesUpdateOneWithoutContract_addressesTocontract_delegateInput
  addresses_addressesTocontract_mgr?: addressesUpdateOneWithoutContract_addressesTocontract_mgrInput
  implicit?: implicitUpdateOneWithoutContractInput
  addresses_addressesTocontract_preorig?: addressesUpdateOneWithoutContract_addressesTocontract_preorigInput
  contract?: contractUpdateOneWithoutOther_contractInput
  other_contract?: contractUpdateManyWithoutContractInput
  delegated_contract?: delegated_contractUpdateManyWithoutContractInput
  delegation?: delegationUpdateManyWithoutContractInput
  origination_contractToorigination_k?: originationUpdateManyWithoutContract_contractToorigination_kInput
  tx_contractTotx_destination?: txUpdateManyWithoutContract_contractTotx_destinationInput
  tx_contractTotx_source?: txUpdateManyWithoutContract_contractTotx_sourceInput
}

export type contractUpsertWithoutOrigination_contractToorigination_sourceInput = {
  update: contractUpdateWithoutOrigination_contractToorigination_sourceDataInput
  create: contractCreateWithoutOrigination_contractToorigination_sourceInput
}

export type delegated_contractCreateWithoutSnapshotInput = {
  implicit: implicitCreateOneWithoutDelegated_contractInput
  contract: contractCreateOneWithoutDelegated_contractInput
}

export type delegated_contractUpdateWithWhereUniqueWithoutSnapshotInput = {
  where: delegated_contractWhereUniqueInput
  data: delegated_contractUpdateWithoutSnapshotDataInput
}

export type delegated_contractUpsertWithWhereUniqueWithoutSnapshotInput = {
  where: delegated_contractWhereUniqueInput
  update: delegated_contractUpdateWithoutSnapshotDataInput
  create: delegated_contractCreateWithoutSnapshotInput
}

export type contractCreateWithoutTx_contractTotx_destinationInput = {
  spendable: boolean
  delegatable: boolean
  credit?: number | null
  script?: string | null
  addresses_addressesTocontract_address: addressesCreateOneWithoutContract_addressesTocontract_addressInput
  block: blockCreateOneWithoutContractInput
  addresses_addressesTocontract_delegate?: addressesCreateOneWithoutContract_addressesTocontract_delegateInput
  addresses_addressesTocontract_mgr?: addressesCreateOneWithoutContract_addressesTocontract_mgrInput
  implicit?: implicitCreateOneWithoutContractInput
  addresses_addressesTocontract_preorig?: addressesCreateOneWithoutContract_addressesTocontract_preorigInput
  contract?: contractCreateOneWithoutOther_contractInput
  other_contract?: contractCreateManyWithoutContractInput
  delegated_contract?: delegated_contractCreateManyWithoutContractInput
  delegation?: delegationCreateManyWithoutContractInput
  origination_contractToorigination_k?: originationCreateManyWithoutContract_contractToorigination_kInput
  origination_contractToorigination_source?: originationCreateManyWithoutContract_contractToorigination_sourceInput
  tx_contractTotx_source?: txCreateManyWithoutContract_contractTotx_sourceInput
}

export type operation_alphaCreateWithoutTxInput = {
  id: number
  operation_kind: number
  autoid?: number
  operation: operationCreateOneWithoutOperation_alphaInput
  addresses_addressesTooperation_alpha_receiver?: addressesCreateOneWithoutOperation_alpha_addressesTooperation_alpha_receiverInput
  addresses_addressesTooperation_alpha_sender?: addressesCreateOneWithoutOperation_alpha_addressesTooperation_alpha_senderInput
  delegation?: delegationCreateManyWithoutOperation_alphaInput
  origination?: originationCreateManyWithoutOperation_alphaInput
}

export type contractCreateWithoutTx_contractTotx_sourceInput = {
  spendable: boolean
  delegatable: boolean
  credit?: number | null
  script?: string | null
  addresses_addressesTocontract_address: addressesCreateOneWithoutContract_addressesTocontract_addressInput
  block: blockCreateOneWithoutContractInput
  addresses_addressesTocontract_delegate?: addressesCreateOneWithoutContract_addressesTocontract_delegateInput
  addresses_addressesTocontract_mgr?: addressesCreateOneWithoutContract_addressesTocontract_mgrInput
  implicit?: implicitCreateOneWithoutContractInput
  addresses_addressesTocontract_preorig?: addressesCreateOneWithoutContract_addressesTocontract_preorigInput
  contract?: contractCreateOneWithoutOther_contractInput
  other_contract?: contractCreateManyWithoutContractInput
  delegated_contract?: delegated_contractCreateManyWithoutContractInput
  delegation?: delegationCreateManyWithoutContractInput
  origination_contractToorigination_k?: originationCreateManyWithoutContract_contractToorigination_kInput
  origination_contractToorigination_source?: originationCreateManyWithoutContract_contractToorigination_sourceInput
  tx_contractTotx_destination?: txCreateManyWithoutContract_contractTotx_destinationInput
}

export type contractUpdateWithoutTx_contractTotx_destinationDataInput = {
  spendable?: boolean | BoolFieldUpdateOperationsInput
  delegatable?: boolean | BoolFieldUpdateOperationsInput
  credit?: number | NullableIntFieldUpdateOperationsInput | null
  script?: string | NullableStringFieldUpdateOperationsInput | null
  addresses_addressesTocontract_address?: addressesUpdateOneRequiredWithoutContract_addressesTocontract_addressInput
  block?: blockUpdateOneRequiredWithoutContractInput
  addresses_addressesTocontract_delegate?: addressesUpdateOneWithoutContract_addressesTocontract_delegateInput
  addresses_addressesTocontract_mgr?: addressesUpdateOneWithoutContract_addressesTocontract_mgrInput
  implicit?: implicitUpdateOneWithoutContractInput
  addresses_addressesTocontract_preorig?: addressesUpdateOneWithoutContract_addressesTocontract_preorigInput
  contract?: contractUpdateOneWithoutOther_contractInput
  other_contract?: contractUpdateManyWithoutContractInput
  delegated_contract?: delegated_contractUpdateManyWithoutContractInput
  delegation?: delegationUpdateManyWithoutContractInput
  origination_contractToorigination_k?: originationUpdateManyWithoutContract_contractToorigination_kInput
  origination_contractToorigination_source?: originationUpdateManyWithoutContract_contractToorigination_sourceInput
  tx_contractTotx_source?: txUpdateManyWithoutContract_contractTotx_sourceInput
}

export type contractUpsertWithoutTx_contractTotx_destinationInput = {
  update: contractUpdateWithoutTx_contractTotx_destinationDataInput
  create: contractCreateWithoutTx_contractTotx_destinationInput
}

export type operation_alphaUpdateWithoutTxDataInput = {
  id?: number | IntFieldUpdateOperationsInput
  operation_kind?: number | IntFieldUpdateOperationsInput
  autoid?: number | IntFieldUpdateOperationsInput
  operation?: operationUpdateOneRequiredWithoutOperation_alphaInput
  addresses_addressesTooperation_alpha_receiver?: addressesUpdateOneWithoutOperation_alpha_addressesTooperation_alpha_receiverInput
  addresses_addressesTooperation_alpha_sender?: addressesUpdateOneWithoutOperation_alpha_addressesTooperation_alpha_senderInput
  delegation?: delegationUpdateManyWithoutOperation_alphaInput
  origination?: originationUpdateManyWithoutOperation_alphaInput
}

export type operation_alphaUpsertWithoutTxInput = {
  update: operation_alphaUpdateWithoutTxDataInput
  create: operation_alphaCreateWithoutTxInput
}

export type contractUpdateWithoutTx_contractTotx_sourceDataInput = {
  spendable?: boolean | BoolFieldUpdateOperationsInput
  delegatable?: boolean | BoolFieldUpdateOperationsInput
  credit?: number | NullableIntFieldUpdateOperationsInput | null
  script?: string | NullableStringFieldUpdateOperationsInput | null
  addresses_addressesTocontract_address?: addressesUpdateOneRequiredWithoutContract_addressesTocontract_addressInput
  block?: blockUpdateOneRequiredWithoutContractInput
  addresses_addressesTocontract_delegate?: addressesUpdateOneWithoutContract_addressesTocontract_delegateInput
  addresses_addressesTocontract_mgr?: addressesUpdateOneWithoutContract_addressesTocontract_mgrInput
  implicit?: implicitUpdateOneWithoutContractInput
  addresses_addressesTocontract_preorig?: addressesUpdateOneWithoutContract_addressesTocontract_preorigInput
  contract?: contractUpdateOneWithoutOther_contractInput
  other_contract?: contractUpdateManyWithoutContractInput
  delegated_contract?: delegated_contractUpdateManyWithoutContractInput
  delegation?: delegationUpdateManyWithoutContractInput
  origination_contractToorigination_k?: originationUpdateManyWithoutContract_contractToorigination_kInput
  origination_contractToorigination_source?: originationUpdateManyWithoutContract_contractToorigination_sourceInput
  tx_contractTotx_destination?: txUpdateManyWithoutContract_contractTotx_destinationInput
}

export type contractUpsertWithoutTx_contractTotx_sourceInput = {
  update: contractUpdateWithoutTx_contractTotx_sourceDataInput
  create: contractCreateWithoutTx_contractTotx_sourceInput
}

export type contractCreateWithoutAddresses_addressesTocontract_addressInput = {
  spendable: boolean
  delegatable: boolean
  credit?: number | null
  script?: string | null
  block: blockCreateOneWithoutContractInput
  addresses_addressesTocontract_delegate?: addressesCreateOneWithoutContract_addressesTocontract_delegateInput
  addresses_addressesTocontract_mgr?: addressesCreateOneWithoutContract_addressesTocontract_mgrInput
  implicit?: implicitCreateOneWithoutContractInput
  addresses_addressesTocontract_preorig?: addressesCreateOneWithoutContract_addressesTocontract_preorigInput
  contract?: contractCreateOneWithoutOther_contractInput
  other_contract?: contractCreateManyWithoutContractInput
  delegated_contract?: delegated_contractCreateManyWithoutContractInput
  delegation?: delegationCreateManyWithoutContractInput
  origination_contractToorigination_k?: originationCreateManyWithoutContract_contractToorigination_kInput
  origination_contractToorigination_source?: originationCreateManyWithoutContract_contractToorigination_sourceInput
  tx_contractTotx_destination?: txCreateManyWithoutContract_contractTotx_destinationInput
  tx_contractTotx_source?: txCreateManyWithoutContract_contractTotx_sourceInput
}

export type contractCreateWithoutAddresses_addressesTocontract_delegateInput = {
  spendable: boolean
  delegatable: boolean
  credit?: number | null
  script?: string | null
  addresses_addressesTocontract_address: addressesCreateOneWithoutContract_addressesTocontract_addressInput
  block: blockCreateOneWithoutContractInput
  addresses_addressesTocontract_mgr?: addressesCreateOneWithoutContract_addressesTocontract_mgrInput
  implicit?: implicitCreateOneWithoutContractInput
  addresses_addressesTocontract_preorig?: addressesCreateOneWithoutContract_addressesTocontract_preorigInput
  contract?: contractCreateOneWithoutOther_contractInput
  other_contract?: contractCreateManyWithoutContractInput
  delegated_contract?: delegated_contractCreateManyWithoutContractInput
  delegation?: delegationCreateManyWithoutContractInput
  origination_contractToorigination_k?: originationCreateManyWithoutContract_contractToorigination_kInput
  origination_contractToorigination_source?: originationCreateManyWithoutContract_contractToorigination_sourceInput
  tx_contractTotx_destination?: txCreateManyWithoutContract_contractTotx_destinationInput
  tx_contractTotx_source?: txCreateManyWithoutContract_contractTotx_sourceInput
}

export type contractCreateWithoutAddresses_addressesTocontract_mgrInput = {
  spendable: boolean
  delegatable: boolean
  credit?: number | null
  script?: string | null
  addresses_addressesTocontract_address: addressesCreateOneWithoutContract_addressesTocontract_addressInput
  block: blockCreateOneWithoutContractInput
  addresses_addressesTocontract_delegate?: addressesCreateOneWithoutContract_addressesTocontract_delegateInput
  implicit?: implicitCreateOneWithoutContractInput
  addresses_addressesTocontract_preorig?: addressesCreateOneWithoutContract_addressesTocontract_preorigInput
  contract?: contractCreateOneWithoutOther_contractInput
  other_contract?: contractCreateManyWithoutContractInput
  delegated_contract?: delegated_contractCreateManyWithoutContractInput
  delegation?: delegationCreateManyWithoutContractInput
  origination_contractToorigination_k?: originationCreateManyWithoutContract_contractToorigination_kInput
  origination_contractToorigination_source?: originationCreateManyWithoutContract_contractToorigination_sourceInput
  tx_contractTotx_destination?: txCreateManyWithoutContract_contractTotx_destinationInput
  tx_contractTotx_source?: txCreateManyWithoutContract_contractTotx_sourceInput
}

export type contractCreateWithoutAddresses_addressesTocontract_preorigInput = {
  spendable: boolean
  delegatable: boolean
  credit?: number | null
  script?: string | null
  addresses_addressesTocontract_address: addressesCreateOneWithoutContract_addressesTocontract_addressInput
  block: blockCreateOneWithoutContractInput
  addresses_addressesTocontract_delegate?: addressesCreateOneWithoutContract_addressesTocontract_delegateInput
  addresses_addressesTocontract_mgr?: addressesCreateOneWithoutContract_addressesTocontract_mgrInput
  implicit?: implicitCreateOneWithoutContractInput
  contract?: contractCreateOneWithoutOther_contractInput
  other_contract?: contractCreateManyWithoutContractInput
  delegated_contract?: delegated_contractCreateManyWithoutContractInput
  delegation?: delegationCreateManyWithoutContractInput
  origination_contractToorigination_k?: originationCreateManyWithoutContract_contractToorigination_kInput
  origination_contractToorigination_source?: originationCreateManyWithoutContract_contractToorigination_sourceInput
  tx_contractTotx_destination?: txCreateManyWithoutContract_contractTotx_destinationInput
  tx_contractTotx_source?: txCreateManyWithoutContract_contractTotx_sourceInput
}

export type delegationCreateWithoutAddressesInput = {
  autoid?: number
  operation_alpha: operation_alphaCreateOneWithoutDelegationInput
  contract: contractCreateOneWithoutDelegationInput
}

export type implicitCreateWithoutAddressesInput = {
  pk?: string | null
  autoid?: number
  operation_implicit_activatedTooperation?: operationCreateOneWithoutImplicit_implicit_activatedTooperationInput
  operation_implicit_revealedTooperation?: operationCreateOneWithoutImplicit_implicit_revealedTooperationInput
  contract?: contractCreateManyWithoutImplicitInput
  deactivated?: deactivatedCreateManyWithoutImplicitInput
  delegated_contract?: delegated_contractCreateManyWithoutImplicitInput
  endorsement?: endorsementCreateManyWithoutImplicitInput
}

export type operation_alphaCreateWithoutAddresses_addressesTooperation_alpha_receiverInput = {
  id: number
  operation_kind: number
  autoid?: number
  operation: operationCreateOneWithoutOperation_alphaInput
  addresses_addressesTooperation_alpha_sender?: addressesCreateOneWithoutOperation_alpha_addressesTooperation_alpha_senderInput
  delegation?: delegationCreateManyWithoutOperation_alphaInput
  origination?: originationCreateManyWithoutOperation_alphaInput
  tx?: txCreateManyWithoutOperation_alphaInput
}

export type operation_alphaCreateWithoutAddresses_addressesTooperation_alpha_senderInput = {
  id: number
  operation_kind: number
  autoid?: number
  operation: operationCreateOneWithoutOperation_alphaInput
  addresses_addressesTooperation_alpha_receiver?: addressesCreateOneWithoutOperation_alpha_addressesTooperation_alpha_receiverInput
  delegation?: delegationCreateManyWithoutOperation_alphaInput
  origination?: originationCreateManyWithoutOperation_alphaInput
  tx?: txCreateManyWithoutOperation_alphaInput
}

export type contractUpdateWithWhereUniqueWithoutAddresses_addressesTocontract_addressInput = {
  where: contractWhereUniqueInput
  data: contractUpdateWithoutAddresses_addressesTocontract_addressDataInput
}

export type contractUpsertWithWhereUniqueWithoutAddresses_addressesTocontract_addressInput = {
  where: contractWhereUniqueInput
  update: contractUpdateWithoutAddresses_addressesTocontract_addressDataInput
  create: contractCreateWithoutAddresses_addressesTocontract_addressInput
}

export type contractUpdateWithWhereUniqueWithoutAddresses_addressesTocontract_delegateInput = {
  where: contractWhereUniqueInput
  data: contractUpdateWithoutAddresses_addressesTocontract_delegateDataInput
}

export type contractUpsertWithWhereUniqueWithoutAddresses_addressesTocontract_delegateInput = {
  where: contractWhereUniqueInput
  update: contractUpdateWithoutAddresses_addressesTocontract_delegateDataInput
  create: contractCreateWithoutAddresses_addressesTocontract_delegateInput
}

export type contractUpdateWithWhereUniqueWithoutAddresses_addressesTocontract_mgrInput = {
  where: contractWhereUniqueInput
  data: contractUpdateWithoutAddresses_addressesTocontract_mgrDataInput
}

export type contractUpsertWithWhereUniqueWithoutAddresses_addressesTocontract_mgrInput = {
  where: contractWhereUniqueInput
  update: contractUpdateWithoutAddresses_addressesTocontract_mgrDataInput
  create: contractCreateWithoutAddresses_addressesTocontract_mgrInput
}

export type contractUpdateWithWhereUniqueWithoutAddresses_addressesTocontract_preorigInput = {
  where: contractWhereUniqueInput
  data: contractUpdateWithoutAddresses_addressesTocontract_preorigDataInput
}

export type contractUpsertWithWhereUniqueWithoutAddresses_addressesTocontract_preorigInput = {
  where: contractWhereUniqueInput
  update: contractUpdateWithoutAddresses_addressesTocontract_preorigDataInput
  create: contractCreateWithoutAddresses_addressesTocontract_preorigInput
}

export type delegationUpdateWithWhereUniqueWithoutAddressesInput = {
  where: delegationWhereUniqueInput
  data: delegationUpdateWithoutAddressesDataInput
}

export type delegationUpsertWithWhereUniqueWithoutAddressesInput = {
  where: delegationWhereUniqueInput
  update: delegationUpdateWithoutAddressesDataInput
  create: delegationCreateWithoutAddressesInput
}

export type implicitUpdateWithWhereUniqueWithoutAddressesInput = {
  where: implicitWhereUniqueInput
  data: implicitUpdateWithoutAddressesDataInput
}

export type implicitUpsertWithWhereUniqueWithoutAddressesInput = {
  where: implicitWhereUniqueInput
  update: implicitUpdateWithoutAddressesDataInput
  create: implicitCreateWithoutAddressesInput
}

export type operation_alphaUpdateWithWhereUniqueWithoutAddresses_addressesTooperation_alpha_receiverInput = {
  where: operation_alphaWhereUniqueInput
  data: operation_alphaUpdateWithoutAddresses_addressesTooperation_alpha_receiverDataInput
}

export type operation_alphaUpsertWithWhereUniqueWithoutAddresses_addressesTooperation_alpha_receiverInput = {
  where: operation_alphaWhereUniqueInput
  update: operation_alphaUpdateWithoutAddresses_addressesTooperation_alpha_receiverDataInput
  create: operation_alphaCreateWithoutAddresses_addressesTooperation_alpha_receiverInput
}

export type operation_alphaUpdateWithWhereUniqueWithoutAddresses_addressesTooperation_alpha_senderInput = {
  where: operation_alphaWhereUniqueInput
  data: operation_alphaUpdateWithoutAddresses_addressesTooperation_alpha_senderDataInput
}

export type operation_alphaUpsertWithWhereUniqueWithoutAddresses_addressesTooperation_alpha_senderInput = {
  where: operation_alphaWhereUniqueInput
  update: operation_alphaUpdateWithoutAddresses_addressesTooperation_alpha_senderDataInput
  create: operation_alphaCreateWithoutAddresses_addressesTooperation_alpha_senderInput
}

export type operationCreateWithoutManager_numbersInput = {
  hash: string
  autoid?: number
  block: blockCreateOneWithoutOperationInput
  chain_chainTooperation: chainCreateOneWithoutOperationInput
  endorsement?: endorsementCreateManyWithoutOperationInput
  implicit_implicit_activatedTooperation?: implicitCreateManyWithoutOperation_implicit_activatedTooperationInput
  implicit_implicit_revealedTooperation?: implicitCreateManyWithoutOperation_implicit_revealedTooperationInput
  operation_alpha?: operation_alphaCreateManyWithoutOperationInput
}

export type operationUpdateWithoutManager_numbersDataInput = {
  hash?: string | StringFieldUpdateOperationsInput
  autoid?: number | IntFieldUpdateOperationsInput
  block?: blockUpdateOneRequiredWithoutOperationInput
  chain_chainTooperation?: chainUpdateOneRequiredWithoutOperationInput
  endorsement?: endorsementUpdateManyWithoutOperationInput
  implicit_implicit_activatedTooperation?: implicitUpdateManyWithoutOperation_implicit_activatedTooperationInput
  implicit_implicit_revealedTooperation?: implicitUpdateManyWithoutOperation_implicit_revealedTooperationInput
  operation_alpha?: operation_alphaUpdateManyWithoutOperationInput
}

export type operationUpsertWithoutManager_numbersInput = {
  update: operationUpdateWithoutManager_numbersDataInput
  create: operationCreateWithoutManager_numbersInput
}

export type block_alphaUpdateWithoutBlockDataInput = {
  baker?: string | StringFieldUpdateOperationsInput
  level_position?: number | IntFieldUpdateOperationsInput
  cycle?: number | IntFieldUpdateOperationsInput
  cycle_position?: number | IntFieldUpdateOperationsInput
  voting_period?: number | IntFieldUpdateOperationsInput
  voting_period_position?: number | IntFieldUpdateOperationsInput
  voting_period_kind?: number | IntFieldUpdateOperationsInput
  consumed_gas?: string | StringFieldUpdateOperationsInput
}

export type block_alphaUpdateManyDataInput = {
  baker?: string | StringFieldUpdateOperationsInput
  level_position?: number | IntFieldUpdateOperationsInput
  cycle?: number | IntFieldUpdateOperationsInput
  cycle_position?: number | IntFieldUpdateOperationsInput
  voting_period?: number | IntFieldUpdateOperationsInput
  voting_period_position?: number | IntFieldUpdateOperationsInput
  voting_period_kind?: number | IntFieldUpdateOperationsInput
  consumed_gas?: string | StringFieldUpdateOperationsInput
}

export type contractUpdateWithoutBlockDataInput = {
  spendable?: boolean | BoolFieldUpdateOperationsInput
  delegatable?: boolean | BoolFieldUpdateOperationsInput
  credit?: number | NullableIntFieldUpdateOperationsInput | null
  script?: string | NullableStringFieldUpdateOperationsInput | null
  addresses_addressesTocontract_address?: addressesUpdateOneRequiredWithoutContract_addressesTocontract_addressInput
  addresses_addressesTocontract_delegate?: addressesUpdateOneWithoutContract_addressesTocontract_delegateInput
  addresses_addressesTocontract_mgr?: addressesUpdateOneWithoutContract_addressesTocontract_mgrInput
  implicit?: implicitUpdateOneWithoutContractInput
  addresses_addressesTocontract_preorig?: addressesUpdateOneWithoutContract_addressesTocontract_preorigInput
  contract?: contractUpdateOneWithoutOther_contractInput
  other_contract?: contractUpdateManyWithoutContractInput
  delegated_contract?: delegated_contractUpdateManyWithoutContractInput
  delegation?: delegationUpdateManyWithoutContractInput
  origination_contractToorigination_k?: originationUpdateManyWithoutContract_contractToorigination_kInput
  origination_contractToorigination_source?: originationUpdateManyWithoutContract_contractToorigination_sourceInput
  tx_contractTotx_destination?: txUpdateManyWithoutContract_contractTotx_destinationInput
  tx_contractTotx_source?: txUpdateManyWithoutContract_contractTotx_sourceInput
}

export type contractUpdateManyDataInput = {
  spendable?: boolean | BoolFieldUpdateOperationsInput
  delegatable?: boolean | BoolFieldUpdateOperationsInput
  credit?: number | NullableIntFieldUpdateOperationsInput | null
  script?: string | NullableStringFieldUpdateOperationsInput | null
}

export type deactivatedUpdateWithoutBlockDataInput = {
  implicit?: implicitUpdateOneRequiredWithoutDeactivatedInput
}

export type deactivatedUpdateManyDataInput = {

}

export type endorsementUpdateWithoutBlockDataInput = {
  id?: number | IntFieldUpdateOperationsInput
  level?: number | IntFieldUpdateOperationsInput
  slot?: number | IntFieldUpdateOperationsInput
  operation?: operationUpdateOneRequiredWithoutEndorsementInput
  implicit?: implicitUpdateOneRequiredWithoutEndorsementInput
}

export type endorsementUpdateManyDataInput = {
  id?: number | IntFieldUpdateOperationsInput
  level?: number | IntFieldUpdateOperationsInput
  slot?: number | IntFieldUpdateOperationsInput
}

export type operationUpdateWithoutBlockDataInput = {
  hash?: string | StringFieldUpdateOperationsInput
  autoid?: number | IntFieldUpdateOperationsInput
  chain_chainTooperation?: chainUpdateOneRequiredWithoutOperationInput
  endorsement?: endorsementUpdateManyWithoutOperationInput
  implicit_implicit_activatedTooperation?: implicitUpdateManyWithoutOperation_implicit_activatedTooperationInput
  implicit_implicit_revealedTooperation?: implicitUpdateManyWithoutOperation_implicit_revealedTooperationInput
  manager_numbers?: manager_numbersUpdateManyWithoutOperationInput
  operation_alpha?: operation_alphaUpdateManyWithoutOperationInput
}

export type operationUpdateManyDataInput = {
  hash?: string | StringFieldUpdateOperationsInput
  autoid?: number | IntFieldUpdateOperationsInput
}

export type operationUpdateWithoutChain_chainTooperationDataInput = {
  hash?: string | StringFieldUpdateOperationsInput
  autoid?: number | IntFieldUpdateOperationsInput
  block?: blockUpdateOneRequiredWithoutOperationInput
  endorsement?: endorsementUpdateManyWithoutOperationInput
  implicit_implicit_activatedTooperation?: implicitUpdateManyWithoutOperation_implicit_activatedTooperationInput
  implicit_implicit_revealedTooperation?: implicitUpdateManyWithoutOperation_implicit_revealedTooperationInput
  manager_numbers?: manager_numbersUpdateManyWithoutOperationInput
  operation_alpha?: operation_alphaUpdateManyWithoutOperationInput
}

export type contractUpdateWithoutContractDataInput = {
  spendable?: boolean | BoolFieldUpdateOperationsInput
  delegatable?: boolean | BoolFieldUpdateOperationsInput
  credit?: number | NullableIntFieldUpdateOperationsInput | null
  script?: string | NullableStringFieldUpdateOperationsInput | null
  addresses_addressesTocontract_address?: addressesUpdateOneRequiredWithoutContract_addressesTocontract_addressInput
  block?: blockUpdateOneRequiredWithoutContractInput
  addresses_addressesTocontract_delegate?: addressesUpdateOneWithoutContract_addressesTocontract_delegateInput
  addresses_addressesTocontract_mgr?: addressesUpdateOneWithoutContract_addressesTocontract_mgrInput
  implicit?: implicitUpdateOneWithoutContractInput
  addresses_addressesTocontract_preorig?: addressesUpdateOneWithoutContract_addressesTocontract_preorigInput
  other_contract?: contractUpdateManyWithoutContractInput
  delegated_contract?: delegated_contractUpdateManyWithoutContractInput
  delegation?: delegationUpdateManyWithoutContractInput
  origination_contractToorigination_k?: originationUpdateManyWithoutContract_contractToorigination_kInput
  origination_contractToorigination_source?: originationUpdateManyWithoutContract_contractToorigination_sourceInput
  tx_contractTotx_destination?: txUpdateManyWithoutContract_contractTotx_destinationInput
  tx_contractTotx_source?: txUpdateManyWithoutContract_contractTotx_sourceInput
}

export type delegated_contractUpdateWithoutContractDataInput = {
  snapshot?: snapshotUpdateOneRequiredWithoutDelegated_contractInput
  implicit?: implicitUpdateOneRequiredWithoutDelegated_contractInput
}

export type delegated_contractUpdateManyDataInput = {

}

export type delegationUpdateWithoutContractDataInput = {
  autoid?: number | IntFieldUpdateOperationsInput
  operation_alpha?: operation_alphaUpdateOneRequiredWithoutDelegationInput
  addresses?: addressesUpdateOneWithoutDelegationInput
}

export type delegationUpdateManyDataInput = {
  autoid?: number | IntFieldUpdateOperationsInput
}

export type originationUpdateWithoutContract_contractToorigination_kDataInput = {
  autoid?: number | IntFieldUpdateOperationsInput
  operation_alpha?: operation_alphaUpdateOneRequiredWithoutOriginationInput
  contract_contractToorigination_source?: contractUpdateOneRequiredWithoutOrigination_contractToorigination_sourceInput
}

export type originationUpdateManyDataInput = {
  autoid?: number | IntFieldUpdateOperationsInput
}

export type originationUpdateWithoutContract_contractToorigination_sourceDataInput = {
  autoid?: number | IntFieldUpdateOperationsInput
  contract_contractToorigination_k?: contractUpdateOneRequiredWithoutOrigination_contractToorigination_kInput
  operation_alpha?: operation_alphaUpdateOneRequiredWithoutOriginationInput
}

export type txUpdateWithoutContract_contractTotx_destinationDataInput = {
  fee?: number | IntFieldUpdateOperationsInput
  amount?: number | IntFieldUpdateOperationsInput
  parameters?: string | NullableStringFieldUpdateOperationsInput | null
  storage?: string | NullableStringFieldUpdateOperationsInput | null
  consumed_gas?: string | StringFieldUpdateOperationsInput
  storage_size?: string | StringFieldUpdateOperationsInput
  paid_storage_size_diff?: string | StringFieldUpdateOperationsInput
  entrypoint?: string | NullableStringFieldUpdateOperationsInput | null
  bigmapdiff?: string | NullableStringFieldUpdateOperationsInput | null
  autoid?: number | IntFieldUpdateOperationsInput
  operation_alpha?: operation_alphaUpdateOneRequiredWithoutTxInput
  contract_contractTotx_source?: contractUpdateOneRequiredWithoutTx_contractTotx_sourceInput
}

export type txUpdateManyDataInput = {
  fee?: number | IntFieldUpdateOperationsInput
  amount?: number | IntFieldUpdateOperationsInput
  parameters?: string | NullableStringFieldUpdateOperationsInput | null
  storage?: string | NullableStringFieldUpdateOperationsInput | null
  consumed_gas?: string | StringFieldUpdateOperationsInput
  storage_size?: string | StringFieldUpdateOperationsInput
  paid_storage_size_diff?: string | StringFieldUpdateOperationsInput
  entrypoint?: string | NullableStringFieldUpdateOperationsInput | null
  bigmapdiff?: string | NullableStringFieldUpdateOperationsInput | null
  autoid?: number | IntFieldUpdateOperationsInput
}

export type txUpdateWithoutContract_contractTotx_sourceDataInput = {
  fee?: number | IntFieldUpdateOperationsInput
  amount?: number | IntFieldUpdateOperationsInput
  parameters?: string | NullableStringFieldUpdateOperationsInput | null
  storage?: string | NullableStringFieldUpdateOperationsInput | null
  consumed_gas?: string | StringFieldUpdateOperationsInput
  storage_size?: string | StringFieldUpdateOperationsInput
  paid_storage_size_diff?: string | StringFieldUpdateOperationsInput
  entrypoint?: string | NullableStringFieldUpdateOperationsInput | null
  bigmapdiff?: string | NullableStringFieldUpdateOperationsInput | null
  autoid?: number | IntFieldUpdateOperationsInput
  contract_contractTotx_destination?: contractUpdateOneRequiredWithoutTx_contractTotx_destinationInput
  operation_alpha?: operation_alphaUpdateOneRequiredWithoutTxInput
}

export type contractUpdateWithoutImplicitDataInput = {
  spendable?: boolean | BoolFieldUpdateOperationsInput
  delegatable?: boolean | BoolFieldUpdateOperationsInput
  credit?: number | NullableIntFieldUpdateOperationsInput | null
  script?: string | NullableStringFieldUpdateOperationsInput | null
  addresses_addressesTocontract_address?: addressesUpdateOneRequiredWithoutContract_addressesTocontract_addressInput
  block?: blockUpdateOneRequiredWithoutContractInput
  addresses_addressesTocontract_delegate?: addressesUpdateOneWithoutContract_addressesTocontract_delegateInput
  addresses_addressesTocontract_mgr?: addressesUpdateOneWithoutContract_addressesTocontract_mgrInput
  addresses_addressesTocontract_preorig?: addressesUpdateOneWithoutContract_addressesTocontract_preorigInput
  contract?: contractUpdateOneWithoutOther_contractInput
  other_contract?: contractUpdateManyWithoutContractInput
  delegated_contract?: delegated_contractUpdateManyWithoutContractInput
  delegation?: delegationUpdateManyWithoutContractInput
  origination_contractToorigination_k?: originationUpdateManyWithoutContract_contractToorigination_kInput
  origination_contractToorigination_source?: originationUpdateManyWithoutContract_contractToorigination_sourceInput
  tx_contractTotx_destination?: txUpdateManyWithoutContract_contractTotx_destinationInput
  tx_contractTotx_source?: txUpdateManyWithoutContract_contractTotx_sourceInput
}

export type deactivatedUpdateWithoutImplicitDataInput = {
  block?: blockUpdateOneRequiredWithoutDeactivatedInput
}

export type delegated_contractUpdateWithoutImplicitDataInput = {
  snapshot?: snapshotUpdateOneRequiredWithoutDelegated_contractInput
  contract?: contractUpdateOneRequiredWithoutDelegated_contractInput
}

export type endorsementUpdateWithoutImplicitDataInput = {
  id?: number | IntFieldUpdateOperationsInput
  level?: number | IntFieldUpdateOperationsInput
  slot?: number | IntFieldUpdateOperationsInput
  block?: blockUpdateOneRequiredWithoutEndorsementInput
  operation?: operationUpdateOneRequiredWithoutEndorsementInput
}

export type endorsementUpdateWithoutOperationDataInput = {
  id?: number | IntFieldUpdateOperationsInput
  level?: number | IntFieldUpdateOperationsInput
  slot?: number | IntFieldUpdateOperationsInput
  block?: blockUpdateOneRequiredWithoutEndorsementInput
  implicit?: implicitUpdateOneRequiredWithoutEndorsementInput
}

export type implicitUpdateWithoutOperation_implicit_activatedTooperationDataInput = {
  pk?: string | NullableStringFieldUpdateOperationsInput | null
  autoid?: number | IntFieldUpdateOperationsInput
  addresses?: addressesUpdateOneRequiredWithoutImplicitInput
  operation_implicit_revealedTooperation?: operationUpdateOneWithoutImplicit_implicit_revealedTooperationInput
  contract?: contractUpdateManyWithoutImplicitInput
  deactivated?: deactivatedUpdateManyWithoutImplicitInput
  delegated_contract?: delegated_contractUpdateManyWithoutImplicitInput
  endorsement?: endorsementUpdateManyWithoutImplicitInput
}

export type implicitUpdateManyDataInput = {
  pk?: string | NullableStringFieldUpdateOperationsInput | null
  autoid?: number | IntFieldUpdateOperationsInput
}

export type implicitUpdateWithoutOperation_implicit_revealedTooperationDataInput = {
  pk?: string | NullableStringFieldUpdateOperationsInput | null
  autoid?: number | IntFieldUpdateOperationsInput
  operation_implicit_activatedTooperation?: operationUpdateOneWithoutImplicit_implicit_activatedTooperationInput
  addresses?: addressesUpdateOneRequiredWithoutImplicitInput
  contract?: contractUpdateManyWithoutImplicitInput
  deactivated?: deactivatedUpdateManyWithoutImplicitInput
  delegated_contract?: delegated_contractUpdateManyWithoutImplicitInput
  endorsement?: endorsementUpdateManyWithoutImplicitInput
}

export type manager_numbersUpdateWithoutOperationDataInput = {
  counter?: string | NullableStringFieldUpdateOperationsInput | null
  gas_limit?: string | NullableStringFieldUpdateOperationsInput | null
  storage_limit?: string | NullableStringFieldUpdateOperationsInput | null
}

export type manager_numbersUpdateManyDataInput = {
  counter?: string | NullableStringFieldUpdateOperationsInput | null
  gas_limit?: string | NullableStringFieldUpdateOperationsInput | null
  storage_limit?: string | NullableStringFieldUpdateOperationsInput | null
}

export type operation_alphaUpdateWithoutOperationDataInput = {
  id?: number | IntFieldUpdateOperationsInput
  operation_kind?: number | IntFieldUpdateOperationsInput
  autoid?: number | IntFieldUpdateOperationsInput
  addresses_addressesTooperation_alpha_receiver?: addressesUpdateOneWithoutOperation_alpha_addressesTooperation_alpha_receiverInput
  addresses_addressesTooperation_alpha_sender?: addressesUpdateOneWithoutOperation_alpha_addressesTooperation_alpha_senderInput
  delegation?: delegationUpdateManyWithoutOperation_alphaInput
  origination?: originationUpdateManyWithoutOperation_alphaInput
  tx?: txUpdateManyWithoutOperation_alphaInput
}

export type operation_alphaUpdateManyDataInput = {
  id?: number | IntFieldUpdateOperationsInput
  operation_kind?: number | IntFieldUpdateOperationsInput
  autoid?: number | IntFieldUpdateOperationsInput
}

export type delegationUpdateWithoutOperation_alphaDataInput = {
  autoid?: number | IntFieldUpdateOperationsInput
  addresses?: addressesUpdateOneWithoutDelegationInput
  contract?: contractUpdateOneRequiredWithoutDelegationInput
}

export type originationUpdateWithoutOperation_alphaDataInput = {
  autoid?: number | IntFieldUpdateOperationsInput
  contract_contractToorigination_k?: contractUpdateOneRequiredWithoutOrigination_contractToorigination_kInput
  contract_contractToorigination_source?: contractUpdateOneRequiredWithoutOrigination_contractToorigination_sourceInput
}

export type txUpdateWithoutOperation_alphaDataInput = {
  fee?: number | IntFieldUpdateOperationsInput
  amount?: number | IntFieldUpdateOperationsInput
  parameters?: string | NullableStringFieldUpdateOperationsInput | null
  storage?: string | NullableStringFieldUpdateOperationsInput | null
  consumed_gas?: string | StringFieldUpdateOperationsInput
  storage_size?: string | StringFieldUpdateOperationsInput
  paid_storage_size_diff?: string | StringFieldUpdateOperationsInput
  entrypoint?: string | NullableStringFieldUpdateOperationsInput | null
  bigmapdiff?: string | NullableStringFieldUpdateOperationsInput | null
  autoid?: number | IntFieldUpdateOperationsInput
  contract_contractTotx_destination?: contractUpdateOneRequiredWithoutTx_contractTotx_destinationInput
  contract_contractTotx_source?: contractUpdateOneRequiredWithoutTx_contractTotx_sourceInput
}

export type delegated_contractUpdateWithoutSnapshotDataInput = {
  implicit?: implicitUpdateOneRequiredWithoutDelegated_contractInput
  contract?: contractUpdateOneRequiredWithoutDelegated_contractInput
}

export type contractUpdateWithoutAddresses_addressesTocontract_addressDataInput = {
  spendable?: boolean | BoolFieldUpdateOperationsInput
  delegatable?: boolean | BoolFieldUpdateOperationsInput
  credit?: number | NullableIntFieldUpdateOperationsInput | null
  script?: string | NullableStringFieldUpdateOperationsInput | null
  block?: blockUpdateOneRequiredWithoutContractInput
  addresses_addressesTocontract_delegate?: addressesUpdateOneWithoutContract_addressesTocontract_delegateInput
  addresses_addressesTocontract_mgr?: addressesUpdateOneWithoutContract_addressesTocontract_mgrInput
  implicit?: implicitUpdateOneWithoutContractInput
  addresses_addressesTocontract_preorig?: addressesUpdateOneWithoutContract_addressesTocontract_preorigInput
  contract?: contractUpdateOneWithoutOther_contractInput
  other_contract?: contractUpdateManyWithoutContractInput
  delegated_contract?: delegated_contractUpdateManyWithoutContractInput
  delegation?: delegationUpdateManyWithoutContractInput
  origination_contractToorigination_k?: originationUpdateManyWithoutContract_contractToorigination_kInput
  origination_contractToorigination_source?: originationUpdateManyWithoutContract_contractToorigination_sourceInput
  tx_contractTotx_destination?: txUpdateManyWithoutContract_contractTotx_destinationInput
  tx_contractTotx_source?: txUpdateManyWithoutContract_contractTotx_sourceInput
}

export type contractUpdateWithoutAddresses_addressesTocontract_delegateDataInput = {
  spendable?: boolean | BoolFieldUpdateOperationsInput
  delegatable?: boolean | BoolFieldUpdateOperationsInput
  credit?: number | NullableIntFieldUpdateOperationsInput | null
  script?: string | NullableStringFieldUpdateOperationsInput | null
  addresses_addressesTocontract_address?: addressesUpdateOneRequiredWithoutContract_addressesTocontract_addressInput
  block?: blockUpdateOneRequiredWithoutContractInput
  addresses_addressesTocontract_mgr?: addressesUpdateOneWithoutContract_addressesTocontract_mgrInput
  implicit?: implicitUpdateOneWithoutContractInput
  addresses_addressesTocontract_preorig?: addressesUpdateOneWithoutContract_addressesTocontract_preorigInput
  contract?: contractUpdateOneWithoutOther_contractInput
  other_contract?: contractUpdateManyWithoutContractInput
  delegated_contract?: delegated_contractUpdateManyWithoutContractInput
  delegation?: delegationUpdateManyWithoutContractInput
  origination_contractToorigination_k?: originationUpdateManyWithoutContract_contractToorigination_kInput
  origination_contractToorigination_source?: originationUpdateManyWithoutContract_contractToorigination_sourceInput
  tx_contractTotx_destination?: txUpdateManyWithoutContract_contractTotx_destinationInput
  tx_contractTotx_source?: txUpdateManyWithoutContract_contractTotx_sourceInput
}

export type contractUpdateWithoutAddresses_addressesTocontract_mgrDataInput = {
  spendable?: boolean | BoolFieldUpdateOperationsInput
  delegatable?: boolean | BoolFieldUpdateOperationsInput
  credit?: number | NullableIntFieldUpdateOperationsInput | null
  script?: string | NullableStringFieldUpdateOperationsInput | null
  addresses_addressesTocontract_address?: addressesUpdateOneRequiredWithoutContract_addressesTocontract_addressInput
  block?: blockUpdateOneRequiredWithoutContractInput
  addresses_addressesTocontract_delegate?: addressesUpdateOneWithoutContract_addressesTocontract_delegateInput
  implicit?: implicitUpdateOneWithoutContractInput
  addresses_addressesTocontract_preorig?: addressesUpdateOneWithoutContract_addressesTocontract_preorigInput
  contract?: contractUpdateOneWithoutOther_contractInput
  other_contract?: contractUpdateManyWithoutContractInput
  delegated_contract?: delegated_contractUpdateManyWithoutContractInput
  delegation?: delegationUpdateManyWithoutContractInput
  origination_contractToorigination_k?: originationUpdateManyWithoutContract_contractToorigination_kInput
  origination_contractToorigination_source?: originationUpdateManyWithoutContract_contractToorigination_sourceInput
  tx_contractTotx_destination?: txUpdateManyWithoutContract_contractTotx_destinationInput
  tx_contractTotx_source?: txUpdateManyWithoutContract_contractTotx_sourceInput
}

export type contractUpdateWithoutAddresses_addressesTocontract_preorigDataInput = {
  spendable?: boolean | BoolFieldUpdateOperationsInput
  delegatable?: boolean | BoolFieldUpdateOperationsInput
  credit?: number | NullableIntFieldUpdateOperationsInput | null
  script?: string | NullableStringFieldUpdateOperationsInput | null
  addresses_addressesTocontract_address?: addressesUpdateOneRequiredWithoutContract_addressesTocontract_addressInput
  block?: blockUpdateOneRequiredWithoutContractInput
  addresses_addressesTocontract_delegate?: addressesUpdateOneWithoutContract_addressesTocontract_delegateInput
  addresses_addressesTocontract_mgr?: addressesUpdateOneWithoutContract_addressesTocontract_mgrInput
  implicit?: implicitUpdateOneWithoutContractInput
  contract?: contractUpdateOneWithoutOther_contractInput
  other_contract?: contractUpdateManyWithoutContractInput
  delegated_contract?: delegated_contractUpdateManyWithoutContractInput
  delegation?: delegationUpdateManyWithoutContractInput
  origination_contractToorigination_k?: originationUpdateManyWithoutContract_contractToorigination_kInput
  origination_contractToorigination_source?: originationUpdateManyWithoutContract_contractToorigination_sourceInput
  tx_contractTotx_destination?: txUpdateManyWithoutContract_contractTotx_destinationInput
  tx_contractTotx_source?: txUpdateManyWithoutContract_contractTotx_sourceInput
}

export type delegationUpdateWithoutAddressesDataInput = {
  autoid?: number | IntFieldUpdateOperationsInput
  operation_alpha?: operation_alphaUpdateOneRequiredWithoutDelegationInput
  contract?: contractUpdateOneRequiredWithoutDelegationInput
}

export type implicitUpdateWithoutAddressesDataInput = {
  pk?: string | NullableStringFieldUpdateOperationsInput | null
  autoid?: number | IntFieldUpdateOperationsInput
  operation_implicit_activatedTooperation?: operationUpdateOneWithoutImplicit_implicit_activatedTooperationInput
  operation_implicit_revealedTooperation?: operationUpdateOneWithoutImplicit_implicit_revealedTooperationInput
  contract?: contractUpdateManyWithoutImplicitInput
  deactivated?: deactivatedUpdateManyWithoutImplicitInput
  delegated_contract?: delegated_contractUpdateManyWithoutImplicitInput
  endorsement?: endorsementUpdateManyWithoutImplicitInput
}

export type operation_alphaUpdateWithoutAddresses_addressesTooperation_alpha_receiverDataInput = {
  id?: number | IntFieldUpdateOperationsInput
  operation_kind?: number | IntFieldUpdateOperationsInput
  autoid?: number | IntFieldUpdateOperationsInput
  operation?: operationUpdateOneRequiredWithoutOperation_alphaInput
  addresses_addressesTooperation_alpha_sender?: addressesUpdateOneWithoutOperation_alpha_addressesTooperation_alpha_senderInput
  delegation?: delegationUpdateManyWithoutOperation_alphaInput
  origination?: originationUpdateManyWithoutOperation_alphaInput
  tx?: txUpdateManyWithoutOperation_alphaInput
}

export type operation_alphaUpdateWithoutAddresses_addressesTooperation_alpha_senderDataInput = {
  id?: number | IntFieldUpdateOperationsInput
  operation_kind?: number | IntFieldUpdateOperationsInput
  autoid?: number | IntFieldUpdateOperationsInput
  operation?: operationUpdateOneRequiredWithoutOperation_alphaInput
  addresses_addressesTooperation_alpha_receiver?: addressesUpdateOneWithoutOperation_alpha_addressesTooperation_alpha_receiverInput
  delegation?: delegationUpdateManyWithoutOperation_alphaInput
  origination?: originationUpdateManyWithoutOperation_alphaInput
  tx?: txUpdateManyWithoutOperation_alphaInput
}

/**
 * Batch Payload for updateMany & deleteMany
 */

export type BatchPayload = {
  count: number
}

/**
 * DMMF
 */
export declare const dmmf: DMMF.Document;
export {};
