generator typegraphql {
  provider = "prisma-client-js"
  output   = "../src/generated/typegraphql-prisma"
}

datasource db {
  provider = "postgresql"
  url      = env("DATABASE_URL")
}

model block {
  hash              String        @unique
  level             Int
  proto             Int
  predecessor       String
  timestamp         DateTime
  validation_passes Int
  merkle_root       String
  fitness           String
  context_hash      String
  sterile           Boolean?
  // balance        balance[]
  block_alpha       block_alpha[]
  contract          contract[]
  deactivated       deactivated[]
  endorsement       endorsement[]
  operation         operation[]

  @@id([hash, level])
  @@index([hash], name: "block_hash")
  @@index([level], name: "block_level")
  @@index([sterile], name: "block_sterile")
}

model block_alpha {
  hash                   String @id
  baker                  String
  level_position         Int
  cycle                  Int
  cycle_position         Int
  voting_period          Int
  voting_period_position Int
  voting_period_kind     Int
  consumed_gas           String
  block                  block  @relation(fields: [hash], references: [hash])

  @@index([baker], name: "block_alpha_baker")
  @@index([cycle], name: "block_alpha_cycle")
  @@index([cycle_position], name: "block_alpha_cycle_position")
  @@index([hash], name: "block_alpha_hash")
  @@index([level_position], name: "block_alpha_level_position")
}

model chain {
  hash      String      @id
  operation operation[]
}

model contract {
  address                                  String               @id
  block_hash                               String
  mgr                                      String?
  delegate                                 String?
  spendable                                Boolean
  delegatable                              Boolean
  credit                                   Int?
  preorig                                  String?
  script                                   String?
  addresses_addressesTocontract_address    addresses            @relation("addressesTocontract_address", fields: [address], references: [address])
  block                                    block                @relation(fields: [block_hash], references: [hash])
  addresses_addressesTocontract_delegate   addresses?           @relation("addressesTocontract_delegate", fields: [delegate], references: [address])
  addresses_addressesTocontract_mgr        addresses?           @relation("addressesTocontract_mgr", fields: [mgr], references: [address])
  implicit                                 implicit?            @relation(fields: [mgr], references: [pkh])
  addresses_addressesTocontract_preorig    addresses?           @relation("addressesTocontract_preorig", fields: [preorig], references: [address])
  contract                                 contract?            @relation("contractTocontract_preorig", fields: [preorig], references: [address])
  // balance                               balance[]
  other_contract                           contract[]           @relation("contractTocontract_preorig")
  delegated_contract                       delegated_contract[]
  delegation                               delegation[]
  origination_contractToorigination_k      origination[]        @relation("contractToorigination_k")
  origination_contractToorigination_source origination[]        @relation("contractToorigination_source")
  tx_contractTotx_destination              tx[]                 @relation("contractTotx_destination")
  tx_contractTotx_source                   tx[]                 @relation("contractTotx_source")

  @@index([address], name: "contract_address")
  @@index([block_hash], name: "contract_block")
  @@index([delegate], name: "contract_delegate")
  @@index([mgr], name: "contract_mgr")
  @@index([preorig], name: "contract_preorig")
}

model deactivated {
  pkh        String
  block_hash String
  block      block    @relation(fields: [block_hash], references: [hash])
  implicit   implicit @relation(fields: [pkh], references: [pkh])

  @@id([pkh, block_hash])
  @@index([block_hash], name: "deactivated_block_hash")
  @@index([pkh], name: "deactivated_pkh")
}

model delegated_contract {
  delegate  String
  delegator String
  cycle     Int
  level     Int
  snapshot  snapshot @relation(fields: [cycle, level], references: [cycle, level])
  implicit  implicit @relation(fields: [delegate], references: [pkh])
  contract  contract @relation(fields: [delegator], references: [address])

  @@id([delegate, delegator, cycle, level])
  @@index([cycle], name: "delegated_contract_cycle")
  @@index([delegate], name: "delegated_contract_delegate")
  @@index([delegator], name: "delegated_contract_delegator")
  @@index([level], name: "delegated_contract_level")
}

model delegation {
  operation_hash  String
  op_id           Int
  source          String
  pkh             String?
  autoid          Int             @default(autoincrement())
  operation_alpha operation_alpha @relation(fields: [operation_hash, op_id], references: [hash, id])
  addresses       addresses?      @relation(fields: [pkh], references: [address])
  contract        contract        @relation(fields: [source], references: [address])

  @@id([operation_hash, op_id])
  @@index([operation_hash], name: "delegation_operation_hash")
  @@index([pkh], name: "delegation_pkh")
  @@index([source], name: "delegation_source")
}

model endorsement {
  block_hash String
  op         String
  id         Int
  level      Int
  pkh        String
  slot       Int
  block      block     @relation(fields: [block_hash], references: [hash])
  operation  operation @relation(fields: [op], references: [hash])
  implicit   implicit  @relation(fields: [pkh], references: [pkh])

  @@id([block_hash, op, id, level, pkh, slot])
}

model implicit {
  pkh                                     String               @id
  activated                               String?
  revealed                                String?
  pk                                      String?
  autoid                                  Int                  @default(autoincrement())
  operation_implicit_activatedTooperation operation?           @relation("implicit_activatedTooperation", fields: [activated], references: [hash])
  addresses                               addresses            @relation(fields: [pkh], references: [address])
  operation_implicit_revealedTooperation  operation?           @relation("implicit_revealedTooperation", fields: [revealed], references: [hash])
  contract                                contract[]
  deactivated                             deactivated[]
  delegated_contract                      delegated_contract[]
  endorsement                             endorsement[]

  @@index([activated], name: "implicit_activated")
  @@index([pkh], name: "implicit_pkh")
  @@index([revealed], name: "implicit_revealed")
}

model operation {
  hash                                   String            @id
  chain                                  String
  block_hash                             String
  autoid                                 Int               @default(autoincrement())
  block                                  block             @relation(fields: [block_hash], references: [hash])
  chain_chainTooperation                 chain             @relation(fields: [chain], references: [hash])
  endorsement                            endorsement[]
  implicit_implicit_activatedTooperation implicit[]        @relation("implicit_activatedTooperation")
  implicit_implicit_revealedTooperation  implicit[]        @relation("implicit_revealedTooperation")
  manager_numbers                        manager_numbers[]
  operation_alpha                        operation_alpha[]

  @@index([block_hash], name: "operation_block")
  @@index([chain], name: "operation_chain")
  @@index([hash], name: "operation_hash")
}

model operation_alpha {
  hash                                          String
  id                                            Int
  operation_kind                                Int
  sender                                        String?
  receiver                                      String?
  autoid                                        Int           @default(autoincrement())
  operation                                     operation     @relation(fields: [hash], references: [hash])
  addresses_addressesTooperation_alpha_receiver addresses?    @relation("addressesTooperation_alpha_receiver", fields: [receiver], references: [address])
  addresses_addressesTooperation_alpha_sender   addresses?    @relation("addressesTooperation_alpha_sender", fields: [sender], references: [address])
  // balance                                    balance[]
  delegation                                    delegation[]
  origination                                   origination[]
  tx                                            tx[]

  @@id([hash, id])
  @@index([operation_kind], name: "operation_alpha_cat")
  @@index([hash], name: "operation_alpha_hash")
}

model origination {
  operation_hash                        String
  op_id                                 Int
  source                                String
  k                                     String
  autoid                                Int             @default(autoincrement())
  contract_contractToorigination_k      contract        @relation("contractToorigination_k", fields: [k], references: [address])
  operation_alpha                       operation_alpha @relation(fields: [operation_hash, op_id], references: [hash, id])
  contract_contractToorigination_source contract        @relation("contractToorigination_source", fields: [source], references: [address])

  @@id([operation_hash, op_id])
  @@index([k], name: "origination_k")
  @@index([operation_hash], name: "origination_operation_hash")
  @@index([source], name: "origination_source")
}

model snapshot {
  cycle              Int
  level              Int
  delegated_contract delegated_contract[]

  @@id([cycle, level])
}

model tx {
  operation_hash                    String
  op_id                             Int
  source                            String
  destination                       String
  fee                               Int
  amount                            Int
  parameters                        String?
  storage                           String?
  consumed_gas                      String
  storage_size                      String
  paid_storage_size_diff            String
  entrypoint                        String?
  bigmapdiff                        String?
  autoid                            Int             @default(autoincrement())
  contract_contractTotx_destination contract        @relation("contractTotx_destination", fields: [destination], references: [address])
  operation_alpha                   operation_alpha @relation(fields: [operation_hash, op_id], references: [hash, id])
  contract_contractTotx_source      contract        @relation("contractTotx_source", fields: [source], references: [address])

  @@id([operation_hash, op_id])
  @@index([destination], name: "tx_destination")
  @@index([operation_hash], name: "tx_operation_hash")
  @@index([source], name: "tx_source")
}

model addresses {
  address                                             String            @id
  contract_addressesTocontract_address                contract[]        @relation("addressesTocontract_address")
  contract_addressesTocontract_delegate               contract[]        @relation("addressesTocontract_delegate")
  contract_addressesTocontract_mgr                    contract[]        @relation("addressesTocontract_mgr")
  contract_addressesTocontract_preorig                contract[]        @relation("addressesTocontract_preorig")
  delegation                                          delegation[]
  implicit                                            implicit[]
  operation_alpha_addressesTooperation_alpha_receiver operation_alpha[] @relation("addressesTooperation_alpha_receiver")
  operation_alpha_addressesTooperation_alpha_sender   operation_alpha[] @relation("addressesTooperation_alpha_sender")
}

model manager_numbers {
  hash          String    @id
  counter       String?
  gas_limit     String?
  storage_limit String?
  operation     operation @relation(fields: [hash], references: [hash])
}

// The underlying table does not contain a valid unique identifier and can therefore currently not be handled.
// model balance {
  // block_hash       String
  // operation_hash   String?
  // op_id            Int?
  // balance_kind     Int
  // contract_address String
  // cycle            Int?
  // diff             Int
  // autoid           Int              @default(autoincrement())
  // block            block            @relation(fields: [block_hash], references: [hash])
  // contract         contract         @relation(fields: [contract_address], references: [address])
  // operation_alpha  operation_alpha? @relation(fields: [operation_hash, op_id], references: [hash, id])

  // @@index([block_hash], name: "balance_block")
  // @@index([balance_kind], name: "balance_cat")
  // @@index([cycle], name: "balance_cycle")
  // @@index([contract_address], name: "balance_k")
  // @@index([operation_hash, op_id], name: "balance_op")
  // @@index([operation_hash], name: "balance_ophash")
// }
