import { ApolloServer } from 'apollo-server-express';
import express from 'express';
import { PubSub } from 'graphql-subscriptions';
import http from 'http';
import { DependencyContainer, inject, injectAll, injectable } from 'tsyringe';
import { buildSchema } from 'type-graphql';

import { DITokens, GraphQLResolverType } from './dependency-injection';
import { ResolverContext } from './resolvers/resolver-context';
import { BackgroundWorker } from './utils/background-worker';
import { EnvConfig } from './utils/env-config';
import { GraphQLContainer } from './utils/graphql-container';
import { Logger, LoggerFactory } from './utils/logger';

@injectable()
export class App {
    private readonly logger: Logger;
    private httpServer: http.Server | undefined;
    private apolloServer: ApolloServer | undefined;

    constructor(
        private readonly config: EnvConfig,
        @injectAll(DITokens.BackgroundWorkers) private readonly backgroundWorkers: readonly BackgroundWorker[],
        @injectAll(DITokens.Resolvers) private readonly resolvers: readonly GraphQLResolverType[],
        @inject(DITokens.Container) private readonly container: DependencyContainer,
        private readonly pubSub: PubSub,
        loggerFactory: LoggerFactory,
    ) {
        this.logger = loggerFactory.get(App);
    }

    async start(): Promise<void> {
        if (this.httpServer || this.apolloServer) {
            throw new Error('App is already started.');
        }

        this.logger.info(`Server starting at: http://${this.config.host}:${this.config.port}`);
        const expressApp = express();
        this.httpServer = http.createServer(expressApp);

        const [firstResolver, ...remainingResolvers] = this.resolvers;
        this.apolloServer = new ApolloServer({
            schema: await buildSchema({
                resolvers: [firstResolver, ...remainingResolvers],
                validate: false,
                pubSub: this.pubSub,
                container: new GraphQLContainer(this.container),
            }),
            subscriptions: { keepAlive: this.config.subscriptionKeepAliveMillis },
            context: ({ req, res }): ResolverContext => ({ req, res, container: this.container }),
        });

        this.backgroundWorkers.forEach(b => b.start());

        this.apolloServer.applyMiddleware({ app: expressApp, path: '/graphql' });
        this.apolloServer.installSubscriptionHandlers(this.httpServer);

        this.httpServer.listen(this.config.port, this.config.host);
        this.logger.info('Server started.');
    }

    async stop(): Promise<void> {
        if (!this.httpServer || !this.apolloServer) {
            throw new Error('App is not started.');
        }

        this.logger.info('Server stopping.');
        this.backgroundWorkers.forEach(b => b.stop());
        await this.apolloServer.stop();
        this.httpServer.close();

        this.apolloServer = undefined;
        this.httpServer = undefined;
        this.logger.info('Server stopped.');
    }
}
