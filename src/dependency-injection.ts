import 'reflect-metadata';

import { PrismaClient } from '@prisma/client';
import { RpcClient } from '@taquito/rpc';
import { PubSub } from 'graphql-subscriptions';
import { container } from 'tsyringe';

import { AccountResolver } from './resolvers/account';
import { BlockNotificationResolver } from './resolvers/block-notification-resolver';
import { OperationNotificationResolver } from './resolvers/operation-notification-resolver';
import { TezosNodeMonitorStatusResolver } from './resolvers/tezos-node-monitor-status-resolver';
import { TezosRpcMonitor } from './rpc/tezos-rpc-monitor';
import { NotificationsPublisher } from './subscriptions/notifications-publisher';
import { BackgroundWorker } from './utils/background-worker';
import { EnvConfig, config } from './utils/env-config';
import { LoggerFactory } from './utils/logger';
import { Constructor } from './utils/reflection';
import { createRootLogger } from './utils/root-logger-factory';

export const enum DITokens {
    Container = 'Container',
    Resolvers = 'Resolvers',
    BackgroundWorkers = 'BackgroundWorkers',
}

export { container };

container.registerInstance(DITokens.Container, container);
container.registerInstance(EnvConfig, config);
container.registerInstance(LoggerFactory, new LoggerFactory(createRootLogger(config.environment)));
container.registerInstance(PrismaClient, new PrismaClient());
container.registerInstance(RpcClient, new RpcClient(config.tezosNodeUrl));
container.registerInstance(PubSub, new PubSub());

container.register<BackgroundWorker>(DITokens.BackgroundWorkers, { useFactory: c => c.resolve(NotificationsPublisher) });
container.register<BackgroundWorker>(DITokens.BackgroundWorkers, { useFactory: c => c.resolve(TezosRpcMonitor) });

export type GraphQLResolverType = Constructor<any>; /* eslint-disable-line @typescript-eslint/no-explicit-any */

if (config.graphQL.queriesEnabled) {
    container.registerInstance<GraphQLResolverType>(DITokens.Resolvers, AccountResolver);
}
if (config.graphQL.subscriptionsEnabled) {
    for (const type of [BlockNotificationResolver, OperationNotificationResolver, TezosNodeMonitorStatusResolver]) {
        container.registerInstance<GraphQLResolverType>(DITokens.Resolvers, type);
    }
}
