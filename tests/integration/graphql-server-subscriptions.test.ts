import 'reflect-metadata';

import { InMemoryCache, NormalizedCacheObject } from 'apollo-cache-inmemory';
import ApolloClient from 'apollo-client';
import { WebSocketLink } from 'apollo-link-ws';
import { expect } from 'chai';
import fs from 'fs';
import { DocumentNode } from 'graphql';
import { afterEach, beforeEach } from 'mocha';
import nock from 'nock';
import { PassThrough } from 'stream';
import { SubscriptionClient } from 'subscriptions-transport-ws';
import ws from 'ws';

import { App } from '../../src/app';
import { container } from '../../src/dependency-injection';
import { EnvConfig } from '../../src/utils/env-config';
import { Logger, LoggerFactory } from '../../src/utils/logger';
import { activateAccountAdded } from './gql/activate-account-added';
import { ballotAdded } from './gql/ballot-added';
import { blockAdded } from './gql/block-added';
import { delegationAdded } from './gql/delegation-added';
import { doubleBakingEvidenceAdded } from './gql/double-baking-evidence-added';
import { doubleEndorsementEvidenceAdded } from './gql/double-endorsement-evidence-added';
import { endorsementAdded } from './gql/endorsement-added';
import { originationAdded } from './gql/origination-added';
import { proposalsAdded } from './gql/proposals-added';
import { revealAdded } from './gql/reveal-added';
import { seedNonceRevelationAdded } from './gql/seed-nonce-revelation-added';
import { transactionAdded } from './gql/transaction-added';

const testEndpoint = 'ws://localhost:3000/graphql';

describe('Apollo GraphQL Subscriptions', () => {
    let app: App;
    let wsClient: SubscriptionClient;
    let apolloClient: ApolloClient<NormalizedCacheObject>;
    let monitorResponseStream: PassThrough;
    let config: EnvConfig;
    let logger: Logger;

    beforeEach(async () => {
        config = container.resolve(EnvConfig);
        logger = container.resolve(LoggerFactory).get('TestRunner');
        app = container.resolve(App);

        // Mock RPC responses
        monitorResponseStream = new PassThrough();
        const maping = [
            {
                resource: '/monitor/heads/main',
                response: monitorResponseStream,
                delayConnection: 100,
                delayBody: 200,
            }, {
                resource: '/chains/main/blocks/BLBiBqzhMU1z5VpcTxHZc1wwwvU92LE4YMU96vTfiQSSbVHCqss',
                response: getTestData('block.json'),
                delayConnection: 200,
                delayBody: 200,
            },
        ];

        setupRpcNock(config.tezosNodeUrl, maping);
        await app.start();

        // Initialize websockets test client
        wsClient = new SubscriptionClient(testEndpoint, {
            reconnect: true,
        }, ws);

        apolloClient = new ApolloClient({
            link: new WebSocketLink(wsClient),
            cache: new InMemoryCache(),
        });
    });

    it('should fetch RPC monitor head and corresponding block, subscribe to Apollo websockets and get corresponding notifications', async () => {
        const endorsementFilter = {
            delegate: {
                in: ['tz1e7uhpwmiKp8Yd2KwFwmVhoT31L478KUe3', 'tz1VxS7ff4YnZRs8b4mMP4WaMVpoQjuo1rjf'],
                notIn: ['tz1NRTQeqcuwybgrZfJavBY3of83u8uLpFBj', 'tz1T8UYSbVuRm6CdhjvwCfXsKXb4yL9ai9Q3'],
                notEqualTo: 'tz1TEZtYnuLiZLdA6c7JysAUJcHMrogu4Cpr',
            },
        };

        const originationFilter = {
            delegate: { isNull: true },
            originated_contract: { isNull: false, includes: 'tz1X6MxebHJ4DLu6VAcEaCcqn6JoPg6tqrWW' },
        };

        const transactionFilter = {
            source: {
                equalTo: 'tz1iHtecBqvddFzQ9Jg9tEnpH5CJSPYv4Ww7',
                notEqualTo: 'tz2FCNBrERXtaTtNX6iimR1UJ5JSDxvdHM93',
                in: ['tz1iHtecBqvddFzQ9Jg9tEnpH5CJSPYv4Ww7', 'tz2FCNBrERXtaTtNX6iimR1UJ5JSDxvdHM93'],
            },
            destination: { notIn: ['tz2FCNBrERXtaTtNX6iimR1UJ5JSDxvdHM93', 'tz1iHtecBqvddFzQ9Jg9tEnpH5CJSPYv4Ww7'] },
            status: { notEqualTo: 'applied', notIn: ['backtracked', 'skipped'] },
        };

        const subscriptionDefinitions = [
            new SubscriptionDefinition('block-notifications', blockAdded),
            new SubscriptionDefinition('activate-account-notifications', activateAccountAdded),
            new SubscriptionDefinition('ballot-notifications', ballotAdded),
            new SubscriptionDefinition('delegation-notifications', delegationAdded),
            new SubscriptionDefinition('double-baking-evidence-notifications', doubleBakingEvidenceAdded),
            new SubscriptionDefinition('double-endorsement-evidence-notifications', doubleEndorsementEvidenceAdded),
            new SubscriptionDefinition('endorsement-notifications', endorsementAdded),
            new SubscriptionDefinition('endorsement-notifications-filtered', endorsementAdded, endorsementFilter),
            new SubscriptionDefinition('origination-notifications', originationAdded),
            new SubscriptionDefinition('origination-notifications-filtered', originationAdded, originationFilter),
            new SubscriptionDefinition('proposals-notifications', proposalsAdded),
            new SubscriptionDefinition('reveal-notifications', revealAdded),
            new SubscriptionDefinition('seed-nonce-revelation-notifications', seedNonceRevelationAdded),
            new SubscriptionDefinition('transaction-notifications', transactionAdded),
            new SubscriptionDefinition('transaction-notifications-filtered', transactionAdded, transactionFilter),
        ];

        createSubscriptions(subscriptionDefinitions, apolloClient);

        // Let the app with workers start before simulating new monitor block header chunk
        await delay(500);

        // Tezos monitor is listening to passed stream, waiting for chunk push
        monitorResponseStream.push(JSON.stringify(getTestData('monitor-block-header.json')));

        /*
         * Wait for tezos monitor to receive head notification chunk and request block data, nock simulates request/response network delays,
         * Let apollo server push all notifications to subscribers
         */
        await delay(1000);

        // Assert proper notifications are received
        for (const definition of subscriptionDefinitions) {
            expect(definition.notifications).have.ordered.members(definition.expectedNotifications, definition.notificationName);
        }

        monitorResponseStream.end();
        monitorResponseStream.destroy();

        logger.info('Unsubscribing');
        subscriptionDefinitions.forEach(d => d.subscription?.unsubscribe());
    });

    afterEach(async () => {
        logger.info('Stopping client');
        apolloClient.stop();
        wsClient.unsubscribeAll();
        wsClient.close();

        await app.stop();
        cleanupRpcNock();
    });
});

function createSubscriptions(definitions: SubscriptionDefinition[], apolloClient: ApolloClient<NormalizedCacheObject>) {
    for (const definition of definitions) {
        definition.subscription = apolloClient
            .subscribe({
                query: definition.query,
                variables: definition.filter ?? {},
            })
            .subscribe({
                next: d => definition.notifications.push(JSON.stringify(d)),
            });
    }
}

class SubscriptionDefinition {
    expectedNotifications: string[];
    notifications: string[] = [];
    subscription?: ZenObservable.Subscription;

    constructor(
        public notificationName: string,
        public query: DocumentNode,
        public filter?: Record<string, unknown>,
    ) {
        this.expectedNotifications = <any[]>getTestData(`expected-${notificationName}.json`).map((e: any) => JSON.stringify(e));
    }
}

async function delay(millis: number): Promise<void> {
    return new Promise(resolve => {
        setTimeout(resolve, millis);
    });
}

function setupRpcNock(rpcUrl: string, mapings: { resource: string; response: any; delayConnection: number; delayBody: number }[]): void {
    mapings.forEach(mapping => {
        nock(rpcUrl)
            .persist()
            .get(mapping.resource)
            .delayConnection(mapping.delayConnection)
            .delayBody(mapping.delayBody)
            .reply(200, mapping.response);
    });
}

function cleanupRpcNock(): void {
    nock.abortPendingRequests();
    nock.cleanAll();
}

function getTestData(filename: string): any {
    return JSON.parse(fs.readFileSync(`./tests/integration/test_data/${filename}`).toString());
}
