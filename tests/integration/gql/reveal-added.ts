import { gql } from 'apollo-server';

export const revealAdded = gql`
subscription ($source: AddressFilter, $status: OperationResultStatusFilter) {
  revealAdded(source: $source, status: $status) {
    kind,
    source,
    fee,
    counter,
    gas_limit,
    storage_limit,
    public_key,
    metadata {
      balance_updates {
        kind,
        delegate
      },
      internal_operation_results {
        kind,
        source,
        nonce,
        amount,
        destination
      },
      operation_result {
        status,
        consumed_gas,
        errors {
          kind,
          id
        }
      }
    }
  }
}
`;
