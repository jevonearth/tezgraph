import 'reflect-metadata';
import { ApolloServer } from 'apollo-server-express';
import { gql } from 'apollo-server';
import { createTestClient } from 'apollo-server-testing';
import { AccountResolver } from '../../../src/resolvers/account';
import { buildSchema } from 'type-graphql';

async function startTestServer() {
    const testServer = new ApolloServer({
        schema: await buildSchema({
            resolvers: [AccountResolver],
        }),
        context: ({ req, res }) => ({ req, res }),
    });

    return testServer;
}

const server = startTestServer();

let testClientQuery: Function;

async function startTestClient() {
    if (!testClientQuery) {
        const { query } = createTestClient(await server);
        testClientQuery = await query;
    }
}

describe('Account Resolvers', () => {
    test('account.originations first', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ") {
                address
                first_seen
                activated
                public_key
                originations (first: 3) {
                    page_info {
                        start_cursor
                        end_cursor
                        has_next_page
                        has_previous_page
                    }
                    edges {
                        cursor
                        node {
                            hash
                            batch_position
                            source
                            kind
                            timestamp
                            level
                            block
                            counter
                            id
                            contract_address
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
                first_seen: '2018-06-30T23:21:42Z',
                activated: '2018-06-30T23:12:42.000Z',
                public_key: null,
                originations: {
                    page_info: {
                        start_cursor: 'ootZaX3yURSCmn5DsLBmnrykrsoBPJf8BSjR5qGP8e8jyxByewa:0',
                        end_cursor: 'opT3F2CJAV3SK37GaqVJxecyYCFexJr8o281DSCe4RKDz1pgRYo:0',
                        has_next_page: true,
                        has_previous_page: false,
                    },
                    edges: [
                        {
                            cursor: 'ootZaX3yURSCmn5DsLBmnrykrsoBPJf8BSjR5qGP8e8jyxByewa:0',
                            node: {
                                hash: 'ootZaX3yURSCmn5DsLBmnrykrsoBPJf8BSjR5qGP8e8jyxByewa',
                                batch_position: 0,
                                source: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
                                kind: 'origination',
                                timestamp: '2018-10-14T13:35:39Z',
                                level: 145207,
                                block: 'BLhCCqvKc6G53jqv1wPYRfAAqWmDQM1u3vAYj65Wg4UYyBVVq58',
                                counter: '6f00000000000000',
                                id: 2336496,
                                contract_address: 'KT1HNBaXPEVtXvfaEzD7eazHAKxcLwAyzu3m',
                            },
                        },
                        {
                            cursor: 'onwcWSuwQW8vM1avsdicvZturwNK3mHggx3Sv4xW3fYMzS2uy1s:0',
                            node: {
                                hash: 'onwcWSuwQW8vM1avsdicvZturwNK3mHggx3Sv4xW3fYMzS2uy1s',
                                batch_position: 0,
                                source: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
                                kind: 'origination',
                                timestamp: '2018-10-14T13:27:39Z',
                                level: 145199,
                                block: 'BKnUdg9QPQcDnTEddCaQtDkE5gwCLhK78b4gy7nn7xsKKcq8Eok',
                                counter: '6e00000000000000',
                                id: 2336315,
                                contract_address: 'KT1UPypHsky3PFeMLhUhCts7bTWxYhLRDfGR',
                            },
                        },
                        {
                            cursor: 'opT3F2CJAV3SK37GaqVJxecyYCFexJr8o281DSCe4RKDz1pgRYo:0',
                            node: {
                                hash: 'opT3F2CJAV3SK37GaqVJxecyYCFexJr8o281DSCe4RKDz1pgRYo',
                                batch_position: 0,
                                source: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
                                kind: 'origination',
                                timestamp: '2018-08-02T11:20:38Z',
                                level: 44608,
                                block: 'BL797ZgqpSoXjz5FWKSYS9kuswn17YRfsY2Dj6DLF1br9485ymR',
                                counter: '6d00000000000000',
                                id: 370138,
                                contract_address: 'KT1NKkc3dfoqqwd2HrrDaqihFbjcZsNFwu6H',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.originations first and before', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ") {
                address
                first_seen
                activated
                public_key
                originations (
                  first: 3
                  before: "opT3F2CJAV3SK37GaqVJxecyYCFexJr8o281DSCe4RKDz1pgRYo:0"
                ) {
                    page_info {
                        start_cursor
                        end_cursor
                        has_next_page
                        has_previous_page
                    }
                    edges {
                        cursor
                        node {
                            hash
                            batch_position
                            source
                            kind
                            timestamp
                            level
                            block
                            counter
                            id
                            contract_address
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
                first_seen: '2018-06-30T23:21:42Z',
                activated: '2018-06-30T23:12:42.000Z',
                public_key: null,
                originations: {
                    page_info: {
                        start_cursor: 'ootZaX3yURSCmn5DsLBmnrykrsoBPJf8BSjR5qGP8e8jyxByewa:0',
                        end_cursor: 'onwcWSuwQW8vM1avsdicvZturwNK3mHggx3Sv4xW3fYMzS2uy1s:0',
                        has_next_page: true,
                        has_previous_page: false,
                    },
                    edges: [
                        {
                            cursor: 'ootZaX3yURSCmn5DsLBmnrykrsoBPJf8BSjR5qGP8e8jyxByewa:0',
                            node: {
                                hash: 'ootZaX3yURSCmn5DsLBmnrykrsoBPJf8BSjR5qGP8e8jyxByewa',
                                batch_position: 0,
                                source: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
                                kind: 'origination',
                                timestamp: '2018-10-14T13:35:39Z',
                                level: 145207,
                                block: 'BLhCCqvKc6G53jqv1wPYRfAAqWmDQM1u3vAYj65Wg4UYyBVVq58',
                                counter: '6f00000000000000',
                                id: 2336496,
                                contract_address: 'KT1HNBaXPEVtXvfaEzD7eazHAKxcLwAyzu3m',
                            },
                        },
                        {
                            cursor: 'onwcWSuwQW8vM1avsdicvZturwNK3mHggx3Sv4xW3fYMzS2uy1s:0',
                            node: {
                                hash: 'onwcWSuwQW8vM1avsdicvZturwNK3mHggx3Sv4xW3fYMzS2uy1s',
                                batch_position: 0,
                                source: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
                                kind: 'origination',
                                timestamp: '2018-10-14T13:27:39Z',
                                level: 145199,
                                block: 'BKnUdg9QPQcDnTEddCaQtDkE5gwCLhK78b4gy7nn7xsKKcq8Eok',
                                counter: '6e00000000000000',
                                id: 2336315,
                                contract_address: 'KT1UPypHsky3PFeMLhUhCts7bTWxYhLRDfGR',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.originations first and after', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ") {
                address
                first_seen
                activated
                public_key
                originations (
                  first: 3
                  after: "opT3F2CJAV3SK37GaqVJxecyYCFexJr8o281DSCe4RKDz1pgRYo:0"
                ) {
                    page_info {
                        start_cursor
                        end_cursor
                        has_next_page
                        has_previous_page
                    }
                    edges {
                        cursor
                        node {
                            hash
                            batch_position
                            source
                            kind
                            timestamp
                            level
                            block
                            counter
                            id
                            contract_address
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
                first_seen: '2018-06-30T23:21:42Z',
                activated: '2018-06-30T23:12:42.000Z',
                public_key: null,
                originations: {
                    page_info: {
                        start_cursor: 'opLxWezEdNaPLnhgWXrtXsiiFoV775ej6mszHXP2XJ67ouAfvXV:0',
                        end_cursor: 'oox4Qe41cPUpYR7BBcnTLMbdW15U9fPfsbBwVhNt4f1es5CX51B:0',
                        has_next_page: false,
                        has_previous_page: true,
                    },
                    edges: [
                        {
                            cursor: 'opLxWezEdNaPLnhgWXrtXsiiFoV775ej6mszHXP2XJ67ouAfvXV:0',
                            node: {
                                hash: 'opLxWezEdNaPLnhgWXrtXsiiFoV775ej6mszHXP2XJ67ouAfvXV',
                                batch_position: 0,
                                source: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
                                kind: 'origination',
                                timestamp: '2018-07-01T02:12:42Z',
                                level: 510,
                                block: 'BLS7hzqeAn4U6NjoKtkkfehEpzbvzWNJfdTeMMq4bxj6wZKJ3xx',
                                counter: '6900000000000000',
                                id: 4207,
                                contract_address: 'KT1FN3e4qY1HGDXEW7eLq3YpHjFJPb4cd4hg',
                            },
                        },
                        {
                            cursor: 'opQR6u8WfvoaByFbcNxuK7YHzutnydCw2myNqv3zrR93SADdCVy:0',
                            node: {
                                hash: 'opQR6u8WfvoaByFbcNxuK7YHzutnydCw2myNqv3zrR93SADdCVy',
                                batch_position: 0,
                                source: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
                                kind: 'origination',
                                timestamp: '2018-06-30T23:46:42Z',
                                level: 364,
                                block: 'BLorWos9ACtkS1zoQ8Xxb5dNTDQhV7z93D9fDm4LnP31ZCPWrsA',
                                counter: '6700000000000000',
                                id: 2941,
                                contract_address: 'KT1ASvdKyRYzQuzkn2Qpb8MChjqbSTimTJxH',
                            },
                        },
                        {
                            cursor: 'oox4Qe41cPUpYR7BBcnTLMbdW15U9fPfsbBwVhNt4f1es5CX51B:0',
                            node: {
                                hash: 'oox4Qe41cPUpYR7BBcnTLMbdW15U9fPfsbBwVhNt4f1es5CX51B',
                                batch_position: 0,
                                source: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
                                kind: 'origination',
                                timestamp: '2018-06-30T23:27:42Z',
                                level: 345,
                                block: 'BLgogqQQPHmNqRxitFd7bvkKpQhZUDBTTTf5bZFMQynaHpyWwuj',
                                counter: '6500000000000000',
                                id: 2781,
                                contract_address: 'KT1CSMoRUcxrspTxRKpR6afvHXNN7NckWQpn',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.originations first and order_by', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ") {
                address
                first_seen
                activated
                public_key
                originations (
                  first: 3
                  order_by: {field: ID, direction: ASC}
                ) {
                    page_info {
                        start_cursor
                        end_cursor
                        has_next_page
                        has_previous_page
                    }
                    edges {
                        cursor
                        node {
                            hash
                            batch_position
                            source
                            kind
                            timestamp
                            level
                            block
                            counter
                            id
                            contract_address
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
                first_seen: '2018-06-30T23:21:42Z',
                activated: '2018-06-30T23:12:42.000Z',
                public_key: null,
                originations: {
                    page_info: {
                        start_cursor: 'oox4Qe41cPUpYR7BBcnTLMbdW15U9fPfsbBwVhNt4f1es5CX51B:0',
                        end_cursor: 'opLxWezEdNaPLnhgWXrtXsiiFoV775ej6mszHXP2XJ67ouAfvXV:0',
                        has_next_page: true,
                        has_previous_page: false,
                    },
                    edges: [
                        {
                            cursor: 'oox4Qe41cPUpYR7BBcnTLMbdW15U9fPfsbBwVhNt4f1es5CX51B:0',
                            node: {
                                hash: 'oox4Qe41cPUpYR7BBcnTLMbdW15U9fPfsbBwVhNt4f1es5CX51B',
                                batch_position: 0,
                                source: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
                                kind: 'origination',
                                timestamp: '2018-06-30T23:27:42Z',
                                level: 345,
                                block: 'BLgogqQQPHmNqRxitFd7bvkKpQhZUDBTTTf5bZFMQynaHpyWwuj',
                                counter: '6500000000000000',
                                id: 2781,
                                contract_address: 'KT1CSMoRUcxrspTxRKpR6afvHXNN7NckWQpn',
                            },
                        },
                        {
                            cursor: 'opQR6u8WfvoaByFbcNxuK7YHzutnydCw2myNqv3zrR93SADdCVy:0',
                            node: {
                                hash: 'opQR6u8WfvoaByFbcNxuK7YHzutnydCw2myNqv3zrR93SADdCVy',
                                batch_position: 0,
                                source: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
                                kind: 'origination',
                                timestamp: '2018-06-30T23:46:42Z',
                                level: 364,
                                block: 'BLorWos9ACtkS1zoQ8Xxb5dNTDQhV7z93D9fDm4LnP31ZCPWrsA',
                                counter: '6700000000000000',
                                id: 2941,
                                contract_address: 'KT1ASvdKyRYzQuzkn2Qpb8MChjqbSTimTJxH',
                            },
                        },
                        {
                            cursor: 'opLxWezEdNaPLnhgWXrtXsiiFoV775ej6mszHXP2XJ67ouAfvXV:0',
                            node: {
                                hash: 'opLxWezEdNaPLnhgWXrtXsiiFoV775ej6mszHXP2XJ67ouAfvXV',
                                batch_position: 0,
                                source: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
                                kind: 'origination',
                                timestamp: '2018-07-01T02:12:42Z',
                                level: 510,
                                block: 'BLS7hzqeAn4U6NjoKtkkfehEpzbvzWNJfdTeMMq4bxj6wZKJ3xx',
                                counter: '6900000000000000',
                                id: 4207,
                                contract_address: 'KT1FN3e4qY1HGDXEW7eLq3YpHjFJPb4cd4hg',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.originations first and order_by and before', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ") {
                address
                first_seen
                activated
                public_key
                originations (
                  first: 3
                  order_by: {field: ID, direction: ASC}
                  before: "opT3F2CJAV3SK37GaqVJxecyYCFexJr8o281DSCe4RKDz1pgRYo:0"
                ) {
                    page_info {
                        start_cursor
                        end_cursor
                        has_next_page
                        has_previous_page
                    }
                    edges {
                        cursor
                        node {
                            hash
                            batch_position
                            source
                            kind
                            timestamp
                            level
                            block
                            counter
                            id
                            contract_address
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
                first_seen: '2018-06-30T23:21:42Z',
                activated: '2018-06-30T23:12:42.000Z',
                public_key: null,
                originations: {
                    page_info: {
                        start_cursor: 'oox4Qe41cPUpYR7BBcnTLMbdW15U9fPfsbBwVhNt4f1es5CX51B:0',
                        end_cursor: 'opLxWezEdNaPLnhgWXrtXsiiFoV775ej6mszHXP2XJ67ouAfvXV:0',
                        has_next_page: true,
                        has_previous_page: false,
                    },
                    edges: [
                        {
                            cursor: 'oox4Qe41cPUpYR7BBcnTLMbdW15U9fPfsbBwVhNt4f1es5CX51B:0',
                            node: {
                                hash: 'oox4Qe41cPUpYR7BBcnTLMbdW15U9fPfsbBwVhNt4f1es5CX51B',
                                batch_position: 0,
                                source: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
                                kind: 'origination',
                                timestamp: '2018-06-30T23:27:42Z',
                                level: 345,
                                block: 'BLgogqQQPHmNqRxitFd7bvkKpQhZUDBTTTf5bZFMQynaHpyWwuj',
                                counter: '6500000000000000',
                                id: 2781,
                                contract_address: 'KT1CSMoRUcxrspTxRKpR6afvHXNN7NckWQpn',
                            },
                        },
                        {
                            cursor: 'opQR6u8WfvoaByFbcNxuK7YHzutnydCw2myNqv3zrR93SADdCVy:0',
                            node: {
                                hash: 'opQR6u8WfvoaByFbcNxuK7YHzutnydCw2myNqv3zrR93SADdCVy',
                                batch_position: 0,
                                source: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
                                kind: 'origination',
                                timestamp: '2018-06-30T23:46:42Z',
                                level: 364,
                                block: 'BLorWos9ACtkS1zoQ8Xxb5dNTDQhV7z93D9fDm4LnP31ZCPWrsA',
                                counter: '6700000000000000',
                                id: 2941,
                                contract_address: 'KT1ASvdKyRYzQuzkn2Qpb8MChjqbSTimTJxH',
                            },
                        },
                        {
                            cursor: 'opLxWezEdNaPLnhgWXrtXsiiFoV775ej6mszHXP2XJ67ouAfvXV:0',
                            node: {
                                hash: 'opLxWezEdNaPLnhgWXrtXsiiFoV775ej6mszHXP2XJ67ouAfvXV',
                                batch_position: 0,
                                source: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
                                kind: 'origination',
                                timestamp: '2018-07-01T02:12:42Z',
                                level: 510,
                                block: 'BLS7hzqeAn4U6NjoKtkkfehEpzbvzWNJfdTeMMq4bxj6wZKJ3xx',
                                counter: '6900000000000000',
                                id: 4207,
                                contract_address: 'KT1FN3e4qY1HGDXEW7eLq3YpHjFJPb4cd4hg',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.originations first and order_by and after', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ") {
                address
                first_seen
                activated
                public_key
                originations (
                  first: 3
                  order_by: {field: ID, direction: ASC}
                  after: "opT3F2CJAV3SK37GaqVJxecyYCFexJr8o281DSCe4RKDz1pgRYo:0"
                ) {
                    page_info {
                        start_cursor
                        end_cursor
                        has_next_page
                        has_previous_page
                    }
                    edges {
                        cursor
                        node {
                            hash
                            batch_position
                            source
                            kind
                            timestamp
                            level
                            block
                            counter
                            id
                            contract_address
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
                first_seen: '2018-06-30T23:21:42Z',
                activated: '2018-06-30T23:12:42.000Z',
                public_key: null,
                originations: {
                    page_info: {
                        start_cursor: 'onwcWSuwQW8vM1avsdicvZturwNK3mHggx3Sv4xW3fYMzS2uy1s:0',
                        end_cursor: 'ootZaX3yURSCmn5DsLBmnrykrsoBPJf8BSjR5qGP8e8jyxByewa:0',
                        has_next_page: false,
                        has_previous_page: true,
                    },
                    edges: [
                        {
                            cursor: 'onwcWSuwQW8vM1avsdicvZturwNK3mHggx3Sv4xW3fYMzS2uy1s:0',
                            node: {
                                hash: 'onwcWSuwQW8vM1avsdicvZturwNK3mHggx3Sv4xW3fYMzS2uy1s',
                                batch_position: 0,
                                source: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
                                kind: 'origination',
                                timestamp: '2018-10-14T13:27:39Z',
                                level: 145199,
                                block: 'BKnUdg9QPQcDnTEddCaQtDkE5gwCLhK78b4gy7nn7xsKKcq8Eok',
                                counter: '6e00000000000000',
                                id: 2336315,
                                contract_address: 'KT1UPypHsky3PFeMLhUhCts7bTWxYhLRDfGR',
                            },
                        },
                        {
                            cursor: 'ootZaX3yURSCmn5DsLBmnrykrsoBPJf8BSjR5qGP8e8jyxByewa:0',
                            node: {
                                hash: 'ootZaX3yURSCmn5DsLBmnrykrsoBPJf8BSjR5qGP8e8jyxByewa',
                                batch_position: 0,
                                source: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
                                kind: 'origination',
                                timestamp: '2018-10-14T13:35:39Z',
                                level: 145207,
                                block: 'BLhCCqvKc6G53jqv1wPYRfAAqWmDQM1u3vAYj65Wg4UYyBVVq58',
                                counter: '6f00000000000000',
                                id: 2336496,
                                contract_address: 'KT1HNBaXPEVtXvfaEzD7eazHAKxcLwAyzu3m',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.originations first and date_range gte', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ") {
                address
                first_seen
                activated
                public_key
                originations (
                  first: 3
                  date_range: {
                    gte: "2018-08-02T11:20:38Z"
                  }
                ) {
                    page_info {
                        start_cursor
                        end_cursor
                        has_next_page
                        has_previous_page
                    }
                    edges {
                        cursor
                        node {
                            hash
                            batch_position
                            source
                            kind
                            timestamp
                            level
                            block
                            counter
                            id
                            contract_address
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
                first_seen: '2018-06-30T23:21:42Z',
                activated: '2018-06-30T23:12:42.000Z',
                public_key: null,
                originations: {
                    page_info: {
                        start_cursor: 'ootZaX3yURSCmn5DsLBmnrykrsoBPJf8BSjR5qGP8e8jyxByewa:0',
                        end_cursor: 'opT3F2CJAV3SK37GaqVJxecyYCFexJr8o281DSCe4RKDz1pgRYo:0',
                        has_next_page: false,
                        has_previous_page: false,
                    },
                    edges: [
                        {
                            cursor: 'ootZaX3yURSCmn5DsLBmnrykrsoBPJf8BSjR5qGP8e8jyxByewa:0',
                            node: {
                                hash: 'ootZaX3yURSCmn5DsLBmnrykrsoBPJf8BSjR5qGP8e8jyxByewa',
                                batch_position: 0,
                                source: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
                                kind: 'origination',
                                timestamp: '2018-10-14T13:35:39Z',
                                level: 145207,
                                block: 'BLhCCqvKc6G53jqv1wPYRfAAqWmDQM1u3vAYj65Wg4UYyBVVq58',
                                counter: '6f00000000000000',
                                id: 2336496,
                                contract_address: 'KT1HNBaXPEVtXvfaEzD7eazHAKxcLwAyzu3m',
                            },
                        },
                        {
                            cursor: 'onwcWSuwQW8vM1avsdicvZturwNK3mHggx3Sv4xW3fYMzS2uy1s:0',
                            node: {
                                hash: 'onwcWSuwQW8vM1avsdicvZturwNK3mHggx3Sv4xW3fYMzS2uy1s',
                                batch_position: 0,
                                source: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
                                kind: 'origination',
                                timestamp: '2018-10-14T13:27:39Z',
                                level: 145199,
                                block: 'BKnUdg9QPQcDnTEddCaQtDkE5gwCLhK78b4gy7nn7xsKKcq8Eok',
                                counter: '6e00000000000000',
                                id: 2336315,
                                contract_address: 'KT1UPypHsky3PFeMLhUhCts7bTWxYhLRDfGR',
                            },
                        },
                        {
                            cursor: 'opT3F2CJAV3SK37GaqVJxecyYCFexJr8o281DSCe4RKDz1pgRYo:0',
                            node: {
                                hash: 'opT3F2CJAV3SK37GaqVJxecyYCFexJr8o281DSCe4RKDz1pgRYo',
                                batch_position: 0,
                                source: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
                                kind: 'origination',
                                timestamp: '2018-08-02T11:20:38Z',
                                level: 44608,
                                block: 'BL797ZgqpSoXjz5FWKSYS9kuswn17YRfsY2Dj6DLF1br9485ymR',
                                counter: '6d00000000000000',
                                id: 370138,
                                contract_address: 'KT1NKkc3dfoqqwd2HrrDaqihFbjcZsNFwu6H',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.originations first and date_range gte and before', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ") {
                address
                first_seen
                activated
                public_key
                originations (
                  first: 3
                  date_range: {
                    gte: "2018-08-02T11:20:38Z"
                  }
                  before: "onwcWSuwQW8vM1avsdicvZturwNK3mHggx3Sv4xW3fYMzS2uy1s:0"
                ) {
                    page_info {
                        start_cursor
                        end_cursor
                        has_next_page
                        has_previous_page
                    }
                    edges {
                        cursor
                        node {
                            hash
                            batch_position
                            source
                            kind
                            timestamp
                            level
                            block
                            counter
                            id
                            contract_address
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
                first_seen: '2018-06-30T23:21:42Z',
                activated: '2018-06-30T23:12:42.000Z',
                public_key: null,
                originations: {
                    page_info: {
                        start_cursor: 'ootZaX3yURSCmn5DsLBmnrykrsoBPJf8BSjR5qGP8e8jyxByewa:0',
                        end_cursor: 'ootZaX3yURSCmn5DsLBmnrykrsoBPJf8BSjR5qGP8e8jyxByewa:0',
                        has_next_page: true,
                        has_previous_page: false,
                    },
                    edges: [
                        {
                            cursor: 'ootZaX3yURSCmn5DsLBmnrykrsoBPJf8BSjR5qGP8e8jyxByewa:0',
                            node: {
                                hash: 'ootZaX3yURSCmn5DsLBmnrykrsoBPJf8BSjR5qGP8e8jyxByewa',
                                batch_position: 0,
                                source: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
                                kind: 'origination',
                                timestamp: '2018-10-14T13:35:39Z',
                                level: 145207,
                                block: 'BLhCCqvKc6G53jqv1wPYRfAAqWmDQM1u3vAYj65Wg4UYyBVVq58',
                                counter: '6f00000000000000',
                                id: 2336496,
                                contract_address: 'KT1HNBaXPEVtXvfaEzD7eazHAKxcLwAyzu3m',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.originations first and date_range gte and after', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ") {
                address
                first_seen
                activated
                public_key
                originations (
                  first: 3
                  date_range: {
                    gte: "2018-08-02T11:20:38Z"
                  }
                  after: "onwcWSuwQW8vM1avsdicvZturwNK3mHggx3Sv4xW3fYMzS2uy1s:0"
                ) {
                    page_info {
                        start_cursor
                        end_cursor
                        has_next_page
                        has_previous_page
                    }
                    edges {
                        cursor
                        node {
                            hash
                            batch_position
                            source
                            kind
                            timestamp
                            level
                            block
                            counter
                            id
                            contract_address
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
                first_seen: '2018-06-30T23:21:42Z',
                activated: '2018-06-30T23:12:42.000Z',
                public_key: null,
                originations: {
                    page_info: {
                        start_cursor: 'opT3F2CJAV3SK37GaqVJxecyYCFexJr8o281DSCe4RKDz1pgRYo:0',
                        end_cursor: 'opT3F2CJAV3SK37GaqVJxecyYCFexJr8o281DSCe4RKDz1pgRYo:0',
                        has_next_page: false,
                        has_previous_page: true,
                    },
                    edges: [
                        {
                            cursor: 'opT3F2CJAV3SK37GaqVJxecyYCFexJr8o281DSCe4RKDz1pgRYo:0',
                            node: {
                                hash: 'opT3F2CJAV3SK37GaqVJxecyYCFexJr8o281DSCe4RKDz1pgRYo',
                                batch_position: 0,
                                source: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
                                kind: 'origination',
                                timestamp: '2018-08-02T11:20:38Z',
                                level: 44608,
                                block: 'BL797ZgqpSoXjz5FWKSYS9kuswn17YRfsY2Dj6DLF1br9485ymR',
                                counter: '6d00000000000000',
                                id: 370138,
                                contract_address: 'KT1NKkc3dfoqqwd2HrrDaqihFbjcZsNFwu6H',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.originations first and date_range gte and order_by', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ") {
                address
                first_seen
                activated
                public_key
                originations (
                    first: 3
                    date_range: {
                    gte: "2018-08-02T11:20:38Z"
                    }
                    order_by: {field: ID, direction: ASC}
                ) {
                    page_info {
                        start_cursor
                        end_cursor
                        has_next_page
                        has_previous_page
                    }
                    edges {
                        cursor
                        node {
                            hash
                            batch_position
                            source
                            kind
                            timestamp
                            level
                            block
                            counter
                            id
                            contract_address
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
                first_seen: '2018-06-30T23:21:42Z',
                activated: '2018-06-30T23:12:42.000Z',
                public_key: null,
                originations: {
                    page_info: {
                        start_cursor: 'opT3F2CJAV3SK37GaqVJxecyYCFexJr8o281DSCe4RKDz1pgRYo:0',
                        end_cursor: 'ootZaX3yURSCmn5DsLBmnrykrsoBPJf8BSjR5qGP8e8jyxByewa:0',
                        has_next_page: false,
                        has_previous_page: false,
                    },
                    edges: [
                        {
                            cursor: 'opT3F2CJAV3SK37GaqVJxecyYCFexJr8o281DSCe4RKDz1pgRYo:0',
                            node: {
                                hash: 'opT3F2CJAV3SK37GaqVJxecyYCFexJr8o281DSCe4RKDz1pgRYo',
                                batch_position: 0,
                                source: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
                                kind: 'origination',
                                timestamp: '2018-08-02T11:20:38Z',
                                level: 44608,
                                block: 'BL797ZgqpSoXjz5FWKSYS9kuswn17YRfsY2Dj6DLF1br9485ymR',
                                counter: '6d00000000000000',
                                id: 370138,
                                contract_address: 'KT1NKkc3dfoqqwd2HrrDaqihFbjcZsNFwu6H',
                            },
                        },
                        {
                            cursor: 'onwcWSuwQW8vM1avsdicvZturwNK3mHggx3Sv4xW3fYMzS2uy1s:0',
                            node: {
                                hash: 'onwcWSuwQW8vM1avsdicvZturwNK3mHggx3Sv4xW3fYMzS2uy1s',
                                batch_position: 0,
                                source: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
                                kind: 'origination',
                                timestamp: '2018-10-14T13:27:39Z',
                                level: 145199,
                                block: 'BKnUdg9QPQcDnTEddCaQtDkE5gwCLhK78b4gy7nn7xsKKcq8Eok',
                                counter: '6e00000000000000',
                                id: 2336315,
                                contract_address: 'KT1UPypHsky3PFeMLhUhCts7bTWxYhLRDfGR',
                            },
                        },
                        {
                            cursor: 'ootZaX3yURSCmn5DsLBmnrykrsoBPJf8BSjR5qGP8e8jyxByewa:0',
                            node: {
                                hash: 'ootZaX3yURSCmn5DsLBmnrykrsoBPJf8BSjR5qGP8e8jyxByewa',
                                batch_position: 0,
                                source: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
                                kind: 'origination',
                                timestamp: '2018-10-14T13:35:39Z',
                                level: 145207,
                                block: 'BLhCCqvKc6G53jqv1wPYRfAAqWmDQM1u3vAYj65Wg4UYyBVVq58',
                                counter: '6f00000000000000',
                                id: 2336496,
                                contract_address: 'KT1HNBaXPEVtXvfaEzD7eazHAKxcLwAyzu3m',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.originations first and date_range lte', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ") {
                address
                first_seen
                activated
                public_key
                originations (
                  first: 3
                  date_range: {
                    lte: "2018-10-14T13:27:39Z"
                  }
                ) {
                    page_info {
                        start_cursor
                        end_cursor
                        has_next_page
                        has_previous_page
                    }
                    edges {
                        cursor
                        node {
                            hash
                            batch_position
                            source
                            kind
                            timestamp
                            level
                            block
                            counter
                            id
                            contract_address
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
                first_seen: '2018-06-30T23:21:42Z',
                activated: '2018-06-30T23:12:42.000Z',
                public_key: null,
                originations: {
                    page_info: {
                        start_cursor: 'onwcWSuwQW8vM1avsdicvZturwNK3mHggx3Sv4xW3fYMzS2uy1s:0',
                        end_cursor: 'opLxWezEdNaPLnhgWXrtXsiiFoV775ej6mszHXP2XJ67ouAfvXV:0',
                        has_next_page: true,
                        has_previous_page: false,
                    },
                    edges: [
                        {
                            cursor: 'onwcWSuwQW8vM1avsdicvZturwNK3mHggx3Sv4xW3fYMzS2uy1s:0',
                            node: {
                                hash: 'onwcWSuwQW8vM1avsdicvZturwNK3mHggx3Sv4xW3fYMzS2uy1s',
                                batch_position: 0,
                                source: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
                                kind: 'origination',
                                timestamp: '2018-10-14T13:27:39Z',
                                level: 145199,
                                block: 'BKnUdg9QPQcDnTEddCaQtDkE5gwCLhK78b4gy7nn7xsKKcq8Eok',
                                counter: '6e00000000000000',
                                id: 2336315,
                                contract_address: 'KT1UPypHsky3PFeMLhUhCts7bTWxYhLRDfGR',
                            },
                        },
                        {
                            cursor: 'opT3F2CJAV3SK37GaqVJxecyYCFexJr8o281DSCe4RKDz1pgRYo:0',
                            node: {
                                hash: 'opT3F2CJAV3SK37GaqVJxecyYCFexJr8o281DSCe4RKDz1pgRYo',
                                batch_position: 0,
                                source: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
                                kind: 'origination',
                                timestamp: '2018-08-02T11:20:38Z',
                                level: 44608,
                                block: 'BL797ZgqpSoXjz5FWKSYS9kuswn17YRfsY2Dj6DLF1br9485ymR',
                                counter: '6d00000000000000',
                                id: 370138,
                                contract_address: 'KT1NKkc3dfoqqwd2HrrDaqihFbjcZsNFwu6H',
                            },
                        },
                        {
                            cursor: 'opLxWezEdNaPLnhgWXrtXsiiFoV775ej6mszHXP2XJ67ouAfvXV:0',
                            node: {
                                hash: 'opLxWezEdNaPLnhgWXrtXsiiFoV775ej6mszHXP2XJ67ouAfvXV',
                                batch_position: 0,
                                source: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
                                kind: 'origination',
                                timestamp: '2018-07-01T02:12:42Z',
                                level: 510,
                                block: 'BLS7hzqeAn4U6NjoKtkkfehEpzbvzWNJfdTeMMq4bxj6wZKJ3xx',
                                counter: '6900000000000000',
                                id: 4207,
                                contract_address: 'KT1FN3e4qY1HGDXEW7eLq3YpHjFJPb4cd4hg',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.originations first and date_range lte and before', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ") {
                address
                first_seen
                activated
                public_key
                originations (
                  first: 3
                  date_range: {
                    lte: "2018-10-14T13:27:39Z"
                  }
                  before: "opT3F2CJAV3SK37GaqVJxecyYCFexJr8o281DSCe4RKDz1pgRYo:0"
                ) {
                    page_info {
                        start_cursor
                        end_cursor
                        has_next_page
                        has_previous_page
                    }
                    edges {
                        cursor
                        node {
                            hash
                            batch_position
                            source
                            kind
                            timestamp
                            level
                            block
                            counter
                            id
                            contract_address
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
                first_seen: '2018-06-30T23:21:42Z',
                activated: '2018-06-30T23:12:42.000Z',
                public_key: null,
                originations: {
                    page_info: {
                        start_cursor: 'onwcWSuwQW8vM1avsdicvZturwNK3mHggx3Sv4xW3fYMzS2uy1s:0',
                        end_cursor: 'onwcWSuwQW8vM1avsdicvZturwNK3mHggx3Sv4xW3fYMzS2uy1s:0',
                        has_next_page: true,
                        has_previous_page: false,
                    },
                    edges: [
                        {
                            cursor: 'onwcWSuwQW8vM1avsdicvZturwNK3mHggx3Sv4xW3fYMzS2uy1s:0',
                            node: {
                                hash: 'onwcWSuwQW8vM1avsdicvZturwNK3mHggx3Sv4xW3fYMzS2uy1s',
                                batch_position: 0,
                                source: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
                                kind: 'origination',
                                timestamp: '2018-10-14T13:27:39Z',
                                level: 145199,
                                block: 'BKnUdg9QPQcDnTEddCaQtDkE5gwCLhK78b4gy7nn7xsKKcq8Eok',
                                counter: '6e00000000000000',
                                id: 2336315,
                                contract_address: 'KT1UPypHsky3PFeMLhUhCts7bTWxYhLRDfGR',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.originations first and date_range lte and after', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ") {
                address
                first_seen
                activated
                public_key
                originations (
                  first: 3
                  date_range: {
                    lte: "2018-10-14T13:27:39Z"
                  }
                  after: "opT3F2CJAV3SK37GaqVJxecyYCFexJr8o281DSCe4RKDz1pgRYo:0"
                ) {
                    page_info {
                        start_cursor
                        end_cursor
                        has_next_page
                        has_previous_page
                    }
                    edges {
                        cursor
                        node {
                            hash
                            batch_position
                            source
                            kind
                            timestamp
                            level
                            block
                            counter
                            id
                            contract_address
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
                first_seen: '2018-06-30T23:21:42Z',
                activated: '2018-06-30T23:12:42.000Z',
                public_key: null,
                originations: {
                    page_info: {
                        start_cursor: 'opLxWezEdNaPLnhgWXrtXsiiFoV775ej6mszHXP2XJ67ouAfvXV:0',
                        end_cursor: 'oox4Qe41cPUpYR7BBcnTLMbdW15U9fPfsbBwVhNt4f1es5CX51B:0',
                        has_next_page: false,
                        has_previous_page: true,
                    },
                    edges: [
                        {
                            cursor: 'opLxWezEdNaPLnhgWXrtXsiiFoV775ej6mszHXP2XJ67ouAfvXV:0',
                            node: {
                                hash: 'opLxWezEdNaPLnhgWXrtXsiiFoV775ej6mszHXP2XJ67ouAfvXV',
                                batch_position: 0,
                                source: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
                                kind: 'origination',
                                timestamp: '2018-07-01T02:12:42Z',
                                level: 510,
                                block: 'BLS7hzqeAn4U6NjoKtkkfehEpzbvzWNJfdTeMMq4bxj6wZKJ3xx',
                                counter: '6900000000000000',
                                id: 4207,
                                contract_address: 'KT1FN3e4qY1HGDXEW7eLq3YpHjFJPb4cd4hg',
                            },
                        },
                        {
                            cursor: 'opQR6u8WfvoaByFbcNxuK7YHzutnydCw2myNqv3zrR93SADdCVy:0',
                            node: {
                                hash: 'opQR6u8WfvoaByFbcNxuK7YHzutnydCw2myNqv3zrR93SADdCVy',
                                batch_position: 0,
                                source: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
                                kind: 'origination',
                                timestamp: '2018-06-30T23:46:42Z',
                                level: 364,
                                block: 'BLorWos9ACtkS1zoQ8Xxb5dNTDQhV7z93D9fDm4LnP31ZCPWrsA',
                                counter: '6700000000000000',
                                id: 2941,
                                contract_address: 'KT1ASvdKyRYzQuzkn2Qpb8MChjqbSTimTJxH',
                            },
                        },
                        {
                            cursor: 'oox4Qe41cPUpYR7BBcnTLMbdW15U9fPfsbBwVhNt4f1es5CX51B:0',
                            node: {
                                hash: 'oox4Qe41cPUpYR7BBcnTLMbdW15U9fPfsbBwVhNt4f1es5CX51B',
                                batch_position: 0,
                                source: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
                                kind: 'origination',
                                timestamp: '2018-06-30T23:27:42Z',
                                level: 345,
                                block: 'BLgogqQQPHmNqRxitFd7bvkKpQhZUDBTTTf5bZFMQynaHpyWwuj',
                                counter: '6500000000000000',
                                id: 2781,
                                contract_address: 'KT1CSMoRUcxrspTxRKpR6afvHXNN7NckWQpn',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.originations first and date_range lte and order_by', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ") {
                address
                first_seen
                activated
                public_key
                originations (
                    first: 3
                    date_range: {
                    lte: "2018-10-14T13:27:39Z"
                    }
                    order_by: {field: ID, direction: ASC}
                ) {
                    page_info {
                        start_cursor
                        end_cursor
                        has_next_page
                        has_previous_page
                    }
                    edges {
                        cursor
                        node {
                            hash
                            batch_position
                            source
                            kind
                            timestamp
                            level
                            block
                            counter
                            id
                            contract_address
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
                first_seen: '2018-06-30T23:21:42Z',
                activated: '2018-06-30T23:12:42.000Z',
                public_key: null,
                originations: {
                    page_info: {
                        start_cursor: 'oox4Qe41cPUpYR7BBcnTLMbdW15U9fPfsbBwVhNt4f1es5CX51B:0',
                        end_cursor: 'opLxWezEdNaPLnhgWXrtXsiiFoV775ej6mszHXP2XJ67ouAfvXV:0',
                        has_next_page: true,
                        has_previous_page: false,
                    },
                    edges: [
                        {
                            cursor: 'oox4Qe41cPUpYR7BBcnTLMbdW15U9fPfsbBwVhNt4f1es5CX51B:0',
                            node: {
                                hash: 'oox4Qe41cPUpYR7BBcnTLMbdW15U9fPfsbBwVhNt4f1es5CX51B',
                                batch_position: 0,
                                source: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
                                kind: 'origination',
                                timestamp: '2018-06-30T23:27:42Z',
                                level: 345,
                                block: 'BLgogqQQPHmNqRxitFd7bvkKpQhZUDBTTTf5bZFMQynaHpyWwuj',
                                counter: '6500000000000000',
                                id: 2781,
                                contract_address: 'KT1CSMoRUcxrspTxRKpR6afvHXNN7NckWQpn',
                            },
                        },
                        {
                            cursor: 'opQR6u8WfvoaByFbcNxuK7YHzutnydCw2myNqv3zrR93SADdCVy:0',
                            node: {
                                hash: 'opQR6u8WfvoaByFbcNxuK7YHzutnydCw2myNqv3zrR93SADdCVy',
                                batch_position: 0,
                                source: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
                                kind: 'origination',
                                timestamp: '2018-06-30T23:46:42Z',
                                level: 364,
                                block: 'BLorWos9ACtkS1zoQ8Xxb5dNTDQhV7z93D9fDm4LnP31ZCPWrsA',
                                counter: '6700000000000000',
                                id: 2941,
                                contract_address: 'KT1ASvdKyRYzQuzkn2Qpb8MChjqbSTimTJxH',
                            },
                        },
                        {
                            cursor: 'opLxWezEdNaPLnhgWXrtXsiiFoV775ej6mszHXP2XJ67ouAfvXV:0',
                            node: {
                                hash: 'opLxWezEdNaPLnhgWXrtXsiiFoV775ej6mszHXP2XJ67ouAfvXV',
                                batch_position: 0,
                                source: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
                                kind: 'origination',
                                timestamp: '2018-07-01T02:12:42Z',
                                level: 510,
                                block: 'BLS7hzqeAn4U6NjoKtkkfehEpzbvzWNJfdTeMMq4bxj6wZKJ3xx',
                                counter: '6900000000000000',
                                id: 4207,
                                contract_address: 'KT1FN3e4qY1HGDXEW7eLq3YpHjFJPb4cd4hg',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.originations first and date_range gte/lte', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ") {
                address
                first_seen
                activated
                public_key
                originations (
                  first: 3
                  date_range: {
                    gte: "2018-08-02T11:20:38Z"
                    lte: "2018-10-14T13:27:39Z"
                  }
                ) {
                    page_info {
                        start_cursor
                        end_cursor
                        has_next_page
                        has_previous_page
                    }
                    edges {
                        cursor
                        node {
                            hash
                            batch_position
                            source
                            kind
                            timestamp
                            level
                            block
                            counter
                            id
                            contract_address
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
                first_seen: '2018-06-30T23:21:42Z',
                activated: '2018-06-30T23:12:42.000Z',
                public_key: null,
                originations: {
                    page_info: {
                        start_cursor: 'onwcWSuwQW8vM1avsdicvZturwNK3mHggx3Sv4xW3fYMzS2uy1s:0',
                        end_cursor: 'opT3F2CJAV3SK37GaqVJxecyYCFexJr8o281DSCe4RKDz1pgRYo:0',
                        has_next_page: false,
                        has_previous_page: false,
                    },
                    edges: [
                        {
                            cursor: 'onwcWSuwQW8vM1avsdicvZturwNK3mHggx3Sv4xW3fYMzS2uy1s:0',
                            node: {
                                hash: 'onwcWSuwQW8vM1avsdicvZturwNK3mHggx3Sv4xW3fYMzS2uy1s',
                                batch_position: 0,
                                source: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
                                kind: 'origination',
                                timestamp: '2018-10-14T13:27:39Z',
                                level: 145199,
                                block: 'BKnUdg9QPQcDnTEddCaQtDkE5gwCLhK78b4gy7nn7xsKKcq8Eok',
                                counter: '6e00000000000000',
                                id: 2336315,
                                contract_address: 'KT1UPypHsky3PFeMLhUhCts7bTWxYhLRDfGR',
                            },
                        },
                        {
                            cursor: 'opT3F2CJAV3SK37GaqVJxecyYCFexJr8o281DSCe4RKDz1pgRYo:0',
                            node: {
                                hash: 'opT3F2CJAV3SK37GaqVJxecyYCFexJr8o281DSCe4RKDz1pgRYo',
                                batch_position: 0,
                                source: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
                                kind: 'origination',
                                timestamp: '2018-08-02T11:20:38Z',
                                level: 44608,
                                block: 'BL797ZgqpSoXjz5FWKSYS9kuswn17YRfsY2Dj6DLF1br9485ymR',
                                counter: '6d00000000000000',
                                id: 370138,
                                contract_address: 'KT1NKkc3dfoqqwd2HrrDaqihFbjcZsNFwu6H',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.originations first and date_range gte/lte and before', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ") {
                address
                first_seen
                activated
                public_key
                originations (
                  first: 3
                  date_range: {
                    gte: "2018-08-02T11:20:38Z"
                    lte: "2019-10-27T12:46:11Z"
                  }
                  before: "opT3F2CJAV3SK37GaqVJxecyYCFexJr8o281DSCe4RKDz1pgRYo:0"
                ) {
                    page_info {
                        start_cursor
                        end_cursor
                        has_next_page
                        has_previous_page
                    }
                    edges {
                        cursor
                        node {
                            hash
                            batch_position
                            source
                            kind
                            timestamp
                            level
                            block
                            counter
                            id
                            contract_address
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
                first_seen: '2018-06-30T23:21:42Z',
                activated: '2018-06-30T23:12:42.000Z',
                public_key: null,
                originations: {
                    page_info: {
                        start_cursor: 'ootZaX3yURSCmn5DsLBmnrykrsoBPJf8BSjR5qGP8e8jyxByewa:0',
                        end_cursor: 'onwcWSuwQW8vM1avsdicvZturwNK3mHggx3Sv4xW3fYMzS2uy1s:0',
                        has_next_page: true,
                        has_previous_page: false,
                    },
                    edges: [
                        {
                            cursor: 'ootZaX3yURSCmn5DsLBmnrykrsoBPJf8BSjR5qGP8e8jyxByewa:0',
                            node: {
                                hash: 'ootZaX3yURSCmn5DsLBmnrykrsoBPJf8BSjR5qGP8e8jyxByewa',
                                batch_position: 0,
                                source: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
                                kind: 'origination',
                                timestamp: '2018-10-14T13:35:39Z',
                                level: 145207,
                                block: 'BLhCCqvKc6G53jqv1wPYRfAAqWmDQM1u3vAYj65Wg4UYyBVVq58',
                                counter: '6f00000000000000',
                                id: 2336496,
                                contract_address: 'KT1HNBaXPEVtXvfaEzD7eazHAKxcLwAyzu3m',
                            },
                        },
                        {
                            cursor: 'onwcWSuwQW8vM1avsdicvZturwNK3mHggx3Sv4xW3fYMzS2uy1s:0',
                            node: {
                                hash: 'onwcWSuwQW8vM1avsdicvZturwNK3mHggx3Sv4xW3fYMzS2uy1s',
                                batch_position: 0,
                                source: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
                                kind: 'origination',
                                timestamp: '2018-10-14T13:27:39Z',
                                level: 145199,
                                block: 'BKnUdg9QPQcDnTEddCaQtDkE5gwCLhK78b4gy7nn7xsKKcq8Eok',
                                counter: '6e00000000000000',
                                id: 2336315,
                                contract_address: 'KT1UPypHsky3PFeMLhUhCts7bTWxYhLRDfGR',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.originations first and date_range gte/lte and after', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ") {
                address
                first_seen
                activated
                public_key
                originations (
                  first: 3
                  date_range: {
                    gte: "2018-06-30T23:27:42Z"
                    lte: "2019-10-27T12:46:11Z"
                  }
                  after: "opLxWezEdNaPLnhgWXrtXsiiFoV775ej6mszHXP2XJ67ouAfvXV:0"
                ) {
                    page_info {
                        start_cursor
                        end_cursor
                        has_next_page
                        has_previous_page
                    }
                    edges {
                        cursor
                        node {
                            hash
                            batch_position
                            source
                            kind
                            timestamp
                            level
                            block
                            counter
                            id
                            contract_address
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
                first_seen: '2018-06-30T23:21:42Z',
                activated: '2018-06-30T23:12:42.000Z',
                public_key: null,
                originations: {
                    page_info: {
                        start_cursor: 'opQR6u8WfvoaByFbcNxuK7YHzutnydCw2myNqv3zrR93SADdCVy:0',
                        end_cursor: 'oox4Qe41cPUpYR7BBcnTLMbdW15U9fPfsbBwVhNt4f1es5CX51B:0',
                        has_next_page: false,
                        has_previous_page: true,
                    },
                    edges: [
                        {
                            cursor: 'opQR6u8WfvoaByFbcNxuK7YHzutnydCw2myNqv3zrR93SADdCVy:0',
                            node: {
                                hash: 'opQR6u8WfvoaByFbcNxuK7YHzutnydCw2myNqv3zrR93SADdCVy',
                                batch_position: 0,
                                source: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
                                kind: 'origination',
                                timestamp: '2018-06-30T23:46:42Z',
                                level: 364,
                                block: 'BLorWos9ACtkS1zoQ8Xxb5dNTDQhV7z93D9fDm4LnP31ZCPWrsA',
                                counter: '6700000000000000',
                                id: 2941,
                                contract_address: 'KT1ASvdKyRYzQuzkn2Qpb8MChjqbSTimTJxH',
                            },
                        },
                        {
                            cursor: 'oox4Qe41cPUpYR7BBcnTLMbdW15U9fPfsbBwVhNt4f1es5CX51B:0',
                            node: {
                                hash: 'oox4Qe41cPUpYR7BBcnTLMbdW15U9fPfsbBwVhNt4f1es5CX51B',
                                batch_position: 0,
                                source: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
                                kind: 'origination',
                                timestamp: '2018-06-30T23:27:42Z',
                                level: 345,
                                block: 'BLgogqQQPHmNqRxitFd7bvkKpQhZUDBTTTf5bZFMQynaHpyWwuj',
                                counter: '6500000000000000',
                                id: 2781,
                                contract_address: 'KT1CSMoRUcxrspTxRKpR6afvHXNN7NckWQpn',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.originations first and date_range gte/lte and order_by', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ") {
                address
                first_seen
                activated
                public_key
                originations (
                    first: 3
                    date_range: {
                    gte: "2018-08-02T11:20:38Z"
                    lte: "2019-10-27T12:46:11Z"
                    }
                    order_by: {field: ID, direction: ASC}
                ) {
                    page_info {
                        start_cursor
                        end_cursor
                        has_next_page
                        has_previous_page
                    }
                    edges {
                        cursor
                        node {
                            hash
                            batch_position
                            source
                            kind
                            timestamp
                            level
                            block
                            counter
                            id
                            contract_address
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
                first_seen: '2018-06-30T23:21:42Z',
                activated: '2018-06-30T23:12:42.000Z',
                public_key: null,
                originations: {
                    page_info: {
                        start_cursor: 'opT3F2CJAV3SK37GaqVJxecyYCFexJr8o281DSCe4RKDz1pgRYo:0',
                        end_cursor: 'ootZaX3yURSCmn5DsLBmnrykrsoBPJf8BSjR5qGP8e8jyxByewa:0',
                        has_next_page: false,
                        has_previous_page: false,
                    },
                    edges: [
                        {
                            cursor: 'opT3F2CJAV3SK37GaqVJxecyYCFexJr8o281DSCe4RKDz1pgRYo:0',
                            node: {
                                hash: 'opT3F2CJAV3SK37GaqVJxecyYCFexJr8o281DSCe4RKDz1pgRYo',
                                batch_position: 0,
                                source: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
                                kind: 'origination',
                                timestamp: '2018-08-02T11:20:38Z',
                                level: 44608,
                                block: 'BL797ZgqpSoXjz5FWKSYS9kuswn17YRfsY2Dj6DLF1br9485ymR',
                                counter: '6d00000000000000',
                                id: 370138,
                                contract_address: 'KT1NKkc3dfoqqwd2HrrDaqihFbjcZsNFwu6H',
                            },
                        },
                        {
                            cursor: 'onwcWSuwQW8vM1avsdicvZturwNK3mHggx3Sv4xW3fYMzS2uy1s:0',
                            node: {
                                hash: 'onwcWSuwQW8vM1avsdicvZturwNK3mHggx3Sv4xW3fYMzS2uy1s',
                                batch_position: 0,
                                source: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
                                kind: 'origination',
                                timestamp: '2018-10-14T13:27:39Z',
                                level: 145199,
                                block: 'BKnUdg9QPQcDnTEddCaQtDkE5gwCLhK78b4gy7nn7xsKKcq8Eok',
                                counter: '6e00000000000000',
                                id: 2336315,
                                contract_address: 'KT1UPypHsky3PFeMLhUhCts7bTWxYhLRDfGR',
                            },
                        },
                        {
                            cursor: 'ootZaX3yURSCmn5DsLBmnrykrsoBPJf8BSjR5qGP8e8jyxByewa:0',
                            node: {
                                hash: 'ootZaX3yURSCmn5DsLBmnrykrsoBPJf8BSjR5qGP8e8jyxByewa',
                                batch_position: 0,
                                source: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
                                kind: 'origination',
                                timestamp: '2018-10-14T13:35:39Z',
                                level: 145207,
                                block: 'BLhCCqvKc6G53jqv1wPYRfAAqWmDQM1u3vAYj65Wg4UYyBVVq58',
                                counter: '6f00000000000000',
                                id: 2336496,
                                contract_address: 'KT1HNBaXPEVtXvfaEzD7eazHAKxcLwAyzu3m',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.originations first and date_range gte and order_by and before', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ") {
                address
                first_seen
                activated
                public_key
                originations (
                  first: 3
                  order_by: {field: ID, direction: ASC}
                  before: "opT3F2CJAV3SK37GaqVJxecyYCFexJr8o281DSCe4RKDz1pgRYo:0"
                  date_range: {
                    gte: "2018-06-30T23:46:42Z"
                  }
                ) {
                    page_info {
                        start_cursor
                        end_cursor
                        has_next_page
                        has_previous_page
                    }
                    edges {
                        cursor
                        node {
                            hash
                            batch_position
                            source
                            kind
                            timestamp
                            level
                            block
                            counter
                            id
                            contract_address
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
                first_seen: '2018-06-30T23:21:42Z',
                activated: '2018-06-30T23:12:42.000Z',
                public_key: null,
                originations: {
                    page_info: {
                        start_cursor: 'opQR6u8WfvoaByFbcNxuK7YHzutnydCw2myNqv3zrR93SADdCVy:0',
                        end_cursor: 'opLxWezEdNaPLnhgWXrtXsiiFoV775ej6mszHXP2XJ67ouAfvXV:0',
                        has_next_page: true,
                        has_previous_page: false,
                    },
                    edges: [
                        {
                            cursor: 'opQR6u8WfvoaByFbcNxuK7YHzutnydCw2myNqv3zrR93SADdCVy:0',
                            node: {
                                hash: 'opQR6u8WfvoaByFbcNxuK7YHzutnydCw2myNqv3zrR93SADdCVy',
                                batch_position: 0,
                                source: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
                                kind: 'origination',
                                timestamp: '2018-06-30T23:46:42Z',
                                level: 364,
                                block: 'BLorWos9ACtkS1zoQ8Xxb5dNTDQhV7z93D9fDm4LnP31ZCPWrsA',
                                counter: '6700000000000000',
                                id: 2941,
                                contract_address: 'KT1ASvdKyRYzQuzkn2Qpb8MChjqbSTimTJxH',
                            },
                        },
                        {
                            cursor: 'opLxWezEdNaPLnhgWXrtXsiiFoV775ej6mszHXP2XJ67ouAfvXV:0',
                            node: {
                                hash: 'opLxWezEdNaPLnhgWXrtXsiiFoV775ej6mszHXP2XJ67ouAfvXV',
                                batch_position: 0,
                                source: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
                                kind: 'origination',
                                timestamp: '2018-07-01T02:12:42Z',
                                level: 510,
                                block: 'BLS7hzqeAn4U6NjoKtkkfehEpzbvzWNJfdTeMMq4bxj6wZKJ3xx',
                                counter: '6900000000000000',
                                id: 4207,
                                contract_address: 'KT1FN3e4qY1HGDXEW7eLq3YpHjFJPb4cd4hg',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.originations first and date_range gte and order_by and after', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ") {
                address
                first_seen
                activated
                public_key
                originations (
                  first: 3
                  order_by: {field: ID, direction: ASC}
                  after: "onwcWSuwQW8vM1avsdicvZturwNK3mHggx3Sv4xW3fYMzS2uy1s:0"
                  date_range: {
                    gte: "2018-06-30T23:46:42Z"
                  }
                ) {
                    page_info {
                        start_cursor
                        end_cursor
                        has_next_page
                        has_previous_page
                    }
                    edges {
                        cursor
                        node {
                            hash
                            batch_position
                            source
                            kind
                            timestamp
                            level
                            block
                            counter
                            id
                            contract_address
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
                first_seen: '2018-06-30T23:21:42Z',
                activated: '2018-06-30T23:12:42.000Z',
                public_key: null,
                originations: {
                    page_info: {
                        start_cursor: 'ootZaX3yURSCmn5DsLBmnrykrsoBPJf8BSjR5qGP8e8jyxByewa:0',
                        end_cursor: 'ootZaX3yURSCmn5DsLBmnrykrsoBPJf8BSjR5qGP8e8jyxByewa:0',
                        has_next_page: false,
                        has_previous_page: true,
                    },
                    edges: [
                        {
                            cursor: 'ootZaX3yURSCmn5DsLBmnrykrsoBPJf8BSjR5qGP8e8jyxByewa:0',
                            node: {
                                hash: 'ootZaX3yURSCmn5DsLBmnrykrsoBPJf8BSjR5qGP8e8jyxByewa',
                                batch_position: 0,
                                source: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
                                kind: 'origination',
                                timestamp: '2018-10-14T13:35:39Z',
                                level: 145207,
                                block: 'BLhCCqvKc6G53jqv1wPYRfAAqWmDQM1u3vAYj65Wg4UYyBVVq58',
                                counter: '6f00000000000000',
                                id: 2336496,
                                contract_address: 'KT1HNBaXPEVtXvfaEzD7eazHAKxcLwAyzu3m',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.originations first and date_range lte and order_by and before', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ") {
                address
                first_seen
                activated
                public_key
                originations (
                  first: 3
                  order_by: {field: ID, direction: ASC}
                  before: "onwcWSuwQW8vM1avsdicvZturwNK3mHggx3Sv4xW3fYMzS2uy1s:0"
                  date_range: {
                    lte: "2019-10-27T12:46:11Z"
                  }
                ) {
                    page_info {
                        start_cursor
                        end_cursor
                        has_next_page
                        has_previous_page
                    }
                    edges {
                        cursor
                        node {
                            hash
                            batch_position
                            source
                            kind
                            timestamp
                            level
                            block
                            counter
                            id
                            contract_address
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
                first_seen: '2018-06-30T23:21:42Z',
                activated: '2018-06-30T23:12:42.000Z',
                public_key: null,
                originations: {
                    page_info: {
                        start_cursor: 'oox4Qe41cPUpYR7BBcnTLMbdW15U9fPfsbBwVhNt4f1es5CX51B:0',
                        end_cursor: 'opLxWezEdNaPLnhgWXrtXsiiFoV775ej6mszHXP2XJ67ouAfvXV:0',
                        has_next_page: true,
                        has_previous_page: false,
                    },
                    edges: [
                        {
                            cursor: 'oox4Qe41cPUpYR7BBcnTLMbdW15U9fPfsbBwVhNt4f1es5CX51B:0',
                            node: {
                                hash: 'oox4Qe41cPUpYR7BBcnTLMbdW15U9fPfsbBwVhNt4f1es5CX51B',
                                batch_position: 0,
                                source: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
                                kind: 'origination',
                                timestamp: '2018-06-30T23:27:42Z',
                                level: 345,
                                block: 'BLgogqQQPHmNqRxitFd7bvkKpQhZUDBTTTf5bZFMQynaHpyWwuj',
                                counter: '6500000000000000',
                                id: 2781,
                                contract_address: 'KT1CSMoRUcxrspTxRKpR6afvHXNN7NckWQpn',
                            },
                        },
                        {
                            cursor: 'opQR6u8WfvoaByFbcNxuK7YHzutnydCw2myNqv3zrR93SADdCVy:0',
                            node: {
                                hash: 'opQR6u8WfvoaByFbcNxuK7YHzutnydCw2myNqv3zrR93SADdCVy',
                                batch_position: 0,
                                source: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
                                kind: 'origination',
                                timestamp: '2018-06-30T23:46:42Z',
                                level: 364,
                                block: 'BLorWos9ACtkS1zoQ8Xxb5dNTDQhV7z93D9fDm4LnP31ZCPWrsA',
                                counter: '6700000000000000',
                                id: 2941,
                                contract_address: 'KT1ASvdKyRYzQuzkn2Qpb8MChjqbSTimTJxH',
                            },
                        },
                        {
                            cursor: 'opLxWezEdNaPLnhgWXrtXsiiFoV775ej6mszHXP2XJ67ouAfvXV:0',
                            node: {
                                hash: 'opLxWezEdNaPLnhgWXrtXsiiFoV775ej6mszHXP2XJ67ouAfvXV',
                                batch_position: 0,
                                source: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
                                kind: 'origination',
                                timestamp: '2018-07-01T02:12:42Z',
                                level: 510,
                                block: 'BLS7hzqeAn4U6NjoKtkkfehEpzbvzWNJfdTeMMq4bxj6wZKJ3xx',
                                counter: '6900000000000000',
                                id: 4207,
                                contract_address: 'KT1FN3e4qY1HGDXEW7eLq3YpHjFJPb4cd4hg',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.originations first and date_range lte and order_by and after', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ") {
                address
                first_seen
                activated
                public_key
                originations (
                  first: 3
                  order_by: {field: ID, direction: ASC}
                  after: "opT3F2CJAV3SK37GaqVJxecyYCFexJr8o281DSCe4RKDz1pgRYo:0"
                  date_range: {
                    lte: "2019-10-27T12:46:11Z"
                  }
                ) {
                    page_info {
                        start_cursor
                        end_cursor
                        has_next_page
                        has_previous_page
                    }
                    edges {
                        cursor
                        node {
                            hash
                            batch_position
                            source
                            kind
                            timestamp
                            level
                            block
                            counter
                            id
                            contract_address
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
                first_seen: '2018-06-30T23:21:42Z',
                activated: '2018-06-30T23:12:42.000Z',
                public_key: null,
                originations: {
                    page_info: {
                        start_cursor: 'onwcWSuwQW8vM1avsdicvZturwNK3mHggx3Sv4xW3fYMzS2uy1s:0',
                        end_cursor: 'ootZaX3yURSCmn5DsLBmnrykrsoBPJf8BSjR5qGP8e8jyxByewa:0',
                        has_next_page: false,
                        has_previous_page: true,
                    },
                    edges: [
                        {
                            cursor: 'onwcWSuwQW8vM1avsdicvZturwNK3mHggx3Sv4xW3fYMzS2uy1s:0',
                            node: {
                                hash: 'onwcWSuwQW8vM1avsdicvZturwNK3mHggx3Sv4xW3fYMzS2uy1s',
                                batch_position: 0,
                                source: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
                                kind: 'origination',
                                timestamp: '2018-10-14T13:27:39Z',
                                level: 145199,
                                block: 'BKnUdg9QPQcDnTEddCaQtDkE5gwCLhK78b4gy7nn7xsKKcq8Eok',
                                counter: '6e00000000000000',
                                id: 2336315,
                                contract_address: 'KT1UPypHsky3PFeMLhUhCts7bTWxYhLRDfGR',
                            },
                        },
                        {
                            cursor: 'ootZaX3yURSCmn5DsLBmnrykrsoBPJf8BSjR5qGP8e8jyxByewa:0',
                            node: {
                                hash: 'ootZaX3yURSCmn5DsLBmnrykrsoBPJf8BSjR5qGP8e8jyxByewa',
                                batch_position: 0,
                                source: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
                                kind: 'origination',
                                timestamp: '2018-10-14T13:35:39Z',
                                level: 145207,
                                block: 'BLhCCqvKc6G53jqv1wPYRfAAqWmDQM1u3vAYj65Wg4UYyBVVq58',
                                counter: '6f00000000000000',
                                id: 2336496,
                                contract_address: 'KT1HNBaXPEVtXvfaEzD7eazHAKxcLwAyzu3m',
                            },
                        },
                    ],
                },
            },
        }));
    });
    test('account.originations first and date_range gte/lte and order_by and before', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ") {
                address
                first_seen
                activated
                public_key
                originations (
                  first: 3
                  order_by: {field: ID, direction: ASC}
                  before: "onwcWSuwQW8vM1avsdicvZturwNK3mHggx3Sv4xW3fYMzS2uy1s:0"
                  date_range: {
                    gte: "2018-08-02T11:20:38Z"
                    lte: "2019-10-27T12:46:11Z"
                  }
                ) {
                    page_info {
                        start_cursor
                        end_cursor
                        has_next_page
                        has_previous_page
                    }
                    edges {
                        cursor
                        node {
                            hash
                            batch_position
                            source
                            kind
                            timestamp
                            level
                            block
                            counter
                            id
                            contract_address
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
                first_seen: '2018-06-30T23:21:42Z',
                activated: '2018-06-30T23:12:42.000Z',
                public_key: null,
                originations: {
                    page_info: {
                        start_cursor: 'opT3F2CJAV3SK37GaqVJxecyYCFexJr8o281DSCe4RKDz1pgRYo:0',
                        end_cursor: 'opT3F2CJAV3SK37GaqVJxecyYCFexJr8o281DSCe4RKDz1pgRYo:0',
                        has_next_page: true,
                        has_previous_page: false,
                    },
                    edges: [
                        {
                            cursor: 'opT3F2CJAV3SK37GaqVJxecyYCFexJr8o281DSCe4RKDz1pgRYo:0',
                            node: {
                                hash: 'opT3F2CJAV3SK37GaqVJxecyYCFexJr8o281DSCe4RKDz1pgRYo',
                                batch_position: 0,
                                source: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
                                kind: 'origination',
                                timestamp: '2018-08-02T11:20:38Z',
                                level: 44608,
                                block: 'BL797ZgqpSoXjz5FWKSYS9kuswn17YRfsY2Dj6DLF1br9485ymR',
                                counter: '6d00000000000000',
                                id: 370138,
                                contract_address: 'KT1NKkc3dfoqqwd2HrrDaqihFbjcZsNFwu6H',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.originations first and date_range gte/lte and order_by and after', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ") {
                address
                first_seen
                activated
                public_key
                originations (
                  first: 3
                  order_by: {field: ID, direction: ASC}
                  after: "opT3F2CJAV3SK37GaqVJxecyYCFexJr8o281DSCe4RKDz1pgRYo:0"
                  date_range: {
                    gte: "2018-08-02T11:20:38Z"
                    lte: "2019-10-27T12:46:11Z"
                  }
                ) {
                    page_info {
                        start_cursor
                        end_cursor
                        has_next_page
                        has_previous_page
                    }
                    edges {
                        cursor
                        node {
                            hash
                            batch_position
                            source
                            kind
                            timestamp
                            level
                            block
                            counter
                            id
                            contract_address
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
                first_seen: '2018-06-30T23:21:42Z',
                activated: '2018-06-30T23:12:42.000Z',
                public_key: null,
                originations: {
                    page_info: {
                        start_cursor: 'onwcWSuwQW8vM1avsdicvZturwNK3mHggx3Sv4xW3fYMzS2uy1s:0',
                        end_cursor: 'ootZaX3yURSCmn5DsLBmnrykrsoBPJf8BSjR5qGP8e8jyxByewa:0',
                        has_next_page: false,
                        has_previous_page: true,
                    },
                    edges: [
                        {
                            cursor: 'onwcWSuwQW8vM1avsdicvZturwNK3mHggx3Sv4xW3fYMzS2uy1s:0',
                            node: {
                                hash: 'onwcWSuwQW8vM1avsdicvZturwNK3mHggx3Sv4xW3fYMzS2uy1s',
                                batch_position: 0,
                                source: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
                                kind: 'origination',
                                timestamp: '2018-10-14T13:27:39Z',
                                level: 145199,
                                block: 'BKnUdg9QPQcDnTEddCaQtDkE5gwCLhK78b4gy7nn7xsKKcq8Eok',
                                counter: '6e00000000000000',
                                id: 2336315,
                                contract_address: 'KT1UPypHsky3PFeMLhUhCts7bTWxYhLRDfGR',
                            },
                        },
                        {
                            cursor: 'ootZaX3yURSCmn5DsLBmnrykrsoBPJf8BSjR5qGP8e8jyxByewa:0',
                            node: {
                                hash: 'ootZaX3yURSCmn5DsLBmnrykrsoBPJf8BSjR5qGP8e8jyxByewa',
                                batch_position: 0,
                                source: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
                                kind: 'origination',
                                timestamp: '2018-10-14T13:35:39Z',
                                level: 145207,
                                block: 'BLhCCqvKc6G53jqv1wPYRfAAqWmDQM1u3vAYj65Wg4UYyBVVq58',
                                counter: '6f00000000000000',
                                id: 2336496,
                                contract_address: 'KT1HNBaXPEVtXvfaEzD7eazHAKxcLwAyzu3m',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.originations last', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ") {
                address
                first_seen
                activated
                public_key
                originations (last: 3) {
                    page_info {
                        start_cursor
                        end_cursor
                        has_next_page
                        has_previous_page
                    }
                    edges {
                        cursor
                        node {
                            hash
                            batch_position
                            source
                            kind
                            timestamp
                            level
                            block
                            counter
                            id
                            contract_address
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
                first_seen: '2018-06-30T23:21:42Z',
                activated: '2018-06-30T23:12:42.000Z',
                public_key: null,
                originations: {
                    page_info: {
                        start_cursor: 'opLxWezEdNaPLnhgWXrtXsiiFoV775ej6mszHXP2XJ67ouAfvXV:0',
                        end_cursor: 'oox4Qe41cPUpYR7BBcnTLMbdW15U9fPfsbBwVhNt4f1es5CX51B:0',
                        has_next_page: false,
                        has_previous_page: true,
                    },
                    edges: [
                        {
                            cursor: 'opLxWezEdNaPLnhgWXrtXsiiFoV775ej6mszHXP2XJ67ouAfvXV:0',
                            node: {
                                hash: 'opLxWezEdNaPLnhgWXrtXsiiFoV775ej6mszHXP2XJ67ouAfvXV',
                                batch_position: 0,
                                source: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
                                kind: 'origination',
                                timestamp: '2018-07-01T02:12:42Z',
                                level: 510,
                                block: 'BLS7hzqeAn4U6NjoKtkkfehEpzbvzWNJfdTeMMq4bxj6wZKJ3xx',
                                counter: '6900000000000000',
                                id: 4207,
                                contract_address: 'KT1FN3e4qY1HGDXEW7eLq3YpHjFJPb4cd4hg',
                            },
                        },
                        {
                            cursor: 'opQR6u8WfvoaByFbcNxuK7YHzutnydCw2myNqv3zrR93SADdCVy:0',
                            node: {
                                hash: 'opQR6u8WfvoaByFbcNxuK7YHzutnydCw2myNqv3zrR93SADdCVy',
                                batch_position: 0,
                                source: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
                                kind: 'origination',
                                timestamp: '2018-06-30T23:46:42Z',
                                level: 364,
                                block: 'BLorWos9ACtkS1zoQ8Xxb5dNTDQhV7z93D9fDm4LnP31ZCPWrsA',
                                counter: '6700000000000000',
                                id: 2941,
                                contract_address: 'KT1ASvdKyRYzQuzkn2Qpb8MChjqbSTimTJxH',
                            },
                        },
                        {
                            cursor: 'oox4Qe41cPUpYR7BBcnTLMbdW15U9fPfsbBwVhNt4f1es5CX51B:0',
                            node: {
                                hash: 'oox4Qe41cPUpYR7BBcnTLMbdW15U9fPfsbBwVhNt4f1es5CX51B',
                                batch_position: 0,
                                source: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
                                kind: 'origination',
                                timestamp: '2018-06-30T23:27:42Z',
                                level: 345,
                                block: 'BLgogqQQPHmNqRxitFd7bvkKpQhZUDBTTTf5bZFMQynaHpyWwuj',
                                counter: '6500000000000000',
                                id: 2781,
                                contract_address: 'KT1CSMoRUcxrspTxRKpR6afvHXNN7NckWQpn',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.originations last and before', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ") {
                address
                first_seen
                activated
                public_key
                originations (
                last: 3
                before: "opT3F2CJAV3SK37GaqVJxecyYCFexJr8o281DSCe4RKDz1pgRYo:0"
                ) {
                    page_info {
                        start_cursor
                        end_cursor
                        has_next_page
                        has_previous_page
                    }
                    edges {
                        cursor
                        node {
                            hash
                            batch_position
                            source
                            kind
                            timestamp
                            level
                            block
                            counter
                            id
                            contract_address
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
                first_seen: '2018-06-30T23:21:42Z',
                activated: '2018-06-30T23:12:42.000Z',
                public_key: null,
                originations: {
                    page_info: {
                        start_cursor: 'ootZaX3yURSCmn5DsLBmnrykrsoBPJf8BSjR5qGP8e8jyxByewa:0',
                        end_cursor: 'onwcWSuwQW8vM1avsdicvZturwNK3mHggx3Sv4xW3fYMzS2uy1s:0',
                        has_next_page: true,
                        has_previous_page: false,
                    },
                    edges: [
                        {
                            cursor: 'ootZaX3yURSCmn5DsLBmnrykrsoBPJf8BSjR5qGP8e8jyxByewa:0',
                            node: {
                                hash: 'ootZaX3yURSCmn5DsLBmnrykrsoBPJf8BSjR5qGP8e8jyxByewa',
                                batch_position: 0,
                                source: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
                                kind: 'origination',
                                timestamp: '2018-10-14T13:35:39Z',
                                level: 145207,
                                block: 'BLhCCqvKc6G53jqv1wPYRfAAqWmDQM1u3vAYj65Wg4UYyBVVq58',
                                counter: '6f00000000000000',
                                id: 2336496,
                                contract_address: 'KT1HNBaXPEVtXvfaEzD7eazHAKxcLwAyzu3m',
                            },
                        },
                        {
                            cursor: 'onwcWSuwQW8vM1avsdicvZturwNK3mHggx3Sv4xW3fYMzS2uy1s:0',
                            node: {
                                hash: 'onwcWSuwQW8vM1avsdicvZturwNK3mHggx3Sv4xW3fYMzS2uy1s',
                                batch_position: 0,
                                source: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
                                kind: 'origination',
                                timestamp: '2018-10-14T13:27:39Z',
                                level: 145199,
                                block: 'BKnUdg9QPQcDnTEddCaQtDkE5gwCLhK78b4gy7nn7xsKKcq8Eok',
                                counter: '6e00000000000000',
                                id: 2336315,
                                contract_address: 'KT1UPypHsky3PFeMLhUhCts7bTWxYhLRDfGR',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.originations last and after', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ") {
                address
                first_seen
                activated
                public_key
                originations (
                last: 3
                after: "opT3F2CJAV3SK37GaqVJxecyYCFexJr8o281DSCe4RKDz1pgRYo:0"
                ) {
                    page_info {
                        start_cursor
                        end_cursor
                        has_next_page
                        has_previous_page
                    }
                    edges {
                        cursor
                        node {
                            hash
                            batch_position
                            source
                            kind
                            timestamp
                            level
                            block
                            counter
                            id
                            contract_address
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
                first_seen: '2018-06-30T23:21:42Z',
                activated: '2018-06-30T23:12:42.000Z',
                public_key: null,
                originations: {
                    page_info: {
                        start_cursor: 'opLxWezEdNaPLnhgWXrtXsiiFoV775ej6mszHXP2XJ67ouAfvXV:0',
                        end_cursor: 'oox4Qe41cPUpYR7BBcnTLMbdW15U9fPfsbBwVhNt4f1es5CX51B:0',
                        has_next_page: false,
                        has_previous_page: true,
                    },
                    edges: [
                        {
                            cursor: 'opLxWezEdNaPLnhgWXrtXsiiFoV775ej6mszHXP2XJ67ouAfvXV:0',
                            node: {
                                hash: 'opLxWezEdNaPLnhgWXrtXsiiFoV775ej6mszHXP2XJ67ouAfvXV',
                                batch_position: 0,
                                source: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
                                kind: 'origination',
                                timestamp: '2018-07-01T02:12:42Z',
                                level: 510,
                                block: 'BLS7hzqeAn4U6NjoKtkkfehEpzbvzWNJfdTeMMq4bxj6wZKJ3xx',
                                counter: '6900000000000000',
                                id: 4207,
                                contract_address: 'KT1FN3e4qY1HGDXEW7eLq3YpHjFJPb4cd4hg',
                            },
                        },
                        {
                            cursor: 'opQR6u8WfvoaByFbcNxuK7YHzutnydCw2myNqv3zrR93SADdCVy:0',
                            node: {
                                hash: 'opQR6u8WfvoaByFbcNxuK7YHzutnydCw2myNqv3zrR93SADdCVy',
                                batch_position: 0,
                                source: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
                                kind: 'origination',
                                timestamp: '2018-06-30T23:46:42Z',
                                level: 364,
                                block: 'BLorWos9ACtkS1zoQ8Xxb5dNTDQhV7z93D9fDm4LnP31ZCPWrsA',
                                counter: '6700000000000000',
                                id: 2941,
                                contract_address: 'KT1ASvdKyRYzQuzkn2Qpb8MChjqbSTimTJxH',
                            },
                        },
                        {
                            cursor: 'oox4Qe41cPUpYR7BBcnTLMbdW15U9fPfsbBwVhNt4f1es5CX51B:0',
                            node: {
                                hash: 'oox4Qe41cPUpYR7BBcnTLMbdW15U9fPfsbBwVhNt4f1es5CX51B',
                                batch_position: 0,
                                source: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
                                kind: 'origination',
                                timestamp: '2018-06-30T23:27:42Z',
                                level: 345,
                                block: 'BLgogqQQPHmNqRxitFd7bvkKpQhZUDBTTTf5bZFMQynaHpyWwuj',
                                counter: '6500000000000000',
                                id: 2781,
                                contract_address: 'KT1CSMoRUcxrspTxRKpR6afvHXNN7NckWQpn',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.originations last and order_by', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ") {
                address
                first_seen
                activated
                public_key
                originations (
                last: 3
                order_by: {field: ID, direction: ASC}
                ) {
                    page_info {
                        start_cursor
                        end_cursor
                        has_next_page
                        has_previous_page
                    }
                    edges {
                        cursor
                        node {
                            hash
                            batch_position
                            source
                            kind
                            timestamp
                            level
                            block
                            counter
                            id
                            contract_address
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
                first_seen: '2018-06-30T23:21:42Z',
                activated: '2018-06-30T23:12:42.000Z',
                public_key: null,
                originations: {
                    page_info: {
                        start_cursor: 'opT3F2CJAV3SK37GaqVJxecyYCFexJr8o281DSCe4RKDz1pgRYo:0',
                        end_cursor: 'ootZaX3yURSCmn5DsLBmnrykrsoBPJf8BSjR5qGP8e8jyxByewa:0',
                        has_next_page: false,
                        has_previous_page: true,
                    },
                    edges: [
                        {
                            cursor: 'opT3F2CJAV3SK37GaqVJxecyYCFexJr8o281DSCe4RKDz1pgRYo:0',
                            node: {
                                hash: 'opT3F2CJAV3SK37GaqVJxecyYCFexJr8o281DSCe4RKDz1pgRYo',
                                batch_position: 0,
                                source: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
                                kind: 'origination',
                                timestamp: '2018-08-02T11:20:38Z',
                                level: 44608,
                                block: 'BL797ZgqpSoXjz5FWKSYS9kuswn17YRfsY2Dj6DLF1br9485ymR',
                                counter: '6d00000000000000',
                                id: 370138,
                                contract_address: 'KT1NKkc3dfoqqwd2HrrDaqihFbjcZsNFwu6H',
                            },
                        },
                        {
                            cursor: 'onwcWSuwQW8vM1avsdicvZturwNK3mHggx3Sv4xW3fYMzS2uy1s:0',
                            node: {
                                hash: 'onwcWSuwQW8vM1avsdicvZturwNK3mHggx3Sv4xW3fYMzS2uy1s',
                                batch_position: 0,
                                source: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
                                kind: 'origination',
                                timestamp: '2018-10-14T13:27:39Z',
                                level: 145199,
                                block: 'BKnUdg9QPQcDnTEddCaQtDkE5gwCLhK78b4gy7nn7xsKKcq8Eok',
                                counter: '6e00000000000000',
                                id: 2336315,
                                contract_address: 'KT1UPypHsky3PFeMLhUhCts7bTWxYhLRDfGR',
                            },
                        },
                        {
                            cursor: 'ootZaX3yURSCmn5DsLBmnrykrsoBPJf8BSjR5qGP8e8jyxByewa:0',
                            node: {
                                hash: 'ootZaX3yURSCmn5DsLBmnrykrsoBPJf8BSjR5qGP8e8jyxByewa',
                                batch_position: 0,
                                source: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
                                kind: 'origination',
                                timestamp: '2018-10-14T13:35:39Z',
                                level: 145207,
                                block: 'BLhCCqvKc6G53jqv1wPYRfAAqWmDQM1u3vAYj65Wg4UYyBVVq58',
                                counter: '6f00000000000000',
                                id: 2336496,
                                contract_address: 'KT1HNBaXPEVtXvfaEzD7eazHAKxcLwAyzu3m',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.originations last and order_by and before', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ") {
                address
                first_seen
                activated
                public_key
                originations (
                last: 3
                order_by: {field: ID, direction: ASC}
                before: "opT3F2CJAV3SK37GaqVJxecyYCFexJr8o281DSCe4RKDz1pgRYo:0"
                ) {
                    page_info {
                        start_cursor
                        end_cursor
                        has_next_page
                        has_previous_page
                    }
                    edges {
                        cursor
                        node {
                            hash
                            batch_position
                            source
                            kind
                            timestamp
                            level
                            block
                            counter
                            id
                            contract_address
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
                first_seen: '2018-06-30T23:21:42Z',
                activated: '2018-06-30T23:12:42.000Z',
                public_key: null,
                originations: {
                    page_info: {
                        start_cursor: 'oox4Qe41cPUpYR7BBcnTLMbdW15U9fPfsbBwVhNt4f1es5CX51B:0',
                        end_cursor: 'opLxWezEdNaPLnhgWXrtXsiiFoV775ej6mszHXP2XJ67ouAfvXV:0',
                        has_next_page: true,
                        has_previous_page: false,
                    },
                    edges: [
                        {
                            cursor: 'oox4Qe41cPUpYR7BBcnTLMbdW15U9fPfsbBwVhNt4f1es5CX51B:0',
                            node: {
                                hash: 'oox4Qe41cPUpYR7BBcnTLMbdW15U9fPfsbBwVhNt4f1es5CX51B',
                                batch_position: 0,
                                source: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
                                kind: 'origination',
                                timestamp: '2018-06-30T23:27:42Z',
                                level: 345,
                                block: 'BLgogqQQPHmNqRxitFd7bvkKpQhZUDBTTTf5bZFMQynaHpyWwuj',
                                counter: '6500000000000000',
                                id: 2781,
                                contract_address: 'KT1CSMoRUcxrspTxRKpR6afvHXNN7NckWQpn',
                            },
                        },
                        {
                            cursor: 'opQR6u8WfvoaByFbcNxuK7YHzutnydCw2myNqv3zrR93SADdCVy:0',
                            node: {
                                hash: 'opQR6u8WfvoaByFbcNxuK7YHzutnydCw2myNqv3zrR93SADdCVy',
                                batch_position: 0,
                                source: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
                                kind: 'origination',
                                timestamp: '2018-06-30T23:46:42Z',
                                level: 364,
                                block: 'BLorWos9ACtkS1zoQ8Xxb5dNTDQhV7z93D9fDm4LnP31ZCPWrsA',
                                counter: '6700000000000000',
                                id: 2941,
                                contract_address: 'KT1ASvdKyRYzQuzkn2Qpb8MChjqbSTimTJxH',
                            },
                        },
                        {
                            cursor: 'opLxWezEdNaPLnhgWXrtXsiiFoV775ej6mszHXP2XJ67ouAfvXV:0',
                            node: {
                                hash: 'opLxWezEdNaPLnhgWXrtXsiiFoV775ej6mszHXP2XJ67ouAfvXV',
                                batch_position: 0,
                                source: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
                                kind: 'origination',
                                timestamp: '2018-07-01T02:12:42Z',
                                level: 510,
                                block: 'BLS7hzqeAn4U6NjoKtkkfehEpzbvzWNJfdTeMMq4bxj6wZKJ3xx',
                                counter: '6900000000000000',
                                id: 4207,
                                contract_address: 'KT1FN3e4qY1HGDXEW7eLq3YpHjFJPb4cd4hg',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.originations last and order_by and after', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ") {
                address
                first_seen
                activated
                public_key
                originations (
                last: 3
                order_by: {field: ID, direction: ASC}
                after: "opT3F2CJAV3SK37GaqVJxecyYCFexJr8o281DSCe4RKDz1pgRYo:0"
                ) {
                    page_info {
                        start_cursor
                        end_cursor
                        has_next_page
                        has_previous_page
                    }
                    edges {
                        cursor
                        node {
                            hash
                            batch_position
                            source
                            kind
                            timestamp
                            level
                            block
                            counter
                            id
                            contract_address
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
                first_seen: '2018-06-30T23:21:42Z',
                activated: '2018-06-30T23:12:42.000Z',
                public_key: null,
                originations: {
                    page_info: {
                        start_cursor: 'onwcWSuwQW8vM1avsdicvZturwNK3mHggx3Sv4xW3fYMzS2uy1s:0',
                        end_cursor: 'ootZaX3yURSCmn5DsLBmnrykrsoBPJf8BSjR5qGP8e8jyxByewa:0',
                        has_next_page: false,
                        has_previous_page: true,
                    },
                    edges: [
                        {
                            cursor: 'onwcWSuwQW8vM1avsdicvZturwNK3mHggx3Sv4xW3fYMzS2uy1s:0',
                            node: {
                                hash: 'onwcWSuwQW8vM1avsdicvZturwNK3mHggx3Sv4xW3fYMzS2uy1s',
                                batch_position: 0,
                                source: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
                                kind: 'origination',
                                timestamp: '2018-10-14T13:27:39Z',
                                level: 145199,
                                block: 'BKnUdg9QPQcDnTEddCaQtDkE5gwCLhK78b4gy7nn7xsKKcq8Eok',
                                counter: '6e00000000000000',
                                id: 2336315,
                                contract_address: 'KT1UPypHsky3PFeMLhUhCts7bTWxYhLRDfGR',
                            },
                        },
                        {
                            cursor: 'ootZaX3yURSCmn5DsLBmnrykrsoBPJf8BSjR5qGP8e8jyxByewa:0',
                            node: {
                                hash: 'ootZaX3yURSCmn5DsLBmnrykrsoBPJf8BSjR5qGP8e8jyxByewa',
                                batch_position: 0,
                                source: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
                                kind: 'origination',
                                timestamp: '2018-10-14T13:35:39Z',
                                level: 145207,
                                block: 'BLhCCqvKc6G53jqv1wPYRfAAqWmDQM1u3vAYj65Wg4UYyBVVq58',
                                counter: '6f00000000000000',
                                id: 2336496,
                                contract_address: 'KT1HNBaXPEVtXvfaEzD7eazHAKxcLwAyzu3m',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.originations last and date_range gte', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ") {
                address
                first_seen
                activated
                public_key
                originations (
                last: 3
                date_range: {
                    gte: "2018-08-02T11:20:38Z"
                }
                ) {
                    page_info {
                        start_cursor
                        end_cursor
                        has_next_page
                        has_previous_page
                    }
                    edges {
                        cursor
                        node {
                            hash
                            batch_position
                            source
                            kind
                            timestamp
                            level
                            block
                            counter
                            id
                            contract_address
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
                first_seen: '2018-06-30T23:21:42Z',
                activated: '2018-06-30T23:12:42.000Z',
                public_key: null,
                originations: {
                    page_info: {
                        start_cursor: 'ootZaX3yURSCmn5DsLBmnrykrsoBPJf8BSjR5qGP8e8jyxByewa:0',
                        end_cursor: 'opT3F2CJAV3SK37GaqVJxecyYCFexJr8o281DSCe4RKDz1pgRYo:0',
                        has_next_page: false,
                        has_previous_page: false,
                    },
                    edges: [
                        {
                            cursor: 'ootZaX3yURSCmn5DsLBmnrykrsoBPJf8BSjR5qGP8e8jyxByewa:0',
                            node: {
                                hash: 'ootZaX3yURSCmn5DsLBmnrykrsoBPJf8BSjR5qGP8e8jyxByewa',
                                batch_position: 0,
                                source: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
                                kind: 'origination',
                                timestamp: '2018-10-14T13:35:39Z',
                                level: 145207,
                                block: 'BLhCCqvKc6G53jqv1wPYRfAAqWmDQM1u3vAYj65Wg4UYyBVVq58',
                                counter: '6f00000000000000',
                                id: 2336496,
                                contract_address: 'KT1HNBaXPEVtXvfaEzD7eazHAKxcLwAyzu3m',
                            },
                        },
                        {
                            cursor: 'onwcWSuwQW8vM1avsdicvZturwNK3mHggx3Sv4xW3fYMzS2uy1s:0',
                            node: {
                                hash: 'onwcWSuwQW8vM1avsdicvZturwNK3mHggx3Sv4xW3fYMzS2uy1s',
                                batch_position: 0,
                                source: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
                                kind: 'origination',
                                timestamp: '2018-10-14T13:27:39Z',
                                level: 145199,
                                block: 'BKnUdg9QPQcDnTEddCaQtDkE5gwCLhK78b4gy7nn7xsKKcq8Eok',
                                counter: '6e00000000000000',
                                id: 2336315,
                                contract_address: 'KT1UPypHsky3PFeMLhUhCts7bTWxYhLRDfGR',
                            },
                        },
                        {
                            cursor: 'opT3F2CJAV3SK37GaqVJxecyYCFexJr8o281DSCe4RKDz1pgRYo:0',
                            node: {
                                hash: 'opT3F2CJAV3SK37GaqVJxecyYCFexJr8o281DSCe4RKDz1pgRYo',
                                batch_position: 0,
                                source: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
                                kind: 'origination',
                                timestamp: '2018-08-02T11:20:38Z',
                                level: 44608,
                                block: 'BL797ZgqpSoXjz5FWKSYS9kuswn17YRfsY2Dj6DLF1br9485ymR',
                                counter: '6d00000000000000',
                                id: 370138,
                                contract_address: 'KT1NKkc3dfoqqwd2HrrDaqihFbjcZsNFwu6H',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.originations last and date_range gte and before', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ") {
                address
                first_seen
                activated
                public_key
                originations (
                last: 3
                date_range: {
                    gte: "2018-08-02T11:20:38Z"
                }
                before: "opT3F2CJAV3SK37GaqVJxecyYCFexJr8o281DSCe4RKDz1pgRYo:0"
                ) {
                    page_info {
                        start_cursor
                        end_cursor
                        has_next_page
                        has_previous_page
                    }
                    edges {
                        cursor
                        node {
                            hash
                            batch_position
                            source
                            kind
                            timestamp
                            level
                            block
                            counter
                            id
                            contract_address
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
                first_seen: '2018-06-30T23:21:42Z',
                activated: '2018-06-30T23:12:42.000Z',
                public_key: null,
                originations: {
                    page_info: {
                        start_cursor: 'ootZaX3yURSCmn5DsLBmnrykrsoBPJf8BSjR5qGP8e8jyxByewa:0',
                        end_cursor: 'onwcWSuwQW8vM1avsdicvZturwNK3mHggx3Sv4xW3fYMzS2uy1s:0',
                        has_next_page: true,
                        has_previous_page: false,
                    },
                    edges: [
                        {
                            cursor: 'ootZaX3yURSCmn5DsLBmnrykrsoBPJf8BSjR5qGP8e8jyxByewa:0',
                            node: {
                                hash: 'ootZaX3yURSCmn5DsLBmnrykrsoBPJf8BSjR5qGP8e8jyxByewa',
                                batch_position: 0,
                                source: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
                                kind: 'origination',
                                timestamp: '2018-10-14T13:35:39Z',
                                level: 145207,
                                block: 'BLhCCqvKc6G53jqv1wPYRfAAqWmDQM1u3vAYj65Wg4UYyBVVq58',
                                counter: '6f00000000000000',
                                id: 2336496,
                                contract_address: 'KT1HNBaXPEVtXvfaEzD7eazHAKxcLwAyzu3m',
                            },
                        },
                        {
                            cursor: 'onwcWSuwQW8vM1avsdicvZturwNK3mHggx3Sv4xW3fYMzS2uy1s:0',
                            node: {
                                hash: 'onwcWSuwQW8vM1avsdicvZturwNK3mHggx3Sv4xW3fYMzS2uy1s',
                                batch_position: 0,
                                source: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
                                kind: 'origination',
                                timestamp: '2018-10-14T13:27:39Z',
                                level: 145199,
                                block: 'BKnUdg9QPQcDnTEddCaQtDkE5gwCLhK78b4gy7nn7xsKKcq8Eok',
                                counter: '6e00000000000000',
                                id: 2336315,
                                contract_address: 'KT1UPypHsky3PFeMLhUhCts7bTWxYhLRDfGR',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.originations last and date_range gte and after', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ") {
                address
                first_seen
                activated
                public_key
                originations (
                last: 3
                date_range: {
                    gte: "2018-06-30T23:46:42Z"
                }
                after: "opLxWezEdNaPLnhgWXrtXsiiFoV775ej6mszHXP2XJ67ouAfvXV:0"
                ) {
                    page_info {
                        start_cursor
                        end_cursor
                        has_next_page
                        has_previous_page
                    }
                    edges {
                        cursor
                        node {
                            hash
                            batch_position
                            source
                            kind
                            timestamp
                            level
                            block
                            counter
                            id
                            contract_address
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
                first_seen: '2018-06-30T23:21:42Z',
                activated: '2018-06-30T23:12:42.000Z',
                public_key: null,
                originations: {
                    page_info: {
                        start_cursor: 'opQR6u8WfvoaByFbcNxuK7YHzutnydCw2myNqv3zrR93SADdCVy:0',
                        end_cursor: 'opQR6u8WfvoaByFbcNxuK7YHzutnydCw2myNqv3zrR93SADdCVy:0',
                        has_next_page: false,
                        has_previous_page: true,
                    },
                    edges: [
                        {
                            cursor: 'opQR6u8WfvoaByFbcNxuK7YHzutnydCw2myNqv3zrR93SADdCVy:0',
                            node: {
                                hash: 'opQR6u8WfvoaByFbcNxuK7YHzutnydCw2myNqv3zrR93SADdCVy',
                                batch_position: 0,
                                source: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
                                kind: 'origination',
                                timestamp: '2018-06-30T23:46:42Z',
                                level: 364,
                                block: 'BLorWos9ACtkS1zoQ8Xxb5dNTDQhV7z93D9fDm4LnP31ZCPWrsA',
                                counter: '6700000000000000',
                                id: 2941,
                                contract_address: 'KT1ASvdKyRYzQuzkn2Qpb8MChjqbSTimTJxH',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.originations last and date_range gte and order_by', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ") {
                address
                first_seen
                activated
                public_key
                originations (
                    last: 3
                    date_range: {
                    gte: "2018-08-02T11:20:38Z"
                    }
                    order_by: {field: ID, direction: ASC}
                ) {
                    page_info {
                        start_cursor
                        end_cursor
                        has_next_page
                        has_previous_page
                    }
                    edges {
                        cursor
                        node {
                            hash
                            batch_position
                            source
                            kind
                            timestamp
                            level
                            block
                            counter
                            id
                            contract_address
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
                first_seen: '2018-06-30T23:21:42Z',
                activated: '2018-06-30T23:12:42.000Z',
                public_key: null,
                originations: {
                    page_info: {
                        start_cursor: 'opT3F2CJAV3SK37GaqVJxecyYCFexJr8o281DSCe4RKDz1pgRYo:0',
                        end_cursor: 'ootZaX3yURSCmn5DsLBmnrykrsoBPJf8BSjR5qGP8e8jyxByewa:0',
                        has_next_page: false,
                        has_previous_page: false,
                    },
                    edges: [
                        {
                            cursor: 'opT3F2CJAV3SK37GaqVJxecyYCFexJr8o281DSCe4RKDz1pgRYo:0',
                            node: {
                                hash: 'opT3F2CJAV3SK37GaqVJxecyYCFexJr8o281DSCe4RKDz1pgRYo',
                                batch_position: 0,
                                source: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
                                kind: 'origination',
                                timestamp: '2018-08-02T11:20:38Z',
                                level: 44608,
                                block: 'BL797ZgqpSoXjz5FWKSYS9kuswn17YRfsY2Dj6DLF1br9485ymR',
                                counter: '6d00000000000000',
                                id: 370138,
                                contract_address: 'KT1NKkc3dfoqqwd2HrrDaqihFbjcZsNFwu6H',
                            },
                        },
                        {
                            cursor: 'onwcWSuwQW8vM1avsdicvZturwNK3mHggx3Sv4xW3fYMzS2uy1s:0',
                            node: {
                                hash: 'onwcWSuwQW8vM1avsdicvZturwNK3mHggx3Sv4xW3fYMzS2uy1s',
                                batch_position: 0,
                                source: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
                                kind: 'origination',
                                timestamp: '2018-10-14T13:27:39Z',
                                level: 145199,
                                block: 'BKnUdg9QPQcDnTEddCaQtDkE5gwCLhK78b4gy7nn7xsKKcq8Eok',
                                counter: '6e00000000000000',
                                id: 2336315,
                                contract_address: 'KT1UPypHsky3PFeMLhUhCts7bTWxYhLRDfGR',
                            },
                        },
                        {
                            cursor: 'ootZaX3yURSCmn5DsLBmnrykrsoBPJf8BSjR5qGP8e8jyxByewa:0',
                            node: {
                                hash: 'ootZaX3yURSCmn5DsLBmnrykrsoBPJf8BSjR5qGP8e8jyxByewa',
                                batch_position: 0,
                                source: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
                                kind: 'origination',
                                timestamp: '2018-10-14T13:35:39Z',
                                level: 145207,
                                block: 'BLhCCqvKc6G53jqv1wPYRfAAqWmDQM1u3vAYj65Wg4UYyBVVq58',
                                counter: '6f00000000000000',
                                id: 2336496,
                                contract_address: 'KT1HNBaXPEVtXvfaEzD7eazHAKxcLwAyzu3m',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.originations last and date_range lte', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ") {
                address
                first_seen
                activated
                public_key
                originations (
                last: 3
                date_range: {
                    lte: "2018-10-14T13:27:39Z"
                }
                ) {
                    page_info {
                        start_cursor
                        end_cursor
                        has_next_page
                        has_previous_page
                    }
                    edges {
                        cursor
                        node {
                            hash
                            batch_position
                            source
                            kind
                            timestamp
                            level
                            block
                            counter
                            id
                            contract_address
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
                first_seen: '2018-06-30T23:21:42Z',
                activated: '2018-06-30T23:12:42.000Z',
                public_key: null,
                originations: {
                    page_info: {
                        start_cursor: 'opLxWezEdNaPLnhgWXrtXsiiFoV775ej6mszHXP2XJ67ouAfvXV:0',
                        end_cursor: 'oox4Qe41cPUpYR7BBcnTLMbdW15U9fPfsbBwVhNt4f1es5CX51B:0',
                        has_next_page: false,
                        has_previous_page: true,
                    },
                    edges: [
                        {
                            cursor: 'opLxWezEdNaPLnhgWXrtXsiiFoV775ej6mszHXP2XJ67ouAfvXV:0',
                            node: {
                                hash: 'opLxWezEdNaPLnhgWXrtXsiiFoV775ej6mszHXP2XJ67ouAfvXV',
                                batch_position: 0,
                                source: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
                                kind: 'origination',
                                timestamp: '2018-07-01T02:12:42Z',
                                level: 510,
                                block: 'BLS7hzqeAn4U6NjoKtkkfehEpzbvzWNJfdTeMMq4bxj6wZKJ3xx',
                                counter: '6900000000000000',
                                id: 4207,
                                contract_address: 'KT1FN3e4qY1HGDXEW7eLq3YpHjFJPb4cd4hg',
                            },
                        },
                        {
                            cursor: 'opQR6u8WfvoaByFbcNxuK7YHzutnydCw2myNqv3zrR93SADdCVy:0',
                            node: {
                                hash: 'opQR6u8WfvoaByFbcNxuK7YHzutnydCw2myNqv3zrR93SADdCVy',
                                batch_position: 0,
                                source: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
                                kind: 'origination',
                                timestamp: '2018-06-30T23:46:42Z',
                                level: 364,
                                block: 'BLorWos9ACtkS1zoQ8Xxb5dNTDQhV7z93D9fDm4LnP31ZCPWrsA',
                                counter: '6700000000000000',
                                id: 2941,
                                contract_address: 'KT1ASvdKyRYzQuzkn2Qpb8MChjqbSTimTJxH',
                            },
                        },
                        {
                            cursor: 'oox4Qe41cPUpYR7BBcnTLMbdW15U9fPfsbBwVhNt4f1es5CX51B:0',
                            node: {
                                hash: 'oox4Qe41cPUpYR7BBcnTLMbdW15U9fPfsbBwVhNt4f1es5CX51B',
                                batch_position: 0,
                                source: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
                                kind: 'origination',
                                timestamp: '2018-06-30T23:27:42Z',
                                level: 345,
                                block: 'BLgogqQQPHmNqRxitFd7bvkKpQhZUDBTTTf5bZFMQynaHpyWwuj',
                                counter: '6500000000000000',
                                id: 2781,
                                contract_address: 'KT1CSMoRUcxrspTxRKpR6afvHXNN7NckWQpn',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.originations last and date_range lte and before', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ") {
                address
                first_seen
                activated
                public_key
                originations (
                last: 3
                date_range: {
                    lte: "2018-10-14T13:27:39Z"
                }
                before: "opT3F2CJAV3SK37GaqVJxecyYCFexJr8o281DSCe4RKDz1pgRYo:0"
                ) {
                    page_info {
                        start_cursor
                        end_cursor
                        has_next_page
                        has_previous_page
                    }
                    edges {
                        cursor
                        node {
                            hash
                            batch_position
                            source
                            kind
                            timestamp
                            level
                            block
                            counter
                            id
                            contract_address
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
                first_seen: '2018-06-30T23:21:42Z',
                activated: '2018-06-30T23:12:42.000Z',
                public_key: null,
                originations: {
                    page_info: {
                        start_cursor: 'onwcWSuwQW8vM1avsdicvZturwNK3mHggx3Sv4xW3fYMzS2uy1s:0',
                        end_cursor: 'onwcWSuwQW8vM1avsdicvZturwNK3mHggx3Sv4xW3fYMzS2uy1s:0',
                        has_next_page: true,
                        has_previous_page: false,
                    },
                    edges: [
                        {
                            cursor: 'onwcWSuwQW8vM1avsdicvZturwNK3mHggx3Sv4xW3fYMzS2uy1s:0',
                            node: {
                                hash: 'onwcWSuwQW8vM1avsdicvZturwNK3mHggx3Sv4xW3fYMzS2uy1s',
                                batch_position: 0,
                                source: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
                                kind: 'origination',
                                timestamp: '2018-10-14T13:27:39Z',
                                level: 145199,
                                block: 'BKnUdg9QPQcDnTEddCaQtDkE5gwCLhK78b4gy7nn7xsKKcq8Eok',
                                counter: '6e00000000000000',
                                id: 2336315,
                                contract_address: 'KT1UPypHsky3PFeMLhUhCts7bTWxYhLRDfGR',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.originations last and date_range lte and after', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ") {
                address
                first_seen
                activated
                public_key
                originations (
                last: 3
                date_range: {
                    lte: "2018-10-14T13:27:39Z"
                }
                after: "opT3F2CJAV3SK37GaqVJxecyYCFexJr8o281DSCe4RKDz1pgRYo:0"
                ) {
                    page_info {
                        start_cursor
                        end_cursor
                        has_next_page
                        has_previous_page
                    }
                    edges {
                        cursor
                        node {
                            hash
                            batch_position
                            source
                            kind
                            timestamp
                            level
                            block
                            counter
                            id
                            contract_address
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
                first_seen: '2018-06-30T23:21:42Z',
                activated: '2018-06-30T23:12:42.000Z',
                public_key: null,
                originations: {
                    page_info: {
                        start_cursor: 'opLxWezEdNaPLnhgWXrtXsiiFoV775ej6mszHXP2XJ67ouAfvXV:0',
                        end_cursor: 'oox4Qe41cPUpYR7BBcnTLMbdW15U9fPfsbBwVhNt4f1es5CX51B:0',
                        has_next_page: false,
                        has_previous_page: true,
                    },
                    edges: [
                        {
                            cursor: 'opLxWezEdNaPLnhgWXrtXsiiFoV775ej6mszHXP2XJ67ouAfvXV:0',
                            node: {
                                hash: 'opLxWezEdNaPLnhgWXrtXsiiFoV775ej6mszHXP2XJ67ouAfvXV',
                                batch_position: 0,
                                source: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
                                kind: 'origination',
                                timestamp: '2018-07-01T02:12:42Z',
                                level: 510,
                                block: 'BLS7hzqeAn4U6NjoKtkkfehEpzbvzWNJfdTeMMq4bxj6wZKJ3xx',
                                counter: '6900000000000000',
                                id: 4207,
                                contract_address: 'KT1FN3e4qY1HGDXEW7eLq3YpHjFJPb4cd4hg',
                            },
                        },
                        {
                            cursor: 'opQR6u8WfvoaByFbcNxuK7YHzutnydCw2myNqv3zrR93SADdCVy:0',
                            node: {
                                hash: 'opQR6u8WfvoaByFbcNxuK7YHzutnydCw2myNqv3zrR93SADdCVy',
                                batch_position: 0,
                                source: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
                                kind: 'origination',
                                timestamp: '2018-06-30T23:46:42Z',
                                level: 364,
                                block: 'BLorWos9ACtkS1zoQ8Xxb5dNTDQhV7z93D9fDm4LnP31ZCPWrsA',
                                counter: '6700000000000000',
                                id: 2941,
                                contract_address: 'KT1ASvdKyRYzQuzkn2Qpb8MChjqbSTimTJxH',
                            },
                        },
                        {
                            cursor: 'oox4Qe41cPUpYR7BBcnTLMbdW15U9fPfsbBwVhNt4f1es5CX51B:0',
                            node: {
                                hash: 'oox4Qe41cPUpYR7BBcnTLMbdW15U9fPfsbBwVhNt4f1es5CX51B',
                                batch_position: 0,
                                source: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
                                kind: 'origination',
                                timestamp: '2018-06-30T23:27:42Z',
                                level: 345,
                                block: 'BLgogqQQPHmNqRxitFd7bvkKpQhZUDBTTTf5bZFMQynaHpyWwuj',
                                counter: '6500000000000000',
                                id: 2781,
                                contract_address: 'KT1CSMoRUcxrspTxRKpR6afvHXNN7NckWQpn',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.originations last and date_range lte and order_by', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ") {
                address
                first_seen
                activated
                public_key
                originations (
                    last: 3
                    date_range: {
                    lte: "2018-10-14T13:27:39Z"
                    }
                    order_by: {field: ID, direction: ASC}
                ) {
                    page_info {
                        start_cursor
                        end_cursor
                        has_next_page
                        has_previous_page
                    }
                    edges {
                        cursor
                        node {
                            hash
                            batch_position
                            source
                            kind
                            timestamp
                            level
                            block
                            counter
                            id
                            contract_address
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
                first_seen: '2018-06-30T23:21:42Z',
                activated: '2018-06-30T23:12:42.000Z',
                public_key: null,
                originations: {
                    page_info: {
                        start_cursor: 'opLxWezEdNaPLnhgWXrtXsiiFoV775ej6mszHXP2XJ67ouAfvXV:0',
                        end_cursor: 'onwcWSuwQW8vM1avsdicvZturwNK3mHggx3Sv4xW3fYMzS2uy1s:0',
                        has_next_page: false,
                        has_previous_page: true,
                    },
                    edges: [
                        {
                            cursor: 'opLxWezEdNaPLnhgWXrtXsiiFoV775ej6mszHXP2XJ67ouAfvXV:0',
                            node: {
                                hash: 'opLxWezEdNaPLnhgWXrtXsiiFoV775ej6mszHXP2XJ67ouAfvXV',
                                batch_position: 0,
                                source: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
                                kind: 'origination',
                                timestamp: '2018-07-01T02:12:42Z',
                                level: 510,
                                block: 'BLS7hzqeAn4U6NjoKtkkfehEpzbvzWNJfdTeMMq4bxj6wZKJ3xx',
                                counter: '6900000000000000',
                                id: 4207,
                                contract_address: 'KT1FN3e4qY1HGDXEW7eLq3YpHjFJPb4cd4hg',
                            },
                        },
                        {
                            cursor: 'opT3F2CJAV3SK37GaqVJxecyYCFexJr8o281DSCe4RKDz1pgRYo:0',
                            node: {
                                hash: 'opT3F2CJAV3SK37GaqVJxecyYCFexJr8o281DSCe4RKDz1pgRYo',
                                batch_position: 0,
                                source: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
                                kind: 'origination',
                                timestamp: '2018-08-02T11:20:38Z',
                                level: 44608,
                                block: 'BL797ZgqpSoXjz5FWKSYS9kuswn17YRfsY2Dj6DLF1br9485ymR',
                                counter: '6d00000000000000',
                                id: 370138,
                                contract_address: 'KT1NKkc3dfoqqwd2HrrDaqihFbjcZsNFwu6H',
                            },
                        },
                        {
                            cursor: 'onwcWSuwQW8vM1avsdicvZturwNK3mHggx3Sv4xW3fYMzS2uy1s:0',
                            node: {
                                hash: 'onwcWSuwQW8vM1avsdicvZturwNK3mHggx3Sv4xW3fYMzS2uy1s',
                                batch_position: 0,
                                source: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
                                kind: 'origination',
                                timestamp: '2018-10-14T13:27:39Z',
                                level: 145199,
                                block: 'BKnUdg9QPQcDnTEddCaQtDkE5gwCLhK78b4gy7nn7xsKKcq8Eok',
                                counter: '6e00000000000000',
                                id: 2336315,
                                contract_address: 'KT1UPypHsky3PFeMLhUhCts7bTWxYhLRDfGR',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.originations last and date_range gte/lte', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ") {
                address
                first_seen
                activated
                public_key
                originations (
                last: 3
                date_range: {
                    gte: "2018-08-02T11:20:38Z"
                    lte: "2019-10-27T12:46:11Z"
                }
                ) {
                    page_info {
                        start_cursor
                        end_cursor
                        has_next_page
                        has_previous_page
                    }
                    edges {
                        cursor
                        node {
                            hash
                            batch_position
                            source
                            kind
                            timestamp
                            level
                            block
                            counter
                            id
                            contract_address
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
                first_seen: '2018-06-30T23:21:42Z',
                activated: '2018-06-30T23:12:42.000Z',
                public_key: null,
                originations: {
                    page_info: {
                        start_cursor: 'ootZaX3yURSCmn5DsLBmnrykrsoBPJf8BSjR5qGP8e8jyxByewa:0',
                        end_cursor: 'opT3F2CJAV3SK37GaqVJxecyYCFexJr8o281DSCe4RKDz1pgRYo:0',
                        has_next_page: false,
                        has_previous_page: false,
                    },
                    edges: [
                        {
                            cursor: 'ootZaX3yURSCmn5DsLBmnrykrsoBPJf8BSjR5qGP8e8jyxByewa:0',
                            node: {
                                hash: 'ootZaX3yURSCmn5DsLBmnrykrsoBPJf8BSjR5qGP8e8jyxByewa',
                                batch_position: 0,
                                source: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
                                kind: 'origination',
                                timestamp: '2018-10-14T13:35:39Z',
                                level: 145207,
                                block: 'BLhCCqvKc6G53jqv1wPYRfAAqWmDQM1u3vAYj65Wg4UYyBVVq58',
                                counter: '6f00000000000000',
                                id: 2336496,
                                contract_address: 'KT1HNBaXPEVtXvfaEzD7eazHAKxcLwAyzu3m',
                            },
                        },
                        {
                            cursor: 'onwcWSuwQW8vM1avsdicvZturwNK3mHggx3Sv4xW3fYMzS2uy1s:0',
                            node: {
                                hash: 'onwcWSuwQW8vM1avsdicvZturwNK3mHggx3Sv4xW3fYMzS2uy1s',
                                batch_position: 0,
                                source: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
                                kind: 'origination',
                                timestamp: '2018-10-14T13:27:39Z',
                                level: 145199,
                                block: 'BKnUdg9QPQcDnTEddCaQtDkE5gwCLhK78b4gy7nn7xsKKcq8Eok',
                                counter: '6e00000000000000',
                                id: 2336315,
                                contract_address: 'KT1UPypHsky3PFeMLhUhCts7bTWxYhLRDfGR',
                            },
                        },
                        {
                            cursor: 'opT3F2CJAV3SK37GaqVJxecyYCFexJr8o281DSCe4RKDz1pgRYo:0',
                            node: {
                                hash: 'opT3F2CJAV3SK37GaqVJxecyYCFexJr8o281DSCe4RKDz1pgRYo',
                                batch_position: 0,
                                source: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
                                kind: 'origination',
                                timestamp: '2018-08-02T11:20:38Z',
                                level: 44608,
                                block: 'BL797ZgqpSoXjz5FWKSYS9kuswn17YRfsY2Dj6DLF1br9485ymR',
                                counter: '6d00000000000000',
                                id: 370138,
                                contract_address: 'KT1NKkc3dfoqqwd2HrrDaqihFbjcZsNFwu6H',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.originations last and date_range gte/lte and before', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ") {
                address
                first_seen
                activated
                public_key
                originations (
                last: 3
                date_range: {
                    gte: "2018-08-02T11:20:38Z"
                    lte: "2019-10-27T12:46:11Z"
                }
                before: "opT3F2CJAV3SK37GaqVJxecyYCFexJr8o281DSCe4RKDz1pgRYo:0"
                ) {
                    page_info {
                        start_cursor
                        end_cursor
                        has_next_page
                        has_previous_page
                    }
                    edges {
                        cursor
                        node {
                            hash
                            batch_position
                            source
                            kind
                            timestamp
                            level
                            block
                            counter
                            id
                            contract_address
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
                first_seen: '2018-06-30T23:21:42Z',
                activated: '2018-06-30T23:12:42.000Z',
                public_key: null,
                originations: {
                    page_info: {
                        start_cursor: 'ootZaX3yURSCmn5DsLBmnrykrsoBPJf8BSjR5qGP8e8jyxByewa:0',
                        end_cursor: 'onwcWSuwQW8vM1avsdicvZturwNK3mHggx3Sv4xW3fYMzS2uy1s:0',
                        has_next_page: true,
                        has_previous_page: false,
                    },
                    edges: [
                        {
                            cursor: 'ootZaX3yURSCmn5DsLBmnrykrsoBPJf8BSjR5qGP8e8jyxByewa:0',
                            node: {
                                hash: 'ootZaX3yURSCmn5DsLBmnrykrsoBPJf8BSjR5qGP8e8jyxByewa',
                                batch_position: 0,
                                source: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
                                kind: 'origination',
                                timestamp: '2018-10-14T13:35:39Z',
                                level: 145207,
                                block: 'BLhCCqvKc6G53jqv1wPYRfAAqWmDQM1u3vAYj65Wg4UYyBVVq58',
                                counter: '6f00000000000000',
                                id: 2336496,
                                contract_address: 'KT1HNBaXPEVtXvfaEzD7eazHAKxcLwAyzu3m',
                            },
                        },
                        {
                            cursor: 'onwcWSuwQW8vM1avsdicvZturwNK3mHggx3Sv4xW3fYMzS2uy1s:0',
                            node: {
                                hash: 'onwcWSuwQW8vM1avsdicvZturwNK3mHggx3Sv4xW3fYMzS2uy1s',
                                batch_position: 0,
                                source: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
                                kind: 'origination',
                                timestamp: '2018-10-14T13:27:39Z',
                                level: 145199,
                                block: 'BKnUdg9QPQcDnTEddCaQtDkE5gwCLhK78b4gy7nn7xsKKcq8Eok',
                                counter: '6e00000000000000',
                                id: 2336315,
                                contract_address: 'KT1UPypHsky3PFeMLhUhCts7bTWxYhLRDfGR',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.originations last and date_range gte/lte and after', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ") {
                address
                first_seen
                activated
                public_key
                originations (
                last: 3
                date_range: {
                    gte: "2018-08-02T11:20:38Z"
                    lte: "2019-10-27T12:46:11Z"
                }
                after: "onwcWSuwQW8vM1avsdicvZturwNK3mHggx3Sv4xW3fYMzS2uy1s:0"
                ) {
                    page_info {
                        start_cursor
                        end_cursor
                        has_next_page
                        has_previous_page
                    }
                    edges {
                        cursor
                        node {
                            hash
                            batch_position
                            source
                            kind
                            timestamp
                            level
                            block
                            counter
                            id
                            contract_address
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
                first_seen: '2018-06-30T23:21:42Z',
                activated: '2018-06-30T23:12:42.000Z',
                public_key: null,
                originations: {
                    page_info: {
                        start_cursor: 'opT3F2CJAV3SK37GaqVJxecyYCFexJr8o281DSCe4RKDz1pgRYo:0',
                        end_cursor: 'opT3F2CJAV3SK37GaqVJxecyYCFexJr8o281DSCe4RKDz1pgRYo:0',
                        has_next_page: false,
                        has_previous_page: true,
                    },
                    edges: [
                        {
                            cursor: 'opT3F2CJAV3SK37GaqVJxecyYCFexJr8o281DSCe4RKDz1pgRYo:0',
                            node: {
                                hash: 'opT3F2CJAV3SK37GaqVJxecyYCFexJr8o281DSCe4RKDz1pgRYo',
                                batch_position: 0,
                                source: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
                                kind: 'origination',
                                timestamp: '2018-08-02T11:20:38Z',
                                level: 44608,
                                block: 'BL797ZgqpSoXjz5FWKSYS9kuswn17YRfsY2Dj6DLF1br9485ymR',
                                counter: '6d00000000000000',
                                id: 370138,
                                contract_address: 'KT1NKkc3dfoqqwd2HrrDaqihFbjcZsNFwu6H',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.originations last and date_range gte/lte and order_by', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ") {
                address
                first_seen
                activated
                public_key
                originations (
                    last: 3
                    date_range: {
                    gte: "2018-08-02T11:20:38Z"
                    lte: "2019-10-27T12:46:11Z"
                    }
                    order_by: {field: ID, direction: ASC}
                ) {
                    page_info {
                        start_cursor
                        end_cursor
                        has_next_page
                        has_previous_page
                    }
                    edges {
                        cursor
                        node {
                            hash
                            batch_position
                            source
                            kind
                            timestamp
                            level
                            block
                            counter
                            id
                            contract_address
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
                first_seen: '2018-06-30T23:21:42Z',
                activated: '2018-06-30T23:12:42.000Z',
                public_key: null,
                originations: {
                    page_info: {
                        start_cursor: 'opT3F2CJAV3SK37GaqVJxecyYCFexJr8o281DSCe4RKDz1pgRYo:0',
                        end_cursor: 'ootZaX3yURSCmn5DsLBmnrykrsoBPJf8BSjR5qGP8e8jyxByewa:0',
                        has_next_page: false,
                        has_previous_page: false,
                    },
                    edges: [
                        {
                            cursor: 'opT3F2CJAV3SK37GaqVJxecyYCFexJr8o281DSCe4RKDz1pgRYo:0',
                            node: {
                                hash: 'opT3F2CJAV3SK37GaqVJxecyYCFexJr8o281DSCe4RKDz1pgRYo',
                                batch_position: 0,
                                source: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
                                kind: 'origination',
                                timestamp: '2018-08-02T11:20:38Z',
                                level: 44608,
                                block: 'BL797ZgqpSoXjz5FWKSYS9kuswn17YRfsY2Dj6DLF1br9485ymR',
                                counter: '6d00000000000000',
                                id: 370138,
                                contract_address: 'KT1NKkc3dfoqqwd2HrrDaqihFbjcZsNFwu6H',
                            },
                        },
                        {
                            cursor: 'onwcWSuwQW8vM1avsdicvZturwNK3mHggx3Sv4xW3fYMzS2uy1s:0',
                            node: {
                                hash: 'onwcWSuwQW8vM1avsdicvZturwNK3mHggx3Sv4xW3fYMzS2uy1s',
                                batch_position: 0,
                                source: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
                                kind: 'origination',
                                timestamp: '2018-10-14T13:27:39Z',
                                level: 145199,
                                block: 'BKnUdg9QPQcDnTEddCaQtDkE5gwCLhK78b4gy7nn7xsKKcq8Eok',
                                counter: '6e00000000000000',
                                id: 2336315,
                                contract_address: 'KT1UPypHsky3PFeMLhUhCts7bTWxYhLRDfGR',
                            },
                        },
                        {
                            cursor: 'ootZaX3yURSCmn5DsLBmnrykrsoBPJf8BSjR5qGP8e8jyxByewa:0',
                            node: {
                                hash: 'ootZaX3yURSCmn5DsLBmnrykrsoBPJf8BSjR5qGP8e8jyxByewa',
                                batch_position: 0,
                                source: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
                                kind: 'origination',
                                timestamp: '2018-10-14T13:35:39Z',
                                level: 145207,
                                block: 'BLhCCqvKc6G53jqv1wPYRfAAqWmDQM1u3vAYj65Wg4UYyBVVq58',
                                counter: '6f00000000000000',
                                id: 2336496,
                                contract_address: 'KT1HNBaXPEVtXvfaEzD7eazHAKxcLwAyzu3m',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.originations last and date_range gte and order_by and before', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ") {
                address
                first_seen
                activated
                public_key
                originations (
                last: 3
                order_by: {field: ID, direction: ASC}
                before: "opLxWezEdNaPLnhgWXrtXsiiFoV775ej6mszHXP2XJ67ouAfvXV:0"
                date_range: {
                    gte: "2018-06-30T23:46:42Z"
                }
                ) {
                    page_info {
                        start_cursor
                        end_cursor
                        has_next_page
                        has_previous_page
                    }
                    edges {
                        cursor
                        node {
                            hash
                            batch_position
                            source
                            kind
                            timestamp
                            level
                            block
                            counter
                            id
                            contract_address
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
                first_seen: '2018-06-30T23:21:42Z',
                activated: '2018-06-30T23:12:42.000Z',
                public_key: null,
                originations: {
                    page_info: {
                        start_cursor: 'opQR6u8WfvoaByFbcNxuK7YHzutnydCw2myNqv3zrR93SADdCVy:0',
                        end_cursor: 'opQR6u8WfvoaByFbcNxuK7YHzutnydCw2myNqv3zrR93SADdCVy:0',
                        has_next_page: true,
                        has_previous_page: false,
                    },
                    edges: [
                        {
                            cursor: 'opQR6u8WfvoaByFbcNxuK7YHzutnydCw2myNqv3zrR93SADdCVy:0',
                            node: {
                                hash: 'opQR6u8WfvoaByFbcNxuK7YHzutnydCw2myNqv3zrR93SADdCVy',
                                batch_position: 0,
                                source: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
                                kind: 'origination',
                                timestamp: '2018-06-30T23:46:42Z',
                                level: 364,
                                block: 'BLorWos9ACtkS1zoQ8Xxb5dNTDQhV7z93D9fDm4LnP31ZCPWrsA',
                                counter: '6700000000000000',
                                id: 2941,
                                contract_address: 'KT1ASvdKyRYzQuzkn2Qpb8MChjqbSTimTJxH',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.originations last and date_range gte and order_by and after', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ") {
                address
                first_seen
                activated
                public_key
                originations (
                last: 3
                order_by: {field: ID, direction: ASC}
                after: "onwcWSuwQW8vM1avsdicvZturwNK3mHggx3Sv4xW3fYMzS2uy1s:0"
                date_range: {
                    gte: "2018-06-30T23:46:42Z"
                }
                ) {
                    page_info {
                        start_cursor
                        end_cursor
                        has_next_page
                        has_previous_page
                    }
                    edges {
                        cursor
                        node {
                            hash
                            batch_position
                            source
                            kind
                            timestamp
                            level
                            block
                            counter
                            id
                            contract_address
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
                first_seen: '2018-06-30T23:21:42Z',
                activated: '2018-06-30T23:12:42.000Z',
                public_key: null,
                originations: {
                    page_info: {
                        start_cursor: 'ootZaX3yURSCmn5DsLBmnrykrsoBPJf8BSjR5qGP8e8jyxByewa:0',
                        end_cursor: 'ootZaX3yURSCmn5DsLBmnrykrsoBPJf8BSjR5qGP8e8jyxByewa:0',
                        has_next_page: false,
                        has_previous_page: true,
                    },
                    edges: [
                        {
                            cursor: 'ootZaX3yURSCmn5DsLBmnrykrsoBPJf8BSjR5qGP8e8jyxByewa:0',
                            node: {
                                hash: 'ootZaX3yURSCmn5DsLBmnrykrsoBPJf8BSjR5qGP8e8jyxByewa',
                                batch_position: 0,
                                source: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
                                kind: 'origination',
                                timestamp: '2018-10-14T13:35:39Z',
                                level: 145207,
                                block: 'BLhCCqvKc6G53jqv1wPYRfAAqWmDQM1u3vAYj65Wg4UYyBVVq58',
                                counter: '6f00000000000000',
                                id: 2336496,
                                contract_address: 'KT1HNBaXPEVtXvfaEzD7eazHAKxcLwAyzu3m',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.originations last and date_range lte and order_by and before', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ") {
                address
                first_seen
                activated
                public_key
                originations (
                last: 3
                order_by: {field: ID, direction: ASC}
                before: "onwcWSuwQW8vM1avsdicvZturwNK3mHggx3Sv4xW3fYMzS2uy1s:0"
                date_range: {
                    lte: "2019-10-27T12:46:11Z"
                }
                ) {
                    page_info {
                        start_cursor
                        end_cursor
                        has_next_page
                        has_previous_page
                    }
                    edges {
                        cursor
                        node {
                            hash
                            batch_position
                            source
                            kind
                            timestamp
                            level
                            block
                            counter
                            id
                            contract_address
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
                first_seen: '2018-06-30T23:21:42Z',
                activated: '2018-06-30T23:12:42.000Z',
                public_key: null,
                originations: {
                    page_info: {
                        start_cursor: 'opQR6u8WfvoaByFbcNxuK7YHzutnydCw2myNqv3zrR93SADdCVy:0',
                        end_cursor: 'opT3F2CJAV3SK37GaqVJxecyYCFexJr8o281DSCe4RKDz1pgRYo:0',
                        has_next_page: true,
                        has_previous_page: true,
                    },
                    edges: [
                        {
                            cursor: 'opQR6u8WfvoaByFbcNxuK7YHzutnydCw2myNqv3zrR93SADdCVy:0',
                            node: {
                                hash: 'opQR6u8WfvoaByFbcNxuK7YHzutnydCw2myNqv3zrR93SADdCVy',
                                batch_position: 0,
                                source: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
                                kind: 'origination',
                                timestamp: '2018-06-30T23:46:42Z',
                                level: 364,
                                block: 'BLorWos9ACtkS1zoQ8Xxb5dNTDQhV7z93D9fDm4LnP31ZCPWrsA',
                                counter: '6700000000000000',
                                id: 2941,
                                contract_address: 'KT1ASvdKyRYzQuzkn2Qpb8MChjqbSTimTJxH',
                            },
                        },
                        {
                            cursor: 'opLxWezEdNaPLnhgWXrtXsiiFoV775ej6mszHXP2XJ67ouAfvXV:0',
                            node: {
                                hash: 'opLxWezEdNaPLnhgWXrtXsiiFoV775ej6mszHXP2XJ67ouAfvXV',
                                batch_position: 0,
                                source: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
                                kind: 'origination',
                                timestamp: '2018-07-01T02:12:42Z',
                                level: 510,
                                block: 'BLS7hzqeAn4U6NjoKtkkfehEpzbvzWNJfdTeMMq4bxj6wZKJ3xx',
                                counter: '6900000000000000',
                                id: 4207,
                                contract_address: 'KT1FN3e4qY1HGDXEW7eLq3YpHjFJPb4cd4hg',
                            },
                        },
                        {
                            cursor: 'opT3F2CJAV3SK37GaqVJxecyYCFexJr8o281DSCe4RKDz1pgRYo:0',
                            node: {
                                hash: 'opT3F2CJAV3SK37GaqVJxecyYCFexJr8o281DSCe4RKDz1pgRYo',
                                batch_position: 0,
                                source: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
                                kind: 'origination',
                                timestamp: '2018-08-02T11:20:38Z',
                                level: 44608,
                                block: 'BL797ZgqpSoXjz5FWKSYS9kuswn17YRfsY2Dj6DLF1br9485ymR',
                                counter: '6d00000000000000',
                                id: 370138,
                                contract_address: 'KT1NKkc3dfoqqwd2HrrDaqihFbjcZsNFwu6H',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.originations last and date_range lte and order_by and after', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ") {
                address
                first_seen
                activated
                public_key
                originations (
                last: 3
                order_by: {field: ID, direction: ASC}
                after: "opT3F2CJAV3SK37GaqVJxecyYCFexJr8o281DSCe4RKDz1pgRYo:0"
                date_range: {
                    lte: "2019-10-27T12:46:11Z"
                }
                ) {
                    page_info {
                        start_cursor
                        end_cursor
                        has_next_page
                        has_previous_page
                    }
                    edges {
                        cursor
                        node {
                            hash
                            batch_position
                            source
                            kind
                            timestamp
                            level
                            block
                            counter
                            id
                            contract_address
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
                first_seen: '2018-06-30T23:21:42Z',
                activated: '2018-06-30T23:12:42.000Z',
                public_key: null,
                originations: {
                    page_info: {
                        start_cursor: 'onwcWSuwQW8vM1avsdicvZturwNK3mHggx3Sv4xW3fYMzS2uy1s:0',
                        end_cursor: 'ootZaX3yURSCmn5DsLBmnrykrsoBPJf8BSjR5qGP8e8jyxByewa:0',
                        has_next_page: false,
                        has_previous_page: true,
                    },
                    edges: [
                        {
                            cursor: 'onwcWSuwQW8vM1avsdicvZturwNK3mHggx3Sv4xW3fYMzS2uy1s:0',
                            node: {
                                hash: 'onwcWSuwQW8vM1avsdicvZturwNK3mHggx3Sv4xW3fYMzS2uy1s',
                                batch_position: 0,
                                source: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
                                kind: 'origination',
                                timestamp: '2018-10-14T13:27:39Z',
                                level: 145199,
                                block: 'BKnUdg9QPQcDnTEddCaQtDkE5gwCLhK78b4gy7nn7xsKKcq8Eok',
                                counter: '6e00000000000000',
                                id: 2336315,
                                contract_address: 'KT1UPypHsky3PFeMLhUhCts7bTWxYhLRDfGR',
                            },
                        },
                        {
                            cursor: 'ootZaX3yURSCmn5DsLBmnrykrsoBPJf8BSjR5qGP8e8jyxByewa:0',
                            node: {
                                hash: 'ootZaX3yURSCmn5DsLBmnrykrsoBPJf8BSjR5qGP8e8jyxByewa',
                                batch_position: 0,
                                source: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
                                kind: 'origination',
                                timestamp: '2018-10-14T13:35:39Z',
                                level: 145207,
                                block: 'BLhCCqvKc6G53jqv1wPYRfAAqWmDQM1u3vAYj65Wg4UYyBVVq58',
                                counter: '6f00000000000000',
                                id: 2336496,
                                contract_address: 'KT1HNBaXPEVtXvfaEzD7eazHAKxcLwAyzu3m',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.originations last and date_range gte/lte and order_by and before', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ") {
                address
                first_seen
                activated
                public_key
                originations (
                last: 3
                order_by: {field: ID, direction: ASC}
                before: "onwcWSuwQW8vM1avsdicvZturwNK3mHggx3Sv4xW3fYMzS2uy1s:0"
                date_range: {
                    gte: "2018-08-02T11:20:38Z"
                    lte: "2019-10-27T12:46:11Z"
                }
                ) {
                    page_info {
                        start_cursor
                        end_cursor
                        has_next_page
                        has_previous_page
                    }
                    edges {
                        cursor
                        node {
                            hash
                            batch_position
                            source
                            kind
                            timestamp
                            level
                            block
                            counter
                            id
                            contract_address
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
                first_seen: '2018-06-30T23:21:42Z',
                activated: '2018-06-30T23:12:42.000Z',
                public_key: null,
                originations: {
                    page_info: {
                        start_cursor: 'opT3F2CJAV3SK37GaqVJxecyYCFexJr8o281DSCe4RKDz1pgRYo:0',
                        end_cursor: 'opT3F2CJAV3SK37GaqVJxecyYCFexJr8o281DSCe4RKDz1pgRYo:0',
                        has_next_page: true,
                        has_previous_page: false,
                    },
                    edges: [
                        {
                            cursor: 'opT3F2CJAV3SK37GaqVJxecyYCFexJr8o281DSCe4RKDz1pgRYo:0',
                            node: {
                                hash: 'opT3F2CJAV3SK37GaqVJxecyYCFexJr8o281DSCe4RKDz1pgRYo',
                                batch_position: 0,
                                source: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
                                kind: 'origination',
                                timestamp: '2018-08-02T11:20:38Z',
                                level: 44608,
                                block: 'BL797ZgqpSoXjz5FWKSYS9kuswn17YRfsY2Dj6DLF1br9485ymR',
                                counter: '6d00000000000000',
                                id: 370138,
                                contract_address: 'KT1NKkc3dfoqqwd2HrrDaqihFbjcZsNFwu6H',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.originations last and date_range gte/lte and order_by and after', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ") {
                address
                first_seen
                activated
                public_key
                originations (
                last: 3
                order_by: {field: ID, direction: ASC}
                after: "opT3F2CJAV3SK37GaqVJxecyYCFexJr8o281DSCe4RKDz1pgRYo:0"
                date_range: {
                    gte: "2018-08-02T11:20:38Z"
                    lte: "2019-10-27T12:46:11Z"
                }
                ) {
                    page_info {
                        start_cursor
                        end_cursor
                        has_next_page
                        has_previous_page
                    }
                    edges {
                        cursor
                        node {
                            hash
                            batch_position
                            source
                            kind
                            timestamp
                            level
                            block
                            counter
                            id
                            contract_address
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
                first_seen: '2018-06-30T23:21:42Z',
                activated: '2018-06-30T23:12:42.000Z',
                public_key: null,
                originations: {
                    page_info: {
                        start_cursor: 'onwcWSuwQW8vM1avsdicvZturwNK3mHggx3Sv4xW3fYMzS2uy1s:0',
                        end_cursor: 'ootZaX3yURSCmn5DsLBmnrykrsoBPJf8BSjR5qGP8e8jyxByewa:0',
                        has_next_page: false,
                        has_previous_page: true,
                    },
                    edges: [
                        {
                            cursor: 'onwcWSuwQW8vM1avsdicvZturwNK3mHggx3Sv4xW3fYMzS2uy1s:0',
                            node: {
                                hash: 'onwcWSuwQW8vM1avsdicvZturwNK3mHggx3Sv4xW3fYMzS2uy1s',
                                batch_position: 0,
                                source: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
                                kind: 'origination',
                                timestamp: '2018-10-14T13:27:39Z',
                                level: 145199,
                                block: 'BKnUdg9QPQcDnTEddCaQtDkE5gwCLhK78b4gy7nn7xsKKcq8Eok',
                                counter: '6e00000000000000',
                                id: 2336315,
                                contract_address: 'KT1UPypHsky3PFeMLhUhCts7bTWxYhLRDfGR',
                            },
                        },
                        {
                            cursor: 'ootZaX3yURSCmn5DsLBmnrykrsoBPJf8BSjR5qGP8e8jyxByewa:0',
                            node: {
                                hash: 'ootZaX3yURSCmn5DsLBmnrykrsoBPJf8BSjR5qGP8e8jyxByewa',
                                batch_position: 0,
                                source: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
                                kind: 'origination',
                                timestamp: '2018-10-14T13:35:39Z',
                                level: 145207,
                                block: 'BLhCCqvKc6G53jqv1wPYRfAAqWmDQM1u3vAYj65Wg4UYyBVVq58',
                                counter: '6f00000000000000',
                                id: 2336496,
                                contract_address: 'KT1HNBaXPEVtXvfaEzD7eazHAKxcLwAyzu3m',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.originations date_range gte/lte error', async () => {
        const GET_ACCOUNT = gql`
            query {
                account(address: "tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ") {
                    address
                    first_seen
                    activated
                    public_key
                    originations (
                    first: 3
                    date_range: {
                        gte: "2019-10-22T11:20:38Z"
                        lte: "2019-10-21T12:46:11Z"
                    }
                    ) {
                        page_info {
                            start_cursor
                            end_cursor
                            has_next_page
                            has_previous_page
                        }
                    }
                }
            }
            `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });
        if (res.errors == undefined) {
            throw new Error('Expected an error message however res.errors is undefined.');
        }

        expect(res.errors[0].message).toMatch('Argument "date" of type "DateRange?" must have the "lte" value greater than the "gte" value.');
    });

    test('account.originations query limit error', async () => {
        const GET_ACCOUNT = gql`
            query {
                account(address: "tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ") {
                    originations (
                    first: 201
                    ) {
                        page_info {
                            start_cursor
                            end_cursor
                            has_next_page
                            has_previous_page
                        }
                    }
                }
            }
            `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });
        if (res.errors == undefined) {
            throw new Error('Expected an error message however res.errors is undefined.');
        }

        expect(res.errors[0].message).toMatch('Argument \"limit\" of type \"Float?\" cannot be greater than 200.');
    });
});
