import 'reflect-metadata';
import { ApolloServer } from 'apollo-server-express';
import { gql } from 'apollo-server';
import { createTestClient } from 'apollo-server-testing';
import { AccountResolver } from '../../../src/resolvers/account';
import { buildSchema } from 'type-graphql';

async function startTestServer() {
    const testServer = new ApolloServer({
        schema: await buildSchema({
            resolvers: [AccountResolver],
        }),
        context: ({ req, res }) => ({ req, res }),
    });

    return testServer;
}

const server = startTestServer();

let testClientQuery: Function;

async function startTestClient() {
    if (!testClientQuery) {
        const { query } = createTestClient(await server);
        testClientQuery = await query;
    }
}

describe('Account Resolvers', () => {
    test('account.endorsements first', async () => {
        const GET_ACCOUNT = gql`
            query {
                account(address: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS") {
                    address
                    first_seen
                    activated
                    public_key
                    endorsements (first: 3) {
                        page_info {
                            start_cursor
                            end_cursor
                            has_next_page
                            has_previous_page
                        }
                        edges {
                            cursor
                            node {
                                hash
                                batch_position
                                kind
                                timestamp
                                level
                                block
                                id
                                source
                                delegate
                                slots
                            }
                        }
                    }
                }
            }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                first_seen: '2018-09-13T14:51:20Z',
                activated: '2018-09-13T14:49:20.000Z',
                public_key: null,
                endorsements: {
                    page_info: {
                        start_cursor: 'onxVkdzq8hDahsyqBhZx9mVzHmQTJ2zd1aQuQixiLTEtT9Syg95:0',
                        end_cursor: 'op6CjEdgjkTpcNyks1BdkM4DhNFXJLiE3coU3VekM9zdYwWcReL:0',
                        has_next_page: true,
                        has_previous_page: false,
                    },
                    edges: [
                        {
                            cursor: 'onxVkdzq8hDahsyqBhZx9mVzHmQTJ2zd1aQuQixiLTEtT9Syg95:0',
                            node: {
                                hash: 'onxVkdzq8hDahsyqBhZx9mVzHmQTJ2zd1aQuQixiLTEtT9Syg95',
                                batch_position: 0,
                                kind: 'endorsement',
                                timestamp: '2020-10-27T15:01:48Z',
                                level: 1189620,
                                block: 'BLCkf49KwQiJLjMiaQ92BeDfzUnLCRiH1E5KCMiEG6ukTwgCSUu',
                                id: 29011132,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                delegate: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                slots: '{23}',
                            },
                        },
                        {
                            cursor: 'oo29bUnXuwrns8XcGTnthznafUJKnspNAWqgSuPvuheEsnFBP5x:0',
                            node: {
                                hash: 'oo29bUnXuwrns8XcGTnthznafUJKnspNAWqgSuPvuheEsnFBP5x',
                                batch_position: 0,
                                kind: 'endorsement',
                                timestamp: '2020-10-25T19:02:22Z',
                                level: 1186992,
                                block: 'BM2YytRc8tQbyVxqQkCZejpGY4uuZwZq2W1nD8pBqoaWPEGkQ9K',
                                id: 28943109,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                delegate: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                slots: '{28}',
                            },
                        },
                        {
                            cursor: 'op6CjEdgjkTpcNyks1BdkM4DhNFXJLiE3coU3VekM9zdYwWcReL:0',
                            node: {
                                hash: 'op6CjEdgjkTpcNyks1BdkM4DhNFXJLiE3coU3VekM9zdYwWcReL',
                                batch_position: 0,
                                kind: 'endorsement',
                                timestamp: '2020-10-24T08:43:48Z',
                                level: 1184939,
                                block: 'BKveoMzNLEBoAsvyBHL7XaMbLiFhrB8DaMoNMvdfk3V6GuM6PoJ',
                                id: 28892185,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                delegate: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                slots: '{29}',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.endorsements first and before', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS") {
                address
                first_seen
                activated
                public_key
                endorsements (
                  first: 3
                  before: "op6CjEdgjkTpcNyks1BdkM4DhNFXJLiE3coU3VekM9zdYwWcReL:0"
                ) {
                    page_info {
                        start_cursor
                        end_cursor
                        has_next_page
                        has_previous_page
                    }
                    edges {
                        cursor
                        node {
                            hash
                            batch_position
                            kind
                            timestamp
                            level
                            block
                            id
                            source
                            delegate
                            slots
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                first_seen: '2018-09-13T14:51:20Z',
                activated: '2018-09-13T14:49:20.000Z',
                public_key: null,
                endorsements: {
                    page_info: {
                        start_cursor: 'onxVkdzq8hDahsyqBhZx9mVzHmQTJ2zd1aQuQixiLTEtT9Syg95:0',
                        end_cursor: 'oo29bUnXuwrns8XcGTnthznafUJKnspNAWqgSuPvuheEsnFBP5x:0',
                        has_next_page: true,
                        has_previous_page: false,
                    },
                    edges: [
                        {
                            cursor: 'onxVkdzq8hDahsyqBhZx9mVzHmQTJ2zd1aQuQixiLTEtT9Syg95:0',
                            node: {
                                hash: 'onxVkdzq8hDahsyqBhZx9mVzHmQTJ2zd1aQuQixiLTEtT9Syg95',
                                batch_position: 0,
                                kind: 'endorsement',
                                timestamp: '2020-10-27T15:01:48Z',
                                level: 1189620,
                                block: 'BLCkf49KwQiJLjMiaQ92BeDfzUnLCRiH1E5KCMiEG6ukTwgCSUu',
                                id: 29011132,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                delegate: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                slots: '{23}',
                            },
                        },
                        {
                            cursor: 'oo29bUnXuwrns8XcGTnthznafUJKnspNAWqgSuPvuheEsnFBP5x:0',
                            node: {
                                hash: 'oo29bUnXuwrns8XcGTnthznafUJKnspNAWqgSuPvuheEsnFBP5x',
                                batch_position: 0,
                                kind: 'endorsement',
                                timestamp: '2020-10-25T19:02:22Z',
                                level: 1186992,
                                block: 'BM2YytRc8tQbyVxqQkCZejpGY4uuZwZq2W1nD8pBqoaWPEGkQ9K',
                                id: 28943109,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                delegate: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                slots: '{28}',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.endorsements first and after', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS") {
                address
                first_seen
                activated
                public_key
                endorsements (
                  first: 3
                  after: "op6CjEdgjkTpcNyks1BdkM4DhNFXJLiE3coU3VekM9zdYwWcReL:0"
                ) {
                    page_info {
                        start_cursor
                        end_cursor
                        has_next_page
                        has_previous_page
                    }
                    edges {
                        cursor
                        node {
                            hash
                            batch_position
                            kind
                            timestamp
                            level
                            block
                            id
                            source
                            delegate
                            slots
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                first_seen: '2018-09-13T14:51:20Z',
                activated: '2018-09-13T14:49:20.000Z',
                public_key: null,
                endorsements: {
                    page_info: {
                        start_cursor: 'op4F7TA7SEbds4Nbq49z5GtqM8YXLLJM7JonGEjo6zZHTaqLqiS:0',
                        end_cursor: 'oommuxojsu7cLR3xtLrBJQz2Cyi3KZiFiGqd3uD95onVvvYtWGB:0',
                        has_next_page: true,
                        has_previous_page: true,
                    },
                    edges: [
                        {
                            cursor: 'op4F7TA7SEbds4Nbq49z5GtqM8YXLLJM7JonGEjo6zZHTaqLqiS:0',
                            node: {
                                hash: 'op4F7TA7SEbds4Nbq49z5GtqM8YXLLJM7JonGEjo6zZHTaqLqiS',
                                batch_position: 0,
                                kind: 'endorsement',
                                timestamp: '2020-10-23T04:38:12Z',
                                level: 1183263,
                                block: 'BLmPW9LAvpMnimn2pRXXmgYMsGGPv5MDpnbeurpTnxXjAUsPpb3',
                                id: 28849385,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                delegate: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                slots: '{3}',
                            },
                        },
                        {
                            cursor: 'ooAk4vFVjGHU9NvMj84uAhrzvMFzcZXnTz5YkGyf18p1KK7cb7y:0',
                            node: {
                                hash: 'ooAk4vFVjGHU9NvMj84uAhrzvMFzcZXnTz5YkGyf18p1KK7cb7y',
                                batch_position: 0,
                                kind: 'endorsement',
                                timestamp: '2020-10-22T19:46:32Z',
                                level: 1182732,
                                block: 'BLVgCLdbCapVSQGmLjJMgpvwqTt51mUUScQYMVfUqK8wRVqUsfr',
                                id: 28836261,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                delegate: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                slots: '{28}',
                            },
                        },
                        {
                            cursor: 'oommuxojsu7cLR3xtLrBJQz2Cyi3KZiFiGqd3uD95onVvvYtWGB:0',
                            node: {
                                hash: 'oommuxojsu7cLR3xtLrBJQz2Cyi3KZiFiGqd3uD95onVvvYtWGB',
                                batch_position: 0,
                                kind: 'endorsement',
                                timestamp: '2020-10-21T10:34:10Z',
                                level: 1180760,
                                block: 'BLBEW3F2hseaCRV5gPF1EdVUc3iYrS4VJGjXuLxvqMenUEgpiK9',
                                id: 28786676,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                delegate: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                slots: '{0}',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.endorsements first and order_by', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS") {
                address
                first_seen
                activated
                public_key
                endorsements (
                  first: 3
                  order_by: {field: ID, direction: ASC}
                ) {
                    page_info {
                        start_cursor
                        end_cursor
                        has_next_page
                        has_previous_page
                    }
                    edges {
                        cursor
                        node {
                            hash
                            batch_position
                            kind
                            timestamp
                            level
                            block
                            id
                            source
                            delegate
                            slots
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                first_seen: '2018-09-13T14:51:20Z',
                activated: '2018-09-13T14:49:20.000Z',
                public_key: null,
                endorsements: {
                    page_info: {
                        start_cursor: 'opZSatngKMGpcxgEGy4if5uRS4fCh6p4p3ab9KEuoDZBGQBvGBJ:0',
                        end_cursor: 'onuTjTGY85y9YpwfEPsquyxqNY7AkQNpGqU2JwU4c4ia2RbZ79Y:0',
                        has_next_page: true,
                        has_previous_page: false,
                    },
                    edges: [
                        {
                            cursor: 'opZSatngKMGpcxgEGy4if5uRS4fCh6p4p3ab9KEuoDZBGQBvGBJ:0',
                            node: {
                                hash: 'opZSatngKMGpcxgEGy4if5uRS4fCh6p4p3ab9KEuoDZBGQBvGBJ',
                                batch_position: 0,
                                kind: 'endorsement',
                                timestamp: '2018-10-04T18:15:29Z',
                                level: 131391,
                                block: 'BKrmjm6kRWWkPiccPDPJeB7nWUr561oVCzAWq4DWLe88RweRVAU',
                                id: 2042670,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                delegate: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                slots: '{24}',
                            },
                        },
                        {
                            cursor: 'ooTcJtDdEVWTaHQxEPZr2Wf47e3w4g4j4zFKq9kJuTxo4UKrrXs:0',
                            node: {
                                hash: 'ooTcJtDdEVWTaHQxEPZr2Wf47e3w4g4j4zFKq9kJuTxo4UKrrXs',
                                batch_position: 0,
                                kind: 'endorsement',
                                timestamp: '2018-10-04T18:39:29Z',
                                level: 131415,
                                block: 'BL3KMddQ78skAfM7saXk3c992Z8nvH8Lh4Tf72QTk6rnJg6RRSX',
                                id: 2043192,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                delegate: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                slots: '{25}',
                            },
                        },
                        {
                            cursor: 'onuTjTGY85y9YpwfEPsquyxqNY7AkQNpGqU2JwU4c4ia2RbZ79Y:0',
                            node: {
                                hash: 'onuTjTGY85y9YpwfEPsquyxqNY7AkQNpGqU2JwU4c4ia2RbZ79Y',
                                batch_position: 0,
                                kind: 'endorsement',
                                timestamp: '2018-10-06T09:26:29Z',
                                level: 133692,
                                block: 'BMNXM16a9gdiicXytKkJ2L16ehTsphrLJNdRjA5g9X75cbFEsMQ',
                                id: 2090560,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                delegate: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                slots: '{13}',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.endorsements first and order_by and before', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS") {
                address
                first_seen
                activated
                public_key
                endorsements (
                  first: 3
                  order_by: {field: ID, direction: ASC}
                  before: "onuTjTGY85y9YpwfEPsquyxqNY7AkQNpGqU2JwU4c4ia2RbZ79Y:0"
                ) {
                    page_info {
                        start_cursor
                        end_cursor
                        has_next_page
                        has_previous_page
                    }
                    edges {
                        cursor
                        node {
                            hash
                            batch_position
                            kind
                            timestamp
                            level
                            block
                            id
                            source
                            delegate
                            slots
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                first_seen: '2018-09-13T14:51:20Z',
                activated: '2018-09-13T14:49:20.000Z',
                public_key: null,
                endorsements: {
                    page_info: {
                        start_cursor: 'opZSatngKMGpcxgEGy4if5uRS4fCh6p4p3ab9KEuoDZBGQBvGBJ:0',
                        end_cursor: 'ooTcJtDdEVWTaHQxEPZr2Wf47e3w4g4j4zFKq9kJuTxo4UKrrXs:0',
                        has_next_page: true,
                        has_previous_page: false,
                    },
                    edges: [
                        {
                            cursor: 'opZSatngKMGpcxgEGy4if5uRS4fCh6p4p3ab9KEuoDZBGQBvGBJ:0',
                            node: {
                                hash: 'opZSatngKMGpcxgEGy4if5uRS4fCh6p4p3ab9KEuoDZBGQBvGBJ',
                                batch_position: 0,
                                kind: 'endorsement',
                                timestamp: '2018-10-04T18:15:29Z',
                                level: 131391,
                                block: 'BKrmjm6kRWWkPiccPDPJeB7nWUr561oVCzAWq4DWLe88RweRVAU',
                                id: 2042670,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                delegate: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                slots: '{24}',
                            },
                        },
                        {
                            cursor: 'ooTcJtDdEVWTaHQxEPZr2Wf47e3w4g4j4zFKq9kJuTxo4UKrrXs:0',
                            node: {
                                hash: 'ooTcJtDdEVWTaHQxEPZr2Wf47e3w4g4j4zFKq9kJuTxo4UKrrXs',
                                batch_position: 0,
                                kind: 'endorsement',
                                timestamp: '2018-10-04T18:39:29Z',
                                level: 131415,
                                block: 'BL3KMddQ78skAfM7saXk3c992Z8nvH8Lh4Tf72QTk6rnJg6RRSX',
                                id: 2043192,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                delegate: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                slots: '{25}',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.endorsements first and order_by and after', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS") {
                address
                first_seen
                activated
                public_key
                endorsements (
                  first: 3
                  order_by: {field: ID, direction: ASC}
                  after: "onuTjTGY85y9YpwfEPsquyxqNY7AkQNpGqU2JwU4c4ia2RbZ79Y:0"
                ) {
                    page_info {
                        start_cursor
                        end_cursor
                        has_next_page
                        has_previous_page
                    }
                    edges {
                        cursor
                        node {
                            hash
                            batch_position
                            kind
                            timestamp
                            level
                            block
                            id
                            source
                            delegate
                            slots
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                first_seen: '2018-09-13T14:51:20Z',
                activated: '2018-09-13T14:49:20.000Z',
                public_key: null,
                endorsements: {
                    page_info: {
                        start_cursor: 'oos6eeoCBxGw5FapEkgYbXChd3wCx5SBd1RkYj7pCbkJYk3TGV6:0',
                        end_cursor: 'ooEZio9pGUCk6REjBDw7N3FVG6e6vWwFneG8cDzoR5yJjUgCjQR:0',
                        has_next_page: true,
                        has_previous_page: true,
                    },
                    edges: [
                        {
                            cursor: 'oos6eeoCBxGw5FapEkgYbXChd3wCx5SBd1RkYj7pCbkJYk3TGV6:0',
                            node: {
                                hash: 'oos6eeoCBxGw5FapEkgYbXChd3wCx5SBd1RkYj7pCbkJYk3TGV6',
                                batch_position: 0,
                                kind: 'endorsement',
                                timestamp: '2018-10-08T04:20:47Z',
                                level: 136215,
                                block: 'BLRexj3qJnWw19BEpw9e6NDmf8iVLeTpnjhjTGwwXuqq63MJdU5',
                                id: 2145056,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                delegate: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                slots: '{9}',
                            },
                        },
                        {
                            cursor: 'oohZBRc7TMQ6peAP8wedZnZSW14a7tevnzLNHVATATKfvgWeQJf:0',
                            node: {
                                hash: 'oohZBRc7TMQ6peAP8wedZnZSW14a7tevnzLNHVATATKfvgWeQJf',
                                batch_position: 0,
                                kind: 'endorsement',
                                timestamp: '2018-10-10T06:03:02Z',
                                level: 139136,
                                block: 'BLTs3iVYT7aw38h61KZ8LrUzAh1AmUXLAjgifRGB4ZB78276YRX',
                                id: 2206336,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                delegate: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                slots: '{11}',
                            },
                        },
                        {
                            cursor: 'ooEZio9pGUCk6REjBDw7N3FVG6e6vWwFneG8cDzoR5yJjUgCjQR:0',
                            node: {
                                hash: 'ooEZio9pGUCk6REjBDw7N3FVG6e6vWwFneG8cDzoR5yJjUgCjQR',
                                batch_position: 0,
                                kind: 'endorsement',
                                timestamp: '2018-10-10T20:21:09Z',
                                level: 139965,
                                block: 'BKnG9AfDm8jXJ7c9dVA5QAP8tD8dTA9jbCePMCpS9Pb4DQrbKWJ',
                                id: 2225474,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                delegate: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                slots: '{28}',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.endorsements first and date_range gte', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS") {
                address
                first_seen
                activated
                public_key
                endorsements (
                  first: 3
                  date_range: {
                    gte: "2019-10-22T18:56:24Z"
                  }
                ) {
                    page_info {
                        start_cursor
                        end_cursor
                        has_next_page
                        has_previous_page
                    }
                    edges {
                        cursor
                        node {
                            hash
                            batch_position
                            kind
                            timestamp
                            level
                            block
                            id
                            source
                            delegate
                            slots
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                first_seen: '2018-09-13T14:51:20Z',
                activated: '2018-09-13T14:49:20.000Z',
                public_key: null,
                endorsements: {
                    page_info: {
                        start_cursor: 'onxVkdzq8hDahsyqBhZx9mVzHmQTJ2zd1aQuQixiLTEtT9Syg95:0',
                        end_cursor: 'op6CjEdgjkTpcNyks1BdkM4DhNFXJLiE3coU3VekM9zdYwWcReL:0',
                        has_next_page: true,
                        has_previous_page: false,
                    },
                    edges: [
                        {
                            cursor: 'onxVkdzq8hDahsyqBhZx9mVzHmQTJ2zd1aQuQixiLTEtT9Syg95:0',
                            node: {
                                hash: 'onxVkdzq8hDahsyqBhZx9mVzHmQTJ2zd1aQuQixiLTEtT9Syg95',
                                batch_position: 0,
                                kind: 'endorsement',
                                timestamp: '2020-10-27T15:01:48Z',
                                level: 1189620,
                                block: 'BLCkf49KwQiJLjMiaQ92BeDfzUnLCRiH1E5KCMiEG6ukTwgCSUu',
                                id: 29011132,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                delegate: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                slots: '{23}',
                            },
                        },
                        {
                            cursor: 'oo29bUnXuwrns8XcGTnthznafUJKnspNAWqgSuPvuheEsnFBP5x:0',
                            node: {
                                hash: 'oo29bUnXuwrns8XcGTnthznafUJKnspNAWqgSuPvuheEsnFBP5x',
                                batch_position: 0,
                                kind: 'endorsement',
                                timestamp: '2020-10-25T19:02:22Z',
                                level: 1186992,
                                block: 'BM2YytRc8tQbyVxqQkCZejpGY4uuZwZq2W1nD8pBqoaWPEGkQ9K',
                                id: 28943109,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                delegate: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                slots: '{28}',
                            },
                        },
                        {
                            cursor: 'op6CjEdgjkTpcNyks1BdkM4DhNFXJLiE3coU3VekM9zdYwWcReL:0',
                            node: {
                                hash: 'op6CjEdgjkTpcNyks1BdkM4DhNFXJLiE3coU3VekM9zdYwWcReL',
                                batch_position: 0,
                                kind: 'endorsement',
                                timestamp: '2020-10-24T08:43:48Z',
                                level: 1184939,
                                block: 'BKveoMzNLEBoAsvyBHL7XaMbLiFhrB8DaMoNMvdfk3V6GuM6PoJ',
                                id: 28892185,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                delegate: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                slots: '{29}',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.endorsements first and date_range gte and before', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS") {
                address
                first_seen
                activated
                public_key
                endorsements (
                  first: 3
                  date_range: {
                    gte: "2019-10-22T18:56:24Z"
                  }
                  before: "op6CjEdgjkTpcNyks1BdkM4DhNFXJLiE3coU3VekM9zdYwWcReL:0"
                ) {
                    page_info {
                        start_cursor
                        end_cursor
                        has_next_page
                        has_previous_page
                    }
                    edges {
                        cursor
                        node {
                            hash
                            batch_position
                            kind
                            timestamp
                            level
                            block
                            id
                            source
                            delegate
                            slots
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                first_seen: '2018-09-13T14:51:20Z',
                activated: '2018-09-13T14:49:20.000Z',
                public_key: null,
                endorsements: {
                    page_info: {
                        start_cursor: 'onxVkdzq8hDahsyqBhZx9mVzHmQTJ2zd1aQuQixiLTEtT9Syg95:0',
                        end_cursor: 'oo29bUnXuwrns8XcGTnthznafUJKnspNAWqgSuPvuheEsnFBP5x:0',
                        has_next_page: true,
                        has_previous_page: false,
                    },
                    edges: [
                        {
                            cursor: 'onxVkdzq8hDahsyqBhZx9mVzHmQTJ2zd1aQuQixiLTEtT9Syg95:0',
                            node: {
                                hash: 'onxVkdzq8hDahsyqBhZx9mVzHmQTJ2zd1aQuQixiLTEtT9Syg95',
                                batch_position: 0,
                                kind: 'endorsement',
                                timestamp: '2020-10-27T15:01:48Z',
                                level: 1189620,
                                block: 'BLCkf49KwQiJLjMiaQ92BeDfzUnLCRiH1E5KCMiEG6ukTwgCSUu',
                                id: 29011132,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                delegate: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                slots: '{23}',
                            },
                        },
                        {
                            cursor: 'oo29bUnXuwrns8XcGTnthznafUJKnspNAWqgSuPvuheEsnFBP5x:0',
                            node: {
                                hash: 'oo29bUnXuwrns8XcGTnthznafUJKnspNAWqgSuPvuheEsnFBP5x',
                                batch_position: 0,
                                kind: 'endorsement',
                                timestamp: '2020-10-25T19:02:22Z',
                                level: 1186992,
                                block: 'BM2YytRc8tQbyVxqQkCZejpGY4uuZwZq2W1nD8pBqoaWPEGkQ9K',
                                id: 28943109,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                delegate: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                slots: '{28}',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.endorsements first and date_range gte and after', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS") {
                address
                first_seen
                activated
                public_key
                endorsements (
                  first: 3
                  date_range: {
                    gte: "2019-10-22T18:56:24Z"
                  }
                  after: "op6CjEdgjkTpcNyks1BdkM4DhNFXJLiE3coU3VekM9zdYwWcReL:0"
                ) {
                    page_info {
                        start_cursor
                        end_cursor
                        has_next_page
                        has_previous_page
                    }
                    edges {
                        cursor
                        node {
                            hash
                            batch_position
                            kind
                            timestamp
                            level
                            block
                            id
                            source
                            delegate
                            slots
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                first_seen: '2018-09-13T14:51:20Z',
                activated: '2018-09-13T14:49:20.000Z',
                public_key: null,
                endorsements: {
                    page_info: {
                        start_cursor: 'op4F7TA7SEbds4Nbq49z5GtqM8YXLLJM7JonGEjo6zZHTaqLqiS:0',
                        end_cursor: 'oommuxojsu7cLR3xtLrBJQz2Cyi3KZiFiGqd3uD95onVvvYtWGB:0',
                        has_next_page: true,
                        has_previous_page: true,
                    },
                    edges: [
                        {
                            cursor: 'op4F7TA7SEbds4Nbq49z5GtqM8YXLLJM7JonGEjo6zZHTaqLqiS:0',
                            node: {
                                hash: 'op4F7TA7SEbds4Nbq49z5GtqM8YXLLJM7JonGEjo6zZHTaqLqiS',
                                batch_position: 0,
                                kind: 'endorsement',
                                timestamp: '2020-10-23T04:38:12Z',
                                level: 1183263,
                                block: 'BLmPW9LAvpMnimn2pRXXmgYMsGGPv5MDpnbeurpTnxXjAUsPpb3',
                                id: 28849385,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                delegate: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                slots: '{3}',
                            },
                        },
                        {
                            cursor: 'ooAk4vFVjGHU9NvMj84uAhrzvMFzcZXnTz5YkGyf18p1KK7cb7y:0',
                            node: {
                                hash: 'ooAk4vFVjGHU9NvMj84uAhrzvMFzcZXnTz5YkGyf18p1KK7cb7y',
                                batch_position: 0,
                                kind: 'endorsement',
                                timestamp: '2020-10-22T19:46:32Z',
                                level: 1182732,
                                block: 'BLVgCLdbCapVSQGmLjJMgpvwqTt51mUUScQYMVfUqK8wRVqUsfr',
                                id: 28836261,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                delegate: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                slots: '{28}',
                            },
                        },
                        {
                            cursor: 'oommuxojsu7cLR3xtLrBJQz2Cyi3KZiFiGqd3uD95onVvvYtWGB:0',
                            node: {
                                hash: 'oommuxojsu7cLR3xtLrBJQz2Cyi3KZiFiGqd3uD95onVvvYtWGB',
                                batch_position: 0,
                                kind: 'endorsement',
                                timestamp: '2020-10-21T10:34:10Z',
                                level: 1180760,
                                block: 'BLBEW3F2hseaCRV5gPF1EdVUc3iYrS4VJGjXuLxvqMenUEgpiK9',
                                id: 28786676,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                delegate: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                slots: '{0}',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.endorsements first and date_range gte and order_by', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS") {
                address
                first_seen
                activated
                public_key
                endorsements (
                    first: 3
                    date_range: {
                    gte: "2019-10-22T18:56:24Z"
                    }
                    order_by: {field: ID, direction: ASC}
                ) {
                    page_info {
                        start_cursor
                        end_cursor
                        has_next_page
                        has_previous_page
                    }
                    edges {
                        cursor
                        node {
                            hash
                            batch_position
                            kind
                            timestamp
                            level
                            block
                            id
                            source
                            delegate
                            slots
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                first_seen: '2018-09-13T14:51:20Z',
                activated: '2018-09-13T14:49:20.000Z',
                public_key: null,
                endorsements: {
                    page_info: {
                        start_cursor: 'onsSSmnhHAX1ARcp6WLGuwJVNr7QDrVTggM3iDsTee2UvTjDPZR:0',
                        end_cursor: 'onkBcrEqbAzaXAK9NRZfZs1joRv3D3CTnTE8nA7c5xAe1aTbVda:0',
                        has_next_page: true,
                        has_previous_page: false,
                    },
                    edges: [
                        {
                            cursor: 'onsSSmnhHAX1ARcp6WLGuwJVNr7QDrVTggM3iDsTee2UvTjDPZR:0',
                            node: {
                                hash: 'onsSSmnhHAX1ARcp6WLGuwJVNr7QDrVTggM3iDsTee2UvTjDPZR',
                                batch_position: 0,
                                kind: 'endorsement',
                                timestamp: '2019-10-22T22:34:49Z',
                                level: 661666,
                                block: 'BMCx8jtyQBnkoqo1bXz62nyLvQ9zXgXKW55TbxmdAoMqjnMSFKS',
                                id: 15160995,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                delegate: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                slots: '{25}',
                            },
                        },
                        {
                            cursor: 'onfEmCLhbmrKanU1jSFTESgVjzTX7bw1dZBTZMK5TjtoU4poDoz:0',
                            node: {
                                hash: 'onfEmCLhbmrKanU1jSFTESgVjzTX7bw1dZBTZMK5TjtoU4poDoz',
                                batch_position: 0,
                                kind: 'endorsement',
                                timestamp: '2019-10-23T00:48:54Z',
                                level: 661798,
                                block: 'BM8WkqFG78LdEZ69AG1wioD658zMR1kQBCaBEnX9Qrqqk2H1d4Q',
                                id: 15164160,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                delegate: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                slots: '{23}',
                            },
                        },
                        {
                            cursor: 'onkBcrEqbAzaXAK9NRZfZs1joRv3D3CTnTE8nA7c5xAe1aTbVda:0',
                            node: {
                                hash: 'onkBcrEqbAzaXAK9NRZfZs1joRv3D3CTnTE8nA7c5xAe1aTbVda',
                                batch_position: 0,
                                kind: 'endorsement',
                                timestamp: '2019-10-23T04:53:03Z',
                                level: 662035,
                                block: 'BLdF7PRZ62YgTLkE7qmcgwKbdDFquCYNy1FeM48mCTLtAFza3we',
                                id: 15169786,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                delegate: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                slots: '{1}',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.endorsements first and date_range lte', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS") {
                address
                first_seen
                activated
                public_key
                endorsements (
                  first: 3
                  date_range: {
                    lte: "2019-10-22T18:56:24Z"
                  }
                ) {
                    page_info {
                        start_cursor
                        end_cursor
                        has_next_page
                        has_previous_page
                    }
                    edges {
                        cursor
                        node {
                            hash
                            batch_position
                            kind
                            timestamp
                            level
                            block
                            id
                            source
                            delegate
                            slots
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                first_seen: '2018-09-13T14:51:20Z',
                activated: '2018-09-13T14:49:20.000Z',
                public_key: null,
                endorsements: {
                    page_info: {
                        start_cursor: 'oo4zEKtirUFj89Qg3Vn691SQa4Lp84n6VNNUL9KDd6ZfXSCmU3q:0',
                        end_cursor: 'onxjQwuzc4AWHWsDVFnC4rJY1B3UMnajJNquW3bhs8mCu4WGCwP:0',
                        has_next_page: true,
                        has_previous_page: false,
                    },
                    edges: [
                        {
                            cursor: 'oo4zEKtirUFj89Qg3Vn691SQa4Lp84n6VNNUL9KDd6ZfXSCmU3q:0',
                            node: {
                                hash: 'oo4zEKtirUFj89Qg3Vn691SQa4Lp84n6VNNUL9KDd6ZfXSCmU3q',
                                batch_position: 0,
                                kind: 'endorsement',
                                timestamp: '2019-10-22T16:41:39Z',
                                level: 661319,
                                block: 'BMLqchUYAjYErYSW5DcAhNVQxWTJ7TtWLMyb4x6vvxRvkgmmrDX',
                                id: 15152486,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                delegate: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                slots: '{8}',
                            },
                        },
                        {
                            cursor: 'oohxAzy22hAxtjwx4ZgLBqZwnWghta3eYC7raWGDjrN7tGUEmAU:0',
                            node: {
                                hash: 'oohxAzy22hAxtjwx4ZgLBqZwnWghta3eYC7raWGDjrN7tGUEmAU',
                                batch_position: 0,
                                kind: 'endorsement',
                                timestamp: '2019-10-22T14:24:30Z',
                                level: 661187,
                                block: 'BL1X1fediWJn97dbHFNVG3ov46Snr5on2ohJAYGDPfVwMj8gwpZ',
                                id: 15149373,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                delegate: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                slots: '{12}',
                            },
                        },
                        {
                            cursor: 'onxjQwuzc4AWHWsDVFnC4rJY1B3UMnajJNquW3bhs8mCu4WGCwP:0',
                            node: {
                                hash: 'onxjQwuzc4AWHWsDVFnC4rJY1B3UMnajJNquW3bhs8mCu4WGCwP',
                                batch_position: 0,
                                kind: 'endorsement',
                                timestamp: '2019-10-22T09:22:24Z',
                                level: 660895,
                                block: 'BMW7KFf8TPgjbMDnxabR77gpg5DzEcYWuZcRNBXQZwR6J5xkJXx',
                                id: 15142272,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                delegate: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                slots: '{10}',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.endorsements first and date_range lte and before', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS") {
                address
                first_seen
                activated
                public_key
                endorsements (
                  first: 3
                  date_range: {
                    lte: "2019-10-22T18:56:24Z"
                  }
                  before: "onxjQwuzc4AWHWsDVFnC4rJY1B3UMnajJNquW3bhs8mCu4WGCwP:0"
                ) {
                    page_info {
                        start_cursor
                        end_cursor
                        has_next_page
                        has_previous_page
                    }
                    edges {
                        cursor
                        node {
                            hash
                            batch_position
                            kind
                            timestamp
                            level
                            block
                            id
                            source
                            delegate
                            slots
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                first_seen: '2018-09-13T14:51:20Z',
                activated: '2018-09-13T14:49:20.000Z',
                public_key: null,
                endorsements: {
                    page_info: {
                        start_cursor: 'oo4zEKtirUFj89Qg3Vn691SQa4Lp84n6VNNUL9KDd6ZfXSCmU3q:0',
                        end_cursor: 'oohxAzy22hAxtjwx4ZgLBqZwnWghta3eYC7raWGDjrN7tGUEmAU:0',
                        has_next_page: true,
                        has_previous_page: false,
                    },
                    edges: [
                        {
                            cursor: 'oo4zEKtirUFj89Qg3Vn691SQa4Lp84n6VNNUL9KDd6ZfXSCmU3q:0',
                            node: {
                                hash: 'oo4zEKtirUFj89Qg3Vn691SQa4Lp84n6VNNUL9KDd6ZfXSCmU3q',
                                batch_position: 0,
                                kind: 'endorsement',
                                timestamp: '2019-10-22T16:41:39Z',
                                level: 661319,
                                block: 'BMLqchUYAjYErYSW5DcAhNVQxWTJ7TtWLMyb4x6vvxRvkgmmrDX',
                                id: 15152486,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                delegate: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                slots: '{8}',
                            },
                        },
                        {
                            cursor: 'oohxAzy22hAxtjwx4ZgLBqZwnWghta3eYC7raWGDjrN7tGUEmAU:0',
                            node: {
                                hash: 'oohxAzy22hAxtjwx4ZgLBqZwnWghta3eYC7raWGDjrN7tGUEmAU',
                                batch_position: 0,
                                kind: 'endorsement',
                                timestamp: '2019-10-22T14:24:30Z',
                                level: 661187,
                                block: 'BL1X1fediWJn97dbHFNVG3ov46Snr5on2ohJAYGDPfVwMj8gwpZ',
                                id: 15149373,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                delegate: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                slots: '{12}',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.endorsements first and date_range lte and after', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS") {
                address
                first_seen
                activated
                public_key
                endorsements (
                  first: 3
                  date_range: {
                    lte: "2019-10-22T18:56:24Z"
                  }
                  after: "oohxAzy22hAxtjwx4ZgLBqZwnWghta3eYC7raWGDjrN7tGUEmAU:0"
                ) {
                    page_info {
                        start_cursor
                        end_cursor
                        has_next_page
                        has_previous_page
                    }
                    edges {
                        cursor
                        node {
                            hash
                            batch_position
                            kind
                            timestamp
                            level
                            block
                            id
                            source
                            delegate
                            slots
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                first_seen: '2018-09-13T14:51:20Z',
                activated: '2018-09-13T14:49:20.000Z',
                public_key: null,
                endorsements: {
                    page_info: {
                        start_cursor: 'onxjQwuzc4AWHWsDVFnC4rJY1B3UMnajJNquW3bhs8mCu4WGCwP:0',
                        end_cursor: 'ooaBA1ktquTAs72WydPUZxokanF3nJ2psZLDacdqreRjZDHTe1P:0',
                        has_next_page: true,
                        has_previous_page: true,
                    },
                    edges: [
                        {
                            cursor: 'onxjQwuzc4AWHWsDVFnC4rJY1B3UMnajJNquW3bhs8mCu4WGCwP:0',
                            node: {
                                hash: 'onxjQwuzc4AWHWsDVFnC4rJY1B3UMnajJNquW3bhs8mCu4WGCwP',
                                batch_position: 0,
                                kind: 'endorsement',
                                timestamp: '2019-10-22T09:22:24Z',
                                level: 660895,
                                block: 'BMW7KFf8TPgjbMDnxabR77gpg5DzEcYWuZcRNBXQZwR6J5xkJXx',
                                id: 15142272,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                delegate: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                slots: '{10}',
                            },
                        },
                        {
                            cursor: 'opUXrvjJo6KfNX4kbRfmqUjNQpVuVuTedbpNoxHXKofqWNrsa33:0',
                            node: {
                                hash: 'opUXrvjJo6KfNX4kbRfmqUjNQpVuVuTedbpNoxHXKofqWNrsa33',
                                batch_position: 0,
                                kind: 'endorsement',
                                timestamp: '2019-10-22T07:57:04Z',
                                level: 660811,
                                block: 'BMDJDXfUTjHaQevUYb9k4B2SZDcYS5CmhvKdHnkgHc4aFr5n7JL',
                                id: 15140261,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                delegate: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                slots: '{6}',
                            },
                        },
                        {
                            cursor: 'ooaBA1ktquTAs72WydPUZxokanF3nJ2psZLDacdqreRjZDHTe1P:0',
                            node: {
                                hash: 'ooaBA1ktquTAs72WydPUZxokanF3nJ2psZLDacdqreRjZDHTe1P',
                                batch_position: 0,
                                kind: 'endorsement',
                                timestamp: '2019-10-22T06:06:50Z',
                                level: 660706,
                                block: 'BLwCBrv7GZ3vTgJi4c8ZBBAhavv2L9FkGeK6J9rWHv46BN9ugyk',
                                id: 15137497,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                delegate: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                slots: '{2}',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.endorsements first and date_range lte and order_by', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS") {
                address
                first_seen
                activated
                public_key
                endorsements (
                    first: 3
                    date_range: {
                    lte: "2019-10-22T18:56:24Z"
                    }
                    order_by: {field: ID, direction: ASC}
                ) {
                    page_info {
                        start_cursor
                        end_cursor
                        has_next_page
                        has_previous_page
                    }
                    edges {
                        cursor
                        node {
                            hash
                            batch_position
                            kind
                            timestamp
                            level
                            block
                            id
                            source
                            delegate
                            slots
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                first_seen: '2018-09-13T14:51:20Z',
                activated: '2018-09-13T14:49:20.000Z',
                public_key: null,
                endorsements: {
                    page_info: {
                        start_cursor: 'opZSatngKMGpcxgEGy4if5uRS4fCh6p4p3ab9KEuoDZBGQBvGBJ:0',
                        end_cursor: 'onuTjTGY85y9YpwfEPsquyxqNY7AkQNpGqU2JwU4c4ia2RbZ79Y:0',
                        has_next_page: true,
                        has_previous_page: false,
                    },
                    edges: [
                        {
                            cursor: 'opZSatngKMGpcxgEGy4if5uRS4fCh6p4p3ab9KEuoDZBGQBvGBJ:0',
                            node: {
                                hash: 'opZSatngKMGpcxgEGy4if5uRS4fCh6p4p3ab9KEuoDZBGQBvGBJ',
                                batch_position: 0,
                                kind: 'endorsement',
                                timestamp: '2018-10-04T18:15:29Z',
                                level: 131391,
                                block: 'BKrmjm6kRWWkPiccPDPJeB7nWUr561oVCzAWq4DWLe88RweRVAU',
                                id: 2042670,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                delegate: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                slots: '{24}',
                            },
                        },
                        {
                            cursor: 'ooTcJtDdEVWTaHQxEPZr2Wf47e3w4g4j4zFKq9kJuTxo4UKrrXs:0',
                            node: {
                                hash: 'ooTcJtDdEVWTaHQxEPZr2Wf47e3w4g4j4zFKq9kJuTxo4UKrrXs',
                                batch_position: 0,
                                kind: 'endorsement',
                                timestamp: '2018-10-04T18:39:29Z',
                                level: 131415,
                                block: 'BL3KMddQ78skAfM7saXk3c992Z8nvH8Lh4Tf72QTk6rnJg6RRSX',
                                id: 2043192,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                delegate: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                slots: '{25}',
                            },
                        },
                        {
                            cursor: 'onuTjTGY85y9YpwfEPsquyxqNY7AkQNpGqU2JwU4c4ia2RbZ79Y:0',
                            node: {
                                hash: 'onuTjTGY85y9YpwfEPsquyxqNY7AkQNpGqU2JwU4c4ia2RbZ79Y',
                                batch_position: 0,
                                kind: 'endorsement',
                                timestamp: '2018-10-06T09:26:29Z',
                                level: 133692,
                                block: 'BMNXM16a9gdiicXytKkJ2L16ehTsphrLJNdRjA5g9X75cbFEsMQ',
                                id: 2090560,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                delegate: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                slots: '{13}',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.endorsements first and date_range gte/lte', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS") {
                address
                first_seen
                activated
                public_key
                endorsements (
                  first: 3
                  date_range: {
                    gte: "2019-10-22T18:56:24Z"
                    lte: "2019-10-27T12:46:11Z"
                  }
                ) {
                    page_info {
                        start_cursor
                        end_cursor
                        has_next_page
                        has_previous_page
                    }
                    edges {
                        cursor
                        node {
                            hash
                            batch_position
                            kind
                            timestamp
                            level
                            block
                            id
                            source
                            delegate
                            slots
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                first_seen: '2018-09-13T14:51:20Z',
                activated: '2018-09-13T14:49:20.000Z',
                public_key: null,
                endorsements: {
                    page_info: {
                        start_cursor: 'opZn2cJbqpULLcRsJJCqLRE2bCAJAK7E1cs13MDT7nTxSNrxJ8i:0',
                        end_cursor: 'oo8ycUqrUVcp9V2JnkSFdB4uD4UDRMmrfVhTJo3Nkd9J8Z7DCdu:0',
                        has_next_page: true,
                        has_previous_page: false,
                    },
                    edges: [
                        {
                            cursor: 'opZn2cJbqpULLcRsJJCqLRE2bCAJAK7E1cs13MDT7nTxSNrxJ8i:0',
                            node: {
                                hash: 'opZn2cJbqpULLcRsJJCqLRE2bCAJAK7E1cs13MDT7nTxSNrxJ8i',
                                batch_position: 0,
                                kind: 'endorsement',
                                timestamp: '2019-10-27T08:44:19Z',
                                level: 667863,
                                block: 'BMMzWra66QyPGWM465uTWz6CewzrzbrK8XJFQUnQ6Fj7owNGP4M',
                                id: 15315917,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                delegate: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                slots: '{25}',
                            },
                        },
                        {
                            cursor: 'ooBNdbbRduUCHU5fTEjbfDS8AcyXPgCk4skMbhX42NJzhSKyfpt:0',
                            node: {
                                hash: 'ooBNdbbRduUCHU5fTEjbfDS8AcyXPgCk4skMbhX42NJzhSKyfpt',
                                batch_position: 0,
                                kind: 'endorsement',
                                timestamp: '2019-10-27T06:19:39Z',
                                level: 667721,
                                block: 'BLXwcgT4Qe88sJuQYvq4jfaNp8NKWABHG18zjsFZaaKrprruhAQ',
                                id: 15310994,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                delegate: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                slots: '{25}',
                            },
                        },
                        {
                            cursor: 'oo8ycUqrUVcp9V2JnkSFdB4uD4UDRMmrfVhTJo3Nkd9J8Z7DCdu:0',
                            node: {
                                hash: 'oo8ycUqrUVcp9V2JnkSFdB4uD4UDRMmrfVhTJo3Nkd9J8Z7DCdu',
                                batch_position: 0,
                                kind: 'endorsement',
                                timestamp: '2019-10-26T23:40:09Z',
                                level: 667327,
                                block: 'BM6jzwZ9nRU7k1wa7JMGKghP4Sxa9LQ3cjoZUjx4qSfamN67AUC',
                                id: 15300988,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                delegate: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                slots: '{21}',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.endorsements first and date_range gte/lte and before', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS") {
                address
                first_seen
                activated
                public_key
                endorsements (
                  first: 3
                  date_range: {
                    gte: "2019-10-22T18:56:24Z"
                    lte: "2019-10-27T12:46:11Z"
                  }
                  before: "oo8ycUqrUVcp9V2JnkSFdB4uD4UDRMmrfVhTJo3Nkd9J8Z7DCdu:0"
                ) {
                    page_info {
                        start_cursor
                        end_cursor
                        has_next_page
                        has_previous_page
                    }
                    edges {
                        cursor
                        node {
                            hash
                            batch_position
                            kind
                            timestamp
                            level
                            block
                            id
                            source
                            delegate
                            slots
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                first_seen: '2018-09-13T14:51:20Z',
                activated: '2018-09-13T14:49:20.000Z',
                public_key: null,
                endorsements: {
                    page_info: {
                        start_cursor: 'opZn2cJbqpULLcRsJJCqLRE2bCAJAK7E1cs13MDT7nTxSNrxJ8i:0',
                        end_cursor: 'ooBNdbbRduUCHU5fTEjbfDS8AcyXPgCk4skMbhX42NJzhSKyfpt:0',
                        has_next_page: true,
                        has_previous_page: false,
                    },
                    edges: [
                        {
                            cursor: 'opZn2cJbqpULLcRsJJCqLRE2bCAJAK7E1cs13MDT7nTxSNrxJ8i:0',
                            node: {
                                hash: 'opZn2cJbqpULLcRsJJCqLRE2bCAJAK7E1cs13MDT7nTxSNrxJ8i',
                                batch_position: 0,
                                kind: 'endorsement',
                                timestamp: '2019-10-27T08:44:19Z',
                                level: 667863,
                                block: 'BMMzWra66QyPGWM465uTWz6CewzrzbrK8XJFQUnQ6Fj7owNGP4M',
                                id: 15315917,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                delegate: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                slots: '{25}',
                            },
                        },
                        {
                            cursor: 'ooBNdbbRduUCHU5fTEjbfDS8AcyXPgCk4skMbhX42NJzhSKyfpt:0',
                            node: {
                                hash: 'ooBNdbbRduUCHU5fTEjbfDS8AcyXPgCk4skMbhX42NJzhSKyfpt',
                                batch_position: 0,
                                kind: 'endorsement',
                                timestamp: '2019-10-27T06:19:39Z',
                                level: 667721,
                                block: 'BLXwcgT4Qe88sJuQYvq4jfaNp8NKWABHG18zjsFZaaKrprruhAQ',
                                id: 15310994,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                delegate: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                slots: '{25}',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.endorsements first and date_range gte/lte and after', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS") {
                address
                first_seen
                activated
                public_key
                endorsements (
                  first: 3
                  date_range: {
                    gte: "2019-10-22T18:56:24Z"
                    lte: "2019-10-27T12:46:11Z"
                  }
                  after: "oo8ycUqrUVcp9V2JnkSFdB4uD4UDRMmrfVhTJo3Nkd9J8Z7DCdu:0"
                ) {
                    page_info {
                        start_cursor
                        end_cursor
                        has_next_page
                        has_previous_page
                    }
                    edges {
                        cursor
                        node {
                            hash
                            batch_position
                            kind
                            timestamp
                            level
                            block
                            id
                            source
                            delegate
                            slots
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                first_seen: '2018-09-13T14:51:20Z',
                activated: '2018-09-13T14:49:20.000Z',
                public_key: null,
                endorsements: {
                    page_info: {
                        start_cursor: 'ooWNszkjWUQXiLvEbpi1jGvN2psmKBHjsUQExMhZGUzjtc3LjS4:0',
                        end_cursor: 'opWWLfccVoxCdJPc1ZheWsQSkiY53fey5obujMb926XRZE3QhL6:0',
                        has_next_page: true,
                        has_previous_page: true,
                    },
                    edges: [
                        {
                            cursor: 'ooWNszkjWUQXiLvEbpi1jGvN2psmKBHjsUQExMhZGUzjtc3LjS4:0',
                            node: {
                                hash: 'ooWNszkjWUQXiLvEbpi1jGvN2psmKBHjsUQExMhZGUzjtc3LjS4',
                                batch_position: 0,
                                kind: 'endorsement',
                                timestamp: '2019-10-26T23:09:53Z',
                                level: 667299,
                                block: 'BLq6Pgw1qdtWr2NTY8Xjke2gPDhAhM8Pg8cP7BidmZp34ywVtp5',
                                id: 15300287,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                delegate: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                slots: '{13}',
                            },
                        },
                        {
                            cursor: 'ooUZd3kSL8Z2TRzjHufuU3fiG7kzuFMoyRLD4WRhTtdNtLDzrx1:0',
                            node: {
                                hash: 'ooUZd3kSL8Z2TRzjHufuU3fiG7kzuFMoyRLD4WRhTtdNtLDzrx1',
                                batch_position: 0,
                                kind: 'endorsement',
                                timestamp: '2019-10-26T22:00:13Z',
                                level: 667230,
                                block: 'BM29ZYK6A5vcos3wet81nDJNXjvjj1CtRizXoTcs8MA7cWmiYCE',
                                id: 15298652,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                delegate: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                slots: '{29}',
                            },
                        },
                        {
                            cursor: 'opWWLfccVoxCdJPc1ZheWsQSkiY53fey5obujMb926XRZE3QhL6:0',
                            node: {
                                hash: 'opWWLfccVoxCdJPc1ZheWsQSkiY53fey5obujMb926XRZE3QhL6',
                                batch_position: 0,
                                kind: 'endorsement',
                                timestamp: '2019-10-26T21:50:13Z',
                                level: 667220,
                                block: 'BMGwWQQRudhVukDipaUHcwymUUtm3aA1471UCiuovm5WKA5ivjd',
                                id: 15298422,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                delegate: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                slots: '{5}',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.endorsements first and date_range gte/lte and order_by', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS") {
                address
                first_seen
                activated
                public_key
                endorsements (
                    first: 3
                    date_range: {
                    gte: "2019-10-22T18:56:24Z"
                    lte: "2019-10-27T12:46:11Z"
                    }
                    order_by: {field: ID, direction: ASC}
                ) {
                    page_info {
                        start_cursor
                        end_cursor
                        has_next_page
                        has_previous_page
                    }
                    edges {
                        cursor
                        node {
                            hash
                            batch_position
                            kind
                            timestamp
                            level
                            block
                            id
                            source
                            delegate
                            slots
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                first_seen: '2018-09-13T14:51:20Z',
                activated: '2018-09-13T14:49:20.000Z',
                public_key: null,
                endorsements: {
                    page_info: {
                        start_cursor: 'onsSSmnhHAX1ARcp6WLGuwJVNr7QDrVTggM3iDsTee2UvTjDPZR:0',
                        end_cursor: 'onkBcrEqbAzaXAK9NRZfZs1joRv3D3CTnTE8nA7c5xAe1aTbVda:0',
                        has_next_page: true,
                        has_previous_page: false,
                    },
                    edges: [
                        {
                            cursor: 'onsSSmnhHAX1ARcp6WLGuwJVNr7QDrVTggM3iDsTee2UvTjDPZR:0',
                            node: {
                                hash: 'onsSSmnhHAX1ARcp6WLGuwJVNr7QDrVTggM3iDsTee2UvTjDPZR',
                                batch_position: 0,
                                kind: 'endorsement',
                                timestamp: '2019-10-22T22:34:49Z',
                                level: 661666,
                                block: 'BMCx8jtyQBnkoqo1bXz62nyLvQ9zXgXKW55TbxmdAoMqjnMSFKS',
                                id: 15160995,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                delegate: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                slots: '{25}',
                            },
                        },
                        {
                            cursor: 'onfEmCLhbmrKanU1jSFTESgVjzTX7bw1dZBTZMK5TjtoU4poDoz:0',
                            node: {
                                hash: 'onfEmCLhbmrKanU1jSFTESgVjzTX7bw1dZBTZMK5TjtoU4poDoz',
                                batch_position: 0,
                                kind: 'endorsement',
                                timestamp: '2019-10-23T00:48:54Z',
                                level: 661798,
                                block: 'BM8WkqFG78LdEZ69AG1wioD658zMR1kQBCaBEnX9Qrqqk2H1d4Q',
                                id: 15164160,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                delegate: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                slots: '{23}',
                            },
                        },
                        {
                            cursor: 'onkBcrEqbAzaXAK9NRZfZs1joRv3D3CTnTE8nA7c5xAe1aTbVda:0',
                            node: {
                                hash: 'onkBcrEqbAzaXAK9NRZfZs1joRv3D3CTnTE8nA7c5xAe1aTbVda',
                                batch_position: 0,
                                kind: 'endorsement',
                                timestamp: '2019-10-23T04:53:03Z',
                                level: 662035,
                                block: 'BLdF7PRZ62YgTLkE7qmcgwKbdDFquCYNy1FeM48mCTLtAFza3we',
                                id: 15169786,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                delegate: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                slots: '{1}',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.endorsements first and date_range gte and order_by and before', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS") {
                address
                first_seen
                activated
                public_key
                endorsements (
                  first: 3
                  order_by: {field: ID, direction: ASC}
                  before: "opST2sKbzb4in9YWfzSd6rmpeX3sYqKyQ25XMjoK43tTYzsV727:0"
                  date_range: {
                    gte: "2019-10-27T12:46:11Z"
                  }
                ) {
                    page_info {
                        start_cursor
                        end_cursor
                        has_next_page
                        has_previous_page
                    }
                    edges {
                        cursor
                        node {
                            hash
                            batch_position
                            kind
                            timestamp
                            level
                            block
                            id
                            source
                            delegate
                            slots
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                first_seen: '2018-09-13T14:51:20Z',
                activated: '2018-09-13T14:49:20.000Z',
                public_key: null,
                endorsements: {
                    page_info: {
                        start_cursor: 'oooq6KLUqxoyNVpToVGbqq1vFiLJBnkfQnF8wzhyT2j6Vx4XPbG:0',
                        end_cursor: 'op5NzHd38gLbvGHp9XoG21NCsqMPxswigXTrq4bvSzzhK7pfBSp:0',
                        has_next_page: true,
                        has_previous_page: false,
                    },
                    edges: [
                        {
                            cursor: 'oooq6KLUqxoyNVpToVGbqq1vFiLJBnkfQnF8wzhyT2j6Vx4XPbG:0',
                            node: {
                                hash: 'oooq6KLUqxoyNVpToVGbqq1vFiLJBnkfQnF8wzhyT2j6Vx4XPbG',
                                batch_position: 0,
                                kind: 'endorsement',
                                timestamp: '2019-10-27T12:53:51Z',
                                level: 668107,
                                block: 'BLTHHeH6qXvrSJdjTWmX1UGr3uJ7nX5zBmHgx1ntRzQBF9FBD6B',
                                id: 15322273,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                delegate: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                slots: '{26}',
                            },
                        },
                        {
                            cursor: 'opPCPkT3V97XHqumey6Z7nhp8eNLy46YMnELTDtm8ma3Uoip51a:0',
                            node: {
                                hash: 'opPCPkT3V97XHqumey6Z7nhp8eNLy46YMnELTDtm8ma3Uoip51a',
                                batch_position: 0,
                                kind: 'endorsement',
                                timestamp: '2019-10-27T16:15:56Z',
                                level: 668307,
                                block: 'BKtFqq5akpZy7ADLLHUGYbwn4PnrfKsbvLxTiPK6Q2vgNJT3E2b',
                                id: 15327442,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                delegate: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                slots: '{12}',
                            },
                        },
                        {
                            cursor: 'op5NzHd38gLbvGHp9XoG21NCsqMPxswigXTrq4bvSzzhK7pfBSp:0',
                            node: {
                                hash: 'op5NzHd38gLbvGHp9XoG21NCsqMPxswigXTrq4bvSzzhK7pfBSp',
                                batch_position: 0,
                                kind: 'endorsement',
                                timestamp: '2019-10-27T16:53:56Z',
                                level: 668343,
                                block: 'BKwmUtMmxXcyJfQ858LMzi42y6rU1eTop4Fa5todrVGeUZAWsui',
                                id: 15328409,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                delegate: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                slots: '{17}',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.endorsements first and date_range gte and order_by and after', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS") {
                address
                first_seen
                activated
                public_key
                endorsements (
                  first: 3
                  order_by: {field: ID, direction: ASC}
                  after: "opPCPkT3V97XHqumey6Z7nhp8eNLy46YMnELTDtm8ma3Uoip51a:0"
                  date_range: {
                    gte: "2019-10-27T12:46:11Z"
                  }
                ) {
                    page_info {
                        start_cursor
                        end_cursor
                        has_next_page
                        has_previous_page
                    }
                    edges {
                        cursor
                        node {
                            hash
                            batch_position
                            kind
                            timestamp
                            level
                            block
                            id
                            source
                            delegate
                            slots
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                first_seen: '2018-09-13T14:51:20Z',
                activated: '2018-09-13T14:49:20.000Z',
                public_key: null,
                endorsements: {
                    page_info: {
                        start_cursor: 'op5NzHd38gLbvGHp9XoG21NCsqMPxswigXTrq4bvSzzhK7pfBSp:0',
                        end_cursor: 'opLsDNGb4JDwcZ8MydkGc97xJJchnr9LNdY3JYnLi9f92LYzgqm:0',
                        has_next_page: true,
                        has_previous_page: true,
                    },
                    edges: [
                        {
                            cursor: 'op5NzHd38gLbvGHp9XoG21NCsqMPxswigXTrq4bvSzzhK7pfBSp:0',
                            node: {
                                hash: 'op5NzHd38gLbvGHp9XoG21NCsqMPxswigXTrq4bvSzzhK7pfBSp',
                                batch_position: 0,
                                kind: 'endorsement',
                                timestamp: '2019-10-27T16:53:56Z',
                                level: 668343,
                                block: 'BKwmUtMmxXcyJfQ858LMzi42y6rU1eTop4Fa5todrVGeUZAWsui',
                                id: 15328409,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                delegate: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                slots: '{17}',
                            },
                        },
                        {
                            cursor: 'opMTjmmjpnX6ZRTpNBEuEXXG1wvtmpX4nRYda8GwTtR5kthTkXz:0',
                            node: {
                                hash: 'opMTjmmjpnX6ZRTpNBEuEXXG1wvtmpX4nRYda8GwTtR5kthTkXz',
                                batch_position: 0,
                                kind: 'endorsement',
                                timestamp: '2019-10-27T18:33:36Z',
                                level: 668440,
                                block: 'BMbNvQ1EJJUTSKgkkjXRTTpfE6pbogfaFEq9S4xS8GqtqhH49in',
                                id: 15330905,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                delegate: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                slots: '{7}',
                            },
                        },
                        {
                            cursor: 'opLsDNGb4JDwcZ8MydkGc97xJJchnr9LNdY3JYnLi9f92LYzgqm:0',
                            node: {
                                hash: 'opLsDNGb4JDwcZ8MydkGc97xJJchnr9LNdY3JYnLi9f92LYzgqm',
                                batch_position: 0,
                                kind: 'endorsement',
                                timestamp: '2019-10-27T18:39:36Z',
                                level: 668446,
                                block: 'BLKLcFU1pTZeYa5fBkcdfYDw6fKz5xbEQLDHS6TnczDSqgcm122',
                                id: 15331054,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                delegate: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                slots: '{26}',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.endorsements first and date_range lte and order_by and before', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS") {
                address
                first_seen
                activated
                public_key
                endorsements (
                  first: 3
                  order_by: {field: ID, direction: ASC}
                  before: "opMTjmmjpnX6ZRTpNBEuEXXG1wvtmpX4nRYda8GwTtR5kthTkXz:0"
                  date_range: {
                    lte: "2019-10-27T12:46:11Z"
                  }
                ) {
                    page_info {
                        start_cursor
                        end_cursor
                        has_next_page
                        has_previous_page
                    }
                    edges {
                        cursor
                        node {
                            hash
                            batch_position
                            kind
                            timestamp
                            level
                            block
                            id
                            source
                            delegate
                            slots
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                first_seen: '2018-09-13T14:51:20Z',
                activated: '2018-09-13T14:49:20.000Z',
                public_key: null,
                endorsements: {
                    page_info: {
                        start_cursor: 'opZSatngKMGpcxgEGy4if5uRS4fCh6p4p3ab9KEuoDZBGQBvGBJ:0',
                        end_cursor: 'onuTjTGY85y9YpwfEPsquyxqNY7AkQNpGqU2JwU4c4ia2RbZ79Y:0',
                        has_next_page: true,
                        has_previous_page: false,
                    },
                    edges: [
                        {
                            cursor: 'opZSatngKMGpcxgEGy4if5uRS4fCh6p4p3ab9KEuoDZBGQBvGBJ:0',
                            node: {
                                hash: 'opZSatngKMGpcxgEGy4if5uRS4fCh6p4p3ab9KEuoDZBGQBvGBJ',
                                batch_position: 0,
                                kind: 'endorsement',
                                timestamp: '2018-10-04T18:15:29Z',
                                level: 131391,
                                block: 'BKrmjm6kRWWkPiccPDPJeB7nWUr561oVCzAWq4DWLe88RweRVAU',
                                id: 2042670,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                delegate: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                slots: '{24}',
                            },
                        },
                        {
                            cursor: 'ooTcJtDdEVWTaHQxEPZr2Wf47e3w4g4j4zFKq9kJuTxo4UKrrXs:0',
                            node: {
                                hash: 'ooTcJtDdEVWTaHQxEPZr2Wf47e3w4g4j4zFKq9kJuTxo4UKrrXs',
                                batch_position: 0,
                                kind: 'endorsement',
                                timestamp: '2018-10-04T18:39:29Z',
                                level: 131415,
                                block: 'BL3KMddQ78skAfM7saXk3c992Z8nvH8Lh4Tf72QTk6rnJg6RRSX',
                                id: 2043192,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                delegate: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                slots: '{25}',
                            },
                        },
                        {
                            cursor: 'onuTjTGY85y9YpwfEPsquyxqNY7AkQNpGqU2JwU4c4ia2RbZ79Y:0',
                            node: {
                                hash: 'onuTjTGY85y9YpwfEPsquyxqNY7AkQNpGqU2JwU4c4ia2RbZ79Y',
                                batch_position: 0,
                                kind: 'endorsement',
                                timestamp: '2018-10-06T09:26:29Z',
                                level: 133692,
                                block: 'BMNXM16a9gdiicXytKkJ2L16ehTsphrLJNdRjA5g9X75cbFEsMQ',
                                id: 2090560,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                delegate: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                slots: '{13}',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.endorsements first and date_range lte and order_by and after', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS") {
                address
                first_seen
                activated
                public_key
                endorsements (
                  first: 3
                  order_by: {field: ID, direction: ASC}
                  after: "onuTjTGY85y9YpwfEPsquyxqNY7AkQNpGqU2JwU4c4ia2RbZ79Y:0"
                  date_range: {
                    lte: "2019-10-27T12:46:11Z"
                  }
                ) {
                    page_info {
                        start_cursor
                        end_cursor
                        has_next_page
                        has_previous_page
                    }
                    edges {
                        cursor
                        node {
                            hash
                            batch_position
                            kind
                            timestamp
                            level
                            block
                            id
                            source
                            delegate
                            slots
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                first_seen: '2018-09-13T14:51:20Z',
                activated: '2018-09-13T14:49:20.000Z',
                public_key: null,
                endorsements: {
                    page_info: {
                        start_cursor: 'oos6eeoCBxGw5FapEkgYbXChd3wCx5SBd1RkYj7pCbkJYk3TGV6:0',
                        end_cursor: 'ooEZio9pGUCk6REjBDw7N3FVG6e6vWwFneG8cDzoR5yJjUgCjQR:0',
                        has_next_page: true,
                        has_previous_page: true,
                    },
                    edges: [
                        {
                            cursor: 'oos6eeoCBxGw5FapEkgYbXChd3wCx5SBd1RkYj7pCbkJYk3TGV6:0',
                            node: {
                                hash: 'oos6eeoCBxGw5FapEkgYbXChd3wCx5SBd1RkYj7pCbkJYk3TGV6',
                                batch_position: 0,
                                kind: 'endorsement',
                                timestamp: '2018-10-08T04:20:47Z',
                                level: 136215,
                                block: 'BLRexj3qJnWw19BEpw9e6NDmf8iVLeTpnjhjTGwwXuqq63MJdU5',
                                id: 2145056,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                delegate: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                slots: '{9}',
                            },
                        },
                        {
                            cursor: 'oohZBRc7TMQ6peAP8wedZnZSW14a7tevnzLNHVATATKfvgWeQJf:0',
                            node: {
                                hash: 'oohZBRc7TMQ6peAP8wedZnZSW14a7tevnzLNHVATATKfvgWeQJf',
                                batch_position: 0,
                                kind: 'endorsement',
                                timestamp: '2018-10-10T06:03:02Z',
                                level: 139136,
                                block: 'BLTs3iVYT7aw38h61KZ8LrUzAh1AmUXLAjgifRGB4ZB78276YRX',
                                id: 2206336,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                delegate: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                slots: '{11}',
                            },
                        },
                        {
                            cursor: 'ooEZio9pGUCk6REjBDw7N3FVG6e6vWwFneG8cDzoR5yJjUgCjQR:0',
                            node: {
                                hash: 'ooEZio9pGUCk6REjBDw7N3FVG6e6vWwFneG8cDzoR5yJjUgCjQR',
                                batch_position: 0,
                                kind: 'endorsement',
                                timestamp: '2018-10-10T20:21:09Z',
                                level: 139965,
                                block: 'BKnG9AfDm8jXJ7c9dVA5QAP8tD8dTA9jbCePMCpS9Pb4DQrbKWJ',
                                id: 2225474,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                delegate: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                slots: '{28}',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.endorsements first and date_range gte/lte and order_by and before', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS") {
                address
                first_seen
                activated
                public_key
                endorsements (
                  first: 3
                  order_by: {field: ID, direction: ASC}
                  before: "onkBcrEqbAzaXAK9NRZfZs1joRv3D3CTnTE8nA7c5xAe1aTbVda:0"
                  date_range: {
                    gte: "2019-10-22T18:56:24Z"
                    lte: "2019-10-27T12:46:11Z"
                  }
                ) {
                    page_info {
                        start_cursor
                        end_cursor
                        has_next_page
                        has_previous_page
                    }
                    edges {
                        cursor
                        node {
                            hash
                            batch_position
                            kind
                            timestamp
                            level
                            block
                            id
                            source
                            delegate
                            slots
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                first_seen: '2018-09-13T14:51:20Z',
                activated: '2018-09-13T14:49:20.000Z',
                public_key: null,
                endorsements: {
                    page_info: {
                        start_cursor: 'onsSSmnhHAX1ARcp6WLGuwJVNr7QDrVTggM3iDsTee2UvTjDPZR:0',
                        end_cursor: 'onfEmCLhbmrKanU1jSFTESgVjzTX7bw1dZBTZMK5TjtoU4poDoz:0',
                        has_next_page: true,
                        has_previous_page: false,
                    },
                    edges: [
                        {
                            cursor: 'onsSSmnhHAX1ARcp6WLGuwJVNr7QDrVTggM3iDsTee2UvTjDPZR:0',
                            node: {
                                hash: 'onsSSmnhHAX1ARcp6WLGuwJVNr7QDrVTggM3iDsTee2UvTjDPZR',
                                batch_position: 0,
                                kind: 'endorsement',
                                timestamp: '2019-10-22T22:34:49Z',
                                level: 661666,
                                block: 'BMCx8jtyQBnkoqo1bXz62nyLvQ9zXgXKW55TbxmdAoMqjnMSFKS',
                                id: 15160995,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                delegate: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                slots: '{25}',
                            },
                        },
                        {
                            cursor: 'onfEmCLhbmrKanU1jSFTESgVjzTX7bw1dZBTZMK5TjtoU4poDoz:0',
                            node: {
                                hash: 'onfEmCLhbmrKanU1jSFTESgVjzTX7bw1dZBTZMK5TjtoU4poDoz',
                                batch_position: 0,
                                kind: 'endorsement',
                                timestamp: '2019-10-23T00:48:54Z',
                                level: 661798,
                                block: 'BM8WkqFG78LdEZ69AG1wioD658zMR1kQBCaBEnX9Qrqqk2H1d4Q',
                                id: 15164160,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                delegate: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                slots: '{23}',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.endorsements first and date_range gte/lte and order_by and after', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS") {
                address
                first_seen
                activated
                public_key
                endorsements (
                  first: 3
                  order_by: {field: ID, direction: ASC}
                  after: "onkBcrEqbAzaXAK9NRZfZs1joRv3D3CTnTE8nA7c5xAe1aTbVda:0"
                  date_range: {
                    gte: "2019-10-22T18:56:24Z"
                    lte: "2019-10-27T12:46:11Z"
                  }
                ) {
                    page_info {
                        start_cursor
                        end_cursor
                        has_next_page
                        has_previous_page
                    }
                    edges {
                        cursor
                        node {
                            hash
                            batch_position
                            kind
                            timestamp
                            level
                            block
                            id
                            source
                            delegate
                            slots
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                first_seen: '2018-09-13T14:51:20Z',
                activated: '2018-09-13T14:49:20.000Z',
                public_key: null,
                endorsements: {
                    page_info: {
                        start_cursor: 'onoUfoDG1s1Ju2C57av4RqDrvr8ewYfzcTv6EHrhooskmFBRALP:0',
                        end_cursor: 'op6JMDKVdLoa8aWEV7eudAH47eGXfsG8Qj8fN2p5AGsqNUE42mX:0',
                        has_next_page: true,
                        has_previous_page: true,
                    },
                    edges: [
                        {
                            cursor: 'onoUfoDG1s1Ju2C57av4RqDrvr8ewYfzcTv6EHrhooskmFBRALP:0',
                            node: {
                                hash: 'onoUfoDG1s1Ju2C57av4RqDrvr8ewYfzcTv6EHrhooskmFBRALP',
                                batch_position: 0,
                                kind: 'endorsement',
                                timestamp: '2019-10-23T09:55:18Z',
                                level: 662329,
                                block: 'BLbhewf3S6nnPgAYaeRE4A31hJ81p3fy9gmCtJBkGh1sp1AuRkr',
                                id: 15176876,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                delegate: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                slots: '{11}',
                            },
                        },
                        {
                            cursor: 'oop3uFb4bB9DkSBw3kBfuutYTLneT54ZiJ1GkjVeRmSamJFRZEG:0',
                            node: {
                                hash: 'oop3uFb4bB9DkSBw3kBfuutYTLneT54ZiJ1GkjVeRmSamJFRZEG',
                                batch_position: 0,
                                kind: 'endorsement',
                                timestamp: '2019-10-23T10:08:18Z',
                                level: 662342,
                                block: 'BLXh62j3QVgbidFsmrfDNfJANA5hpm2DxjRYeiB4yAZVeGzCcZm',
                                id: 15177183,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                delegate: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                slots: '{10}',
                            },
                        },
                        {
                            cursor: 'op6JMDKVdLoa8aWEV7eudAH47eGXfsG8Qj8fN2p5AGsqNUE42mX:0',
                            node: {
                                hash: 'op6JMDKVdLoa8aWEV7eudAH47eGXfsG8Qj8fN2p5AGsqNUE42mX',
                                batch_position: 0,
                                kind: 'endorsement',
                                timestamp: '2019-10-23T10:45:49Z',
                                level: 662378,
                                block: 'BMb2bQamtVDXbPrsRXemZZXEuCiR5Y1W1yv8LEfvVgX75S2Y7m2',
                                id: 15178079,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                delegate: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                slots: '{20}',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.endorsements last', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS") {
                address
                first_seen
                activated
                public_key
                endorsements (last: 3) {
                    page_info {
                        start_cursor
                        end_cursor
                        has_next_page
                        has_previous_page
                    }
                    edges {
                        cursor
                        node {
                            hash
                            batch_position
                            kind
                            timestamp
                            level
                            block
                            id
                            source
                            delegate
                            slots
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                first_seen: '2018-09-13T14:51:20Z',
                activated: '2018-09-13T14:49:20.000Z',
                public_key: null,
                endorsements: {
                    page_info: {
                        start_cursor: 'onuTjTGY85y9YpwfEPsquyxqNY7AkQNpGqU2JwU4c4ia2RbZ79Y:0',
                        end_cursor: 'opZSatngKMGpcxgEGy4if5uRS4fCh6p4p3ab9KEuoDZBGQBvGBJ:0',
                        has_next_page: false,
                        has_previous_page: true,
                    },
                    edges: [
                        {
                            cursor: 'onuTjTGY85y9YpwfEPsquyxqNY7AkQNpGqU2JwU4c4ia2RbZ79Y:0',
                            node: {
                                hash: 'onuTjTGY85y9YpwfEPsquyxqNY7AkQNpGqU2JwU4c4ia2RbZ79Y',
                                batch_position: 0,
                                kind: 'endorsement',
                                timestamp: '2018-10-06T09:26:29Z',
                                level: 133692,
                                block: 'BMNXM16a9gdiicXytKkJ2L16ehTsphrLJNdRjA5g9X75cbFEsMQ',
                                id: 2090560,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                delegate: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                slots: '{13}',
                            },
                        },
                        {
                            cursor: 'ooTcJtDdEVWTaHQxEPZr2Wf47e3w4g4j4zFKq9kJuTxo4UKrrXs:0',
                            node: {
                                hash: 'ooTcJtDdEVWTaHQxEPZr2Wf47e3w4g4j4zFKq9kJuTxo4UKrrXs',
                                batch_position: 0,
                                kind: 'endorsement',
                                timestamp: '2018-10-04T18:39:29Z',
                                level: 131415,
                                block: 'BL3KMddQ78skAfM7saXk3c992Z8nvH8Lh4Tf72QTk6rnJg6RRSX',
                                id: 2043192,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                delegate: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                slots: '{25}',
                            },
                        },
                        {
                            cursor: 'opZSatngKMGpcxgEGy4if5uRS4fCh6p4p3ab9KEuoDZBGQBvGBJ:0',
                            node: {
                                hash: 'opZSatngKMGpcxgEGy4if5uRS4fCh6p4p3ab9KEuoDZBGQBvGBJ',
                                batch_position: 0,
                                kind: 'endorsement',
                                timestamp: '2018-10-04T18:15:29Z',
                                level: 131391,
                                block: 'BKrmjm6kRWWkPiccPDPJeB7nWUr561oVCzAWq4DWLe88RweRVAU',
                                id: 2042670,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                delegate: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                slots: '{24}',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.endorsements last and before', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS") {
                address
                first_seen
                activated
                public_key
                endorsements (
                last: 3
                before: "op6CjEdgjkTpcNyks1BdkM4DhNFXJLiE3coU3VekM9zdYwWcReL:0"
                ) {
                    page_info {
                        start_cursor
                        end_cursor
                        has_next_page
                        has_previous_page
                    }
                    edges {
                        cursor
                        node {
                            hash
                            batch_position
                            kind
                            timestamp
                            level
                            block
                            id
                            source
                            delegate
                            slots
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                first_seen: '2018-09-13T14:51:20Z',
                activated: '2018-09-13T14:49:20.000Z',
                public_key: null,
                endorsements: {
                    page_info: {
                        start_cursor: 'onxVkdzq8hDahsyqBhZx9mVzHmQTJ2zd1aQuQixiLTEtT9Syg95:0',
                        end_cursor: 'oo29bUnXuwrns8XcGTnthznafUJKnspNAWqgSuPvuheEsnFBP5x:0',
                        has_next_page: true,
                        has_previous_page: false,
                    },
                    edges: [
                        {
                            cursor: 'onxVkdzq8hDahsyqBhZx9mVzHmQTJ2zd1aQuQixiLTEtT9Syg95:0',
                            node: {
                                hash: 'onxVkdzq8hDahsyqBhZx9mVzHmQTJ2zd1aQuQixiLTEtT9Syg95',
                                batch_position: 0,
                                kind: 'endorsement',
                                timestamp: '2020-10-27T15:01:48Z',
                                level: 1189620,
                                block: 'BLCkf49KwQiJLjMiaQ92BeDfzUnLCRiH1E5KCMiEG6ukTwgCSUu',
                                id: 29011132,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                delegate: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                slots: '{23}',
                            },
                        },
                        {
                            cursor: 'oo29bUnXuwrns8XcGTnthznafUJKnspNAWqgSuPvuheEsnFBP5x:0',
                            node: {
                                hash: 'oo29bUnXuwrns8XcGTnthznafUJKnspNAWqgSuPvuheEsnFBP5x',
                                batch_position: 0,
                                kind: 'endorsement',
                                timestamp: '2020-10-25T19:02:22Z',
                                level: 1186992,
                                block: 'BM2YytRc8tQbyVxqQkCZejpGY4uuZwZq2W1nD8pBqoaWPEGkQ9K',
                                id: 28943109,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                delegate: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                slots: '{28}',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.endorsements last and after', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS") {
                address
                first_seen
                activated
                public_key
                endorsements (
                last: 3
                after: "op6CjEdgjkTpcNyks1BdkM4DhNFXJLiE3coU3VekM9zdYwWcReL:0"
                ) {
                    page_info {
                        start_cursor
                        end_cursor
                        has_next_page
                        has_previous_page
                    }
                    edges {
                        cursor
                        node {
                            hash
                            batch_position
                            kind
                            timestamp
                            level
                            block
                            id
                            source
                            delegate
                            slots
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                first_seen: '2018-09-13T14:51:20Z',
                activated: '2018-09-13T14:49:20.000Z',
                public_key: null,
                endorsements: {
                    page_info: {
                        start_cursor: 'onuTjTGY85y9YpwfEPsquyxqNY7AkQNpGqU2JwU4c4ia2RbZ79Y:0',
                        end_cursor: 'opZSatngKMGpcxgEGy4if5uRS4fCh6p4p3ab9KEuoDZBGQBvGBJ:0',
                        has_next_page: false,
                        has_previous_page: true,
                    },
                    edges: [
                        {
                            cursor: 'onuTjTGY85y9YpwfEPsquyxqNY7AkQNpGqU2JwU4c4ia2RbZ79Y:0',
                            node: {
                                hash: 'onuTjTGY85y9YpwfEPsquyxqNY7AkQNpGqU2JwU4c4ia2RbZ79Y',
                                batch_position: 0,
                                kind: 'endorsement',
                                timestamp: '2018-10-06T09:26:29Z',
                                level: 133692,
                                block: 'BMNXM16a9gdiicXytKkJ2L16ehTsphrLJNdRjA5g9X75cbFEsMQ',
                                id: 2090560,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                delegate: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                slots: '{13}',
                            },
                        },
                        {
                            cursor: 'ooTcJtDdEVWTaHQxEPZr2Wf47e3w4g4j4zFKq9kJuTxo4UKrrXs:0',
                            node: {
                                hash: 'ooTcJtDdEVWTaHQxEPZr2Wf47e3w4g4j4zFKq9kJuTxo4UKrrXs',
                                batch_position: 0,
                                kind: 'endorsement',
                                timestamp: '2018-10-04T18:39:29Z',
                                level: 131415,
                                block: 'BL3KMddQ78skAfM7saXk3c992Z8nvH8Lh4Tf72QTk6rnJg6RRSX',
                                id: 2043192,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                delegate: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                slots: '{25}',
                            },
                        },
                        {
                            cursor: 'opZSatngKMGpcxgEGy4if5uRS4fCh6p4p3ab9KEuoDZBGQBvGBJ:0',
                            node: {
                                hash: 'opZSatngKMGpcxgEGy4if5uRS4fCh6p4p3ab9KEuoDZBGQBvGBJ',
                                batch_position: 0,
                                kind: 'endorsement',
                                timestamp: '2018-10-04T18:15:29Z',
                                level: 131391,
                                block: 'BKrmjm6kRWWkPiccPDPJeB7nWUr561oVCzAWq4DWLe88RweRVAU',
                                id: 2042670,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                delegate: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                slots: '{24}',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.endorsements last and order_by', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS") {
                address
                first_seen
                activated
                public_key
                endorsements (
                last: 3
                order_by: {field: ID, direction: ASC}
                ) {
                    page_info {
                        start_cursor
                        end_cursor
                        has_next_page
                        has_previous_page
                    }
                    edges {
                        cursor
                        node {
                            hash
                            batch_position
                            kind
                            timestamp
                            level
                            block
                            id
                            source
                            delegate
                            slots
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                first_seen: '2018-09-13T14:51:20Z',
                activated: '2018-09-13T14:49:20.000Z',
                public_key: null,
                endorsements: {
                    page_info: {
                        start_cursor: 'op6CjEdgjkTpcNyks1BdkM4DhNFXJLiE3coU3VekM9zdYwWcReL:0',
                        end_cursor: 'onxVkdzq8hDahsyqBhZx9mVzHmQTJ2zd1aQuQixiLTEtT9Syg95:0',
                        has_next_page: false,
                        has_previous_page: true,
                    },
                    edges: [
                        {
                            cursor: 'op6CjEdgjkTpcNyks1BdkM4DhNFXJLiE3coU3VekM9zdYwWcReL:0',
                            node: {
                                hash: 'op6CjEdgjkTpcNyks1BdkM4DhNFXJLiE3coU3VekM9zdYwWcReL',
                                batch_position: 0,
                                kind: 'endorsement',
                                timestamp: '2020-10-24T08:43:48Z',
                                level: 1184939,
                                block: 'BKveoMzNLEBoAsvyBHL7XaMbLiFhrB8DaMoNMvdfk3V6GuM6PoJ',
                                id: 28892185,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                delegate: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                slots: '{29}',
                            },
                        },
                        {
                            cursor: 'oo29bUnXuwrns8XcGTnthznafUJKnspNAWqgSuPvuheEsnFBP5x:0',
                            node: {
                                hash: 'oo29bUnXuwrns8XcGTnthznafUJKnspNAWqgSuPvuheEsnFBP5x',
                                batch_position: 0,
                                kind: 'endorsement',
                                timestamp: '2020-10-25T19:02:22Z',
                                level: 1186992,
                                block: 'BM2YytRc8tQbyVxqQkCZejpGY4uuZwZq2W1nD8pBqoaWPEGkQ9K',
                                id: 28943109,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                delegate: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                slots: '{28}',
                            },
                        },
                        {
                            cursor: 'onxVkdzq8hDahsyqBhZx9mVzHmQTJ2zd1aQuQixiLTEtT9Syg95:0',
                            node: {
                                hash: 'onxVkdzq8hDahsyqBhZx9mVzHmQTJ2zd1aQuQixiLTEtT9Syg95',
                                batch_position: 0,
                                kind: 'endorsement',
                                timestamp: '2020-10-27T15:01:48Z',
                                level: 1189620,
                                block: 'BLCkf49KwQiJLjMiaQ92BeDfzUnLCRiH1E5KCMiEG6ukTwgCSUu',
                                id: 29011132,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                delegate: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                slots: '{23}',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.endorsements last and order_by and before', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS") {
                address
                first_seen
                activated
                public_key
                endorsements (
                last: 3
                order_by: {field: ID, direction: ASC}
                before: "op6CjEdgjkTpcNyks1BdkM4DhNFXJLiE3coU3VekM9zdYwWcReL:0"
                ) {
                    page_info {
                        start_cursor
                        end_cursor
                        has_next_page
                        has_previous_page
                    }
                    edges {
                        cursor
                        node {
                            hash
                            batch_position
                            kind
                            timestamp
                            level
                            block
                            id
                            source
                            delegate
                            slots
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                first_seen: '2018-09-13T14:51:20Z',
                activated: '2018-09-13T14:49:20.000Z',
                public_key: null,
                endorsements: {
                    page_info: {
                        start_cursor: 'oommuxojsu7cLR3xtLrBJQz2Cyi3KZiFiGqd3uD95onVvvYtWGB:0',
                        end_cursor: 'op4F7TA7SEbds4Nbq49z5GtqM8YXLLJM7JonGEjo6zZHTaqLqiS:0',
                        has_next_page: true,
                        has_previous_page: true,
                    },
                    edges: [
                        {
                            cursor: 'oommuxojsu7cLR3xtLrBJQz2Cyi3KZiFiGqd3uD95onVvvYtWGB:0',
                            node: {
                                hash: 'oommuxojsu7cLR3xtLrBJQz2Cyi3KZiFiGqd3uD95onVvvYtWGB',
                                batch_position: 0,
                                kind: 'endorsement',
                                timestamp: '2020-10-21T10:34:10Z',
                                level: 1180760,
                                block: 'BLBEW3F2hseaCRV5gPF1EdVUc3iYrS4VJGjXuLxvqMenUEgpiK9',
                                id: 28786676,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                delegate: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                slots: '{0}',
                            },
                        },
                        {
                            cursor: 'ooAk4vFVjGHU9NvMj84uAhrzvMFzcZXnTz5YkGyf18p1KK7cb7y:0',
                            node: {
                                hash: 'ooAk4vFVjGHU9NvMj84uAhrzvMFzcZXnTz5YkGyf18p1KK7cb7y',
                                batch_position: 0,
                                kind: 'endorsement',
                                timestamp: '2020-10-22T19:46:32Z',
                                level: 1182732,
                                block: 'BLVgCLdbCapVSQGmLjJMgpvwqTt51mUUScQYMVfUqK8wRVqUsfr',
                                id: 28836261,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                delegate: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                slots: '{28}',
                            },
                        },
                        {
                            cursor: 'op4F7TA7SEbds4Nbq49z5GtqM8YXLLJM7JonGEjo6zZHTaqLqiS:0',
                            node: {
                                hash: 'op4F7TA7SEbds4Nbq49z5GtqM8YXLLJM7JonGEjo6zZHTaqLqiS',
                                batch_position: 0,
                                kind: 'endorsement',
                                timestamp: '2020-10-23T04:38:12Z',
                                level: 1183263,
                                block: 'BLmPW9LAvpMnimn2pRXXmgYMsGGPv5MDpnbeurpTnxXjAUsPpb3',
                                id: 28849385,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                delegate: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                slots: '{3}',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.endorsements last and order_by and after', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS") {
                address
                first_seen
                activated
                public_key
                endorsements (
                last: 3
                order_by: {field: ID, direction: ASC}
                after: "op6CjEdgjkTpcNyks1BdkM4DhNFXJLiE3coU3VekM9zdYwWcReL:0"
                ) {
                    page_info {
                        start_cursor
                        end_cursor
                        has_next_page
                        has_previous_page
                    }
                    edges {
                        cursor
                        node {
                            hash
                            batch_position
                            kind
                            timestamp
                            level
                            block
                            id
                            source
                            delegate
                            slots
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                first_seen: '2018-09-13T14:51:20Z',
                activated: '2018-09-13T14:49:20.000Z',
                public_key: null,
                endorsements: {
                    page_info: {
                        start_cursor: 'oo29bUnXuwrns8XcGTnthznafUJKnspNAWqgSuPvuheEsnFBP5x:0',
                        end_cursor: 'onxVkdzq8hDahsyqBhZx9mVzHmQTJ2zd1aQuQixiLTEtT9Syg95:0',
                        has_next_page: false,
                        has_previous_page: true,
                    },
                    edges: [
                        {
                            cursor: 'oo29bUnXuwrns8XcGTnthznafUJKnspNAWqgSuPvuheEsnFBP5x:0',
                            node: {
                                hash: 'oo29bUnXuwrns8XcGTnthznafUJKnspNAWqgSuPvuheEsnFBP5x',
                                batch_position: 0,
                                kind: 'endorsement',
                                timestamp: '2020-10-25T19:02:22Z',
                                level: 1186992,
                                block: 'BM2YytRc8tQbyVxqQkCZejpGY4uuZwZq2W1nD8pBqoaWPEGkQ9K',
                                id: 28943109,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                delegate: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                slots: '{28}',
                            },
                        },
                        {
                            cursor: 'onxVkdzq8hDahsyqBhZx9mVzHmQTJ2zd1aQuQixiLTEtT9Syg95:0',
                            node: {
                                hash: 'onxVkdzq8hDahsyqBhZx9mVzHmQTJ2zd1aQuQixiLTEtT9Syg95',
                                batch_position: 0,
                                kind: 'endorsement',
                                timestamp: '2020-10-27T15:01:48Z',
                                level: 1189620,
                                block: 'BLCkf49KwQiJLjMiaQ92BeDfzUnLCRiH1E5KCMiEG6ukTwgCSUu',
                                id: 29011132,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                delegate: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                slots: '{23}',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.endorsements last and date_range gte', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS") {
                address
                first_seen
                activated
                public_key
                endorsements (
                last: 3
                date_range: {
                    gte: "2019-10-22T18:56:24Z"
                }
                ) {
                    page_info {
                        start_cursor
                        end_cursor
                        has_next_page
                        has_previous_page
                    }
                    edges {
                        cursor
                        node {
                            hash
                            batch_position
                            kind
                            timestamp
                            level
                            block
                            id
                            source
                            delegate
                            slots
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                first_seen: '2018-09-13T14:51:20Z',
                activated: '2018-09-13T14:49:20.000Z',
                public_key: null,
                endorsements: {
                    page_info: {
                        start_cursor: 'onkBcrEqbAzaXAK9NRZfZs1joRv3D3CTnTE8nA7c5xAe1aTbVda:0',
                        end_cursor: 'onsSSmnhHAX1ARcp6WLGuwJVNr7QDrVTggM3iDsTee2UvTjDPZR:0',
                        has_next_page: false,
                        has_previous_page: true,
                    },
                    edges: [
                        {
                            cursor: 'onkBcrEqbAzaXAK9NRZfZs1joRv3D3CTnTE8nA7c5xAe1aTbVda:0',
                            node: {
                                hash: 'onkBcrEqbAzaXAK9NRZfZs1joRv3D3CTnTE8nA7c5xAe1aTbVda',
                                batch_position: 0,
                                kind: 'endorsement',
                                timestamp: '2019-10-23T04:53:03Z',
                                level: 662035,
                                block: 'BLdF7PRZ62YgTLkE7qmcgwKbdDFquCYNy1FeM48mCTLtAFza3we',
                                id: 15169786,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                delegate: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                slots: '{1}',
                            },
                        },
                        {
                            cursor: 'onfEmCLhbmrKanU1jSFTESgVjzTX7bw1dZBTZMK5TjtoU4poDoz:0',
                            node: {
                                hash: 'onfEmCLhbmrKanU1jSFTESgVjzTX7bw1dZBTZMK5TjtoU4poDoz',
                                batch_position: 0,
                                kind: 'endorsement',
                                timestamp: '2019-10-23T00:48:54Z',
                                level: 661798,
                                block: 'BM8WkqFG78LdEZ69AG1wioD658zMR1kQBCaBEnX9Qrqqk2H1d4Q',
                                id: 15164160,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                delegate: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                slots: '{23}',
                            },
                        },
                        {
                            cursor: 'onsSSmnhHAX1ARcp6WLGuwJVNr7QDrVTggM3iDsTee2UvTjDPZR:0',
                            node: {
                                hash: 'onsSSmnhHAX1ARcp6WLGuwJVNr7QDrVTggM3iDsTee2UvTjDPZR',
                                batch_position: 0,
                                kind: 'endorsement',
                                timestamp: '2019-10-22T22:34:49Z',
                                level: 661666,
                                block: 'BMCx8jtyQBnkoqo1bXz62nyLvQ9zXgXKW55TbxmdAoMqjnMSFKS',
                                id: 15160995,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                delegate: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                slots: '{25}',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.endorsements last and date_range gte and before', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS") {
                address
                first_seen
                activated
                public_key
                endorsements (
                last: 3
                date_range: {
                    gte: "2019-10-22T18:56:24Z"
                }
                before: "op6CjEdgjkTpcNyks1BdkM4DhNFXJLiE3coU3VekM9zdYwWcReL:0"
                ) {
                    page_info {
                        start_cursor
                        end_cursor
                        has_next_page
                        has_previous_page
                    }
                    edges {
                        cursor
                        node {
                            hash
                            batch_position
                            kind
                            timestamp
                            level
                            block
                            id
                            source
                            delegate
                            slots
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                first_seen: '2018-09-13T14:51:20Z',
                activated: '2018-09-13T14:49:20.000Z',
                public_key: null,
                endorsements: {
                    page_info: {
                        start_cursor: 'onxVkdzq8hDahsyqBhZx9mVzHmQTJ2zd1aQuQixiLTEtT9Syg95:0',
                        end_cursor: 'oo29bUnXuwrns8XcGTnthznafUJKnspNAWqgSuPvuheEsnFBP5x:0',
                        has_next_page: true,
                        has_previous_page: false,
                    },
                    edges: [
                        {
                            cursor: 'onxVkdzq8hDahsyqBhZx9mVzHmQTJ2zd1aQuQixiLTEtT9Syg95:0',
                            node: {
                                hash: 'onxVkdzq8hDahsyqBhZx9mVzHmQTJ2zd1aQuQixiLTEtT9Syg95',
                                batch_position: 0,
                                kind: 'endorsement',
                                timestamp: '2020-10-27T15:01:48Z',
                                level: 1189620,
                                block: 'BLCkf49KwQiJLjMiaQ92BeDfzUnLCRiH1E5KCMiEG6ukTwgCSUu',
                                id: 29011132,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                delegate: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                slots: '{23}',
                            },
                        },
                        {
                            cursor: 'oo29bUnXuwrns8XcGTnthznafUJKnspNAWqgSuPvuheEsnFBP5x:0',
                            node: {
                                hash: 'oo29bUnXuwrns8XcGTnthznafUJKnspNAWqgSuPvuheEsnFBP5x',
                                batch_position: 0,
                                kind: 'endorsement',
                                timestamp: '2020-10-25T19:02:22Z',
                                level: 1186992,
                                block: 'BM2YytRc8tQbyVxqQkCZejpGY4uuZwZq2W1nD8pBqoaWPEGkQ9K',
                                id: 28943109,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                delegate: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                slots: '{28}',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.endorsements last and date_range gte and after', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS") {
                address
                first_seen
                activated
                public_key
                endorsements (
                last: 3
                date_range: {
                    gte: "2019-10-22T18:56:24Z"
                }
                after: "op6CjEdgjkTpcNyks1BdkM4DhNFXJLiE3coU3VekM9zdYwWcReL:0"
                ) {
                    page_info {
                        start_cursor
                        end_cursor
                        has_next_page
                        has_previous_page
                    }
                    edges {
                        cursor
                        node {
                            hash
                            batch_position
                            kind
                            timestamp
                            level
                            block
                            id
                            source
                            delegate
                            slots
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                first_seen: '2018-09-13T14:51:20Z',
                activated: '2018-09-13T14:49:20.000Z',
                public_key: null,
                endorsements: {
                    page_info: {
                        start_cursor: 'onkBcrEqbAzaXAK9NRZfZs1joRv3D3CTnTE8nA7c5xAe1aTbVda:0',
                        end_cursor: 'onsSSmnhHAX1ARcp6WLGuwJVNr7QDrVTggM3iDsTee2UvTjDPZR:0',
                        has_next_page: false,
                        has_previous_page: true,
                    },
                    edges: [
                        {
                            cursor: 'onkBcrEqbAzaXAK9NRZfZs1joRv3D3CTnTE8nA7c5xAe1aTbVda:0',
                            node: {
                                hash: 'onkBcrEqbAzaXAK9NRZfZs1joRv3D3CTnTE8nA7c5xAe1aTbVda',
                                batch_position: 0,
                                kind: 'endorsement',
                                timestamp: '2019-10-23T04:53:03Z',
                                level: 662035,
                                block: 'BLdF7PRZ62YgTLkE7qmcgwKbdDFquCYNy1FeM48mCTLtAFza3we',
                                id: 15169786,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                delegate: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                slots: '{1}',
                            },
                        },
                        {
                            cursor: 'onfEmCLhbmrKanU1jSFTESgVjzTX7bw1dZBTZMK5TjtoU4poDoz:0',
                            node: {
                                hash: 'onfEmCLhbmrKanU1jSFTESgVjzTX7bw1dZBTZMK5TjtoU4poDoz',
                                batch_position: 0,
                                kind: 'endorsement',
                                timestamp: '2019-10-23T00:48:54Z',
                                level: 661798,
                                block: 'BM8WkqFG78LdEZ69AG1wioD658zMR1kQBCaBEnX9Qrqqk2H1d4Q',
                                id: 15164160,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                delegate: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                slots: '{23}',
                            },
                        },
                        {
                            cursor: 'onsSSmnhHAX1ARcp6WLGuwJVNr7QDrVTggM3iDsTee2UvTjDPZR:0',
                            node: {
                                hash: 'onsSSmnhHAX1ARcp6WLGuwJVNr7QDrVTggM3iDsTee2UvTjDPZR',
                                batch_position: 0,
                                kind: 'endorsement',
                                timestamp: '2019-10-22T22:34:49Z',
                                level: 661666,
                                block: 'BMCx8jtyQBnkoqo1bXz62nyLvQ9zXgXKW55TbxmdAoMqjnMSFKS',
                                id: 15160995,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                delegate: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                slots: '{25}',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.endorsements last and date_range gte and order_by', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS") {
                address
                first_seen
                activated
                public_key
                endorsements (
                    last: 3
                    date_range: {
                    gte: "2019-10-22T18:56:24Z"
                    }
                    order_by: {field: ID, direction: ASC}
                ) {
                    page_info {
                        start_cursor
                        end_cursor
                        has_next_page
                        has_previous_page
                    }
                    edges {
                        cursor
                        node {
                            hash
                            batch_position
                            kind
                            timestamp
                            level
                            block
                            id
                            source
                            delegate
                            slots
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                first_seen: '2018-09-13T14:51:20Z',
                activated: '2018-09-13T14:49:20.000Z',
                public_key: null,
                endorsements: {
                    page_info: {
                        start_cursor: 'op6CjEdgjkTpcNyks1BdkM4DhNFXJLiE3coU3VekM9zdYwWcReL:0',
                        end_cursor: 'onxVkdzq8hDahsyqBhZx9mVzHmQTJ2zd1aQuQixiLTEtT9Syg95:0',
                        has_next_page: false,
                        has_previous_page: true,
                    },
                    edges: [
                        {
                            cursor: 'op6CjEdgjkTpcNyks1BdkM4DhNFXJLiE3coU3VekM9zdYwWcReL:0',
                            node: {
                                hash: 'op6CjEdgjkTpcNyks1BdkM4DhNFXJLiE3coU3VekM9zdYwWcReL',
                                batch_position: 0,
                                kind: 'endorsement',
                                timestamp: '2020-10-24T08:43:48Z',
                                level: 1184939,
                                block: 'BKveoMzNLEBoAsvyBHL7XaMbLiFhrB8DaMoNMvdfk3V6GuM6PoJ',
                                id: 28892185,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                delegate: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                slots: '{29}',
                            },
                        },
                        {
                            cursor: 'oo29bUnXuwrns8XcGTnthznafUJKnspNAWqgSuPvuheEsnFBP5x:0',
                            node: {
                                hash: 'oo29bUnXuwrns8XcGTnthznafUJKnspNAWqgSuPvuheEsnFBP5x',
                                batch_position: 0,
                                kind: 'endorsement',
                                timestamp: '2020-10-25T19:02:22Z',
                                level: 1186992,
                                block: 'BM2YytRc8tQbyVxqQkCZejpGY4uuZwZq2W1nD8pBqoaWPEGkQ9K',
                                id: 28943109,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                delegate: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                slots: '{28}',
                            },
                        },
                        {
                            cursor: 'onxVkdzq8hDahsyqBhZx9mVzHmQTJ2zd1aQuQixiLTEtT9Syg95:0',
                            node: {
                                hash: 'onxVkdzq8hDahsyqBhZx9mVzHmQTJ2zd1aQuQixiLTEtT9Syg95',
                                batch_position: 0,
                                kind: 'endorsement',
                                timestamp: '2020-10-27T15:01:48Z',
                                level: 1189620,
                                block: 'BLCkf49KwQiJLjMiaQ92BeDfzUnLCRiH1E5KCMiEG6ukTwgCSUu',
                                id: 29011132,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                delegate: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                slots: '{23}',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.endorsements last and date_range lte', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS") {
                address
                first_seen
                activated
                public_key
                endorsements (
                last: 3
                date_range: {
                    lte: "2019-10-22T18:56:24Z"
                }
                ) {
                    page_info {
                        start_cursor
                        end_cursor
                        has_next_page
                        has_previous_page
                    }
                    edges {
                        cursor
                        node {
                            hash
                            batch_position
                            kind
                            timestamp
                            level
                            block
                            id
                            source
                            delegate
                            slots
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                first_seen: '2018-09-13T14:51:20Z',
                activated: '2018-09-13T14:49:20.000Z',
                public_key: null,
                endorsements: {
                    page_info: {
                        start_cursor: 'onuTjTGY85y9YpwfEPsquyxqNY7AkQNpGqU2JwU4c4ia2RbZ79Y:0',
                        end_cursor: 'opZSatngKMGpcxgEGy4if5uRS4fCh6p4p3ab9KEuoDZBGQBvGBJ:0',
                        has_next_page: false,
                        has_previous_page: true,
                    },
                    edges: [
                        {
                            cursor: 'onuTjTGY85y9YpwfEPsquyxqNY7AkQNpGqU2JwU4c4ia2RbZ79Y:0',
                            node: {
                                hash: 'onuTjTGY85y9YpwfEPsquyxqNY7AkQNpGqU2JwU4c4ia2RbZ79Y',
                                batch_position: 0,
                                kind: 'endorsement',
                                timestamp: '2018-10-06T09:26:29Z',
                                level: 133692,
                                block: 'BMNXM16a9gdiicXytKkJ2L16ehTsphrLJNdRjA5g9X75cbFEsMQ',
                                id: 2090560,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                delegate: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                slots: '{13}',
                            },
                        },
                        {
                            cursor: 'ooTcJtDdEVWTaHQxEPZr2Wf47e3w4g4j4zFKq9kJuTxo4UKrrXs:0',
                            node: {
                                hash: 'ooTcJtDdEVWTaHQxEPZr2Wf47e3w4g4j4zFKq9kJuTxo4UKrrXs',
                                batch_position: 0,
                                kind: 'endorsement',
                                timestamp: '2018-10-04T18:39:29Z',
                                level: 131415,
                                block: 'BL3KMddQ78skAfM7saXk3c992Z8nvH8Lh4Tf72QTk6rnJg6RRSX',
                                id: 2043192,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                delegate: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                slots: '{25}',
                            },
                        },
                        {
                            cursor: 'opZSatngKMGpcxgEGy4if5uRS4fCh6p4p3ab9KEuoDZBGQBvGBJ:0',
                            node: {
                                hash: 'opZSatngKMGpcxgEGy4if5uRS4fCh6p4p3ab9KEuoDZBGQBvGBJ',
                                batch_position: 0,
                                kind: 'endorsement',
                                timestamp: '2018-10-04T18:15:29Z',
                                level: 131391,
                                block: 'BKrmjm6kRWWkPiccPDPJeB7nWUr561oVCzAWq4DWLe88RweRVAU',
                                id: 2042670,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                delegate: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                slots: '{24}',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.endorsements last and date_range lte and before', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS") {
                address
                first_seen
                activated
                public_key
                endorsements (
                last: 3
                date_range: {
                    lte: "2019-10-22T18:56:24Z"
                }
                before: "onxjQwuzc4AWHWsDVFnC4rJY1B3UMnajJNquW3bhs8mCu4WGCwP:0"
                ) {
                    page_info {
                        start_cursor
                        end_cursor
                        has_next_page
                        has_previous_page
                    }
                    edges {
                        cursor
                        node {
                            hash
                            batch_position
                            kind
                            timestamp
                            level
                            block
                            id
                            source
                            delegate
                            slots
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                first_seen: '2018-09-13T14:51:20Z',
                activated: '2018-09-13T14:49:20.000Z',
                public_key: null,
                endorsements: {
                    page_info: {
                        start_cursor: 'oo4zEKtirUFj89Qg3Vn691SQa4Lp84n6VNNUL9KDd6ZfXSCmU3q:0',
                        end_cursor: 'oohxAzy22hAxtjwx4ZgLBqZwnWghta3eYC7raWGDjrN7tGUEmAU:0',
                        has_next_page: true,
                        has_previous_page: false,
                    },
                    edges: [
                        {
                            cursor: 'oo4zEKtirUFj89Qg3Vn691SQa4Lp84n6VNNUL9KDd6ZfXSCmU3q:0',
                            node: {
                                hash: 'oo4zEKtirUFj89Qg3Vn691SQa4Lp84n6VNNUL9KDd6ZfXSCmU3q',
                                batch_position: 0,
                                kind: 'endorsement',
                                timestamp: '2019-10-22T16:41:39Z',
                                level: 661319,
                                block: 'BMLqchUYAjYErYSW5DcAhNVQxWTJ7TtWLMyb4x6vvxRvkgmmrDX',
                                id: 15152486,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                delegate: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                slots: '{8}',
                            },
                        },
                        {
                            cursor: 'oohxAzy22hAxtjwx4ZgLBqZwnWghta3eYC7raWGDjrN7tGUEmAU:0',
                            node: {
                                hash: 'oohxAzy22hAxtjwx4ZgLBqZwnWghta3eYC7raWGDjrN7tGUEmAU',
                                batch_position: 0,
                                kind: 'endorsement',
                                timestamp: '2019-10-22T14:24:30Z',
                                level: 661187,
                                block: 'BL1X1fediWJn97dbHFNVG3ov46Snr5on2ohJAYGDPfVwMj8gwpZ',
                                id: 15149373,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                delegate: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                slots: '{12}',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.endorsements last and date_range lte and after', async () => {
        const GET_ACCOUNT = gql`
       query {
            account(address: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS") {
                address
                first_seen
                activated
                public_key
                endorsements (
                last: 3
                date_range: {
                    lte: "2019-10-22T18:56:24Z"
                }
                after: "ooTcJtDdEVWTaHQxEPZr2Wf47e3w4g4j4zFKq9kJuTxo4UKrrXs:0"
                ) {
                    page_info {
                        start_cursor
                        end_cursor
                        has_next_page
                        has_previous_page
                    }
                    edges {
                        cursor
                        node {
                            hash
                            batch_position
                            kind
                            timestamp
                            level
                            block
                            id
                            source
                            delegate
                            slots
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                first_seen: '2018-09-13T14:51:20Z',
                activated: '2018-09-13T14:49:20.000Z',
                public_key: null,
                endorsements: {
                    page_info: {
                        start_cursor: 'opZSatngKMGpcxgEGy4if5uRS4fCh6p4p3ab9KEuoDZBGQBvGBJ:0',
                        end_cursor: 'opZSatngKMGpcxgEGy4if5uRS4fCh6p4p3ab9KEuoDZBGQBvGBJ:0',
                        has_next_page: false,
                        has_previous_page: true,
                    },
                    edges: [
                        {
                            cursor: 'opZSatngKMGpcxgEGy4if5uRS4fCh6p4p3ab9KEuoDZBGQBvGBJ:0',
                            node: {
                                hash: 'opZSatngKMGpcxgEGy4if5uRS4fCh6p4p3ab9KEuoDZBGQBvGBJ',
                                batch_position: 0,
                                kind: 'endorsement',
                                timestamp: '2018-10-04T18:15:29Z',
                                level: 131391,
                                block: 'BKrmjm6kRWWkPiccPDPJeB7nWUr561oVCzAWq4DWLe88RweRVAU',
                                id: 2042670,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                delegate: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                slots: '{24}',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.endorsements last and date_range lte and order_by', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS") {
                address
                first_seen
                activated
                public_key
                endorsements (
                    last: 3
                    date_range: {
                    lte: "2019-10-22T18:56:24Z"
                    }
                    order_by: {field: ID, direction: ASC}
                ) {
                    page_info {
                        start_cursor
                        end_cursor
                        has_next_page
                        has_previous_page
                    }
                    edges {
                        cursor
                        node {
                            hash
                            batch_position
                            kind
                            timestamp
                            level
                            block
                            id
                            source
                            delegate
                            slots
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                first_seen: '2018-09-13T14:51:20Z',
                activated: '2018-09-13T14:49:20.000Z',
                public_key: null,
                endorsements: {
                    page_info: {
                        start_cursor: 'onxjQwuzc4AWHWsDVFnC4rJY1B3UMnajJNquW3bhs8mCu4WGCwP:0',
                        end_cursor: 'oo4zEKtirUFj89Qg3Vn691SQa4Lp84n6VNNUL9KDd6ZfXSCmU3q:0',
                        has_next_page: false,
                        has_previous_page: true,
                    },
                    edges: [
                        {
                            cursor: 'onxjQwuzc4AWHWsDVFnC4rJY1B3UMnajJNquW3bhs8mCu4WGCwP:0',
                            node: {
                                hash: 'onxjQwuzc4AWHWsDVFnC4rJY1B3UMnajJNquW3bhs8mCu4WGCwP',
                                batch_position: 0,
                                kind: 'endorsement',
                                timestamp: '2019-10-22T09:22:24Z',
                                level: 660895,
                                block: 'BMW7KFf8TPgjbMDnxabR77gpg5DzEcYWuZcRNBXQZwR6J5xkJXx',
                                id: 15142272,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                delegate: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                slots: '{10}',
                            },
                        },
                        {
                            cursor: 'oohxAzy22hAxtjwx4ZgLBqZwnWghta3eYC7raWGDjrN7tGUEmAU:0',
                            node: {
                                hash: 'oohxAzy22hAxtjwx4ZgLBqZwnWghta3eYC7raWGDjrN7tGUEmAU',
                                batch_position: 0,
                                kind: 'endorsement',
                                timestamp: '2019-10-22T14:24:30Z',
                                level: 661187,
                                block: 'BL1X1fediWJn97dbHFNVG3ov46Snr5on2ohJAYGDPfVwMj8gwpZ',
                                id: 15149373,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                delegate: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                slots: '{12}',
                            },
                        },
                        {
                            cursor: 'oo4zEKtirUFj89Qg3Vn691SQa4Lp84n6VNNUL9KDd6ZfXSCmU3q:0',
                            node: {
                                hash: 'oo4zEKtirUFj89Qg3Vn691SQa4Lp84n6VNNUL9KDd6ZfXSCmU3q',
                                batch_position: 0,
                                kind: 'endorsement',
                                timestamp: '2019-10-22T16:41:39Z',
                                level: 661319,
                                block: 'BMLqchUYAjYErYSW5DcAhNVQxWTJ7TtWLMyb4x6vvxRvkgmmrDX',
                                id: 15152486,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                delegate: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                slots: '{8}',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.endorsements last and date_range gte/lte', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS") {
                address
                first_seen
                activated
                public_key
                endorsements (
                last: 3
                date_range: {
                    gte: "2019-10-22T18:56:24Z"
                    lte: "2019-10-27T12:46:11Z"
                }
                ) {
                    page_info {
                        start_cursor
                        end_cursor
                        has_next_page
                        has_previous_page
                    }
                    edges {
                        cursor
                        node {
                            hash
                            batch_position
                            kind
                            timestamp
                            level
                            block
                            id
                            source
                            delegate
                            slots
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                first_seen: '2018-09-13T14:51:20Z',
                activated: '2018-09-13T14:49:20.000Z',
                public_key: null,
                endorsements: {
                    page_info: {
                        start_cursor: 'onkBcrEqbAzaXAK9NRZfZs1joRv3D3CTnTE8nA7c5xAe1aTbVda:0',
                        end_cursor: 'onsSSmnhHAX1ARcp6WLGuwJVNr7QDrVTggM3iDsTee2UvTjDPZR:0',
                        has_next_page: false,
                        has_previous_page: true,
                    },
                    edges: [
                        {
                            cursor: 'onkBcrEqbAzaXAK9NRZfZs1joRv3D3CTnTE8nA7c5xAe1aTbVda:0',
                            node: {
                                hash: 'onkBcrEqbAzaXAK9NRZfZs1joRv3D3CTnTE8nA7c5xAe1aTbVda',
                                batch_position: 0,
                                kind: 'endorsement',
                                timestamp: '2019-10-23T04:53:03Z',
                                level: 662035,
                                block: 'BLdF7PRZ62YgTLkE7qmcgwKbdDFquCYNy1FeM48mCTLtAFza3we',
                                id: 15169786,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                delegate: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                slots: '{1}',
                            },
                        },
                        {
                            cursor: 'onfEmCLhbmrKanU1jSFTESgVjzTX7bw1dZBTZMK5TjtoU4poDoz:0',
                            node: {
                                hash: 'onfEmCLhbmrKanU1jSFTESgVjzTX7bw1dZBTZMK5TjtoU4poDoz',
                                batch_position: 0,
                                kind: 'endorsement',
                                timestamp: '2019-10-23T00:48:54Z',
                                level: 661798,
                                block: 'BM8WkqFG78LdEZ69AG1wioD658zMR1kQBCaBEnX9Qrqqk2H1d4Q',
                                id: 15164160,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                delegate: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                slots: '{23}',
                            },
                        },
                        {
                            cursor: 'onsSSmnhHAX1ARcp6WLGuwJVNr7QDrVTggM3iDsTee2UvTjDPZR:0',
                            node: {
                                hash: 'onsSSmnhHAX1ARcp6WLGuwJVNr7QDrVTggM3iDsTee2UvTjDPZR',
                                batch_position: 0,
                                kind: 'endorsement',
                                timestamp: '2019-10-22T22:34:49Z',
                                level: 661666,
                                block: 'BMCx8jtyQBnkoqo1bXz62nyLvQ9zXgXKW55TbxmdAoMqjnMSFKS',
                                id: 15160995,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                delegate: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                slots: '{25}',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.endorsements last and date_range gte/lte and before', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS") {
                address
                first_seen
                activated
                public_key
                endorsements (
                last: 3
                date_range: {
                    gte: "2019-10-22T18:56:24Z"
                    lte: "2019-10-27T12:46:11Z"
                }
                before: "onfEmCLhbmrKanU1jSFTESgVjzTX7bw1dZBTZMK5TjtoU4poDoz:0"
                ) {
                    page_info {
                        start_cursor
                        end_cursor
                        has_next_page
                        has_previous_page
                    }
                    edges {
                        cursor
                        node {
                            hash
                            batch_position
                            kind
                            timestamp
                            level
                            block
                            id
                            source
                            delegate
                            slots
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                first_seen: '2018-09-13T14:51:20Z',
                activated: '2018-09-13T14:49:20.000Z',
                public_key: null,
                endorsements: {
                    page_info: {
                        start_cursor: 'oop3uFb4bB9DkSBw3kBfuutYTLneT54ZiJ1GkjVeRmSamJFRZEG:0',
                        end_cursor: 'onkBcrEqbAzaXAK9NRZfZs1joRv3D3CTnTE8nA7c5xAe1aTbVda:0',
                        has_next_page: true,
                        has_previous_page: true,
                    },
                    edges: [
                        {
                            cursor: 'oop3uFb4bB9DkSBw3kBfuutYTLneT54ZiJ1GkjVeRmSamJFRZEG:0',
                            node: {
                                hash: 'oop3uFb4bB9DkSBw3kBfuutYTLneT54ZiJ1GkjVeRmSamJFRZEG',
                                batch_position: 0,
                                kind: 'endorsement',
                                timestamp: '2019-10-23T10:08:18Z',
                                level: 662342,
                                block: 'BLXh62j3QVgbidFsmrfDNfJANA5hpm2DxjRYeiB4yAZVeGzCcZm',
                                id: 15177183,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                delegate: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                slots: '{10}',
                            },
                        },
                        {
                            cursor: 'onoUfoDG1s1Ju2C57av4RqDrvr8ewYfzcTv6EHrhooskmFBRALP:0',
                            node: {
                                hash: 'onoUfoDG1s1Ju2C57av4RqDrvr8ewYfzcTv6EHrhooskmFBRALP',
                                batch_position: 0,
                                kind: 'endorsement',
                                timestamp: '2019-10-23T09:55:18Z',
                                level: 662329,
                                block: 'BLbhewf3S6nnPgAYaeRE4A31hJ81p3fy9gmCtJBkGh1sp1AuRkr',
                                id: 15176876,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                delegate: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                slots: '{11}',
                            },
                        },
                        {
                            cursor: 'onkBcrEqbAzaXAK9NRZfZs1joRv3D3CTnTE8nA7c5xAe1aTbVda:0',
                            node: {
                                hash: 'onkBcrEqbAzaXAK9NRZfZs1joRv3D3CTnTE8nA7c5xAe1aTbVda',
                                batch_position: 0,
                                kind: 'endorsement',
                                timestamp: '2019-10-23T04:53:03Z',
                                level: 662035,
                                block: 'BLdF7PRZ62YgTLkE7qmcgwKbdDFquCYNy1FeM48mCTLtAFza3we',
                                id: 15169786,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                delegate: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                slots: '{1}',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.endorsements last and date_range gte/lte and after', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS") {
                address
                first_seen
                activated
                public_key
                endorsements (
                last: 3
                date_range: {
                    gte: "2019-10-22T18:56:24Z"
                    lte: "2019-10-27T12:46:11Z"
                }
                after: "op6CjEdgjkTpcNyks1BdkM4DhNFXJLiE3coU3VekM9zdYwWcReL:0"
                ) {
                    page_info {
                        start_cursor
                        end_cursor
                        has_next_page
                        has_previous_page
                    }
                    edges {
                        cursor
                        node {
                            hash
                            batch_position
                            kind
                            timestamp
                            level
                            block
                            id
                            source
                            delegate
                            slots
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                first_seen: '2018-09-13T14:51:20Z',
                activated: '2018-09-13T14:49:20.000Z',
                public_key: null,
                endorsements: {
                    page_info: {
                        start_cursor: 'onkBcrEqbAzaXAK9NRZfZs1joRv3D3CTnTE8nA7c5xAe1aTbVda:0',
                        end_cursor: 'onsSSmnhHAX1ARcp6WLGuwJVNr7QDrVTggM3iDsTee2UvTjDPZR:0',
                        has_next_page: false,
                        has_previous_page: true,
                    },
                    edges: [
                        {
                            cursor: 'onkBcrEqbAzaXAK9NRZfZs1joRv3D3CTnTE8nA7c5xAe1aTbVda:0',
                            node: {
                                hash: 'onkBcrEqbAzaXAK9NRZfZs1joRv3D3CTnTE8nA7c5xAe1aTbVda',
                                batch_position: 0,
                                kind: 'endorsement',
                                timestamp: '2019-10-23T04:53:03Z',
                                level: 662035,
                                block: 'BLdF7PRZ62YgTLkE7qmcgwKbdDFquCYNy1FeM48mCTLtAFza3we',
                                id: 15169786,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                delegate: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                slots: '{1}',
                            },
                        },
                        {
                            cursor: 'onfEmCLhbmrKanU1jSFTESgVjzTX7bw1dZBTZMK5TjtoU4poDoz:0',
                            node: {
                                hash: 'onfEmCLhbmrKanU1jSFTESgVjzTX7bw1dZBTZMK5TjtoU4poDoz',
                                batch_position: 0,
                                kind: 'endorsement',
                                timestamp: '2019-10-23T00:48:54Z',
                                level: 661798,
                                block: 'BM8WkqFG78LdEZ69AG1wioD658zMR1kQBCaBEnX9Qrqqk2H1d4Q',
                                id: 15164160,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                delegate: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                slots: '{23}',
                            },
                        },
                        {
                            cursor: 'onsSSmnhHAX1ARcp6WLGuwJVNr7QDrVTggM3iDsTee2UvTjDPZR:0',
                            node: {
                                hash: 'onsSSmnhHAX1ARcp6WLGuwJVNr7QDrVTggM3iDsTee2UvTjDPZR',
                                batch_position: 0,
                                kind: 'endorsement',
                                timestamp: '2019-10-22T22:34:49Z',
                                level: 661666,
                                block: 'BMCx8jtyQBnkoqo1bXz62nyLvQ9zXgXKW55TbxmdAoMqjnMSFKS',
                                id: 15160995,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                delegate: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                slots: '{25}',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.endorsements last and date_range gte/lte and order_by', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS") {
                address
                first_seen
                activated
                public_key
                endorsements (
                    last: 3
                    date_range: {
                    gte: "2019-10-22T18:56:24Z"
                    lte: "2019-10-27T12:46:11Z"
                    }
                    order_by: {field: ID, direction: ASC}
                ) {
                    page_info {
                        start_cursor
                        end_cursor
                        has_next_page
                        has_previous_page
                    }
                    edges {
                        cursor
                        node {
                            hash
                            batch_position
                            kind
                            timestamp
                            level
                            block
                            id
                            source
                            delegate
                            slots
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                first_seen: '2018-09-13T14:51:20Z',
                activated: '2018-09-13T14:49:20.000Z',
                public_key: null,
                endorsements: {
                    page_info: {
                        start_cursor: 'oo8ycUqrUVcp9V2JnkSFdB4uD4UDRMmrfVhTJo3Nkd9J8Z7DCdu:0',
                        end_cursor: 'opZn2cJbqpULLcRsJJCqLRE2bCAJAK7E1cs13MDT7nTxSNrxJ8i:0',
                        has_next_page: false,
                        has_previous_page: true,
                    },
                    edges: [
                        {
                            cursor: 'oo8ycUqrUVcp9V2JnkSFdB4uD4UDRMmrfVhTJo3Nkd9J8Z7DCdu:0',
                            node: {
                                hash: 'oo8ycUqrUVcp9V2JnkSFdB4uD4UDRMmrfVhTJo3Nkd9J8Z7DCdu',
                                batch_position: 0,
                                kind: 'endorsement',
                                timestamp: '2019-10-26T23:40:09Z',
                                level: 667327,
                                block: 'BM6jzwZ9nRU7k1wa7JMGKghP4Sxa9LQ3cjoZUjx4qSfamN67AUC',
                                id: 15300988,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                delegate: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                slots: '{21}',
                            },
                        },
                        {
                            cursor: 'ooBNdbbRduUCHU5fTEjbfDS8AcyXPgCk4skMbhX42NJzhSKyfpt:0',
                            node: {
                                hash: 'ooBNdbbRduUCHU5fTEjbfDS8AcyXPgCk4skMbhX42NJzhSKyfpt',
                                batch_position: 0,
                                kind: 'endorsement',
                                timestamp: '2019-10-27T06:19:39Z',
                                level: 667721,
                                block: 'BLXwcgT4Qe88sJuQYvq4jfaNp8NKWABHG18zjsFZaaKrprruhAQ',
                                id: 15310994,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                delegate: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                slots: '{25}',
                            },
                        },
                        {
                            cursor: 'opZn2cJbqpULLcRsJJCqLRE2bCAJAK7E1cs13MDT7nTxSNrxJ8i:0',
                            node: {
                                hash: 'opZn2cJbqpULLcRsJJCqLRE2bCAJAK7E1cs13MDT7nTxSNrxJ8i',
                                batch_position: 0,
                                kind: 'endorsement',
                                timestamp: '2019-10-27T08:44:19Z',
                                level: 667863,
                                block: 'BMMzWra66QyPGWM465uTWz6CewzrzbrK8XJFQUnQ6Fj7owNGP4M',
                                id: 15315917,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                delegate: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                slots: '{25}',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.endorsements last and date_range gte and order_by and before', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS") {
                address
                first_seen
                activated
                public_key
                endorsements (
                last: 3
                order_by: {field: ID, direction: ASC}
                before: "opEzXxddQYjJQRWeXqynBTA22raRESWgfKUx6LPCGp194nFW6tR:0"
                date_range: {
                    gte: "2019-10-27T12:46:11Z"
                }
                ) {
                    page_info {
                        start_cursor
                        end_cursor
                        has_next_page
                        has_previous_page
                    }
                    edges {
                        cursor
                        node {
                            hash
                            batch_position
                            kind
                            timestamp
                            level
                            block
                            id
                            source
                            delegate
                            slots
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                first_seen: '2018-09-13T14:51:20Z',
                activated: '2018-09-13T14:49:20.000Z',
                public_key: null,
                endorsements: {
                    page_info: {
                        start_cursor: 'onrmgYH57FJvpDGFdV9w6e9Tr4GAuHwqUApBKzzVbDPr61r8Fhh:0',
                        end_cursor: 'onfGug1uuV4RzM7yKPxaQZhPWcKwtZLkX61HcBj7yo6643CCSNY:0',
                        has_next_page: true,
                        has_previous_page: true,
                    },
                    edges: [
                        {
                            cursor: 'onrmgYH57FJvpDGFdV9w6e9Tr4GAuHwqUApBKzzVbDPr61r8Fhh:0',
                            node: {
                                hash: 'onrmgYH57FJvpDGFdV9w6e9Tr4GAuHwqUApBKzzVbDPr61r8Fhh',
                                batch_position: 0,
                                kind: 'endorsement',
                                timestamp: '2019-10-28T00:08:56Z',
                                level: 668768,
                                block: 'BLe6n4cJQizKwX6ojALfwk4i4KtHTH1oUhLqY5Wo9FYp1wRxhFv',
                                id: 15338966,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                delegate: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                slots: '{11}',
                            },
                        },
                        {
                            cursor: 'oo9tiinXivJqpgBqQKVsvHbgd35RtLDRaLpb9M3VNp61tjiGHfd:0',
                            node: {
                                hash: 'oo9tiinXivJqpgBqQKVsvHbgd35RtLDRaLpb9M3VNp61tjiGHfd',
                                batch_position: 0,
                                kind: 'endorsement',
                                timestamp: '2019-10-28T00:09:56Z',
                                level: 668769,
                                block: 'BLQHPZ1w4KWJp3erfjWg16e2Ymuo6Z45MUaS64HiBNDc6JZ5nzp',
                                id: 15338984,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                delegate: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                slots: '{5}',
                            },
                        },
                        {
                            cursor: 'onfGug1uuV4RzM7yKPxaQZhPWcKwtZLkX61HcBj7yo6643CCSNY:0',
                            node: {
                                hash: 'onfGug1uuV4RzM7yKPxaQZhPWcKwtZLkX61HcBj7yo6643CCSNY',
                                batch_position: 0,
                                kind: 'endorsement',
                                timestamp: '2019-10-28T00:18:56Z',
                                level: 668778,
                                block: 'BLGfdQShZMGED8YVBvCvCadeFYQwAGqKqgqdKzEUhDa7zPhHKd3',
                                id: 15339199,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                delegate: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                slots: '{28}',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.endorsements last and date_range gte and order_by and after', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS") {
                address
                first_seen
                activated
                public_key
                endorsements (
                last: 3
                order_by: {field: ID, direction: ASC}
                after: "op6CjEdgjkTpcNyks1BdkM4DhNFXJLiE3coU3VekM9zdYwWcReL:0"
                date_range: {
                    gte: "2019-10-27T12:46:11Z"
                }
                ) {
                    page_info {
                        start_cursor
                        end_cursor
                        has_next_page
                        has_previous_page
                    }
                    edges {
                        cursor
                        node {
                            hash
                            batch_position
                            kind
                            timestamp
                            level
                            block
                            id
                            source
                            delegate
                            slots
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                first_seen: '2018-09-13T14:51:20Z',
                activated: '2018-09-13T14:49:20.000Z',
                public_key: null,
                endorsements: {
                    page_info: {
                        start_cursor: 'oo29bUnXuwrns8XcGTnthznafUJKnspNAWqgSuPvuheEsnFBP5x:0',
                        end_cursor: 'onxVkdzq8hDahsyqBhZx9mVzHmQTJ2zd1aQuQixiLTEtT9Syg95:0',
                        has_next_page: false,
                        has_previous_page: true,
                    },
                    edges: [
                        {
                            cursor: 'oo29bUnXuwrns8XcGTnthznafUJKnspNAWqgSuPvuheEsnFBP5x:0',
                            node: {
                                hash: 'oo29bUnXuwrns8XcGTnthznafUJKnspNAWqgSuPvuheEsnFBP5x',
                                batch_position: 0,
                                kind: 'endorsement',
                                timestamp: '2020-10-25T19:02:22Z',
                                level: 1186992,
                                block: 'BM2YytRc8tQbyVxqQkCZejpGY4uuZwZq2W1nD8pBqoaWPEGkQ9K',
                                id: 28943109,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                delegate: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                slots: '{28}',
                            },
                        },
                        {
                            cursor: 'onxVkdzq8hDahsyqBhZx9mVzHmQTJ2zd1aQuQixiLTEtT9Syg95:0',
                            node: {
                                hash: 'onxVkdzq8hDahsyqBhZx9mVzHmQTJ2zd1aQuQixiLTEtT9Syg95',
                                batch_position: 0,
                                kind: 'endorsement',
                                timestamp: '2020-10-27T15:01:48Z',
                                level: 1189620,
                                block: 'BLCkf49KwQiJLjMiaQ92BeDfzUnLCRiH1E5KCMiEG6ukTwgCSUu',
                                id: 29011132,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                delegate: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                slots: '{23}',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.endorsements last and date_range lte and order_by and before', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS") {
                address
                first_seen
                activated
                public_key
                endorsements (
                last: 3
                order_by: {field: ID, direction: ASC}
                before: "op6CjEdgjkTpcNyks1BdkM4DhNFXJLiE3coU3VekM9zdYwWcReL:0"
                date_range: {
                    lte: "2019-10-27T12:46:11Z"
                }
                ) {
                    page_info {
                        start_cursor
                        end_cursor
                        has_next_page
                        has_previous_page
                    }
                    edges {
                        cursor
                        node {
                            hash
                            batch_position
                            kind
                            timestamp
                            level
                            block
                            id
                            source
                            delegate
                            slots
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                first_seen: '2018-09-13T14:51:20Z',
                activated: '2018-09-13T14:49:20.000Z',
                public_key: null,
                endorsements: {
                    page_info: {
                        start_cursor: 'oo8ycUqrUVcp9V2JnkSFdB4uD4UDRMmrfVhTJo3Nkd9J8Z7DCdu:0',
                        end_cursor: 'opZn2cJbqpULLcRsJJCqLRE2bCAJAK7E1cs13MDT7nTxSNrxJ8i:0',
                        has_next_page: false,
                        has_previous_page: true,
                    },
                    edges: [
                        {
                            cursor: 'oo8ycUqrUVcp9V2JnkSFdB4uD4UDRMmrfVhTJo3Nkd9J8Z7DCdu:0',
                            node: {
                                hash: 'oo8ycUqrUVcp9V2JnkSFdB4uD4UDRMmrfVhTJo3Nkd9J8Z7DCdu',
                                batch_position: 0,
                                kind: 'endorsement',
                                timestamp: '2019-10-26T23:40:09Z',
                                level: 667327,
                                block: 'BM6jzwZ9nRU7k1wa7JMGKghP4Sxa9LQ3cjoZUjx4qSfamN67AUC',
                                id: 15300988,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                delegate: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                slots: '{21}',
                            },
                        },
                        {
                            cursor: 'ooBNdbbRduUCHU5fTEjbfDS8AcyXPgCk4skMbhX42NJzhSKyfpt:0',
                            node: {
                                hash: 'ooBNdbbRduUCHU5fTEjbfDS8AcyXPgCk4skMbhX42NJzhSKyfpt',
                                batch_position: 0,
                                kind: 'endorsement',
                                timestamp: '2019-10-27T06:19:39Z',
                                level: 667721,
                                block: 'BLXwcgT4Qe88sJuQYvq4jfaNp8NKWABHG18zjsFZaaKrprruhAQ',
                                id: 15310994,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                delegate: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                slots: '{25}',
                            },
                        },
                        {
                            cursor: 'opZn2cJbqpULLcRsJJCqLRE2bCAJAK7E1cs13MDT7nTxSNrxJ8i:0',
                            node: {
                                hash: 'opZn2cJbqpULLcRsJJCqLRE2bCAJAK7E1cs13MDT7nTxSNrxJ8i',
                                batch_position: 0,
                                kind: 'endorsement',
                                timestamp: '2019-10-27T08:44:19Z',
                                level: 667863,
                                block: 'BMMzWra66QyPGWM465uTWz6CewzrzbrK8XJFQUnQ6Fj7owNGP4M',
                                id: 15315917,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                delegate: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                slots: '{25}',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.endorsements last and date_range lte and order_by and after', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS") {
                address
                first_seen
                activated
                public_key
                endorsements (
                last: 3
                order_by: {field: ID, direction: ASC}
                after: "ooWNszkjWUQXiLvEbpi1jGvN2psmKBHjsUQExMhZGUzjtc3LjS4:0"
                date_range: {
                    lte: "2019-10-27T12:46:11Z"
                }
                ) {
                    page_info {
                        start_cursor
                        end_cursor
                        has_next_page
                        has_previous_page
                    }
                    edges {
                        cursor
                        node {
                            hash
                            batch_position
                            kind
                            timestamp
                            level
                            block
                            id
                            source
                            delegate
                            slots
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                first_seen: '2018-09-13T14:51:20Z',
                activated: '2018-09-13T14:49:20.000Z',
                public_key: null,
                endorsements: {
                    page_info: {
                        start_cursor: 'oo8ycUqrUVcp9V2JnkSFdB4uD4UDRMmrfVhTJo3Nkd9J8Z7DCdu:0',
                        end_cursor: 'opZn2cJbqpULLcRsJJCqLRE2bCAJAK7E1cs13MDT7nTxSNrxJ8i:0',
                        has_next_page: false,
                        has_previous_page: true,
                    },
                    edges: [
                        {
                            cursor: 'oo8ycUqrUVcp9V2JnkSFdB4uD4UDRMmrfVhTJo3Nkd9J8Z7DCdu:0',
                            node: {
                                hash: 'oo8ycUqrUVcp9V2JnkSFdB4uD4UDRMmrfVhTJo3Nkd9J8Z7DCdu',
                                batch_position: 0,
                                kind: 'endorsement',
                                timestamp: '2019-10-26T23:40:09Z',
                                level: 667327,
                                block: 'BM6jzwZ9nRU7k1wa7JMGKghP4Sxa9LQ3cjoZUjx4qSfamN67AUC',
                                id: 15300988,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                delegate: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                slots: '{21}',
                            },
                        },
                        {
                            cursor: 'ooBNdbbRduUCHU5fTEjbfDS8AcyXPgCk4skMbhX42NJzhSKyfpt:0',
                            node: {
                                hash: 'ooBNdbbRduUCHU5fTEjbfDS8AcyXPgCk4skMbhX42NJzhSKyfpt',
                                batch_position: 0,
                                kind: 'endorsement',
                                timestamp: '2019-10-27T06:19:39Z',
                                level: 667721,
                                block: 'BLXwcgT4Qe88sJuQYvq4jfaNp8NKWABHG18zjsFZaaKrprruhAQ',
                                id: 15310994,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                delegate: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                slots: '{25}',
                            },
                        },
                        {
                            cursor: 'opZn2cJbqpULLcRsJJCqLRE2bCAJAK7E1cs13MDT7nTxSNrxJ8i:0',
                            node: {
                                hash: 'opZn2cJbqpULLcRsJJCqLRE2bCAJAK7E1cs13MDT7nTxSNrxJ8i',
                                batch_position: 0,
                                kind: 'endorsement',
                                timestamp: '2019-10-27T08:44:19Z',
                                level: 667863,
                                block: 'BMMzWra66QyPGWM465uTWz6CewzrzbrK8XJFQUnQ6Fj7owNGP4M',
                                id: 15315917,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                delegate: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                slots: '{25}',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.endorsements last and date_range gte/lte and order_by and before', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS") {
                address
                first_seen
                activated
                public_key
                endorsements (
                last: 3
                order_by: {field: ID, direction: ASC}
                before: "ooWNszkjWUQXiLvEbpi1jGvN2psmKBHjsUQExMhZGUzjtc3LjS4:0"
                date_range: {
                    gte: "2019-10-22T18:56:24Z"
                    lte: "2019-10-27T12:46:11Z"
                }
                ) {
                    page_info {
                        start_cursor
                        end_cursor
                        has_next_page
                        has_previous_page
                    }
                    edges {
                        cursor
                        node {
                            hash
                            batch_position
                            kind
                            timestamp
                            level
                            block
                            id
                            source
                            delegate
                            slots
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                first_seen: '2018-09-13T14:51:20Z',
                activated: '2018-09-13T14:49:20.000Z',
                public_key: null,
                endorsements: {
                    page_info: {
                        start_cursor: 'opSSmenf1oRYAB7T5L29727L2bTJSjdudn7nCdUCEcJZkc92a2H:0',
                        end_cursor: 'ooUZd3kSL8Z2TRzjHufuU3fiG7kzuFMoyRLD4WRhTtdNtLDzrx1:0',
                        has_next_page: true,
                        has_previous_page: true,
                    },
                    edges: [
                        {
                            cursor: 'opSSmenf1oRYAB7T5L29727L2bTJSjdudn7nCdUCEcJZkc92a2H:0',
                            node: {
                                hash: 'opSSmenf1oRYAB7T5L29727L2bTJSjdudn7nCdUCEcJZkc92a2H',
                                batch_position: 0,
                                kind: 'endorsement',
                                timestamp: '2019-10-26T21:44:13Z',
                                level: 667214,
                                block: 'BLXmM8Amy9WxsHsWrQYp39rYdpDpPkyJkSRqU8mDG985cuZrCXm',
                                id: 15298287,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                delegate: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                slots: '{27}',
                            },
                        },
                        {
                            cursor: 'opWWLfccVoxCdJPc1ZheWsQSkiY53fey5obujMb926XRZE3QhL6:0',
                            node: {
                                hash: 'opWWLfccVoxCdJPc1ZheWsQSkiY53fey5obujMb926XRZE3QhL6',
                                batch_position: 0,
                                kind: 'endorsement',
                                timestamp: '2019-10-26T21:50:13Z',
                                level: 667220,
                                block: 'BMGwWQQRudhVukDipaUHcwymUUtm3aA1471UCiuovm5WKA5ivjd',
                                id: 15298422,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                delegate: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                slots: '{5}',
                            },
                        },
                        {
                            cursor: 'ooUZd3kSL8Z2TRzjHufuU3fiG7kzuFMoyRLD4WRhTtdNtLDzrx1:0',
                            node: {
                                hash: 'ooUZd3kSL8Z2TRzjHufuU3fiG7kzuFMoyRLD4WRhTtdNtLDzrx1',
                                batch_position: 0,
                                kind: 'endorsement',
                                timestamp: '2019-10-26T22:00:13Z',
                                level: 667230,
                                block: 'BM29ZYK6A5vcos3wet81nDJNXjvjj1CtRizXoTcs8MA7cWmiYCE',
                                id: 15298652,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                delegate: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                slots: '{29}',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.endorsements last and date_range gte/lte and order_by and after', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS") {
                address
                first_seen
                activated
                public_key
                endorsements (
                last: 3
                order_by: {field: ID, direction: ASC}
                after: "ooWNszkjWUQXiLvEbpi1jGvN2psmKBHjsUQExMhZGUzjtc3LjS4:0"
                date_range: {
                    gte: "2019-10-22T18:56:24Z"
                    lte: "2019-10-27T12:46:11Z"
                }
                ) {
                    page_info {
                        start_cursor
                        end_cursor
                        has_next_page
                        has_previous_page
                    }
                    edges {
                        cursor
                        node {
                            hash
                            batch_position
                            kind
                            timestamp
                            level
                            block
                            id
                            source
                            delegate
                            slots
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                first_seen: '2018-09-13T14:51:20Z',
                activated: '2018-09-13T14:49:20.000Z',
                public_key: null,
                endorsements: {
                    page_info: {
                        start_cursor: 'oo8ycUqrUVcp9V2JnkSFdB4uD4UDRMmrfVhTJo3Nkd9J8Z7DCdu:0',
                        end_cursor: 'opZn2cJbqpULLcRsJJCqLRE2bCAJAK7E1cs13MDT7nTxSNrxJ8i:0',
                        has_next_page: false,
                        has_previous_page: true,
                    },
                    edges: [
                        {
                            cursor: 'oo8ycUqrUVcp9V2JnkSFdB4uD4UDRMmrfVhTJo3Nkd9J8Z7DCdu:0',
                            node: {
                                hash: 'oo8ycUqrUVcp9V2JnkSFdB4uD4UDRMmrfVhTJo3Nkd9J8Z7DCdu',
                                batch_position: 0,
                                kind: 'endorsement',
                                timestamp: '2019-10-26T23:40:09Z',
                                level: 667327,
                                block: 'BM6jzwZ9nRU7k1wa7JMGKghP4Sxa9LQ3cjoZUjx4qSfamN67AUC',
                                id: 15300988,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                delegate: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                slots: '{21}',
                            },
                        },
                        {
                            cursor: 'ooBNdbbRduUCHU5fTEjbfDS8AcyXPgCk4skMbhX42NJzhSKyfpt:0',
                            node: {
                                hash: 'ooBNdbbRduUCHU5fTEjbfDS8AcyXPgCk4skMbhX42NJzhSKyfpt',
                                batch_position: 0,
                                kind: 'endorsement',
                                timestamp: '2019-10-27T06:19:39Z',
                                level: 667721,
                                block: 'BLXwcgT4Qe88sJuQYvq4jfaNp8NKWABHG18zjsFZaaKrprruhAQ',
                                id: 15310994,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                delegate: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                slots: '{25}',
                            },
                        },
                        {
                            cursor: 'opZn2cJbqpULLcRsJJCqLRE2bCAJAK7E1cs13MDT7nTxSNrxJ8i:0',
                            node: {
                                hash: 'opZn2cJbqpULLcRsJJCqLRE2bCAJAK7E1cs13MDT7nTxSNrxJ8i',
                                batch_position: 0,
                                kind: 'endorsement',
                                timestamp: '2019-10-27T08:44:19Z',
                                level: 667863,
                                block: 'BMMzWra66QyPGWM465uTWz6CewzrzbrK8XJFQUnQ6Fj7owNGP4M',
                                id: 15315917,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                delegate: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                slots: '{25}',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.endorsements date_range gte/lte error', async () => {
        const GET_ACCOUNT = gql`
            query {
                account(address: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS") {
                    address
                    first_seen
                    activated
                    public_key
                    endorsements (
                    first: 3
                    date_range: {
                        gte: "2019-10-22T18:56:24Z"
                        lte: "2019-10-21T12:46:11Z"
                    }
                    ) {
                        page_info {
                            start_cursor
                            end_cursor
                            has_next_page
                            has_previous_page
                        }
                        edges {
                            cursor
                            node {
                                hash
                                batch_position
                                kind
                                timestamp
                                level
                                block
                                id
                                source
                                delegate
                                slots
                            }
                        }
                    }
                }
            }
            `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });
        if (res.errors == undefined) {
            throw new Error('Expected an error message however res.errors is undefined.');
        }

        expect(res.errors[0].message).toMatch('Argument "date" of type "DateRange?" must have the "lte" value greater than the "gte" value.');
    });

    test('account.endorsements query limit error', async () => {
        const GET_ACCOUNT = gql`
            query {
                account(address: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS") {
                    delegations (
                    first: 201
                    ) {
                        page_info {
                            start_cursor
                            end_cursor
                            has_next_page
                            has_previous_page
                        }
                    }
                }
            }
            `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });
        if (res.errors == undefined) {
            throw new Error('Expected an error message however res.errors is undefined.');
        }

        expect(res.errors[0].message).toMatch('Argument \"limit\" of type \"Float?\" cannot be greater than 200.');
    });
});
