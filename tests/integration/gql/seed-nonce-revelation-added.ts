import { gql } from 'apollo-server';

export const seedNonceRevelationAdded = gql`
subscription ($hash: OperationHashFilter, $protocol: ProtocolHashFilter, $branch: BlockHashFilter) {
  seedNonceRevelationAdded(hash: $hash, protocol: $protocol, branch: $branch) {
    kind,
    level,
    nonce,
    metadata {
      balance_updates {
        kind,
        category,
        contract,
        delegate,
        cycle,
        change
      }
    }
  }
}
`;
