import { gql } from 'apollo-server';

export const endorsementAdded = gql`
subscription ($delegate: AddressFilter) {
  endorsementAdded(delegate: $delegate) {
    kind,
    level,
    metadata {
      balance_updates {
        kind,
        category,
        contract,
        delegate,
        cycle,
        change
      },
      delegate,
      slots
    }
  }
}
`;
