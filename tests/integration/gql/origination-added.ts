import { gql } from 'apollo-server';

export const originationAdded = gql`
subscription (
  $source: AddressFilter,
  $delegate: NullableAddressFilter,
  $originated_contract: NullableAddressArrayFilter,
  $status: OperationResultStatusFilter
) {
  originationAdded(source: $source, delegate: $delegate, originated_contract: $originated_contract, status: $status) {
    kind,
    source,
    fee,
    counter,
    gas_limit,
    storage_limit,
    balance,
    delegate,
    script {
      code,
      storage
    },
    metadata {
      balance_updates {
        kind,
        delegate
      },
      internal_operation_results {
        kind,
        source,
        nonce,
        amount,
        destination
      },
      operation_result {
        status,
        consumed_gas,
        errors {
          kind,
          id
        }
      }
    }
  }
}
`;
