import 'reflect-metadata';
import { ApolloServer } from 'apollo-server-express';
import { gql } from 'apollo-server';
import { createTestClient } from 'apollo-server-testing';
import { AccountResolver } from '../../../src/resolvers/account';
import { buildSchema } from 'type-graphql';

async function startTestServer() {
    const testServer = new ApolloServer({
        schema: await buildSchema({
            resolvers: [AccountResolver],
        }),
        context: ({ req, res }) => ({ req, res }),
    });

    return testServer;
}

const server = startTestServer();

let testClientQuery: Function;

async function startTestClient() {
    if (!testClientQuery) {
        const { query } = createTestClient(await server);
        testClientQuery = await query;
    }
}

describe('Account Resolvers', () => {
    test('account.operations first', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS") {
                address
                first_seen
                activated
                public_key
                operations (first: 3) {
                    page_info {
                        start_cursor
                        end_cursor
                        has_next_page
                        has_previous_page
                    }
                    edges {
                        cursor
                        node {
                            hash
                            batch_position
                            kind
                            timestamp
                            level
                            block
                            id
                            source
                            gas
                            counter
                            gas_limit
                            storage_limit
                            public_key
                            amount
                            parameters
                            entrypoint
                            contract_address
                            destination
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                first_seen: '2018-09-13T14:51:20Z',
                activated: '2018-09-13T14:49:20.000Z',
                public_key: null,
                operations: {
                    page_info: {
                        start_cursor: 'opNxsLZeNMttsyh4PxLk35dmRsJyLXQwJjpVp89ip7pU2j5yzyR:0',
                        end_cursor: 'ootgGUV6dDRfBBF9wutPQZWzeetqiAUkwrkwSL1Qy6NrTNUs6Jw:0',
                        has_next_page: true,
                        has_previous_page: false,
                    },
                    edges: [
                        {
                            cursor: 'opNxsLZeNMttsyh4PxLk35dmRsJyLXQwJjpVp89ip7pU2j5yzyR:0',
                            node: {
                                hash: 'opNxsLZeNMttsyh4PxLk35dmRsJyLXQwJjpVp89ip7pU2j5yzyR',
                                batch_position: 0,
                                kind: 'transaction',
                                timestamp: '2020-08-15T22:11:48Z',
                                level: 1085872,
                                block: 'BLEwgk9RsqEfhtm6u3PrLXcu7pXQ6dWMysJRVTAhNRqVqhCsF9g',
                                id: 26331765,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                gas: 1285,
                                counter: 'e2d0010000000000',
                                gas_limit: '4328000000000000',
                                storage_limit: '1501000000000000',
                                public_key: null,
                                amount: '50000000',
                                parameters: '{ "prim": "Unit" }',
                                entrypoint: null,
                                contract_address: null,
                                destination: 'tz1UovZ16T6vTuCRQhWKtE7jqeC6QaTzbc3P',
                            },
                        },
                        {
                            cursor: 'opEHTGa4n4efFQfpaWFNqoY4dbzXrBEo7Xo8v74AX5pEREqK28S:0',
                            node: {
                                hash: 'opEHTGa4n4efFQfpaWFNqoY4dbzXrBEo7Xo8v74AX5pEREqK28S',
                                batch_position: 0,
                                kind: 'transaction',
                                timestamp: '2020-07-17T19:46:24Z',
                                level: 1044207,
                                block: 'BMYhcRrNnHTthRueAKoiyBxyuzJWEMqMgkxzqiVFXHtS84ALKua',
                                id: 25210462,
                                source: 'tz1YPCK2iRxWZYHxeUtKBKRosdQ1SAHKeACe',
                                gas: 1420,
                                counter: '3ee8200000000000',
                                gas_limit: '6829000000000000',
                                storage_limit: '2c01000000000000',
                                public_key: null,
                                amount: '192000000',
                                parameters: '{ "prim": "Unit" }',
                                entrypoint: null,
                                contract_address: null,
                                destination: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                            },
                        },
                        {
                            cursor: 'ootgGUV6dDRfBBF9wutPQZWzeetqiAUkwrkwSL1Qy6NrTNUs6Jw:0',
                            node: {
                                hash: 'ootgGUV6dDRfBBF9wutPQZWzeetqiAUkwrkwSL1Qy6NrTNUs6Jw',
                                batch_position: 0,
                                kind: 'transaction',
                                timestamp: '2020-07-15T20:58:27Z',
                                level: 1041413,
                                block: 'BMf1K4SE3WqRJ6eD345kU74pjLkfB7ABK5u5KwZrxckNHFvUr4v',
                                id: 25134264,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                gas: 1285,
                                counter: 'e1d0010000000000',
                                gas_limit: '4328000000000000',
                                storage_limit: '1501000000000000',
                                public_key: null,
                                amount: '14193181',
                                parameters: '{ "prim": "Unit" }',
                                entrypoint: null,
                                contract_address: null,
                                destination: 'tz1cb9DMkq9ZdrDVxi3HwyLEMtbDseeZPZEa',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.operations first and before', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS") {
                address
                first_seen
                activated
                public_key
                operations (
                  first: 3
                  before: "ooa7UPV5iL5TeV469j9BqeXxFr7tTviyxRRizVFWoc3rU1eAH8N:0"
                ) {
                    page_info {
                        start_cursor
                        end_cursor
                        has_next_page
                        has_previous_page
                    }
                    edges {
                        cursor
                        node {
                            hash
                            batch_position
                            kind
                            timestamp
                            level
                            block
                            id
                            source
                            gas
                            counter
                            gas_limit
                            storage_limit
                            public_key
                            amount
                            parameters
                            entrypoint
                            contract_address
                            destination
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                first_seen: '2018-09-13T14:51:20Z',
                activated: '2018-09-13T14:49:20.000Z',
                public_key: null,
                operations: {
                    page_info: {
                        start_cursor: 'opNxsLZeNMttsyh4PxLk35dmRsJyLXQwJjpVp89ip7pU2j5yzyR:0',
                        end_cursor: 'ootgGUV6dDRfBBF9wutPQZWzeetqiAUkwrkwSL1Qy6NrTNUs6Jw:0',
                        has_next_page: true,
                        has_previous_page: false,
                    },
                    edges: [
                        {
                            cursor: 'opNxsLZeNMttsyh4PxLk35dmRsJyLXQwJjpVp89ip7pU2j5yzyR:0',
                            node: {
                                hash: 'opNxsLZeNMttsyh4PxLk35dmRsJyLXQwJjpVp89ip7pU2j5yzyR',
                                batch_position: 0,
                                kind: 'transaction',
                                timestamp: '2020-08-15T22:11:48Z',
                                level: 1085872,
                                block: 'BLEwgk9RsqEfhtm6u3PrLXcu7pXQ6dWMysJRVTAhNRqVqhCsF9g',
                                id: 26331765,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                gas: 1285,
                                counter: 'e2d0010000000000',
                                gas_limit: '4328000000000000',
                                storage_limit: '1501000000000000',
                                public_key: null,
                                amount: '50000000',
                                parameters: '{ "prim": "Unit" }',
                                entrypoint: null,
                                contract_address: null,
                                destination: 'tz1UovZ16T6vTuCRQhWKtE7jqeC6QaTzbc3P',
                            },
                        },
                        {
                            cursor: 'opEHTGa4n4efFQfpaWFNqoY4dbzXrBEo7Xo8v74AX5pEREqK28S:0',
                            node: {
                                hash: 'opEHTGa4n4efFQfpaWFNqoY4dbzXrBEo7Xo8v74AX5pEREqK28S',
                                batch_position: 0,
                                kind: 'transaction',
                                timestamp: '2020-07-17T19:46:24Z',
                                level: 1044207,
                                block: 'BMYhcRrNnHTthRueAKoiyBxyuzJWEMqMgkxzqiVFXHtS84ALKua',
                                id: 25210462,
                                source: 'tz1YPCK2iRxWZYHxeUtKBKRosdQ1SAHKeACe',
                                gas: 1420,
                                counter: '3ee8200000000000',
                                gas_limit: '6829000000000000',
                                storage_limit: '2c01000000000000',
                                public_key: null,
                                amount: '192000000',
                                parameters: '{ "prim": "Unit" }',
                                entrypoint: null,
                                contract_address: null,
                                destination: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                            },
                        },
                        {
                            cursor: 'ootgGUV6dDRfBBF9wutPQZWzeetqiAUkwrkwSL1Qy6NrTNUs6Jw:0',
                            node: {
                                hash: 'ootgGUV6dDRfBBF9wutPQZWzeetqiAUkwrkwSL1Qy6NrTNUs6Jw',
                                batch_position: 0,
                                kind: 'transaction',
                                timestamp: '2020-07-15T20:58:27Z',
                                level: 1041413,
                                block: 'BMf1K4SE3WqRJ6eD345kU74pjLkfB7ABK5u5KwZrxckNHFvUr4v',
                                id: 25134264,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                gas: 1285,
                                counter: 'e1d0010000000000',
                                gas_limit: '4328000000000000',
                                storage_limit: '1501000000000000',
                                public_key: null,
                                amount: '14193181',
                                parameters: '{ "prim": "Unit" }',
                                entrypoint: null,
                                contract_address: null,
                                destination: 'tz1cb9DMkq9ZdrDVxi3HwyLEMtbDseeZPZEa',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.operations first and after', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS") {
                address
                first_seen
                activated
                public_key
                operations (
                  first: 3
                  after: "oowUdQhpUKrsM9UgPBxPXQHq5ri9BVZGsEWXvFk2w48GkZZXxno:0"
                ) {
                    page_info {
                        start_cursor
                        end_cursor
                        has_next_page
                        has_previous_page
                    }
                    edges {
                        cursor
                        node {
                            hash
                            batch_position
                            kind
                            timestamp
                            level
                            block
                            id
                            source
                            gas
                            counter
                            gas_limit
                            storage_limit
                            public_key
                            amount
                            parameters
                            entrypoint
                            contract_address
                            destination
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                first_seen: '2018-09-13T14:51:20Z',
                activated: '2018-09-13T14:49:20.000Z',
                public_key: null,
                operations: {
                    page_info: {
                        start_cursor: 'ooa7UPV5iL5TeV469j9BqeXxFr7tTviyxRRizVFWoc3rU1eAH8N:0',
                        end_cursor: 'oo8sVjaRzoFHaoxgtVdAbqbPujVJzt1ToknGwapTjHJM5jRhyMx:0',
                        has_next_page: true,
                        has_previous_page: true,
                    },
                    edges: [
                        {
                            cursor: 'ooa7UPV5iL5TeV469j9BqeXxFr7tTviyxRRizVFWoc3rU1eAH8N:0',
                            node: {
                                hash: 'ooa7UPV5iL5TeV469j9BqeXxFr7tTviyxRRizVFWoc3rU1eAH8N',
                                batch_position: 0,
                                kind: 'transaction',
                                timestamp: '2020-07-15T20:55:27Z',
                                level: 1041410,
                                block: 'BMUm98MH7pD2eC9s5WHiPvsSjMFGPyQiJB7zHLQY9uX3ozGpQFE',
                                id: 25134171,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                gas: 1285,
                                counter: 'ded0010000000000',
                                gas_limit: '4328000000000000',
                                storage_limit: '',
                                public_key: null,
                                amount: '325873000',
                                parameters: '{ "prim": "Unit" }',
                                entrypoint: null,
                                contract_address: null,
                                destination: 'tz1gEQe76hTA9L5MpTRtNnDhhqQ1YbzVPxnY',
                            },
                        },
                        {
                            cursor: 'ooZ68VKGkWdnLEY3nRGkaZZAMvMHvdf5nrnssmmhc7eXc7LEQGD:0',
                            node: {
                                hash: 'ooZ68VKGkWdnLEY3nRGkaZZAMvMHvdf5nrnssmmhc7eXc7LEQGD',
                                batch_position: 0,
                                kind: 'transaction',
                                timestamp: '2020-07-15T20:54:27Z',
                                level: 1041409,
                                block: 'BLAuF4FoKqgzDt1rY4NB1VPWmNtNzsN9tJowi9f64KLNofU3McH',
                                id: 25134138,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                gas: 1286,
                                counter: 'ddd0010000000000',
                                gas_limit: '4328000000000000',
                                storage_limit: '1501000000000000',
                                public_key: null,
                                amount: '286268648',
                                parameters: '{ "prim": "Unit" }',
                                entrypoint: null,
                                contract_address: null,
                                destination: 'tz1gEQe76hTA9L5MpTRtNnDhhqQ1YbzVPxnY',
                            },
                        },
                        {
                            cursor: 'oo8sVjaRzoFHaoxgtVdAbqbPujVJzt1ToknGwapTjHJM5jRhyMx:0',
                            node: {
                                hash: 'oo8sVjaRzoFHaoxgtVdAbqbPujVJzt1ToknGwapTjHJM5jRhyMx',
                                batch_position: 0,
                                kind: 'transaction',
                                timestamp: '2020-07-15T20:52:27Z',
                                level: 1041407,
                                block: 'BKj2RNeeJg6ZUQMr3MYWaDv9t6zJE1j9QFTvW6MhMLDRTjtM5Dp',
                                id: 25134075,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                gas: 1284,
                                counter: 'dcd0010000000000',
                                gas_limit: '4328000000000000',
                                storage_limit: '',
                                public_key: null,
                                amount: '223081138',
                                parameters: '{ "prim": "Unit" }',
                                entrypoint: null,
                                contract_address: null,
                                destination: 'tz1RR8ZLwjNNma9JkohknkPCWfiuxKxc5y9W',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.operations first and order_by', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS") {
                address
                first_seen
                activated
                public_key
                operations (
                  first: 3
                  order_by: {field: ID, direction: ASC}
                ) {
                    page_info {
                        start_cursor
                        end_cursor
                        has_next_page
                        has_previous_page
                    }
                    edges {
                        cursor
                        node {
                            hash
                            batch_position
                            kind
                            timestamp
                            level
                            block
                            id
                            source
                            gas
                            counter
                            gas_limit
                            storage_limit
                            public_key
                            amount
                            parameters
                            entrypoint
                            contract_address
                            destination
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                first_seen: '2018-09-13T14:51:20Z',
                activated: '2018-09-13T14:49:20.000Z',
                public_key: null,
                operations: {
                    page_info: {
                        start_cursor: 'ontHLDmy96uXcNBUnarBPoWYSohPN54fNiWbdvHpSEHvA2mdeu5:0',
                        end_cursor: 'oop8aPtDYg7K3xqqMadPKoXzxTLNpBukasrKtBQs9bsjXQ1UWVt:1',
                        has_next_page: true,
                        has_previous_page: false,
                    },
                    edges: [
                        {
                            cursor: 'ontHLDmy96uXcNBUnarBPoWYSohPN54fNiWbdvHpSEHvA2mdeu5:0',
                            node: {
                                hash: 'ontHLDmy96uXcNBUnarBPoWYSohPN54fNiWbdvHpSEHvA2mdeu5',
                                batch_position: 0,
                                kind: 'activate_account',
                                timestamp: '2018-09-13T14:49:20Z',
                                level: 101955,
                                block: 'BLrAAf7V4FDypigoEexGwXdzoonabRSjWdFaEAubV7p5JdGaaU5',
                                id: 1425677,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                gas: null,
                                counter: null,
                                gas_limit: null,
                                storage_limit: null,
                                public_key: null,
                                amount: null,
                                parameters: null,
                                entrypoint: null,
                                contract_address: null,
                                destination: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                            },
                        },
                        {
                            cursor: 'opQqKKXhzznFRxMPdiLyP4V1KWfuDrrNSkqF8jQ6v4JENR3K6jP:0',
                            node: {
                                hash: 'opQqKKXhzznFRxMPdiLyP4V1KWfuDrrNSkqF8jQ6v4JENR3K6jP',
                                batch_position: 0,
                                kind: 'transaction',
                                timestamp: '2018-09-13T14:51:20Z',
                                level: 101957,
                                block: 'BMQzE1UWod12P5XkE15fS4CNRvKLqmWknwCEDuTP1FKEhzrVjtU',
                                id: 1425714,
                                source: 'tz1LBEKXaxQbd5Gtzbc1ATCwc3pppu81aWGc',
                                gas: 10,
                                counter: '62e7000000000000',
                                gas_limit: 'c800000000000000',
                                storage_limit: '',
                                public_key: null,
                                amount: '14',
                                parameters: null,
                                entrypoint: null,
                                contract_address: null,
                                destination: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                            },
                        },
                        {
                            cursor: 'oop8aPtDYg7K3xqqMadPKoXzxTLNpBukasrKtBQs9bsjXQ1UWVt:1',
                            node: {
                                hash: 'oop8aPtDYg7K3xqqMadPKoXzxTLNpBukasrKtBQs9bsjXQ1UWVt',
                                batch_position: 1,
                                kind: 'transaction',
                                timestamp: '2018-09-13T16:12:50Z',
                                level: 102031,
                                block: 'BLowVvoAyjeoMY4BRja6rEPdX37UjfdeB4JFtgQ4ZvCiRg9MNdA',
                                id: 1427150,
                                source: 'tz1PECYmqmuba13rnNxyoBkvH2ChUt8BuJuG',
                                gas: 0,
                                counter: '9d4d000000000000',
                                gas_limit: '',
                                storage_limit: '',
                                public_key: null,
                                amount: '1000000',
                                parameters: null,
                                entrypoint: null,
                                contract_address: null,
                                destination: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.operations first and order_by and before', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS") {
                address
                first_seen
                activated
                public_key
                operations (
                  first: 3
                  order_by: {field: ID, direction: ASC}
                  before: "oo8sVjaRzoFHaoxgtVdAbqbPujVJzt1ToknGwapTjHJM5jRhyMx:0"
                ) {
                    page_info {
                        start_cursor
                        end_cursor
                        has_next_page
                        has_previous_page
                    }
                    edges {
                        cursor
                        node {
                            hash
                            batch_position
                            kind
                            timestamp
                            level
                            block
                            id
                            source
                            gas
                            counter
                            gas_limit
                            storage_limit
                            public_key
                            amount
                            parameters
                            entrypoint
                            contract_address
                            destination
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                first_seen: '2018-09-13T14:51:20Z',
                activated: '2018-09-13T14:49:20.000Z',
                public_key: null,
                operations: {
                    page_info: {
                        start_cursor: 'ontHLDmy96uXcNBUnarBPoWYSohPN54fNiWbdvHpSEHvA2mdeu5:0',
                        end_cursor: 'oop8aPtDYg7K3xqqMadPKoXzxTLNpBukasrKtBQs9bsjXQ1UWVt:1',
                        has_next_page: true,
                        has_previous_page: false,
                    },
                    edges: [
                        {
                            cursor: 'ontHLDmy96uXcNBUnarBPoWYSohPN54fNiWbdvHpSEHvA2mdeu5:0',
                            node: {
                                hash: 'ontHLDmy96uXcNBUnarBPoWYSohPN54fNiWbdvHpSEHvA2mdeu5',
                                batch_position: 0,
                                kind: 'activate_account',
                                timestamp: '2018-09-13T14:49:20Z',
                                level: 101955,
                                block: 'BLrAAf7V4FDypigoEexGwXdzoonabRSjWdFaEAubV7p5JdGaaU5',
                                id: 1425677,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                gas: null,
                                counter: null,
                                gas_limit: null,
                                storage_limit: null,
                                public_key: null,
                                amount: null,
                                parameters: null,
                                entrypoint: null,
                                contract_address: null,
                                destination: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                            },
                        },
                        {
                            cursor: 'opQqKKXhzznFRxMPdiLyP4V1KWfuDrrNSkqF8jQ6v4JENR3K6jP:0',
                            node: {
                                hash: 'opQqKKXhzznFRxMPdiLyP4V1KWfuDrrNSkqF8jQ6v4JENR3K6jP',
                                batch_position: 0,
                                kind: 'transaction',
                                timestamp: '2018-09-13T14:51:20Z',
                                level: 101957,
                                block: 'BMQzE1UWod12P5XkE15fS4CNRvKLqmWknwCEDuTP1FKEhzrVjtU',
                                id: 1425714,
                                source: 'tz1LBEKXaxQbd5Gtzbc1ATCwc3pppu81aWGc',
                                gas: 10,
                                counter: '62e7000000000000',
                                gas_limit: 'c800000000000000',
                                storage_limit: '',
                                public_key: null,
                                amount: '14',
                                parameters: null,
                                entrypoint: null,
                                contract_address: null,
                                destination: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                            },
                        },
                        {
                            cursor: 'oop8aPtDYg7K3xqqMadPKoXzxTLNpBukasrKtBQs9bsjXQ1UWVt:1',
                            node: {
                                hash: 'oop8aPtDYg7K3xqqMadPKoXzxTLNpBukasrKtBQs9bsjXQ1UWVt',
                                batch_position: 1,
                                kind: 'transaction',
                                timestamp: '2018-09-13T16:12:50Z',
                                level: 102031,
                                block: 'BLowVvoAyjeoMY4BRja6rEPdX37UjfdeB4JFtgQ4ZvCiRg9MNdA',
                                id: 1427150,
                                source: 'tz1PECYmqmuba13rnNxyoBkvH2ChUt8BuJuG',
                                gas: 0,
                                counter: '9d4d000000000000',
                                gas_limit: '',
                                storage_limit: '',
                                public_key: null,
                                amount: '1000000',
                                parameters: null,
                                entrypoint: null,
                                contract_address: null,
                                destination: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.operations first and order_by and after', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS") {
                address
                first_seen
                activated
                public_key
                operations (
                  first: 3
                  order_by: {field: ID, direction: ASC}
                  after: "oo8sVjaRzoFHaoxgtVdAbqbPujVJzt1ToknGwapTjHJM5jRhyMx:0"
                ) {
                    page_info {
                        start_cursor
                        end_cursor
                        has_next_page
                        has_previous_page
                    }
                    edges {
                        cursor
                        node {
                            hash
                            batch_position
                            kind
                            timestamp
                            level
                            block
                            id
                            source
                            gas
                            counter
                            gas_limit
                            storage_limit
                            public_key
                            amount
                            parameters
                            entrypoint
                            contract_address
                            destination
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                first_seen: '2018-09-13T14:51:20Z',
                activated: '2018-09-13T14:49:20.000Z',
                public_key: null,
                operations: {
                    page_info: {
                        start_cursor: 'ooZ68VKGkWdnLEY3nRGkaZZAMvMHvdf5nrnssmmhc7eXc7LEQGD:0',
                        end_cursor: 'oowUdQhpUKrsM9UgPBxPXQHq5ri9BVZGsEWXvFk2w48GkZZXxno:0',
                        has_next_page: true,
                        has_previous_page: true,
                    },
                    edges: [
                        {
                            cursor: 'ooZ68VKGkWdnLEY3nRGkaZZAMvMHvdf5nrnssmmhc7eXc7LEQGD:0',
                            node: {
                                hash: 'ooZ68VKGkWdnLEY3nRGkaZZAMvMHvdf5nrnssmmhc7eXc7LEQGD',
                                batch_position: 0,
                                kind: 'transaction',
                                timestamp: '2020-07-15T20:54:27Z',
                                level: 1041409,
                                block: 'BLAuF4FoKqgzDt1rY4NB1VPWmNtNzsN9tJowi9f64KLNofU3McH',
                                id: 25134138,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                gas: 1286,
                                counter: 'ddd0010000000000',
                                gas_limit: '4328000000000000',
                                storage_limit: '1501000000000000',
                                public_key: null,
                                amount: '286268648',
                                parameters: '{ "prim": "Unit" }',
                                entrypoint: null,
                                contract_address: null,
                                destination: 'tz1gEQe76hTA9L5MpTRtNnDhhqQ1YbzVPxnY',
                            },
                        },
                        {
                            cursor: 'ooa7UPV5iL5TeV469j9BqeXxFr7tTviyxRRizVFWoc3rU1eAH8N:0',
                            node: {
                                hash: 'ooa7UPV5iL5TeV469j9BqeXxFr7tTviyxRRizVFWoc3rU1eAH8N',
                                batch_position: 0,
                                kind: 'transaction',
                                timestamp: '2020-07-15T20:55:27Z',
                                level: 1041410,
                                block: 'BMUm98MH7pD2eC9s5WHiPvsSjMFGPyQiJB7zHLQY9uX3ozGpQFE',
                                id: 25134171,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                gas: 1285,
                                counter: 'ded0010000000000',
                                gas_limit: '4328000000000000',
                                storage_limit: '',
                                public_key: null,
                                amount: '325873000',
                                parameters: '{ "prim": "Unit" }',
                                entrypoint: null,
                                contract_address: null,
                                destination: 'tz1gEQe76hTA9L5MpTRtNnDhhqQ1YbzVPxnY',
                            },
                        },
                        {
                            cursor: 'oowUdQhpUKrsM9UgPBxPXQHq5ri9BVZGsEWXvFk2w48GkZZXxno:0',
                            node: {
                                hash: 'oowUdQhpUKrsM9UgPBxPXQHq5ri9BVZGsEWXvFk2w48GkZZXxno',
                                batch_position: 0,
                                kind: 'transaction',
                                timestamp: '2020-07-15T20:56:27Z',
                                level: 1041411,
                                block: 'BL25QMmdBxgkic7PB2z53B38rPTMYy3kAfPevQXDDsBrzbbRy6j',
                                id: 25134203,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                gas: 1284,
                                counter: 'dfd0010000000000',
                                gas_limit: '4328000000000000',
                                storage_limit: '',
                                public_key: null,
                                amount: '79651872',
                                parameters: '{ "prim": "Unit" }',
                                entrypoint: null,
                                contract_address: null,
                                destination: 'tz1bJAQfucuxvLwCHyPXvG24PjtLT1V3ewbt',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.operations first and date_range gte', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS") {
                address
                first_seen
                activated
                public_key
                operations (
                  first: 3
                  date_range: {
                    gte: "2019-10-22T18:56:24Z"
                  }
                ) {
                    page_info {
                        start_cursor
                        end_cursor
                        has_next_page
                        has_previous_page
                    }
                    edges {
                        cursor
                        node {
                            hash
                            batch_position
                            kind
                            timestamp
                            level
                            block
                            id
                            source
                            gas
                            counter
                            gas_limit
                            storage_limit
                            public_key
                            amount
                            parameters
                            entrypoint
                            contract_address
                            destination
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                first_seen: '2018-09-13T14:51:20Z',
                activated: '2018-09-13T14:49:20.000Z',
                public_key: null,
                operations: {
                    page_info: {
                        start_cursor: 'opNxsLZeNMttsyh4PxLk35dmRsJyLXQwJjpVp89ip7pU2j5yzyR:0',
                        end_cursor: 'ootgGUV6dDRfBBF9wutPQZWzeetqiAUkwrkwSL1Qy6NrTNUs6Jw:0',
                        has_next_page: true,
                        has_previous_page: false,
                    },
                    edges: [
                        {
                            cursor: 'opNxsLZeNMttsyh4PxLk35dmRsJyLXQwJjpVp89ip7pU2j5yzyR:0',
                            node: {
                                hash: 'opNxsLZeNMttsyh4PxLk35dmRsJyLXQwJjpVp89ip7pU2j5yzyR',
                                batch_position: 0,
                                kind: 'transaction',
                                timestamp: '2020-08-15T22:11:48Z',
                                level: 1085872,
                                block: 'BLEwgk9RsqEfhtm6u3PrLXcu7pXQ6dWMysJRVTAhNRqVqhCsF9g',
                                id: 26331765,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                gas: 1285,
                                counter: 'e2d0010000000000',
                                gas_limit: '4328000000000000',
                                storage_limit: '1501000000000000',
                                public_key: null,
                                amount: '50000000',
                                parameters: '{ "prim": "Unit" }',
                                entrypoint: null,
                                contract_address: null,
                                destination: 'tz1UovZ16T6vTuCRQhWKtE7jqeC6QaTzbc3P',
                            },
                        },
                        {
                            cursor: 'opEHTGa4n4efFQfpaWFNqoY4dbzXrBEo7Xo8v74AX5pEREqK28S:0',
                            node: {
                                hash: 'opEHTGa4n4efFQfpaWFNqoY4dbzXrBEo7Xo8v74AX5pEREqK28S',
                                batch_position: 0,
                                kind: 'transaction',
                                timestamp: '2020-07-17T19:46:24Z',
                                level: 1044207,
                                block: 'BMYhcRrNnHTthRueAKoiyBxyuzJWEMqMgkxzqiVFXHtS84ALKua',
                                id: 25210462,
                                source: 'tz1YPCK2iRxWZYHxeUtKBKRosdQ1SAHKeACe',
                                gas: 1420,
                                counter: '3ee8200000000000',
                                gas_limit: '6829000000000000',
                                storage_limit: '2c01000000000000',
                                public_key: null,
                                amount: '192000000',
                                parameters: '{ "prim": "Unit" }',
                                entrypoint: null,
                                contract_address: null,
                                destination: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                            },
                        },
                        {
                            cursor: 'ootgGUV6dDRfBBF9wutPQZWzeetqiAUkwrkwSL1Qy6NrTNUs6Jw:0',
                            node: {
                                hash: 'ootgGUV6dDRfBBF9wutPQZWzeetqiAUkwrkwSL1Qy6NrTNUs6Jw',
                                batch_position: 0,
                                kind: 'transaction',
                                timestamp: '2020-07-15T20:58:27Z',
                                level: 1041413,
                                block: 'BMf1K4SE3WqRJ6eD345kU74pjLkfB7ABK5u5KwZrxckNHFvUr4v',
                                id: 25134264,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                gas: 1285,
                                counter: 'e1d0010000000000',
                                gas_limit: '4328000000000000',
                                storage_limit: '1501000000000000',
                                public_key: null,
                                amount: '14193181',
                                parameters: '{ "prim": "Unit" }',
                                entrypoint: null,
                                contract_address: null,
                                destination: 'tz1cb9DMkq9ZdrDVxi3HwyLEMtbDseeZPZEa',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.operations first and date_range gte and before', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS") {
                address
                first_seen
                activated
                public_key
                operations (
                  first: 3
                  date_range: {
                    gte: "2019-10-22T18:56:24Z"
                  }
                  before: "oo8sVjaRzoFHaoxgtVdAbqbPujVJzt1ToknGwapTjHJM5jRhyMx:0"
                ) {
                    page_info {
                        start_cursor
                        end_cursor
                        has_next_page
                        has_previous_page
                    }
                    edges {
                        cursor
                        node {
                            hash
                            batch_position
                            kind
                            timestamp
                            level
                            block
                            id
                            source
                            gas
                            counter
                            gas_limit
                            storage_limit
                            public_key
                            amount
                            parameters
                            entrypoint
                            contract_address
                            destination
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                first_seen: '2018-09-13T14:51:20Z',
                activated: '2018-09-13T14:49:20.000Z',
                public_key: null,
                operations: {
                    page_info: {
                        start_cursor: 'opNxsLZeNMttsyh4PxLk35dmRsJyLXQwJjpVp89ip7pU2j5yzyR:0',
                        end_cursor: 'ootgGUV6dDRfBBF9wutPQZWzeetqiAUkwrkwSL1Qy6NrTNUs6Jw:0',
                        has_next_page: true,
                        has_previous_page: false,
                    },
                    edges: [
                        {
                            cursor: 'opNxsLZeNMttsyh4PxLk35dmRsJyLXQwJjpVp89ip7pU2j5yzyR:0',
                            node: {
                                hash: 'opNxsLZeNMttsyh4PxLk35dmRsJyLXQwJjpVp89ip7pU2j5yzyR',
                                batch_position: 0,
                                kind: 'transaction',
                                timestamp: '2020-08-15T22:11:48Z',
                                level: 1085872,
                                block: 'BLEwgk9RsqEfhtm6u3PrLXcu7pXQ6dWMysJRVTAhNRqVqhCsF9g',
                                id: 26331765,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                gas: 1285,
                                counter: 'e2d0010000000000',
                                gas_limit: '4328000000000000',
                                storage_limit: '1501000000000000',
                                public_key: null,
                                amount: '50000000',
                                parameters: '{ "prim": "Unit" }',
                                entrypoint: null,
                                contract_address: null,
                                destination: 'tz1UovZ16T6vTuCRQhWKtE7jqeC6QaTzbc3P',
                            },
                        },
                        {
                            cursor: 'opEHTGa4n4efFQfpaWFNqoY4dbzXrBEo7Xo8v74AX5pEREqK28S:0',
                            node: {
                                hash: 'opEHTGa4n4efFQfpaWFNqoY4dbzXrBEo7Xo8v74AX5pEREqK28S',
                                batch_position: 0,
                                kind: 'transaction',
                                timestamp: '2020-07-17T19:46:24Z',
                                level: 1044207,
                                block: 'BMYhcRrNnHTthRueAKoiyBxyuzJWEMqMgkxzqiVFXHtS84ALKua',
                                id: 25210462,
                                source: 'tz1YPCK2iRxWZYHxeUtKBKRosdQ1SAHKeACe',
                                gas: 1420,
                                counter: '3ee8200000000000',
                                gas_limit: '6829000000000000',
                                storage_limit: '2c01000000000000',
                                public_key: null,
                                amount: '192000000',
                                parameters: '{ "prim": "Unit" }',
                                entrypoint: null,
                                contract_address: null,
                                destination: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                            },
                        },
                        {
                            cursor: 'ootgGUV6dDRfBBF9wutPQZWzeetqiAUkwrkwSL1Qy6NrTNUs6Jw:0',
                            node: {
                                hash: 'ootgGUV6dDRfBBF9wutPQZWzeetqiAUkwrkwSL1Qy6NrTNUs6Jw',
                                batch_position: 0,
                                kind: 'transaction',
                                timestamp: '2020-07-15T20:58:27Z',
                                level: 1041413,
                                block: 'BMf1K4SE3WqRJ6eD345kU74pjLkfB7ABK5u5KwZrxckNHFvUr4v',
                                id: 25134264,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                gas: 1285,
                                counter: 'e1d0010000000000',
                                gas_limit: '4328000000000000',
                                storage_limit: '1501000000000000',
                                public_key: null,
                                amount: '14193181',
                                parameters: '{ "prim": "Unit" }',
                                entrypoint: null,
                                contract_address: null,
                                destination: 'tz1cb9DMkq9ZdrDVxi3HwyLEMtbDseeZPZEa',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.operations first and date_range gte and after', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS") {
                address
                first_seen
                activated
                public_key
                operations (
                  first: 3
                  date_range: {
                    gte: "2019-10-22T18:56:24Z"
                  }
                  after: "oo8sVjaRzoFHaoxgtVdAbqbPujVJzt1ToknGwapTjHJM5jRhyMx:0"
                ) {
                    page_info {
                        start_cursor
                        end_cursor
                        has_next_page
                        has_previous_page
                    }
                    edges {
                        cursor
                        node {
                            hash
                            batch_position
                            kind
                            timestamp
                            level
                            block
                            id
                            source
                            gas
                            counter
                            gas_limit
                            storage_limit
                            public_key
                            amount
                            parameters
                            entrypoint
                            contract_address
                            destination
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                first_seen: '2018-09-13T14:51:20Z',
                activated: '2018-09-13T14:49:20.000Z',
                public_key: null,
                operations: {
                    page_info: {
                        start_cursor: 'opB8Yp3kyXctnsv1PnSxeHcempXtuvJ8y4ttKhReust8iBXj9t8:0',
                        end_cursor: 'onecWS9W7u5NtbgjHr5KCSvjpvUUUfZDBnKN3ZmpU5n2zm6XTm9:0',
                        has_next_page: true,
                        has_previous_page: true,
                    },
                    edges: [
                        {
                            cursor: 'opB8Yp3kyXctnsv1PnSxeHcempXtuvJ8y4ttKhReust8iBXj9t8:0',
                            node: {
                                hash: 'opB8Yp3kyXctnsv1PnSxeHcempXtuvJ8y4ttKhReust8iBXj9t8',
                                batch_position: 0,
                                kind: 'transaction',
                                timestamp: '2020-07-15T20:50:27Z',
                                level: 1041405,
                                block: 'BKvd9oBBiWLok31cP9ftX6Nftcu9DHq1GVPhXb5KAnuzB2zS5GJ',
                                id: 25134017,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                gas: 1285,
                                counter: 'dbd0010000000000',
                                gas_limit: '4328000000000000',
                                storage_limit: '1501000000000000',
                                public_key: null,
                                amount: '50217319',
                                parameters: '{ "prim": "Unit" }',
                                entrypoint: null,
                                contract_address: null,
                                destination: 'tz1X3xMBb3XzbpK4cehfQRoDgTVFJ2mBQBwt',
                            },
                        },
                        {
                            cursor: 'onmJTCgaEfto2uNzjc5NCZziddopqeEKh2a6dCvJDNMLf6bmqqt:0',
                            node: {
                                hash: 'onmJTCgaEfto2uNzjc5NCZziddopqeEKh2a6dCvJDNMLf6bmqqt',
                                batch_position: 0,
                                kind: 'transaction',
                                timestamp: '2020-07-15T20:48:27Z',
                                level: 1041403,
                                block: 'BLdGSJRJRx1SsfnWonxp4vVMCwnMR8reSDx8CVFY6MPJ5rFjW8h',
                                id: 25133955,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                gas: 1284,
                                counter: 'dad0010000000000',
                                gas_limit: '4328000000000000',
                                storage_limit: '',
                                public_key: null,
                                amount: '11602357',
                                parameters: '{ "prim": "Unit" }',
                                entrypoint: null,
                                contract_address: null,
                                destination: 'tz1ZcKt8A46vuam4hQSHMtBWgFrDhHnYjS9q',
                            },
                        },
                        {
                            cursor: 'onecWS9W7u5NtbgjHr5KCSvjpvUUUfZDBnKN3ZmpU5n2zm6XTm9:0',
                            node: {
                                hash: 'onecWS9W7u5NtbgjHr5KCSvjpvUUUfZDBnKN3ZmpU5n2zm6XTm9',
                                batch_position: 0,
                                kind: 'transaction',
                                timestamp: '2020-07-15T20:47:27Z',
                                level: 1041402,
                                block: 'BMF8HCV1GXYgeH9NhLcxrFdUsE65tpJdVBoXaEynAX3jrD24yEH',
                                id: 25133920,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                gas: 1283,
                                counter: 'd9d0010000000000',
                                gas_limit: '4328000000000000',
                                storage_limit: '',
                                public_key: null,
                                amount: '560732',
                                parameters: '{ "prim": "Unit" }',
                                entrypoint: null,
                                contract_address: null,
                                destination: 'tz1XBrjDhscw9Qg6o43VZ3V6eErDaQjAP6g2',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.operations first and date_range gte and order_by', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS") {
                address
                first_seen
                activated
                public_key
                operations (
                    first: 3
                    date_range: {
                    gte: "2019-10-22T18:56:24Z"
                    }
                    order_by: {field: ID, direction: ASC}
                ) {
                    page_info {
                        start_cursor
                        end_cursor
                        has_next_page
                        has_previous_page
                    }
                    edges {
                        cursor
                        node {
                            hash
                            batch_position
                            kind
                            timestamp
                            level
                            block
                            id
                            source
                            gas
                            counter
                            gas_limit
                            storage_limit
                            public_key
                            amount
                            parameters
                            entrypoint
                            contract_address
                            destination
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                first_seen: '2018-09-13T14:51:20Z',
                activated: '2018-09-13T14:49:20.000Z',
                public_key: null,
                operations: {
                    page_info: {
                        start_cursor: 'ooUuE97rDNfvxm4tVy8CK8yHz6P3LdvFj5Sn41vw26hgRXT5YRr:1',
                        end_cursor: 'ooADLykQuRbk3yUc5f465SRJhGyDtJDFit48WpCp9tipnDK399X:0',
                        has_next_page: true,
                        has_previous_page: false,
                    },
                    edges: [
                        {
                            cursor: 'ooUuE97rDNfvxm4tVy8CK8yHz6P3LdvFj5Sn41vw26hgRXT5YRr:1',
                            node: {
                                hash: 'ooUuE97rDNfvxm4tVy8CK8yHz6P3LdvFj5Sn41vw26hgRXT5YRr',
                                batch_position: 1,
                                kind: 'delegation',
                                timestamp: '2019-10-24T19:05:42Z',
                                level: 664258,
                                block: 'BKm7KPd6erxKgeHE4TTsqe78b5HJx33QTWa6rfNpSkt8PuzHkmf',
                                id: 15224994,
                                source: 'tz1RAWNEEFr2DSEDQVPnHXhH24GLMDF5XNjJ',
                                gas: 2500,
                                counter: '1f9e210000000000',
                                gas_limit: '1027000000000000',
                                storage_limit: '',
                                public_key: null,
                                amount: null,
                                parameters: null,
                                entrypoint: null,
                                contract_address: null,
                                destination: null,
                            },
                        },
                        {
                            cursor: 'onf5R7jCRJMfATspY8BB7ye4izQUVPzsBohqqNgsg8XTjqxReeU:0',
                            node: {
                                hash: 'onf5R7jCRJMfATspY8BB7ye4izQUVPzsBohqqNgsg8XTjqxReeU',
                                batch_position: 0,
                                kind: 'delegation',
                                timestamp: '2019-10-24T19:30:42Z',
                                level: 664283,
                                block: 'BM2WbVVx8AG1rdMcVNtiX9wFtrTFSk2BKrMhbHh1Z56gTwnzQs7',
                                id: 15225640,
                                source: 'tz1YPCK2iRxWZYHxeUtKBKRosdQ1SAHKeACe',
                                gas: 2500,
                                counter: '3be8200000000000',
                                gas_limit: '1027000000000000',
                                storage_limit: '',
                                public_key: null,
                                amount: null,
                                parameters: null,
                                entrypoint: null,
                                contract_address: null,
                                destination: null,
                            },
                        },
                        {
                            cursor: 'ooADLykQuRbk3yUc5f465SRJhGyDtJDFit48WpCp9tipnDK399X:0',
                            node: {
                                hash: 'ooADLykQuRbk3yUc5f465SRJhGyDtJDFit48WpCp9tipnDK399X',
                                batch_position: 0,
                                kind: 'transaction',
                                timestamp: '2019-10-24T20:38:46Z',
                                level: 664348,
                                block: 'BM8uvwRPXp7QtwUwcr2FtSsooUJcRbnN7hvDhUochXS8dpcXfQ8',
                                id: 15227316,
                                source: 'tz1KtGwriE7VuLwT3LwuvU9Nv4wAxP7XZ57d',
                                gas: 17920,
                                counter: 'e2fe010000000000',
                                gas_limit: 'fa58020000000000',
                                storage_limit: '2c01000000000000',
                                public_key: null,
                                amount: '10009000000',
                                parameters: '{ "prim": "Unit" }',
                                entrypoint: null,
                                contract_address: null,
                                destination: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.operations first and date_range lte', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS") {
                address
                first_seen
                activated
                public_key
                operations (
                  first: 3
                  date_range: {
                    lte: "2019-10-22T18:56:24Z"
                  }
                ) {
                    page_info {
                        start_cursor
                        end_cursor
                        has_next_page
                        has_previous_page
                    }
                    edges {
                        cursor
                        node {
                            hash
                            batch_position
                            kind
                            timestamp
                            level
                            block
                            id
                            source
                            gas
                            counter
                            gas_limit
                            storage_limit
                            public_key
                            amount
                            parameters
                            entrypoint
                            contract_address
                            destination
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                first_seen: '2018-09-13T14:51:20Z',
                activated: '2018-09-13T14:49:20.000Z',
                public_key: null,
                operations: {
                    page_info: {
                        start_cursor: 'opT4s5rWRbAEcbq6GZ272LyxNCxACqdk7c6Jm4buDcR6Fh4Zs3c:0',
                        end_cursor: 'opZSVPxNoaExjX6dCFnoPAh5yHk6g7BaJdce2AXUHuwW3c7ucNA:0',
                        has_next_page: true,
                        has_previous_page: false,
                    },
                    edges: [
                        {
                            cursor: 'opT4s5rWRbAEcbq6GZ272LyxNCxACqdk7c6Jm4buDcR6Fh4Zs3c:0',
                            node: {
                                hash: 'opT4s5rWRbAEcbq6GZ272LyxNCxACqdk7c6Jm4buDcR6Fh4Zs3c',
                                batch_position: 0,
                                kind: 'transaction',
                                timestamp: '2019-06-23T00:44:47Z',
                                level: 491770,
                                block: 'BLyWtfcsLhVGAbsYJPSFpUQiPwdHkHExBrgB3WD9wesuo3PLfUV',
                                id: 10910203,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                gas: 1564,
                                counter: 'c8d0010000000000',
                                gas_limit: '4533000000000000',
                                storage_limit: '',
                                public_key: null,
                                amount: '0',
                                parameters: null,
                                entrypoint: null,
                                contract_address: null,
                                destination: 'KT1BRudFZEXLYANgmZTka1xCDN5nWTMWY7SZ',
                            },
                        },
                        {
                            cursor: 'oobMbc7MHSz5ttV3AcjiXqaxZSxs9ShGjcHmopoo85Jays2rniJ:0',
                            node: {
                                hash: 'oobMbc7MHSz5ttV3AcjiXqaxZSxs9ShGjcHmopoo85Jays2rniJ',
                                batch_position: 0,
                                kind: 'transaction',
                                timestamp: '2019-06-13T16:31:03Z',
                                level: 478922,
                                block: 'BKsTAwJ939thYVT5gN4HGE8kPApsRUXELGySbv7CNgXZ9VkGKar',
                                id: 10588192,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                gas: 1284,
                                counter: 'c7d0010000000000',
                                gas_limit: '3c28000000000000',
                                storage_limit: '',
                                public_key: null,
                                amount: '21594067',
                                parameters: null,
                                entrypoint: null,
                                contract_address: null,
                                destination: 'KT1SrkUKHSf9m2yruXSMCVfrfUUsXN8yVtag',
                            },
                        },
                        {
                            cursor: 'opZSVPxNoaExjX6dCFnoPAh5yHk6g7BaJdce2AXUHuwW3c7ucNA:0',
                            node: {
                                hash: 'opZSVPxNoaExjX6dCFnoPAh5yHk6g7BaJdce2AXUHuwW3c7ucNA',
                                batch_position: 0,
                                kind: 'transaction',
                                timestamp: '2019-06-13T16:29:03Z',
                                level: 478920,
                                block: 'BME5VpmuMaZfWK7sgrmqrH73yZnuiDhJGuE8dsURJTodB5LWDCe',
                                id: 10588145,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                gas: 1284,
                                counter: 'c6d0010000000000',
                                gas_limit: '3c28000000000000',
                                storage_limit: '',
                                public_key: null,
                                amount: '26594806',
                                parameters: null,
                                entrypoint: null,
                                contract_address: null,
                                destination: 'KT1Hw82JKNLeQUF58Tr5s8hk3Yn8XW3Hv81C',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.operations first and date_range lte and before', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS") {
                address
                first_seen
                activated
                public_key
                operations (
                  first: 3
                  date_range: {
                    lte: "2019-10-22T18:56:24Z"
                  }
                  before: "opZSVPxNoaExjX6dCFnoPAh5yHk6g7BaJdce2AXUHuwW3c7ucNA:0"
                ) {
                    page_info {
                        start_cursor
                        end_cursor
                        has_next_page
                        has_previous_page
                    }
                    edges {
                        cursor
                        node {
                            hash
                            batch_position
                            kind
                            timestamp
                            level
                            block
                            id
                            source
                            gas
                            counter
                            gas_limit
                            storage_limit
                            public_key
                            amount
                            parameters
                            entrypoint
                            contract_address
                            destination
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                first_seen: '2018-09-13T14:51:20Z',
                activated: '2018-09-13T14:49:20.000Z',
                public_key: null,
                operations: {
                    page_info: {
                        start_cursor: 'opT4s5rWRbAEcbq6GZ272LyxNCxACqdk7c6Jm4buDcR6Fh4Zs3c:0',
                        end_cursor: 'oobMbc7MHSz5ttV3AcjiXqaxZSxs9ShGjcHmopoo85Jays2rniJ:0',
                        has_next_page: true,
                        has_previous_page: false,
                    },
                    edges: [
                        {
                            cursor: 'opT4s5rWRbAEcbq6GZ272LyxNCxACqdk7c6Jm4buDcR6Fh4Zs3c:0',
                            node: {
                                hash: 'opT4s5rWRbAEcbq6GZ272LyxNCxACqdk7c6Jm4buDcR6Fh4Zs3c',
                                batch_position: 0,
                                kind: 'transaction',
                                timestamp: '2019-06-23T00:44:47Z',
                                level: 491770,
                                block: 'BLyWtfcsLhVGAbsYJPSFpUQiPwdHkHExBrgB3WD9wesuo3PLfUV',
                                id: 10910203,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                gas: 1564,
                                counter: 'c8d0010000000000',
                                gas_limit: '4533000000000000',
                                storage_limit: '',
                                public_key: null,
                                amount: '0',
                                parameters: null,
                                entrypoint: null,
                                contract_address: null,
                                destination: 'KT1BRudFZEXLYANgmZTka1xCDN5nWTMWY7SZ',
                            },
                        },
                        {
                            cursor: 'oobMbc7MHSz5ttV3AcjiXqaxZSxs9ShGjcHmopoo85Jays2rniJ:0',
                            node: {
                                hash: 'oobMbc7MHSz5ttV3AcjiXqaxZSxs9ShGjcHmopoo85Jays2rniJ',
                                batch_position: 0,
                                kind: 'transaction',
                                timestamp: '2019-06-13T16:31:03Z',
                                level: 478922,
                                block: 'BKsTAwJ939thYVT5gN4HGE8kPApsRUXELGySbv7CNgXZ9VkGKar',
                                id: 10588192,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                gas: 1284,
                                counter: 'c7d0010000000000',
                                gas_limit: '3c28000000000000',
                                storage_limit: '',
                                public_key: null,
                                amount: '21594067',
                                parameters: null,
                                entrypoint: null,
                                contract_address: null,
                                destination: 'KT1SrkUKHSf9m2yruXSMCVfrfUUsXN8yVtag',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.operations first and date_range lte and after', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS") {
                address
                first_seen
                activated
                public_key
                operations (
                  first: 3
                  date_range: {
                    lte: "2019-10-22T18:56:24Z"
                  }
                  after: "oobMbc7MHSz5ttV3AcjiXqaxZSxs9ShGjcHmopoo85Jays2rniJ:0"
                ) {
                    page_info {
                        start_cursor
                        end_cursor
                        has_next_page
                        has_previous_page
                    }
                    edges {
                        cursor
                        node {
                            hash
                            batch_position
                            kind
                            timestamp
                            level
                            block
                            id
                            source
                            gas
                            counter
                            gas_limit
                            storage_limit
                            public_key
                            amount
                            parameters
                            entrypoint
                            contract_address
                            destination
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                first_seen: '2018-09-13T14:51:20Z',
                activated: '2018-09-13T14:49:20.000Z',
                public_key: null,
                operations: {
                    page_info: {
                        start_cursor: 'opZSVPxNoaExjX6dCFnoPAh5yHk6g7BaJdce2AXUHuwW3c7ucNA:0',
                        end_cursor: 'op2Jk1AuscHtt6NWL58ETYmqenNHK6C1KSv7Rk1rPzadSTKqpL1:0',
                        has_next_page: true,
                        has_previous_page: true,
                    },
                    edges: [
                        {
                            cursor: 'opZSVPxNoaExjX6dCFnoPAh5yHk6g7BaJdce2AXUHuwW3c7ucNA:0',
                            node: {
                                hash: 'opZSVPxNoaExjX6dCFnoPAh5yHk6g7BaJdce2AXUHuwW3c7ucNA',
                                batch_position: 0,
                                kind: 'transaction',
                                timestamp: '2019-06-13T16:29:03Z',
                                level: 478920,
                                block: 'BME5VpmuMaZfWK7sgrmqrH73yZnuiDhJGuE8dsURJTodB5LWDCe',
                                id: 10588145,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                gas: 1284,
                                counter: 'c6d0010000000000',
                                gas_limit: '3c28000000000000',
                                storage_limit: '',
                                public_key: null,
                                amount: '26594806',
                                parameters: null,
                                entrypoint: null,
                                contract_address: null,
                                destination: 'KT1Hw82JKNLeQUF58Tr5s8hk3Yn8XW3Hv81C',
                            },
                        },
                        {
                            cursor: 'ooZ59b3ddyATdUsbiWwX6gco4hZ7VJeBbucCjvAbNyggrnTDKbN:0',
                            node: {
                                hash: 'ooZ59b3ddyATdUsbiWwX6gco4hZ7VJeBbucCjvAbNyggrnTDKbN',
                                batch_position: 0,
                                kind: 'transaction',
                                timestamp: '2019-06-13T16:27:03Z',
                                level: 478918,
                                block: 'BLhWGpWqz19bGcLYoJxotAxm17vNGjymwMbGK5yizRRMQhkxEai',
                                id: 10588092,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                gas: 1284,
                                counter: 'c5d0010000000000',
                                gas_limit: '3c28000000000000',
                                storage_limit: '',
                                public_key: null,
                                amount: '166379804',
                                parameters: null,
                                entrypoint: null,
                                contract_address: null,
                                destination: 'KT1FPhB9bBH6BXH7a7DsX5eBUvJRyqcZhfyn',
                            },
                        },
                        {
                            cursor: 'op2Jk1AuscHtt6NWL58ETYmqenNHK6C1KSv7Rk1rPzadSTKqpL1:0',
                            node: {
                                hash: 'op2Jk1AuscHtt6NWL58ETYmqenNHK6C1KSv7Rk1rPzadSTKqpL1',
                                batch_position: 0,
                                kind: 'transaction',
                                timestamp: '2019-06-13T16:26:03Z',
                                level: 478917,
                                block: 'BLBgbfhyuDD3Q2b7mAKm2S4kroYLVDp3gbkRpU7PPaZLDodi11e',
                                id: 10588066,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                gas: 1284,
                                counter: 'c4d0010000000000',
                                gas_limit: '3c28000000000000',
                                storage_limit: '',
                                public_key: null,
                                amount: '171235783',
                                parameters: null,
                                entrypoint: null,
                                contract_address: null,
                                destination: 'KT1C8f3CYXEwuvvvGeG9pKMLWa2Xcyci8eUn',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.operations first and date_range lte and order_by', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS") {
                address
                first_seen
                activated
                public_key
                operations (
                    first: 3
                    date_range: {
                    lte: "2019-10-22T18:56:24Z"
                    }
                    order_by: {field: ID, direction: ASC}
                ) {
                    page_info {
                        start_cursor
                        end_cursor
                        has_next_page
                        has_previous_page
                    }
                    edges {
                        cursor
                        node {
                            hash
                            batch_position
                            kind
                            timestamp
                            level
                            block
                            id
                            source
                            gas
                            counter
                            gas_limit
                            storage_limit
                            public_key
                            amount
                            parameters
                            entrypoint
                            contract_address
                            destination
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                first_seen: '2018-09-13T14:51:20Z',
                activated: '2018-09-13T14:49:20.000Z',
                public_key: null,
                operations: {
                    page_info: {
                        start_cursor: 'ontHLDmy96uXcNBUnarBPoWYSohPN54fNiWbdvHpSEHvA2mdeu5:0',
                        end_cursor: 'oop8aPtDYg7K3xqqMadPKoXzxTLNpBukasrKtBQs9bsjXQ1UWVt:1',
                        has_next_page: true,
                        has_previous_page: false,
                    },
                    edges: [
                        {
                            cursor: 'ontHLDmy96uXcNBUnarBPoWYSohPN54fNiWbdvHpSEHvA2mdeu5:0',
                            node: {
                                hash: 'ontHLDmy96uXcNBUnarBPoWYSohPN54fNiWbdvHpSEHvA2mdeu5',
                                batch_position: 0,
                                kind: 'activate_account',
                                timestamp: '2018-09-13T14:49:20Z',
                                level: 101955,
                                block: 'BLrAAf7V4FDypigoEexGwXdzoonabRSjWdFaEAubV7p5JdGaaU5',
                                id: 1425677,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                gas: null,
                                counter: null,
                                gas_limit: null,
                                storage_limit: null,
                                public_key: null,
                                amount: null,
                                parameters: null,
                                entrypoint: null,
                                contract_address: null,
                                destination: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                            },
                        },
                        {
                            cursor: 'opQqKKXhzznFRxMPdiLyP4V1KWfuDrrNSkqF8jQ6v4JENR3K6jP:0',
                            node: {
                                hash: 'opQqKKXhzznFRxMPdiLyP4V1KWfuDrrNSkqF8jQ6v4JENR3K6jP',
                                batch_position: 0,
                                kind: 'transaction',
                                timestamp: '2018-09-13T14:51:20Z',
                                level: 101957,
                                block: 'BMQzE1UWod12P5XkE15fS4CNRvKLqmWknwCEDuTP1FKEhzrVjtU',
                                id: 1425714,
                                source: 'tz1LBEKXaxQbd5Gtzbc1ATCwc3pppu81aWGc',
                                gas: 10,
                                counter: '62e7000000000000',
                                gas_limit: 'c800000000000000',
                                storage_limit: '',
                                public_key: null,
                                amount: '14',
                                parameters: null,
                                entrypoint: null,
                                contract_address: null,
                                destination: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                            },
                        },
                        {
                            cursor: 'oop8aPtDYg7K3xqqMadPKoXzxTLNpBukasrKtBQs9bsjXQ1UWVt:1',
                            node: {
                                hash: 'oop8aPtDYg7K3xqqMadPKoXzxTLNpBukasrKtBQs9bsjXQ1UWVt',
                                batch_position: 1,
                                kind: 'transaction',
                                timestamp: '2018-09-13T16:12:50Z',
                                level: 102031,
                                block: 'BLowVvoAyjeoMY4BRja6rEPdX37UjfdeB4JFtgQ4ZvCiRg9MNdA',
                                id: 1427150,
                                source: 'tz1PECYmqmuba13rnNxyoBkvH2ChUt8BuJuG',
                                gas: 0,
                                counter: '9d4d000000000000',
                                gas_limit: '',
                                storage_limit: '',
                                public_key: null,
                                amount: '1000000',
                                parameters: null,
                                entrypoint: null,
                                contract_address: null,
                                destination: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.operations first and date_range gte/lte', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS") {
                address
                first_seen
                activated
                public_key
                operations (
                  first: 3
                  date_range: {
                    gte: "2019-10-22T18:56:24Z"
                    lte: "2019-10-27T12:46:11Z"
                  }
                ) {
                    page_info {
                        start_cursor
                        end_cursor
                        has_next_page
                        has_previous_page
                    }
                    edges {
                        cursor
                        node {
                            hash
                            batch_position
                            kind
                            timestamp
                            level
                            block
                            id
                            source
                            gas
                            counter
                            gas_limit
                            storage_limit
                            public_key
                            amount
                            parameters
                            entrypoint
                            contract_address
                            destination
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                first_seen: '2018-09-13T14:51:20Z',
                activated: '2018-09-13T14:49:20.000Z',
                public_key: null,
                operations: {
                    page_info: {
                        start_cursor: 'ooADLykQuRbk3yUc5f465SRJhGyDtJDFit48WpCp9tipnDK399X:0',
                        end_cursor: 'ooUuE97rDNfvxm4tVy8CK8yHz6P3LdvFj5Sn41vw26hgRXT5YRr:1',
                        has_next_page: false,
                        has_previous_page: false,
                    },
                    edges: [
                        {
                            cursor: 'ooADLykQuRbk3yUc5f465SRJhGyDtJDFit48WpCp9tipnDK399X:0',
                            node: {
                                hash: 'ooADLykQuRbk3yUc5f465SRJhGyDtJDFit48WpCp9tipnDK399X',
                                batch_position: 0,
                                kind: 'transaction',
                                timestamp: '2019-10-24T20:38:46Z',
                                level: 664348,
                                block: 'BM8uvwRPXp7QtwUwcr2FtSsooUJcRbnN7hvDhUochXS8dpcXfQ8',
                                id: 15227316,
                                source: 'tz1KtGwriE7VuLwT3LwuvU9Nv4wAxP7XZ57d',
                                gas: 17920,
                                counter: 'e2fe010000000000',
                                gas_limit: 'fa58020000000000',
                                storage_limit: '2c01000000000000',
                                public_key: null,
                                amount: '10009000000',
                                parameters: '{ "prim": "Unit" }',
                                entrypoint: null,
                                contract_address: null,
                                destination: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                            },
                        },
                        {
                            cursor: 'onf5R7jCRJMfATspY8BB7ye4izQUVPzsBohqqNgsg8XTjqxReeU:0',
                            node: {
                                hash: 'onf5R7jCRJMfATspY8BB7ye4izQUVPzsBohqqNgsg8XTjqxReeU',
                                batch_position: 0,
                                kind: 'delegation',
                                timestamp: '2019-10-24T19:30:42Z',
                                level: 664283,
                                block: 'BM2WbVVx8AG1rdMcVNtiX9wFtrTFSk2BKrMhbHh1Z56gTwnzQs7',
                                id: 15225640,
                                source: 'tz1YPCK2iRxWZYHxeUtKBKRosdQ1SAHKeACe',
                                gas: 2500,
                                counter: '3be8200000000000',
                                gas_limit: '1027000000000000',
                                storage_limit: '',
                                public_key: null,
                                amount: null,
                                parameters: null,
                                entrypoint: null,
                                contract_address: null,
                                destination: null,
                            },
                        },
                        {
                            cursor: 'ooUuE97rDNfvxm4tVy8CK8yHz6P3LdvFj5Sn41vw26hgRXT5YRr:1',
                            node: {
                                hash: 'ooUuE97rDNfvxm4tVy8CK8yHz6P3LdvFj5Sn41vw26hgRXT5YRr',
                                batch_position: 1,
                                kind: 'delegation',
                                timestamp: '2019-10-24T19:05:42Z',
                                level: 664258,
                                block: 'BKm7KPd6erxKgeHE4TTsqe78b5HJx33QTWa6rfNpSkt8PuzHkmf',
                                id: 15224994,
                                source: 'tz1RAWNEEFr2DSEDQVPnHXhH24GLMDF5XNjJ',
                                gas: 2500,
                                counter: '1f9e210000000000',
                                gas_limit: '1027000000000000',
                                storage_limit: '',
                                public_key: null,
                                amount: null,
                                parameters: null,
                                entrypoint: null,
                                contract_address: null,
                                destination: null,
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.operations first and date_range gte/lte and before', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS") {
                address
                first_seen
                activated
                public_key
                operations (
                  first: 3
                  date_range: {
                    gte: "2019-10-22T18:56:24Z"
                    lte: "2019-10-27T12:46:11Z"
                  }
                  before: "ooUuE97rDNfvxm4tVy8CK8yHz6P3LdvFj5Sn41vw26hgRXT5YRr:1"
                ) {
                    page_info {
                        start_cursor
                        end_cursor
                        has_next_page
                        has_previous_page
                    }
                    edges {
                        cursor
                        node {
                            hash
                            batch_position
                            kind
                            timestamp
                            level
                            block
                            id
                            source
                            gas
                            counter
                            gas_limit
                            storage_limit
                            public_key
                            amount
                            parameters
                            entrypoint
                            contract_address
                            destination
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                first_seen: '2018-09-13T14:51:20Z',
                activated: '2018-09-13T14:49:20.000Z',
                public_key: null,
                operations: {
                    page_info: {
                        start_cursor: 'ooADLykQuRbk3yUc5f465SRJhGyDtJDFit48WpCp9tipnDK399X:0',
                        end_cursor: 'onf5R7jCRJMfATspY8BB7ye4izQUVPzsBohqqNgsg8XTjqxReeU:0',
                        has_next_page: true,
                        has_previous_page: false,
                    },
                    edges: [
                        {
                            cursor: 'ooADLykQuRbk3yUc5f465SRJhGyDtJDFit48WpCp9tipnDK399X:0',
                            node: {
                                hash: 'ooADLykQuRbk3yUc5f465SRJhGyDtJDFit48WpCp9tipnDK399X',
                                batch_position: 0,
                                kind: 'transaction',
                                timestamp: '2019-10-24T20:38:46Z',
                                level: 664348,
                                block: 'BM8uvwRPXp7QtwUwcr2FtSsooUJcRbnN7hvDhUochXS8dpcXfQ8',
                                id: 15227316,
                                source: 'tz1KtGwriE7VuLwT3LwuvU9Nv4wAxP7XZ57d',
                                gas: 17920,
                                counter: 'e2fe010000000000',
                                gas_limit: 'fa58020000000000',
                                storage_limit: '2c01000000000000',
                                public_key: null,
                                amount: '10009000000',
                                parameters: '{ "prim": "Unit" }',
                                entrypoint: null,
                                contract_address: null,
                                destination: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                            },
                        },
                        {
                            cursor: 'onf5R7jCRJMfATspY8BB7ye4izQUVPzsBohqqNgsg8XTjqxReeU:0',
                            node: {
                                hash: 'onf5R7jCRJMfATspY8BB7ye4izQUVPzsBohqqNgsg8XTjqxReeU',
                                batch_position: 0,
                                kind: 'delegation',
                                timestamp: '2019-10-24T19:30:42Z',
                                level: 664283,
                                block: 'BM2WbVVx8AG1rdMcVNtiX9wFtrTFSk2BKrMhbHh1Z56gTwnzQs7',
                                id: 15225640,
                                source: 'tz1YPCK2iRxWZYHxeUtKBKRosdQ1SAHKeACe',
                                gas: 2500,
                                counter: '3be8200000000000',
                                gas_limit: '1027000000000000',
                                storage_limit: '',
                                public_key: null,
                                amount: null,
                                parameters: null,
                                entrypoint: null,
                                contract_address: null,
                                destination: null,
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.operations first and date_range gte/lte and after', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS") {
                address
                first_seen
                activated
                public_key
                operations (
                  first: 3
                  date_range: {
                    gte: "2019-10-22T18:56:24Z"
                    lte: "2019-10-27T12:46:11Z"
                  }
                  after: "ooADLykQuRbk3yUc5f465SRJhGyDtJDFit48WpCp9tipnDK399X:0"
                ) {
                    page_info {
                        start_cursor
                        end_cursor
                        has_next_page
                        has_previous_page
                    }
                    edges {
                        cursor
                        node {
                            hash
                            batch_position
                            kind
                            timestamp
                            level
                            block
                            id
                            source
                            gas
                            counter
                            gas_limit
                            storage_limit
                            public_key
                            amount
                            parameters
                            entrypoint
                            contract_address
                            destination
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                first_seen: '2018-09-13T14:51:20Z',
                activated: '2018-09-13T14:49:20.000Z',
                public_key: null,
                operations: {
                    page_info: {
                        start_cursor: 'onf5R7jCRJMfATspY8BB7ye4izQUVPzsBohqqNgsg8XTjqxReeU:0',
                        end_cursor: 'ooUuE97rDNfvxm4tVy8CK8yHz6P3LdvFj5Sn41vw26hgRXT5YRr:1',
                        has_next_page: false,
                        has_previous_page: true,
                    },
                    edges: [
                        {
                            cursor: 'onf5R7jCRJMfATspY8BB7ye4izQUVPzsBohqqNgsg8XTjqxReeU:0',
                            node: {
                                hash: 'onf5R7jCRJMfATspY8BB7ye4izQUVPzsBohqqNgsg8XTjqxReeU',
                                batch_position: 0,
                                kind: 'delegation',
                                timestamp: '2019-10-24T19:30:42Z',
                                level: 664283,
                                block: 'BM2WbVVx8AG1rdMcVNtiX9wFtrTFSk2BKrMhbHh1Z56gTwnzQs7',
                                id: 15225640,
                                source: 'tz1YPCK2iRxWZYHxeUtKBKRosdQ1SAHKeACe',
                                gas: 2500,
                                counter: '3be8200000000000',
                                gas_limit: '1027000000000000',
                                storage_limit: '',
                                public_key: null,
                                amount: null,
                                parameters: null,
                                entrypoint: null,
                                contract_address: null,
                                destination: null,
                            },
                        },
                        {
                            cursor: 'ooUuE97rDNfvxm4tVy8CK8yHz6P3LdvFj5Sn41vw26hgRXT5YRr:1',
                            node: {
                                hash: 'ooUuE97rDNfvxm4tVy8CK8yHz6P3LdvFj5Sn41vw26hgRXT5YRr',
                                batch_position: 1,
                                kind: 'delegation',
                                timestamp: '2019-10-24T19:05:42Z',
                                level: 664258,
                                block: 'BKm7KPd6erxKgeHE4TTsqe78b5HJx33QTWa6rfNpSkt8PuzHkmf',
                                id: 15224994,
                                source: 'tz1RAWNEEFr2DSEDQVPnHXhH24GLMDF5XNjJ',
                                gas: 2500,
                                counter: '1f9e210000000000',
                                gas_limit: '1027000000000000',
                                storage_limit: '',
                                public_key: null,
                                amount: null,
                                parameters: null,
                                entrypoint: null,
                                contract_address: null,
                                destination: null,
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.operations first and date_range gte/lte and order_by', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS") {
                address
                first_seen
                activated
                public_key
                operations (
                    first: 3
                    date_range: {
                    gte: "2019-10-22T18:56:24Z"
                    lte: "2019-10-27T12:46:11Z"
                    }
                    order_by: {field: ID, direction: ASC}
                ) {
                    page_info {
                        start_cursor
                        end_cursor
                        has_next_page
                        has_previous_page
                    }
                    edges {
                        cursor
                        node {
                            hash
                            batch_position
                            kind
                            timestamp
                            level
                            block
                            id
                            source
                            gas
                            counter
                            gas_limit
                            storage_limit
                            public_key
                            amount
                            parameters
                            entrypoint
                            contract_address
                            destination
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                first_seen: '2018-09-13T14:51:20Z',
                activated: '2018-09-13T14:49:20.000Z',
                public_key: null,
                operations: {
                    page_info: {
                        start_cursor: 'ooUuE97rDNfvxm4tVy8CK8yHz6P3LdvFj5Sn41vw26hgRXT5YRr:1',
                        end_cursor: 'ooADLykQuRbk3yUc5f465SRJhGyDtJDFit48WpCp9tipnDK399X:0',
                        has_next_page: false,
                        has_previous_page: false,
                    },
                    edges: [
                        {
                            cursor: 'ooUuE97rDNfvxm4tVy8CK8yHz6P3LdvFj5Sn41vw26hgRXT5YRr:1',
                            node: {
                                hash: 'ooUuE97rDNfvxm4tVy8CK8yHz6P3LdvFj5Sn41vw26hgRXT5YRr',
                                batch_position: 1,
                                kind: 'delegation',
                                timestamp: '2019-10-24T19:05:42Z',
                                level: 664258,
                                block: 'BKm7KPd6erxKgeHE4TTsqe78b5HJx33QTWa6rfNpSkt8PuzHkmf',
                                id: 15224994,
                                source: 'tz1RAWNEEFr2DSEDQVPnHXhH24GLMDF5XNjJ',
                                gas: 2500,
                                counter: '1f9e210000000000',
                                gas_limit: '1027000000000000',
                                storage_limit: '',
                                public_key: null,
                                amount: null,
                                parameters: null,
                                entrypoint: null,
                                contract_address: null,
                                destination: null,
                            },
                        },
                        {
                            cursor: 'onf5R7jCRJMfATspY8BB7ye4izQUVPzsBohqqNgsg8XTjqxReeU:0',
                            node: {
                                hash: 'onf5R7jCRJMfATspY8BB7ye4izQUVPzsBohqqNgsg8XTjqxReeU',
                                batch_position: 0,
                                kind: 'delegation',
                                timestamp: '2019-10-24T19:30:42Z',
                                level: 664283,
                                block: 'BM2WbVVx8AG1rdMcVNtiX9wFtrTFSk2BKrMhbHh1Z56gTwnzQs7',
                                id: 15225640,
                                source: 'tz1YPCK2iRxWZYHxeUtKBKRosdQ1SAHKeACe',
                                gas: 2500,
                                counter: '3be8200000000000',
                                gas_limit: '1027000000000000',
                                storage_limit: '',
                                public_key: null,
                                amount: null,
                                parameters: null,
                                entrypoint: null,
                                contract_address: null,
                                destination: null,
                            },
                        },
                        {
                            cursor: 'ooADLykQuRbk3yUc5f465SRJhGyDtJDFit48WpCp9tipnDK399X:0',
                            node: {
                                hash: 'ooADLykQuRbk3yUc5f465SRJhGyDtJDFit48WpCp9tipnDK399X',
                                batch_position: 0,
                                kind: 'transaction',
                                timestamp: '2019-10-24T20:38:46Z',
                                level: 664348,
                                block: 'BM8uvwRPXp7QtwUwcr2FtSsooUJcRbnN7hvDhUochXS8dpcXfQ8',
                                id: 15227316,
                                source: 'tz1KtGwriE7VuLwT3LwuvU9Nv4wAxP7XZ57d',
                                gas: 17920,
                                counter: 'e2fe010000000000',
                                gas_limit: 'fa58020000000000',
                                storage_limit: '2c01000000000000',
                                public_key: null,
                                amount: '10009000000',
                                parameters: '{ "prim": "Unit" }',
                                entrypoint: null,
                                contract_address: null,
                                destination: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.operations first and date_range gte and order_by and before', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS") {
                address
                first_seen
                activated
                public_key
                operations (
                  first: 3
                  order_by: {field: ID, direction: ASC}
                  before: "opBg2pLmofX5zTSWc3vHj9w3tXUE7wdbtkvehvRuP3DXFFsonHw:0"
                  date_range: {
                    gte: "2019-10-27T12:46:11Z"
                  }
                ) {
                    page_info {
                        start_cursor
                        end_cursor
                        has_next_page
                        has_previous_page
                    }
                    edges {
                        cursor
                        node {
                            hash
                            batch_position
                            kind
                            timestamp
                            level
                            block
                            id
                            source
                            gas
                            counter
                            gas_limit
                            storage_limit
                            public_key
                            amount
                            parameters
                            entrypoint
                            contract_address
                            destination
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                first_seen: '2018-09-13T14:51:20Z',
                activated: '2018-09-13T14:49:20.000Z',
                public_key: null,
                operations: {
                    page_info: {
                        start_cursor: 'ooTga7Hce67qSQ7J73bGcwReCpbwxMN7CPbzC27Wtcy6mKnEGez:0',
                        end_cursor: 'ooTga7Hce67qSQ7J73bGcwReCpbwxMN7CPbzC27Wtcy6mKnEGez:0',
                        has_next_page: true,
                        has_previous_page: false,
                    },
                    edges: [
                        {
                            cursor: 'ooTga7Hce67qSQ7J73bGcwReCpbwxMN7CPbzC27Wtcy6mKnEGez:0',
                            node: {
                                hash: 'ooTga7Hce67qSQ7J73bGcwReCpbwxMN7CPbzC27Wtcy6mKnEGez',
                                batch_position: 0,
                                kind: 'seed_nonce_revelation',
                                timestamp: '2019-12-18T13:44:30Z',
                                level: 741378,
                                block: 'BLHNypF2AZdxRs66kwrjVVsr3svPC72EWYvifBNt3wwNRmCPVcw',
                                id: 17185980,
                                source: null,
                                gas: null,
                                counter: null,
                                gas_limit: null,
                                storage_limit: null,
                                public_key: null,
                                amount: null,
                                parameters: null,
                                entrypoint: null,
                                contract_address: null,
                                destination: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.operations first and date_range gte and order_by and after', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS") {
                address
                first_seen
                activated
                public_key
                operations (
                  first: 3
                  order_by: {field: ID, direction: ASC}
                  after: "oo8sVjaRzoFHaoxgtVdAbqbPujVJzt1ToknGwapTjHJM5jRhyMx:0"
                  date_range: {
                    gte: "2019-10-27T12:46:11Z"
                  }
                ) {
                    page_info {
                        start_cursor
                        end_cursor
                        has_next_page
                        has_previous_page
                    }
                    edges {
                        cursor
                        node {
                            hash
                            batch_position
                            kind
                            timestamp
                            level
                            block
                            id
                            source
                            gas
                            counter
                            gas_limit
                            storage_limit
                            public_key
                            amount
                            parameters
                            entrypoint
                            contract_address
                            destination
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                first_seen: '2018-09-13T14:51:20Z',
                activated: '2018-09-13T14:49:20.000Z',
                public_key: null,
                operations: {
                    page_info: {
                        start_cursor: 'ooZ68VKGkWdnLEY3nRGkaZZAMvMHvdf5nrnssmmhc7eXc7LEQGD:0',
                        end_cursor: 'oowUdQhpUKrsM9UgPBxPXQHq5ri9BVZGsEWXvFk2w48GkZZXxno:0',
                        has_next_page: true,
                        has_previous_page: true,
                    },
                    edges: [
                        {
                            cursor: 'ooZ68VKGkWdnLEY3nRGkaZZAMvMHvdf5nrnssmmhc7eXc7LEQGD:0',
                            node: {
                                hash: 'ooZ68VKGkWdnLEY3nRGkaZZAMvMHvdf5nrnssmmhc7eXc7LEQGD',
                                batch_position: 0,
                                kind: 'transaction',
                                timestamp: '2020-07-15T20:54:27Z',
                                level: 1041409,
                                block: 'BLAuF4FoKqgzDt1rY4NB1VPWmNtNzsN9tJowi9f64KLNofU3McH',
                                id: 25134138,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                gas: 1286,
                                counter: 'ddd0010000000000',
                                gas_limit: '4328000000000000',
                                storage_limit: '1501000000000000',
                                public_key: null,
                                amount: '286268648',
                                parameters: '{ "prim": "Unit" }',
                                entrypoint: null,
                                contract_address: null,
                                destination: 'tz1gEQe76hTA9L5MpTRtNnDhhqQ1YbzVPxnY',
                            },
                        },
                        {
                            cursor: 'ooa7UPV5iL5TeV469j9BqeXxFr7tTviyxRRizVFWoc3rU1eAH8N:0',
                            node: {
                                hash: 'ooa7UPV5iL5TeV469j9BqeXxFr7tTviyxRRizVFWoc3rU1eAH8N',
                                batch_position: 0,
                                kind: 'transaction',
                                timestamp: '2020-07-15T20:55:27Z',
                                level: 1041410,
                                block: 'BMUm98MH7pD2eC9s5WHiPvsSjMFGPyQiJB7zHLQY9uX3ozGpQFE',
                                id: 25134171,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                gas: 1285,
                                counter: 'ded0010000000000',
                                gas_limit: '4328000000000000',
                                storage_limit: '',
                                public_key: null,
                                amount: '325873000',
                                parameters: '{ "prim": "Unit" }',
                                entrypoint: null,
                                contract_address: null,
                                destination: 'tz1gEQe76hTA9L5MpTRtNnDhhqQ1YbzVPxnY',
                            },
                        },
                        {
                            cursor: 'oowUdQhpUKrsM9UgPBxPXQHq5ri9BVZGsEWXvFk2w48GkZZXxno:0',
                            node: {
                                hash: 'oowUdQhpUKrsM9UgPBxPXQHq5ri9BVZGsEWXvFk2w48GkZZXxno',
                                batch_position: 0,
                                kind: 'transaction',
                                timestamp: '2020-07-15T20:56:27Z',
                                level: 1041411,
                                block: 'BL25QMmdBxgkic7PB2z53B38rPTMYy3kAfPevQXDDsBrzbbRy6j',
                                id: 25134203,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                gas: 1284,
                                counter: 'dfd0010000000000',
                                gas_limit: '4328000000000000',
                                storage_limit: '',
                                public_key: null,
                                amount: '79651872',
                                parameters: '{ "prim": "Unit" }',
                                entrypoint: null,
                                contract_address: null,
                                destination: 'tz1bJAQfucuxvLwCHyPXvG24PjtLT1V3ewbt',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.operations first and date_range lte and order_by and before', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS") {
                address
                first_seen
                activated
                public_key
                operations (
                  first: 3
                  order_by: {field: ID, direction: ASC}
                  before: "oo8sVjaRzoFHaoxgtVdAbqbPujVJzt1ToknGwapTjHJM5jRhyMx:0"
                  date_range: {
                    lte: "2019-10-27T12:46:11Z"
                  }
                ) {
                    page_info {
                        start_cursor
                        end_cursor
                        has_next_page
                        has_previous_page
                    }
                    edges {
                        cursor
                        node {
                            hash
                            batch_position
                            kind
                            timestamp
                            level
                            block
                            id
                            source
                            gas
                            counter
                            gas_limit
                            storage_limit
                            public_key
                            amount
                            parameters
                            entrypoint
                            contract_address
                            destination
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                first_seen: '2018-09-13T14:51:20Z',
                activated: '2018-09-13T14:49:20.000Z',
                public_key: null,
                operations: {
                    page_info: {
                        start_cursor: 'ontHLDmy96uXcNBUnarBPoWYSohPN54fNiWbdvHpSEHvA2mdeu5:0',
                        end_cursor: 'oop8aPtDYg7K3xqqMadPKoXzxTLNpBukasrKtBQs9bsjXQ1UWVt:1',
                        has_next_page: true,
                        has_previous_page: false,
                    },
                    edges: [
                        {
                            cursor: 'ontHLDmy96uXcNBUnarBPoWYSohPN54fNiWbdvHpSEHvA2mdeu5:0',
                            node: {
                                hash: 'ontHLDmy96uXcNBUnarBPoWYSohPN54fNiWbdvHpSEHvA2mdeu5',
                                batch_position: 0,
                                kind: 'activate_account',
                                timestamp: '2018-09-13T14:49:20Z',
                                level: 101955,
                                block: 'BLrAAf7V4FDypigoEexGwXdzoonabRSjWdFaEAubV7p5JdGaaU5',
                                id: 1425677,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                gas: null,
                                counter: null,
                                gas_limit: null,
                                storage_limit: null,
                                public_key: null,
                                amount: null,
                                parameters: null,
                                entrypoint: null,
                                contract_address: null,
                                destination: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                            },
                        },
                        {
                            cursor: 'opQqKKXhzznFRxMPdiLyP4V1KWfuDrrNSkqF8jQ6v4JENR3K6jP:0',
                            node: {
                                hash: 'opQqKKXhzznFRxMPdiLyP4V1KWfuDrrNSkqF8jQ6v4JENR3K6jP',
                                batch_position: 0,
                                kind: 'transaction',
                                timestamp: '2018-09-13T14:51:20Z',
                                level: 101957,
                                block: 'BMQzE1UWod12P5XkE15fS4CNRvKLqmWknwCEDuTP1FKEhzrVjtU',
                                id: 1425714,
                                source: 'tz1LBEKXaxQbd5Gtzbc1ATCwc3pppu81aWGc',
                                gas: 10,
                                counter: '62e7000000000000',
                                gas_limit: 'c800000000000000',
                                storage_limit: '',
                                public_key: null,
                                amount: '14',
                                parameters: null,
                                entrypoint: null,
                                contract_address: null,
                                destination: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                            },
                        },
                        {
                            cursor: 'oop8aPtDYg7K3xqqMadPKoXzxTLNpBukasrKtBQs9bsjXQ1UWVt:1',
                            node: {
                                hash: 'oop8aPtDYg7K3xqqMadPKoXzxTLNpBukasrKtBQs9bsjXQ1UWVt',
                                batch_position: 1,
                                kind: 'transaction',
                                timestamp: '2018-09-13T16:12:50Z',
                                level: 102031,
                                block: 'BLowVvoAyjeoMY4BRja6rEPdX37UjfdeB4JFtgQ4ZvCiRg9MNdA',
                                id: 1427150,
                                source: 'tz1PECYmqmuba13rnNxyoBkvH2ChUt8BuJuG',
                                gas: 0,
                                counter: '9d4d000000000000',
                                gas_limit: '',
                                storage_limit: '',
                                public_key: null,
                                amount: '1000000',
                                parameters: null,
                                entrypoint: null,
                                contract_address: null,
                                destination: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.operations first and date_range lte and order_by and after', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS") {
                address
                first_seen
                activated
                public_key
                operations (
                  first: 3
                  order_by: {field: ID, direction: ASC}
                  after: "opQqKKXhzznFRxMPdiLyP4V1KWfuDrrNSkqF8jQ6v4JENR3K6jP:0"
                  date_range: {
                    lte: "2019-10-27T12:46:11Z"
                  }
                ) {
                    page_info {
                        start_cursor
                        end_cursor
                        has_next_page
                        has_previous_page
                    }
                    edges {
                        cursor
                        node {
                            hash
                            batch_position
                            kind
                            timestamp
                            level
                            block
                            id
                            source
                            gas
                            counter
                            gas_limit
                            storage_limit
                            public_key
                            amount
                            parameters
                            entrypoint
                            contract_address
                            destination
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                first_seen: '2018-09-13T14:51:20Z',
                activated: '2018-09-13T14:49:20.000Z',
                public_key: null,
                operations: {
                    page_info: {
                        start_cursor: 'oop8aPtDYg7K3xqqMadPKoXzxTLNpBukasrKtBQs9bsjXQ1UWVt:1',
                        end_cursor: 'ooeHBn4BDojRRLsNc4eWta1JrHoxGEEU8p2EjufpUcRNMcKJyNb:0',
                        has_next_page: true,
                        has_previous_page: true,
                    },
                    edges: [
                        {
                            cursor: 'oop8aPtDYg7K3xqqMadPKoXzxTLNpBukasrKtBQs9bsjXQ1UWVt:1',
                            node: {
                                hash: 'oop8aPtDYg7K3xqqMadPKoXzxTLNpBukasrKtBQs9bsjXQ1UWVt',
                                batch_position: 1,
                                kind: 'transaction',
                                timestamp: '2018-09-13T16:12:50Z',
                                level: 102031,
                                block: 'BLowVvoAyjeoMY4BRja6rEPdX37UjfdeB4JFtgQ4ZvCiRg9MNdA',
                                id: 1427150,
                                source: 'tz1PECYmqmuba13rnNxyoBkvH2ChUt8BuJuG',
                                gas: 0,
                                counter: '9d4d000000000000',
                                gas_limit: '',
                                storage_limit: '',
                                public_key: null,
                                amount: '1000000',
                                parameters: null,
                                entrypoint: null,
                                contract_address: null,
                                destination: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                            },
                        },
                        {
                            cursor: 'ooe9ypjsaQZPhowQpj69MZicQ3ZUwFGxQJyMVxdGpMRWF9SBZWr:0',
                            node: {
                                hash: 'ooe9ypjsaQZPhowQpj69MZicQ3ZUwFGxQJyMVxdGpMRWF9SBZWr',
                                batch_position: 0,
                                kind: 'transaction',
                                timestamp: '2018-09-13T16:17:05Z',
                                level: 102034,
                                block: 'BLFRNMJkWm9aRBgTwEdhZGcgKZ3idCWYWua8bRCfhx1BBU5xMmJ',
                                id: 1427201,
                                source: 'tz1PECYmqmuba13rnNxyoBkvH2ChUt8BuJuG',
                                gas: 0,
                                counter: '9f4d000000000000',
                                gas_limit: 'c800000000000000',
                                storage_limit: '',
                                public_key: null,
                                amount: '4199800000',
                                parameters: null,
                                entrypoint: null,
                                contract_address: null,
                                destination: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                            },
                        },
                        {
                            cursor: 'ooeHBn4BDojRRLsNc4eWta1JrHoxGEEU8p2EjufpUcRNMcKJyNb:0',
                            node: {
                                hash: 'ooeHBn4BDojRRLsNc4eWta1JrHoxGEEU8p2EjufpUcRNMcKJyNb',
                                batch_position: 0,
                                kind: 'transaction',
                                timestamp: '2018-09-13T16:18:05Z',
                                level: 102035,
                                block: 'BLJLwRhrzz4srdUXw3FHgPazUnnMc3B1R15wpSWsBJPAYRJQCng',
                                id: 1427217,
                                source: 'tz1PECYmqmuba13rnNxyoBkvH2ChUt8BuJuG',
                                gas: 0,
                                counter: 'a04d000000000000',
                                gas_limit: 'c800000000000000',
                                storage_limit: '',
                                public_key: null,
                                amount: '20000',
                                parameters: null,
                                entrypoint: null,
                                contract_address: null,
                                destination: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                            },
                        },
                    ],
                },
            },
        }));
    });
    test('account.operations first and date_range gte/lte and order_by and before', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS") {
                address
                first_seen
                activated
                public_key
                operations (
                  first: 3
                  order_by: {field: ID, direction: ASC}
                  before: "ooADLykQuRbk3yUc5f465SRJhGyDtJDFit48WpCp9tipnDK399X:0"
                  date_range: {
                    gte: "2019-10-22T18:56:24Z"
                    lte: "2019-10-27T12:46:11Z"
                  }
                ) {
                    page_info {
                        start_cursor
                        end_cursor
                        has_next_page
                        has_previous_page
                    }
                    edges {
                        cursor
                        node {
                            hash
                            batch_position
                            kind
                            timestamp
                            level
                            block
                            id
                            source
                            gas
                            counter
                            gas_limit
                            storage_limit
                            public_key
                            amount
                            parameters
                            entrypoint
                            contract_address
                            destination
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                first_seen: '2018-09-13T14:51:20Z',
                activated: '2018-09-13T14:49:20.000Z',
                public_key: null,
                operations: {
                    page_info: {
                        start_cursor: 'ooUuE97rDNfvxm4tVy8CK8yHz6P3LdvFj5Sn41vw26hgRXT5YRr:1',
                        end_cursor: 'onf5R7jCRJMfATspY8BB7ye4izQUVPzsBohqqNgsg8XTjqxReeU:0',
                        has_next_page: true,
                        has_previous_page: false,
                    },
                    edges: [
                        {
                            cursor: 'ooUuE97rDNfvxm4tVy8CK8yHz6P3LdvFj5Sn41vw26hgRXT5YRr:1',
                            node: {
                                hash: 'ooUuE97rDNfvxm4tVy8CK8yHz6P3LdvFj5Sn41vw26hgRXT5YRr',
                                batch_position: 1,
                                kind: 'delegation',
                                timestamp: '2019-10-24T19:05:42Z',
                                level: 664258,
                                block: 'BKm7KPd6erxKgeHE4TTsqe78b5HJx33QTWa6rfNpSkt8PuzHkmf',
                                id: 15224994,
                                source: 'tz1RAWNEEFr2DSEDQVPnHXhH24GLMDF5XNjJ',
                                gas: 2500,
                                counter: '1f9e210000000000',
                                gas_limit: '1027000000000000',
                                storage_limit: '',
                                public_key: null,
                                amount: null,
                                parameters: null,
                                entrypoint: null,
                                contract_address: null,
                                destination: null,
                            },
                        },
                        {
                            cursor: 'onf5R7jCRJMfATspY8BB7ye4izQUVPzsBohqqNgsg8XTjqxReeU:0',
                            node: {
                                hash: 'onf5R7jCRJMfATspY8BB7ye4izQUVPzsBohqqNgsg8XTjqxReeU',
                                batch_position: 0,
                                kind: 'delegation',
                                timestamp: '2019-10-24T19:30:42Z',
                                level: 664283,
                                block: 'BM2WbVVx8AG1rdMcVNtiX9wFtrTFSk2BKrMhbHh1Z56gTwnzQs7',
                                id: 15225640,
                                source: 'tz1YPCK2iRxWZYHxeUtKBKRosdQ1SAHKeACe',
                                gas: 2500,
                                counter: '3be8200000000000',
                                gas_limit: '1027000000000000',
                                storage_limit: '',
                                public_key: null,
                                amount: null,
                                parameters: null,
                                entrypoint: null,
                                contract_address: null,
                                destination: null,
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.operations first and date_range gte/lte and order_by and after', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS") {
                address
                first_seen
                activated
                public_key
                operations (
                  first: 3
                  order_by: {field: ID, direction: ASC}
                  after: "ooUuE97rDNfvxm4tVy8CK8yHz6P3LdvFj5Sn41vw26hgRXT5YRr:1"
                  date_range: {
                    gte: "2019-10-22T18:56:24Z"
                    lte: "2019-10-27T12:46:11Z"
                  }
                ) {
                    page_info {
                        start_cursor
                        end_cursor
                        has_next_page
                        has_previous_page
                    }
                    edges {
                        cursor
                        node {
                            hash
                            batch_position
                            kind
                            timestamp
                            level
                            block
                            id
                            source
                            gas
                            counter
                            gas_limit
                            storage_limit
                            public_key
                            amount
                            parameters
                            entrypoint
                            contract_address
                            destination
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                first_seen: '2018-09-13T14:51:20Z',
                activated: '2018-09-13T14:49:20.000Z',
                public_key: null,
                operations: {
                    page_info: {
                        start_cursor: 'onf5R7jCRJMfATspY8BB7ye4izQUVPzsBohqqNgsg8XTjqxReeU:0',
                        end_cursor: 'ooADLykQuRbk3yUc5f465SRJhGyDtJDFit48WpCp9tipnDK399X:0',
                        has_next_page: false,
                        has_previous_page: true,
                    },
                    edges: [
                        {
                            cursor: 'onf5R7jCRJMfATspY8BB7ye4izQUVPzsBohqqNgsg8XTjqxReeU:0',
                            node: {
                                hash: 'onf5R7jCRJMfATspY8BB7ye4izQUVPzsBohqqNgsg8XTjqxReeU',
                                batch_position: 0,
                                kind: 'delegation',
                                timestamp: '2019-10-24T19:30:42Z',
                                level: 664283,
                                block: 'BM2WbVVx8AG1rdMcVNtiX9wFtrTFSk2BKrMhbHh1Z56gTwnzQs7',
                                id: 15225640,
                                source: 'tz1YPCK2iRxWZYHxeUtKBKRosdQ1SAHKeACe',
                                gas: 2500,
                                counter: '3be8200000000000',
                                gas_limit: '1027000000000000',
                                storage_limit: '',
                                public_key: null,
                                amount: null,
                                parameters: null,
                                entrypoint: null,
                                contract_address: null,
                                destination: null,
                            },
                        },
                        {
                            cursor: 'ooADLykQuRbk3yUc5f465SRJhGyDtJDFit48WpCp9tipnDK399X:0',
                            node: {
                                hash: 'ooADLykQuRbk3yUc5f465SRJhGyDtJDFit48WpCp9tipnDK399X',
                                batch_position: 0,
                                kind: 'transaction',
                                timestamp: '2019-10-24T20:38:46Z',
                                level: 664348,
                                block: 'BM8uvwRPXp7QtwUwcr2FtSsooUJcRbnN7hvDhUochXS8dpcXfQ8',
                                id: 15227316,
                                source: 'tz1KtGwriE7VuLwT3LwuvU9Nv4wAxP7XZ57d',
                                gas: 17920,
                                counter: 'e2fe010000000000',
                                gas_limit: 'fa58020000000000',
                                storage_limit: '2c01000000000000',
                                public_key: null,
                                amount: '10009000000',
                                parameters: '{ "prim": "Unit" }',
                                entrypoint: null,
                                contract_address: null,
                                destination: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.operations last', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS") {
                address
                first_seen
                activated
                public_key
                operations (last: 3) {
                    page_info {
                        start_cursor
                        end_cursor
                        has_next_page
                        has_previous_page
                    }
                    edges {
                        cursor
                        node {
                            hash
                            batch_position
                            kind
                            timestamp
                            level
                            block
                            id
                            source
                            gas
                            counter
                            gas_limit
                            storage_limit
                            public_key
                            amount
                            parameters
                            entrypoint
                            contract_address
                            destination
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                first_seen: '2018-09-13T14:51:20Z',
                activated: '2018-09-13T14:49:20.000Z',
                public_key: null,
                operations: {
                    page_info: {
                        start_cursor: 'oop8aPtDYg7K3xqqMadPKoXzxTLNpBukasrKtBQs9bsjXQ1UWVt:1',
                        end_cursor: 'ontHLDmy96uXcNBUnarBPoWYSohPN54fNiWbdvHpSEHvA2mdeu5:0',
                        has_next_page: false,
                        has_previous_page: true,
                    },
                    edges: [
                        {
                            cursor: 'oop8aPtDYg7K3xqqMadPKoXzxTLNpBukasrKtBQs9bsjXQ1UWVt:1',
                            node: {
                                hash: 'oop8aPtDYg7K3xqqMadPKoXzxTLNpBukasrKtBQs9bsjXQ1UWVt',
                                batch_position: 1,
                                kind: 'transaction',
                                timestamp: '2018-09-13T16:12:50Z',
                                level: 102031,
                                block: 'BLowVvoAyjeoMY4BRja6rEPdX37UjfdeB4JFtgQ4ZvCiRg9MNdA',
                                id: 1427150,
                                source: 'tz1PECYmqmuba13rnNxyoBkvH2ChUt8BuJuG',
                                gas: 0,
                                counter: '9d4d000000000000',
                                gas_limit: '',
                                storage_limit: '',
                                public_key: null,
                                amount: '1000000',
                                parameters: null,
                                entrypoint: null,
                                contract_address: null,
                                destination: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                            },
                        },
                        {
                            cursor: 'opQqKKXhzznFRxMPdiLyP4V1KWfuDrrNSkqF8jQ6v4JENR3K6jP:0',
                            node: {
                                hash: 'opQqKKXhzznFRxMPdiLyP4V1KWfuDrrNSkqF8jQ6v4JENR3K6jP',
                                batch_position: 0,
                                kind: 'transaction',
                                timestamp: '2018-09-13T14:51:20Z',
                                level: 101957,
                                block: 'BMQzE1UWod12P5XkE15fS4CNRvKLqmWknwCEDuTP1FKEhzrVjtU',
                                id: 1425714,
                                source: 'tz1LBEKXaxQbd5Gtzbc1ATCwc3pppu81aWGc',
                                gas: 10,
                                counter: '62e7000000000000',
                                gas_limit: 'c800000000000000',
                                storage_limit: '',
                                public_key: null,
                                amount: '14',
                                parameters: null,
                                entrypoint: null,
                                contract_address: null,
                                destination: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                            },
                        },
                        {
                            cursor: 'ontHLDmy96uXcNBUnarBPoWYSohPN54fNiWbdvHpSEHvA2mdeu5:0',
                            node: {
                                hash: 'ontHLDmy96uXcNBUnarBPoWYSohPN54fNiWbdvHpSEHvA2mdeu5',
                                batch_position: 0,
                                kind: 'activate_account',
                                timestamp: '2018-09-13T14:49:20Z',
                                level: 101955,
                                block: 'BLrAAf7V4FDypigoEexGwXdzoonabRSjWdFaEAubV7p5JdGaaU5',
                                id: 1425677,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                gas: null,
                                counter: null,
                                gas_limit: null,
                                storage_limit: null,
                                public_key: null,
                                amount: null,
                                parameters: null,
                                entrypoint: null,
                                contract_address: null,
                                destination: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.operations last and before', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS") {
                address
                first_seen
                activated
                public_key
                operations (
                last: 3
                before: "ooUuE97rDNfvxm4tVy8CK8yHz6P3LdvFj5Sn41vw26hgRXT5YRr:1"
                ) {
                    page_info {
                        start_cursor
                        end_cursor
                        has_next_page
                        has_previous_page
                    }
                    edges {
                        cursor
                        node {
                            hash
                            batch_position
                            kind
                            timestamp
                            level
                            block
                            id
                            source
                            gas
                            counter
                            gas_limit
                            storage_limit
                            public_key
                            amount
                            parameters
                            entrypoint
                            contract_address
                            destination
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                first_seen: '2018-09-13T14:51:20Z',
                activated: '2018-09-13T14:49:20.000Z',
                public_key: null,
                operations: {
                    page_info: {
                        start_cursor: 'ooTga7Hce67qSQ7J73bGcwReCpbwxMN7CPbzC27Wtcy6mKnEGez:0',
                        end_cursor: 'onf5R7jCRJMfATspY8BB7ye4izQUVPzsBohqqNgsg8XTjqxReeU:0',
                        has_next_page: true,
                        has_previous_page: true,
                    },
                    edges: [
                        {
                            cursor: 'ooTga7Hce67qSQ7J73bGcwReCpbwxMN7CPbzC27Wtcy6mKnEGez:0',
                            node: {
                                hash: 'ooTga7Hce67qSQ7J73bGcwReCpbwxMN7CPbzC27Wtcy6mKnEGez',
                                batch_position: 0,
                                kind: 'seed_nonce_revelation',
                                timestamp: '2019-12-18T13:44:30Z',
                                level: 741378,
                                block: 'BLHNypF2AZdxRs66kwrjVVsr3svPC72EWYvifBNt3wwNRmCPVcw',
                                id: 17185980,
                                source: null,
                                gas: null,
                                counter: null,
                                gas_limit: null,
                                storage_limit: null,
                                public_key: null,
                                amount: null,
                                parameters: null,
                                entrypoint: null,
                                contract_address: null,
                                destination: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                            },
                        },
                        {
                            cursor: 'ooADLykQuRbk3yUc5f465SRJhGyDtJDFit48WpCp9tipnDK399X:0',
                            node: {
                                hash: 'ooADLykQuRbk3yUc5f465SRJhGyDtJDFit48WpCp9tipnDK399X',
                                batch_position: 0,
                                kind: 'transaction',
                                timestamp: '2019-10-24T20:38:46Z',
                                level: 664348,
                                block: 'BM8uvwRPXp7QtwUwcr2FtSsooUJcRbnN7hvDhUochXS8dpcXfQ8',
                                id: 15227316,
                                source: 'tz1KtGwriE7VuLwT3LwuvU9Nv4wAxP7XZ57d',
                                gas: 17920,
                                counter: 'e2fe010000000000',
                                gas_limit: 'fa58020000000000',
                                storage_limit: '2c01000000000000',
                                public_key: null,
                                amount: '10009000000',
                                parameters: '{ "prim": "Unit" }',
                                entrypoint: null,
                                contract_address: null,
                                destination: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                            },
                        },
                        {
                            cursor: 'onf5R7jCRJMfATspY8BB7ye4izQUVPzsBohqqNgsg8XTjqxReeU:0',
                            node: {
                                hash: 'onf5R7jCRJMfATspY8BB7ye4izQUVPzsBohqqNgsg8XTjqxReeU',
                                batch_position: 0,
                                kind: 'delegation',
                                timestamp: '2019-10-24T19:30:42Z',
                                level: 664283,
                                block: 'BM2WbVVx8AG1rdMcVNtiX9wFtrTFSk2BKrMhbHh1Z56gTwnzQs7',
                                id: 15225640,
                                source: 'tz1YPCK2iRxWZYHxeUtKBKRosdQ1SAHKeACe',
                                gas: 2500,
                                counter: '3be8200000000000',
                                gas_limit: '1027000000000000',
                                storage_limit: '',
                                public_key: null,
                                amount: null,
                                parameters: null,
                                entrypoint: null,
                                contract_address: null,
                                destination: null,
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.operations last and after', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS") {
                address
                first_seen
                activated
                public_key
                operations (
                last: 3
                after: "oobMbc7MHSz5ttV3AcjiXqaxZSxs9ShGjcHmopoo85Jays2rniJ:0"
                ) {
                    page_info {
                        start_cursor
                        end_cursor
                        has_next_page
                        has_previous_page
                    }
                    edges {
                        cursor
                        node {
                            hash
                            batch_position
                            kind
                            timestamp
                            level
                            block
                            id
                            source
                            gas
                            counter
                            gas_limit
                            storage_limit
                            public_key
                            amount
                            parameters
                            entrypoint
                            contract_address
                            destination
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                first_seen: '2018-09-13T14:51:20Z',
                activated: '2018-09-13T14:49:20.000Z',
                public_key: null,
                operations: {
                    page_info: {
                        start_cursor: 'oop8aPtDYg7K3xqqMadPKoXzxTLNpBukasrKtBQs9bsjXQ1UWVt:1',
                        end_cursor: 'ontHLDmy96uXcNBUnarBPoWYSohPN54fNiWbdvHpSEHvA2mdeu5:0',
                        has_next_page: false,
                        has_previous_page: true,
                    },
                    edges: [
                        {
                            cursor: 'oop8aPtDYg7K3xqqMadPKoXzxTLNpBukasrKtBQs9bsjXQ1UWVt:1',
                            node: {
                                hash: 'oop8aPtDYg7K3xqqMadPKoXzxTLNpBukasrKtBQs9bsjXQ1UWVt',
                                batch_position: 1,
                                kind: 'transaction',
                                timestamp: '2018-09-13T16:12:50Z',
                                level: 102031,
                                block: 'BLowVvoAyjeoMY4BRja6rEPdX37UjfdeB4JFtgQ4ZvCiRg9MNdA',
                                id: 1427150,
                                source: 'tz1PECYmqmuba13rnNxyoBkvH2ChUt8BuJuG',
                                gas: 0,
                                counter: '9d4d000000000000',
                                gas_limit: '',
                                storage_limit: '',
                                public_key: null,
                                amount: '1000000',
                                parameters: null,
                                entrypoint: null,
                                contract_address: null,
                                destination: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                            },
                        },
                        {
                            cursor: 'opQqKKXhzznFRxMPdiLyP4V1KWfuDrrNSkqF8jQ6v4JENR3K6jP:0',
                            node: {
                                hash: 'opQqKKXhzznFRxMPdiLyP4V1KWfuDrrNSkqF8jQ6v4JENR3K6jP',
                                batch_position: 0,
                                kind: 'transaction',
                                timestamp: '2018-09-13T14:51:20Z',
                                level: 101957,
                                block: 'BMQzE1UWod12P5XkE15fS4CNRvKLqmWknwCEDuTP1FKEhzrVjtU',
                                id: 1425714,
                                source: 'tz1LBEKXaxQbd5Gtzbc1ATCwc3pppu81aWGc',
                                gas: 10,
                                counter: '62e7000000000000',
                                gas_limit: 'c800000000000000',
                                storage_limit: '',
                                public_key: null,
                                amount: '14',
                                parameters: null,
                                entrypoint: null,
                                contract_address: null,
                                destination: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                            },
                        },
                        {
                            cursor: 'ontHLDmy96uXcNBUnarBPoWYSohPN54fNiWbdvHpSEHvA2mdeu5:0',
                            node: {
                                hash: 'ontHLDmy96uXcNBUnarBPoWYSohPN54fNiWbdvHpSEHvA2mdeu5',
                                batch_position: 0,
                                kind: 'activate_account',
                                timestamp: '2018-09-13T14:49:20Z',
                                level: 101955,
                                block: 'BLrAAf7V4FDypigoEexGwXdzoonabRSjWdFaEAubV7p5JdGaaU5',
                                id: 1425677,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                gas: null,
                                counter: null,
                                gas_limit: null,
                                storage_limit: null,
                                public_key: null,
                                amount: null,
                                parameters: null,
                                entrypoint: null,
                                contract_address: null,
                                destination: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.operations last and order_by', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS") {
                address
                first_seen
                activated
                public_key
                operations (
                last: 3
                order_by: {field: ID, direction: ASC}
                ) {
                    page_info {
                        start_cursor
                        end_cursor
                        has_next_page
                        has_previous_page
                    }
                    edges {
                        cursor
                        node {
                            hash
                            batch_position
                            kind
                            timestamp
                            level
                            block
                            id
                            source
                            gas
                            counter
                            gas_limit
                            storage_limit
                            public_key
                            amount
                            parameters
                            entrypoint
                            contract_address
                            destination
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                first_seen: '2018-09-13T14:51:20Z',
                activated: '2018-09-13T14:49:20.000Z',
                public_key: null,
                operations: {
                    page_info: {
                        start_cursor: 'ootgGUV6dDRfBBF9wutPQZWzeetqiAUkwrkwSL1Qy6NrTNUs6Jw:0',
                        end_cursor: 'opNxsLZeNMttsyh4PxLk35dmRsJyLXQwJjpVp89ip7pU2j5yzyR:0',
                        has_next_page: false,
                        has_previous_page: true,
                    },
                    edges: [
                        {
                            cursor: 'ootgGUV6dDRfBBF9wutPQZWzeetqiAUkwrkwSL1Qy6NrTNUs6Jw:0',
                            node: {
                                hash: 'ootgGUV6dDRfBBF9wutPQZWzeetqiAUkwrkwSL1Qy6NrTNUs6Jw',
                                batch_position: 0,
                                kind: 'transaction',
                                timestamp: '2020-07-15T20:58:27Z',
                                level: 1041413,
                                block: 'BMf1K4SE3WqRJ6eD345kU74pjLkfB7ABK5u5KwZrxckNHFvUr4v',
                                id: 25134264,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                gas: 1285,
                                counter: 'e1d0010000000000',
                                gas_limit: '4328000000000000',
                                storage_limit: '1501000000000000',
                                public_key: null,
                                amount: '14193181',
                                parameters: '{ "prim": "Unit" }',
                                entrypoint: null,
                                contract_address: null,
                                destination: 'tz1cb9DMkq9ZdrDVxi3HwyLEMtbDseeZPZEa',
                            },
                        },
                        {
                            cursor: 'opEHTGa4n4efFQfpaWFNqoY4dbzXrBEo7Xo8v74AX5pEREqK28S:0',
                            node: {
                                hash: 'opEHTGa4n4efFQfpaWFNqoY4dbzXrBEo7Xo8v74AX5pEREqK28S',
                                batch_position: 0,
                                kind: 'transaction',
                                timestamp: '2020-07-17T19:46:24Z',
                                level: 1044207,
                                block: 'BMYhcRrNnHTthRueAKoiyBxyuzJWEMqMgkxzqiVFXHtS84ALKua',
                                id: 25210462,
                                source: 'tz1YPCK2iRxWZYHxeUtKBKRosdQ1SAHKeACe',
                                gas: 1420,
                                counter: '3ee8200000000000',
                                gas_limit: '6829000000000000',
                                storage_limit: '2c01000000000000',
                                public_key: null,
                                amount: '192000000',
                                parameters: '{ "prim": "Unit" }',
                                entrypoint: null,
                                contract_address: null,
                                destination: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                            },
                        },
                        {
                            cursor: 'opNxsLZeNMttsyh4PxLk35dmRsJyLXQwJjpVp89ip7pU2j5yzyR:0',
                            node: {
                                hash: 'opNxsLZeNMttsyh4PxLk35dmRsJyLXQwJjpVp89ip7pU2j5yzyR',
                                batch_position: 0,
                                kind: 'transaction',
                                timestamp: '2020-08-15T22:11:48Z',
                                level: 1085872,
                                block: 'BLEwgk9RsqEfhtm6u3PrLXcu7pXQ6dWMysJRVTAhNRqVqhCsF9g',
                                id: 26331765,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                gas: 1285,
                                counter: 'e2d0010000000000',
                                gas_limit: '4328000000000000',
                                storage_limit: '1501000000000000',
                                public_key: null,
                                amount: '50000000',
                                parameters: '{ "prim": "Unit" }',
                                entrypoint: null,
                                contract_address: null,
                                destination: 'tz1UovZ16T6vTuCRQhWKtE7jqeC6QaTzbc3P',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.operations last and order_by and before', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS") {
                address
                first_seen
                activated
                public_key
                operations (
                last: 3
                order_by: {field: ID, direction: ASC}
                before: "oo8sVjaRzoFHaoxgtVdAbqbPujVJzt1ToknGwapTjHJM5jRhyMx:0"
                ) {
                    page_info {
                        start_cursor
                        end_cursor
                        has_next_page
                        has_previous_page
                    }
                    edges {
                        cursor
                        node {
                            hash
                            batch_position
                            kind
                            timestamp
                            level
                            block
                            id
                            source
                            gas
                            counter
                            gas_limit
                            storage_limit
                            public_key
                            amount
                            parameters
                            entrypoint
                            contract_address
                            destination
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                first_seen: '2018-09-13T14:51:20Z',
                activated: '2018-09-13T14:49:20.000Z',
                public_key: null,
                operations: {
                    page_info: {
                        start_cursor: 'onecWS9W7u5NtbgjHr5KCSvjpvUUUfZDBnKN3ZmpU5n2zm6XTm9:0',
                        end_cursor: 'opB8Yp3kyXctnsv1PnSxeHcempXtuvJ8y4ttKhReust8iBXj9t8:0',
                        has_next_page: true,
                        has_previous_page: true,
                    },
                    edges: [
                        {
                            cursor: 'onecWS9W7u5NtbgjHr5KCSvjpvUUUfZDBnKN3ZmpU5n2zm6XTm9:0',
                            node: {
                                hash: 'onecWS9W7u5NtbgjHr5KCSvjpvUUUfZDBnKN3ZmpU5n2zm6XTm9',
                                batch_position: 0,
                                kind: 'transaction',
                                timestamp: '2020-07-15T20:47:27Z',
                                level: 1041402,
                                block: 'BMF8HCV1GXYgeH9NhLcxrFdUsE65tpJdVBoXaEynAX3jrD24yEH',
                                id: 25133920,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                gas: 1283,
                                counter: 'd9d0010000000000',
                                gas_limit: '4328000000000000',
                                storage_limit: '',
                                public_key: null,
                                amount: '560732',
                                parameters: '{ "prim": "Unit" }',
                                entrypoint: null,
                                contract_address: null,
                                destination: 'tz1XBrjDhscw9Qg6o43VZ3V6eErDaQjAP6g2',
                            },
                        },
                        {
                            cursor: 'onmJTCgaEfto2uNzjc5NCZziddopqeEKh2a6dCvJDNMLf6bmqqt:0',
                            node: {
                                hash: 'onmJTCgaEfto2uNzjc5NCZziddopqeEKh2a6dCvJDNMLf6bmqqt',
                                batch_position: 0,
                                kind: 'transaction',
                                timestamp: '2020-07-15T20:48:27Z',
                                level: 1041403,
                                block: 'BLdGSJRJRx1SsfnWonxp4vVMCwnMR8reSDx8CVFY6MPJ5rFjW8h',
                                id: 25133955,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                gas: 1284,
                                counter: 'dad0010000000000',
                                gas_limit: '4328000000000000',
                                storage_limit: '',
                                public_key: null,
                                amount: '11602357',
                                parameters: '{ "prim": "Unit" }',
                                entrypoint: null,
                                contract_address: null,
                                destination: 'tz1ZcKt8A46vuam4hQSHMtBWgFrDhHnYjS9q',
                            },
                        },
                        {
                            cursor: 'opB8Yp3kyXctnsv1PnSxeHcempXtuvJ8y4ttKhReust8iBXj9t8:0',
                            node: {
                                hash: 'opB8Yp3kyXctnsv1PnSxeHcempXtuvJ8y4ttKhReust8iBXj9t8',
                                batch_position: 0,
                                kind: 'transaction',
                                timestamp: '2020-07-15T20:50:27Z',
                                level: 1041405,
                                block: 'BKvd9oBBiWLok31cP9ftX6Nftcu9DHq1GVPhXb5KAnuzB2zS5GJ',
                                id: 25134017,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                gas: 1285,
                                counter: 'dbd0010000000000',
                                gas_limit: '4328000000000000',
                                storage_limit: '1501000000000000',
                                public_key: null,
                                amount: '50217319',
                                parameters: '{ "prim": "Unit" }',
                                entrypoint: null,
                                contract_address: null,
                                destination: 'tz1X3xMBb3XzbpK4cehfQRoDgTVFJ2mBQBwt',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.operations last and order_by and after', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS") {
                address
                first_seen
                activated
                public_key
                operations (
                last: 3
                order_by: {field: ID, direction: ASC}
                after: "oo8sVjaRzoFHaoxgtVdAbqbPujVJzt1ToknGwapTjHJM5jRhyMx:0"
                ) {
                    page_info {
                        start_cursor
                        end_cursor
                        has_next_page
                        has_previous_page
                    }
                    edges {
                        cursor
                        node {
                            hash
                            batch_position
                            kind
                            timestamp
                            level
                            block
                            id
                            source
                            gas
                            counter
                            gas_limit
                            storage_limit
                            public_key
                            amount
                            parameters
                            entrypoint
                            contract_address
                            destination
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                first_seen: '2018-09-13T14:51:20Z',
                activated: '2018-09-13T14:49:20.000Z',
                public_key: null,
                operations: {
                    page_info: {
                        start_cursor: 'ootgGUV6dDRfBBF9wutPQZWzeetqiAUkwrkwSL1Qy6NrTNUs6Jw:0',
                        end_cursor: 'opNxsLZeNMttsyh4PxLk35dmRsJyLXQwJjpVp89ip7pU2j5yzyR:0',
                        has_next_page: false,
                        has_previous_page: true,
                    },
                    edges: [
                        {
                            cursor: 'ootgGUV6dDRfBBF9wutPQZWzeetqiAUkwrkwSL1Qy6NrTNUs6Jw:0',
                            node: {
                                hash: 'ootgGUV6dDRfBBF9wutPQZWzeetqiAUkwrkwSL1Qy6NrTNUs6Jw',
                                batch_position: 0,
                                kind: 'transaction',
                                timestamp: '2020-07-15T20:58:27Z',
                                level: 1041413,
                                block: 'BMf1K4SE3WqRJ6eD345kU74pjLkfB7ABK5u5KwZrxckNHFvUr4v',
                                id: 25134264,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                gas: 1285,
                                counter: 'e1d0010000000000',
                                gas_limit: '4328000000000000',
                                storage_limit: '1501000000000000',
                                public_key: null,
                                amount: '14193181',
                                parameters: '{ "prim": "Unit" }',
                                entrypoint: null,
                                contract_address: null,
                                destination: 'tz1cb9DMkq9ZdrDVxi3HwyLEMtbDseeZPZEa',
                            },
                        },
                        {
                            cursor: 'opEHTGa4n4efFQfpaWFNqoY4dbzXrBEo7Xo8v74AX5pEREqK28S:0',
                            node: {
                                hash: 'opEHTGa4n4efFQfpaWFNqoY4dbzXrBEo7Xo8v74AX5pEREqK28S',
                                batch_position: 0,
                                kind: 'transaction',
                                timestamp: '2020-07-17T19:46:24Z',
                                level: 1044207,
                                block: 'BMYhcRrNnHTthRueAKoiyBxyuzJWEMqMgkxzqiVFXHtS84ALKua',
                                id: 25210462,
                                source: 'tz1YPCK2iRxWZYHxeUtKBKRosdQ1SAHKeACe',
                                gas: 1420,
                                counter: '3ee8200000000000',
                                gas_limit: '6829000000000000',
                                storage_limit: '2c01000000000000',
                                public_key: null,
                                amount: '192000000',
                                parameters: '{ "prim": "Unit" }',
                                entrypoint: null,
                                contract_address: null,
                                destination: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                            },
                        },
                        {
                            cursor: 'opNxsLZeNMttsyh4PxLk35dmRsJyLXQwJjpVp89ip7pU2j5yzyR:0',
                            node: {
                                hash: 'opNxsLZeNMttsyh4PxLk35dmRsJyLXQwJjpVp89ip7pU2j5yzyR',
                                batch_position: 0,
                                kind: 'transaction',
                                timestamp: '2020-08-15T22:11:48Z',
                                level: 1085872,
                                block: 'BLEwgk9RsqEfhtm6u3PrLXcu7pXQ6dWMysJRVTAhNRqVqhCsF9g',
                                id: 26331765,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                gas: 1285,
                                counter: 'e2d0010000000000',
                                gas_limit: '4328000000000000',
                                storage_limit: '1501000000000000',
                                public_key: null,
                                amount: '50000000',
                                parameters: '{ "prim": "Unit" }',
                                entrypoint: null,
                                contract_address: null,
                                destination: 'tz1UovZ16T6vTuCRQhWKtE7jqeC6QaTzbc3P',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.operations last and date_range gte', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS") {
                address
                first_seen
                activated
                public_key
                operations (
                last: 3
                date_range: {
                    gte: "2019-10-22T18:56:24Z"
                }
                ) {
                    page_info {
                        start_cursor
                        end_cursor
                        has_next_page
                        has_previous_page
                    }
                    edges {
                        cursor
                        node {
                            hash
                            batch_position
                            kind
                            timestamp
                            level
                            block
                            id
                            source
                            gas
                            counter
                            gas_limit
                            storage_limit
                            public_key
                            amount
                            parameters
                            entrypoint
                            contract_address
                            destination
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                first_seen: '2018-09-13T14:51:20Z',
                activated: '2018-09-13T14:49:20.000Z',
                public_key: null,
                operations: {
                    page_info: {
                        start_cursor: 'ooADLykQuRbk3yUc5f465SRJhGyDtJDFit48WpCp9tipnDK399X:0',
                        end_cursor: 'ooUuE97rDNfvxm4tVy8CK8yHz6P3LdvFj5Sn41vw26hgRXT5YRr:1',
                        has_next_page: false,
                        has_previous_page: true,
                    },
                    edges: [
                        {
                            cursor: 'ooADLykQuRbk3yUc5f465SRJhGyDtJDFit48WpCp9tipnDK399X:0',
                            node: {
                                hash: 'ooADLykQuRbk3yUc5f465SRJhGyDtJDFit48WpCp9tipnDK399X',
                                batch_position: 0,
                                kind: 'transaction',
                                timestamp: '2019-10-24T20:38:46Z',
                                level: 664348,
                                block: 'BM8uvwRPXp7QtwUwcr2FtSsooUJcRbnN7hvDhUochXS8dpcXfQ8',
                                id: 15227316,
                                source: 'tz1KtGwriE7VuLwT3LwuvU9Nv4wAxP7XZ57d',
                                gas: 17920,
                                counter: 'e2fe010000000000',
                                gas_limit: 'fa58020000000000',
                                storage_limit: '2c01000000000000',
                                public_key: null,
                                amount: '10009000000',
                                parameters: '{ "prim": "Unit" }',
                                entrypoint: null,
                                contract_address: null,
                                destination: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                            },
                        },
                        {
                            cursor: 'onf5R7jCRJMfATspY8BB7ye4izQUVPzsBohqqNgsg8XTjqxReeU:0',
                            node: {
                                hash: 'onf5R7jCRJMfATspY8BB7ye4izQUVPzsBohqqNgsg8XTjqxReeU',
                                batch_position: 0,
                                kind: 'delegation',
                                timestamp: '2019-10-24T19:30:42Z',
                                level: 664283,
                                block: 'BM2WbVVx8AG1rdMcVNtiX9wFtrTFSk2BKrMhbHh1Z56gTwnzQs7',
                                id: 15225640,
                                source: 'tz1YPCK2iRxWZYHxeUtKBKRosdQ1SAHKeACe',
                                gas: 2500,
                                counter: '3be8200000000000',
                                gas_limit: '1027000000000000',
                                storage_limit: '',
                                public_key: null,
                                amount: null,
                                parameters: null,
                                entrypoint: null,
                                contract_address: null,
                                destination: null,
                            },
                        },
                        {
                            cursor: 'ooUuE97rDNfvxm4tVy8CK8yHz6P3LdvFj5Sn41vw26hgRXT5YRr:1',
                            node: {
                                hash: 'ooUuE97rDNfvxm4tVy8CK8yHz6P3LdvFj5Sn41vw26hgRXT5YRr',
                                batch_position: 1,
                                kind: 'delegation',
                                timestamp: '2019-10-24T19:05:42Z',
                                level: 664258,
                                block: 'BKm7KPd6erxKgeHE4TTsqe78b5HJx33QTWa6rfNpSkt8PuzHkmf',
                                id: 15224994,
                                source: 'tz1RAWNEEFr2DSEDQVPnHXhH24GLMDF5XNjJ',
                                gas: 2500,
                                counter: '1f9e210000000000',
                                gas_limit: '1027000000000000',
                                storage_limit: '',
                                public_key: null,
                                amount: null,
                                parameters: null,
                                entrypoint: null,
                                contract_address: null,
                                destination: null,
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.operations last and date_range gte and before', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS") {
                address
                first_seen
                activated
                public_key
                operations (
                last: 3
                date_range: {
                    gte: "2019-10-22T18:56:24Z"
                }
                before: "ooUuE97rDNfvxm4tVy8CK8yHz6P3LdvFj5Sn41vw26hgRXT5YRr:1"
                ) {
                    page_info {
                        start_cursor
                        end_cursor
                        has_next_page
                        has_previous_page
                    }
                    edges {
                        cursor
                        node {
                            hash
                            batch_position
                            kind
                            timestamp
                            level
                            block
                            id
                            source
                            gas
                            counter
                            gas_limit
                            storage_limit
                            public_key
                            amount
                            parameters
                            entrypoint
                            contract_address
                            destination
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                first_seen: '2018-09-13T14:51:20Z',
                activated: '2018-09-13T14:49:20.000Z',
                public_key: null,
                operations: {
                    page_info: {
                        start_cursor: 'ooTga7Hce67qSQ7J73bGcwReCpbwxMN7CPbzC27Wtcy6mKnEGez:0',
                        end_cursor: 'onf5R7jCRJMfATspY8BB7ye4izQUVPzsBohqqNgsg8XTjqxReeU:0',
                        has_next_page: true,
                        has_previous_page: true,
                    },
                    edges: [
                        {
                            cursor: 'ooTga7Hce67qSQ7J73bGcwReCpbwxMN7CPbzC27Wtcy6mKnEGez:0',
                            node: {
                                hash: 'ooTga7Hce67qSQ7J73bGcwReCpbwxMN7CPbzC27Wtcy6mKnEGez',
                                batch_position: 0,
                                kind: 'seed_nonce_revelation',
                                timestamp: '2019-12-18T13:44:30Z',
                                level: 741378,
                                block: 'BLHNypF2AZdxRs66kwrjVVsr3svPC72EWYvifBNt3wwNRmCPVcw',
                                id: 17185980,
                                source: null,
                                gas: null,
                                counter: null,
                                gas_limit: null,
                                storage_limit: null,
                                public_key: null,
                                amount: null,
                                parameters: null,
                                entrypoint: null,
                                contract_address: null,
                                destination: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                            },
                        },
                        {
                            cursor: 'ooADLykQuRbk3yUc5f465SRJhGyDtJDFit48WpCp9tipnDK399X:0',
                            node: {
                                hash: 'ooADLykQuRbk3yUc5f465SRJhGyDtJDFit48WpCp9tipnDK399X',
                                batch_position: 0,
                                kind: 'transaction',
                                timestamp: '2019-10-24T20:38:46Z',
                                level: 664348,
                                block: 'BM8uvwRPXp7QtwUwcr2FtSsooUJcRbnN7hvDhUochXS8dpcXfQ8',
                                id: 15227316,
                                source: 'tz1KtGwriE7VuLwT3LwuvU9Nv4wAxP7XZ57d',
                                gas: 17920,
                                counter: 'e2fe010000000000',
                                gas_limit: 'fa58020000000000',
                                storage_limit: '2c01000000000000',
                                public_key: null,
                                amount: '10009000000',
                                parameters: '{ "prim": "Unit" }',
                                entrypoint: null,
                                contract_address: null,
                                destination: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                            },
                        },
                        {
                            cursor: 'onf5R7jCRJMfATspY8BB7ye4izQUVPzsBohqqNgsg8XTjqxReeU:0',
                            node: {
                                hash: 'onf5R7jCRJMfATspY8BB7ye4izQUVPzsBohqqNgsg8XTjqxReeU',
                                batch_position: 0,
                                kind: 'delegation',
                                timestamp: '2019-10-24T19:30:42Z',
                                level: 664283,
                                block: 'BM2WbVVx8AG1rdMcVNtiX9wFtrTFSk2BKrMhbHh1Z56gTwnzQs7',
                                id: 15225640,
                                source: 'tz1YPCK2iRxWZYHxeUtKBKRosdQ1SAHKeACe',
                                gas: 2500,
                                counter: '3be8200000000000',
                                gas_limit: '1027000000000000',
                                storage_limit: '',
                                public_key: null,
                                amount: null,
                                parameters: null,
                                entrypoint: null,
                                contract_address: null,
                                destination: null,
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.operations last and date_range gte and after', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS") {
                address
                first_seen
                activated
                public_key
                operations (
                last: 3
                date_range: {
                    gte: "2019-10-22T18:56:24Z"
                }
                after: "ooADLykQuRbk3yUc5f465SRJhGyDtJDFit48WpCp9tipnDK399X:0"
                ) {
                    page_info {
                        start_cursor
                        end_cursor
                        has_next_page
                        has_previous_page
                    }
                    edges {
                        cursor
                        node {
                            hash
                            batch_position
                            kind
                            timestamp
                            level
                            block
                            id
                            source
                            gas
                            counter
                            gas_limit
                            storage_limit
                            public_key
                            amount
                            parameters
                            entrypoint
                            contract_address
                            destination
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                first_seen: '2018-09-13T14:51:20Z',
                activated: '2018-09-13T14:49:20.000Z',
                public_key: null,
                operations: {
                    page_info: {
                        start_cursor: 'onf5R7jCRJMfATspY8BB7ye4izQUVPzsBohqqNgsg8XTjqxReeU:0',
                        end_cursor: 'ooUuE97rDNfvxm4tVy8CK8yHz6P3LdvFj5Sn41vw26hgRXT5YRr:1',
                        has_next_page: false,
                        has_previous_page: true,
                    },
                    edges: [
                        {
                            cursor: 'onf5R7jCRJMfATspY8BB7ye4izQUVPzsBohqqNgsg8XTjqxReeU:0',
                            node: {
                                hash: 'onf5R7jCRJMfATspY8BB7ye4izQUVPzsBohqqNgsg8XTjqxReeU',
                                batch_position: 0,
                                kind: 'delegation',
                                timestamp: '2019-10-24T19:30:42Z',
                                level: 664283,
                                block: 'BM2WbVVx8AG1rdMcVNtiX9wFtrTFSk2BKrMhbHh1Z56gTwnzQs7',
                                id: 15225640,
                                source: 'tz1YPCK2iRxWZYHxeUtKBKRosdQ1SAHKeACe',
                                gas: 2500,
                                counter: '3be8200000000000',
                                gas_limit: '1027000000000000',
                                storage_limit: '',
                                public_key: null,
                                amount: null,
                                parameters: null,
                                entrypoint: null,
                                contract_address: null,
                                destination: null,
                            },
                        },
                        {
                            cursor: 'ooUuE97rDNfvxm4tVy8CK8yHz6P3LdvFj5Sn41vw26hgRXT5YRr:1',
                            node: {
                                hash: 'ooUuE97rDNfvxm4tVy8CK8yHz6P3LdvFj5Sn41vw26hgRXT5YRr',
                                batch_position: 1,
                                kind: 'delegation',
                                timestamp: '2019-10-24T19:05:42Z',
                                level: 664258,
                                block: 'BKm7KPd6erxKgeHE4TTsqe78b5HJx33QTWa6rfNpSkt8PuzHkmf',
                                id: 15224994,
                                source: 'tz1RAWNEEFr2DSEDQVPnHXhH24GLMDF5XNjJ',
                                gas: 2500,
                                counter: '1f9e210000000000',
                                gas_limit: '1027000000000000',
                                storage_limit: '',
                                public_key: null,
                                amount: null,
                                parameters: null,
                                entrypoint: null,
                                contract_address: null,
                                destination: null,
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.operations last and date_range gte and order_by', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS") {
                address
                first_seen
                activated
                public_key
                operations (
                    last: 3
                    date_range: {
                    gte: "2019-10-22T18:56:24Z"
                    }
                    order_by: {field: ID, direction: ASC}
                ) {
                    page_info {
                        start_cursor
                        end_cursor
                        has_next_page
                        has_previous_page
                    }
                    edges {
                        cursor
                        node {
                            hash
                            batch_position
                            kind
                            timestamp
                            level
                            block
                            id
                            source
                            gas
                            counter
                            gas_limit
                            storage_limit
                            public_key
                            amount
                            parameters
                            entrypoint
                            contract_address
                            destination
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                first_seen: '2018-09-13T14:51:20Z',
                activated: '2018-09-13T14:49:20.000Z',
                public_key: null,
                operations: {
                    page_info: {
                        start_cursor: 'ootgGUV6dDRfBBF9wutPQZWzeetqiAUkwrkwSL1Qy6NrTNUs6Jw:0',
                        end_cursor: 'opNxsLZeNMttsyh4PxLk35dmRsJyLXQwJjpVp89ip7pU2j5yzyR:0',
                        has_next_page: false,
                        has_previous_page: true,
                    },
                    edges: [
                        {
                            cursor: 'ootgGUV6dDRfBBF9wutPQZWzeetqiAUkwrkwSL1Qy6NrTNUs6Jw:0',
                            node: {
                                hash: 'ootgGUV6dDRfBBF9wutPQZWzeetqiAUkwrkwSL1Qy6NrTNUs6Jw',
                                batch_position: 0,
                                kind: 'transaction',
                                timestamp: '2020-07-15T20:58:27Z',
                                level: 1041413,
                                block: 'BMf1K4SE3WqRJ6eD345kU74pjLkfB7ABK5u5KwZrxckNHFvUr4v',
                                id: 25134264,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                gas: 1285,
                                counter: 'e1d0010000000000',
                                gas_limit: '4328000000000000',
                                storage_limit: '1501000000000000',
                                public_key: null,
                                amount: '14193181',
                                parameters: '{ "prim": "Unit" }',
                                entrypoint: null,
                                contract_address: null,
                                destination: 'tz1cb9DMkq9ZdrDVxi3HwyLEMtbDseeZPZEa',
                            },
                        },
                        {
                            cursor: 'opEHTGa4n4efFQfpaWFNqoY4dbzXrBEo7Xo8v74AX5pEREqK28S:0',
                            node: {
                                hash: 'opEHTGa4n4efFQfpaWFNqoY4dbzXrBEo7Xo8v74AX5pEREqK28S',
                                batch_position: 0,
                                kind: 'transaction',
                                timestamp: '2020-07-17T19:46:24Z',
                                level: 1044207,
                                block: 'BMYhcRrNnHTthRueAKoiyBxyuzJWEMqMgkxzqiVFXHtS84ALKua',
                                id: 25210462,
                                source: 'tz1YPCK2iRxWZYHxeUtKBKRosdQ1SAHKeACe',
                                gas: 1420,
                                counter: '3ee8200000000000',
                                gas_limit: '6829000000000000',
                                storage_limit: '2c01000000000000',
                                public_key: null,
                                amount: '192000000',
                                parameters: '{ "prim": "Unit" }',
                                entrypoint: null,
                                contract_address: null,
                                destination: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                            },
                        },
                        {
                            cursor: 'opNxsLZeNMttsyh4PxLk35dmRsJyLXQwJjpVp89ip7pU2j5yzyR:0',
                            node: {
                                hash: 'opNxsLZeNMttsyh4PxLk35dmRsJyLXQwJjpVp89ip7pU2j5yzyR',
                                batch_position: 0,
                                kind: 'transaction',
                                timestamp: '2020-08-15T22:11:48Z',
                                level: 1085872,
                                block: 'BLEwgk9RsqEfhtm6u3PrLXcu7pXQ6dWMysJRVTAhNRqVqhCsF9g',
                                id: 26331765,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                gas: 1285,
                                counter: 'e2d0010000000000',
                                gas_limit: '4328000000000000',
                                storage_limit: '1501000000000000',
                                public_key: null,
                                amount: '50000000',
                                parameters: '{ "prim": "Unit" }',
                                entrypoint: null,
                                contract_address: null,
                                destination: 'tz1UovZ16T6vTuCRQhWKtE7jqeC6QaTzbc3P',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.operations last and date_range lte', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS") {
                address
                first_seen
                activated
                public_key
                operations (
                last: 3
                date_range: {
                    lte: "2019-10-22T18:56:24Z"
                }
                ) {
                    page_info {
                        start_cursor
                        end_cursor
                        has_next_page
                        has_previous_page
                    }
                    edges {
                        cursor
                        node {
                            hash
                            batch_position
                            kind
                            timestamp
                            level
                            block
                            id
                            source
                            gas
                            counter
                            gas_limit
                            storage_limit
                            public_key
                            amount
                            parameters
                            entrypoint
                            contract_address
                            destination
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                first_seen: '2018-09-13T14:51:20Z',
                activated: '2018-09-13T14:49:20.000Z',
                public_key: null,
                operations: {
                    page_info: {
                        start_cursor: 'oop8aPtDYg7K3xqqMadPKoXzxTLNpBukasrKtBQs9bsjXQ1UWVt:1',
                        end_cursor: 'ontHLDmy96uXcNBUnarBPoWYSohPN54fNiWbdvHpSEHvA2mdeu5:0',
                        has_next_page: false,
                        has_previous_page: true,
                    },
                    edges: [
                        {
                            cursor: 'oop8aPtDYg7K3xqqMadPKoXzxTLNpBukasrKtBQs9bsjXQ1UWVt:1',
                            node: {
                                hash: 'oop8aPtDYg7K3xqqMadPKoXzxTLNpBukasrKtBQs9bsjXQ1UWVt',
                                batch_position: 1,
                                kind: 'transaction',
                                timestamp: '2018-09-13T16:12:50Z',
                                level: 102031,
                                block: 'BLowVvoAyjeoMY4BRja6rEPdX37UjfdeB4JFtgQ4ZvCiRg9MNdA',
                                id: 1427150,
                                source: 'tz1PECYmqmuba13rnNxyoBkvH2ChUt8BuJuG',
                                gas: 0,
                                counter: '9d4d000000000000',
                                gas_limit: '',
                                storage_limit: '',
                                public_key: null,
                                amount: '1000000',
                                parameters: null,
                                entrypoint: null,
                                contract_address: null,
                                destination: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                            },
                        },
                        {
                            cursor: 'opQqKKXhzznFRxMPdiLyP4V1KWfuDrrNSkqF8jQ6v4JENR3K6jP:0',
                            node: {
                                hash: 'opQqKKXhzznFRxMPdiLyP4V1KWfuDrrNSkqF8jQ6v4JENR3K6jP',
                                batch_position: 0,
                                kind: 'transaction',
                                timestamp: '2018-09-13T14:51:20Z',
                                level: 101957,
                                block: 'BMQzE1UWod12P5XkE15fS4CNRvKLqmWknwCEDuTP1FKEhzrVjtU',
                                id: 1425714,
                                source: 'tz1LBEKXaxQbd5Gtzbc1ATCwc3pppu81aWGc',
                                gas: 10,
                                counter: '62e7000000000000',
                                gas_limit: 'c800000000000000',
                                storage_limit: '',
                                public_key: null,
                                amount: '14',
                                parameters: null,
                                entrypoint: null,
                                contract_address: null,
                                destination: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                            },
                        },
                        {
                            cursor: 'ontHLDmy96uXcNBUnarBPoWYSohPN54fNiWbdvHpSEHvA2mdeu5:0',
                            node: {
                                hash: 'ontHLDmy96uXcNBUnarBPoWYSohPN54fNiWbdvHpSEHvA2mdeu5',
                                batch_position: 0,
                                kind: 'activate_account',
                                timestamp: '2018-09-13T14:49:20Z',
                                level: 101955,
                                block: 'BLrAAf7V4FDypigoEexGwXdzoonabRSjWdFaEAubV7p5JdGaaU5',
                                id: 1425677,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                gas: null,
                                counter: null,
                                gas_limit: null,
                                storage_limit: null,
                                public_key: null,
                                amount: null,
                                parameters: null,
                                entrypoint: null,
                                contract_address: null,
                                destination: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.operations last and date_range lte and before', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS") {
                address
                first_seen
                activated
                public_key
                operations (
                last: 3
                date_range: {
                    lte: "2019-10-22T18:56:24Z"
                }
                before: "opZSVPxNoaExjX6dCFnoPAh5yHk6g7BaJdce2AXUHuwW3c7ucNA:0"
                ) {
                    page_info {
                        start_cursor
                        end_cursor
                        has_next_page
                        has_previous_page
                    }
                    edges {
                        cursor
                        node {
                            hash
                            batch_position
                            kind
                            timestamp
                            level
                            block
                            id
                            source
                            gas
                            counter
                            gas_limit
                            storage_limit
                            public_key
                            amount
                            parameters
                            entrypoint
                            contract_address
                            destination
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                first_seen: '2018-09-13T14:51:20Z',
                activated: '2018-09-13T14:49:20.000Z',
                public_key: null,
                operations: {
                    page_info: {
                        start_cursor: 'opT4s5rWRbAEcbq6GZ272LyxNCxACqdk7c6Jm4buDcR6Fh4Zs3c:0',
                        end_cursor: 'oobMbc7MHSz5ttV3AcjiXqaxZSxs9ShGjcHmopoo85Jays2rniJ:0',
                        has_next_page: true,
                        has_previous_page: false,
                    },
                    edges: [
                        {
                            cursor: 'opT4s5rWRbAEcbq6GZ272LyxNCxACqdk7c6Jm4buDcR6Fh4Zs3c:0',
                            node: {
                                hash: 'opT4s5rWRbAEcbq6GZ272LyxNCxACqdk7c6Jm4buDcR6Fh4Zs3c',
                                batch_position: 0,
                                kind: 'transaction',
                                timestamp: '2019-06-23T00:44:47Z',
                                level: 491770,
                                block: 'BLyWtfcsLhVGAbsYJPSFpUQiPwdHkHExBrgB3WD9wesuo3PLfUV',
                                id: 10910203,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                gas: 1564,
                                counter: 'c8d0010000000000',
                                gas_limit: '4533000000000000',
                                storage_limit: '',
                                public_key: null,
                                amount: '0',
                                parameters: null,
                                entrypoint: null,
                                contract_address: null,
                                destination: 'KT1BRudFZEXLYANgmZTka1xCDN5nWTMWY7SZ',
                            },
                        },
                        {
                            cursor: 'oobMbc7MHSz5ttV3AcjiXqaxZSxs9ShGjcHmopoo85Jays2rniJ:0',
                            node: {
                                hash: 'oobMbc7MHSz5ttV3AcjiXqaxZSxs9ShGjcHmopoo85Jays2rniJ',
                                batch_position: 0,
                                kind: 'transaction',
                                timestamp: '2019-06-13T16:31:03Z',
                                level: 478922,
                                block: 'BKsTAwJ939thYVT5gN4HGE8kPApsRUXELGySbv7CNgXZ9VkGKar',
                                id: 10588192,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                gas: 1284,
                                counter: 'c7d0010000000000',
                                gas_limit: '3c28000000000000',
                                storage_limit: '',
                                public_key: null,
                                amount: '21594067',
                                parameters: null,
                                entrypoint: null,
                                contract_address: null,
                                destination: 'KT1SrkUKHSf9m2yruXSMCVfrfUUsXN8yVtag',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.operations last and date_range lte and after', async () => {
        const GET_ACCOUNT = gql`
       query {
            account(address: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS") {
                address
                first_seen
                activated
                public_key
                operations (
                last: 3
                date_range: {
                    lte: "2019-10-22T18:56:24Z"
                }
                after: "oobMbc7MHSz5ttV3AcjiXqaxZSxs9ShGjcHmopoo85Jays2rniJ:0"
                ) {
                    page_info {
                        start_cursor
                        end_cursor
                        has_next_page
                        has_previous_page
                    }
                    edges {
                        cursor
                        node {
                            hash
                            batch_position
                            kind
                            timestamp
                            level
                            block
                            id
                            source
                            gas
                            counter
                            gas_limit
                            storage_limit
                            public_key
                            amount
                            parameters
                            entrypoint
                            contract_address
                            destination
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                first_seen: '2018-09-13T14:51:20Z',
                activated: '2018-09-13T14:49:20.000Z',
                public_key: null,
                operations: {
                    page_info: {
                        start_cursor: 'oop8aPtDYg7K3xqqMadPKoXzxTLNpBukasrKtBQs9bsjXQ1UWVt:1',
                        end_cursor: 'ontHLDmy96uXcNBUnarBPoWYSohPN54fNiWbdvHpSEHvA2mdeu5:0',
                        has_next_page: false,
                        has_previous_page: true,
                    },
                    edges: [
                        {
                            cursor: 'oop8aPtDYg7K3xqqMadPKoXzxTLNpBukasrKtBQs9bsjXQ1UWVt:1',
                            node: {
                                hash: 'oop8aPtDYg7K3xqqMadPKoXzxTLNpBukasrKtBQs9bsjXQ1UWVt',
                                batch_position: 1,
                                kind: 'transaction',
                                timestamp: '2018-09-13T16:12:50Z',
                                level: 102031,
                                block: 'BLowVvoAyjeoMY4BRja6rEPdX37UjfdeB4JFtgQ4ZvCiRg9MNdA',
                                id: 1427150,
                                source: 'tz1PECYmqmuba13rnNxyoBkvH2ChUt8BuJuG',
                                gas: 0,
                                counter: '9d4d000000000000',
                                gas_limit: '',
                                storage_limit: '',
                                public_key: null,
                                amount: '1000000',
                                parameters: null,
                                entrypoint: null,
                                contract_address: null,
                                destination: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                            },
                        },
                        {
                            cursor: 'opQqKKXhzznFRxMPdiLyP4V1KWfuDrrNSkqF8jQ6v4JENR3K6jP:0',
                            node: {
                                hash: 'opQqKKXhzznFRxMPdiLyP4V1KWfuDrrNSkqF8jQ6v4JENR3K6jP',
                                batch_position: 0,
                                kind: 'transaction',
                                timestamp: '2018-09-13T14:51:20Z',
                                level: 101957,
                                block: 'BMQzE1UWod12P5XkE15fS4CNRvKLqmWknwCEDuTP1FKEhzrVjtU',
                                id: 1425714,
                                source: 'tz1LBEKXaxQbd5Gtzbc1ATCwc3pppu81aWGc',
                                gas: 10,
                                counter: '62e7000000000000',
                                gas_limit: 'c800000000000000',
                                storage_limit: '',
                                public_key: null,
                                amount: '14',
                                parameters: null,
                                entrypoint: null,
                                contract_address: null,
                                destination: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                            },
                        },
                        {
                            cursor: 'ontHLDmy96uXcNBUnarBPoWYSohPN54fNiWbdvHpSEHvA2mdeu5:0',
                            node: {
                                hash: 'ontHLDmy96uXcNBUnarBPoWYSohPN54fNiWbdvHpSEHvA2mdeu5',
                                batch_position: 0,
                                kind: 'activate_account',
                                timestamp: '2018-09-13T14:49:20Z',
                                level: 101955,
                                block: 'BLrAAf7V4FDypigoEexGwXdzoonabRSjWdFaEAubV7p5JdGaaU5',
                                id: 1425677,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                gas: null,
                                counter: null,
                                gas_limit: null,
                                storage_limit: null,
                                public_key: null,
                                amount: null,
                                parameters: null,
                                entrypoint: null,
                                contract_address: null,
                                destination: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.operations last and date_range lte and order_by', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS") {
                address
                first_seen
                activated
                public_key
                operations (
                    last: 3
                    date_range: {
                    lte: "2019-10-22T18:56:24Z"
                    }
                    order_by: {field: ID, direction: ASC}
                ) {
                    page_info {
                        start_cursor
                        end_cursor
                        has_next_page
                        has_previous_page
                    }
                    edges {
                        cursor
                        node {
                            hash
                            batch_position
                            kind
                            timestamp
                            level
                            block
                            id
                            source
                            gas
                            counter
                            gas_limit
                            storage_limit
                            public_key
                            amount
                            parameters
                            entrypoint
                            contract_address
                            destination
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                first_seen: '2018-09-13T14:51:20Z',
                activated: '2018-09-13T14:49:20.000Z',
                public_key: null,
                operations: {
                    page_info: {
                        start_cursor: 'opZSVPxNoaExjX6dCFnoPAh5yHk6g7BaJdce2AXUHuwW3c7ucNA:0',
                        end_cursor: 'opT4s5rWRbAEcbq6GZ272LyxNCxACqdk7c6Jm4buDcR6Fh4Zs3c:0',
                        has_next_page: false,
                        has_previous_page: true,
                    },
                    edges: [
                        {
                            cursor: 'opZSVPxNoaExjX6dCFnoPAh5yHk6g7BaJdce2AXUHuwW3c7ucNA:0',
                            node: {
                                hash: 'opZSVPxNoaExjX6dCFnoPAh5yHk6g7BaJdce2AXUHuwW3c7ucNA',
                                batch_position: 0,
                                kind: 'transaction',
                                timestamp: '2019-06-13T16:29:03Z',
                                level: 478920,
                                block: 'BME5VpmuMaZfWK7sgrmqrH73yZnuiDhJGuE8dsURJTodB5LWDCe',
                                id: 10588145,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                gas: 1284,
                                counter: 'c6d0010000000000',
                                gas_limit: '3c28000000000000',
                                storage_limit: '',
                                public_key: null,
                                amount: '26594806',
                                parameters: null,
                                entrypoint: null,
                                contract_address: null,
                                destination: 'KT1Hw82JKNLeQUF58Tr5s8hk3Yn8XW3Hv81C',
                            },
                        },
                        {
                            cursor: 'oobMbc7MHSz5ttV3AcjiXqaxZSxs9ShGjcHmopoo85Jays2rniJ:0',
                            node: {
                                hash: 'oobMbc7MHSz5ttV3AcjiXqaxZSxs9ShGjcHmopoo85Jays2rniJ',
                                batch_position: 0,
                                kind: 'transaction',
                                timestamp: '2019-06-13T16:31:03Z',
                                level: 478922,
                                block: 'BKsTAwJ939thYVT5gN4HGE8kPApsRUXELGySbv7CNgXZ9VkGKar',
                                id: 10588192,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                gas: 1284,
                                counter: 'c7d0010000000000',
                                gas_limit: '3c28000000000000',
                                storage_limit: '',
                                public_key: null,
                                amount: '21594067',
                                parameters: null,
                                entrypoint: null,
                                contract_address: null,
                                destination: 'KT1SrkUKHSf9m2yruXSMCVfrfUUsXN8yVtag',
                            },
                        },
                        {
                            cursor: 'opT4s5rWRbAEcbq6GZ272LyxNCxACqdk7c6Jm4buDcR6Fh4Zs3c:0',
                            node: {
                                hash: 'opT4s5rWRbAEcbq6GZ272LyxNCxACqdk7c6Jm4buDcR6Fh4Zs3c',
                                batch_position: 0,
                                kind: 'transaction',
                                timestamp: '2019-06-23T00:44:47Z',
                                level: 491770,
                                block: 'BLyWtfcsLhVGAbsYJPSFpUQiPwdHkHExBrgB3WD9wesuo3PLfUV',
                                id: 10910203,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                gas: 1564,
                                counter: 'c8d0010000000000',
                                gas_limit: '4533000000000000',
                                storage_limit: '',
                                public_key: null,
                                amount: '0',
                                parameters: null,
                                entrypoint: null,
                                contract_address: null,
                                destination: 'KT1BRudFZEXLYANgmZTka1xCDN5nWTMWY7SZ',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.operations last and date_range gte/lte', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS") {
                address
                first_seen
                activated
                public_key
                operations (
                last: 3
                date_range: {
                    gte: "2019-10-22T18:56:24Z"
                    lte: "2019-10-27T12:46:11Z"
                }
                ) {
                    page_info {
                        start_cursor
                        end_cursor
                        has_next_page
                        has_previous_page
                    }
                    edges {
                        cursor
                        node {
                            hash
                            batch_position
                            kind
                            timestamp
                            level
                            block
                            id
                            source
                            gas
                            counter
                            gas_limit
                            storage_limit
                            public_key
                            amount
                            parameters
                            entrypoint
                            contract_address
                            destination
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                first_seen: '2018-09-13T14:51:20Z',
                activated: '2018-09-13T14:49:20.000Z',
                public_key: null,
                operations: {
                    page_info: {
                        start_cursor: 'ooADLykQuRbk3yUc5f465SRJhGyDtJDFit48WpCp9tipnDK399X:0',
                        end_cursor: 'ooUuE97rDNfvxm4tVy8CK8yHz6P3LdvFj5Sn41vw26hgRXT5YRr:1',
                        has_next_page: false,
                        has_previous_page: false,
                    },
                    edges: [
                        {
                            cursor: 'ooADLykQuRbk3yUc5f465SRJhGyDtJDFit48WpCp9tipnDK399X:0',
                            node: {
                                hash: 'ooADLykQuRbk3yUc5f465SRJhGyDtJDFit48WpCp9tipnDK399X',
                                batch_position: 0,
                                kind: 'transaction',
                                timestamp: '2019-10-24T20:38:46Z',
                                level: 664348,
                                block: 'BM8uvwRPXp7QtwUwcr2FtSsooUJcRbnN7hvDhUochXS8dpcXfQ8',
                                id: 15227316,
                                source: 'tz1KtGwriE7VuLwT3LwuvU9Nv4wAxP7XZ57d',
                                gas: 17920,
                                counter: 'e2fe010000000000',
                                gas_limit: 'fa58020000000000',
                                storage_limit: '2c01000000000000',
                                public_key: null,
                                amount: '10009000000',
                                parameters: '{ "prim": "Unit" }',
                                entrypoint: null,
                                contract_address: null,
                                destination: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                            },
                        },
                        {
                            cursor: 'onf5R7jCRJMfATspY8BB7ye4izQUVPzsBohqqNgsg8XTjqxReeU:0',
                            node: {
                                hash: 'onf5R7jCRJMfATspY8BB7ye4izQUVPzsBohqqNgsg8XTjqxReeU',
                                batch_position: 0,
                                kind: 'delegation',
                                timestamp: '2019-10-24T19:30:42Z',
                                level: 664283,
                                block: 'BM2WbVVx8AG1rdMcVNtiX9wFtrTFSk2BKrMhbHh1Z56gTwnzQs7',
                                id: 15225640,
                                source: 'tz1YPCK2iRxWZYHxeUtKBKRosdQ1SAHKeACe',
                                gas: 2500,
                                counter: '3be8200000000000',
                                gas_limit: '1027000000000000',
                                storage_limit: '',
                                public_key: null,
                                amount: null,
                                parameters: null,
                                entrypoint: null,
                                contract_address: null,
                                destination: null,
                            },
                        },
                        {
                            cursor: 'ooUuE97rDNfvxm4tVy8CK8yHz6P3LdvFj5Sn41vw26hgRXT5YRr:1',
                            node: {
                                hash: 'ooUuE97rDNfvxm4tVy8CK8yHz6P3LdvFj5Sn41vw26hgRXT5YRr',
                                batch_position: 1,
                                kind: 'delegation',
                                timestamp: '2019-10-24T19:05:42Z',
                                level: 664258,
                                block: 'BKm7KPd6erxKgeHE4TTsqe78b5HJx33QTWa6rfNpSkt8PuzHkmf',
                                id: 15224994,
                                source: 'tz1RAWNEEFr2DSEDQVPnHXhH24GLMDF5XNjJ',
                                gas: 2500,
                                counter: '1f9e210000000000',
                                gas_limit: '1027000000000000',
                                storage_limit: '',
                                public_key: null,
                                amount: null,
                                parameters: null,
                                entrypoint: null,
                                contract_address: null,
                                destination: null,
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.operations last and date_range gte/lte and before', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS") {
                address
                first_seen
                activated
                public_key
                operations (
                last: 3
                date_range: {
                    gte: "2019-10-22T18:56:24Z"
                    lte: "2019-10-27T12:46:11Z"
                }
                before: "ooUuE97rDNfvxm4tVy8CK8yHz6P3LdvFj5Sn41vw26hgRXT5YRr:1"
                ) {
                    page_info {
                        start_cursor
                        end_cursor
                        has_next_page
                        has_previous_page
                    }
                    edges {
                        cursor
                        node {
                            hash
                            batch_position
                            kind
                            timestamp
                            level
                            block
                            id
                            source
                            gas
                            counter
                            gas_limit
                            storage_limit
                            public_key
                            amount
                            parameters
                            entrypoint
                            contract_address
                            destination
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                first_seen: '2018-09-13T14:51:20Z',
                activated: '2018-09-13T14:49:20.000Z',
                public_key: null,
                operations: {
                    page_info: {
                        start_cursor: 'ooADLykQuRbk3yUc5f465SRJhGyDtJDFit48WpCp9tipnDK399X:0',
                        end_cursor: 'onf5R7jCRJMfATspY8BB7ye4izQUVPzsBohqqNgsg8XTjqxReeU:0',
                        has_next_page: true,
                        has_previous_page: false,
                    },
                    edges: [
                        {
                            cursor: 'ooADLykQuRbk3yUc5f465SRJhGyDtJDFit48WpCp9tipnDK399X:0',
                            node: {
                                hash: 'ooADLykQuRbk3yUc5f465SRJhGyDtJDFit48WpCp9tipnDK399X',
                                batch_position: 0,
                                kind: 'transaction',
                                timestamp: '2019-10-24T20:38:46Z',
                                level: 664348,
                                block: 'BM8uvwRPXp7QtwUwcr2FtSsooUJcRbnN7hvDhUochXS8dpcXfQ8',
                                id: 15227316,
                                source: 'tz1KtGwriE7VuLwT3LwuvU9Nv4wAxP7XZ57d',
                                gas: 17920,
                                counter: 'e2fe010000000000',
                                gas_limit: 'fa58020000000000',
                                storage_limit: '2c01000000000000',
                                public_key: null,
                                amount: '10009000000',
                                parameters: '{ "prim": "Unit" }',
                                entrypoint: null,
                                contract_address: null,
                                destination: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                            },
                        },
                        {
                            cursor: 'onf5R7jCRJMfATspY8BB7ye4izQUVPzsBohqqNgsg8XTjqxReeU:0',
                            node: {
                                hash: 'onf5R7jCRJMfATspY8BB7ye4izQUVPzsBohqqNgsg8XTjqxReeU',
                                batch_position: 0,
                                kind: 'delegation',
                                timestamp: '2019-10-24T19:30:42Z',
                                level: 664283,
                                block: 'BM2WbVVx8AG1rdMcVNtiX9wFtrTFSk2BKrMhbHh1Z56gTwnzQs7',
                                id: 15225640,
                                source: 'tz1YPCK2iRxWZYHxeUtKBKRosdQ1SAHKeACe',
                                gas: 2500,
                                counter: '3be8200000000000',
                                gas_limit: '1027000000000000',
                                storage_limit: '',
                                public_key: null,
                                amount: null,
                                parameters: null,
                                entrypoint: null,
                                contract_address: null,
                                destination: null,
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.operations last and date_range gte/lte and after', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS") {
                address
                first_seen
                activated
                public_key
                operations (
                last: 3
                date_range: {
                    gte: "2019-10-22T18:56:24Z"
                    lte: "2019-10-27T12:46:11Z"
                }
                after: "ooADLykQuRbk3yUc5f465SRJhGyDtJDFit48WpCp9tipnDK399X:0"
                ) {
                    page_info {
                        start_cursor
                        end_cursor
                        has_next_page
                        has_previous_page
                    }
                    edges {
                        cursor
                        node {
                            hash
                            batch_position
                            kind
                            timestamp
                            level
                            block
                            id
                            source
                            gas
                            counter
                            gas_limit
                            storage_limit
                            public_key
                            amount
                            parameters
                            entrypoint
                            contract_address
                            destination
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                first_seen: '2018-09-13T14:51:20Z',
                activated: '2018-09-13T14:49:20.000Z',
                public_key: null,
                operations: {
                    page_info: {
                        start_cursor: 'onf5R7jCRJMfATspY8BB7ye4izQUVPzsBohqqNgsg8XTjqxReeU:0',
                        end_cursor: 'ooUuE97rDNfvxm4tVy8CK8yHz6P3LdvFj5Sn41vw26hgRXT5YRr:1',
                        has_next_page: false,
                        has_previous_page: true,
                    },
                    edges: [
                        {
                            cursor: 'onf5R7jCRJMfATspY8BB7ye4izQUVPzsBohqqNgsg8XTjqxReeU:0',
                            node: {
                                hash: 'onf5R7jCRJMfATspY8BB7ye4izQUVPzsBohqqNgsg8XTjqxReeU',
                                batch_position: 0,
                                kind: 'delegation',
                                timestamp: '2019-10-24T19:30:42Z',
                                level: 664283,
                                block: 'BM2WbVVx8AG1rdMcVNtiX9wFtrTFSk2BKrMhbHh1Z56gTwnzQs7',
                                id: 15225640,
                                source: 'tz1YPCK2iRxWZYHxeUtKBKRosdQ1SAHKeACe',
                                gas: 2500,
                                counter: '3be8200000000000',
                                gas_limit: '1027000000000000',
                                storage_limit: '',
                                public_key: null,
                                amount: null,
                                parameters: null,
                                entrypoint: null,
                                contract_address: null,
                                destination: null,
                            },
                        },
                        {
                            cursor: 'ooUuE97rDNfvxm4tVy8CK8yHz6P3LdvFj5Sn41vw26hgRXT5YRr:1',
                            node: {
                                hash: 'ooUuE97rDNfvxm4tVy8CK8yHz6P3LdvFj5Sn41vw26hgRXT5YRr',
                                batch_position: 1,
                                kind: 'delegation',
                                timestamp: '2019-10-24T19:05:42Z',
                                level: 664258,
                                block: 'BKm7KPd6erxKgeHE4TTsqe78b5HJx33QTWa6rfNpSkt8PuzHkmf',
                                id: 15224994,
                                source: 'tz1RAWNEEFr2DSEDQVPnHXhH24GLMDF5XNjJ',
                                gas: 2500,
                                counter: '1f9e210000000000',
                                gas_limit: '1027000000000000',
                                storage_limit: '',
                                public_key: null,
                                amount: null,
                                parameters: null,
                                entrypoint: null,
                                contract_address: null,
                                destination: null,
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.operations last and date_range gte/lte and order_by', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS") {
                address
                first_seen
                activated
                public_key
                operations (
                    last: 3
                    date_range: {
                    gte: "2019-10-22T18:56:24Z"
                    lte: "2020-11-27T12:46:11Z"
                    }
                    order_by: {field: ID, direction: ASC}
                ) {
                    page_info {
                        start_cursor
                        end_cursor
                        has_next_page
                        has_previous_page
                    }
                    edges {
                        cursor
                        node {
                            hash
                            batch_position
                            kind
                            timestamp
                            level
                            block
                            id
                            source
                            gas
                            counter
                            gas_limit
                            storage_limit
                            public_key
                            amount
                            parameters
                            entrypoint
                            contract_address
                            destination
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                first_seen: '2018-09-13T14:51:20Z',
                activated: '2018-09-13T14:49:20.000Z',
                public_key: null,
                operations: {
                    page_info: {
                        start_cursor: 'ootgGUV6dDRfBBF9wutPQZWzeetqiAUkwrkwSL1Qy6NrTNUs6Jw:0',
                        end_cursor: 'opNxsLZeNMttsyh4PxLk35dmRsJyLXQwJjpVp89ip7pU2j5yzyR:0',
                        has_next_page: false,
                        has_previous_page: true,
                    },
                    edges: [
                        {
                            cursor: 'ootgGUV6dDRfBBF9wutPQZWzeetqiAUkwrkwSL1Qy6NrTNUs6Jw:0',
                            node: {
                                hash: 'ootgGUV6dDRfBBF9wutPQZWzeetqiAUkwrkwSL1Qy6NrTNUs6Jw',
                                batch_position: 0,
                                kind: 'transaction',
                                timestamp: '2020-07-15T20:58:27Z',
                                level: 1041413,
                                block: 'BMf1K4SE3WqRJ6eD345kU74pjLkfB7ABK5u5KwZrxckNHFvUr4v',
                                id: 25134264,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                gas: 1285,
                                counter: 'e1d0010000000000',
                                gas_limit: '4328000000000000',
                                storage_limit: '1501000000000000',
                                public_key: null,
                                amount: '14193181',
                                parameters: '{ "prim": "Unit" }',
                                entrypoint: null,
                                contract_address: null,
                                destination: 'tz1cb9DMkq9ZdrDVxi3HwyLEMtbDseeZPZEa',
                            },
                        },
                        {
                            cursor: 'opEHTGa4n4efFQfpaWFNqoY4dbzXrBEo7Xo8v74AX5pEREqK28S:0',
                            node: {
                                hash: 'opEHTGa4n4efFQfpaWFNqoY4dbzXrBEo7Xo8v74AX5pEREqK28S',
                                batch_position: 0,
                                kind: 'transaction',
                                timestamp: '2020-07-17T19:46:24Z',
                                level: 1044207,
                                block: 'BMYhcRrNnHTthRueAKoiyBxyuzJWEMqMgkxzqiVFXHtS84ALKua',
                                id: 25210462,
                                source: 'tz1YPCK2iRxWZYHxeUtKBKRosdQ1SAHKeACe',
                                gas: 1420,
                                counter: '3ee8200000000000',
                                gas_limit: '6829000000000000',
                                storage_limit: '2c01000000000000',
                                public_key: null,
                                amount: '192000000',
                                parameters: '{ "prim": "Unit" }',
                                entrypoint: null,
                                contract_address: null,
                                destination: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                            },
                        },
                        {
                            cursor: 'opNxsLZeNMttsyh4PxLk35dmRsJyLXQwJjpVp89ip7pU2j5yzyR:0',
                            node: {
                                hash: 'opNxsLZeNMttsyh4PxLk35dmRsJyLXQwJjpVp89ip7pU2j5yzyR',
                                batch_position: 0,
                                kind: 'transaction',
                                timestamp: '2020-08-15T22:11:48Z',
                                level: 1085872,
                                block: 'BLEwgk9RsqEfhtm6u3PrLXcu7pXQ6dWMysJRVTAhNRqVqhCsF9g',
                                id: 26331765,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                gas: 1285,
                                counter: 'e2d0010000000000',
                                gas_limit: '4328000000000000',
                                storage_limit: '1501000000000000',
                                public_key: null,
                                amount: '50000000',
                                parameters: '{ "prim": "Unit" }',
                                entrypoint: null,
                                contract_address: null,
                                destination: 'tz1UovZ16T6vTuCRQhWKtE7jqeC6QaTzbc3P',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.operations last and date_range gte and order_by and before', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS") {
                address
                first_seen
                activated
                public_key
                operations (
                last: 3
                order_by: {field: ID, direction: ASC}
                before: "opBg2pLmofX5zTSWc3vHj9w3tXUE7wdbtkvehvRuP3DXFFsonHw:0"
                date_range: {
                    gte: "2019-10-22T12:46:11Z"
                }
                ) {
                    page_info {
                        start_cursor
                        end_cursor
                        has_next_page
                        has_previous_page
                    }
                    edges {
                        cursor
                        node {
                            hash
                            batch_position
                            kind
                            timestamp
                            level
                            block
                            id
                            source
                            gas
                            counter
                            gas_limit
                            storage_limit
                            public_key
                            amount
                            parameters
                            entrypoint
                            contract_address
                            destination
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                first_seen: '2018-09-13T14:51:20Z',
                activated: '2018-09-13T14:49:20.000Z',
                public_key: null,
                operations: {
                    page_info: {
                        start_cursor: 'onf5R7jCRJMfATspY8BB7ye4izQUVPzsBohqqNgsg8XTjqxReeU:0',
                        end_cursor: 'ooTga7Hce67qSQ7J73bGcwReCpbwxMN7CPbzC27Wtcy6mKnEGez:0',
                        has_next_page: true,
                        has_previous_page: true,
                    },
                    edges: [
                        {
                            cursor: 'onf5R7jCRJMfATspY8BB7ye4izQUVPzsBohqqNgsg8XTjqxReeU:0',
                            node: {
                                hash: 'onf5R7jCRJMfATspY8BB7ye4izQUVPzsBohqqNgsg8XTjqxReeU',
                                batch_position: 0,
                                kind: 'delegation',
                                timestamp: '2019-10-24T19:30:42Z',
                                level: 664283,
                                block: 'BM2WbVVx8AG1rdMcVNtiX9wFtrTFSk2BKrMhbHh1Z56gTwnzQs7',
                                id: 15225640,
                                source: 'tz1YPCK2iRxWZYHxeUtKBKRosdQ1SAHKeACe',
                                gas: 2500,
                                counter: '3be8200000000000',
                                gas_limit: '1027000000000000',
                                storage_limit: '',
                                public_key: null,
                                amount: null,
                                parameters: null,
                                entrypoint: null,
                                contract_address: null,
                                destination: null,
                            },
                        },
                        {
                            cursor: 'ooADLykQuRbk3yUc5f465SRJhGyDtJDFit48WpCp9tipnDK399X:0',
                            node: {
                                hash: 'ooADLykQuRbk3yUc5f465SRJhGyDtJDFit48WpCp9tipnDK399X',
                                batch_position: 0,
                                kind: 'transaction',
                                timestamp: '2019-10-24T20:38:46Z',
                                level: 664348,
                                block: 'BM8uvwRPXp7QtwUwcr2FtSsooUJcRbnN7hvDhUochXS8dpcXfQ8',
                                id: 15227316,
                                source: 'tz1KtGwriE7VuLwT3LwuvU9Nv4wAxP7XZ57d',
                                gas: 17920,
                                counter: 'e2fe010000000000',
                                gas_limit: 'fa58020000000000',
                                storage_limit: '2c01000000000000',
                                public_key: null,
                                amount: '10009000000',
                                parameters: '{ "prim": "Unit" }',
                                entrypoint: null,
                                contract_address: null,
                                destination: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                            },
                        },
                        {
                            cursor: 'ooTga7Hce67qSQ7J73bGcwReCpbwxMN7CPbzC27Wtcy6mKnEGez:0',
                            node: {
                                hash: 'ooTga7Hce67qSQ7J73bGcwReCpbwxMN7CPbzC27Wtcy6mKnEGez',
                                batch_position: 0,
                                kind: 'seed_nonce_revelation',
                                timestamp: '2019-12-18T13:44:30Z',
                                level: 741378,
                                block: 'BLHNypF2AZdxRs66kwrjVVsr3svPC72EWYvifBNt3wwNRmCPVcw',
                                id: 17185980,
                                source: null,
                                gas: null,
                                counter: null,
                                gas_limit: null,
                                storage_limit: null,
                                public_key: null,
                                amount: null,
                                parameters: null,
                                entrypoint: null,
                                contract_address: null,
                                destination: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.operations last and date_range gte and order_by and after', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS") {
                address
                first_seen
                activated
                public_key
                operations (
                last: 3
                order_by: {field: ID, direction: ASC}
                after: "oo8sVjaRzoFHaoxgtVdAbqbPujVJzt1ToknGwapTjHJM5jRhyMx:0"
                date_range: {
                    gte: "2019-10-27T12:46:11Z"
                }
                ) {
                    page_info {
                        start_cursor
                        end_cursor
                        has_next_page
                        has_previous_page
                    }
                    edges {
                        cursor
                        node {
                            hash
                            batch_position
                            kind
                            timestamp
                            level
                            block
                            id
                            source
                            gas
                            counter
                            gas_limit
                            storage_limit
                            public_key
                            amount
                            parameters
                            entrypoint
                            contract_address
                            destination
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                first_seen: '2018-09-13T14:51:20Z',
                activated: '2018-09-13T14:49:20.000Z',
                public_key: null,
                operations: {
                    page_info: {
                        start_cursor: 'ootgGUV6dDRfBBF9wutPQZWzeetqiAUkwrkwSL1Qy6NrTNUs6Jw:0',
                        end_cursor: 'opNxsLZeNMttsyh4PxLk35dmRsJyLXQwJjpVp89ip7pU2j5yzyR:0',
                        has_next_page: false,
                        has_previous_page: true,
                    },
                    edges: [
                        {
                            cursor: 'ootgGUV6dDRfBBF9wutPQZWzeetqiAUkwrkwSL1Qy6NrTNUs6Jw:0',
                            node: {
                                hash: 'ootgGUV6dDRfBBF9wutPQZWzeetqiAUkwrkwSL1Qy6NrTNUs6Jw',
                                batch_position: 0,
                                kind: 'transaction',
                                timestamp: '2020-07-15T20:58:27Z',
                                level: 1041413,
                                block: 'BMf1K4SE3WqRJ6eD345kU74pjLkfB7ABK5u5KwZrxckNHFvUr4v',
                                id: 25134264,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                gas: 1285,
                                counter: 'e1d0010000000000',
                                gas_limit: '4328000000000000',
                                storage_limit: '1501000000000000',
                                public_key: null,
                                amount: '14193181',
                                parameters: '{ "prim": "Unit" }',
                                entrypoint: null,
                                contract_address: null,
                                destination: 'tz1cb9DMkq9ZdrDVxi3HwyLEMtbDseeZPZEa',
                            },
                        },
                        {
                            cursor: 'opEHTGa4n4efFQfpaWFNqoY4dbzXrBEo7Xo8v74AX5pEREqK28S:0',
                            node: {
                                hash: 'opEHTGa4n4efFQfpaWFNqoY4dbzXrBEo7Xo8v74AX5pEREqK28S',
                                batch_position: 0,
                                kind: 'transaction',
                                timestamp: '2020-07-17T19:46:24Z',
                                level: 1044207,
                                block: 'BMYhcRrNnHTthRueAKoiyBxyuzJWEMqMgkxzqiVFXHtS84ALKua',
                                id: 25210462,
                                source: 'tz1YPCK2iRxWZYHxeUtKBKRosdQ1SAHKeACe',
                                gas: 1420,
                                counter: '3ee8200000000000',
                                gas_limit: '6829000000000000',
                                storage_limit: '2c01000000000000',
                                public_key: null,
                                amount: '192000000',
                                parameters: '{ "prim": "Unit" }',
                                entrypoint: null,
                                contract_address: null,
                                destination: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                            },
                        },
                        {
                            cursor: 'opNxsLZeNMttsyh4PxLk35dmRsJyLXQwJjpVp89ip7pU2j5yzyR:0',
                            node: {
                                hash: 'opNxsLZeNMttsyh4PxLk35dmRsJyLXQwJjpVp89ip7pU2j5yzyR',
                                batch_position: 0,
                                kind: 'transaction',
                                timestamp: '2020-08-15T22:11:48Z',
                                level: 1085872,
                                block: 'BLEwgk9RsqEfhtm6u3PrLXcu7pXQ6dWMysJRVTAhNRqVqhCsF9g',
                                id: 26331765,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                gas: 1285,
                                counter: 'e2d0010000000000',
                                gas_limit: '4328000000000000',
                                storage_limit: '1501000000000000',
                                public_key: null,
                                amount: '50000000',
                                parameters: '{ "prim": "Unit" }',
                                entrypoint: null,
                                contract_address: null,
                                destination: 'tz1UovZ16T6vTuCRQhWKtE7jqeC6QaTzbc3P',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.operations last and date_range lte and order_by and before', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS") {
                address
                first_seen
                activated
                public_key
                operations (
                last: 3
                order_by: {field: ID, direction: ASC}
                before: "ooADLykQuRbk3yUc5f465SRJhGyDtJDFit48WpCp9tipnDK399X:0"
                date_range: {
                    lte: "2019-10-27T12:46:11Z"
                }
                ) {
                    page_info {
                        start_cursor
                        end_cursor
                        has_next_page
                        has_previous_page
                    }
                    edges {
                        cursor
                        node {
                            hash
                            batch_position
                            kind
                            timestamp
                            level
                            block
                            id
                            source
                            gas
                            counter
                            gas_limit
                            storage_limit
                            public_key
                            amount
                            parameters
                            entrypoint
                            contract_address
                            destination
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                first_seen: '2018-09-13T14:51:20Z',
                activated: '2018-09-13T14:49:20.000Z',
                public_key: null,
                operations: {
                    page_info: {
                        start_cursor: 'opT4s5rWRbAEcbq6GZ272LyxNCxACqdk7c6Jm4buDcR6Fh4Zs3c:0',
                        end_cursor: 'onf5R7jCRJMfATspY8BB7ye4izQUVPzsBohqqNgsg8XTjqxReeU:0',
                        has_next_page: true,
                        has_previous_page: true,
                    },
                    edges: [
                        {
                            cursor: 'opT4s5rWRbAEcbq6GZ272LyxNCxACqdk7c6Jm4buDcR6Fh4Zs3c:0',
                            node: {
                                hash: 'opT4s5rWRbAEcbq6GZ272LyxNCxACqdk7c6Jm4buDcR6Fh4Zs3c',
                                batch_position: 0,
                                kind: 'transaction',
                                timestamp: '2019-06-23T00:44:47Z',
                                level: 491770,
                                block: 'BLyWtfcsLhVGAbsYJPSFpUQiPwdHkHExBrgB3WD9wesuo3PLfUV',
                                id: 10910203,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                gas: 1564,
                                counter: 'c8d0010000000000',
                                gas_limit: '4533000000000000',
                                storage_limit: '',
                                public_key: null,
                                amount: '0',
                                parameters: null,
                                entrypoint: null,
                                contract_address: null,
                                destination: 'KT1BRudFZEXLYANgmZTka1xCDN5nWTMWY7SZ',
                            },
                        },
                        {
                            cursor: 'ooUuE97rDNfvxm4tVy8CK8yHz6P3LdvFj5Sn41vw26hgRXT5YRr:1',
                            node: {
                                hash: 'ooUuE97rDNfvxm4tVy8CK8yHz6P3LdvFj5Sn41vw26hgRXT5YRr',
                                batch_position: 1,
                                kind: 'delegation',
                                timestamp: '2019-10-24T19:05:42Z',
                                level: 664258,
                                block: 'BKm7KPd6erxKgeHE4TTsqe78b5HJx33QTWa6rfNpSkt8PuzHkmf',
                                id: 15224994,
                                source: 'tz1RAWNEEFr2DSEDQVPnHXhH24GLMDF5XNjJ',
                                gas: 2500,
                                counter: '1f9e210000000000',
                                gas_limit: '1027000000000000',
                                storage_limit: '',
                                public_key: null,
                                amount: null,
                                parameters: null,
                                entrypoint: null,
                                contract_address: null,
                                destination: null,
                            },
                        },
                        {
                            cursor: 'onf5R7jCRJMfATspY8BB7ye4izQUVPzsBohqqNgsg8XTjqxReeU:0',
                            node: {
                                hash: 'onf5R7jCRJMfATspY8BB7ye4izQUVPzsBohqqNgsg8XTjqxReeU',
                                batch_position: 0,
                                kind: 'delegation',
                                timestamp: '2019-10-24T19:30:42Z',
                                level: 664283,
                                block: 'BM2WbVVx8AG1rdMcVNtiX9wFtrTFSk2BKrMhbHh1Z56gTwnzQs7',
                                id: 15225640,
                                source: 'tz1YPCK2iRxWZYHxeUtKBKRosdQ1SAHKeACe',
                                gas: 2500,
                                counter: '3be8200000000000',
                                gas_limit: '1027000000000000',
                                storage_limit: '',
                                public_key: null,
                                amount: null,
                                parameters: null,
                                entrypoint: null,
                                contract_address: null,
                                destination: null,
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.operations last and date_range lte and order_by and after', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS") {
                address
                first_seen
                activated
                public_key
                operations (
                last: 3
                order_by: {field: ID, direction: ASC}
                after: "ooUuE97rDNfvxm4tVy8CK8yHz6P3LdvFj5Sn41vw26hgRXT5YRr:1"
                date_range: {
                    lte: "2019-10-27T12:46:11Z"
                }
                ) {
                    page_info {
                        start_cursor
                        end_cursor
                        has_next_page
                        has_previous_page
                    }
                    edges {
                        cursor
                        node {
                            hash
                            batch_position
                            kind
                            timestamp
                            level
                            block
                            id
                            source
                            gas
                            counter
                            gas_limit
                            storage_limit
                            public_key
                            amount
                            parameters
                            entrypoint
                            contract_address
                            destination
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                first_seen: '2018-09-13T14:51:20Z',
                activated: '2018-09-13T14:49:20.000Z',
                public_key: null,
                operations: {
                    page_info: {
                        start_cursor: 'onf5R7jCRJMfATspY8BB7ye4izQUVPzsBohqqNgsg8XTjqxReeU:0',
                        end_cursor: 'ooADLykQuRbk3yUc5f465SRJhGyDtJDFit48WpCp9tipnDK399X:0',
                        has_next_page: false,
                        has_previous_page: true,
                    },
                    edges: [
                        {
                            cursor: 'onf5R7jCRJMfATspY8BB7ye4izQUVPzsBohqqNgsg8XTjqxReeU:0',
                            node: {
                                hash: 'onf5R7jCRJMfATspY8BB7ye4izQUVPzsBohqqNgsg8XTjqxReeU',
                                batch_position: 0,
                                kind: 'delegation',
                                timestamp: '2019-10-24T19:30:42Z',
                                level: 664283,
                                block: 'BM2WbVVx8AG1rdMcVNtiX9wFtrTFSk2BKrMhbHh1Z56gTwnzQs7',
                                id: 15225640,
                                source: 'tz1YPCK2iRxWZYHxeUtKBKRosdQ1SAHKeACe',
                                gas: 2500,
                                counter: '3be8200000000000',
                                gas_limit: '1027000000000000',
                                storage_limit: '',
                                public_key: null,
                                amount: null,
                                parameters: null,
                                entrypoint: null,
                                contract_address: null,
                                destination: null,
                            },
                        },
                        {
                            cursor: 'ooADLykQuRbk3yUc5f465SRJhGyDtJDFit48WpCp9tipnDK399X:0',
                            node: {
                                hash: 'ooADLykQuRbk3yUc5f465SRJhGyDtJDFit48WpCp9tipnDK399X',
                                batch_position: 0,
                                kind: 'transaction',
                                timestamp: '2019-10-24T20:38:46Z',
                                level: 664348,
                                block: 'BM8uvwRPXp7QtwUwcr2FtSsooUJcRbnN7hvDhUochXS8dpcXfQ8',
                                id: 15227316,
                                source: 'tz1KtGwriE7VuLwT3LwuvU9Nv4wAxP7XZ57d',
                                gas: 17920,
                                counter: 'e2fe010000000000',
                                gas_limit: 'fa58020000000000',
                                storage_limit: '2c01000000000000',
                                public_key: null,
                                amount: '10009000000',
                                parameters: '{ "prim": "Unit" }',
                                entrypoint: null,
                                contract_address: null,
                                destination: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.operations last and date_range gte/lte and order_by and before', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS") {
                address
                first_seen
                activated
                public_key
                operations (
                last: 3
                order_by: {field: ID, direction: ASC}
                before: "ooADLykQuRbk3yUc5f465SRJhGyDtJDFit48WpCp9tipnDK399X:0"
                date_range: {
                    gte: "2019-10-22T18:56:24Z"
                    lte: "2019-10-27T12:46:11Z"
                }
                ) {
                    page_info {
                        start_cursor
                        end_cursor
                        has_next_page
                        has_previous_page
                    }
                    edges {
                        cursor
                        node {
                            hash
                            batch_position
                            kind
                            timestamp
                            level
                            block
                            id
                            source
                            gas
                            counter
                            gas_limit
                            storage_limit
                            public_key
                            amount
                            parameters
                            entrypoint
                            contract_address
                            destination
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                first_seen: '2018-09-13T14:51:20Z',
                activated: '2018-09-13T14:49:20.000Z',
                public_key: null,
                operations: {
                    page_info: {
                        start_cursor: 'ooUuE97rDNfvxm4tVy8CK8yHz6P3LdvFj5Sn41vw26hgRXT5YRr:1',
                        end_cursor: 'onf5R7jCRJMfATspY8BB7ye4izQUVPzsBohqqNgsg8XTjqxReeU:0',
                        has_next_page: true,
                        has_previous_page: false,
                    },
                    edges: [
                        {
                            cursor: 'ooUuE97rDNfvxm4tVy8CK8yHz6P3LdvFj5Sn41vw26hgRXT5YRr:1',
                            node: {
                                hash: 'ooUuE97rDNfvxm4tVy8CK8yHz6P3LdvFj5Sn41vw26hgRXT5YRr',
                                batch_position: 1,
                                kind: 'delegation',
                                timestamp: '2019-10-24T19:05:42Z',
                                level: 664258,
                                block: 'BKm7KPd6erxKgeHE4TTsqe78b5HJx33QTWa6rfNpSkt8PuzHkmf',
                                id: 15224994,
                                source: 'tz1RAWNEEFr2DSEDQVPnHXhH24GLMDF5XNjJ',
                                gas: 2500,
                                counter: '1f9e210000000000',
                                gas_limit: '1027000000000000',
                                storage_limit: '',
                                public_key: null,
                                amount: null,
                                parameters: null,
                                entrypoint: null,
                                contract_address: null,
                                destination: null,
                            },
                        },
                        {
                            cursor: 'onf5R7jCRJMfATspY8BB7ye4izQUVPzsBohqqNgsg8XTjqxReeU:0',
                            node: {
                                hash: 'onf5R7jCRJMfATspY8BB7ye4izQUVPzsBohqqNgsg8XTjqxReeU',
                                batch_position: 0,
                                kind: 'delegation',
                                timestamp: '2019-10-24T19:30:42Z',
                                level: 664283,
                                block: 'BM2WbVVx8AG1rdMcVNtiX9wFtrTFSk2BKrMhbHh1Z56gTwnzQs7',
                                id: 15225640,
                                source: 'tz1YPCK2iRxWZYHxeUtKBKRosdQ1SAHKeACe',
                                gas: 2500,
                                counter: '3be8200000000000',
                                gas_limit: '1027000000000000',
                                storage_limit: '',
                                public_key: null,
                                amount: null,
                                parameters: null,
                                entrypoint: null,
                                contract_address: null,
                                destination: null,
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.operations last and date_range gte/lte and order_by and after', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS") {
                address
                first_seen
                activated
                public_key
                operations (
                last: 3
                order_by: {field: ID, direction: ASC}
                after: "ooUuE97rDNfvxm4tVy8CK8yHz6P3LdvFj5Sn41vw26hgRXT5YRr:1"
                date_range: {
                    gte: "2019-10-22T18:56:24Z"
                    lte: "2019-10-27T12:46:11Z"
                }
                ) {
                    page_info {
                        start_cursor
                        end_cursor
                        has_next_page
                        has_previous_page
                    }
                    edges {
                        cursor
                        node {
                            hash
                            batch_position
                            kind
                            timestamp
                            level
                            block
                            id
                            source
                            gas
                            counter
                            gas_limit
                            storage_limit
                            public_key
                            amount
                            parameters
                            entrypoint
                            contract_address
                            destination
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                first_seen: '2018-09-13T14:51:20Z',
                activated: '2018-09-13T14:49:20.000Z',
                public_key: null,
                operations: {
                    page_info: {
                        start_cursor: 'onf5R7jCRJMfATspY8BB7ye4izQUVPzsBohqqNgsg8XTjqxReeU:0',
                        end_cursor: 'ooADLykQuRbk3yUc5f465SRJhGyDtJDFit48WpCp9tipnDK399X:0',
                        has_next_page: false,
                        has_previous_page: true,
                    },
                    edges: [
                        {
                            cursor: 'onf5R7jCRJMfATspY8BB7ye4izQUVPzsBohqqNgsg8XTjqxReeU:0',
                            node: {
                                hash: 'onf5R7jCRJMfATspY8BB7ye4izQUVPzsBohqqNgsg8XTjqxReeU',
                                batch_position: 0,
                                kind: 'delegation',
                                timestamp: '2019-10-24T19:30:42Z',
                                level: 664283,
                                block: 'BM2WbVVx8AG1rdMcVNtiX9wFtrTFSk2BKrMhbHh1Z56gTwnzQs7',
                                id: 15225640,
                                source: 'tz1YPCK2iRxWZYHxeUtKBKRosdQ1SAHKeACe',
                                gas: 2500,
                                counter: '3be8200000000000',
                                gas_limit: '1027000000000000',
                                storage_limit: '',
                                public_key: null,
                                amount: null,
                                parameters: null,
                                entrypoint: null,
                                contract_address: null,
                                destination: null,
                            },
                        },
                        {
                            cursor: 'ooADLykQuRbk3yUc5f465SRJhGyDtJDFit48WpCp9tipnDK399X:0',
                            node: {
                                hash: 'ooADLykQuRbk3yUc5f465SRJhGyDtJDFit48WpCp9tipnDK399X',
                                batch_position: 0,
                                kind: 'transaction',
                                timestamp: '2019-10-24T20:38:46Z',
                                level: 664348,
                                block: 'BM8uvwRPXp7QtwUwcr2FtSsooUJcRbnN7hvDhUochXS8dpcXfQ8',
                                id: 15227316,
                                source: 'tz1KtGwriE7VuLwT3LwuvU9Nv4wAxP7XZ57d',
                                gas: 17920,
                                counter: 'e2fe010000000000',
                                gas_limit: 'fa58020000000000',
                                storage_limit: '2c01000000000000',
                                public_key: null,
                                amount: '10009000000',
                                parameters: '{ "prim": "Unit" }',
                                entrypoint: null,
                                contract_address: null,
                                destination: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.operations date_range gte/lte error', async () => {
        const GET_ACCOUNT = gql`
            query {
                account(address: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS") {
                    address
                    first_seen
                    activated
                    public_key
                    operations (
                    first: 3
                    date_range: {
                        gte: "2019-10-22T18:56:24Z"
                        lte: "2019-10-21T12:46:11Z"
                    }
                    ) {
                        page_info {
                            start_cursor
                            end_cursor
                            has_next_page
                            has_previous_page
                        }
                    }
                }
            }
            `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });
        if (res.errors == undefined) {
            throw new Error('Expected an error message however res.errors is undefined.');
        }

        expect(res.errors[0].message).toMatch('Argument "date" of type "DateRange?" must have the "lte" value greater than the "gte" value.');
    });

    test('account.operations query limit error', async () => {
        const GET_ACCOUNT = gql`
            query {
                account(address: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS") {
                    operations (
                    first: 201
                    ) {
                        page_info {
                            start_cursor
                            end_cursor
                            has_next_page
                            has_previous_page
                        }
                    }
                }
            }
            `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });
        if (res.errors == undefined) {
            throw new Error('Expected an error message however res.errors is undefined.');
        }

        expect(res.errors[0].message).toMatch('Argument \"limit\" of type \"Float?\" cannot be greater than 200.');
    });
});
