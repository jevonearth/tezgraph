import 'reflect-metadata';
import { ApolloServer } from 'apollo-server-express';
import { gql } from 'apollo-server';
import { createTestClient } from 'apollo-server-testing';
import { AccountResolver } from '../../../src/resolvers/account';
import { buildSchema } from 'type-graphql';

async function startTestServer() {
    const testServer = new ApolloServer({
        schema: await buildSchema({
            resolvers: [AccountResolver],
        }),
        context: ({ req, res }) => ({ req, res }),
    });

    return testServer;
}

const server = startTestServer();

let testClientQuery: Function;

async function startTestClient() {
    if (!testClientQuery) {
        const { query } = createTestClient(await server);
        testClientQuery = await query;
    }
}

describe('Account Resolvers', () => {
    test('account.delegations first', async () => {
        const GET_ACCOUNT = gql`
            query {
                account(address: "tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6") {
                    address
                    first_seen
                    activated
                    public_key
                    delegations (first: 3) {
                        page_info {
                            start_cursor
                            end_cursor
                            has_next_page
                            has_previous_page
                        }
                        edges {
                            cursor
                            node {
                                hash
                                batch_position
                                source
                                kind
                                timestamp
                                level
                                block
                                gas
                                counter
                                gas_limit
                                id
                                delegate
                            }
                        }
                    }
                }
            }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6',
                first_seen: '2018-09-17T09:23:51Z',
                activated: '2018-07-15T13:01:27.000Z',
                public_key: null,
                delegations: {
                    page_info: {
                        start_cursor: 'ooLzsB1PuLKqeGZznuz3YaFRiWD6G1TDKmAf4GqtVb4jnEbMoxg:1',
                        end_cursor: 'ooqZeZuSdWTeo1zdSP887RqurGVnUE7dF9ZqqQMyorppaT7KBuP:1',
                        has_next_page: true,
                        has_previous_page: false,
                    },
                    edges: [
                        {
                            cursor: 'ooLzsB1PuLKqeGZznuz3YaFRiWD6G1TDKmAf4GqtVb4jnEbMoxg:1',
                            node: {
                                hash: 'ooLzsB1PuLKqeGZznuz3YaFRiWD6G1TDKmAf4GqtVb4jnEbMoxg',
                                batch_position: 1,
                                source: 'tz1N73oapfHFiTzjv3UvKFnQY7Fp7CEUPqJk',
                                kind: 'delegation',
                                timestamp: '2020-09-08T14:35:40Z',
                                level: 1119602,
                                block: 'BLCzUbS7sUFLofeGzcwHGjoHHB8GYYR2X9msGzyZFss7Dni8JBw',
                                gas: 8738,
                                counter: '6584702',
                                gas_limit: '10600',
                                id: 27228347,
                                delegate: 'tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6',
                            },
                        },
                        {
                            cursor: 'ongTwy9dcZg35v84ur7rQcTgSA25Yj3rAm4acchPRutrQbLPrDX:1',
                            node: {
                                hash: 'ongTwy9dcZg35v84ur7rQcTgSA25Yj3rAm4acchPRutrQbLPrDX',
                                batch_position: 1,
                                source: 'tz1QAAxPbfTn5L9BbNsiUwjWDvMZQbAGgXND',
                                kind: 'delegation',
                                timestamp: '2019-10-27T12:46:11Z',
                                level: 668100,
                                block: 'BKyB7ypqUv2coNZLJ6StFxhRasJPwXwtrDycTVcRxGusAxPWWJs',
                                gas: 4335,
                                counter: '2219653',
                                gas_limit: '10600',
                                id: 15322110,
                                delegate: 'tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6',
                            },
                        },
                        {
                            cursor: 'ooqZeZuSdWTeo1zdSP887RqurGVnUE7dF9ZqqQMyorppaT7KBuP:1',
                            node: {
                                hash: 'ooqZeZuSdWTeo1zdSP887RqurGVnUE7dF9ZqqQMyorppaT7KBuP',
                                batch_position: 1,
                                source: 'tz1N5dH9DFu43oBD4Uu9d8jkGGatSRskqeGh',
                                kind: 'delegation',
                                timestamp: '2019-10-22T18:56:24Z',
                                level: 661451,
                                block: 'BMCgbqzH9uH48irmAkzrzf84LZfmLFNh3LVGGA3eWaEaDCR994x',
                                gas: 2720,
                                counter: '2186842',
                                gas_limit: '10600',
                                id: 15155828,
                                delegate: 'tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.delegations first and before', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6") {
                address
                first_seen
                activated
                public_key
                delegations (
                  first: 3
                  before: "ooqZeZuSdWTeo1zdSP887RqurGVnUE7dF9ZqqQMyorppaT7KBuP:1"
                ) {
                    page_info {
                        start_cursor
                        end_cursor
                        has_next_page
                        has_previous_page
                    }
                    edges {
                        cursor
                        node {
                            hash
                            batch_position
                            source
                            kind
                            timestamp
                            level
                            block
                            gas
                            counter
                            gas_limit
                            id
                            delegate
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6',
                first_seen: '2018-09-17T09:23:51Z',
                activated: '2018-07-15T13:01:27.000Z',
                public_key: null,
                delegations: {
                    page_info: {
                        start_cursor: 'ooLzsB1PuLKqeGZznuz3YaFRiWD6G1TDKmAf4GqtVb4jnEbMoxg:1',
                        end_cursor: 'ongTwy9dcZg35v84ur7rQcTgSA25Yj3rAm4acchPRutrQbLPrDX:1',
                        has_next_page: true,
                        has_previous_page: false,
                    },
                    edges: [
                        {
                            cursor: 'ooLzsB1PuLKqeGZznuz3YaFRiWD6G1TDKmAf4GqtVb4jnEbMoxg:1',
                            node: {
                                hash: 'ooLzsB1PuLKqeGZznuz3YaFRiWD6G1TDKmAf4GqtVb4jnEbMoxg',
                                batch_position: 1,
                                source: 'tz1N73oapfHFiTzjv3UvKFnQY7Fp7CEUPqJk',
                                kind: 'delegation',
                                timestamp: '2020-09-08T14:35:40Z',
                                level: 1119602,
                                block: 'BLCzUbS7sUFLofeGzcwHGjoHHB8GYYR2X9msGzyZFss7Dni8JBw',
                                gas: 8738,
                                counter: '6584702',
                                gas_limit: '10600',
                                id: 27228347,
                                delegate: 'tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6',
                            },
                        },
                        {
                            cursor: 'ongTwy9dcZg35v84ur7rQcTgSA25Yj3rAm4acchPRutrQbLPrDX:1',
                            node: {
                                hash: 'ongTwy9dcZg35v84ur7rQcTgSA25Yj3rAm4acchPRutrQbLPrDX',
                                batch_position: 1,
                                source: 'tz1QAAxPbfTn5L9BbNsiUwjWDvMZQbAGgXND',
                                kind: 'delegation',
                                timestamp: '2019-10-27T12:46:11Z',
                                level: 668100,
                                block: 'BKyB7ypqUv2coNZLJ6StFxhRasJPwXwtrDycTVcRxGusAxPWWJs',
                                gas: 4335,
                                counter: '2219653',
                                gas_limit: '10600',
                                id: 15322110,
                                delegate: 'tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.delegations first and after', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6") {
                address
                first_seen
                activated
                public_key
                delegations (
                  first: 3
                  after: "ooqZeZuSdWTeo1zdSP887RqurGVnUE7dF9ZqqQMyorppaT7KBuP:1"
                ) {
                    page_info {
                        start_cursor
                        end_cursor
                        has_next_page
                        has_previous_page
                    }
                    edges {
                        cursor
                        node {
                            hash
                            batch_position
                            source
                            kind
                            timestamp
                            level
                            block
                            gas
                            counter
                            gas_limit
                            id
                            delegate
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6',
                first_seen: '2018-09-17T09:23:51Z',
                activated: '2018-07-15T13:01:27.000Z',
                public_key: null,
                delegations: {
                    page_info: {
                        start_cursor: 'oomdibzcw4AVcouPFfiEtLdqaRjq3oHnSp5tSVndEUJrvtHmZ45:1',
                        end_cursor: 'oomdibzcw4AVcouPFfiEtLdqaRjq3oHnSp5tSVndEUJrvtHmZ45:1',
                        has_next_page: false,
                        has_previous_page: true,
                    },
                    edges: [
                        {
                            cursor: 'oomdibzcw4AVcouPFfiEtLdqaRjq3oHnSp5tSVndEUJrvtHmZ45:1',
                            node: {
                                hash: 'oomdibzcw4AVcouPFfiEtLdqaRjq3oHnSp5tSVndEUJrvtHmZ45',
                                batch_position: 1,
                                source: 'tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6',
                                kind: 'delegation',
                                timestamp: '2018-07-24T17:27:27Z',
                                level: 32476,
                                block: 'BMZSMuqeweWSmT4JgrF5k3BS8RK3ynuach2Z5oSLHQ7X3BqtG7s',
                                gas: 50000,
                                counter: '33527',
                                gas_limit: '0',
                                id: 199112,
                                delegate: 'tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.delegations first and order_by', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6") {
                address
                first_seen
                activated
                public_key
                delegations (
                  first: 3
                  order_by: {field: ID, direction: ASC}
                ) {
                    page_info {
                        start_cursor
                        end_cursor
                        has_next_page
                        has_previous_page
                    }
                    edges {
                        cursor
                        node {
                            hash
                            batch_position
                            source
                            kind
                            timestamp
                            level
                            block
                            gas
                            counter
                            gas_limit
                            id
                            delegate
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6',
                first_seen: '2018-09-17T09:23:51Z',
                activated: '2018-07-15T13:01:27.000Z',
                public_key: null,
                delegations: {
                    page_info: {
                        start_cursor: 'oomdibzcw4AVcouPFfiEtLdqaRjq3oHnSp5tSVndEUJrvtHmZ45:1',
                        end_cursor: 'ongTwy9dcZg35v84ur7rQcTgSA25Yj3rAm4acchPRutrQbLPrDX:1',
                        has_next_page: true,
                        has_previous_page: false,
                    },
                    edges: [
                        {
                            cursor: 'oomdibzcw4AVcouPFfiEtLdqaRjq3oHnSp5tSVndEUJrvtHmZ45:1',
                            node: {
                                hash: 'oomdibzcw4AVcouPFfiEtLdqaRjq3oHnSp5tSVndEUJrvtHmZ45',
                                batch_position: 1,
                                source: 'tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6',
                                kind: 'delegation',
                                timestamp: '2018-07-24T17:27:27Z',
                                level: 32476,
                                block: 'BMZSMuqeweWSmT4JgrF5k3BS8RK3ynuach2Z5oSLHQ7X3BqtG7s',
                                gas: 50000,
                                counter: '33527',
                                gas_limit: '0',
                                id: 199112,
                                delegate: 'tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6',
                            },
                        },
                        {
                            cursor: 'ooqZeZuSdWTeo1zdSP887RqurGVnUE7dF9ZqqQMyorppaT7KBuP:1',
                            node: {
                                hash: 'ooqZeZuSdWTeo1zdSP887RqurGVnUE7dF9ZqqQMyorppaT7KBuP',
                                batch_position: 1,
                                source: 'tz1N5dH9DFu43oBD4Uu9d8jkGGatSRskqeGh',
                                kind: 'delegation',
                                timestamp: '2019-10-22T18:56:24Z',
                                level: 661451,
                                block: 'BMCgbqzH9uH48irmAkzrzf84LZfmLFNh3LVGGA3eWaEaDCR994x',
                                gas: 2720,
                                counter: '2186842',
                                gas_limit: '10600',
                                id: 15155828,
                                delegate: 'tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6',
                            },
                        },
                        {
                            cursor: 'ongTwy9dcZg35v84ur7rQcTgSA25Yj3rAm4acchPRutrQbLPrDX:1',
                            node: {
                                hash: 'ongTwy9dcZg35v84ur7rQcTgSA25Yj3rAm4acchPRutrQbLPrDX',
                                batch_position: 1,
                                source: 'tz1QAAxPbfTn5L9BbNsiUwjWDvMZQbAGgXND',
                                kind: 'delegation',
                                timestamp: '2019-10-27T12:46:11Z',
                                level: 668100,
                                block: 'BKyB7ypqUv2coNZLJ6StFxhRasJPwXwtrDycTVcRxGusAxPWWJs',
                                gas: 4335,
                                counter: '2219653',
                                gas_limit: '10600',
                                id: 15322110,
                                delegate: 'tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.delegations first and order_by and before', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6") {
                address
                first_seen
                activated
                public_key
                delegations (
                  first: 3
                  order_by: {field: ID, direction: ASC}
                  before:"ooqZeZuSdWTeo1zdSP887RqurGVnUE7dF9ZqqQMyorppaT7KBuP:1"
                ) {
                    page_info {
                        start_cursor
                        end_cursor
                        has_next_page
                        has_previous_page
                    }
                    edges {
                        cursor
                        node {
                            hash
                            batch_position
                            source
                            kind
                            timestamp
                            level
                            block
                            gas
                            counter
                            gas_limit
                            id
                            delegate
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6',
                first_seen: '2018-09-17T09:23:51Z',
                activated: '2018-07-15T13:01:27.000Z',
                public_key: null,
                delegations: {
                    page_info: {
                        start_cursor: 'oomdibzcw4AVcouPFfiEtLdqaRjq3oHnSp5tSVndEUJrvtHmZ45:1',
                        end_cursor: 'oomdibzcw4AVcouPFfiEtLdqaRjq3oHnSp5tSVndEUJrvtHmZ45:1',
                        has_next_page: true,
                        has_previous_page: false,
                    },
                    edges: [
                        {
                            cursor: 'oomdibzcw4AVcouPFfiEtLdqaRjq3oHnSp5tSVndEUJrvtHmZ45:1',
                            node: {
                                hash: 'oomdibzcw4AVcouPFfiEtLdqaRjq3oHnSp5tSVndEUJrvtHmZ45',
                                batch_position: 1,
                                source: 'tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6',
                                kind: 'delegation',
                                timestamp: '2018-07-24T17:27:27Z',
                                level: 32476,
                                block: 'BMZSMuqeweWSmT4JgrF5k3BS8RK3ynuach2Z5oSLHQ7X3BqtG7s',
                                gas: 50000,
                                counter: '33527',
                                gas_limit: '0',
                                id: 199112,
                                delegate: 'tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.delegations first and order_by and after', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6") {
                address
                first_seen
                activated
                public_key
                delegations (
                  first: 3
                  order_by: {field: ID, direction: ASC}
                  after:"ooqZeZuSdWTeo1zdSP887RqurGVnUE7dF9ZqqQMyorppaT7KBuP:1"
                ) {
                    page_info {
                        start_cursor
                        end_cursor
                        has_next_page
                        has_previous_page
                    }
                    edges {
                        cursor
                        node {
                            hash
                            batch_position
                            source
                            kind
                            timestamp
                            level
                            block
                            gas
                            counter
                            gas_limit
                            id
                            delegate
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6',
                first_seen: '2018-09-17T09:23:51Z',
                activated: '2018-07-15T13:01:27.000Z',
                public_key: null,
                delegations: {
                    page_info: {
                        start_cursor: 'ongTwy9dcZg35v84ur7rQcTgSA25Yj3rAm4acchPRutrQbLPrDX:1',
                        end_cursor: 'ooLzsB1PuLKqeGZznuz3YaFRiWD6G1TDKmAf4GqtVb4jnEbMoxg:1',
                        has_next_page: false,
                        has_previous_page: true,
                    },
                    edges: [
                        {
                            cursor: 'ongTwy9dcZg35v84ur7rQcTgSA25Yj3rAm4acchPRutrQbLPrDX:1',
                            node: {
                                hash: 'ongTwy9dcZg35v84ur7rQcTgSA25Yj3rAm4acchPRutrQbLPrDX',
                                batch_position: 1,
                                source: 'tz1QAAxPbfTn5L9BbNsiUwjWDvMZQbAGgXND',
                                kind: 'delegation',
                                timestamp: '2019-10-27T12:46:11Z',
                                level: 668100,
                                block: 'BKyB7ypqUv2coNZLJ6StFxhRasJPwXwtrDycTVcRxGusAxPWWJs',
                                gas: 4335,
                                counter: '2219653',
                                gas_limit: '10600',
                                id: 15322110,
                                delegate: 'tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6',
                            },
                        },
                        {
                            cursor: 'ooLzsB1PuLKqeGZznuz3YaFRiWD6G1TDKmAf4GqtVb4jnEbMoxg:1',
                            node: {
                                hash: 'ooLzsB1PuLKqeGZznuz3YaFRiWD6G1TDKmAf4GqtVb4jnEbMoxg',
                                batch_position: 1,
                                source: 'tz1N73oapfHFiTzjv3UvKFnQY7Fp7CEUPqJk',
                                kind: 'delegation',
                                timestamp: '2020-09-08T14:35:40Z',
                                level: 1119602,
                                block: 'BLCzUbS7sUFLofeGzcwHGjoHHB8GYYR2X9msGzyZFss7Dni8JBw',
                                gas: 8738,
                                counter: '6584702',
                                gas_limit: '10600',
                                id: 27228347,
                                delegate: 'tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.delegations first and date_range gte', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6") {
                address
                first_seen
                activated
                public_key
                delegations (
                  first: 3
                  date_range: {
                    gte: "2019-10-22T18:56:24Z"
                  }
                ) {
                    page_info {
                        start_cursor
                        end_cursor
                        has_next_page
                        has_previous_page
                    }
                    edges {
                        cursor
                        node {
                            hash
                            batch_position
                            source
                            kind
                            timestamp
                            level
                            block
                            gas
                            counter
                            gas_limit
                            id
                            delegate
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6',
                first_seen: '2018-09-17T09:23:51Z',
                activated: '2018-07-15T13:01:27.000Z',
                public_key: null,
                delegations: {
                    page_info: {
                        start_cursor: 'ooLzsB1PuLKqeGZznuz3YaFRiWD6G1TDKmAf4GqtVb4jnEbMoxg:1',
                        end_cursor: 'ooqZeZuSdWTeo1zdSP887RqurGVnUE7dF9ZqqQMyorppaT7KBuP:1',
                        has_next_page: false,
                        has_previous_page: false,
                    },
                    edges: [
                        {
                            cursor: 'ooLzsB1PuLKqeGZznuz3YaFRiWD6G1TDKmAf4GqtVb4jnEbMoxg:1',
                            node: {
                                hash: 'ooLzsB1PuLKqeGZznuz3YaFRiWD6G1TDKmAf4GqtVb4jnEbMoxg',
                                batch_position: 1,
                                source: 'tz1N73oapfHFiTzjv3UvKFnQY7Fp7CEUPqJk',
                                kind: 'delegation',
                                timestamp: '2020-09-08T14:35:40Z',
                                level: 1119602,
                                block: 'BLCzUbS7sUFLofeGzcwHGjoHHB8GYYR2X9msGzyZFss7Dni8JBw',
                                gas: 8738,
                                counter: '6584702',
                                gas_limit: '10600',
                                id: 27228347,
                                delegate: 'tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6',
                            },
                        },
                        {
                            cursor: 'ongTwy9dcZg35v84ur7rQcTgSA25Yj3rAm4acchPRutrQbLPrDX:1',
                            node: {
                                hash: 'ongTwy9dcZg35v84ur7rQcTgSA25Yj3rAm4acchPRutrQbLPrDX',
                                batch_position: 1,
                                source: 'tz1QAAxPbfTn5L9BbNsiUwjWDvMZQbAGgXND',
                                kind: 'delegation',
                                timestamp: '2019-10-27T12:46:11Z',
                                level: 668100,
                                block: 'BKyB7ypqUv2coNZLJ6StFxhRasJPwXwtrDycTVcRxGusAxPWWJs',
                                gas: 4335,
                                counter: '2219653',
                                gas_limit: '10600',
                                id: 15322110,
                                delegate: 'tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6',
                            },
                        },
                        {
                            cursor: 'ooqZeZuSdWTeo1zdSP887RqurGVnUE7dF9ZqqQMyorppaT7KBuP:1',
                            node: {
                                hash: 'ooqZeZuSdWTeo1zdSP887RqurGVnUE7dF9ZqqQMyorppaT7KBuP',
                                batch_position: 1,
                                source: 'tz1N5dH9DFu43oBD4Uu9d8jkGGatSRskqeGh',
                                kind: 'delegation',
                                timestamp: '2019-10-22T18:56:24Z',
                                level: 661451,
                                block: 'BMCgbqzH9uH48irmAkzrzf84LZfmLFNh3LVGGA3eWaEaDCR994x',
                                gas: 2720,
                                counter: '2186842',
                                gas_limit: '10600',
                                id: 15155828,
                                delegate: 'tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.delegations first and date_range gte and before', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6") {
                address
                first_seen
                activated
                public_key
                delegations (
                  first: 3
                  date_range: {
                    gte: "2019-10-22T18:56:24Z"
                  }
                  before:"ongTwy9dcZg35v84ur7rQcTgSA25Yj3rAm4acchPRutrQbLPrDX:1"
                ) {
                    page_info {
                        start_cursor
                        end_cursor
                        has_next_page
                        has_previous_page
                    }
                    edges {
                        cursor
                        node {
                            hash
                            batch_position
                            source
                            kind
                            timestamp
                            level
                            block
                            gas
                            counter
                            gas_limit
                            id
                            delegate
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6',
                first_seen: '2018-09-17T09:23:51Z',
                activated: '2018-07-15T13:01:27.000Z',
                public_key: null,
                delegations: {
                    page_info: {
                        start_cursor: 'ooLzsB1PuLKqeGZznuz3YaFRiWD6G1TDKmAf4GqtVb4jnEbMoxg:1',
                        end_cursor: 'ooLzsB1PuLKqeGZznuz3YaFRiWD6G1TDKmAf4GqtVb4jnEbMoxg:1',
                        has_next_page: true,
                        has_previous_page: false,
                    },
                    edges: [
                        {
                            cursor: 'ooLzsB1PuLKqeGZznuz3YaFRiWD6G1TDKmAf4GqtVb4jnEbMoxg:1',
                            node: {
                                hash: 'ooLzsB1PuLKqeGZznuz3YaFRiWD6G1TDKmAf4GqtVb4jnEbMoxg',
                                batch_position: 1,
                                source: 'tz1N73oapfHFiTzjv3UvKFnQY7Fp7CEUPqJk',
                                kind: 'delegation',
                                timestamp: '2020-09-08T14:35:40Z',
                                level: 1119602,
                                block: 'BLCzUbS7sUFLofeGzcwHGjoHHB8GYYR2X9msGzyZFss7Dni8JBw',
                                gas: 8738,
                                counter: '6584702',
                                gas_limit: '10600',
                                id: 27228347,
                                delegate: 'tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.delegations first and date_range gte and after', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6") {
                address
                first_seen
                activated
                public_key
                delegations (
                  first: 3
                  date_range: {
                    gte: "2019-10-22T18:56:24Z"
                  }
                  after:"ongTwy9dcZg35v84ur7rQcTgSA25Yj3rAm4acchPRutrQbLPrDX:1"
                ) {
                    page_info {
                        start_cursor
                        end_cursor
                        has_next_page
                        has_previous_page
                    }
                    edges {
                        cursor
                        node {
                            hash
                            batch_position
                            source
                            kind
                            timestamp
                            level
                            block
                            gas
                            counter
                            gas_limit
                            id
                            delegate
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6',
                first_seen: '2018-09-17T09:23:51Z',
                activated: '2018-07-15T13:01:27.000Z',
                public_key: null,
                delegations: {
                    page_info: {
                        start_cursor: 'ooqZeZuSdWTeo1zdSP887RqurGVnUE7dF9ZqqQMyorppaT7KBuP:1',
                        end_cursor: 'ooqZeZuSdWTeo1zdSP887RqurGVnUE7dF9ZqqQMyorppaT7KBuP:1',
                        has_next_page: false,
                        has_previous_page: true,
                    },
                    edges: [
                        {
                            cursor: 'ooqZeZuSdWTeo1zdSP887RqurGVnUE7dF9ZqqQMyorppaT7KBuP:1',
                            node: {
                                hash: 'ooqZeZuSdWTeo1zdSP887RqurGVnUE7dF9ZqqQMyorppaT7KBuP',
                                batch_position: 1,
                                source: 'tz1N5dH9DFu43oBD4Uu9d8jkGGatSRskqeGh',
                                kind: 'delegation',
                                timestamp: '2019-10-22T18:56:24Z',
                                level: 661451,
                                block: 'BMCgbqzH9uH48irmAkzrzf84LZfmLFNh3LVGGA3eWaEaDCR994x',
                                gas: 2720,
                                counter: '2186842',
                                gas_limit: '10600',
                                id: 15155828,
                                delegate: 'tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.delegations first and date_range gte and order_by', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6") {
                address
                first_seen
                activated
                public_key
                delegations (
                    first: 3
                    date_range: {
                    gte: "2019-10-22T18:56:24Z"
                    }
                    order_by: {field: ID, direction: ASC}
                ) {
                    page_info {
                        start_cursor
                        end_cursor
                        has_next_page
                        has_previous_page
                    }
                    edges {
                        cursor
                        node {
                            hash
                            batch_position
                            source
                            kind
                            timestamp
                            level
                            block
                            gas
                            counter
                            gas_limit
                            id
                            delegate
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6',
                first_seen: '2018-09-17T09:23:51Z',
                activated: '2018-07-15T13:01:27.000Z',
                public_key: null,
                delegations: {
                    page_info: {
                        start_cursor: 'ooqZeZuSdWTeo1zdSP887RqurGVnUE7dF9ZqqQMyorppaT7KBuP:1',
                        end_cursor: 'ooLzsB1PuLKqeGZznuz3YaFRiWD6G1TDKmAf4GqtVb4jnEbMoxg:1',
                        has_next_page: false,
                        has_previous_page: false,
                    },
                    edges: [
                        {
                            cursor: 'ooqZeZuSdWTeo1zdSP887RqurGVnUE7dF9ZqqQMyorppaT7KBuP:1',
                            node: {
                                hash: 'ooqZeZuSdWTeo1zdSP887RqurGVnUE7dF9ZqqQMyorppaT7KBuP',
                                batch_position: 1,
                                source: 'tz1N5dH9DFu43oBD4Uu9d8jkGGatSRskqeGh',
                                kind: 'delegation',
                                timestamp: '2019-10-22T18:56:24Z',
                                level: 661451,
                                block: 'BMCgbqzH9uH48irmAkzrzf84LZfmLFNh3LVGGA3eWaEaDCR994x',
                                gas: 2720,
                                counter: '2186842',
                                gas_limit: '10600',
                                id: 15155828,
                                delegate: 'tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6',
                            },
                        },
                        {
                            cursor: 'ongTwy9dcZg35v84ur7rQcTgSA25Yj3rAm4acchPRutrQbLPrDX:1',
                            node: {
                                hash: 'ongTwy9dcZg35v84ur7rQcTgSA25Yj3rAm4acchPRutrQbLPrDX',
                                batch_position: 1,
                                source: 'tz1QAAxPbfTn5L9BbNsiUwjWDvMZQbAGgXND',
                                kind: 'delegation',
                                timestamp: '2019-10-27T12:46:11Z',
                                level: 668100,
                                block: 'BKyB7ypqUv2coNZLJ6StFxhRasJPwXwtrDycTVcRxGusAxPWWJs',
                                gas: 4335,
                                counter: '2219653',
                                gas_limit: '10600',
                                id: 15322110,
                                delegate: 'tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6',
                            },
                        },
                        {
                            cursor: 'ooLzsB1PuLKqeGZznuz3YaFRiWD6G1TDKmAf4GqtVb4jnEbMoxg:1',
                            node: {
                                hash: 'ooLzsB1PuLKqeGZznuz3YaFRiWD6G1TDKmAf4GqtVb4jnEbMoxg',
                                batch_position: 1,
                                source: 'tz1N73oapfHFiTzjv3UvKFnQY7Fp7CEUPqJk',
                                kind: 'delegation',
                                timestamp: '2020-09-08T14:35:40Z',
                                level: 1119602,
                                block: 'BLCzUbS7sUFLofeGzcwHGjoHHB8GYYR2X9msGzyZFss7Dni8JBw',
                                gas: 8738,
                                counter: '6584702',
                                gas_limit: '10600',
                                id: 27228347,
                                delegate: 'tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.delegations first and date_range lte', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6") {
                address
                first_seen
                activated
                public_key
                delegations (
                  first: 3
                  date_range: {
                    lte: "2019-10-22T18:56:24Z"
                  }
                ) {
                    page_info {
                        start_cursor
                        end_cursor
                        has_next_page
                        has_previous_page
                    }
                    edges {
                        cursor
                        node {
                            hash
                            batch_position
                            source
                            kind
                            timestamp
                            level
                            block
                            gas
                            counter
                            gas_limit
                            id
                            delegate
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6',
                first_seen: '2018-09-17T09:23:51Z',
                activated: '2018-07-15T13:01:27.000Z',
                public_key: null,
                delegations: {
                    page_info: {
                        start_cursor: 'ooqZeZuSdWTeo1zdSP887RqurGVnUE7dF9ZqqQMyorppaT7KBuP:1',
                        end_cursor: 'oomdibzcw4AVcouPFfiEtLdqaRjq3oHnSp5tSVndEUJrvtHmZ45:1',
                        has_next_page: false,
                        has_previous_page: false,
                    },
                    edges: [
                        {
                            cursor: 'ooqZeZuSdWTeo1zdSP887RqurGVnUE7dF9ZqqQMyorppaT7KBuP:1',
                            node: {
                                hash: 'ooqZeZuSdWTeo1zdSP887RqurGVnUE7dF9ZqqQMyorppaT7KBuP',
                                batch_position: 1,
                                source: 'tz1N5dH9DFu43oBD4Uu9d8jkGGatSRskqeGh',
                                kind: 'delegation',
                                timestamp: '2019-10-22T18:56:24Z',
                                level: 661451,
                                block: 'BMCgbqzH9uH48irmAkzrzf84LZfmLFNh3LVGGA3eWaEaDCR994x',
                                gas: 2720,
                                counter: '2186842',
                                gas_limit: '10600',
                                id: 15155828,
                                delegate: 'tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6',
                            },
                        },
                        {
                            cursor: 'oomdibzcw4AVcouPFfiEtLdqaRjq3oHnSp5tSVndEUJrvtHmZ45:1',
                            node: {
                                hash: 'oomdibzcw4AVcouPFfiEtLdqaRjq3oHnSp5tSVndEUJrvtHmZ45',
                                batch_position: 1,
                                source: 'tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6',
                                kind: 'delegation',
                                timestamp: '2018-07-24T17:27:27Z',
                                level: 32476,
                                block: 'BMZSMuqeweWSmT4JgrF5k3BS8RK3ynuach2Z5oSLHQ7X3BqtG7s',
                                gas: 50000,
                                counter: '33527',
                                gas_limit: '0',
                                id: 199112,
                                delegate: 'tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.delegations first and date_range lte and before', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6") {
                address
                first_seen
                activated
                public_key
                delegations (
                  first: 3
                  date_range: {
                    lte: "2019-10-22T18:56:24Z"
                  }
                  before: "oomdibzcw4AVcouPFfiEtLdqaRjq3oHnSp5tSVndEUJrvtHmZ45:1"
                ) {
                    page_info {
                        start_cursor
                        end_cursor
                        has_next_page
                        has_previous_page
                    }
                    edges {
                        cursor
                        node {
                            hash
                            batch_position
                            source
                            kind
                            timestamp
                            level
                            block
                            gas
                            counter
                            gas_limit
                            id
                            delegate
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6',
                first_seen: '2018-09-17T09:23:51Z',
                activated: '2018-07-15T13:01:27.000Z',
                public_key: null,
                delegations: {
                    page_info: {
                        start_cursor: 'ooqZeZuSdWTeo1zdSP887RqurGVnUE7dF9ZqqQMyorppaT7KBuP:1',
                        end_cursor: 'ooqZeZuSdWTeo1zdSP887RqurGVnUE7dF9ZqqQMyorppaT7KBuP:1',
                        has_next_page: true,
                        has_previous_page: false,
                    },
                    edges: [
                        {
                            cursor: 'ooqZeZuSdWTeo1zdSP887RqurGVnUE7dF9ZqqQMyorppaT7KBuP:1',
                            node: {
                                hash: 'ooqZeZuSdWTeo1zdSP887RqurGVnUE7dF9ZqqQMyorppaT7KBuP',
                                batch_position: 1,
                                source: 'tz1N5dH9DFu43oBD4Uu9d8jkGGatSRskqeGh',
                                kind: 'delegation',
                                timestamp: '2019-10-22T18:56:24Z',
                                level: 661451,
                                block: 'BMCgbqzH9uH48irmAkzrzf84LZfmLFNh3LVGGA3eWaEaDCR994x',
                                gas: 2720,
                                counter: '2186842',
                                gas_limit: '10600',
                                id: 15155828,
                                delegate: 'tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.delegations first and date_range lte and after', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6") {
                address
                first_seen
                activated
                public_key
                delegations (
                  first: 3
                  date_range: {
                    lte: "2019-10-22T18:56:24Z"
                  }
                  after: "ooqZeZuSdWTeo1zdSP887RqurGVnUE7dF9ZqqQMyorppaT7KBuP:1"
                ) {
                    page_info {
                        start_cursor
                        end_cursor
                        has_next_page
                        has_previous_page
                    }
                    edges {
                        cursor
                        node {
                            hash
                            batch_position
                            source
                            kind
                            timestamp
                            level
                            block
                            gas
                            counter
                            gas_limit
                            id
                            delegate
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6',
                first_seen: '2018-09-17T09:23:51Z',
                activated: '2018-07-15T13:01:27.000Z',
                public_key: null,
                delegations: {
                    page_info: {
                        start_cursor: 'oomdibzcw4AVcouPFfiEtLdqaRjq3oHnSp5tSVndEUJrvtHmZ45:1',
                        end_cursor: 'oomdibzcw4AVcouPFfiEtLdqaRjq3oHnSp5tSVndEUJrvtHmZ45:1',
                        has_next_page: false,
                        has_previous_page: true,
                    },
                    edges: [
                        {
                            cursor: 'oomdibzcw4AVcouPFfiEtLdqaRjq3oHnSp5tSVndEUJrvtHmZ45:1',
                            node: {
                                hash: 'oomdibzcw4AVcouPFfiEtLdqaRjq3oHnSp5tSVndEUJrvtHmZ45',
                                batch_position: 1,
                                source: 'tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6',
                                kind: 'delegation',
                                timestamp: '2018-07-24T17:27:27Z',
                                level: 32476,
                                block: 'BMZSMuqeweWSmT4JgrF5k3BS8RK3ynuach2Z5oSLHQ7X3BqtG7s',
                                gas: 50000,
                                counter: '33527',
                                gas_limit: '0',
                                id: 199112,
                                delegate: 'tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.delegations first and date_range lte and order_by', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6") {
                address
                first_seen
                activated
                public_key
                delegations (
                    first: 3
                    date_range: {
                    lte: "2019-10-22T18:56:24Z"
                    }
                    order_by: {field: ID, direction: ASC}
                ) {
                    page_info {
                        start_cursor
                        end_cursor
                        has_next_page
                        has_previous_page
                    }
                    edges {
                        cursor
                        node {
                            hash
                            batch_position
                            source
                            kind
                            timestamp
                            level
                            block
                            gas
                            counter
                            gas_limit
                            id
                            delegate
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6',
                first_seen: '2018-09-17T09:23:51Z',
                activated: '2018-07-15T13:01:27.000Z',
                public_key: null,
                delegations: {
                    page_info: {
                        start_cursor: 'oomdibzcw4AVcouPFfiEtLdqaRjq3oHnSp5tSVndEUJrvtHmZ45:1',
                        end_cursor: 'ooqZeZuSdWTeo1zdSP887RqurGVnUE7dF9ZqqQMyorppaT7KBuP:1',
                        has_next_page: false,
                        has_previous_page: false,
                    },
                    edges: [
                        {
                            cursor: 'oomdibzcw4AVcouPFfiEtLdqaRjq3oHnSp5tSVndEUJrvtHmZ45:1',
                            node: {
                                hash: 'oomdibzcw4AVcouPFfiEtLdqaRjq3oHnSp5tSVndEUJrvtHmZ45',
                                batch_position: 1,
                                source: 'tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6',
                                kind: 'delegation',
                                timestamp: '2018-07-24T17:27:27Z',
                                level: 32476,
                                block: 'BMZSMuqeweWSmT4JgrF5k3BS8RK3ynuach2Z5oSLHQ7X3BqtG7s',
                                gas: 50000,
                                counter: '33527',
                                gas_limit: '0',
                                id: 199112,
                                delegate: 'tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6',
                            },
                        },
                        {
                            cursor: 'ooqZeZuSdWTeo1zdSP887RqurGVnUE7dF9ZqqQMyorppaT7KBuP:1',
                            node: {
                                hash: 'ooqZeZuSdWTeo1zdSP887RqurGVnUE7dF9ZqqQMyorppaT7KBuP',
                                batch_position: 1,
                                source: 'tz1N5dH9DFu43oBD4Uu9d8jkGGatSRskqeGh',
                                kind: 'delegation',
                                timestamp: '2019-10-22T18:56:24Z',
                                level: 661451,
                                block: 'BMCgbqzH9uH48irmAkzrzf84LZfmLFNh3LVGGA3eWaEaDCR994x',
                                gas: 2720,
                                counter: '2186842',
                                gas_limit: '10600',
                                id: 15155828,
                                delegate: 'tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.delegations first and date_range gte/lte', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6") {
                address
                first_seen
                activated
                public_key
                delegations (
                  first: 3
                  date_range: {
                    gte: "2019-10-22T18:56:24Z"
                    lte: "2019-10-27T12:46:11Z"
                  }
                ) {
                    page_info {
                        start_cursor
                        end_cursor
                        has_next_page
                        has_previous_page
                    }
                    edges {
                        cursor
                        node {
                            hash
                            batch_position
                            source
                            kind
                            timestamp
                            level
                            block
                            gas
                            counter
                            gas_limit
                            id
                            delegate
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6',
                first_seen: '2018-09-17T09:23:51Z',
                activated: '2018-07-15T13:01:27.000Z',
                public_key: null,
                delegations: {
                    page_info: {
                        start_cursor: 'ongTwy9dcZg35v84ur7rQcTgSA25Yj3rAm4acchPRutrQbLPrDX:1',
                        end_cursor: 'ooqZeZuSdWTeo1zdSP887RqurGVnUE7dF9ZqqQMyorppaT7KBuP:1',
                        has_next_page: false,
                        has_previous_page: false,
                    },
                    edges: [
                        {
                            cursor: 'ongTwy9dcZg35v84ur7rQcTgSA25Yj3rAm4acchPRutrQbLPrDX:1',
                            node: {
                                hash: 'ongTwy9dcZg35v84ur7rQcTgSA25Yj3rAm4acchPRutrQbLPrDX',
                                batch_position: 1,
                                source: 'tz1QAAxPbfTn5L9BbNsiUwjWDvMZQbAGgXND',
                                kind: 'delegation',
                                timestamp: '2019-10-27T12:46:11Z',
                                level: 668100,
                                block: 'BKyB7ypqUv2coNZLJ6StFxhRasJPwXwtrDycTVcRxGusAxPWWJs',
                                gas: 4335,
                                counter: '2219653',
                                gas_limit: '10600',
                                id: 15322110,
                                delegate: 'tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6',
                            },
                        },
                        {
                            cursor: 'ooqZeZuSdWTeo1zdSP887RqurGVnUE7dF9ZqqQMyorppaT7KBuP:1',
                            node: {
                                hash: 'ooqZeZuSdWTeo1zdSP887RqurGVnUE7dF9ZqqQMyorppaT7KBuP',
                                batch_position: 1,
                                source: 'tz1N5dH9DFu43oBD4Uu9d8jkGGatSRskqeGh',
                                kind: 'delegation',
                                timestamp: '2019-10-22T18:56:24Z',
                                level: 661451,
                                block: 'BMCgbqzH9uH48irmAkzrzf84LZfmLFNh3LVGGA3eWaEaDCR994x',
                                gas: 2720,
                                counter: '2186842',
                                gas_limit: '10600',
                                id: 15155828,
                                delegate: 'tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.delegations first and date_range gte/lte and before', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6") {
                address
                first_seen
                activated
                public_key
                delegations (
                  first: 3
                  date_range: {
                    gte: "2019-10-22T18:56:24Z"
                    lte: "2019-10-27T12:46:11Z"
                  }
                  before: "ooqZeZuSdWTeo1zdSP887RqurGVnUE7dF9ZqqQMyorppaT7KBuP:1"
                ) {
                    page_info {
                        start_cursor
                        end_cursor
                        has_next_page
                        has_previous_page
                    }
                    edges {
                        cursor
                        node {
                            hash
                            batch_position
                            source
                            kind
                            timestamp
                            level
                            block
                            gas
                            counter
                            gas_limit
                            id
                            delegate
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6',
                first_seen: '2018-09-17T09:23:51Z',
                activated: '2018-07-15T13:01:27.000Z',
                public_key: null,
                delegations: {
                    page_info: {
                        start_cursor: 'ongTwy9dcZg35v84ur7rQcTgSA25Yj3rAm4acchPRutrQbLPrDX:1',
                        end_cursor: 'ongTwy9dcZg35v84ur7rQcTgSA25Yj3rAm4acchPRutrQbLPrDX:1',
                        has_next_page: true,
                        has_previous_page: false,
                    },
                    edges: [
                        {
                            cursor: 'ongTwy9dcZg35v84ur7rQcTgSA25Yj3rAm4acchPRutrQbLPrDX:1',
                            node: {
                                hash: 'ongTwy9dcZg35v84ur7rQcTgSA25Yj3rAm4acchPRutrQbLPrDX',
                                batch_position: 1,
                                source: 'tz1QAAxPbfTn5L9BbNsiUwjWDvMZQbAGgXND',
                                kind: 'delegation',
                                timestamp: '2019-10-27T12:46:11Z',
                                level: 668100,
                                block: 'BKyB7ypqUv2coNZLJ6StFxhRasJPwXwtrDycTVcRxGusAxPWWJs',
                                gas: 4335,
                                counter: '2219653',
                                gas_limit: '10600',
                                id: 15322110,
                                delegate: 'tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.delegations first and date_range gte/lte and after', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6") {
                address
                first_seen
                activated
                public_key
                delegations (
                  first: 3
                  date_range: {
                    gte: "2019-10-22T18:56:24Z"
                    lte: "2019-10-27T12:46:11Z"
                  }
                  after: "ongTwy9dcZg35v84ur7rQcTgSA25Yj3rAm4acchPRutrQbLPrDX:1"
                ) {
                    page_info {
                        start_cursor
                        end_cursor
                        has_next_page
                        has_previous_page
                    }
                    edges {
                        cursor
                        node {
                            hash
                            batch_position
                            source
                            kind
                            timestamp
                            level
                            block
                            gas
                            counter
                            gas_limit
                            id
                            delegate
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6',
                first_seen: '2018-09-17T09:23:51Z',
                activated: '2018-07-15T13:01:27.000Z',
                public_key: null,
                delegations: {
                    page_info: {
                        start_cursor: 'ooqZeZuSdWTeo1zdSP887RqurGVnUE7dF9ZqqQMyorppaT7KBuP:1',
                        end_cursor: 'ooqZeZuSdWTeo1zdSP887RqurGVnUE7dF9ZqqQMyorppaT7KBuP:1',
                        has_next_page: false,
                        has_previous_page: true,
                    },
                    edges: [
                        {
                            cursor: 'ooqZeZuSdWTeo1zdSP887RqurGVnUE7dF9ZqqQMyorppaT7KBuP:1',
                            node: {
                                hash: 'ooqZeZuSdWTeo1zdSP887RqurGVnUE7dF9ZqqQMyorppaT7KBuP',
                                batch_position: 1,
                                source: 'tz1N5dH9DFu43oBD4Uu9d8jkGGatSRskqeGh',
                                kind: 'delegation',
                                timestamp: '2019-10-22T18:56:24Z',
                                level: 661451,
                                block: 'BMCgbqzH9uH48irmAkzrzf84LZfmLFNh3LVGGA3eWaEaDCR994x',
                                gas: 2720,
                                counter: '2186842',
                                gas_limit: '10600',
                                id: 15155828,
                                delegate: 'tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.delegations first and date_range gte/lte and order_by', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6") {
                address
                first_seen
                activated
                public_key
                delegations (
                    first: 3
                    date_range: {
                    gte: "2019-10-22T18:56:24Z"
                    lte: "2019-10-27T12:46:11Z"
                    }
                    order_by: {field: ID, direction: ASC}
                ) {
                    page_info {
                        start_cursor
                        end_cursor
                        has_next_page
                        has_previous_page
                    }
                    edges {
                        cursor
                        node {
                            hash
                            batch_position
                            source
                            kind
                            timestamp
                            level
                            block
                            gas
                            counter
                            gas_limit
                            id
                            delegate
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6',
                first_seen: '2018-09-17T09:23:51Z',
                activated: '2018-07-15T13:01:27.000Z',
                public_key: null,
                delegations: {
                    page_info: {
                        start_cursor: 'ooqZeZuSdWTeo1zdSP887RqurGVnUE7dF9ZqqQMyorppaT7KBuP:1',
                        end_cursor: 'ongTwy9dcZg35v84ur7rQcTgSA25Yj3rAm4acchPRutrQbLPrDX:1',
                        has_next_page: false,
                        has_previous_page: false,
                    },
                    edges: [
                        {
                            cursor: 'ooqZeZuSdWTeo1zdSP887RqurGVnUE7dF9ZqqQMyorppaT7KBuP:1',
                            node: {
                                hash: 'ooqZeZuSdWTeo1zdSP887RqurGVnUE7dF9ZqqQMyorppaT7KBuP',
                                batch_position: 1,
                                source: 'tz1N5dH9DFu43oBD4Uu9d8jkGGatSRskqeGh',
                                kind: 'delegation',
                                timestamp: '2019-10-22T18:56:24Z',
                                level: 661451,
                                block: 'BMCgbqzH9uH48irmAkzrzf84LZfmLFNh3LVGGA3eWaEaDCR994x',
                                gas: 2720,
                                counter: '2186842',
                                gas_limit: '10600',
                                id: 15155828,
                                delegate: 'tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6',
                            },
                        },
                        {
                            cursor: 'ongTwy9dcZg35v84ur7rQcTgSA25Yj3rAm4acchPRutrQbLPrDX:1',
                            node: {
                                hash: 'ongTwy9dcZg35v84ur7rQcTgSA25Yj3rAm4acchPRutrQbLPrDX',
                                batch_position: 1,
                                source: 'tz1QAAxPbfTn5L9BbNsiUwjWDvMZQbAGgXND',
                                kind: 'delegation',
                                timestamp: '2019-10-27T12:46:11Z',
                                level: 668100,
                                block: 'BKyB7ypqUv2coNZLJ6StFxhRasJPwXwtrDycTVcRxGusAxPWWJs',
                                gas: 4335,
                                counter: '2219653',
                                gas_limit: '10600',
                                id: 15322110,
                                delegate: 'tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.delegations first and date_range gte and order_by and before', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6") {
                address
                first_seen
                activated
                public_key
                delegations (
                  first: 3
                  order_by: {field: ID, direction: ASC}
                  before:"ooLzsB1PuLKqeGZznuz3YaFRiWD6G1TDKmAf4GqtVb4jnEbMoxg:1"
                  date_range: {
                    gte: "2019-10-27T12:46:11Z"
                  }
                ) {
                    page_info {
                        start_cursor
                        end_cursor
                        has_next_page
                        has_previous_page
                    }
                    edges {
                        cursor
                        node {
                            hash
                            batch_position
                            source
                            kind
                            timestamp
                            level
                            block
                            gas
                            counter
                            gas_limit
                            id
                            delegate
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6',
                first_seen: '2018-09-17T09:23:51Z',
                activated: '2018-07-15T13:01:27.000Z',
                public_key: null,
                delegations: {
                    page_info: {
                        start_cursor: 'ongTwy9dcZg35v84ur7rQcTgSA25Yj3rAm4acchPRutrQbLPrDX:1',
                        end_cursor: 'ongTwy9dcZg35v84ur7rQcTgSA25Yj3rAm4acchPRutrQbLPrDX:1',
                        has_next_page: true,
                        has_previous_page: false,
                    },
                    edges: [
                        {
                            cursor: 'ongTwy9dcZg35v84ur7rQcTgSA25Yj3rAm4acchPRutrQbLPrDX:1',
                            node: {
                                hash: 'ongTwy9dcZg35v84ur7rQcTgSA25Yj3rAm4acchPRutrQbLPrDX',
                                batch_position: 1,
                                source: 'tz1QAAxPbfTn5L9BbNsiUwjWDvMZQbAGgXND',
                                kind: 'delegation',
                                timestamp: '2019-10-27T12:46:11Z',
                                level: 668100,
                                block: 'BKyB7ypqUv2coNZLJ6StFxhRasJPwXwtrDycTVcRxGusAxPWWJs',
                                gas: 4335,
                                counter: '2219653',
                                gas_limit: '10600',
                                id: 15322110,
                                delegate: 'tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.delegations first and date_range gte and order_by and after', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6") {
                address
                first_seen
                activated
                public_key
                delegations (
                  first: 3
                  order_by: {field: ID, direction: ASC}
                  after:"ongTwy9dcZg35v84ur7rQcTgSA25Yj3rAm4acchPRutrQbLPrDX:1"
                  date_range: {
                    gte: "2019-10-27T12:46:11Z"
                  }
                ) {
                    page_info {
                        start_cursor
                        end_cursor
                        has_next_page
                        has_previous_page
                    }
                    edges {
                        cursor
                        node {
                            hash
                            batch_position
                            source
                            kind
                            timestamp
                            level
                            block
                            gas
                            counter
                            gas_limit
                            id
                            delegate
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6',
                first_seen: '2018-09-17T09:23:51Z',
                activated: '2018-07-15T13:01:27.000Z',
                public_key: null,
                delegations: {
                    page_info: {
                        start_cursor: 'ooLzsB1PuLKqeGZznuz3YaFRiWD6G1TDKmAf4GqtVb4jnEbMoxg:1',
                        end_cursor: 'ooLzsB1PuLKqeGZznuz3YaFRiWD6G1TDKmAf4GqtVb4jnEbMoxg:1',
                        has_next_page: false,
                        has_previous_page: true,
                    },
                    edges: [
                        {
                            cursor: 'ooLzsB1PuLKqeGZznuz3YaFRiWD6G1TDKmAf4GqtVb4jnEbMoxg:1',
                            node: {
                                hash: 'ooLzsB1PuLKqeGZznuz3YaFRiWD6G1TDKmAf4GqtVb4jnEbMoxg',
                                batch_position: 1,
                                source: 'tz1N73oapfHFiTzjv3UvKFnQY7Fp7CEUPqJk',
                                kind: 'delegation',
                                timestamp: '2020-09-08T14:35:40Z',
                                level: 1119602,
                                block: 'BLCzUbS7sUFLofeGzcwHGjoHHB8GYYR2X9msGzyZFss7Dni8JBw',
                                gas: 8738,
                                counter: '6584702',
                                gas_limit: '10600',
                                id: 27228347,
                                delegate: 'tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.delegations first and date_range lte and order_by and before', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6") {
                address
                first_seen
                activated
                public_key
                delegations (
                  first: 3
                  order_by: {field: ID, direction: ASC}
                  before:"ongTwy9dcZg35v84ur7rQcTgSA25Yj3rAm4acchPRutrQbLPrDX:1"
                  date_range: {
                    lte: "2019-10-27T12:46:11Z"
                  }
                ) {
                    page_info {
                        start_cursor
                        end_cursor
                        has_next_page
                        has_previous_page
                    }
                    edges {
                        cursor
                        node {
                            hash
                            batch_position
                            source
                            kind
                            timestamp
                            level
                            block
                            gas
                            counter
                            gas_limit
                            id
                            delegate
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6',
                first_seen: '2018-09-17T09:23:51Z',
                activated: '2018-07-15T13:01:27.000Z',
                public_key: null,
                delegations: {
                    page_info: {
                        start_cursor: 'oomdibzcw4AVcouPFfiEtLdqaRjq3oHnSp5tSVndEUJrvtHmZ45:1',
                        end_cursor: 'ooqZeZuSdWTeo1zdSP887RqurGVnUE7dF9ZqqQMyorppaT7KBuP:1',
                        has_next_page: true,
                        has_previous_page: false,
                    },
                    edges: [
                        {
                            cursor: 'oomdibzcw4AVcouPFfiEtLdqaRjq3oHnSp5tSVndEUJrvtHmZ45:1',
                            node: {
                                hash: 'oomdibzcw4AVcouPFfiEtLdqaRjq3oHnSp5tSVndEUJrvtHmZ45',
                                batch_position: 1,
                                source: 'tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6',
                                kind: 'delegation',
                                timestamp: '2018-07-24T17:27:27Z',
                                level: 32476,
                                block: 'BMZSMuqeweWSmT4JgrF5k3BS8RK3ynuach2Z5oSLHQ7X3BqtG7s',
                                gas: 50000,
                                counter: '33527',
                                gas_limit: '0',
                                id: 199112,
                                delegate: 'tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6',
                            },
                        },
                        {
                            cursor: 'ooqZeZuSdWTeo1zdSP887RqurGVnUE7dF9ZqqQMyorppaT7KBuP:1',
                            node: {
                                hash: 'ooqZeZuSdWTeo1zdSP887RqurGVnUE7dF9ZqqQMyorppaT7KBuP',
                                batch_position: 1,
                                source: 'tz1N5dH9DFu43oBD4Uu9d8jkGGatSRskqeGh',
                                kind: 'delegation',
                                timestamp: '2019-10-22T18:56:24Z',
                                level: 661451,
                                block: 'BMCgbqzH9uH48irmAkzrzf84LZfmLFNh3LVGGA3eWaEaDCR994x',
                                gas: 2720,
                                counter: '2186842',
                                gas_limit: '10600',
                                id: 15155828,
                                delegate: 'tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.delegations first and date_range lte and order_by and after', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6") {
                address
                first_seen
                activated
                public_key
                delegations (
                  first: 3
                  order_by: {field: ID, direction: ASC}
                  after:"ooqZeZuSdWTeo1zdSP887RqurGVnUE7dF9ZqqQMyorppaT7KBuP:1"
                  date_range: {
                    lte: "2019-10-27T12:46:11Z"
                  }
                ) {
                    page_info {
                        start_cursor
                        end_cursor
                        has_next_page
                        has_previous_page
                    }
                    edges {
                        cursor
                        node {
                            hash
                            batch_position
                            source
                            kind
                            timestamp
                            level
                            block
                            gas
                            counter
                            gas_limit
                            id
                            delegate
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6',
                first_seen: '2018-09-17T09:23:51Z',
                activated: '2018-07-15T13:01:27.000Z',
                public_key: null,
                delegations: {
                    page_info: {
                        start_cursor: 'ongTwy9dcZg35v84ur7rQcTgSA25Yj3rAm4acchPRutrQbLPrDX:1',
                        end_cursor: 'ongTwy9dcZg35v84ur7rQcTgSA25Yj3rAm4acchPRutrQbLPrDX:1',
                        has_next_page: false,
                        has_previous_page: true,
                    },
                    edges: [
                        {
                            cursor: 'ongTwy9dcZg35v84ur7rQcTgSA25Yj3rAm4acchPRutrQbLPrDX:1',
                            node: {
                                hash: 'ongTwy9dcZg35v84ur7rQcTgSA25Yj3rAm4acchPRutrQbLPrDX',
                                batch_position: 1,
                                source: 'tz1QAAxPbfTn5L9BbNsiUwjWDvMZQbAGgXND',
                                kind: 'delegation',
                                timestamp: '2019-10-27T12:46:11Z',
                                level: 668100,
                                block: 'BKyB7ypqUv2coNZLJ6StFxhRasJPwXwtrDycTVcRxGusAxPWWJs',
                                gas: 4335,
                                counter: '2219653',
                                gas_limit: '10600',
                                id: 15322110,
                                delegate: 'tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6',
                            },
                        },
                    ],
                },
            },
        }));
    });
    test('account.delegations first and date_range gte/lte and order_by and before', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6") {
                address
                first_seen
                activated
                public_key
                delegations (
                  first: 3
                  order_by: {field: ID, direction: ASC}
                  before:"ongTwy9dcZg35v84ur7rQcTgSA25Yj3rAm4acchPRutrQbLPrDX:1"
                  date_range: {
                    gte: "2019-10-22T18:56:24Z"
                    lte: "2019-10-27T12:46:11Z"
                  }
                ) {
                    page_info {
                        start_cursor
                        end_cursor
                        has_next_page
                        has_previous_page
                    }
                    edges {
                        cursor
                        node {
                            hash
                            batch_position
                            source
                            kind
                            timestamp
                            level
                            block
                            gas
                            counter
                            gas_limit
                            id
                            delegate
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6',
                first_seen: '2018-09-17T09:23:51Z',
                activated: '2018-07-15T13:01:27.000Z',
                public_key: null,
                delegations: {
                    page_info: {
                        start_cursor: 'ooqZeZuSdWTeo1zdSP887RqurGVnUE7dF9ZqqQMyorppaT7KBuP:1',
                        end_cursor: 'ooqZeZuSdWTeo1zdSP887RqurGVnUE7dF9ZqqQMyorppaT7KBuP:1',
                        has_next_page: true,
                        has_previous_page: false,
                    },
                    edges: [
                        {
                            cursor: 'ooqZeZuSdWTeo1zdSP887RqurGVnUE7dF9ZqqQMyorppaT7KBuP:1',
                            node: {
                                hash: 'ooqZeZuSdWTeo1zdSP887RqurGVnUE7dF9ZqqQMyorppaT7KBuP',
                                batch_position: 1,
                                source: 'tz1N5dH9DFu43oBD4Uu9d8jkGGatSRskqeGh',
                                kind: 'delegation',
                                timestamp: '2019-10-22T18:56:24Z',
                                level: 661451,
                                block: 'BMCgbqzH9uH48irmAkzrzf84LZfmLFNh3LVGGA3eWaEaDCR994x',
                                gas: 2720,
                                counter: '2186842',
                                gas_limit: '10600',
                                id: 15155828,
                                delegate: 'tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.delegations first and date_range gte/lte and order_by and after', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6") {
                address
                first_seen
                activated
                public_key
                delegations (
                  first: 3
                  order_by: {field: ID, direction: ASC}
                  after:"ooqZeZuSdWTeo1zdSP887RqurGVnUE7dF9ZqqQMyorppaT7KBuP:1"
                  date_range: {
                    gte: "2019-10-22T18:56:24Z"
                    lte: "2019-10-27T12:46:11Z"
                  }
                ) {
                    page_info {
                        start_cursor
                        end_cursor
                        has_next_page
                        has_previous_page
                    }
                    edges {
                        cursor
                        node {
                            hash
                            batch_position
                            source
                            kind
                            timestamp
                            level
                            block
                            gas
                            counter
                            gas_limit
                            id
                            delegate
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6',
                first_seen: '2018-09-17T09:23:51Z',
                activated: '2018-07-15T13:01:27.000Z',
                public_key: null,
                delegations: {
                    page_info: {
                        start_cursor: 'ongTwy9dcZg35v84ur7rQcTgSA25Yj3rAm4acchPRutrQbLPrDX:1',
                        end_cursor: 'ongTwy9dcZg35v84ur7rQcTgSA25Yj3rAm4acchPRutrQbLPrDX:1',
                        has_next_page: false,
                        has_previous_page: true,
                    },
                    edges: [
                        {
                            cursor: 'ongTwy9dcZg35v84ur7rQcTgSA25Yj3rAm4acchPRutrQbLPrDX:1',
                            node: {
                                hash: 'ongTwy9dcZg35v84ur7rQcTgSA25Yj3rAm4acchPRutrQbLPrDX',
                                batch_position: 1,
                                source: 'tz1QAAxPbfTn5L9BbNsiUwjWDvMZQbAGgXND',
                                kind: 'delegation',
                                timestamp: '2019-10-27T12:46:11Z',
                                level: 668100,
                                block: 'BKyB7ypqUv2coNZLJ6StFxhRasJPwXwtrDycTVcRxGusAxPWWJs',
                                gas: 4335,
                                counter: '2219653',
                                gas_limit: '10600',
                                id: 15322110,
                                delegate: 'tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.delegations last', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6") {
                address
                first_seen
                activated
                public_key
                delegations (last: 3) {
                    page_info {
                        start_cursor
                        end_cursor
                        has_next_page
                        has_previous_page
                    }
                    edges {
                        cursor
                        node {
                            hash
                            batch_position
                            source
                            kind
                            timestamp
                            level
                            block
                            gas
                            counter
                            gas_limit
                            id
                            delegate
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6',
                first_seen: '2018-09-17T09:23:51Z',
                activated: '2018-07-15T13:01:27.000Z',
                public_key: null,
                delegations: {
                    page_info: {
                        start_cursor: 'ongTwy9dcZg35v84ur7rQcTgSA25Yj3rAm4acchPRutrQbLPrDX:1',
                        end_cursor: 'oomdibzcw4AVcouPFfiEtLdqaRjq3oHnSp5tSVndEUJrvtHmZ45:1',
                        has_next_page: false,
                        has_previous_page: true,
                    },
                    edges: [
                        {
                            cursor: 'ongTwy9dcZg35v84ur7rQcTgSA25Yj3rAm4acchPRutrQbLPrDX:1',
                            node: {
                                hash: 'ongTwy9dcZg35v84ur7rQcTgSA25Yj3rAm4acchPRutrQbLPrDX',
                                batch_position: 1,
                                source: 'tz1QAAxPbfTn5L9BbNsiUwjWDvMZQbAGgXND',
                                kind: 'delegation',
                                timestamp: '2019-10-27T12:46:11Z',
                                level: 668100,
                                block: 'BKyB7ypqUv2coNZLJ6StFxhRasJPwXwtrDycTVcRxGusAxPWWJs',
                                gas: 4335,
                                counter: '2219653',
                                gas_limit: '10600',
                                id: 15322110,
                                delegate: 'tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6',
                            },
                        },
                        {
                            cursor: 'ooqZeZuSdWTeo1zdSP887RqurGVnUE7dF9ZqqQMyorppaT7KBuP:1',
                            node: {
                                hash: 'ooqZeZuSdWTeo1zdSP887RqurGVnUE7dF9ZqqQMyorppaT7KBuP',
                                batch_position: 1,
                                source: 'tz1N5dH9DFu43oBD4Uu9d8jkGGatSRskqeGh',
                                kind: 'delegation',
                                timestamp: '2019-10-22T18:56:24Z',
                                level: 661451,
                                block: 'BMCgbqzH9uH48irmAkzrzf84LZfmLFNh3LVGGA3eWaEaDCR994x',
                                gas: 2720,
                                counter: '2186842',
                                gas_limit: '10600',
                                id: 15155828,
                                delegate: 'tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6',
                            },
                        },
                        {
                            cursor: 'oomdibzcw4AVcouPFfiEtLdqaRjq3oHnSp5tSVndEUJrvtHmZ45:1',
                            node: {
                                hash: 'oomdibzcw4AVcouPFfiEtLdqaRjq3oHnSp5tSVndEUJrvtHmZ45',
                                batch_position: 1,
                                source: 'tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6',
                                kind: 'delegation',
                                timestamp: '2018-07-24T17:27:27Z',
                                level: 32476,
                                block: 'BMZSMuqeweWSmT4JgrF5k3BS8RK3ynuach2Z5oSLHQ7X3BqtG7s',
                                gas: 50000,
                                counter: '33527',
                                gas_limit: '0',
                                id: 199112,
                                delegate: 'tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.delegations last and before', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6") {
                address
                first_seen
                activated
                public_key
                delegations (
                last: 3
                before: "ooqZeZuSdWTeo1zdSP887RqurGVnUE7dF9ZqqQMyorppaT7KBuP:1"
                ) {
                    page_info {
                        start_cursor
                        end_cursor
                        has_next_page
                        has_previous_page
                    }
                    edges {
                        cursor
                        node {
                            hash
                            batch_position
                            source
                            kind
                            timestamp
                            level
                            block
                            gas
                            counter
                            gas_limit
                            id
                            delegate
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6',
                first_seen: '2018-09-17T09:23:51Z',
                activated: '2018-07-15T13:01:27.000Z',
                public_key: null,
                delegations: {
                    page_info: {
                        start_cursor: 'ooLzsB1PuLKqeGZznuz3YaFRiWD6G1TDKmAf4GqtVb4jnEbMoxg:1',
                        end_cursor: 'ongTwy9dcZg35v84ur7rQcTgSA25Yj3rAm4acchPRutrQbLPrDX:1',
                        has_next_page: true,
                        has_previous_page: false,
                    },
                    edges: [
                        {
                            cursor: 'ooLzsB1PuLKqeGZznuz3YaFRiWD6G1TDKmAf4GqtVb4jnEbMoxg:1',
                            node: {
                                hash: 'ooLzsB1PuLKqeGZznuz3YaFRiWD6G1TDKmAf4GqtVb4jnEbMoxg',
                                batch_position: 1,
                                source: 'tz1N73oapfHFiTzjv3UvKFnQY7Fp7CEUPqJk',
                                kind: 'delegation',
                                timestamp: '2020-09-08T14:35:40Z',
                                level: 1119602,
                                block: 'BLCzUbS7sUFLofeGzcwHGjoHHB8GYYR2X9msGzyZFss7Dni8JBw',
                                gas: 8738,
                                counter: '6584702',
                                gas_limit: '10600',
                                id: 27228347,
                                delegate: 'tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6',
                            },
                        },
                        {
                            cursor: 'ongTwy9dcZg35v84ur7rQcTgSA25Yj3rAm4acchPRutrQbLPrDX:1',
                            node: {
                                hash: 'ongTwy9dcZg35v84ur7rQcTgSA25Yj3rAm4acchPRutrQbLPrDX',
                                batch_position: 1,
                                source: 'tz1QAAxPbfTn5L9BbNsiUwjWDvMZQbAGgXND',
                                kind: 'delegation',
                                timestamp: '2019-10-27T12:46:11Z',
                                level: 668100,
                                block: 'BKyB7ypqUv2coNZLJ6StFxhRasJPwXwtrDycTVcRxGusAxPWWJs',
                                gas: 4335,
                                counter: '2219653',
                                gas_limit: '10600',
                                id: 15322110,
                                delegate: 'tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.delegations last and after', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6") {
                address
                first_seen
                activated
                public_key
                delegations (
                last: 3
                after: "ooqZeZuSdWTeo1zdSP887RqurGVnUE7dF9ZqqQMyorppaT7KBuP:1"
                ) {
                    page_info {
                        start_cursor
                        end_cursor
                        has_next_page
                        has_previous_page
                    }
                    edges {
                        cursor
                        node {
                            hash
                            batch_position
                            source
                            kind
                            timestamp
                            level
                            block
                            gas
                            counter
                            gas_limit
                            id
                            delegate
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6',
                first_seen: '2018-09-17T09:23:51Z',
                activated: '2018-07-15T13:01:27.000Z',
                public_key: null,
                delegations: {
                    page_info: {
                        start_cursor: 'oomdibzcw4AVcouPFfiEtLdqaRjq3oHnSp5tSVndEUJrvtHmZ45:1',
                        end_cursor: 'oomdibzcw4AVcouPFfiEtLdqaRjq3oHnSp5tSVndEUJrvtHmZ45:1',
                        has_next_page: false,
                        has_previous_page: true,
                    },
                    edges: [
                        {
                            cursor: 'oomdibzcw4AVcouPFfiEtLdqaRjq3oHnSp5tSVndEUJrvtHmZ45:1',
                            node: {
                                hash: 'oomdibzcw4AVcouPFfiEtLdqaRjq3oHnSp5tSVndEUJrvtHmZ45',
                                batch_position: 1,
                                source: 'tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6',
                                kind: 'delegation',
                                timestamp: '2018-07-24T17:27:27Z',
                                level: 32476,
                                block: 'BMZSMuqeweWSmT4JgrF5k3BS8RK3ynuach2Z5oSLHQ7X3BqtG7s',
                                gas: 50000,
                                counter: '33527',
                                gas_limit: '0',
                                id: 199112,
                                delegate: 'tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.delegations last and order_by', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6") {
                address
                first_seen
                activated
                public_key
                delegations (
                last: 3
                order_by: {field: ID, direction: ASC}
                ) {
                    page_info {
                        start_cursor
                        end_cursor
                        has_next_page
                        has_previous_page
                    }
                    edges {
                        cursor
                        node {
                            hash
                            batch_position
                            source
                            kind
                            timestamp
                            level
                            block
                            gas
                            counter
                            gas_limit
                            id
                            delegate
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6',
                first_seen: '2018-09-17T09:23:51Z',
                activated: '2018-07-15T13:01:27.000Z',
                public_key: null,
                delegations: {
                    page_info: {
                        start_cursor: 'ooqZeZuSdWTeo1zdSP887RqurGVnUE7dF9ZqqQMyorppaT7KBuP:1',
                        end_cursor: 'ooLzsB1PuLKqeGZznuz3YaFRiWD6G1TDKmAf4GqtVb4jnEbMoxg:1',
                        has_next_page: false,
                        has_previous_page: true,
                    },
                    edges: [
                        {
                            cursor: 'ooqZeZuSdWTeo1zdSP887RqurGVnUE7dF9ZqqQMyorppaT7KBuP:1',
                            node: {
                                hash: 'ooqZeZuSdWTeo1zdSP887RqurGVnUE7dF9ZqqQMyorppaT7KBuP',
                                batch_position: 1,
                                source: 'tz1N5dH9DFu43oBD4Uu9d8jkGGatSRskqeGh',
                                kind: 'delegation',
                                timestamp: '2019-10-22T18:56:24Z',
                                level: 661451,
                                block: 'BMCgbqzH9uH48irmAkzrzf84LZfmLFNh3LVGGA3eWaEaDCR994x',
                                gas: 2720,
                                counter: '2186842',
                                gas_limit: '10600',
                                id: 15155828,
                                delegate: 'tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6',
                            },
                        },
                        {
                            cursor: 'ongTwy9dcZg35v84ur7rQcTgSA25Yj3rAm4acchPRutrQbLPrDX:1',
                            node: {
                                hash: 'ongTwy9dcZg35v84ur7rQcTgSA25Yj3rAm4acchPRutrQbLPrDX',
                                batch_position: 1,
                                source: 'tz1QAAxPbfTn5L9BbNsiUwjWDvMZQbAGgXND',
                                kind: 'delegation',
                                timestamp: '2019-10-27T12:46:11Z',
                                level: 668100,
                                block: 'BKyB7ypqUv2coNZLJ6StFxhRasJPwXwtrDycTVcRxGusAxPWWJs',
                                gas: 4335,
                                counter: '2219653',
                                gas_limit: '10600',
                                id: 15322110,
                                delegate: 'tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6',
                            },
                        },
                        {
                            cursor: 'ooLzsB1PuLKqeGZznuz3YaFRiWD6G1TDKmAf4GqtVb4jnEbMoxg:1',
                            node: {
                                hash: 'ooLzsB1PuLKqeGZznuz3YaFRiWD6G1TDKmAf4GqtVb4jnEbMoxg',
                                batch_position: 1,
                                source: 'tz1N73oapfHFiTzjv3UvKFnQY7Fp7CEUPqJk',
                                kind: 'delegation',
                                timestamp: '2020-09-08T14:35:40Z',
                                level: 1119602,
                                block: 'BLCzUbS7sUFLofeGzcwHGjoHHB8GYYR2X9msGzyZFss7Dni8JBw',
                                gas: 8738,
                                counter: '6584702',
                                gas_limit: '10600',
                                id: 27228347,
                                delegate: 'tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.delegations last and order_by and before', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6") {
                address
                first_seen
                activated
                public_key
                delegations (
                last: 3
                order_by: {field: ID, direction: ASC}
                before:"ooqZeZuSdWTeo1zdSP887RqurGVnUE7dF9ZqqQMyorppaT7KBuP:1"
                ) {
                    page_info {
                        start_cursor
                        end_cursor
                        has_next_page
                        has_previous_page
                    }
                    edges {
                        cursor
                        node {
                            hash
                            batch_position
                            source
                            kind
                            timestamp
                            level
                            block
                            gas
                            counter
                            gas_limit
                            id
                            delegate
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6',
                first_seen: '2018-09-17T09:23:51Z',
                activated: '2018-07-15T13:01:27.000Z',
                public_key: null,
                delegations: {
                    page_info: {
                        start_cursor: 'oomdibzcw4AVcouPFfiEtLdqaRjq3oHnSp5tSVndEUJrvtHmZ45:1',
                        end_cursor: 'oomdibzcw4AVcouPFfiEtLdqaRjq3oHnSp5tSVndEUJrvtHmZ45:1',
                        has_next_page: true,
                        has_previous_page: false,
                    },
                    edges: [
                        {
                            cursor: 'oomdibzcw4AVcouPFfiEtLdqaRjq3oHnSp5tSVndEUJrvtHmZ45:1',
                            node: {
                                hash: 'oomdibzcw4AVcouPFfiEtLdqaRjq3oHnSp5tSVndEUJrvtHmZ45',
                                batch_position: 1,
                                source: 'tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6',
                                kind: 'delegation',
                                timestamp: '2018-07-24T17:27:27Z',
                                level: 32476,
                                block: 'BMZSMuqeweWSmT4JgrF5k3BS8RK3ynuach2Z5oSLHQ7X3BqtG7s',
                                gas: 50000,
                                counter: '33527',
                                gas_limit: '0',
                                id: 199112,
                                delegate: 'tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.delegations last and order_by and after', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6") {
                address
                first_seen
                activated
                public_key
                delegations (
                last: 3
                order_by: {field: ID, direction: ASC}
                after:"ooqZeZuSdWTeo1zdSP887RqurGVnUE7dF9ZqqQMyorppaT7KBuP:1"
                ) {
                    page_info {
                        start_cursor
                        end_cursor
                        has_next_page
                        has_previous_page
                    }
                    edges {
                        cursor
                        node {
                            hash
                            batch_position
                            source
                            kind
                            timestamp
                            level
                            block
                            gas
                            counter
                            gas_limit
                            id
                            delegate
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6',
                first_seen: '2018-09-17T09:23:51Z',
                activated: '2018-07-15T13:01:27.000Z',
                public_key: null,
                delegations: {
                    page_info: {
                        start_cursor: 'ongTwy9dcZg35v84ur7rQcTgSA25Yj3rAm4acchPRutrQbLPrDX:1',
                        end_cursor: 'ooLzsB1PuLKqeGZznuz3YaFRiWD6G1TDKmAf4GqtVb4jnEbMoxg:1',
                        has_next_page: false,
                        has_previous_page: true,
                    },
                    edges: [
                        {
                            cursor: 'ongTwy9dcZg35v84ur7rQcTgSA25Yj3rAm4acchPRutrQbLPrDX:1',
                            node: {
                                hash: 'ongTwy9dcZg35v84ur7rQcTgSA25Yj3rAm4acchPRutrQbLPrDX',
                                batch_position: 1,
                                source: 'tz1QAAxPbfTn5L9BbNsiUwjWDvMZQbAGgXND',
                                kind: 'delegation',
                                timestamp: '2019-10-27T12:46:11Z',
                                level: 668100,
                                block: 'BKyB7ypqUv2coNZLJ6StFxhRasJPwXwtrDycTVcRxGusAxPWWJs',
                                gas: 4335,
                                counter: '2219653',
                                gas_limit: '10600',
                                id: 15322110,
                                delegate: 'tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6',
                            },
                        },
                        {
                            cursor: 'ooLzsB1PuLKqeGZznuz3YaFRiWD6G1TDKmAf4GqtVb4jnEbMoxg:1',
                            node: {
                                hash: 'ooLzsB1PuLKqeGZznuz3YaFRiWD6G1TDKmAf4GqtVb4jnEbMoxg',
                                batch_position: 1,
                                source: 'tz1N73oapfHFiTzjv3UvKFnQY7Fp7CEUPqJk',
                                kind: 'delegation',
                                timestamp: '2020-09-08T14:35:40Z',
                                level: 1119602,
                                block: 'BLCzUbS7sUFLofeGzcwHGjoHHB8GYYR2X9msGzyZFss7Dni8JBw',
                                gas: 8738,
                                counter: '6584702',
                                gas_limit: '10600',
                                id: 27228347,
                                delegate: 'tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.delegations last and date_range gte', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6") {
                address
                first_seen
                activated
                public_key
                delegations (
                last: 3
                date_range: {
                    gte: "2019-10-22T18:56:24Z"
                }
                ) {
                    page_info {
                        start_cursor
                        end_cursor
                        has_next_page
                        has_previous_page
                    }
                    edges {
                        cursor
                        node {
                            hash
                            batch_position
                            source
                            kind
                            timestamp
                            level
                            block
                            gas
                            counter
                            gas_limit
                            id
                            delegate
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6',
                first_seen: '2018-09-17T09:23:51Z',
                activated: '2018-07-15T13:01:27.000Z',
                public_key: null,
                delegations: {
                    page_info: {
                        start_cursor: 'ooLzsB1PuLKqeGZznuz3YaFRiWD6G1TDKmAf4GqtVb4jnEbMoxg:1',
                        end_cursor: 'ooqZeZuSdWTeo1zdSP887RqurGVnUE7dF9ZqqQMyorppaT7KBuP:1',
                        has_next_page: false,
                        has_previous_page: false,
                    },
                    edges: [
                        {
                            cursor: 'ooLzsB1PuLKqeGZznuz3YaFRiWD6G1TDKmAf4GqtVb4jnEbMoxg:1',
                            node: {
                                hash: 'ooLzsB1PuLKqeGZznuz3YaFRiWD6G1TDKmAf4GqtVb4jnEbMoxg',
                                batch_position: 1,
                                source: 'tz1N73oapfHFiTzjv3UvKFnQY7Fp7CEUPqJk',
                                kind: 'delegation',
                                timestamp: '2020-09-08T14:35:40Z',
                                level: 1119602,
                                block: 'BLCzUbS7sUFLofeGzcwHGjoHHB8GYYR2X9msGzyZFss7Dni8JBw',
                                gas: 8738,
                                counter: '6584702',
                                gas_limit: '10600',
                                id: 27228347,
                                delegate: 'tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6',
                            },
                        },
                        {
                            cursor: 'ongTwy9dcZg35v84ur7rQcTgSA25Yj3rAm4acchPRutrQbLPrDX:1',
                            node: {
                                hash: 'ongTwy9dcZg35v84ur7rQcTgSA25Yj3rAm4acchPRutrQbLPrDX',
                                batch_position: 1,
                                source: 'tz1QAAxPbfTn5L9BbNsiUwjWDvMZQbAGgXND',
                                kind: 'delegation',
                                timestamp: '2019-10-27T12:46:11Z',
                                level: 668100,
                                block: 'BKyB7ypqUv2coNZLJ6StFxhRasJPwXwtrDycTVcRxGusAxPWWJs',
                                gas: 4335,
                                counter: '2219653',
                                gas_limit: '10600',
                                id: 15322110,
                                delegate: 'tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6',
                            },
                        },
                        {
                            cursor: 'ooqZeZuSdWTeo1zdSP887RqurGVnUE7dF9ZqqQMyorppaT7KBuP:1',
                            node: {
                                hash: 'ooqZeZuSdWTeo1zdSP887RqurGVnUE7dF9ZqqQMyorppaT7KBuP',
                                batch_position: 1,
                                source: 'tz1N5dH9DFu43oBD4Uu9d8jkGGatSRskqeGh',
                                kind: 'delegation',
                                timestamp: '2019-10-22T18:56:24Z',
                                level: 661451,
                                block: 'BMCgbqzH9uH48irmAkzrzf84LZfmLFNh3LVGGA3eWaEaDCR994x',
                                gas: 2720,
                                counter: '2186842',
                                gas_limit: '10600',
                                id: 15155828,
                                delegate: 'tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.delegations last and date_range gte and before', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6") {
                address
                first_seen
                activated
                public_key
                delegations (
                last: 3
                date_range: {
                    gte: "2019-10-22T18:56:24Z"
                }
                before: "ooqZeZuSdWTeo1zdSP887RqurGVnUE7dF9ZqqQMyorppaT7KBuP:1"
                ) {
                    page_info {
                        start_cursor
                        end_cursor
                        has_next_page
                        has_previous_page
                    }
                    edges {
                        cursor
                        node {
                            hash
                            batch_position
                            source
                            kind
                            timestamp
                            level
                            block
                            gas
                            counter
                            gas_limit
                            id
                            delegate
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6',
                first_seen: '2018-09-17T09:23:51Z',
                activated: '2018-07-15T13:01:27.000Z',
                public_key: null,
                delegations: {
                    page_info: {
                        start_cursor: 'ooLzsB1PuLKqeGZznuz3YaFRiWD6G1TDKmAf4GqtVb4jnEbMoxg:1',
                        end_cursor: 'ongTwy9dcZg35v84ur7rQcTgSA25Yj3rAm4acchPRutrQbLPrDX:1',
                        has_next_page: true,
                        has_previous_page: false,
                    },
                    edges: [
                        {
                            cursor: 'ooLzsB1PuLKqeGZznuz3YaFRiWD6G1TDKmAf4GqtVb4jnEbMoxg:1',
                            node: {
                                hash: 'ooLzsB1PuLKqeGZznuz3YaFRiWD6G1TDKmAf4GqtVb4jnEbMoxg',
                                batch_position: 1,
                                source: 'tz1N73oapfHFiTzjv3UvKFnQY7Fp7CEUPqJk',
                                kind: 'delegation',
                                timestamp: '2020-09-08T14:35:40Z',
                                level: 1119602,
                                block: 'BLCzUbS7sUFLofeGzcwHGjoHHB8GYYR2X9msGzyZFss7Dni8JBw',
                                gas: 8738,
                                counter: '6584702',
                                gas_limit: '10600',
                                id: 27228347,
                                delegate: 'tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6',
                            },
                        },
                        {
                            cursor: 'ongTwy9dcZg35v84ur7rQcTgSA25Yj3rAm4acchPRutrQbLPrDX:1',
                            node: {
                                hash: 'ongTwy9dcZg35v84ur7rQcTgSA25Yj3rAm4acchPRutrQbLPrDX',
                                batch_position: 1,
                                source: 'tz1QAAxPbfTn5L9BbNsiUwjWDvMZQbAGgXND',
                                kind: 'delegation',
                                timestamp: '2019-10-27T12:46:11Z',
                                level: 668100,
                                block: 'BKyB7ypqUv2coNZLJ6StFxhRasJPwXwtrDycTVcRxGusAxPWWJs',
                                gas: 4335,
                                counter: '2219653',
                                gas_limit: '10600',
                                id: 15322110,
                                delegate: 'tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.delegations last and date_range gte and after', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6") {
                address
                first_seen
                activated
                public_key
                delegations (
                last: 3
                date_range: {
                    gte: "2019-10-22T18:56:24Z"
                }
                after: "ongTwy9dcZg35v84ur7rQcTgSA25Yj3rAm4acchPRutrQbLPrDX:1"
                ) {
                    page_info {
                        start_cursor
                        end_cursor
                        has_next_page
                        has_previous_page
                    }
                    edges {
                        cursor
                        node {
                            hash
                            batch_position
                            source
                            kind
                            timestamp
                            level
                            block
                            gas
                            counter
                            gas_limit
                            id
                            delegate
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6',
                first_seen: '2018-09-17T09:23:51Z',
                activated: '2018-07-15T13:01:27.000Z',
                public_key: null,
                delegations: {
                    page_info: {
                        start_cursor: 'ooqZeZuSdWTeo1zdSP887RqurGVnUE7dF9ZqqQMyorppaT7KBuP:1',
                        end_cursor: 'ooqZeZuSdWTeo1zdSP887RqurGVnUE7dF9ZqqQMyorppaT7KBuP:1',
                        has_next_page: false,
                        has_previous_page: true,
                    },
                    edges: [
                        {
                            cursor: 'ooqZeZuSdWTeo1zdSP887RqurGVnUE7dF9ZqqQMyorppaT7KBuP:1',
                            node: {
                                hash: 'ooqZeZuSdWTeo1zdSP887RqurGVnUE7dF9ZqqQMyorppaT7KBuP',
                                batch_position: 1,
                                source: 'tz1N5dH9DFu43oBD4Uu9d8jkGGatSRskqeGh',
                                kind: 'delegation',
                                timestamp: '2019-10-22T18:56:24Z',
                                level: 661451,
                                block: 'BMCgbqzH9uH48irmAkzrzf84LZfmLFNh3LVGGA3eWaEaDCR994x',
                                gas: 2720,
                                counter: '2186842',
                                gas_limit: '10600',
                                id: 15155828,
                                delegate: 'tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.delegations last and date_range gte and order_by', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6") {
                address
                first_seen
                activated
                public_key
                delegations (
                    last: 3
                    date_range: {
                    gte: "2019-10-22T18:56:24Z"
                    }
                    order_by: {field: ID, direction: ASC}
                ) {
                    page_info {
                        start_cursor
                        end_cursor
                        has_next_page
                        has_previous_page
                    }
                    edges {
                        cursor
                        node {
                            hash
                            batch_position
                            source
                            kind
                            timestamp
                            level
                            block
                            gas
                            counter
                            gas_limit
                            id
                            delegate
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6',
                first_seen: '2018-09-17T09:23:51Z',
                activated: '2018-07-15T13:01:27.000Z',
                public_key: null,
                delegations: {
                    page_info: {
                        start_cursor: 'ooqZeZuSdWTeo1zdSP887RqurGVnUE7dF9ZqqQMyorppaT7KBuP:1',
                        end_cursor: 'ooLzsB1PuLKqeGZznuz3YaFRiWD6G1TDKmAf4GqtVb4jnEbMoxg:1',
                        has_next_page: false,
                        has_previous_page: false,
                    },
                    edges: [
                        {
                            cursor: 'ooqZeZuSdWTeo1zdSP887RqurGVnUE7dF9ZqqQMyorppaT7KBuP:1',
                            node: {
                                hash: 'ooqZeZuSdWTeo1zdSP887RqurGVnUE7dF9ZqqQMyorppaT7KBuP',
                                batch_position: 1,
                                source: 'tz1N5dH9DFu43oBD4Uu9d8jkGGatSRskqeGh',
                                kind: 'delegation',
                                timestamp: '2019-10-22T18:56:24Z',
                                level: 661451,
                                block: 'BMCgbqzH9uH48irmAkzrzf84LZfmLFNh3LVGGA3eWaEaDCR994x',
                                gas: 2720,
                                counter: '2186842',
                                gas_limit: '10600',
                                id: 15155828,
                                delegate: 'tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6',
                            },
                        },
                        {
                            cursor: 'ongTwy9dcZg35v84ur7rQcTgSA25Yj3rAm4acchPRutrQbLPrDX:1',
                            node: {
                                hash: 'ongTwy9dcZg35v84ur7rQcTgSA25Yj3rAm4acchPRutrQbLPrDX',
                                batch_position: 1,
                                source: 'tz1QAAxPbfTn5L9BbNsiUwjWDvMZQbAGgXND',
                                kind: 'delegation',
                                timestamp: '2019-10-27T12:46:11Z',
                                level: 668100,
                                block: 'BKyB7ypqUv2coNZLJ6StFxhRasJPwXwtrDycTVcRxGusAxPWWJs',
                                gas: 4335,
                                counter: '2219653',
                                gas_limit: '10600',
                                id: 15322110,
                                delegate: 'tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6',
                            },
                        },
                        {
                            cursor: 'ooLzsB1PuLKqeGZznuz3YaFRiWD6G1TDKmAf4GqtVb4jnEbMoxg:1',
                            node: {
                                hash: 'ooLzsB1PuLKqeGZznuz3YaFRiWD6G1TDKmAf4GqtVb4jnEbMoxg',
                                batch_position: 1,
                                source: 'tz1N73oapfHFiTzjv3UvKFnQY7Fp7CEUPqJk',
                                kind: 'delegation',
                                timestamp: '2020-09-08T14:35:40Z',
                                level: 1119602,
                                block: 'BLCzUbS7sUFLofeGzcwHGjoHHB8GYYR2X9msGzyZFss7Dni8JBw',
                                gas: 8738,
                                counter: '6584702',
                                gas_limit: '10600',
                                id: 27228347,
                                delegate: 'tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.delegations last and date_range lte', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6") {
                address
                first_seen
                activated
                public_key
                delegations (
                last: 3
                date_range: {
                    lte: "2019-10-22T18:56:24Z"
                }
                ) {
                    page_info {
                        start_cursor
                        end_cursor
                        has_next_page
                        has_previous_page
                    }
                    edges {
                        cursor
                        node {
                            hash
                            batch_position
                            source
                            kind
                            timestamp
                            level
                            block
                            gas
                            counter
                            gas_limit
                            id
                            delegate
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6',
                first_seen: '2018-09-17T09:23:51Z',
                activated: '2018-07-15T13:01:27.000Z',
                public_key: null,
                delegations: {
                    page_info: {
                        start_cursor: 'ooqZeZuSdWTeo1zdSP887RqurGVnUE7dF9ZqqQMyorppaT7KBuP:1',
                        end_cursor: 'oomdibzcw4AVcouPFfiEtLdqaRjq3oHnSp5tSVndEUJrvtHmZ45:1',
                        has_next_page: false,
                        has_previous_page: false,
                    },
                    edges: [
                        {
                            cursor: 'ooqZeZuSdWTeo1zdSP887RqurGVnUE7dF9ZqqQMyorppaT7KBuP:1',
                            node: {
                                hash: 'ooqZeZuSdWTeo1zdSP887RqurGVnUE7dF9ZqqQMyorppaT7KBuP',
                                batch_position: 1,
                                source: 'tz1N5dH9DFu43oBD4Uu9d8jkGGatSRskqeGh',
                                kind: 'delegation',
                                timestamp: '2019-10-22T18:56:24Z',
                                level: 661451,
                                block: 'BMCgbqzH9uH48irmAkzrzf84LZfmLFNh3LVGGA3eWaEaDCR994x',
                                gas: 2720,
                                counter: '2186842',
                                gas_limit: '10600',
                                id: 15155828,
                                delegate: 'tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6',
                            },
                        },
                        {
                            cursor: 'oomdibzcw4AVcouPFfiEtLdqaRjq3oHnSp5tSVndEUJrvtHmZ45:1',
                            node: {
                                hash: 'oomdibzcw4AVcouPFfiEtLdqaRjq3oHnSp5tSVndEUJrvtHmZ45',
                                batch_position: 1,
                                source: 'tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6',
                                kind: 'delegation',
                                timestamp: '2018-07-24T17:27:27Z',
                                level: 32476,
                                block: 'BMZSMuqeweWSmT4JgrF5k3BS8RK3ynuach2Z5oSLHQ7X3BqtG7s',
                                gas: 50000,
                                counter: '33527',
                                gas_limit: '0',
                                id: 199112,
                                delegate: 'tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.delegations last and date_range lte and before', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6") {
                address
                first_seen
                activated
                public_key
                delegations (
                last: 3
                date_range: {
                    lte: "2019-10-22T18:56:24Z"
                }
                before: "oomdibzcw4AVcouPFfiEtLdqaRjq3oHnSp5tSVndEUJrvtHmZ45:1"
                ) {
                    page_info {
                        start_cursor
                        end_cursor
                        has_next_page
                        has_previous_page
                    }
                    edges {
                        cursor
                        node {
                            hash
                            batch_position
                            source
                            kind
                            timestamp
                            level
                            block
                            gas
                            counter
                            gas_limit
                            id
                            delegate
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6',
                first_seen: '2018-09-17T09:23:51Z',
                activated: '2018-07-15T13:01:27.000Z',
                public_key: null,
                delegations: {
                    page_info: {
                        start_cursor: 'ooqZeZuSdWTeo1zdSP887RqurGVnUE7dF9ZqqQMyorppaT7KBuP:1',
                        end_cursor: 'ooqZeZuSdWTeo1zdSP887RqurGVnUE7dF9ZqqQMyorppaT7KBuP:1',
                        has_next_page: true,
                        has_previous_page: false,
                    },
                    edges: [
                        {
                            cursor: 'ooqZeZuSdWTeo1zdSP887RqurGVnUE7dF9ZqqQMyorppaT7KBuP:1',
                            node: {
                                hash: 'ooqZeZuSdWTeo1zdSP887RqurGVnUE7dF9ZqqQMyorppaT7KBuP',
                                batch_position: 1,
                                source: 'tz1N5dH9DFu43oBD4Uu9d8jkGGatSRskqeGh',
                                kind: 'delegation',
                                timestamp: '2019-10-22T18:56:24Z',
                                level: 661451,
                                block: 'BMCgbqzH9uH48irmAkzrzf84LZfmLFNh3LVGGA3eWaEaDCR994x',
                                gas: 2720,
                                counter: '2186842',
                                gas_limit: '10600',
                                id: 15155828,
                                delegate: 'tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.delegations last and date_range lte and after', async () => {
        const GET_ACCOUNT = gql`
       query {
            account(address: "tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6") {
                address
                first_seen
                activated
                public_key
                delegations (
                last: 3
                date_range: {
                    lte: "2019-10-22T18:56:24Z"
                }
                after: "ooqZeZuSdWTeo1zdSP887RqurGVnUE7dF9ZqqQMyorppaT7KBuP:1"
                ) {
                    page_info {
                        start_cursor
                        end_cursor
                        has_next_page
                        has_previous_page
                    }
                    edges {
                        cursor
                        node {
                            hash
                            batch_position
                            source
                            kind
                            timestamp
                            level
                            block
                            gas
                            counter
                            gas_limit
                            id
                            delegate
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6',
                first_seen: '2018-09-17T09:23:51Z',
                activated: '2018-07-15T13:01:27.000Z',
                public_key: null,
                delegations: {
                    page_info: {
                        start_cursor: 'oomdibzcw4AVcouPFfiEtLdqaRjq3oHnSp5tSVndEUJrvtHmZ45:1',
                        end_cursor: 'oomdibzcw4AVcouPFfiEtLdqaRjq3oHnSp5tSVndEUJrvtHmZ45:1',
                        has_next_page: false,
                        has_previous_page: true,
                    },
                    edges: [
                        {
                            cursor: 'oomdibzcw4AVcouPFfiEtLdqaRjq3oHnSp5tSVndEUJrvtHmZ45:1',
                            node: {
                                hash: 'oomdibzcw4AVcouPFfiEtLdqaRjq3oHnSp5tSVndEUJrvtHmZ45',
                                batch_position: 1,
                                source: 'tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6',
                                kind: 'delegation',
                                timestamp: '2018-07-24T17:27:27Z',
                                level: 32476,
                                block: 'BMZSMuqeweWSmT4JgrF5k3BS8RK3ynuach2Z5oSLHQ7X3BqtG7s',
                                gas: 50000,
                                counter: '33527',
                                gas_limit: '0',
                                id: 199112,
                                delegate: 'tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.delegations last and date_range lte and order_by', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6") {
                address
                first_seen
                activated
                public_key
                delegations (
                    last: 3
                    date_range: {
                    lte: "2019-10-22T18:56:24Z"
                    }
                    order_by: {field: ID, direction: ASC}
                ) {
                    page_info {
                        start_cursor
                        end_cursor
                        has_next_page
                        has_previous_page
                    }
                    edges {
                        cursor
                        node {
                            hash
                            batch_position
                            source
                            kind
                            timestamp
                            level
                            block
                            gas
                            counter
                            gas_limit
                            id
                            delegate
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6',
                first_seen: '2018-09-17T09:23:51Z',
                activated: '2018-07-15T13:01:27.000Z',
                public_key: null,
                delegations: {
                    page_info: {
                        start_cursor: 'oomdibzcw4AVcouPFfiEtLdqaRjq3oHnSp5tSVndEUJrvtHmZ45:1',
                        end_cursor: 'ooqZeZuSdWTeo1zdSP887RqurGVnUE7dF9ZqqQMyorppaT7KBuP:1',
                        has_next_page: false,
                        has_previous_page: false,
                    },
                    edges: [
                        {
                            cursor: 'oomdibzcw4AVcouPFfiEtLdqaRjq3oHnSp5tSVndEUJrvtHmZ45:1',
                            node: {
                                hash: 'oomdibzcw4AVcouPFfiEtLdqaRjq3oHnSp5tSVndEUJrvtHmZ45',
                                batch_position: 1,
                                source: 'tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6',
                                kind: 'delegation',
                                timestamp: '2018-07-24T17:27:27Z',
                                level: 32476,
                                block: 'BMZSMuqeweWSmT4JgrF5k3BS8RK3ynuach2Z5oSLHQ7X3BqtG7s',
                                gas: 50000,
                                counter: '33527',
                                gas_limit: '0',
                                id: 199112,
                                delegate: 'tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6',
                            },
                        },
                        {
                            cursor: 'ooqZeZuSdWTeo1zdSP887RqurGVnUE7dF9ZqqQMyorppaT7KBuP:1',
                            node: {
                                hash: 'ooqZeZuSdWTeo1zdSP887RqurGVnUE7dF9ZqqQMyorppaT7KBuP',
                                batch_position: 1,
                                source: 'tz1N5dH9DFu43oBD4Uu9d8jkGGatSRskqeGh',
                                kind: 'delegation',
                                timestamp: '2019-10-22T18:56:24Z',
                                level: 661451,
                                block: 'BMCgbqzH9uH48irmAkzrzf84LZfmLFNh3LVGGA3eWaEaDCR994x',
                                gas: 2720,
                                counter: '2186842',
                                gas_limit: '10600',
                                id: 15155828,
                                delegate: 'tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.delegations last and date_range gte/lte', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6") {
                address
                first_seen
                activated
                public_key
                delegations (
                last: 3
                date_range: {
                    gte: "2019-10-22T18:56:24Z"
                    lte: "2019-10-27T12:46:11Z"
                }
                ) {
                    page_info {
                        start_cursor
                        end_cursor
                        has_next_page
                        has_previous_page
                    }
                    edges {
                        cursor
                        node {
                            hash
                            batch_position
                            source
                            kind
                            timestamp
                            level
                            block
                            gas
                            counter
                            gas_limit
                            id
                            delegate
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6',
                first_seen: '2018-09-17T09:23:51Z',
                activated: '2018-07-15T13:01:27.000Z',
                public_key: null,
                delegations: {
                    page_info: {
                        start_cursor: 'ongTwy9dcZg35v84ur7rQcTgSA25Yj3rAm4acchPRutrQbLPrDX:1',
                        end_cursor: 'ooqZeZuSdWTeo1zdSP887RqurGVnUE7dF9ZqqQMyorppaT7KBuP:1',
                        has_next_page: false,
                        has_previous_page: false,
                    },
                    edges: [
                        {
                            cursor: 'ongTwy9dcZg35v84ur7rQcTgSA25Yj3rAm4acchPRutrQbLPrDX:1',
                            node: {
                                hash: 'ongTwy9dcZg35v84ur7rQcTgSA25Yj3rAm4acchPRutrQbLPrDX',
                                batch_position: 1,
                                source: 'tz1QAAxPbfTn5L9BbNsiUwjWDvMZQbAGgXND',
                                kind: 'delegation',
                                timestamp: '2019-10-27T12:46:11Z',
                                level: 668100,
                                block: 'BKyB7ypqUv2coNZLJ6StFxhRasJPwXwtrDycTVcRxGusAxPWWJs',
                                gas: 4335,
                                counter: '2219653',
                                gas_limit: '10600',
                                id: 15322110,
                                delegate: 'tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6',
                            },
                        },
                        {
                            cursor: 'ooqZeZuSdWTeo1zdSP887RqurGVnUE7dF9ZqqQMyorppaT7KBuP:1',
                            node: {
                                hash: 'ooqZeZuSdWTeo1zdSP887RqurGVnUE7dF9ZqqQMyorppaT7KBuP',
                                batch_position: 1,
                                source: 'tz1N5dH9DFu43oBD4Uu9d8jkGGatSRskqeGh',
                                kind: 'delegation',
                                timestamp: '2019-10-22T18:56:24Z',
                                level: 661451,
                                block: 'BMCgbqzH9uH48irmAkzrzf84LZfmLFNh3LVGGA3eWaEaDCR994x',
                                gas: 2720,
                                counter: '2186842',
                                gas_limit: '10600',
                                id: 15155828,
                                delegate: 'tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.delegations last and date_range gte/lte and before', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6") {
                address
                first_seen
                activated
                public_key
                delegations (
                last: 3
                date_range: {
                    gte: "2019-10-22T18:56:24Z"
                    lte: "2019-10-27T12:46:11Z"
                }
                before:"ooqZeZuSdWTeo1zdSP887RqurGVnUE7dF9ZqqQMyorppaT7KBuP:1"
                ) {
                    page_info {
                        start_cursor
                        end_cursor
                        has_next_page
                        has_previous_page
                    }
                    edges {
                        cursor
                        node {
                            hash
                            batch_position
                            source
                            kind
                            timestamp
                            level
                            block
                            gas
                            counter
                            gas_limit
                            id
                            delegate
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6',
                first_seen: '2018-09-17T09:23:51Z',
                activated: '2018-07-15T13:01:27.000Z',
                public_key: null,
                delegations: {
                    page_info: {
                        start_cursor: 'ongTwy9dcZg35v84ur7rQcTgSA25Yj3rAm4acchPRutrQbLPrDX:1',
                        end_cursor: 'ongTwy9dcZg35v84ur7rQcTgSA25Yj3rAm4acchPRutrQbLPrDX:1',
                        has_next_page: true,
                        has_previous_page: false,
                    },
                    edges: [
                        {
                            cursor: 'ongTwy9dcZg35v84ur7rQcTgSA25Yj3rAm4acchPRutrQbLPrDX:1',
                            node: {
                                hash: 'ongTwy9dcZg35v84ur7rQcTgSA25Yj3rAm4acchPRutrQbLPrDX',
                                batch_position: 1,
                                source: 'tz1QAAxPbfTn5L9BbNsiUwjWDvMZQbAGgXND',
                                kind: 'delegation',
                                timestamp: '2019-10-27T12:46:11Z',
                                level: 668100,
                                block: 'BKyB7ypqUv2coNZLJ6StFxhRasJPwXwtrDycTVcRxGusAxPWWJs',
                                gas: 4335,
                                counter: '2219653',
                                gas_limit: '10600',
                                id: 15322110,
                                delegate: 'tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.delegations last and date_range gte/lte and after', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6") {
                address
                first_seen
                activated
                public_key
                delegations (
                last: 3
                date_range: {
                    gte: "2019-10-22T18:56:24Z"
                    lte: "2019-10-27T12:46:11Z"
                }
                after:"ongTwy9dcZg35v84ur7rQcTgSA25Yj3rAm4acchPRutrQbLPrDX:1"
                ) {
                    page_info {
                        start_cursor
                        end_cursor
                        has_next_page
                        has_previous_page
                    }
                    edges {
                        cursor
                        node {
                            hash
                            batch_position
                            source
                            kind
                            timestamp
                            level
                            block
                            gas
                            counter
                            gas_limit
                            id
                            delegate
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6',
                first_seen: '2018-09-17T09:23:51Z',
                activated: '2018-07-15T13:01:27.000Z',
                public_key: null,
                delegations: {
                    page_info: {
                        start_cursor: 'ooqZeZuSdWTeo1zdSP887RqurGVnUE7dF9ZqqQMyorppaT7KBuP:1',
                        end_cursor: 'ooqZeZuSdWTeo1zdSP887RqurGVnUE7dF9ZqqQMyorppaT7KBuP:1',
                        has_next_page: false,
                        has_previous_page: true,
                    },
                    edges: [
                        {
                            cursor: 'ooqZeZuSdWTeo1zdSP887RqurGVnUE7dF9ZqqQMyorppaT7KBuP:1',
                            node: {
                                hash: 'ooqZeZuSdWTeo1zdSP887RqurGVnUE7dF9ZqqQMyorppaT7KBuP',
                                batch_position: 1,
                                source: 'tz1N5dH9DFu43oBD4Uu9d8jkGGatSRskqeGh',
                                kind: 'delegation',
                                timestamp: '2019-10-22T18:56:24Z',
                                level: 661451,
                                block: 'BMCgbqzH9uH48irmAkzrzf84LZfmLFNh3LVGGA3eWaEaDCR994x',
                                gas: 2720,
                                counter: '2186842',
                                gas_limit: '10600',
                                id: 15155828,
                                delegate: 'tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.delegations last and date_range gte/lte and order_by', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6") {
                address
                first_seen
                activated
                public_key
                delegations (
                    last: 3
                    date_range: {
                    gte: "2019-10-22T18:56:24Z"
                    lte: "2019-10-27T12:46:11Z"
                    }
                    order_by: {field: ID, direction: ASC}
                ) {
                    page_info {
                        start_cursor
                        end_cursor
                        has_next_page
                        has_previous_page
                    }
                    edges {
                        cursor
                        node {
                            hash
                            batch_position
                            source
                            kind
                            timestamp
                            level
                            block
                            gas
                            counter
                            gas_limit
                            id
                            delegate
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6',
                first_seen: '2018-09-17T09:23:51Z',
                activated: '2018-07-15T13:01:27.000Z',
                public_key: null,
                delegations: {
                    page_info: {
                        start_cursor: 'ooqZeZuSdWTeo1zdSP887RqurGVnUE7dF9ZqqQMyorppaT7KBuP:1',
                        end_cursor: 'ongTwy9dcZg35v84ur7rQcTgSA25Yj3rAm4acchPRutrQbLPrDX:1',
                        has_next_page: false,
                        has_previous_page: false,
                    },
                    edges: [
                        {
                            cursor: 'ooqZeZuSdWTeo1zdSP887RqurGVnUE7dF9ZqqQMyorppaT7KBuP:1',
                            node: {
                                hash: 'ooqZeZuSdWTeo1zdSP887RqurGVnUE7dF9ZqqQMyorppaT7KBuP',
                                batch_position: 1,
                                source: 'tz1N5dH9DFu43oBD4Uu9d8jkGGatSRskqeGh',
                                kind: 'delegation',
                                timestamp: '2019-10-22T18:56:24Z',
                                level: 661451,
                                block: 'BMCgbqzH9uH48irmAkzrzf84LZfmLFNh3LVGGA3eWaEaDCR994x',
                                gas: 2720,
                                counter: '2186842',
                                gas_limit: '10600',
                                id: 15155828,
                                delegate: 'tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6',
                            },
                        },
                        {
                            cursor: 'ongTwy9dcZg35v84ur7rQcTgSA25Yj3rAm4acchPRutrQbLPrDX:1',
                            node: {
                                hash: 'ongTwy9dcZg35v84ur7rQcTgSA25Yj3rAm4acchPRutrQbLPrDX',
                                batch_position: 1,
                                source: 'tz1QAAxPbfTn5L9BbNsiUwjWDvMZQbAGgXND',
                                kind: 'delegation',
                                timestamp: '2019-10-27T12:46:11Z',
                                level: 668100,
                                block: 'BKyB7ypqUv2coNZLJ6StFxhRasJPwXwtrDycTVcRxGusAxPWWJs',
                                gas: 4335,
                                counter: '2219653',
                                gas_limit: '10600',
                                id: 15322110,
                                delegate: 'tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.delegations last and date_range gte and order_by and before', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6") {
                address
                first_seen
                activated
                public_key
                delegations (
                last: 3
                order_by: {field: ID, direction: ASC}
                before:"ooLzsB1PuLKqeGZznuz3YaFRiWD6G1TDKmAf4GqtVb4jnEbMoxg:1"
                date_range: {
                    gte: "2019-10-27T12:46:11Z"
                }
                ) {
                    page_info {
                        start_cursor
                        end_cursor
                        has_next_page
                        has_previous_page
                    }
                    edges {
                        cursor
                        node {
                            hash
                            batch_position
                            source
                            kind
                            timestamp
                            level
                            block
                            gas
                            counter
                            gas_limit
                            id
                            delegate
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6',
                first_seen: '2018-09-17T09:23:51Z',
                activated: '2018-07-15T13:01:27.000Z',
                public_key: null,
                delegations: {
                    page_info: {
                        start_cursor: 'ongTwy9dcZg35v84ur7rQcTgSA25Yj3rAm4acchPRutrQbLPrDX:1',
                        end_cursor: 'ongTwy9dcZg35v84ur7rQcTgSA25Yj3rAm4acchPRutrQbLPrDX:1',
                        has_next_page: true,
                        has_previous_page: false,
                    },
                    edges: [
                        {
                            cursor: 'ongTwy9dcZg35v84ur7rQcTgSA25Yj3rAm4acchPRutrQbLPrDX:1',
                            node: {
                                hash: 'ongTwy9dcZg35v84ur7rQcTgSA25Yj3rAm4acchPRutrQbLPrDX',
                                batch_position: 1,
                                source: 'tz1QAAxPbfTn5L9BbNsiUwjWDvMZQbAGgXND',
                                kind: 'delegation',
                                timestamp: '2019-10-27T12:46:11Z',
                                level: 668100,
                                block: 'BKyB7ypqUv2coNZLJ6StFxhRasJPwXwtrDycTVcRxGusAxPWWJs',
                                gas: 4335,
                                counter: '2219653',
                                gas_limit: '10600',
                                id: 15322110,
                                delegate: 'tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.delegations last and date_range gte and order_by and after', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6") {
                address
                first_seen
                activated
                public_key
                delegations (
                last: 3
                order_by: {field: ID, direction: ASC}
                after:"ongTwy9dcZg35v84ur7rQcTgSA25Yj3rAm4acchPRutrQbLPrDX:1"
                date_range: {
                    gte: "2019-10-27T12:46:11Z"
                }
                ) {
                    page_info {
                        start_cursor
                        end_cursor
                        has_next_page
                        has_previous_page
                    }
                    edges {
                        cursor
                        node {
                            hash
                            batch_position
                            source
                            kind
                            timestamp
                            level
                            block
                            gas
                            counter
                            gas_limit
                            id
                            delegate
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6',
                first_seen: '2018-09-17T09:23:51Z',
                activated: '2018-07-15T13:01:27.000Z',
                public_key: null,
                delegations: {
                    page_info: {
                        start_cursor: 'ooLzsB1PuLKqeGZznuz3YaFRiWD6G1TDKmAf4GqtVb4jnEbMoxg:1',
                        end_cursor: 'ooLzsB1PuLKqeGZznuz3YaFRiWD6G1TDKmAf4GqtVb4jnEbMoxg:1',
                        has_next_page: false,
                        has_previous_page: true,
                    },
                    edges: [
                        {
                            cursor: 'ooLzsB1PuLKqeGZznuz3YaFRiWD6G1TDKmAf4GqtVb4jnEbMoxg:1',
                            node: {
                                hash: 'ooLzsB1PuLKqeGZznuz3YaFRiWD6G1TDKmAf4GqtVb4jnEbMoxg',
                                batch_position: 1,
                                source: 'tz1N73oapfHFiTzjv3UvKFnQY7Fp7CEUPqJk',
                                kind: 'delegation',
                                timestamp: '2020-09-08T14:35:40Z',
                                level: 1119602,
                                block: 'BLCzUbS7sUFLofeGzcwHGjoHHB8GYYR2X9msGzyZFss7Dni8JBw',
                                gas: 8738,
                                counter: '6584702',
                                gas_limit: '10600',
                                id: 27228347,
                                delegate: 'tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.delegations last and date_range lte and order_by and before', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6") {
                address
                first_seen
                activated
                public_key
                delegations (
                last: 3
                order_by: {field: ID, direction: ASC}
                before:"ongTwy9dcZg35v84ur7rQcTgSA25Yj3rAm4acchPRutrQbLPrDX:1"
                date_range: {
                    lte: "2019-10-27T12:46:11Z"
                }
                ) {
                    page_info {
                        start_cursor
                        end_cursor
                        has_next_page
                        has_previous_page
                    }
                    edges {
                        cursor
                        node {
                            hash
                            batch_position
                            source
                            kind
                            timestamp
                            level
                            block
                            gas
                            counter
                            gas_limit
                            id
                            delegate
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6',
                first_seen: '2018-09-17T09:23:51Z',
                activated: '2018-07-15T13:01:27.000Z',
                public_key: null,
                delegations: {
                    page_info: {
                        start_cursor: 'oomdibzcw4AVcouPFfiEtLdqaRjq3oHnSp5tSVndEUJrvtHmZ45:1',
                        end_cursor: 'ooqZeZuSdWTeo1zdSP887RqurGVnUE7dF9ZqqQMyorppaT7KBuP:1',
                        has_next_page: true,
                        has_previous_page: false,
                    },
                    edges: [
                        {
                            cursor: 'oomdibzcw4AVcouPFfiEtLdqaRjq3oHnSp5tSVndEUJrvtHmZ45:1',
                            node: {
                                hash: 'oomdibzcw4AVcouPFfiEtLdqaRjq3oHnSp5tSVndEUJrvtHmZ45',
                                batch_position: 1,
                                source: 'tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6',
                                kind: 'delegation',
                                timestamp: '2018-07-24T17:27:27Z',
                                level: 32476,
                                block: 'BMZSMuqeweWSmT4JgrF5k3BS8RK3ynuach2Z5oSLHQ7X3BqtG7s',
                                gas: 50000,
                                counter: '33527',
                                gas_limit: '0',
                                id: 199112,
                                delegate: 'tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6',
                            },
                        },
                        {
                            cursor: 'ooqZeZuSdWTeo1zdSP887RqurGVnUE7dF9ZqqQMyorppaT7KBuP:1',
                            node: {
                                hash: 'ooqZeZuSdWTeo1zdSP887RqurGVnUE7dF9ZqqQMyorppaT7KBuP',
                                batch_position: 1,
                                source: 'tz1N5dH9DFu43oBD4Uu9d8jkGGatSRskqeGh',
                                kind: 'delegation',
                                timestamp: '2019-10-22T18:56:24Z',
                                level: 661451,
                                block: 'BMCgbqzH9uH48irmAkzrzf84LZfmLFNh3LVGGA3eWaEaDCR994x',
                                gas: 2720,
                                counter: '2186842',
                                gas_limit: '10600',
                                id: 15155828,
                                delegate: 'tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.delegations last and date_range lte and order_by and after', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6") {
                address
                first_seen
                activated
                public_key
                delegations (
                last: 3
                order_by: {field: ID, direction: ASC}
                after:"ooqZeZuSdWTeo1zdSP887RqurGVnUE7dF9ZqqQMyorppaT7KBuP:1"
                date_range: {
                    lte: "2019-10-27T12:46:11Z"
                }
                ) {
                    page_info {
                        start_cursor
                        end_cursor
                        has_next_page
                        has_previous_page
                    }
                    edges {
                        cursor
                        node {
                            hash
                            batch_position
                            source
                            kind
                            timestamp
                            level
                            block
                            gas
                            counter
                            gas_limit
                            id
                            delegate
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6',
                first_seen: '2018-09-17T09:23:51Z',
                activated: '2018-07-15T13:01:27.000Z',
                public_key: null,
                delegations: {
                    page_info: {
                        start_cursor: 'ongTwy9dcZg35v84ur7rQcTgSA25Yj3rAm4acchPRutrQbLPrDX:1',
                        end_cursor: 'ongTwy9dcZg35v84ur7rQcTgSA25Yj3rAm4acchPRutrQbLPrDX:1',
                        has_next_page: false,
                        has_previous_page: true,
                    },
                    edges: [
                        {
                            cursor: 'ongTwy9dcZg35v84ur7rQcTgSA25Yj3rAm4acchPRutrQbLPrDX:1',
                            node: {
                                hash: 'ongTwy9dcZg35v84ur7rQcTgSA25Yj3rAm4acchPRutrQbLPrDX',
                                batch_position: 1,
                                source: 'tz1QAAxPbfTn5L9BbNsiUwjWDvMZQbAGgXND',
                                kind: 'delegation',
                                timestamp: '2019-10-27T12:46:11Z',
                                level: 668100,
                                block: 'BKyB7ypqUv2coNZLJ6StFxhRasJPwXwtrDycTVcRxGusAxPWWJs',
                                gas: 4335,
                                counter: '2219653',
                                gas_limit: '10600',
                                id: 15322110,
                                delegate: 'tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.delegations last and date_range gte/lte and order_by and before', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6") {
                address
                first_seen
                activated
                public_key
                delegations (
                last: 3
                order_by: {field: ID, direction: ASC}
                before:"ongTwy9dcZg35v84ur7rQcTgSA25Yj3rAm4acchPRutrQbLPrDX:1"
                date_range: {
                    gte: "2019-10-22T18:56:24Z"
                    lte: "2019-10-27T12:46:11Z"
                }
                ) {
                    page_info {
                        start_cursor
                        end_cursor
                        has_next_page
                        has_previous_page
                    }
                    edges {
                        cursor
                        node {
                            hash
                            batch_position
                            source
                            kind
                            timestamp
                            level
                            block
                            gas
                            counter
                            gas_limit
                            id
                            delegate
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6',
                first_seen: '2018-09-17T09:23:51Z',
                activated: '2018-07-15T13:01:27.000Z',
                public_key: null,
                delegations: {
                    page_info: {
                        start_cursor: 'ooqZeZuSdWTeo1zdSP887RqurGVnUE7dF9ZqqQMyorppaT7KBuP:1',
                        end_cursor: 'ooqZeZuSdWTeo1zdSP887RqurGVnUE7dF9ZqqQMyorppaT7KBuP:1',
                        has_next_page: true,
                        has_previous_page: false,
                    },
                    edges: [
                        {
                            cursor: 'ooqZeZuSdWTeo1zdSP887RqurGVnUE7dF9ZqqQMyorppaT7KBuP:1',
                            node: {
                                hash: 'ooqZeZuSdWTeo1zdSP887RqurGVnUE7dF9ZqqQMyorppaT7KBuP',
                                batch_position: 1,
                                source: 'tz1N5dH9DFu43oBD4Uu9d8jkGGatSRskqeGh',
                                kind: 'delegation',
                                timestamp: '2019-10-22T18:56:24Z',
                                level: 661451,
                                block: 'BMCgbqzH9uH48irmAkzrzf84LZfmLFNh3LVGGA3eWaEaDCR994x',
                                gas: 2720,
                                counter: '2186842',
                                gas_limit: '10600',
                                id: 15155828,
                                delegate: 'tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.delegations last and date_range gte/lte and order_by and after', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6") {
                address
                first_seen
                activated
                public_key
                delegations (
                last: 3
                order_by: {field: ID, direction: ASC}
                after:"ooqZeZuSdWTeo1zdSP887RqurGVnUE7dF9ZqqQMyorppaT7KBuP:1"
                date_range: {
                    gte: "2019-10-22T18:56:24Z"
                    lte: "2019-10-27T12:46:11Z"
                }
                ) {
                    page_info {
                        start_cursor
                        end_cursor
                        has_next_page
                        has_previous_page
                    }
                    edges {
                        cursor
                        node {
                            hash
                            batch_position
                            source
                            kind
                            timestamp
                            level
                            block
                            gas
                            counter
                            gas_limit
                            id
                            delegate
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6',
                first_seen: '2018-09-17T09:23:51Z',
                activated: '2018-07-15T13:01:27.000Z',
                public_key: null,
                delegations: {
                    page_info: {
                        start_cursor: 'ongTwy9dcZg35v84ur7rQcTgSA25Yj3rAm4acchPRutrQbLPrDX:1',
                        end_cursor: 'ongTwy9dcZg35v84ur7rQcTgSA25Yj3rAm4acchPRutrQbLPrDX:1',
                        has_next_page: false,
                        has_previous_page: true,
                    },
                    edges: [
                        {
                            cursor: 'ongTwy9dcZg35v84ur7rQcTgSA25Yj3rAm4acchPRutrQbLPrDX:1',
                            node: {
                                hash: 'ongTwy9dcZg35v84ur7rQcTgSA25Yj3rAm4acchPRutrQbLPrDX',
                                batch_position: 1,
                                source: 'tz1QAAxPbfTn5L9BbNsiUwjWDvMZQbAGgXND',
                                kind: 'delegation',
                                timestamp: '2019-10-27T12:46:11Z',
                                level: 668100,
                                block: 'BKyB7ypqUv2coNZLJ6StFxhRasJPwXwtrDycTVcRxGusAxPWWJs',
                                gas: 4335,
                                counter: '2219653',
                                gas_limit: '10600',
                                id: 15322110,
                                delegate: 'tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.delegations date_range gte/lte error', async () => {
        const GET_ACCOUNT = gql`
            query {
                account(address: "tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6") {
                    address
                    first_seen
                    activated
                    public_key
                    delegations (
                    first: 3
                    date_range: {
                        gte: "2019-10-22T18:56:24Z"
                        lte: "2019-10-21T12:46:11Z"
                    }
                    ) {
                        page_info {
                            start_cursor
                            end_cursor
                            has_next_page
                            has_previous_page
                        }
                        edges {
                            cursor
                            node {
                                hash
                                batch_position
                                source
                                kind
                                timestamp
                                level
                                block
                                gas
                                counter
                                gas_limit
                                id
                                delegate
                            }
                        }
                    }
                }
            }
            `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });
        if (res.errors == undefined) {
            throw new Error('Expected an error message however res.errors is undefined.');
        }

        expect(res.errors[0].message).toMatch('Argument "date" of type "DateRange?" must have the "lte" value greater than the "gte" value.');
    });

    test('account.delegations query limit error', async () => {
        const GET_ACCOUNT = gql`
            query {
                account(address: "tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6") {
                    delegations (
                    first: 201
                    ) {
                        page_info {
                            start_cursor
                            end_cursor
                            has_next_page
                            has_previous_page
                        }
                    }
                }
            }
            `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });
        if (res.errors == undefined) {
            throw new Error('Expected an error message however res.errors is undefined.');
        }

        expect(res.errors[0].message).toMatch('Argument \"limit\" of type \"Float?\" cannot be greater than 200.');
    });
});
