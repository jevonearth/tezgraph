import { gql } from 'apollo-server';

export const activateAccountAdded = gql`
subscription ($pkh: AddressFilter) {
  activateAccountAdded(pkh: $pkh) {
    kind,
    pkh,
    secret,
    metadata {
      balance_updates {
        kind,
        category,
        contract,
        delegate,
        cycle,
        change
      }
    }
  }
}
`;
