import { gql } from 'apollo-server';

export const doubleEndorsementEvidenceAdded = gql`
subscription ($delegate: AddressArrayFilter) {
  doubleEndorsementEvidenceAdded(delegate: $delegate) {
    kind,
    op1 {
      branch,
      operations {
        kind,
        level
      },
      signature
    },
    op2 {
      branch,
      operations {
        kind,
        level
      },
      signature
    },
    metadata {
      balance_updates {
        kind,
        category,
        contract,
        delegate,
        cycle,
        change
      }
    }
  }
}
`;
