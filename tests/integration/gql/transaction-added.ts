import { gql } from 'apollo-server';

export const transactionAdded = gql`
subscription ($source: AddressFilter, $destination: AddressFilter, $status: OperationResultStatusFilter) {
  transactionAdded(source: $source, destination: $destination, status: $status) {
        kind,
        source,
        fee,
        counter,
        gas_limit,
        storage_limit,
        amount,
        destination,
        parameters {
            entrypoint,
            value
        },
        metadata {
            balance_updates {
                kind,
                category,
                contract,
                delegate,
                cycle,
                change
            },
            internal_operation_results {
                kind,
                source,
                nonce,
                amount,
                destination,
                balance
            },
            operation_result {
                status,
                consumed_gas,
                errors {
                    kind,
                    id
                },
                storage,
                balance_updates {
                    kind,
                    category,
                    contract,
                    delegate,
                    cycle,
                    change
                },
                originated_contracts,
                storage_size,
                paid_storage_size_diff,
                allocated_destination_contract
            }
        }
    }
}
`;
