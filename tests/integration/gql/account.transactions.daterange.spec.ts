import 'reflect-metadata';
import { ApolloServer } from 'apollo-server-express';
import { gql } from 'apollo-server';
import { createTestClient } from 'apollo-server-testing';
import { AccountResolver } from '../../../src/resolvers/account';
import { buildSchema } from 'type-graphql';

async function startTestServer() {
    const testServer = new ApolloServer({
        schema: await buildSchema({
            resolvers: [AccountResolver],
        }),
        context: ({ req, res }) => ({ req, res }),
    });

    return testServer;
}

const server = startTestServer();

let testClientQuery: Function;

async function startTestClient() {
    if (!testClientQuery) {
        const { query } = createTestClient(await server);
        testClientQuery = await query;
    }
}

describe('Account Resolvers', () => {
    test('account.transactions - daterange format: 2019-06-13 16:22', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS") {
                address
                first_seen
                activated
                transactions(
                  last: 3
                  date_range: {
                      lte: "2020-02-27T17:55:56Z"
                      gte: "2019-06-13 16:22"
                  }
                  after: "onfyzsehpe5MpHPZK1iTGkFxqpuhG2Xny3L9X24VCBXNRDD27pg:0"
                ) {
                page_info {
                    start_cursor
                    end_cursor
                    has_next_page
                    has_previous_page
                }
                edges {
                        cursor
                        node {
                            hash
                            batch_position
                            source
                            kind
                            timestamp
                            level
                            block
                            gas
                            counter
                            gas_limit
                            storage_limit
                            amount
                            parameters
                            entrypoint
                            id
                            destination
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                first_seen: '2018-09-13T14:51:20Z',
                activated: '2018-09-13T14:49:20.000Z',
                transactions: {
                    page_info: {
                        start_cursor: 'oov7xyb6aNBGhZmsqbvGKbpWoYEDA6535V5xLMQXxQtBLVdFTmX:0',
                        end_cursor: 'oo1F42RgALBHR3ktYLq1yaeWzVFepNsuUGibaC5nWuGedjJJWci:0',
                        has_next_page: false,
                        has_previous_page: true,
                    },
                    edges: [
                        {
                            cursor: 'oov7xyb6aNBGhZmsqbvGKbpWoYEDA6535V5xLMQXxQtBLVdFTmX:0',
                            node: {
                                hash: 'oov7xyb6aNBGhZmsqbvGKbpWoYEDA6535V5xLMQXxQtBLVdFTmX',
                                batch_position: 0,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                kind: 'transaction',
                                timestamp: '2019-06-13T16:24:03Z',
                                level: 478915,
                                block: 'BM8xzgJhGS6Ti7fzDmMrM23yHTRnQMRSHnnmcdnmgnWraRLotmD',
                                gas: 1285,
                                counter: 'c2d0010000000000',
                                gas_limit: '3c28000000000000',
                                storage_limit: '',
                                amount: '277694173',
                                parameters: null,
                                entrypoint: null,
                                id: 10588016,
                                destination: 'KT1LdKDekzfeQEzTpj1yaJsXGfXUGhsexwq8',
                            },
                        },
                        {
                            cursor: 'oo1F42RgALBHR3ktYLq1yaeWzVFepNsuUGibaC5nWuGedjJJWci:0',
                            node: {
                                hash: 'oo1F42RgALBHR3ktYLq1yaeWzVFepNsuUGibaC5nWuGedjJJWci',
                                batch_position: 0,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                kind: 'transaction',
                                timestamp: '2019-06-13T16:22:03Z',
                                level: 478913,
                                block: 'BLQCC1ymavfJiSN9sW7zoz34smGtmexStE2Jq8p4qenNHhxXbua',
                                gas: 1284,
                                counter: 'c1d0010000000000',
                                gas_limit: '3c28000000000000',
                                storage_limit: '',
                                amount: '32933337',
                                parameters: null,
                                entrypoint: null,
                                id: 10587967,
                                destination: 'KT1SXkrJxbkuQ3i3XbreG1WFuG5C8RZsDkSC',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.transactions - daterange format: 2019-06-13 4:22PM', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS") {
                address
                first_seen
                activated
                transactions(
                  last: 3
                  date_range: {
                      lte: "2020-02-27T17:55:56Z"
                      gte: "2019-06-13 4:22PM"
                  }
                  after: "onfyzsehpe5MpHPZK1iTGkFxqpuhG2Xny3L9X24VCBXNRDD27pg:0"
                ) {
                page_info {
                    start_cursor
                    end_cursor
                    has_next_page
                    has_previous_page
                }
                edges {
                        cursor
                        node {
                            hash
                            batch_position
                            source
                            kind
                            timestamp
                            level
                            block
                            gas
                            counter
                            gas_limit
                            storage_limit
                            amount
                            parameters
                            entrypoint
                            id
                            destination
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                first_seen: '2018-09-13T14:51:20Z',
                activated: '2018-09-13T14:49:20.000Z',
                transactions: {
                    page_info: {
                        start_cursor: 'oov7xyb6aNBGhZmsqbvGKbpWoYEDA6535V5xLMQXxQtBLVdFTmX:0',
                        end_cursor: 'oo1F42RgALBHR3ktYLq1yaeWzVFepNsuUGibaC5nWuGedjJJWci:0',
                        has_next_page: false,
                        has_previous_page: true,
                    },
                    edges: [
                        {
                            cursor: 'oov7xyb6aNBGhZmsqbvGKbpWoYEDA6535V5xLMQXxQtBLVdFTmX:0',
                            node: {
                                hash: 'oov7xyb6aNBGhZmsqbvGKbpWoYEDA6535V5xLMQXxQtBLVdFTmX',
                                batch_position: 0,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                kind: 'transaction',
                                timestamp: '2019-06-13T16:24:03Z',
                                level: 478915,
                                block: 'BM8xzgJhGS6Ti7fzDmMrM23yHTRnQMRSHnnmcdnmgnWraRLotmD',
                                gas: 1285,
                                counter: 'c2d0010000000000',
                                gas_limit: '3c28000000000000',
                                storage_limit: '',
                                amount: '277694173',
                                parameters: null,
                                entrypoint: null,
                                id: 10588016,
                                destination: 'KT1LdKDekzfeQEzTpj1yaJsXGfXUGhsexwq8',
                            },
                        },
                        {
                            cursor: 'oo1F42RgALBHR3ktYLq1yaeWzVFepNsuUGibaC5nWuGedjJJWci:0',
                            node: {
                                hash: 'oo1F42RgALBHR3ktYLq1yaeWzVFepNsuUGibaC5nWuGedjJJWci',
                                batch_position: 0,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                kind: 'transaction',
                                timestamp: '2019-06-13T16:22:03Z',
                                level: 478913,
                                block: 'BLQCC1ymavfJiSN9sW7zoz34smGtmexStE2Jq8p4qenNHhxXbua',
                                gas: 1284,
                                counter: 'c1d0010000000000',
                                gas_limit: '3c28000000000000',
                                storage_limit: '',
                                amount: '32933337',
                                parameters: null,
                                entrypoint: null,
                                id: 10587967,
                                destination: 'KT1SXkrJxbkuQ3i3XbreG1WFuG5C8RZsDkSC',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.transactions - daterange format: 2019-06-13 4:22 PM', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS") {
                address
                first_seen
                activated
                transactions(
                  last: 3
                  date_range: {
                      lte: "2020-02-27T17:55:56Z"
                      gte: "2019-06-13 4:22 PM"
                  }
                  after: "onfyzsehpe5MpHPZK1iTGkFxqpuhG2Xny3L9X24VCBXNRDD27pg:0"
                ) {
                page_info {
                    start_cursor
                    end_cursor
                    has_next_page
                    has_previous_page
                }
                edges {
                        cursor
                        node {
                            hash
                            batch_position
                            source
                            kind
                            timestamp
                            level
                            block
                            gas
                            counter
                            gas_limit
                            storage_limit
                            amount
                            parameters
                            entrypoint
                            id
                            destination
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                first_seen: '2018-09-13T14:51:20Z',
                activated: '2018-09-13T14:49:20.000Z',
                transactions: {
                    page_info: {
                        start_cursor: 'oov7xyb6aNBGhZmsqbvGKbpWoYEDA6535V5xLMQXxQtBLVdFTmX:0',
                        end_cursor: 'oo1F42RgALBHR3ktYLq1yaeWzVFepNsuUGibaC5nWuGedjJJWci:0',
                        has_next_page: false,
                        has_previous_page: true,
                    },
                    edges: [
                        {
                            cursor: 'oov7xyb6aNBGhZmsqbvGKbpWoYEDA6535V5xLMQXxQtBLVdFTmX:0',
                            node: {
                                hash: 'oov7xyb6aNBGhZmsqbvGKbpWoYEDA6535V5xLMQXxQtBLVdFTmX',
                                batch_position: 0,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                kind: 'transaction',
                                timestamp: '2019-06-13T16:24:03Z',
                                level: 478915,
                                block: 'BM8xzgJhGS6Ti7fzDmMrM23yHTRnQMRSHnnmcdnmgnWraRLotmD',
                                gas: 1285,
                                counter: 'c2d0010000000000',
                                gas_limit: '3c28000000000000',
                                storage_limit: '',
                                amount: '277694173',
                                parameters: null,
                                entrypoint: null,
                                id: 10588016,
                                destination: 'KT1LdKDekzfeQEzTpj1yaJsXGfXUGhsexwq8',
                            },
                        },
                        {
                            cursor: 'oo1F42RgALBHR3ktYLq1yaeWzVFepNsuUGibaC5nWuGedjJJWci:0',
                            node: {
                                hash: 'oo1F42RgALBHR3ktYLq1yaeWzVFepNsuUGibaC5nWuGedjJJWci',
                                batch_position: 0,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                kind: 'transaction',
                                timestamp: '2019-06-13T16:22:03Z',
                                level: 478913,
                                block: 'BLQCC1ymavfJiSN9sW7zoz34smGtmexStE2Jq8p4qenNHhxXbua',
                                gas: 1284,
                                counter: 'c1d0010000000000',
                                gas_limit: '3c28000000000000',
                                storage_limit: '',
                                amount: '32933337',
                                parameters: null,
                                entrypoint: null,
                                id: 10587967,
                                destination: 'KT1SXkrJxbkuQ3i3XbreG1WFuG5C8RZsDkSC',
                            },
                        },
                    ],
                },
            },
        }));
    });
    test('account.transactions - daterange format: 2019 06-13 4:22PM', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS") {
                address
                first_seen
                activated
                transactions(
                  last: 3
                  date_range: {
                      lte: "2020-02-27T17:55:56Z"
                      gte: "2019 06-13 4:22PM"
                  }
                  after: "onfyzsehpe5MpHPZK1iTGkFxqpuhG2Xny3L9X24VCBXNRDD27pg:0"
                ) {
                page_info {
                    start_cursor
                    end_cursor
                    has_next_page
                    has_previous_page
                }
                edges {
                        cursor
                        node {
                            hash
                            batch_position
                            source
                            kind
                            timestamp
                            level
                            block
                            gas
                            counter
                            gas_limit
                            storage_limit
                            amount
                            parameters
                            entrypoint
                            id
                            destination
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                first_seen: '2018-09-13T14:51:20Z',
                activated: '2018-09-13T14:49:20.000Z',
                transactions: {
                    page_info: {
                        start_cursor: 'oov7xyb6aNBGhZmsqbvGKbpWoYEDA6535V5xLMQXxQtBLVdFTmX:0',
                        end_cursor: 'oo1F42RgALBHR3ktYLq1yaeWzVFepNsuUGibaC5nWuGedjJJWci:0',
                        has_next_page: false,
                        has_previous_page: true,
                    },
                    edges: [
                        {
                            cursor: 'oov7xyb6aNBGhZmsqbvGKbpWoYEDA6535V5xLMQXxQtBLVdFTmX:0',
                            node: {
                                hash: 'oov7xyb6aNBGhZmsqbvGKbpWoYEDA6535V5xLMQXxQtBLVdFTmX',
                                batch_position: 0,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                kind: 'transaction',
                                timestamp: '2019-06-13T16:24:03Z',
                                level: 478915,
                                block: 'BM8xzgJhGS6Ti7fzDmMrM23yHTRnQMRSHnnmcdnmgnWraRLotmD',
                                gas: 1285,
                                counter: 'c2d0010000000000',
                                gas_limit: '3c28000000000000',
                                storage_limit: '',
                                amount: '277694173',
                                parameters: null,
                                entrypoint: null,
                                id: 10588016,
                                destination: 'KT1LdKDekzfeQEzTpj1yaJsXGfXUGhsexwq8',
                            },
                        },
                        {
                            cursor: 'oo1F42RgALBHR3ktYLq1yaeWzVFepNsuUGibaC5nWuGedjJJWci:0',
                            node: {
                                hash: 'oo1F42RgALBHR3ktYLq1yaeWzVFepNsuUGibaC5nWuGedjJJWci',
                                batch_position: 0,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                kind: 'transaction',
                                timestamp: '2019-06-13T16:22:03Z',
                                level: 478913,
                                block: 'BLQCC1ymavfJiSN9sW7zoz34smGtmexStE2Jq8p4qenNHhxXbua',
                                gas: 1284,
                                counter: 'c1d0010000000000',
                                gas_limit: '3c28000000000000',
                                storage_limit: '',
                                amount: '32933337',
                                parameters: null,
                                entrypoint: null,
                                id: 10587967,
                                destination: 'KT1SXkrJxbkuQ3i3XbreG1WFuG5C8RZsDkSC',
                            },
                        },
                    ],
                },
            },
        }));
    });
    test('account.transactions - daterange format: 2019 06-13 4:22 PM', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS") {
                address
                first_seen
                activated
                transactions(
                  last: 3
                  date_range: {
                      lte: "2020-02-27T17:55:56Z"
                      gte: "2019 06-13 4:22 PM"
                  }
                  after: "onfyzsehpe5MpHPZK1iTGkFxqpuhG2Xny3L9X24VCBXNRDD27pg:0"
                ) {
                page_info {
                    start_cursor
                    end_cursor
                    has_next_page
                    has_previous_page
                }
                edges {
                        cursor
                        node {
                            hash
                            batch_position
                            source
                            kind
                            timestamp
                            level
                            block
                            gas
                            counter
                            gas_limit
                            storage_limit
                            amount
                            parameters
                            entrypoint
                            id
                            destination
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                first_seen: '2018-09-13T14:51:20Z',
                activated: '2018-09-13T14:49:20.000Z',
                transactions: {
                    page_info: {
                        start_cursor: 'oov7xyb6aNBGhZmsqbvGKbpWoYEDA6535V5xLMQXxQtBLVdFTmX:0',
                        end_cursor: 'oo1F42RgALBHR3ktYLq1yaeWzVFepNsuUGibaC5nWuGedjJJWci:0',
                        has_next_page: false,
                        has_previous_page: true,
                    },
                    edges: [
                        {
                            cursor: 'oov7xyb6aNBGhZmsqbvGKbpWoYEDA6535V5xLMQXxQtBLVdFTmX:0',
                            node: {
                                hash: 'oov7xyb6aNBGhZmsqbvGKbpWoYEDA6535V5xLMQXxQtBLVdFTmX',
                                batch_position: 0,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                kind: 'transaction',
                                timestamp: '2019-06-13T16:24:03Z',
                                level: 478915,
                                block: 'BM8xzgJhGS6Ti7fzDmMrM23yHTRnQMRSHnnmcdnmgnWraRLotmD',
                                gas: 1285,
                                counter: 'c2d0010000000000',
                                gas_limit: '3c28000000000000',
                                storage_limit: '',
                                amount: '277694173',
                                parameters: null,
                                entrypoint: null,
                                id: 10588016,
                                destination: 'KT1LdKDekzfeQEzTpj1yaJsXGfXUGhsexwq8',
                            },
                        },
                        {
                            cursor: 'oo1F42RgALBHR3ktYLq1yaeWzVFepNsuUGibaC5nWuGedjJJWci:0',
                            node: {
                                hash: 'oo1F42RgALBHR3ktYLq1yaeWzVFepNsuUGibaC5nWuGedjJJWci',
                                batch_position: 0,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                kind: 'transaction',
                                timestamp: '2019-06-13T16:22:03Z',
                                level: 478913,
                                block: 'BLQCC1ymavfJiSN9sW7zoz34smGtmexStE2Jq8p4qenNHhxXbua',
                                gas: 1284,
                                counter: 'c1d0010000000000',
                                gas_limit: '3c28000000000000',
                                storage_limit: '',
                                amount: '32933337',
                                parameters: null,
                                entrypoint: null,
                                id: 10587967,
                                destination: 'KT1SXkrJxbkuQ3i3XbreG1WFuG5C8RZsDkSC',
                            },
                        },
                    ],
                },
            },
        }));
    });
    test('account.transactions - daterange format: 2019 06/13 4:22PM', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS") {
                address
                first_seen
                activated
                transactions(
                  last: 3
                  date_range: {
                      lte: "2020-02-27T17:55:56Z"
                      gte: "2019 06/13 4:22PM"
                  }
                  after: "onfyzsehpe5MpHPZK1iTGkFxqpuhG2Xny3L9X24VCBXNRDD27pg:0"
                ) {
                page_info {
                    start_cursor
                    end_cursor
                    has_next_page
                    has_previous_page
                }
                edges {
                        cursor
                        node {
                            hash
                            batch_position
                            source
                            kind
                            timestamp
                            level
                            block
                            gas
                            counter
                            gas_limit
                            storage_limit
                            amount
                            parameters
                            entrypoint
                            id
                            destination
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                first_seen: '2018-09-13T14:51:20Z',
                activated: '2018-09-13T14:49:20.000Z',
                transactions: {
                    page_info: {
                        start_cursor: 'oov7xyb6aNBGhZmsqbvGKbpWoYEDA6535V5xLMQXxQtBLVdFTmX:0',
                        end_cursor: 'oo1F42RgALBHR3ktYLq1yaeWzVFepNsuUGibaC5nWuGedjJJWci:0',
                        has_next_page: false,
                        has_previous_page: true,
                    },
                    edges: [
                        {
                            cursor: 'oov7xyb6aNBGhZmsqbvGKbpWoYEDA6535V5xLMQXxQtBLVdFTmX:0',
                            node: {
                                hash: 'oov7xyb6aNBGhZmsqbvGKbpWoYEDA6535V5xLMQXxQtBLVdFTmX',
                                batch_position: 0,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                kind: 'transaction',
                                timestamp: '2019-06-13T16:24:03Z',
                                level: 478915,
                                block: 'BM8xzgJhGS6Ti7fzDmMrM23yHTRnQMRSHnnmcdnmgnWraRLotmD',
                                gas: 1285,
                                counter: 'c2d0010000000000',
                                gas_limit: '3c28000000000000',
                                storage_limit: '',
                                amount: '277694173',
                                parameters: null,
                                entrypoint: null,
                                id: 10588016,
                                destination: 'KT1LdKDekzfeQEzTpj1yaJsXGfXUGhsexwq8',
                            },
                        },
                        {
                            cursor: 'oo1F42RgALBHR3ktYLq1yaeWzVFepNsuUGibaC5nWuGedjJJWci:0',
                            node: {
                                hash: 'oo1F42RgALBHR3ktYLq1yaeWzVFepNsuUGibaC5nWuGedjJJWci',
                                batch_position: 0,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                kind: 'transaction',
                                timestamp: '2019-06-13T16:22:03Z',
                                level: 478913,
                                block: 'BLQCC1ymavfJiSN9sW7zoz34smGtmexStE2Jq8p4qenNHhxXbua',
                                gas: 1284,
                                counter: 'c1d0010000000000',
                                gas_limit: '3c28000000000000',
                                storage_limit: '',
                                amount: '32933337',
                                parameters: null,
                                entrypoint: null,
                                id: 10587967,
                                destination: 'KT1SXkrJxbkuQ3i3XbreG1WFuG5C8RZsDkSC',
                            },
                        },
                    ],
                },
            },
        }));
    });
    test('account.transactions - daterange format: 2019 06/13 4:22 PM', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS") {
                address
                first_seen
                activated
                transactions(
                  last: 3
                  date_range: {
                      lte: "2020-02-27T17:55:56Z"
                      gte: "2019 06/13 4:22 PM"
                  }
                  after: "onfyzsehpe5MpHPZK1iTGkFxqpuhG2Xny3L9X24VCBXNRDD27pg:0"
                ) {
                page_info {
                    start_cursor
                    end_cursor
                    has_next_page
                    has_previous_page
                }
                edges {
                        cursor
                        node {
                            hash
                            batch_position
                            source
                            kind
                            timestamp
                            level
                            block
                            gas
                            counter
                            gas_limit
                            storage_limit
                            amount
                            parameters
                            entrypoint
                            id
                            destination
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                first_seen: '2018-09-13T14:51:20Z',
                activated: '2018-09-13T14:49:20.000Z',
                transactions: {
                    page_info: {
                        start_cursor: 'oov7xyb6aNBGhZmsqbvGKbpWoYEDA6535V5xLMQXxQtBLVdFTmX:0',
                        end_cursor: 'oo1F42RgALBHR3ktYLq1yaeWzVFepNsuUGibaC5nWuGedjJJWci:0',
                        has_next_page: false,
                        has_previous_page: true,
                    },
                    edges: [
                        {
                            cursor: 'oov7xyb6aNBGhZmsqbvGKbpWoYEDA6535V5xLMQXxQtBLVdFTmX:0',
                            node: {
                                hash: 'oov7xyb6aNBGhZmsqbvGKbpWoYEDA6535V5xLMQXxQtBLVdFTmX',
                                batch_position: 0,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                kind: 'transaction',
                                timestamp: '2019-06-13T16:24:03Z',
                                level: 478915,
                                block: 'BM8xzgJhGS6Ti7fzDmMrM23yHTRnQMRSHnnmcdnmgnWraRLotmD',
                                gas: 1285,
                                counter: 'c2d0010000000000',
                                gas_limit: '3c28000000000000',
                                storage_limit: '',
                                amount: '277694173',
                                parameters: null,
                                entrypoint: null,
                                id: 10588016,
                                destination: 'KT1LdKDekzfeQEzTpj1yaJsXGfXUGhsexwq8',
                            },
                        },
                        {
                            cursor: 'oo1F42RgALBHR3ktYLq1yaeWzVFepNsuUGibaC5nWuGedjJJWci:0',
                            node: {
                                hash: 'oo1F42RgALBHR3ktYLq1yaeWzVFepNsuUGibaC5nWuGedjJJWci',
                                batch_position: 0,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                kind: 'transaction',
                                timestamp: '2019-06-13T16:22:03Z',
                                level: 478913,
                                block: 'BLQCC1ymavfJiSN9sW7zoz34smGtmexStE2Jq8p4qenNHhxXbua',
                                gas: 1284,
                                counter: 'c1d0010000000000',
                                gas_limit: '3c28000000000000',
                                storage_limit: '',
                                amount: '32933337',
                                parameters: null,
                                entrypoint: null,
                                id: 10587967,
                                destination: 'KT1SXkrJxbkuQ3i3XbreG1WFuG5C8RZsDkSC',
                            },
                        },
                    ],
                },
            },
        }));
    });
    test('account.transactions - daterange format: 2019/06/13 16:22', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS") {
                address
                first_seen
                activated
                transactions(
                  last: 3
                  date_range: {
                      lte: "2020-02-27T17:55:56Z"
                      gte: "2019/06/13 16:22"
                  }
                  after: "onfyzsehpe5MpHPZK1iTGkFxqpuhG2Xny3L9X24VCBXNRDD27pg:0"
                ) {
                page_info {
                    start_cursor
                    end_cursor
                    has_next_page
                    has_previous_page
                }
                edges {
                        cursor
                        node {
                            hash
                            batch_position
                            source
                            kind
                            timestamp
                            level
                            block
                            gas
                            counter
                            gas_limit
                            storage_limit
                            amount
                            parameters
                            entrypoint
                            id
                            destination
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                first_seen: '2018-09-13T14:51:20Z',
                activated: '2018-09-13T14:49:20.000Z',
                transactions: {
                    page_info: {
                        start_cursor: 'oov7xyb6aNBGhZmsqbvGKbpWoYEDA6535V5xLMQXxQtBLVdFTmX:0',
                        end_cursor: 'oo1F42RgALBHR3ktYLq1yaeWzVFepNsuUGibaC5nWuGedjJJWci:0',
                        has_next_page: false,
                        has_previous_page: true,
                    },
                    edges: [
                        {
                            cursor: 'oov7xyb6aNBGhZmsqbvGKbpWoYEDA6535V5xLMQXxQtBLVdFTmX:0',
                            node: {
                                hash: 'oov7xyb6aNBGhZmsqbvGKbpWoYEDA6535V5xLMQXxQtBLVdFTmX',
                                batch_position: 0,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                kind: 'transaction',
                                timestamp: '2019-06-13T16:24:03Z',
                                level: 478915,
                                block: 'BM8xzgJhGS6Ti7fzDmMrM23yHTRnQMRSHnnmcdnmgnWraRLotmD',
                                gas: 1285,
                                counter: 'c2d0010000000000',
                                gas_limit: '3c28000000000000',
                                storage_limit: '',
                                amount: '277694173',
                                parameters: null,
                                entrypoint: null,
                                id: 10588016,
                                destination: 'KT1LdKDekzfeQEzTpj1yaJsXGfXUGhsexwq8',
                            },
                        },
                        {
                            cursor: 'oo1F42RgALBHR3ktYLq1yaeWzVFepNsuUGibaC5nWuGedjJJWci:0',
                            node: {
                                hash: 'oo1F42RgALBHR3ktYLq1yaeWzVFepNsuUGibaC5nWuGedjJJWci',
                                batch_position: 0,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                kind: 'transaction',
                                timestamp: '2019-06-13T16:22:03Z',
                                level: 478913,
                                block: 'BLQCC1ymavfJiSN9sW7zoz34smGtmexStE2Jq8p4qenNHhxXbua',
                                gas: 1284,
                                counter: 'c1d0010000000000',
                                gas_limit: '3c28000000000000',
                                storage_limit: '',
                                amount: '32933337',
                                parameters: null,
                                entrypoint: null,
                                id: 10587967,
                                destination: 'KT1SXkrJxbkuQ3i3XbreG1WFuG5C8RZsDkSC',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.transactions - daterange format: 2019/06/13 4:22PM', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS") {
                address
                first_seen
                activated
                transactions(
                  last: 3
                  date_range: {
                      lte: "2020-02-27T17:55:56Z"
                      gte: "2019/06/13 4:22PM"
                  }
                  after: "onfyzsehpe5MpHPZK1iTGkFxqpuhG2Xny3L9X24VCBXNRDD27pg:0"
                ) {
                page_info {
                    start_cursor
                    end_cursor
                    has_next_page
                    has_previous_page
                }
                edges {
                        cursor
                        node {
                            hash
                            batch_position
                            source
                            kind
                            timestamp
                            level
                            block
                            gas
                            counter
                            gas_limit
                            storage_limit
                            amount
                            parameters
                            entrypoint
                            id
                            destination
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                first_seen: '2018-09-13T14:51:20Z',
                activated: '2018-09-13T14:49:20.000Z',
                transactions: {
                    page_info: {
                        start_cursor: 'oov7xyb6aNBGhZmsqbvGKbpWoYEDA6535V5xLMQXxQtBLVdFTmX:0',
                        end_cursor: 'oo1F42RgALBHR3ktYLq1yaeWzVFepNsuUGibaC5nWuGedjJJWci:0',
                        has_next_page: false,
                        has_previous_page: true,
                    },
                    edges: [
                        {
                            cursor: 'oov7xyb6aNBGhZmsqbvGKbpWoYEDA6535V5xLMQXxQtBLVdFTmX:0',
                            node: {
                                hash: 'oov7xyb6aNBGhZmsqbvGKbpWoYEDA6535V5xLMQXxQtBLVdFTmX',
                                batch_position: 0,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                kind: 'transaction',
                                timestamp: '2019-06-13T16:24:03Z',
                                level: 478915,
                                block: 'BM8xzgJhGS6Ti7fzDmMrM23yHTRnQMRSHnnmcdnmgnWraRLotmD',
                                gas: 1285,
                                counter: 'c2d0010000000000',
                                gas_limit: '3c28000000000000',
                                storage_limit: '',
                                amount: '277694173',
                                parameters: null,
                                entrypoint: null,
                                id: 10588016,
                                destination: 'KT1LdKDekzfeQEzTpj1yaJsXGfXUGhsexwq8',
                            },
                        },
                        {
                            cursor: 'oo1F42RgALBHR3ktYLq1yaeWzVFepNsuUGibaC5nWuGedjJJWci:0',
                            node: {
                                hash: 'oo1F42RgALBHR3ktYLq1yaeWzVFepNsuUGibaC5nWuGedjJJWci',
                                batch_position: 0,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                kind: 'transaction',
                                timestamp: '2019-06-13T16:22:03Z',
                                level: 478913,
                                block: 'BLQCC1ymavfJiSN9sW7zoz34smGtmexStE2Jq8p4qenNHhxXbua',
                                gas: 1284,
                                counter: 'c1d0010000000000',
                                gas_limit: '3c28000000000000',
                                storage_limit: '',
                                amount: '32933337',
                                parameters: null,
                                entrypoint: null,
                                id: 10587967,
                                destination: 'KT1SXkrJxbkuQ3i3XbreG1WFuG5C8RZsDkSC',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.transactions - daterange format: 2019/06/13 4:22 PM', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS") {
                address
                first_seen
                activated
                transactions(
                  last: 3
                  date_range: {
                      lte: "2020-02-27T17:55:56Z"
                      gte: "2019/06/13 4:22 PM"
                  }
                  after: "onfyzsehpe5MpHPZK1iTGkFxqpuhG2Xny3L9X24VCBXNRDD27pg:0"
                ) {
                page_info {
                    start_cursor
                    end_cursor
                    has_next_page
                    has_previous_page
                }
                edges {
                        cursor
                        node {
                            hash
                            batch_position
                            source
                            kind
                            timestamp
                            level
                            block
                            gas
                            counter
                            gas_limit
                            storage_limit
                            amount
                            parameters
                            entrypoint
                            id
                            destination
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                first_seen: '2018-09-13T14:51:20Z',
                activated: '2018-09-13T14:49:20.000Z',
                transactions: {
                    page_info: {
                        start_cursor: 'oov7xyb6aNBGhZmsqbvGKbpWoYEDA6535V5xLMQXxQtBLVdFTmX:0',
                        end_cursor: 'oo1F42RgALBHR3ktYLq1yaeWzVFepNsuUGibaC5nWuGedjJJWci:0',
                        has_next_page: false,
                        has_previous_page: true,
                    },
                    edges: [
                        {
                            cursor: 'oov7xyb6aNBGhZmsqbvGKbpWoYEDA6535V5xLMQXxQtBLVdFTmX:0',
                            node: {
                                hash: 'oov7xyb6aNBGhZmsqbvGKbpWoYEDA6535V5xLMQXxQtBLVdFTmX',
                                batch_position: 0,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                kind: 'transaction',
                                timestamp: '2019-06-13T16:24:03Z',
                                level: 478915,
                                block: 'BM8xzgJhGS6Ti7fzDmMrM23yHTRnQMRSHnnmcdnmgnWraRLotmD',
                                gas: 1285,
                                counter: 'c2d0010000000000',
                                gas_limit: '3c28000000000000',
                                storage_limit: '',
                                amount: '277694173',
                                parameters: null,
                                entrypoint: null,
                                id: 10588016,
                                destination: 'KT1LdKDekzfeQEzTpj1yaJsXGfXUGhsexwq8',
                            },
                        },
                        {
                            cursor: 'oo1F42RgALBHR3ktYLq1yaeWzVFepNsuUGibaC5nWuGedjJJWci:0',
                            node: {
                                hash: 'oo1F42RgALBHR3ktYLq1yaeWzVFepNsuUGibaC5nWuGedjJJWci',
                                batch_position: 0,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                kind: 'transaction',
                                timestamp: '2019-06-13T16:22:03Z',
                                level: 478913,
                                block: 'BLQCC1ymavfJiSN9sW7zoz34smGtmexStE2Jq8p4qenNHhxXbua',
                                gas: 1284,
                                counter: 'c1d0010000000000',
                                gas_limit: '3c28000000000000',
                                storage_limit: '',
                                amount: '32933337',
                                parameters: null,
                                entrypoint: null,
                                id: 10587967,
                                destination: 'KT1SXkrJxbkuQ3i3XbreG1WFuG5C8RZsDkSC',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.transactions - daterange format: 2019 06 13 16:22', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS") {
                address
                first_seen
                activated
                transactions(
                  last: 3
                  date_range: {
                      lte: "2020-02-27T17:55:56Z"
                      gte: "2019 06 13 16:22"
                  }
                  after: "onfyzsehpe5MpHPZK1iTGkFxqpuhG2Xny3L9X24VCBXNRDD27pg:0"
                ) {
                page_info {
                    start_cursor
                    end_cursor
                    has_next_page
                    has_previous_page
                }
                edges {
                        cursor
                        node {
                            hash
                            batch_position
                            source
                            kind
                            timestamp
                            level
                            block
                            gas
                            counter
                            gas_limit
                            storage_limit
                            amount
                            parameters
                            entrypoint
                            id
                            destination
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                first_seen: '2018-09-13T14:51:20Z',
                activated: '2018-09-13T14:49:20.000Z',
                transactions: {
                    page_info: {
                        start_cursor: 'oov7xyb6aNBGhZmsqbvGKbpWoYEDA6535V5xLMQXxQtBLVdFTmX:0',
                        end_cursor: 'oo1F42RgALBHR3ktYLq1yaeWzVFepNsuUGibaC5nWuGedjJJWci:0',
                        has_next_page: false,
                        has_previous_page: true,
                    },
                    edges: [
                        {
                            cursor: 'oov7xyb6aNBGhZmsqbvGKbpWoYEDA6535V5xLMQXxQtBLVdFTmX:0',
                            node: {
                                hash: 'oov7xyb6aNBGhZmsqbvGKbpWoYEDA6535V5xLMQXxQtBLVdFTmX',
                                batch_position: 0,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                kind: 'transaction',
                                timestamp: '2019-06-13T16:24:03Z',
                                level: 478915,
                                block: 'BM8xzgJhGS6Ti7fzDmMrM23yHTRnQMRSHnnmcdnmgnWraRLotmD',
                                gas: 1285,
                                counter: 'c2d0010000000000',
                                gas_limit: '3c28000000000000',
                                storage_limit: '',
                                amount: '277694173',
                                parameters: null,
                                entrypoint: null,
                                id: 10588016,
                                destination: 'KT1LdKDekzfeQEzTpj1yaJsXGfXUGhsexwq8',
                            },
                        },
                        {
                            cursor: 'oo1F42RgALBHR3ktYLq1yaeWzVFepNsuUGibaC5nWuGedjJJWci:0',
                            node: {
                                hash: 'oo1F42RgALBHR3ktYLq1yaeWzVFepNsuUGibaC5nWuGedjJJWci',
                                batch_position: 0,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                kind: 'transaction',
                                timestamp: '2019-06-13T16:22:03Z',
                                level: 478913,
                                block: 'BLQCC1ymavfJiSN9sW7zoz34smGtmexStE2Jq8p4qenNHhxXbua',
                                gas: 1284,
                                counter: 'c1d0010000000000',
                                gas_limit: '3c28000000000000',
                                storage_limit: '',
                                amount: '32933337',
                                parameters: null,
                                entrypoint: null,
                                id: 10587967,
                                destination: 'KT1SXkrJxbkuQ3i3XbreG1WFuG5C8RZsDkSC',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.transactions - daterange format: 2019 06 13 4:22PM', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS") {
                address
                first_seen
                activated
                transactions(
                  last: 3
                  date_range: {
                      lte: "2020-02-27T17:55:56Z"
                      gte: "2019 06 13 4:22PM"
                  }
                  after: "onfyzsehpe5MpHPZK1iTGkFxqpuhG2Xny3L9X24VCBXNRDD27pg:0"
                ) {
                page_info {
                    start_cursor
                    end_cursor
                    has_next_page
                    has_previous_page
                }
                edges {
                        cursor
                        node {
                            hash
                            batch_position
                            source
                            kind
                            timestamp
                            level
                            block
                            gas
                            counter
                            gas_limit
                            storage_limit
                            amount
                            parameters
                            entrypoint
                            id
                            destination
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                first_seen: '2018-09-13T14:51:20Z',
                activated: '2018-09-13T14:49:20.000Z',
                transactions: {
                    page_info: {
                        start_cursor: 'oov7xyb6aNBGhZmsqbvGKbpWoYEDA6535V5xLMQXxQtBLVdFTmX:0',
                        end_cursor: 'oo1F42RgALBHR3ktYLq1yaeWzVFepNsuUGibaC5nWuGedjJJWci:0',
                        has_next_page: false,
                        has_previous_page: true,
                    },
                    edges: [
                        {
                            cursor: 'oov7xyb6aNBGhZmsqbvGKbpWoYEDA6535V5xLMQXxQtBLVdFTmX:0',
                            node: {
                                hash: 'oov7xyb6aNBGhZmsqbvGKbpWoYEDA6535V5xLMQXxQtBLVdFTmX',
                                batch_position: 0,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                kind: 'transaction',
                                timestamp: '2019-06-13T16:24:03Z',
                                level: 478915,
                                block: 'BM8xzgJhGS6Ti7fzDmMrM23yHTRnQMRSHnnmcdnmgnWraRLotmD',
                                gas: 1285,
                                counter: 'c2d0010000000000',
                                gas_limit: '3c28000000000000',
                                storage_limit: '',
                                amount: '277694173',
                                parameters: null,
                                entrypoint: null,
                                id: 10588016,
                                destination: 'KT1LdKDekzfeQEzTpj1yaJsXGfXUGhsexwq8',
                            },
                        },
                        {
                            cursor: 'oo1F42RgALBHR3ktYLq1yaeWzVFepNsuUGibaC5nWuGedjJJWci:0',
                            node: {
                                hash: 'oo1F42RgALBHR3ktYLq1yaeWzVFepNsuUGibaC5nWuGedjJJWci',
                                batch_position: 0,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                kind: 'transaction',
                                timestamp: '2019-06-13T16:22:03Z',
                                level: 478913,
                                block: 'BLQCC1ymavfJiSN9sW7zoz34smGtmexStE2Jq8p4qenNHhxXbua',
                                gas: 1284,
                                counter: 'c1d0010000000000',
                                gas_limit: '3c28000000000000',
                                storage_limit: '',
                                amount: '32933337',
                                parameters: null,
                                entrypoint: null,
                                id: 10587967,
                                destination: 'KT1SXkrJxbkuQ3i3XbreG1WFuG5C8RZsDkSC',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.transactions - daterange format: 2019 06 13 4:22 PM', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS") {
                address
                first_seen
                activated
                transactions(
                  last: 3
                  date_range: {
                      lte: "2020-02-27T17:55:56Z"
                      gte: "2019 06 13 4:22 PM"
                  }
                  after: "onfyzsehpe5MpHPZK1iTGkFxqpuhG2Xny3L9X24VCBXNRDD27pg:0"
                ) {
                page_info {
                    start_cursor
                    end_cursor
                    has_next_page
                    has_previous_page
                }
                edges {
                        cursor
                        node {
                            hash
                            batch_position
                            source
                            kind
                            timestamp
                            level
                            block
                            gas
                            counter
                            gas_limit
                            storage_limit
                            amount
                            parameters
                            entrypoint
                            id
                            destination
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                first_seen: '2018-09-13T14:51:20Z',
                activated: '2018-09-13T14:49:20.000Z',
                transactions: {
                    page_info: {
                        start_cursor: 'oov7xyb6aNBGhZmsqbvGKbpWoYEDA6535V5xLMQXxQtBLVdFTmX:0',
                        end_cursor: 'oo1F42RgALBHR3ktYLq1yaeWzVFepNsuUGibaC5nWuGedjJJWci:0',
                        has_next_page: false,
                        has_previous_page: true,
                    },
                    edges: [
                        {
                            cursor: 'oov7xyb6aNBGhZmsqbvGKbpWoYEDA6535V5xLMQXxQtBLVdFTmX:0',
                            node: {
                                hash: 'oov7xyb6aNBGhZmsqbvGKbpWoYEDA6535V5xLMQXxQtBLVdFTmX',
                                batch_position: 0,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                kind: 'transaction',
                                timestamp: '2019-06-13T16:24:03Z',
                                level: 478915,
                                block: 'BM8xzgJhGS6Ti7fzDmMrM23yHTRnQMRSHnnmcdnmgnWraRLotmD',
                                gas: 1285,
                                counter: 'c2d0010000000000',
                                gas_limit: '3c28000000000000',
                                storage_limit: '',
                                amount: '277694173',
                                parameters: null,
                                entrypoint: null,
                                id: 10588016,
                                destination: 'KT1LdKDekzfeQEzTpj1yaJsXGfXUGhsexwq8',
                            },
                        },
                        {
                            cursor: 'oo1F42RgALBHR3ktYLq1yaeWzVFepNsuUGibaC5nWuGedjJJWci:0',
                            node: {
                                hash: 'oo1F42RgALBHR3ktYLq1yaeWzVFepNsuUGibaC5nWuGedjJJWci',
                                batch_position: 0,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                kind: 'transaction',
                                timestamp: '2019-06-13T16:22:03Z',
                                level: 478913,
                                block: 'BLQCC1ymavfJiSN9sW7zoz34smGtmexStE2Jq8p4qenNHhxXbua',
                                gas: 1284,
                                counter: 'c1d0010000000000',
                                gas_limit: '3c28000000000000',
                                storage_limit: '',
                                amount: '32933337',
                                parameters: null,
                                entrypoint: null,
                                id: 10587967,
                                destination: 'KT1SXkrJxbkuQ3i3XbreG1WFuG5C8RZsDkSC',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.transactions - daterange format: 06 13 2019 16:22', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS") {
                address
                first_seen
                activated
                transactions(
                  last: 3
                  date_range: {
                      lte: "2020-02-27T17:55:56Z"
                      gte: "06 13 2019 16:22"
                  }
                  after: "onfyzsehpe5MpHPZK1iTGkFxqpuhG2Xny3L9X24VCBXNRDD27pg:0"
                ) {
                page_info {
                    start_cursor
                    end_cursor
                    has_next_page
                    has_previous_page
                }
                edges {
                        cursor
                        node {
                            hash
                            batch_position
                            source
                            kind
                            timestamp
                            level
                            block
                            gas
                            counter
                            gas_limit
                            storage_limit
                            amount
                            parameters
                            entrypoint
                            id
                            destination
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                first_seen: '2018-09-13T14:51:20Z',
                activated: '2018-09-13T14:49:20.000Z',
                transactions: {
                    page_info: {
                        start_cursor: 'oov7xyb6aNBGhZmsqbvGKbpWoYEDA6535V5xLMQXxQtBLVdFTmX:0',
                        end_cursor: 'oo1F42RgALBHR3ktYLq1yaeWzVFepNsuUGibaC5nWuGedjJJWci:0',
                        has_next_page: false,
                        has_previous_page: true,
                    },
                    edges: [
                        {
                            cursor: 'oov7xyb6aNBGhZmsqbvGKbpWoYEDA6535V5xLMQXxQtBLVdFTmX:0',
                            node: {
                                hash: 'oov7xyb6aNBGhZmsqbvGKbpWoYEDA6535V5xLMQXxQtBLVdFTmX',
                                batch_position: 0,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                kind: 'transaction',
                                timestamp: '2019-06-13T16:24:03Z',
                                level: 478915,
                                block: 'BM8xzgJhGS6Ti7fzDmMrM23yHTRnQMRSHnnmcdnmgnWraRLotmD',
                                gas: 1285,
                                counter: 'c2d0010000000000',
                                gas_limit: '3c28000000000000',
                                storage_limit: '',
                                amount: '277694173',
                                parameters: null,
                                entrypoint: null,
                                id: 10588016,
                                destination: 'KT1LdKDekzfeQEzTpj1yaJsXGfXUGhsexwq8',
                            },
                        },
                        {
                            cursor: 'oo1F42RgALBHR3ktYLq1yaeWzVFepNsuUGibaC5nWuGedjJJWci:0',
                            node: {
                                hash: 'oo1F42RgALBHR3ktYLq1yaeWzVFepNsuUGibaC5nWuGedjJJWci',
                                batch_position: 0,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                kind: 'transaction',
                                timestamp: '2019-06-13T16:22:03Z',
                                level: 478913,
                                block: 'BLQCC1ymavfJiSN9sW7zoz34smGtmexStE2Jq8p4qenNHhxXbua',
                                gas: 1284,
                                counter: 'c1d0010000000000',
                                gas_limit: '3c28000000000000',
                                storage_limit: '',
                                amount: '32933337',
                                parameters: null,
                                entrypoint: null,
                                id: 10587967,
                                destination: 'KT1SXkrJxbkuQ3i3XbreG1WFuG5C8RZsDkSC',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.transactions - daterange format: 06 13 2019 4:22PM', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS") {
                address
                first_seen
                activated
                transactions(
                  last: 3
                  date_range: {
                      lte: "2020-02-27T17:55:56Z"
                      gte: "06 13 2019 4:22PM"
                  }
                  after: "onfyzsehpe5MpHPZK1iTGkFxqpuhG2Xny3L9X24VCBXNRDD27pg:0"
                ) {
                page_info {
                    start_cursor
                    end_cursor
                    has_next_page
                    has_previous_page
                }
                edges {
                        cursor
                        node {
                            hash
                            batch_position
                            source
                            kind
                            timestamp
                            level
                            block
                            gas
                            counter
                            gas_limit
                            storage_limit
                            amount
                            parameters
                            entrypoint
                            id
                            destination
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                first_seen: '2018-09-13T14:51:20Z',
                activated: '2018-09-13T14:49:20.000Z',
                transactions: {
                    page_info: {
                        start_cursor: 'oov7xyb6aNBGhZmsqbvGKbpWoYEDA6535V5xLMQXxQtBLVdFTmX:0',
                        end_cursor: 'oo1F42RgALBHR3ktYLq1yaeWzVFepNsuUGibaC5nWuGedjJJWci:0',
                        has_next_page: false,
                        has_previous_page: true,
                    },
                    edges: [
                        {
                            cursor: 'oov7xyb6aNBGhZmsqbvGKbpWoYEDA6535V5xLMQXxQtBLVdFTmX:0',
                            node: {
                                hash: 'oov7xyb6aNBGhZmsqbvGKbpWoYEDA6535V5xLMQXxQtBLVdFTmX',
                                batch_position: 0,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                kind: 'transaction',
                                timestamp: '2019-06-13T16:24:03Z',
                                level: 478915,
                                block: 'BM8xzgJhGS6Ti7fzDmMrM23yHTRnQMRSHnnmcdnmgnWraRLotmD',
                                gas: 1285,
                                counter: 'c2d0010000000000',
                                gas_limit: '3c28000000000000',
                                storage_limit: '',
                                amount: '277694173',
                                parameters: null,
                                entrypoint: null,
                                id: 10588016,
                                destination: 'KT1LdKDekzfeQEzTpj1yaJsXGfXUGhsexwq8',
                            },
                        },
                        {
                            cursor: 'oo1F42RgALBHR3ktYLq1yaeWzVFepNsuUGibaC5nWuGedjJJWci:0',
                            node: {
                                hash: 'oo1F42RgALBHR3ktYLq1yaeWzVFepNsuUGibaC5nWuGedjJJWci',
                                batch_position: 0,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                kind: 'transaction',
                                timestamp: '2019-06-13T16:22:03Z',
                                level: 478913,
                                block: 'BLQCC1ymavfJiSN9sW7zoz34smGtmexStE2Jq8p4qenNHhxXbua',
                                gas: 1284,
                                counter: 'c1d0010000000000',
                                gas_limit: '3c28000000000000',
                                storage_limit: '',
                                amount: '32933337',
                                parameters: null,
                                entrypoint: null,
                                id: 10587967,
                                destination: 'KT1SXkrJxbkuQ3i3XbreG1WFuG5C8RZsDkSC',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.transactions - daterange format: 06 13 2019 4:22 PM', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS") {
                address
                first_seen
                activated
                transactions(
                  last: 3
                  date_range: {
                      lte: "2020-02-27T17:55:56Z"
                      gte: "06 13 2019 4:22 PM"
                  }
                  after: "onfyzsehpe5MpHPZK1iTGkFxqpuhG2Xny3L9X24VCBXNRDD27pg:0"
                ) {
                page_info {
                    start_cursor
                    end_cursor
                    has_next_page
                    has_previous_page
                }
                edges {
                        cursor
                        node {
                            hash
                            batch_position
                            source
                            kind
                            timestamp
                            level
                            block
                            gas
                            counter
                            gas_limit
                            storage_limit
                            amount
                            parameters
                            entrypoint
                            id
                            destination
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                first_seen: '2018-09-13T14:51:20Z',
                activated: '2018-09-13T14:49:20.000Z',
                transactions: {
                    page_info: {
                        start_cursor: 'oov7xyb6aNBGhZmsqbvGKbpWoYEDA6535V5xLMQXxQtBLVdFTmX:0',
                        end_cursor: 'oo1F42RgALBHR3ktYLq1yaeWzVFepNsuUGibaC5nWuGedjJJWci:0',
                        has_next_page: false,
                        has_previous_page: true,
                    },
                    edges: [
                        {
                            cursor: 'oov7xyb6aNBGhZmsqbvGKbpWoYEDA6535V5xLMQXxQtBLVdFTmX:0',
                            node: {
                                hash: 'oov7xyb6aNBGhZmsqbvGKbpWoYEDA6535V5xLMQXxQtBLVdFTmX',
                                batch_position: 0,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                kind: 'transaction',
                                timestamp: '2019-06-13T16:24:03Z',
                                level: 478915,
                                block: 'BM8xzgJhGS6Ti7fzDmMrM23yHTRnQMRSHnnmcdnmgnWraRLotmD',
                                gas: 1285,
                                counter: 'c2d0010000000000',
                                gas_limit: '3c28000000000000',
                                storage_limit: '',
                                amount: '277694173',
                                parameters: null,
                                entrypoint: null,
                                id: 10588016,
                                destination: 'KT1LdKDekzfeQEzTpj1yaJsXGfXUGhsexwq8',
                            },
                        },
                        {
                            cursor: 'oo1F42RgALBHR3ktYLq1yaeWzVFepNsuUGibaC5nWuGedjJJWci:0',
                            node: {
                                hash: 'oo1F42RgALBHR3ktYLq1yaeWzVFepNsuUGibaC5nWuGedjJJWci',
                                batch_position: 0,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                kind: 'transaction',
                                timestamp: '2019-06-13T16:22:03Z',
                                level: 478913,
                                block: 'BLQCC1ymavfJiSN9sW7zoz34smGtmexStE2Jq8p4qenNHhxXbua',
                                gas: 1284,
                                counter: 'c1d0010000000000',
                                gas_limit: '3c28000000000000',
                                storage_limit: '',
                                amount: '32933337',
                                parameters: null,
                                entrypoint: null,
                                id: 10587967,
                                destination: 'KT1SXkrJxbkuQ3i3XbreG1WFuG5C8RZsDkSC',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.transactions - daterange format: 06-13-2019 16:22', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS") {
                address
                first_seen
                activated
                transactions(
                  last: 3
                  date_range: {
                      lte: "2020-02-27T17:55:56Z"
                      gte: "06-13-2019 16:22"
                  }
                  after: "onfyzsehpe5MpHPZK1iTGkFxqpuhG2Xny3L9X24VCBXNRDD27pg:0"
                ) {
                page_info {
                    start_cursor
                    end_cursor
                    has_next_page
                    has_previous_page
                }
                edges {
                        cursor
                        node {
                            hash
                            batch_position
                            source
                            kind
                            timestamp
                            level
                            block
                            gas
                            counter
                            gas_limit
                            storage_limit
                            amount
                            parameters
                            entrypoint
                            id
                            destination
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                first_seen: '2018-09-13T14:51:20Z',
                activated: '2018-09-13T14:49:20.000Z',
                transactions: {
                    page_info: {
                        start_cursor: 'oov7xyb6aNBGhZmsqbvGKbpWoYEDA6535V5xLMQXxQtBLVdFTmX:0',
                        end_cursor: 'oo1F42RgALBHR3ktYLq1yaeWzVFepNsuUGibaC5nWuGedjJJWci:0',
                        has_next_page: false,
                        has_previous_page: true,
                    },
                    edges: [
                        {
                            cursor: 'oov7xyb6aNBGhZmsqbvGKbpWoYEDA6535V5xLMQXxQtBLVdFTmX:0',
                            node: {
                                hash: 'oov7xyb6aNBGhZmsqbvGKbpWoYEDA6535V5xLMQXxQtBLVdFTmX',
                                batch_position: 0,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                kind: 'transaction',
                                timestamp: '2019-06-13T16:24:03Z',
                                level: 478915,
                                block: 'BM8xzgJhGS6Ti7fzDmMrM23yHTRnQMRSHnnmcdnmgnWraRLotmD',
                                gas: 1285,
                                counter: 'c2d0010000000000',
                                gas_limit: '3c28000000000000',
                                storage_limit: '',
                                amount: '277694173',
                                parameters: null,
                                entrypoint: null,
                                id: 10588016,
                                destination: 'KT1LdKDekzfeQEzTpj1yaJsXGfXUGhsexwq8',
                            },
                        },
                        {
                            cursor: 'oo1F42RgALBHR3ktYLq1yaeWzVFepNsuUGibaC5nWuGedjJJWci:0',
                            node: {
                                hash: 'oo1F42RgALBHR3ktYLq1yaeWzVFepNsuUGibaC5nWuGedjJJWci',
                                batch_position: 0,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                kind: 'transaction',
                                timestamp: '2019-06-13T16:22:03Z',
                                level: 478913,
                                block: 'BLQCC1ymavfJiSN9sW7zoz34smGtmexStE2Jq8p4qenNHhxXbua',
                                gas: 1284,
                                counter: 'c1d0010000000000',
                                gas_limit: '3c28000000000000',
                                storage_limit: '',
                                amount: '32933337',
                                parameters: null,
                                entrypoint: null,
                                id: 10587967,
                                destination: 'KT1SXkrJxbkuQ3i3XbreG1WFuG5C8RZsDkSC',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.transactions - daterange format: 06-13-2019 4:22PM', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS") {
                address
                first_seen
                activated
                transactions(
                  last: 3
                  date_range: {
                      lte: "2020-02-27T17:55:56Z"
                      gte: "06-13-2019 4:22PM"
                  }
                  after: "onfyzsehpe5MpHPZK1iTGkFxqpuhG2Xny3L9X24VCBXNRDD27pg:0"
                ) {
                page_info {
                    start_cursor
                    end_cursor
                    has_next_page
                    has_previous_page
                }
                edges {
                        cursor
                        node {
                            hash
                            batch_position
                            source
                            kind
                            timestamp
                            level
                            block
                            gas
                            counter
                            gas_limit
                            storage_limit
                            amount
                            parameters
                            entrypoint
                            id
                            destination
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                first_seen: '2018-09-13T14:51:20Z',
                activated: '2018-09-13T14:49:20.000Z',
                transactions: {
                    page_info: {
                        start_cursor: 'oov7xyb6aNBGhZmsqbvGKbpWoYEDA6535V5xLMQXxQtBLVdFTmX:0',
                        end_cursor: 'oo1F42RgALBHR3ktYLq1yaeWzVFepNsuUGibaC5nWuGedjJJWci:0',
                        has_next_page: false,
                        has_previous_page: true,
                    },
                    edges: [
                        {
                            cursor: 'oov7xyb6aNBGhZmsqbvGKbpWoYEDA6535V5xLMQXxQtBLVdFTmX:0',
                            node: {
                                hash: 'oov7xyb6aNBGhZmsqbvGKbpWoYEDA6535V5xLMQXxQtBLVdFTmX',
                                batch_position: 0,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                kind: 'transaction',
                                timestamp: '2019-06-13T16:24:03Z',
                                level: 478915,
                                block: 'BM8xzgJhGS6Ti7fzDmMrM23yHTRnQMRSHnnmcdnmgnWraRLotmD',
                                gas: 1285,
                                counter: 'c2d0010000000000',
                                gas_limit: '3c28000000000000',
                                storage_limit: '',
                                amount: '277694173',
                                parameters: null,
                                entrypoint: null,
                                id: 10588016,
                                destination: 'KT1LdKDekzfeQEzTpj1yaJsXGfXUGhsexwq8',
                            },
                        },
                        {
                            cursor: 'oo1F42RgALBHR3ktYLq1yaeWzVFepNsuUGibaC5nWuGedjJJWci:0',
                            node: {
                                hash: 'oo1F42RgALBHR3ktYLq1yaeWzVFepNsuUGibaC5nWuGedjJJWci',
                                batch_position: 0,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                kind: 'transaction',
                                timestamp: '2019-06-13T16:22:03Z',
                                level: 478913,
                                block: 'BLQCC1ymavfJiSN9sW7zoz34smGtmexStE2Jq8p4qenNHhxXbua',
                                gas: 1284,
                                counter: 'c1d0010000000000',
                                gas_limit: '3c28000000000000',
                                storage_limit: '',
                                amount: '32933337',
                                parameters: null,
                                entrypoint: null,
                                id: 10587967,
                                destination: 'KT1SXkrJxbkuQ3i3XbreG1WFuG5C8RZsDkSC',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.transactions - daterange format: 06-13-2019 4:22 PM', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS") {
                address
                first_seen
                activated
                transactions(
                  last: 3
                  date_range: {
                      lte: "2020-02-27T17:55:56Z"
                      gte: "06-13-2019 4:22 PM"
                  }
                  after: "onfyzsehpe5MpHPZK1iTGkFxqpuhG2Xny3L9X24VCBXNRDD27pg:0"
                ) {
                page_info {
                    start_cursor
                    end_cursor
                    has_next_page
                    has_previous_page
                }
                edges {
                        cursor
                        node {
                            hash
                            batch_position
                            source
                            kind
                            timestamp
                            level
                            block
                            gas
                            counter
                            gas_limit
                            storage_limit
                            amount
                            parameters
                            entrypoint
                            id
                            destination
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                first_seen: '2018-09-13T14:51:20Z',
                activated: '2018-09-13T14:49:20.000Z',
                transactions: {
                    page_info: {
                        start_cursor: 'oov7xyb6aNBGhZmsqbvGKbpWoYEDA6535V5xLMQXxQtBLVdFTmX:0',
                        end_cursor: 'oo1F42RgALBHR3ktYLq1yaeWzVFepNsuUGibaC5nWuGedjJJWci:0',
                        has_next_page: false,
                        has_previous_page: true,
                    },
                    edges: [
                        {
                            cursor: 'oov7xyb6aNBGhZmsqbvGKbpWoYEDA6535V5xLMQXxQtBLVdFTmX:0',
                            node: {
                                hash: 'oov7xyb6aNBGhZmsqbvGKbpWoYEDA6535V5xLMQXxQtBLVdFTmX',
                                batch_position: 0,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                kind: 'transaction',
                                timestamp: '2019-06-13T16:24:03Z',
                                level: 478915,
                                block: 'BM8xzgJhGS6Ti7fzDmMrM23yHTRnQMRSHnnmcdnmgnWraRLotmD',
                                gas: 1285,
                                counter: 'c2d0010000000000',
                                gas_limit: '3c28000000000000',
                                storage_limit: '',
                                amount: '277694173',
                                parameters: null,
                                entrypoint: null,
                                id: 10588016,
                                destination: 'KT1LdKDekzfeQEzTpj1yaJsXGfXUGhsexwq8',
                            },
                        },
                        {
                            cursor: 'oo1F42RgALBHR3ktYLq1yaeWzVFepNsuUGibaC5nWuGedjJJWci:0',
                            node: {
                                hash: 'oo1F42RgALBHR3ktYLq1yaeWzVFepNsuUGibaC5nWuGedjJJWci',
                                batch_position: 0,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                kind: 'transaction',
                                timestamp: '2019-06-13T16:22:03Z',
                                level: 478913,
                                block: 'BLQCC1ymavfJiSN9sW7zoz34smGtmexStE2Jq8p4qenNHhxXbua',
                                gas: 1284,
                                counter: 'c1d0010000000000',
                                gas_limit: '3c28000000000000',
                                storage_limit: '',
                                amount: '32933337',
                                parameters: null,
                                entrypoint: null,
                                id: 10587967,
                                destination: 'KT1SXkrJxbkuQ3i3XbreG1WFuG5C8RZsDkSC',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.transactions - daterange format: 06/13/2019 16:22', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS") {
                address
                first_seen
                activated
                transactions(
                  last: 3
                  date_range: {
                      lte: "2020-02-27T17:55:56Z"
                      gte: "06/13/2019 16:22"
                  }
                  after: "onfyzsehpe5MpHPZK1iTGkFxqpuhG2Xny3L9X24VCBXNRDD27pg:0"
                ) {
                page_info {
                    start_cursor
                    end_cursor
                    has_next_page
                    has_previous_page
                }
                edges {
                        cursor
                        node {
                            hash
                            batch_position
                            source
                            kind
                            timestamp
                            level
                            block
                            gas
                            counter
                            gas_limit
                            storage_limit
                            amount
                            parameters
                            entrypoint
                            id
                            destination
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                first_seen: '2018-09-13T14:51:20Z',
                activated: '2018-09-13T14:49:20.000Z',
                transactions: {
                    page_info: {
                        start_cursor: 'oov7xyb6aNBGhZmsqbvGKbpWoYEDA6535V5xLMQXxQtBLVdFTmX:0',
                        end_cursor: 'oo1F42RgALBHR3ktYLq1yaeWzVFepNsuUGibaC5nWuGedjJJWci:0',
                        has_next_page: false,
                        has_previous_page: true,
                    },
                    edges: [
                        {
                            cursor: 'oov7xyb6aNBGhZmsqbvGKbpWoYEDA6535V5xLMQXxQtBLVdFTmX:0',
                            node: {
                                hash: 'oov7xyb6aNBGhZmsqbvGKbpWoYEDA6535V5xLMQXxQtBLVdFTmX',
                                batch_position: 0,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                kind: 'transaction',
                                timestamp: '2019-06-13T16:24:03Z',
                                level: 478915,
                                block: 'BM8xzgJhGS6Ti7fzDmMrM23yHTRnQMRSHnnmcdnmgnWraRLotmD',
                                gas: 1285,
                                counter: 'c2d0010000000000',
                                gas_limit: '3c28000000000000',
                                storage_limit: '',
                                amount: '277694173',
                                parameters: null,
                                entrypoint: null,
                                id: 10588016,
                                destination: 'KT1LdKDekzfeQEzTpj1yaJsXGfXUGhsexwq8',
                            },
                        },
                        {
                            cursor: 'oo1F42RgALBHR3ktYLq1yaeWzVFepNsuUGibaC5nWuGedjJJWci:0',
                            node: {
                                hash: 'oo1F42RgALBHR3ktYLq1yaeWzVFepNsuUGibaC5nWuGedjJJWci',
                                batch_position: 0,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                kind: 'transaction',
                                timestamp: '2019-06-13T16:22:03Z',
                                level: 478913,
                                block: 'BLQCC1ymavfJiSN9sW7zoz34smGtmexStE2Jq8p4qenNHhxXbua',
                                gas: 1284,
                                counter: 'c1d0010000000000',
                                gas_limit: '3c28000000000000',
                                storage_limit: '',
                                amount: '32933337',
                                parameters: null,
                                entrypoint: null,
                                id: 10587967,
                                destination: 'KT1SXkrJxbkuQ3i3XbreG1WFuG5C8RZsDkSC',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.transactions - daterange format: 06/13/2019 4:22PM', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS") {
                address
                first_seen
                activated
                transactions(
                  last: 3
                  date_range: {
                      lte: "2020-02-27T17:55:56Z"
                      gte: "06/13/2019 4:22PM"
                  }
                  after: "onfyzsehpe5MpHPZK1iTGkFxqpuhG2Xny3L9X24VCBXNRDD27pg:0"
                ) {
                page_info {
                    start_cursor
                    end_cursor
                    has_next_page
                    has_previous_page
                }
                edges {
                        cursor
                        node {
                            hash
                            batch_position
                            source
                            kind
                            timestamp
                            level
                            block
                            gas
                            counter
                            gas_limit
                            storage_limit
                            amount
                            parameters
                            entrypoint
                            id
                            destination
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                first_seen: '2018-09-13T14:51:20Z',
                activated: '2018-09-13T14:49:20.000Z',
                transactions: {
                    page_info: {
                        start_cursor: 'oov7xyb6aNBGhZmsqbvGKbpWoYEDA6535V5xLMQXxQtBLVdFTmX:0',
                        end_cursor: 'oo1F42RgALBHR3ktYLq1yaeWzVFepNsuUGibaC5nWuGedjJJWci:0',
                        has_next_page: false,
                        has_previous_page: true,
                    },
                    edges: [
                        {
                            cursor: 'oov7xyb6aNBGhZmsqbvGKbpWoYEDA6535V5xLMQXxQtBLVdFTmX:0',
                            node: {
                                hash: 'oov7xyb6aNBGhZmsqbvGKbpWoYEDA6535V5xLMQXxQtBLVdFTmX',
                                batch_position: 0,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                kind: 'transaction',
                                timestamp: '2019-06-13T16:24:03Z',
                                level: 478915,
                                block: 'BM8xzgJhGS6Ti7fzDmMrM23yHTRnQMRSHnnmcdnmgnWraRLotmD',
                                gas: 1285,
                                counter: 'c2d0010000000000',
                                gas_limit: '3c28000000000000',
                                storage_limit: '',
                                amount: '277694173',
                                parameters: null,
                                entrypoint: null,
                                id: 10588016,
                                destination: 'KT1LdKDekzfeQEzTpj1yaJsXGfXUGhsexwq8',
                            },
                        },
                        {
                            cursor: 'oo1F42RgALBHR3ktYLq1yaeWzVFepNsuUGibaC5nWuGedjJJWci:0',
                            node: {
                                hash: 'oo1F42RgALBHR3ktYLq1yaeWzVFepNsuUGibaC5nWuGedjJJWci',
                                batch_position: 0,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                kind: 'transaction',
                                timestamp: '2019-06-13T16:22:03Z',
                                level: 478913,
                                block: 'BLQCC1ymavfJiSN9sW7zoz34smGtmexStE2Jq8p4qenNHhxXbua',
                                gas: 1284,
                                counter: 'c1d0010000000000',
                                gas_limit: '3c28000000000000',
                                storage_limit: '',
                                amount: '32933337',
                                parameters: null,
                                entrypoint: null,
                                id: 10587967,
                                destination: 'KT1SXkrJxbkuQ3i3XbreG1WFuG5C8RZsDkSC',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.transactions - daterange format: 06/13/2019 4:22 PM', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS") {
                address
                first_seen
                activated
                transactions(
                  last: 3
                  date_range: {
                      lte: "2020-02-27T17:55:56Z"
                      gte: "06/13/2019 4:22 PM"
                  }
                  after: "onfyzsehpe5MpHPZK1iTGkFxqpuhG2Xny3L9X24VCBXNRDD27pg:0"
                ) {
                page_info {
                    start_cursor
                    end_cursor
                    has_next_page
                    has_previous_page
                }
                edges {
                        cursor
                        node {
                            hash
                            batch_position
                            source
                            kind
                            timestamp
                            level
                            block
                            gas
                            counter
                            gas_limit
                            storage_limit
                            amount
                            parameters
                            entrypoint
                            id
                            destination
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                first_seen: '2018-09-13T14:51:20Z',
                activated: '2018-09-13T14:49:20.000Z',
                transactions: {
                    page_info: {
                        start_cursor: 'oov7xyb6aNBGhZmsqbvGKbpWoYEDA6535V5xLMQXxQtBLVdFTmX:0',
                        end_cursor: 'oo1F42RgALBHR3ktYLq1yaeWzVFepNsuUGibaC5nWuGedjJJWci:0',
                        has_next_page: false,
                        has_previous_page: true,
                    },
                    edges: [
                        {
                            cursor: 'oov7xyb6aNBGhZmsqbvGKbpWoYEDA6535V5xLMQXxQtBLVdFTmX:0',
                            node: {
                                hash: 'oov7xyb6aNBGhZmsqbvGKbpWoYEDA6535V5xLMQXxQtBLVdFTmX',
                                batch_position: 0,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                kind: 'transaction',
                                timestamp: '2019-06-13T16:24:03Z',
                                level: 478915,
                                block: 'BM8xzgJhGS6Ti7fzDmMrM23yHTRnQMRSHnnmcdnmgnWraRLotmD',
                                gas: 1285,
                                counter: 'c2d0010000000000',
                                gas_limit: '3c28000000000000',
                                storage_limit: '',
                                amount: '277694173',
                                parameters: null,
                                entrypoint: null,
                                id: 10588016,
                                destination: 'KT1LdKDekzfeQEzTpj1yaJsXGfXUGhsexwq8',
                            },
                        },
                        {
                            cursor: 'oo1F42RgALBHR3ktYLq1yaeWzVFepNsuUGibaC5nWuGedjJJWci:0',
                            node: {
                                hash: 'oo1F42RgALBHR3ktYLq1yaeWzVFepNsuUGibaC5nWuGedjJJWci',
                                batch_position: 0,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                kind: 'transaction',
                                timestamp: '2019-06-13T16:22:03Z',
                                level: 478913,
                                block: 'BLQCC1ymavfJiSN9sW7zoz34smGtmexStE2Jq8p4qenNHhxXbua',
                                gas: 1284,
                                counter: 'c1d0010000000000',
                                gas_limit: '3c28000000000000',
                                storage_limit: '',
                                amount: '32933337',
                                parameters: null,
                                entrypoint: null,
                                id: 10587967,
                                destination: 'KT1SXkrJxbkuQ3i3XbreG1WFuG5C8RZsDkSC',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.transactions - daterange format: 2020', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS") {
                address
                first_seen
                activated
                transactions(
                  last: 3
                  date_range: {
                      gte: "2020"
                      lte:"2020-02-27T18:07:36Z"
                  }
                ) {
                page_info {
                    start_cursor
                    end_cursor
                    has_next_page
                    has_previous_page
                }
                edges {
                        cursor
                        node {
                            hash
                            batch_position
                            source
                            kind
                            timestamp
                            level
                            block
                            gas
                            counter
                            gas_limit
                            storage_limit
                            amount
                            parameters
                            entrypoint
                            id
                            destination
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                first_seen: '2018-09-13T14:51:20Z',
                activated: '2018-09-13T14:49:20.000Z',
                transactions: {
                    page_info: {
                        start_cursor: 'ooZM6ErfewYXeUJGN4as8qiHPDT6u465tTuJKF6eQktXDkgtZKW:0',
                        end_cursor: 'oodK29zqUFb7NyMa5mRyb6DVR71FyWHh5gXF8WVgCDG6uGVuCaT:0',
                        has_next_page: false,
                        has_previous_page: true,
                    },
                    edges: [
                        {
                            cursor: 'ooZM6ErfewYXeUJGN4as8qiHPDT6u465tTuJKF6eQktXDkgtZKW:0',
                            node: {
                                hash: 'ooZM6ErfewYXeUJGN4as8qiHPDT6u465tTuJKF6eQktXDkgtZKW',
                                batch_position: 0,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                kind: 'transaction',
                                timestamp: '2020-02-27T17:55:56Z',
                                level: 842861,
                                block: 'BL7YnpqVo7tZEw3hhSVLYNf7RHLMNTRSopuu9CHmQuALvRbwVRP',
                                gas: 1283,
                                counter: 'cbd0010000000000',
                                gas_limit: '4328000000000000',
                                storage_limit: '1501000000000000',
                                amount: '10000',
                                parameters: '{ "prim": "Unit" }',
                                entrypoint: 'default',
                                id: 19920382,
                                destination: 'tz1QkeWxQgkHbmyjJuNKnTJBCNtUxtHtvbJU',
                            },
                        },
                        {
                            cursor: 'ooHN7P3X9pUeDyWWibvZqEaYEPic4xqASsZcxk4WMQ1n7hvojah:0',
                            node: {
                                hash: 'ooHN7P3X9pUeDyWWibvZqEaYEPic4xqASsZcxk4WMQ1n7hvojah',
                                batch_position: 0,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                kind: 'transaction',
                                timestamp: '2020-02-22T22:47:26Z',
                                level: 836082,
                                block: 'BM8ieLyATFHdtgvqhq6t9y9HacUMjhSpfHwSJNobRd2us13L5HR',
                                gas: 1284,
                                counter: 'cad0010000000000',
                                gas_limit: '4328000000000000',
                                storage_limit: '',
                                amount: '100000000',
                                parameters: '{ "prim": "Unit" }',
                                entrypoint: 'default',
                                id: 19731787,
                                destination: 'tz1RAWNEEFr2DSEDQVPnHXhH24GLMDF5XNjJ',
                            },
                        },
                        {
                            cursor: 'oodK29zqUFb7NyMa5mRyb6DVR71FyWHh5gXF8WVgCDG6uGVuCaT:0',
                            node: {
                                hash: 'oodK29zqUFb7NyMa5mRyb6DVR71FyWHh5gXF8WVgCDG6uGVuCaT',
                                batch_position: 0,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                kind: 'transaction',
                                timestamp: '2020-02-22T22:29:26Z',
                                level: 836064,
                                block: 'BKspGC8xE7CT8431tR5CErsn5dEVUB6eVtXLeCSpv4285z3rZa4',
                                gas: 1284,
                                counter: 'c9d0010000000000',
                                gas_limit: '4328000000000000',
                                storage_limit: '1501000000000000',
                                amount: '100000',
                                parameters: '{ "prim": "Unit" }',
                                entrypoint: 'default',
                                id: 19731302,
                                destination: 'tz1VPz5qhzTrRTNevHC33ArBTBPV6jHpwjQB',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.transactions - daterange format: 2020-01', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS") {
                address
                first_seen
                activated
                transactions(
                  last: 3
                  date_range: {
                      gte: "2020-01"
                      lte:"2020-02-27T18:07:36Z"
                  }
                ) {
                page_info {
                    start_cursor
                    end_cursor
                    has_next_page
                    has_previous_page
                }
                edges {
                        cursor
                        node {
                            hash
                            batch_position
                            source
                            kind
                            timestamp
                            level
                            block
                            gas
                            counter
                            gas_limit
                            storage_limit
                            amount
                            parameters
                            entrypoint
                            id
                            destination
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                first_seen: '2018-09-13T14:51:20Z',
                activated: '2018-09-13T14:49:20.000Z',
                transactions: {
                    page_info: {
                        start_cursor: 'ooZM6ErfewYXeUJGN4as8qiHPDT6u465tTuJKF6eQktXDkgtZKW:0',
                        end_cursor: 'oodK29zqUFb7NyMa5mRyb6DVR71FyWHh5gXF8WVgCDG6uGVuCaT:0',
                        has_next_page: false,
                        has_previous_page: true,
                    },
                    edges: [
                        {
                            cursor: 'ooZM6ErfewYXeUJGN4as8qiHPDT6u465tTuJKF6eQktXDkgtZKW:0',
                            node: {
                                hash: 'ooZM6ErfewYXeUJGN4as8qiHPDT6u465tTuJKF6eQktXDkgtZKW',
                                batch_position: 0,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                kind: 'transaction',
                                timestamp: '2020-02-27T17:55:56Z',
                                level: 842861,
                                block: 'BL7YnpqVo7tZEw3hhSVLYNf7RHLMNTRSopuu9CHmQuALvRbwVRP',
                                gas: 1283,
                                counter: 'cbd0010000000000',
                                gas_limit: '4328000000000000',
                                storage_limit: '1501000000000000',
                                amount: '10000',
                                parameters: '{ "prim": "Unit" }',
                                entrypoint: 'default',
                                id: 19920382,
                                destination: 'tz1QkeWxQgkHbmyjJuNKnTJBCNtUxtHtvbJU',
                            },
                        },
                        {
                            cursor: 'ooHN7P3X9pUeDyWWibvZqEaYEPic4xqASsZcxk4WMQ1n7hvojah:0',
                            node: {
                                hash: 'ooHN7P3X9pUeDyWWibvZqEaYEPic4xqASsZcxk4WMQ1n7hvojah',
                                batch_position: 0,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                kind: 'transaction',
                                timestamp: '2020-02-22T22:47:26Z',
                                level: 836082,
                                block: 'BM8ieLyATFHdtgvqhq6t9y9HacUMjhSpfHwSJNobRd2us13L5HR',
                                gas: 1284,
                                counter: 'cad0010000000000',
                                gas_limit: '4328000000000000',
                                storage_limit: '',
                                amount: '100000000',
                                parameters: '{ "prim": "Unit" }',
                                entrypoint: 'default',
                                id: 19731787,
                                destination: 'tz1RAWNEEFr2DSEDQVPnHXhH24GLMDF5XNjJ',
                            },
                        },
                        {
                            cursor: 'oodK29zqUFb7NyMa5mRyb6DVR71FyWHh5gXF8WVgCDG6uGVuCaT:0',
                            node: {
                                hash: 'oodK29zqUFb7NyMa5mRyb6DVR71FyWHh5gXF8WVgCDG6uGVuCaT',
                                batch_position: 0,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                kind: 'transaction',
                                timestamp: '2020-02-22T22:29:26Z',
                                level: 836064,
                                block: 'BKspGC8xE7CT8431tR5CErsn5dEVUB6eVtXLeCSpv4285z3rZa4',
                                gas: 1284,
                                counter: 'c9d0010000000000',
                                gas_limit: '4328000000000000',
                                storage_limit: '1501000000000000',
                                amount: '100000',
                                parameters: '{ "prim": "Unit" }',
                                entrypoint: 'default',
                                id: 19731302,
                                destination: 'tz1VPz5qhzTrRTNevHC33ArBTBPV6jHpwjQB',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.transactions - daterange format: 2020-01-01', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS") {
                address
                first_seen
                activated
                transactions(
                  last: 3
                  date_range: {
                      gte: "2020-01-01"
                      lte:"2020-02-27T18:07:36Z"
                  }
                ) {
                page_info {
                    start_cursor
                    end_cursor
                    has_next_page
                    has_previous_page
                }
                edges {
                        cursor
                        node {
                            hash
                            batch_position
                            source
                            kind
                            timestamp
                            level
                            block
                            gas
                            counter
                            gas_limit
                            storage_limit
                            amount
                            parameters
                            entrypoint
                            id
                            destination
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                first_seen: '2018-09-13T14:51:20Z',
                activated: '2018-09-13T14:49:20.000Z',
                transactions: {
                    page_info: {
                        start_cursor: 'ooZM6ErfewYXeUJGN4as8qiHPDT6u465tTuJKF6eQktXDkgtZKW:0',
                        end_cursor: 'oodK29zqUFb7NyMa5mRyb6DVR71FyWHh5gXF8WVgCDG6uGVuCaT:0',
                        has_next_page: false,
                        has_previous_page: true,
                    },
                    edges: [
                        {
                            cursor: 'ooZM6ErfewYXeUJGN4as8qiHPDT6u465tTuJKF6eQktXDkgtZKW:0',
                            node: {
                                hash: 'ooZM6ErfewYXeUJGN4as8qiHPDT6u465tTuJKF6eQktXDkgtZKW',
                                batch_position: 0,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                kind: 'transaction',
                                timestamp: '2020-02-27T17:55:56Z',
                                level: 842861,
                                block: 'BL7YnpqVo7tZEw3hhSVLYNf7RHLMNTRSopuu9CHmQuALvRbwVRP',
                                gas: 1283,
                                counter: 'cbd0010000000000',
                                gas_limit: '4328000000000000',
                                storage_limit: '1501000000000000',
                                amount: '10000',
                                parameters: '{ "prim": "Unit" }',
                                entrypoint: 'default',
                                id: 19920382,
                                destination: 'tz1QkeWxQgkHbmyjJuNKnTJBCNtUxtHtvbJU',
                            },
                        },
                        {
                            cursor: 'ooHN7P3X9pUeDyWWibvZqEaYEPic4xqASsZcxk4WMQ1n7hvojah:0',
                            node: {
                                hash: 'ooHN7P3X9pUeDyWWibvZqEaYEPic4xqASsZcxk4WMQ1n7hvojah',
                                batch_position: 0,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                kind: 'transaction',
                                timestamp: '2020-02-22T22:47:26Z',
                                level: 836082,
                                block: 'BM8ieLyATFHdtgvqhq6t9y9HacUMjhSpfHwSJNobRd2us13L5HR',
                                gas: 1284,
                                counter: 'cad0010000000000',
                                gas_limit: '4328000000000000',
                                storage_limit: '',
                                amount: '100000000',
                                parameters: '{ "prim": "Unit" }',
                                entrypoint: 'default',
                                id: 19731787,
                                destination: 'tz1RAWNEEFr2DSEDQVPnHXhH24GLMDF5XNjJ',
                            },
                        },
                        {
                            cursor: 'oodK29zqUFb7NyMa5mRyb6DVR71FyWHh5gXF8WVgCDG6uGVuCaT:0',
                            node: {
                                hash: 'oodK29zqUFb7NyMa5mRyb6DVR71FyWHh5gXF8WVgCDG6uGVuCaT',
                                batch_position: 0,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                kind: 'transaction',
                                timestamp: '2020-02-22T22:29:26Z',
                                level: 836064,
                                block: 'BKspGC8xE7CT8431tR5CErsn5dEVUB6eVtXLeCSpv4285z3rZa4',
                                gas: 1284,
                                counter: 'c9d0010000000000',
                                gas_limit: '4328000000000000',
                                storage_limit: '1501000000000000',
                                amount: '100000',
                                parameters: '{ "prim": "Unit" }',
                                entrypoint: 'default',
                                id: 19731302,
                                destination: 'tz1VPz5qhzTrRTNevHC33ArBTBPV6jHpwjQB',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.transactions - daterange format: 2020-01-01 00:', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS") {
                address
                first_seen
                activated
                transactions(
                  last: 3
                  date_range: {
                      gte: "2020-01-01 00:"
                      lte:"2020-02-27T18:07:36Z"
                  }
                ) {
                page_info {
                    start_cursor
                    end_cursor
                    has_next_page
                    has_previous_page
                }
                edges {
                        cursor
                        node {
                            hash
                            batch_position
                            source
                            kind
                            timestamp
                            level
                            block
                            gas
                            counter
                            gas_limit
                            storage_limit
                            amount
                            parameters
                            entrypoint
                            id
                            destination
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                first_seen: '2018-09-13T14:51:20Z',
                activated: '2018-09-13T14:49:20.000Z',
                transactions: {
                    page_info: {
                        start_cursor: 'ooZM6ErfewYXeUJGN4as8qiHPDT6u465tTuJKF6eQktXDkgtZKW:0',
                        end_cursor: 'oodK29zqUFb7NyMa5mRyb6DVR71FyWHh5gXF8WVgCDG6uGVuCaT:0',
                        has_next_page: false,
                        has_previous_page: true,
                    },
                    edges: [
                        {
                            cursor: 'ooZM6ErfewYXeUJGN4as8qiHPDT6u465tTuJKF6eQktXDkgtZKW:0',
                            node: {
                                hash: 'ooZM6ErfewYXeUJGN4as8qiHPDT6u465tTuJKF6eQktXDkgtZKW',
                                batch_position: 0,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                kind: 'transaction',
                                timestamp: '2020-02-27T17:55:56Z',
                                level: 842861,
                                block: 'BL7YnpqVo7tZEw3hhSVLYNf7RHLMNTRSopuu9CHmQuALvRbwVRP',
                                gas: 1283,
                                counter: 'cbd0010000000000',
                                gas_limit: '4328000000000000',
                                storage_limit: '1501000000000000',
                                amount: '10000',
                                parameters: '{ "prim": "Unit" }',
                                entrypoint: 'default',
                                id: 19920382,
                                destination: 'tz1QkeWxQgkHbmyjJuNKnTJBCNtUxtHtvbJU',
                            },
                        },
                        {
                            cursor: 'ooHN7P3X9pUeDyWWibvZqEaYEPic4xqASsZcxk4WMQ1n7hvojah:0',
                            node: {
                                hash: 'ooHN7P3X9pUeDyWWibvZqEaYEPic4xqASsZcxk4WMQ1n7hvojah',
                                batch_position: 0,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                kind: 'transaction',
                                timestamp: '2020-02-22T22:47:26Z',
                                level: 836082,
                                block: 'BM8ieLyATFHdtgvqhq6t9y9HacUMjhSpfHwSJNobRd2us13L5HR',
                                gas: 1284,
                                counter: 'cad0010000000000',
                                gas_limit: '4328000000000000',
                                storage_limit: '',
                                amount: '100000000',
                                parameters: '{ "prim": "Unit" }',
                                entrypoint: 'default',
                                id: 19731787,
                                destination: 'tz1RAWNEEFr2DSEDQVPnHXhH24GLMDF5XNjJ',
                            },
                        },
                        {
                            cursor: 'oodK29zqUFb7NyMa5mRyb6DVR71FyWHh5gXF8WVgCDG6uGVuCaT:0',
                            node: {
                                hash: 'oodK29zqUFb7NyMa5mRyb6DVR71FyWHh5gXF8WVgCDG6uGVuCaT',
                                batch_position: 0,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                kind: 'transaction',
                                timestamp: '2020-02-22T22:29:26Z',
                                level: 836064,
                                block: 'BKspGC8xE7CT8431tR5CErsn5dEVUB6eVtXLeCSpv4285z3rZa4',
                                gas: 1284,
                                counter: 'c9d0010000000000',
                                gas_limit: '4328000000000000',
                                storage_limit: '1501000000000000',
                                amount: '100000',
                                parameters: '{ "prim": "Unit" }',
                                entrypoint: 'default',
                                id: 19731302,
                                destination: 'tz1VPz5qhzTrRTNevHC33ArBTBPV6jHpwjQB',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.transactions - daterange format: 2020-01-01 00:00', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS") {
                address
                first_seen
                activated
                transactions(
                  last: 3
                  date_range: {
                      gte: "2020-01-01 00:00"
                      lte:"2020-02-27T18:07:36Z"
                  }
                ) {
                page_info {
                    start_cursor
                    end_cursor
                    has_next_page
                    has_previous_page
                }
                edges {
                        cursor
                        node {
                            hash
                            batch_position
                            source
                            kind
                            timestamp
                            level
                            block
                            gas
                            counter
                            gas_limit
                            storage_limit
                            amount
                            parameters
                            entrypoint
                            id
                            destination
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                first_seen: '2018-09-13T14:51:20Z',
                activated: '2018-09-13T14:49:20.000Z',
                transactions: {
                    page_info: {
                        start_cursor: 'ooZM6ErfewYXeUJGN4as8qiHPDT6u465tTuJKF6eQktXDkgtZKW:0',
                        end_cursor: 'oodK29zqUFb7NyMa5mRyb6DVR71FyWHh5gXF8WVgCDG6uGVuCaT:0',
                        has_next_page: false,
                        has_previous_page: true,
                    },
                    edges: [
                        {
                            cursor: 'ooZM6ErfewYXeUJGN4as8qiHPDT6u465tTuJKF6eQktXDkgtZKW:0',
                            node: {
                                hash: 'ooZM6ErfewYXeUJGN4as8qiHPDT6u465tTuJKF6eQktXDkgtZKW',
                                batch_position: 0,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                kind: 'transaction',
                                timestamp: '2020-02-27T17:55:56Z',
                                level: 842861,
                                block: 'BL7YnpqVo7tZEw3hhSVLYNf7RHLMNTRSopuu9CHmQuALvRbwVRP',
                                gas: 1283,
                                counter: 'cbd0010000000000',
                                gas_limit: '4328000000000000',
                                storage_limit: '1501000000000000',
                                amount: '10000',
                                parameters: '{ "prim": "Unit" }',
                                entrypoint: 'default',
                                id: 19920382,
                                destination: 'tz1QkeWxQgkHbmyjJuNKnTJBCNtUxtHtvbJU',
                            },
                        },
                        {
                            cursor: 'ooHN7P3X9pUeDyWWibvZqEaYEPic4xqASsZcxk4WMQ1n7hvojah:0',
                            node: {
                                hash: 'ooHN7P3X9pUeDyWWibvZqEaYEPic4xqASsZcxk4WMQ1n7hvojah',
                                batch_position: 0,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                kind: 'transaction',
                                timestamp: '2020-02-22T22:47:26Z',
                                level: 836082,
                                block: 'BM8ieLyATFHdtgvqhq6t9y9HacUMjhSpfHwSJNobRd2us13L5HR',
                                gas: 1284,
                                counter: 'cad0010000000000',
                                gas_limit: '4328000000000000',
                                storage_limit: '',
                                amount: '100000000',
                                parameters: '{ "prim": "Unit" }',
                                entrypoint: 'default',
                                id: 19731787,
                                destination: 'tz1RAWNEEFr2DSEDQVPnHXhH24GLMDF5XNjJ',
                            },
                        },
                        {
                            cursor: 'oodK29zqUFb7NyMa5mRyb6DVR71FyWHh5gXF8WVgCDG6uGVuCaT:0',
                            node: {
                                hash: 'oodK29zqUFb7NyMa5mRyb6DVR71FyWHh5gXF8WVgCDG6uGVuCaT',
                                batch_position: 0,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                kind: 'transaction',
                                timestamp: '2020-02-22T22:29:26Z',
                                level: 836064,
                                block: 'BKspGC8xE7CT8431tR5CErsn5dEVUB6eVtXLeCSpv4285z3rZa4',
                                gas: 1284,
                                counter: 'c9d0010000000000',
                                gas_limit: '4328000000000000',
                                storage_limit: '1501000000000000',
                                amount: '100000',
                                parameters: '{ "prim": "Unit" }',
                                entrypoint: 'default',
                                id: 19731302,
                                destination: 'tz1VPz5qhzTrRTNevHC33ArBTBPV6jHpwjQB',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.transactions - daterange format: 2020-01-01 00:00:00', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS") {
                address
                first_seen
                activated
                transactions(
                  last: 3
                  date_range: {
                      gte: "2020-01-01 00:00:00"
                      lte:"2020-02-27T18:07:36Z"
                  }
                ) {
                page_info {
                    start_cursor
                    end_cursor
                    has_next_page
                    has_previous_page
                }
                edges {
                        cursor
                        node {
                            hash
                            batch_position
                            source
                            kind
                            timestamp
                            level
                            block
                            gas
                            counter
                            gas_limit
                            storage_limit
                            amount
                            parameters
                            entrypoint
                            id
                            destination
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                first_seen: '2018-09-13T14:51:20Z',
                activated: '2018-09-13T14:49:20.000Z',
                transactions: {
                    page_info: {
                        start_cursor: 'ooZM6ErfewYXeUJGN4as8qiHPDT6u465tTuJKF6eQktXDkgtZKW:0',
                        end_cursor: 'oodK29zqUFb7NyMa5mRyb6DVR71FyWHh5gXF8WVgCDG6uGVuCaT:0',
                        has_next_page: false,
                        has_previous_page: true,
                    },
                    edges: [
                        {
                            cursor: 'ooZM6ErfewYXeUJGN4as8qiHPDT6u465tTuJKF6eQktXDkgtZKW:0',
                            node: {
                                hash: 'ooZM6ErfewYXeUJGN4as8qiHPDT6u465tTuJKF6eQktXDkgtZKW',
                                batch_position: 0,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                kind: 'transaction',
                                timestamp: '2020-02-27T17:55:56Z',
                                level: 842861,
                                block: 'BL7YnpqVo7tZEw3hhSVLYNf7RHLMNTRSopuu9CHmQuALvRbwVRP',
                                gas: 1283,
                                counter: 'cbd0010000000000',
                                gas_limit: '4328000000000000',
                                storage_limit: '1501000000000000',
                                amount: '10000',
                                parameters: '{ "prim": "Unit" }',
                                entrypoint: 'default',
                                id: 19920382,
                                destination: 'tz1QkeWxQgkHbmyjJuNKnTJBCNtUxtHtvbJU',
                            },
                        },
                        {
                            cursor: 'ooHN7P3X9pUeDyWWibvZqEaYEPic4xqASsZcxk4WMQ1n7hvojah:0',
                            node: {
                                hash: 'ooHN7P3X9pUeDyWWibvZqEaYEPic4xqASsZcxk4WMQ1n7hvojah',
                                batch_position: 0,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                kind: 'transaction',
                                timestamp: '2020-02-22T22:47:26Z',
                                level: 836082,
                                block: 'BM8ieLyATFHdtgvqhq6t9y9HacUMjhSpfHwSJNobRd2us13L5HR',
                                gas: 1284,
                                counter: 'cad0010000000000',
                                gas_limit: '4328000000000000',
                                storage_limit: '',
                                amount: '100000000',
                                parameters: '{ "prim": "Unit" }',
                                entrypoint: 'default',
                                id: 19731787,
                                destination: 'tz1RAWNEEFr2DSEDQVPnHXhH24GLMDF5XNjJ',
                            },
                        },
                        {
                            cursor: 'oodK29zqUFb7NyMa5mRyb6DVR71FyWHh5gXF8WVgCDG6uGVuCaT:0',
                            node: {
                                hash: 'oodK29zqUFb7NyMa5mRyb6DVR71FyWHh5gXF8WVgCDG6uGVuCaT',
                                batch_position: 0,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                kind: 'transaction',
                                timestamp: '2020-02-22T22:29:26Z',
                                level: 836064,
                                block: 'BKspGC8xE7CT8431tR5CErsn5dEVUB6eVtXLeCSpv4285z3rZa4',
                                gas: 1284,
                                counter: 'c9d0010000000000',
                                gas_limit: '4328000000000000',
                                storage_limit: '1501000000000000',
                                amount: '100000',
                                parameters: '{ "prim": "Unit" }',
                                entrypoint: 'default',
                                id: 19731302,
                                destination: 'tz1VPz5qhzTrRTNevHC33ArBTBPV6jHpwjQB',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.transactions - daterange format: 2020-01-01 00:00:00.000', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS") {
                address
                first_seen
                activated
                transactions(
                  last: 3
                  date_range: {
                      gte: "2020-01-01 00:00:00.000"
                      lte:"2020-02-27T18:07:36Z"
                  }
                ) {
                page_info {
                    start_cursor
                    end_cursor
                    has_next_page
                    has_previous_page
                }
                edges {
                        cursor
                        node {
                            hash
                            batch_position
                            source
                            kind
                            timestamp
                            level
                            block
                            gas
                            counter
                            gas_limit
                            storage_limit
                            amount
                            parameters
                            entrypoint
                            id
                            destination
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                first_seen: '2018-09-13T14:51:20Z',
                activated: '2018-09-13T14:49:20.000Z',
                transactions: {
                    page_info: {
                        start_cursor: 'ooZM6ErfewYXeUJGN4as8qiHPDT6u465tTuJKF6eQktXDkgtZKW:0',
                        end_cursor: 'oodK29zqUFb7NyMa5mRyb6DVR71FyWHh5gXF8WVgCDG6uGVuCaT:0',
                        has_next_page: false,
                        has_previous_page: true,
                    },
                    edges: [
                        {
                            cursor: 'ooZM6ErfewYXeUJGN4as8qiHPDT6u465tTuJKF6eQktXDkgtZKW:0',
                            node: {
                                hash: 'ooZM6ErfewYXeUJGN4as8qiHPDT6u465tTuJKF6eQktXDkgtZKW',
                                batch_position: 0,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                kind: 'transaction',
                                timestamp: '2020-02-27T17:55:56Z',
                                level: 842861,
                                block: 'BL7YnpqVo7tZEw3hhSVLYNf7RHLMNTRSopuu9CHmQuALvRbwVRP',
                                gas: 1283,
                                counter: 'cbd0010000000000',
                                gas_limit: '4328000000000000',
                                storage_limit: '1501000000000000',
                                amount: '10000',
                                parameters: '{ "prim": "Unit" }',
                                entrypoint: 'default',
                                id: 19920382,
                                destination: 'tz1QkeWxQgkHbmyjJuNKnTJBCNtUxtHtvbJU',
                            },
                        },
                        {
                            cursor: 'ooHN7P3X9pUeDyWWibvZqEaYEPic4xqASsZcxk4WMQ1n7hvojah:0',
                            node: {
                                hash: 'ooHN7P3X9pUeDyWWibvZqEaYEPic4xqASsZcxk4WMQ1n7hvojah',
                                batch_position: 0,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                kind: 'transaction',
                                timestamp: '2020-02-22T22:47:26Z',
                                level: 836082,
                                block: 'BM8ieLyATFHdtgvqhq6t9y9HacUMjhSpfHwSJNobRd2us13L5HR',
                                gas: 1284,
                                counter: 'cad0010000000000',
                                gas_limit: '4328000000000000',
                                storage_limit: '',
                                amount: '100000000',
                                parameters: '{ "prim": "Unit" }',
                                entrypoint: 'default',
                                id: 19731787,
                                destination: 'tz1RAWNEEFr2DSEDQVPnHXhH24GLMDF5XNjJ',
                            },
                        },
                        {
                            cursor: 'oodK29zqUFb7NyMa5mRyb6DVR71FyWHh5gXF8WVgCDG6uGVuCaT:0',
                            node: {
                                hash: 'oodK29zqUFb7NyMa5mRyb6DVR71FyWHh5gXF8WVgCDG6uGVuCaT',
                                batch_position: 0,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                kind: 'transaction',
                                timestamp: '2020-02-22T22:29:26Z',
                                level: 836064,
                                block: 'BKspGC8xE7CT8431tR5CErsn5dEVUB6eVtXLeCSpv4285z3rZa4',
                                gas: 1284,
                                counter: 'c9d0010000000000',
                                gas_limit: '4328000000000000',
                                storage_limit: '1501000000000000',
                                amount: '100000',
                                parameters: '{ "prim": "Unit" }',
                                entrypoint: 'default',
                                id: 19731302,
                                destination: 'tz1VPz5qhzTrRTNevHC33ArBTBPV6jHpwjQB',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.transactions - daterange format: 2020/01', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS") {
                address
                first_seen
                activated
                transactions(
                  last: 3
                  date_range: {
                      gte: "2020/01"
                      lte:"2020-02-27T18:07:36Z"
                  }
                ) {
                page_info {
                    start_cursor
                    end_cursor
                    has_next_page
                    has_previous_page
                }
                edges {
                        cursor
                        node {
                            hash
                            batch_position
                            source
                            kind
                            timestamp
                            level
                            block
                            gas
                            counter
                            gas_limit
                            storage_limit
                            amount
                            parameters
                            entrypoint
                            id
                            destination
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                first_seen: '2018-09-13T14:51:20Z',
                activated: '2018-09-13T14:49:20.000Z',
                transactions: {
                    page_info: {
                        start_cursor: 'ooZM6ErfewYXeUJGN4as8qiHPDT6u465tTuJKF6eQktXDkgtZKW:0',
                        end_cursor: 'oodK29zqUFb7NyMa5mRyb6DVR71FyWHh5gXF8WVgCDG6uGVuCaT:0',
                        has_next_page: false,
                        has_previous_page: true,
                    },
                    edges: [
                        {
                            cursor: 'ooZM6ErfewYXeUJGN4as8qiHPDT6u465tTuJKF6eQktXDkgtZKW:0',
                            node: {
                                hash: 'ooZM6ErfewYXeUJGN4as8qiHPDT6u465tTuJKF6eQktXDkgtZKW',
                                batch_position: 0,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                kind: 'transaction',
                                timestamp: '2020-02-27T17:55:56Z',
                                level: 842861,
                                block: 'BL7YnpqVo7tZEw3hhSVLYNf7RHLMNTRSopuu9CHmQuALvRbwVRP',
                                gas: 1283,
                                counter: 'cbd0010000000000',
                                gas_limit: '4328000000000000',
                                storage_limit: '1501000000000000',
                                amount: '10000',
                                parameters: '{ "prim": "Unit" }',
                                entrypoint: 'default',
                                id: 19920382,
                                destination: 'tz1QkeWxQgkHbmyjJuNKnTJBCNtUxtHtvbJU',
                            },
                        },
                        {
                            cursor: 'ooHN7P3X9pUeDyWWibvZqEaYEPic4xqASsZcxk4WMQ1n7hvojah:0',
                            node: {
                                hash: 'ooHN7P3X9pUeDyWWibvZqEaYEPic4xqASsZcxk4WMQ1n7hvojah',
                                batch_position: 0,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                kind: 'transaction',
                                timestamp: '2020-02-22T22:47:26Z',
                                level: 836082,
                                block: 'BM8ieLyATFHdtgvqhq6t9y9HacUMjhSpfHwSJNobRd2us13L5HR',
                                gas: 1284,
                                counter: 'cad0010000000000',
                                gas_limit: '4328000000000000',
                                storage_limit: '',
                                amount: '100000000',
                                parameters: '{ "prim": "Unit" }',
                                entrypoint: 'default',
                                id: 19731787,
                                destination: 'tz1RAWNEEFr2DSEDQVPnHXhH24GLMDF5XNjJ',
                            },
                        },
                        {
                            cursor: 'oodK29zqUFb7NyMa5mRyb6DVR71FyWHh5gXF8WVgCDG6uGVuCaT:0',
                            node: {
                                hash: 'oodK29zqUFb7NyMa5mRyb6DVR71FyWHh5gXF8WVgCDG6uGVuCaT',
                                batch_position: 0,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                kind: 'transaction',
                                timestamp: '2020-02-22T22:29:26Z',
                                level: 836064,
                                block: 'BKspGC8xE7CT8431tR5CErsn5dEVUB6eVtXLeCSpv4285z3rZa4',
                                gas: 1284,
                                counter: 'c9d0010000000000',
                                gas_limit: '4328000000000000',
                                storage_limit: '1501000000000000',
                                amount: '100000',
                                parameters: '{ "prim": "Unit" }',
                                entrypoint: 'default',
                                id: 19731302,
                                destination: 'tz1VPz5qhzTrRTNevHC33ArBTBPV6jHpwjQB',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.transactions - daterange format: 2020/01/01', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS") {
                address
                first_seen
                activated
                transactions(
                  last: 3
                  date_range: {
                      gte: "2020/01/01"
                      lte:"2020-02-27T18:07:36Z"
                  }
                ) {
                page_info {
                    start_cursor
                    end_cursor
                    has_next_page
                    has_previous_page
                }
                edges {
                        cursor
                        node {
                            hash
                            batch_position
                            source
                            kind
                            timestamp
                            level
                            block
                            gas
                            counter
                            gas_limit
                            storage_limit
                            amount
                            parameters
                            entrypoint
                            id
                            destination
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                first_seen: '2018-09-13T14:51:20Z',
                activated: '2018-09-13T14:49:20.000Z',
                transactions: {
                    page_info: {
                        start_cursor: 'ooZM6ErfewYXeUJGN4as8qiHPDT6u465tTuJKF6eQktXDkgtZKW:0',
                        end_cursor: 'oodK29zqUFb7NyMa5mRyb6DVR71FyWHh5gXF8WVgCDG6uGVuCaT:0',
                        has_next_page: false,
                        has_previous_page: true,
                    },
                    edges: [
                        {
                            cursor: 'ooZM6ErfewYXeUJGN4as8qiHPDT6u465tTuJKF6eQktXDkgtZKW:0',
                            node: {
                                hash: 'ooZM6ErfewYXeUJGN4as8qiHPDT6u465tTuJKF6eQktXDkgtZKW',
                                batch_position: 0,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                kind: 'transaction',
                                timestamp: '2020-02-27T17:55:56Z',
                                level: 842861,
                                block: 'BL7YnpqVo7tZEw3hhSVLYNf7RHLMNTRSopuu9CHmQuALvRbwVRP',
                                gas: 1283,
                                counter: 'cbd0010000000000',
                                gas_limit: '4328000000000000',
                                storage_limit: '1501000000000000',
                                amount: '10000',
                                parameters: '{ "prim": "Unit" }',
                                entrypoint: 'default',
                                id: 19920382,
                                destination: 'tz1QkeWxQgkHbmyjJuNKnTJBCNtUxtHtvbJU',
                            },
                        },
                        {
                            cursor: 'ooHN7P3X9pUeDyWWibvZqEaYEPic4xqASsZcxk4WMQ1n7hvojah:0',
                            node: {
                                hash: 'ooHN7P3X9pUeDyWWibvZqEaYEPic4xqASsZcxk4WMQ1n7hvojah',
                                batch_position: 0,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                kind: 'transaction',
                                timestamp: '2020-02-22T22:47:26Z',
                                level: 836082,
                                block: 'BM8ieLyATFHdtgvqhq6t9y9HacUMjhSpfHwSJNobRd2us13L5HR',
                                gas: 1284,
                                counter: 'cad0010000000000',
                                gas_limit: '4328000000000000',
                                storage_limit: '',
                                amount: '100000000',
                                parameters: '{ "prim": "Unit" }',
                                entrypoint: 'default',
                                id: 19731787,
                                destination: 'tz1RAWNEEFr2DSEDQVPnHXhH24GLMDF5XNjJ',
                            },
                        },
                        {
                            cursor: 'oodK29zqUFb7NyMa5mRyb6DVR71FyWHh5gXF8WVgCDG6uGVuCaT:0',
                            node: {
                                hash: 'oodK29zqUFb7NyMa5mRyb6DVR71FyWHh5gXF8WVgCDG6uGVuCaT',
                                batch_position: 0,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                kind: 'transaction',
                                timestamp: '2020-02-22T22:29:26Z',
                                level: 836064,
                                block: 'BKspGC8xE7CT8431tR5CErsn5dEVUB6eVtXLeCSpv4285z3rZa4',
                                gas: 1284,
                                counter: 'c9d0010000000000',
                                gas_limit: '4328000000000000',
                                storage_limit: '1501000000000000',
                                amount: '100000',
                                parameters: '{ "prim": "Unit" }',
                                entrypoint: 'default',
                                id: 19731302,
                                destination: 'tz1VPz5qhzTrRTNevHC33ArBTBPV6jHpwjQB',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.transactions - daterange format: 2020/01/01 00:', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS") {
                address
                first_seen
                activated
                transactions(
                  last: 3
                  date_range: {
                      gte: "2020/01/01 00:"
                      lte:"2020-02-27T18:07:36Z"
                  }
                ) {
                page_info {
                    start_cursor
                    end_cursor
                    has_next_page
                    has_previous_page
                }
                edges {
                        cursor
                        node {
                            hash
                            batch_position
                            source
                            kind
                            timestamp
                            level
                            block
                            gas
                            counter
                            gas_limit
                            storage_limit
                            amount
                            parameters
                            entrypoint
                            id
                            destination
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                first_seen: '2018-09-13T14:51:20Z',
                activated: '2018-09-13T14:49:20.000Z',
                transactions: {
                    page_info: {
                        start_cursor: 'ooZM6ErfewYXeUJGN4as8qiHPDT6u465tTuJKF6eQktXDkgtZKW:0',
                        end_cursor: 'oodK29zqUFb7NyMa5mRyb6DVR71FyWHh5gXF8WVgCDG6uGVuCaT:0',
                        has_next_page: false,
                        has_previous_page: true,
                    },
                    edges: [
                        {
                            cursor: 'ooZM6ErfewYXeUJGN4as8qiHPDT6u465tTuJKF6eQktXDkgtZKW:0',
                            node: {
                                hash: 'ooZM6ErfewYXeUJGN4as8qiHPDT6u465tTuJKF6eQktXDkgtZKW',
                                batch_position: 0,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                kind: 'transaction',
                                timestamp: '2020-02-27T17:55:56Z',
                                level: 842861,
                                block: 'BL7YnpqVo7tZEw3hhSVLYNf7RHLMNTRSopuu9CHmQuALvRbwVRP',
                                gas: 1283,
                                counter: 'cbd0010000000000',
                                gas_limit: '4328000000000000',
                                storage_limit: '1501000000000000',
                                amount: '10000',
                                parameters: '{ "prim": "Unit" }',
                                entrypoint: 'default',
                                id: 19920382,
                                destination: 'tz1QkeWxQgkHbmyjJuNKnTJBCNtUxtHtvbJU',
                            },
                        },
                        {
                            cursor: 'ooHN7P3X9pUeDyWWibvZqEaYEPic4xqASsZcxk4WMQ1n7hvojah:0',
                            node: {
                                hash: 'ooHN7P3X9pUeDyWWibvZqEaYEPic4xqASsZcxk4WMQ1n7hvojah',
                                batch_position: 0,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                kind: 'transaction',
                                timestamp: '2020-02-22T22:47:26Z',
                                level: 836082,
                                block: 'BM8ieLyATFHdtgvqhq6t9y9HacUMjhSpfHwSJNobRd2us13L5HR',
                                gas: 1284,
                                counter: 'cad0010000000000',
                                gas_limit: '4328000000000000',
                                storage_limit: '',
                                amount: '100000000',
                                parameters: '{ "prim": "Unit" }',
                                entrypoint: 'default',
                                id: 19731787,
                                destination: 'tz1RAWNEEFr2DSEDQVPnHXhH24GLMDF5XNjJ',
                            },
                        },
                        {
                            cursor: 'oodK29zqUFb7NyMa5mRyb6DVR71FyWHh5gXF8WVgCDG6uGVuCaT:0',
                            node: {
                                hash: 'oodK29zqUFb7NyMa5mRyb6DVR71FyWHh5gXF8WVgCDG6uGVuCaT',
                                batch_position: 0,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                kind: 'transaction',
                                timestamp: '2020-02-22T22:29:26Z',
                                level: 836064,
                                block: 'BKspGC8xE7CT8431tR5CErsn5dEVUB6eVtXLeCSpv4285z3rZa4',
                                gas: 1284,
                                counter: 'c9d0010000000000',
                                gas_limit: '4328000000000000',
                                storage_limit: '1501000000000000',
                                amount: '100000',
                                parameters: '{ "prim": "Unit" }',
                                entrypoint: 'default',
                                id: 19731302,
                                destination: 'tz1VPz5qhzTrRTNevHC33ArBTBPV6jHpwjQB',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.transactions - daterange format: 2020/01/01 00:00', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS") {
                address
                first_seen
                activated
                transactions(
                  last: 3
                  date_range: {
                      gte: "2020/01/01 00:00"
                      lte:"2020-02-27T18:07:36Z"
                  }
                ) {
                page_info {
                    start_cursor
                    end_cursor
                    has_next_page
                    has_previous_page
                }
                edges {
                        cursor
                        node {
                            hash
                            batch_position
                            source
                            kind
                            timestamp
                            level
                            block
                            gas
                            counter
                            gas_limit
                            storage_limit
                            amount
                            parameters
                            entrypoint
                            id
                            destination
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                first_seen: '2018-09-13T14:51:20Z',
                activated: '2018-09-13T14:49:20.000Z',
                transactions: {
                    page_info: {
                        start_cursor: 'ooZM6ErfewYXeUJGN4as8qiHPDT6u465tTuJKF6eQktXDkgtZKW:0',
                        end_cursor: 'oodK29zqUFb7NyMa5mRyb6DVR71FyWHh5gXF8WVgCDG6uGVuCaT:0',
                        has_next_page: false,
                        has_previous_page: true,
                    },
                    edges: [
                        {
                            cursor: 'ooZM6ErfewYXeUJGN4as8qiHPDT6u465tTuJKF6eQktXDkgtZKW:0',
                            node: {
                                hash: 'ooZM6ErfewYXeUJGN4as8qiHPDT6u465tTuJKF6eQktXDkgtZKW',
                                batch_position: 0,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                kind: 'transaction',
                                timestamp: '2020-02-27T17:55:56Z',
                                level: 842861,
                                block: 'BL7YnpqVo7tZEw3hhSVLYNf7RHLMNTRSopuu9CHmQuALvRbwVRP',
                                gas: 1283,
                                counter: 'cbd0010000000000',
                                gas_limit: '4328000000000000',
                                storage_limit: '1501000000000000',
                                amount: '10000',
                                parameters: '{ "prim": "Unit" }',
                                entrypoint: 'default',
                                id: 19920382,
                                destination: 'tz1QkeWxQgkHbmyjJuNKnTJBCNtUxtHtvbJU',
                            },
                        },
                        {
                            cursor: 'ooHN7P3X9pUeDyWWibvZqEaYEPic4xqASsZcxk4WMQ1n7hvojah:0',
                            node: {
                                hash: 'ooHN7P3X9pUeDyWWibvZqEaYEPic4xqASsZcxk4WMQ1n7hvojah',
                                batch_position: 0,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                kind: 'transaction',
                                timestamp: '2020-02-22T22:47:26Z',
                                level: 836082,
                                block: 'BM8ieLyATFHdtgvqhq6t9y9HacUMjhSpfHwSJNobRd2us13L5HR',
                                gas: 1284,
                                counter: 'cad0010000000000',
                                gas_limit: '4328000000000000',
                                storage_limit: '',
                                amount: '100000000',
                                parameters: '{ "prim": "Unit" }',
                                entrypoint: 'default',
                                id: 19731787,
                                destination: 'tz1RAWNEEFr2DSEDQVPnHXhH24GLMDF5XNjJ',
                            },
                        },
                        {
                            cursor: 'oodK29zqUFb7NyMa5mRyb6DVR71FyWHh5gXF8WVgCDG6uGVuCaT:0',
                            node: {
                                hash: 'oodK29zqUFb7NyMa5mRyb6DVR71FyWHh5gXF8WVgCDG6uGVuCaT',
                                batch_position: 0,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                kind: 'transaction',
                                timestamp: '2020-02-22T22:29:26Z',
                                level: 836064,
                                block: 'BKspGC8xE7CT8431tR5CErsn5dEVUB6eVtXLeCSpv4285z3rZa4',
                                gas: 1284,
                                counter: 'c9d0010000000000',
                                gas_limit: '4328000000000000',
                                storage_limit: '1501000000000000',
                                amount: '100000',
                                parameters: '{ "prim": "Unit" }',
                                entrypoint: 'default',
                                id: 19731302,
                                destination: 'tz1VPz5qhzTrRTNevHC33ArBTBPV6jHpwjQB',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.transactions - daterange format: 2020/01/01 00:00:00', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS") {
                address
                first_seen
                activated
                transactions(
                  last: 3
                  date_range: {
                      gte: "2020/01/01 00:00:00"
                      lte:"2020-02-27T18:07:36Z"
                  }
                ) {
                page_info {
                    start_cursor
                    end_cursor
                    has_next_page
                    has_previous_page
                }
                edges {
                        cursor
                        node {
                            hash
                            batch_position
                            source
                            kind
                            timestamp
                            level
                            block
                            gas
                            counter
                            gas_limit
                            storage_limit
                            amount
                            parameters
                            entrypoint
                            id
                            destination
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                first_seen: '2018-09-13T14:51:20Z',
                activated: '2018-09-13T14:49:20.000Z',
                transactions: {
                    page_info: {
                        start_cursor: 'ooZM6ErfewYXeUJGN4as8qiHPDT6u465tTuJKF6eQktXDkgtZKW:0',
                        end_cursor: 'oodK29zqUFb7NyMa5mRyb6DVR71FyWHh5gXF8WVgCDG6uGVuCaT:0',
                        has_next_page: false,
                        has_previous_page: true,
                    },
                    edges: [
                        {
                            cursor: 'ooZM6ErfewYXeUJGN4as8qiHPDT6u465tTuJKF6eQktXDkgtZKW:0',
                            node: {
                                hash: 'ooZM6ErfewYXeUJGN4as8qiHPDT6u465tTuJKF6eQktXDkgtZKW',
                                batch_position: 0,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                kind: 'transaction',
                                timestamp: '2020-02-27T17:55:56Z',
                                level: 842861,
                                block: 'BL7YnpqVo7tZEw3hhSVLYNf7RHLMNTRSopuu9CHmQuALvRbwVRP',
                                gas: 1283,
                                counter: 'cbd0010000000000',
                                gas_limit: '4328000000000000',
                                storage_limit: '1501000000000000',
                                amount: '10000',
                                parameters: '{ "prim": "Unit" }',
                                entrypoint: 'default',
                                id: 19920382,
                                destination: 'tz1QkeWxQgkHbmyjJuNKnTJBCNtUxtHtvbJU',
                            },
                        },
                        {
                            cursor: 'ooHN7P3X9pUeDyWWibvZqEaYEPic4xqASsZcxk4WMQ1n7hvojah:0',
                            node: {
                                hash: 'ooHN7P3X9pUeDyWWibvZqEaYEPic4xqASsZcxk4WMQ1n7hvojah',
                                batch_position: 0,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                kind: 'transaction',
                                timestamp: '2020-02-22T22:47:26Z',
                                level: 836082,
                                block: 'BM8ieLyATFHdtgvqhq6t9y9HacUMjhSpfHwSJNobRd2us13L5HR',
                                gas: 1284,
                                counter: 'cad0010000000000',
                                gas_limit: '4328000000000000',
                                storage_limit: '',
                                amount: '100000000',
                                parameters: '{ "prim": "Unit" }',
                                entrypoint: 'default',
                                id: 19731787,
                                destination: 'tz1RAWNEEFr2DSEDQVPnHXhH24GLMDF5XNjJ',
                            },
                        },
                        {
                            cursor: 'oodK29zqUFb7NyMa5mRyb6DVR71FyWHh5gXF8WVgCDG6uGVuCaT:0',
                            node: {
                                hash: 'oodK29zqUFb7NyMa5mRyb6DVR71FyWHh5gXF8WVgCDG6uGVuCaT',
                                batch_position: 0,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                kind: 'transaction',
                                timestamp: '2020-02-22T22:29:26Z',
                                level: 836064,
                                block: 'BKspGC8xE7CT8431tR5CErsn5dEVUB6eVtXLeCSpv4285z3rZa4',
                                gas: 1284,
                                counter: 'c9d0010000000000',
                                gas_limit: '4328000000000000',
                                storage_limit: '1501000000000000',
                                amount: '100000',
                                parameters: '{ "prim": "Unit" }',
                                entrypoint: 'default',
                                id: 19731302,
                                destination: 'tz1VPz5qhzTrRTNevHC33ArBTBPV6jHpwjQB',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.transactions - daterange format: 2020/01/01 00:00:00.000', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS") {
                address
                first_seen
                activated
                transactions(
                  last: 3
                  date_range: {
                      gte: "2020/01/01 00:00:00.000"
                      lte:"2020-02-27T18:07:36Z"
                  }
                ) {
                page_info {
                    start_cursor
                    end_cursor
                    has_next_page
                    has_previous_page
                }
                edges {
                        cursor
                        node {
                            hash
                            batch_position
                            source
                            kind
                            timestamp
                            level
                            block
                            gas
                            counter
                            gas_limit
                            storage_limit
                            amount
                            parameters
                            entrypoint
                            id
                            destination
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                first_seen: '2018-09-13T14:51:20Z',
                activated: '2018-09-13T14:49:20.000Z',
                transactions: {
                    page_info: {
                        start_cursor: 'ooZM6ErfewYXeUJGN4as8qiHPDT6u465tTuJKF6eQktXDkgtZKW:0',
                        end_cursor: 'oodK29zqUFb7NyMa5mRyb6DVR71FyWHh5gXF8WVgCDG6uGVuCaT:0',
                        has_next_page: false,
                        has_previous_page: true,
                    },
                    edges: [
                        {
                            cursor: 'ooZM6ErfewYXeUJGN4as8qiHPDT6u465tTuJKF6eQktXDkgtZKW:0',
                            node: {
                                hash: 'ooZM6ErfewYXeUJGN4as8qiHPDT6u465tTuJKF6eQktXDkgtZKW',
                                batch_position: 0,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                kind: 'transaction',
                                timestamp: '2020-02-27T17:55:56Z',
                                level: 842861,
                                block: 'BL7YnpqVo7tZEw3hhSVLYNf7RHLMNTRSopuu9CHmQuALvRbwVRP',
                                gas: 1283,
                                counter: 'cbd0010000000000',
                                gas_limit: '4328000000000000',
                                storage_limit: '1501000000000000',
                                amount: '10000',
                                parameters: '{ "prim": "Unit" }',
                                entrypoint: 'default',
                                id: 19920382,
                                destination: 'tz1QkeWxQgkHbmyjJuNKnTJBCNtUxtHtvbJU',
                            },
                        },
                        {
                            cursor: 'ooHN7P3X9pUeDyWWibvZqEaYEPic4xqASsZcxk4WMQ1n7hvojah:0',
                            node: {
                                hash: 'ooHN7P3X9pUeDyWWibvZqEaYEPic4xqASsZcxk4WMQ1n7hvojah',
                                batch_position: 0,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                kind: 'transaction',
                                timestamp: '2020-02-22T22:47:26Z',
                                level: 836082,
                                block: 'BM8ieLyATFHdtgvqhq6t9y9HacUMjhSpfHwSJNobRd2us13L5HR',
                                gas: 1284,
                                counter: 'cad0010000000000',
                                gas_limit: '4328000000000000',
                                storage_limit: '',
                                amount: '100000000',
                                parameters: '{ "prim": "Unit" }',
                                entrypoint: 'default',
                                id: 19731787,
                                destination: 'tz1RAWNEEFr2DSEDQVPnHXhH24GLMDF5XNjJ',
                            },
                        },
                        {
                            cursor: 'oodK29zqUFb7NyMa5mRyb6DVR71FyWHh5gXF8WVgCDG6uGVuCaT:0',
                            node: {
                                hash: 'oodK29zqUFb7NyMa5mRyb6DVR71FyWHh5gXF8WVgCDG6uGVuCaT',
                                batch_position: 0,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                kind: 'transaction',
                                timestamp: '2020-02-22T22:29:26Z',
                                level: 836064,
                                block: 'BKspGC8xE7CT8431tR5CErsn5dEVUB6eVtXLeCSpv4285z3rZa4',
                                gas: 1284,
                                counter: 'c9d0010000000000',
                                gas_limit: '4328000000000000',
                                storage_limit: '1501000000000000',
                                amount: '100000',
                                parameters: '{ "prim": "Unit" }',
                                entrypoint: 'default',
                                id: 19731302,
                                destination: 'tz1VPz5qhzTrRTNevHC33ArBTBPV6jHpwjQB',
                            },
                        },
                    ],
                },
            },
        }));
    });
});
