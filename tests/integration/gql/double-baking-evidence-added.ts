import { gql } from 'apollo-server';

export const doubleBakingEvidenceAdded = gql`
subscription ($delegate: AddressArrayFilter) {
  doubleBakingEvidenceAdded(delegate: $delegate) {
    kind,
    bh1 {
      level,
      proto,
      predecessor,
      timestamp,
      validation_pass,
      operations_hash,
      fitness,
      context,
      priority,
      proof_of_work_nonce,
      seed_nonce_hash,
      signature
    },
    bh2 {
      level,
      proto,
      predecessor,
      timestamp,
      validation_pass,
      operations_hash,
      fitness,
      context,
      priority,
      proof_of_work_nonce,
      seed_nonce_hash,
      signature
    },
    metadata {
      balance_updates {
        kind,
        category,
        contract,
        delegate,
        cycle,
        change
      }
    }
  }
}
`;
