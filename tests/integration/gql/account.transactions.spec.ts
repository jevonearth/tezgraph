import 'reflect-metadata';
import { ApolloServer } from 'apollo-server-express';
import { gql } from 'apollo-server';
import { createTestClient } from 'apollo-server-testing';
import { AccountResolver } from '../../../src/resolvers/account';
import { buildSchema } from 'type-graphql';

async function startTestServer() {
    const testServer = new ApolloServer({
        schema: await buildSchema({
            resolvers: [AccountResolver],
        }),
        context: ({ req, res }) => ({ req, res }),
    });

    return testServer;
}

const server = startTestServer();

let testClientQuery: Function;

async function startTestClient() {
    if (!testClientQuery) {
        const { query } = createTestClient(await server);
        testClientQuery = await query;
    }
}

describe('Account Resolvers', () => {
    test('account.transactions first', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS") {
                address
                first_seen
                activated
                transactions(
                  first:3
                ) {
                page_info {
                    start_cursor
                    end_cursor
                    has_next_page
                    has_previous_page
                }
                edges {
                        cursor
                        node {
                            hash
                            batch_position
                            source
                            kind
                            timestamp
                            level
                            block
                            gas
                            counter
                            gas_limit
                            storage_limit
                            amount
                            parameters
                            entrypoint
                            id
                            destination
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                first_seen: '2018-09-13T14:51:20Z',
                activated: '2018-09-13T14:49:20.000Z',
                transactions: {
                    page_info: {
                        start_cursor: 'opNxsLZeNMttsyh4PxLk35dmRsJyLXQwJjpVp89ip7pU2j5yzyR:0',
                        end_cursor: 'ootgGUV6dDRfBBF9wutPQZWzeetqiAUkwrkwSL1Qy6NrTNUs6Jw:0',
                        has_next_page: true,
                        has_previous_page: false,
                    },
                    edges: [
                        {
                            cursor: 'opNxsLZeNMttsyh4PxLk35dmRsJyLXQwJjpVp89ip7pU2j5yzyR:0',
                            node: {
                                hash: 'opNxsLZeNMttsyh4PxLk35dmRsJyLXQwJjpVp89ip7pU2j5yzyR',
                                batch_position: 0,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                kind: 'transaction',
                                timestamp: '2020-08-15T22:11:48Z',
                                level: 1085872,
                                block: 'BLEwgk9RsqEfhtm6u3PrLXcu7pXQ6dWMysJRVTAhNRqVqhCsF9g',
                                gas: 1285,
                                counter: 'e2d0010000000000',
                                gas_limit: '4328000000000000',
                                storage_limit: '1501000000000000',
                                amount: '50000000',
                                parameters: '{ "prim": "Unit" }',
                                entrypoint: 'default',
                                id: 26331765,
                                destination: 'tz1UovZ16T6vTuCRQhWKtE7jqeC6QaTzbc3P',
                            },
                        },
                        {
                            cursor: 'opEHTGa4n4efFQfpaWFNqoY4dbzXrBEo7Xo8v74AX5pEREqK28S:0',
                            node: {
                                hash: 'opEHTGa4n4efFQfpaWFNqoY4dbzXrBEo7Xo8v74AX5pEREqK28S',
                                batch_position: 0,
                                source: 'tz1YPCK2iRxWZYHxeUtKBKRosdQ1SAHKeACe',
                                kind: 'transaction',
                                timestamp: '2020-07-17T19:46:24Z',
                                level: 1044207,
                                block: 'BMYhcRrNnHTthRueAKoiyBxyuzJWEMqMgkxzqiVFXHtS84ALKua',
                                gas: 1420,
                                counter: '3ee8200000000000',
                                gas_limit: '6829000000000000',
                                storage_limit: '2c01000000000000',
                                amount: '192000000',
                                parameters: '{ "prim": "Unit" }',
                                entrypoint: 'default',
                                id: 25210462,
                                destination: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                            },
                        },
                        {
                            cursor: 'ootgGUV6dDRfBBF9wutPQZWzeetqiAUkwrkwSL1Qy6NrTNUs6Jw:0',
                            node: {
                                hash: 'ootgGUV6dDRfBBF9wutPQZWzeetqiAUkwrkwSL1Qy6NrTNUs6Jw',
                                batch_position: 0,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                kind: 'transaction',
                                timestamp: '2020-07-15T20:58:27Z',
                                level: 1041413,
                                block: 'BMf1K4SE3WqRJ6eD345kU74pjLkfB7ABK5u5KwZrxckNHFvUr4v',
                                gas: 1285,
                                counter: 'e1d0010000000000',
                                gas_limit: '4328000000000000',
                                storage_limit: '1501000000000000',
                                amount: '14193181',
                                parameters: '{ "prim": "Unit" }',
                                entrypoint: 'default',
                                id: 25134264,
                                destination: 'tz1cb9DMkq9ZdrDVxi3HwyLEMtbDseeZPZEa',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.transactions first and before', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS") {
                address
                first_seen
                activated
                transactions(
                  first:3
                  before:"oo8sVjaRzoFHaoxgtVdAbqbPujVJzt1ToknGwapTjHJM5jRhyMx:0"
                ) {
                page_info {
                    start_cursor
                    end_cursor
                    has_next_page
                    has_previous_page
                }
                edges {
                        cursor
                        node {
                            hash
                            batch_position
                            source
                            kind
                            timestamp
                            level
                            block
                            gas
                            counter
                            gas_limit
                            storage_limit
                            amount
                            parameters
                            entrypoint
                            id
                            destination
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                first_seen: '2018-09-13T14:51:20Z',
                activated: '2018-09-13T14:49:20.000Z',
                transactions: {
                    page_info: {
                        start_cursor: 'opNxsLZeNMttsyh4PxLk35dmRsJyLXQwJjpVp89ip7pU2j5yzyR:0',
                        end_cursor: 'ootgGUV6dDRfBBF9wutPQZWzeetqiAUkwrkwSL1Qy6NrTNUs6Jw:0',
                        has_next_page: true,
                        has_previous_page: false,
                    },
                    edges: [
                        {
                            cursor: 'opNxsLZeNMttsyh4PxLk35dmRsJyLXQwJjpVp89ip7pU2j5yzyR:0',
                            node: {
                                hash: 'opNxsLZeNMttsyh4PxLk35dmRsJyLXQwJjpVp89ip7pU2j5yzyR',
                                batch_position: 0,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                kind: 'transaction',
                                timestamp: '2020-08-15T22:11:48Z',
                                level: 1085872,
                                block: 'BLEwgk9RsqEfhtm6u3PrLXcu7pXQ6dWMysJRVTAhNRqVqhCsF9g',
                                gas: 1285,
                                counter: 'e2d0010000000000',
                                gas_limit: '4328000000000000',
                                storage_limit: '1501000000000000',
                                amount: '50000000',
                                parameters: '{ "prim": "Unit" }',
                                entrypoint: 'default',
                                id: 26331765,
                                destination: 'tz1UovZ16T6vTuCRQhWKtE7jqeC6QaTzbc3P',
                            },
                        },
                        {
                            cursor: 'opEHTGa4n4efFQfpaWFNqoY4dbzXrBEo7Xo8v74AX5pEREqK28S:0',
                            node: {
                                hash: 'opEHTGa4n4efFQfpaWFNqoY4dbzXrBEo7Xo8v74AX5pEREqK28S',
                                batch_position: 0,
                                source: 'tz1YPCK2iRxWZYHxeUtKBKRosdQ1SAHKeACe',
                                kind: 'transaction',
                                timestamp: '2020-07-17T19:46:24Z',
                                level: 1044207,
                                block: 'BMYhcRrNnHTthRueAKoiyBxyuzJWEMqMgkxzqiVFXHtS84ALKua',
                                gas: 1420,
                                counter: '3ee8200000000000',
                                gas_limit: '6829000000000000',
                                storage_limit: '2c01000000000000',
                                amount: '192000000',
                                parameters: '{ "prim": "Unit" }',
                                entrypoint: 'default',
                                id: 25210462,
                                destination: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                            },
                        },
                        {
                            cursor: 'ootgGUV6dDRfBBF9wutPQZWzeetqiAUkwrkwSL1Qy6NrTNUs6Jw:0',
                            node: {
                                hash: 'ootgGUV6dDRfBBF9wutPQZWzeetqiAUkwrkwSL1Qy6NrTNUs6Jw',
                                batch_position: 0,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                kind: 'transaction',
                                timestamp: '2020-07-15T20:58:27Z',
                                level: 1041413,
                                block: 'BMf1K4SE3WqRJ6eD345kU74pjLkfB7ABK5u5KwZrxckNHFvUr4v',
                                gas: 1285,
                                counter: 'e1d0010000000000',
                                gas_limit: '4328000000000000',
                                storage_limit: '1501000000000000',
                                amount: '14193181',
                                parameters: '{ "prim": "Unit" }',
                                entrypoint: 'default',
                                id: 25134264,
                                destination: 'tz1cb9DMkq9ZdrDVxi3HwyLEMtbDseeZPZEa',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.transactions first and after', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS") {
                address
                first_seen
                activated
                transactions(
                  first:3
                  after:"oo8sVjaRzoFHaoxgtVdAbqbPujVJzt1ToknGwapTjHJM5jRhyMx:0"
                ) {
                page_info {
                    start_cursor
                    end_cursor
                    has_next_page
                    has_previous_page
                }
                edges {
                        cursor
                        node {
                            hash
                            batch_position
                            source
                            kind
                            timestamp
                            level
                            block
                            gas
                            counter
                            gas_limit
                            storage_limit
                            amount
                            parameters
                            entrypoint
                            id
                            destination
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                first_seen: '2018-09-13T14:51:20Z',
                activated: '2018-09-13T14:49:20.000Z',
                transactions: {
                    page_info: {
                        start_cursor: 'opB8Yp3kyXctnsv1PnSxeHcempXtuvJ8y4ttKhReust8iBXj9t8:0',
                        end_cursor: 'onecWS9W7u5NtbgjHr5KCSvjpvUUUfZDBnKN3ZmpU5n2zm6XTm9:0',
                        has_next_page: true,
                        has_previous_page: true,
                    },
                    edges: [
                        {
                            cursor: 'opB8Yp3kyXctnsv1PnSxeHcempXtuvJ8y4ttKhReust8iBXj9t8:0',
                            node: {
                                hash: 'opB8Yp3kyXctnsv1PnSxeHcempXtuvJ8y4ttKhReust8iBXj9t8',
                                batch_position: 0,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                kind: 'transaction',
                                timestamp: '2020-07-15T20:50:27Z',
                                level: 1041405,
                                block: 'BKvd9oBBiWLok31cP9ftX6Nftcu9DHq1GVPhXb5KAnuzB2zS5GJ',
                                gas: 1285,
                                counter: 'dbd0010000000000',
                                gas_limit: '4328000000000000',
                                storage_limit: '1501000000000000',
                                amount: '50217319',
                                parameters: '{ "prim": "Unit" }',
                                entrypoint: 'default',
                                id: 25134017,
                                destination: 'tz1X3xMBb3XzbpK4cehfQRoDgTVFJ2mBQBwt',
                            },
                        },
                        {
                            cursor: 'onmJTCgaEfto2uNzjc5NCZziddopqeEKh2a6dCvJDNMLf6bmqqt:0',
                            node: {
                                hash: 'onmJTCgaEfto2uNzjc5NCZziddopqeEKh2a6dCvJDNMLf6bmqqt',
                                batch_position: 0,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                kind: 'transaction',
                                timestamp: '2020-07-15T20:48:27Z',
                                level: 1041403,
                                block: 'BLdGSJRJRx1SsfnWonxp4vVMCwnMR8reSDx8CVFY6MPJ5rFjW8h',
                                gas: 1284,
                                counter: 'dad0010000000000',
                                gas_limit: '4328000000000000',
                                storage_limit: '',
                                amount: '11602357',
                                parameters: '{ "prim": "Unit" }',
                                entrypoint: 'default',
                                id: 25133955,
                                destination: 'tz1ZcKt8A46vuam4hQSHMtBWgFrDhHnYjS9q',
                            },
                        },
                        {
                            cursor: 'onecWS9W7u5NtbgjHr5KCSvjpvUUUfZDBnKN3ZmpU5n2zm6XTm9:0',
                            node: {
                                hash: 'onecWS9W7u5NtbgjHr5KCSvjpvUUUfZDBnKN3ZmpU5n2zm6XTm9',
                                batch_position: 0,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                kind: 'transaction',
                                timestamp: '2020-07-15T20:47:27Z',
                                level: 1041402,
                                block: 'BMF8HCV1GXYgeH9NhLcxrFdUsE65tpJdVBoXaEynAX3jrD24yEH',
                                gas: 1283,
                                counter: 'd9d0010000000000',
                                gas_limit: '4328000000000000',
                                storage_limit: '',
                                amount: '560732',
                                parameters: '{ "prim": "Unit" }',
                                entrypoint: 'default',
                                id: 25133920,
                                destination: 'tz1XBrjDhscw9Qg6o43VZ3V6eErDaQjAP6g2',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.transactions first and order_by', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS") {
                address
                first_seen
                activated
                transactions(
                  first: 3
                  order_by: {field: ID, direction: ASC}
                ) {
                page_info {
                    start_cursor
                    end_cursor
                    has_next_page
                    has_previous_page
                }
                edges {
                        cursor
                        node {
                            hash
                            batch_position
                            source
                            kind
                            timestamp
                            level
                            block
                            gas
                            counter
                            gas_limit
                            storage_limit
                            amount
                            parameters
                            entrypoint
                            id
                            destination
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                first_seen: '2018-09-13T14:51:20Z',
                activated: '2018-09-13T14:49:20.000Z',
                transactions: {
                    page_info: {
                        start_cursor: 'opQqKKXhzznFRxMPdiLyP4V1KWfuDrrNSkqF8jQ6v4JENR3K6jP:0',
                        end_cursor: 'ooe9ypjsaQZPhowQpj69MZicQ3ZUwFGxQJyMVxdGpMRWF9SBZWr:0',
                        has_next_page: true,
                        has_previous_page: false,
                    },
                    edges: [
                        {
                            cursor: 'opQqKKXhzznFRxMPdiLyP4V1KWfuDrrNSkqF8jQ6v4JENR3K6jP:0',
                            node: {
                                hash: 'opQqKKXhzznFRxMPdiLyP4V1KWfuDrrNSkqF8jQ6v4JENR3K6jP',
                                batch_position: 0,
                                source: 'tz1LBEKXaxQbd5Gtzbc1ATCwc3pppu81aWGc',
                                kind: 'transaction',
                                timestamp: '2018-09-13T14:51:20Z',
                                level: 101957,
                                block: 'BMQzE1UWod12P5XkE15fS4CNRvKLqmWknwCEDuTP1FKEhzrVjtU',
                                gas: 10,
                                counter: '62e7000000000000',
                                gas_limit: 'c800000000000000',
                                storage_limit: '',
                                amount: '14',
                                parameters: null,
                                entrypoint: null,
                                id: 1425714,
                                destination: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                            },
                        },
                        {
                            cursor: 'oop8aPtDYg7K3xqqMadPKoXzxTLNpBukasrKtBQs9bsjXQ1UWVt:1',
                            node: {
                                hash: 'oop8aPtDYg7K3xqqMadPKoXzxTLNpBukasrKtBQs9bsjXQ1UWVt',
                                batch_position: 1,
                                source: 'tz1PECYmqmuba13rnNxyoBkvH2ChUt8BuJuG',
                                kind: 'transaction',
                                timestamp: '2018-09-13T16:12:50Z',
                                level: 102031,
                                block: 'BLowVvoAyjeoMY4BRja6rEPdX37UjfdeB4JFtgQ4ZvCiRg9MNdA',
                                gas: 0,
                                counter: '9d4d000000000000',
                                gas_limit: '',
                                storage_limit: '',
                                amount: '1000000',
                                parameters: null,
                                entrypoint: null,
                                id: 1427150,
                                destination: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                            },
                        },
                        {
                            cursor: 'ooe9ypjsaQZPhowQpj69MZicQ3ZUwFGxQJyMVxdGpMRWF9SBZWr:0',
                            node: {
                                hash: 'ooe9ypjsaQZPhowQpj69MZicQ3ZUwFGxQJyMVxdGpMRWF9SBZWr',
                                batch_position: 0,
                                source: 'tz1PECYmqmuba13rnNxyoBkvH2ChUt8BuJuG',
                                kind: 'transaction',
                                timestamp: '2018-09-13T16:17:05Z',
                                level: 102034,
                                block: 'BLFRNMJkWm9aRBgTwEdhZGcgKZ3idCWYWua8bRCfhx1BBU5xMmJ',
                                gas: 0,
                                counter: '9f4d000000000000',
                                gas_limit: 'c800000000000000',
                                storage_limit: '',
                                amount: '4199800000',
                                parameters: null,
                                entrypoint: null,
                                id: 1427201,
                                destination: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.transactions first and order_by and before', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS") {
                address
                first_seen
                activated
                transactions(
                  first: 3
                  order_by: {field: ID, direction: ASC}
                  before: "ooe9ypjsaQZPhowQpj69MZicQ3ZUwFGxQJyMVxdGpMRWF9SBZWr:0"
                ) {
                page_info {
                    start_cursor
                    end_cursor
                    has_next_page
                    has_previous_page
                }
                edges {
                        cursor
                        node {
                            hash
                            batch_position
                            source
                            kind
                            timestamp
                            level
                            block
                            gas
                            counter
                            gas_limit
                            storage_limit
                            amount
                            parameters
                            entrypoint
                            id
                            destination
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                first_seen: '2018-09-13T14:51:20Z',
                activated: '2018-09-13T14:49:20.000Z',
                transactions: {
                    page_info: {
                        start_cursor: 'opQqKKXhzznFRxMPdiLyP4V1KWfuDrrNSkqF8jQ6v4JENR3K6jP:0',
                        end_cursor: 'oop8aPtDYg7K3xqqMadPKoXzxTLNpBukasrKtBQs9bsjXQ1UWVt:1',
                        has_next_page: true,
                        has_previous_page: false,
                    },
                    edges: [
                        {
                            cursor: 'opQqKKXhzznFRxMPdiLyP4V1KWfuDrrNSkqF8jQ6v4JENR3K6jP:0',
                            node: {
                                hash: 'opQqKKXhzznFRxMPdiLyP4V1KWfuDrrNSkqF8jQ6v4JENR3K6jP',
                                batch_position: 0,
                                source: 'tz1LBEKXaxQbd5Gtzbc1ATCwc3pppu81aWGc',
                                kind: 'transaction',
                                timestamp: '2018-09-13T14:51:20Z',
                                level: 101957,
                                block: 'BMQzE1UWod12P5XkE15fS4CNRvKLqmWknwCEDuTP1FKEhzrVjtU',
                                gas: 10,
                                counter: '62e7000000000000',
                                gas_limit: 'c800000000000000',
                                storage_limit: '',
                                amount: '14',
                                parameters: null,
                                entrypoint: null,
                                id: 1425714,
                                destination: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                            },
                        },
                        {
                            cursor: 'oop8aPtDYg7K3xqqMadPKoXzxTLNpBukasrKtBQs9bsjXQ1UWVt:1',
                            node: {
                                hash: 'oop8aPtDYg7K3xqqMadPKoXzxTLNpBukasrKtBQs9bsjXQ1UWVt',
                                batch_position: 1,
                                source: 'tz1PECYmqmuba13rnNxyoBkvH2ChUt8BuJuG',
                                kind: 'transaction',
                                timestamp: '2018-09-13T16:12:50Z',
                                level: 102031,
                                block: 'BLowVvoAyjeoMY4BRja6rEPdX37UjfdeB4JFtgQ4ZvCiRg9MNdA',
                                gas: 0,
                                counter: '9d4d000000000000',
                                gas_limit: '',
                                storage_limit: '',
                                amount: '1000000',
                                parameters: null,
                                entrypoint: null,
                                id: 1427150,
                                destination: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.transactions first and order_by and after', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS") {
                address
                first_seen
                activated
                transactions(
                  first: 3
                  order_by: {field: ID, direction: ASC}
                  after: "ooe9ypjsaQZPhowQpj69MZicQ3ZUwFGxQJyMVxdGpMRWF9SBZWr:0"
                ) {
                page_info {
                    start_cursor
                    end_cursor
                    has_next_page
                    has_previous_page
                }
                edges {
                        cursor
                        node {
                            hash
                            batch_position
                            source
                            kind
                            timestamp
                            level
                            block
                            gas
                            counter
                            gas_limit
                            storage_limit
                            amount
                            parameters
                            entrypoint
                            id
                            destination
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                first_seen: '2018-09-13T14:51:20Z',
                activated: '2018-09-13T14:49:20.000Z',
                transactions: {
                    page_info: {
                        start_cursor: 'ooeHBn4BDojRRLsNc4eWta1JrHoxGEEU8p2EjufpUcRNMcKJyNb:0',
                        end_cursor: 'ooXu4igMeKVc2CT7CYUa62WxJRcpXJosLM2FF1vBXmBT61FR19K:0',
                        has_next_page: true,
                        has_previous_page: true,
                    },
                    edges: [
                        {
                            cursor: 'ooeHBn4BDojRRLsNc4eWta1JrHoxGEEU8p2EjufpUcRNMcKJyNb:0',
                            node: {
                                hash: 'ooeHBn4BDojRRLsNc4eWta1JrHoxGEEU8p2EjufpUcRNMcKJyNb',
                                batch_position: 0,
                                source: 'tz1PECYmqmuba13rnNxyoBkvH2ChUt8BuJuG',
                                kind: 'transaction',
                                timestamp: '2018-09-13T16:18:05Z',
                                level: 102035,
                                block: 'BLJLwRhrzz4srdUXw3FHgPazUnnMc3B1R15wpSWsBJPAYRJQCng',
                                gas: 0,
                                counter: 'a04d000000000000',
                                gas_limit: 'c800000000000000',
                                storage_limit: '',
                                amount: '20000',
                                parameters: null,
                                entrypoint: null,
                                id: 1427217,
                                destination: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                            },
                        },
                        {
                            cursor: 'op4hQbsXN35JghDjC9pM3wUFQW8roEmwcncBY5KoW2hF3CQbPGH:0',
                            node: {
                                hash: 'op4hQbsXN35JghDjC9pM3wUFQW8roEmwcncBY5KoW2hF3CQbPGH',
                                batch_position: 0,
                                source: 'tz1PECYmqmuba13rnNxyoBkvH2ChUt8BuJuG',
                                kind: 'transaction',
                                timestamp: '2018-09-13T16:23:20Z',
                                level: 102039,
                                block: 'BM1CtHUump3aahzSMVHh6HVBEvmWramVCpuTm7tFFnPGVhS6xr5',
                                gas: 0,
                                counter: 'a14d000000000000',
                                gas_limit: 'c800000000000000',
                                storage_limit: '',
                                amount: '3000',
                                parameters: null,
                                entrypoint: null,
                                id: 1427300,
                                destination: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                            },
                        },
                        {
                            cursor: 'ooXu4igMeKVc2CT7CYUa62WxJRcpXJosLM2FF1vBXmBT61FR19K:0',
                            node: {
                                hash: 'ooXu4igMeKVc2CT7CYUa62WxJRcpXJosLM2FF1vBXmBT61FR19K',
                                batch_position: 0,
                                source: 'tz1KtGwriE7VuLwT3LwuvU9Nv4wAxP7XZ57d',
                                kind: 'transaction',
                                timestamp: '2018-10-05T00:32:44Z',
                                level: 131767,
                                block: 'BMWGakwzmTPyXT5ncfT9HfF154AJhXi8z1o1UuGeFaMJV1o93nK',
                                gas: 0,
                                counter: '7de5010000000000',
                                gas_limit: 'c800000000000000',
                                storage_limit: '',
                                amount: '5543250000',
                                parameters: null,
                                entrypoint: null,
                                id: 2050623,
                                destination: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.transactions first and date_range gte', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS") {
                address
                first_seen
                activated
                transactions(
                  first: 3
                  date_range: {
                    gte: "2019-10-22T18:56:24Z"
                  }
                ) {
                page_info {
                    start_cursor
                    end_cursor
                    has_next_page
                    has_previous_page
                }
                edges {
                        cursor
                        node {
                            hash
                            batch_position
                            source
                            kind
                            timestamp
                            level
                            block
                            gas
                            counter
                            gas_limit
                            storage_limit
                            amount
                            parameters
                            entrypoint
                            id
                            destination
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                first_seen: '2018-09-13T14:51:20Z',
                activated: '2018-09-13T14:49:20.000Z',
                transactions: {
                    page_info: {
                        start_cursor: 'opNxsLZeNMttsyh4PxLk35dmRsJyLXQwJjpVp89ip7pU2j5yzyR:0',
                        end_cursor: 'ootgGUV6dDRfBBF9wutPQZWzeetqiAUkwrkwSL1Qy6NrTNUs6Jw:0',
                        has_next_page: true,
                        has_previous_page: false,
                    },
                    edges: [
                        {
                            cursor: 'opNxsLZeNMttsyh4PxLk35dmRsJyLXQwJjpVp89ip7pU2j5yzyR:0',
                            node: {
                                hash: 'opNxsLZeNMttsyh4PxLk35dmRsJyLXQwJjpVp89ip7pU2j5yzyR',
                                batch_position: 0,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                kind: 'transaction',
                                timestamp: '2020-08-15T22:11:48Z',
                                level: 1085872,
                                block: 'BLEwgk9RsqEfhtm6u3PrLXcu7pXQ6dWMysJRVTAhNRqVqhCsF9g',
                                gas: 1285,
                                counter: 'e2d0010000000000',
                                gas_limit: '4328000000000000',
                                storage_limit: '1501000000000000',
                                amount: '50000000',
                                parameters: '{ "prim": "Unit" }',
                                entrypoint: 'default',
                                id: 26331765,
                                destination: 'tz1UovZ16T6vTuCRQhWKtE7jqeC6QaTzbc3P',
                            },
                        },
                        {
                            cursor: 'opEHTGa4n4efFQfpaWFNqoY4dbzXrBEo7Xo8v74AX5pEREqK28S:0',
                            node: {
                                hash: 'opEHTGa4n4efFQfpaWFNqoY4dbzXrBEo7Xo8v74AX5pEREqK28S',
                                batch_position: 0,
                                source: 'tz1YPCK2iRxWZYHxeUtKBKRosdQ1SAHKeACe',
                                kind: 'transaction',
                                timestamp: '2020-07-17T19:46:24Z',
                                level: 1044207,
                                block: 'BMYhcRrNnHTthRueAKoiyBxyuzJWEMqMgkxzqiVFXHtS84ALKua',
                                gas: 1420,
                                counter: '3ee8200000000000',
                                gas_limit: '6829000000000000',
                                storage_limit: '2c01000000000000',
                                amount: '192000000',
                                parameters: '{ "prim": "Unit" }',
                                entrypoint: 'default',
                                id: 25210462,
                                destination: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                            },
                        },
                        {
                            cursor: 'ootgGUV6dDRfBBF9wutPQZWzeetqiAUkwrkwSL1Qy6NrTNUs6Jw:0',
                            node: {
                                hash: 'ootgGUV6dDRfBBF9wutPQZWzeetqiAUkwrkwSL1Qy6NrTNUs6Jw',
                                batch_position: 0,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                kind: 'transaction',
                                timestamp: '2020-07-15T20:58:27Z',
                                level: 1041413,
                                block: 'BMf1K4SE3WqRJ6eD345kU74pjLkfB7ABK5u5KwZrxckNHFvUr4v',
                                gas: 1285,
                                counter: 'e1d0010000000000',
                                gas_limit: '4328000000000000',
                                storage_limit: '1501000000000000',
                                amount: '14193181',
                                parameters: '{ "prim": "Unit" }',
                                entrypoint: 'default',
                                id: 25134264,
                                destination: 'tz1cb9DMkq9ZdrDVxi3HwyLEMtbDseeZPZEa',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.transactions first and date_range gte and before', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS") {
                address
                first_seen
                activated
                transactions(
                  first: 3
                  date_range: {
                    gte: "2019-10-22T18:56:24Z"
                  }
                  before:"ooADLykQuRbk3yUc5f465SRJhGyDtJDFit48WpCp9tipnDK399X:0"
                ) {
                page_info {
                    start_cursor
                    end_cursor
                    has_next_page
                    has_previous_page
                }
                edges {
                        cursor
                        node {
                            hash
                            batch_position
                            source
                            kind
                            timestamp
                            level
                            block
                            gas
                            counter
                            gas_limit
                            storage_limit
                            amount
                            parameters
                            entrypoint
                            id
                            destination
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                first_seen: '2018-09-13T14:51:20Z',
                activated: '2018-09-13T14:49:20.000Z',
                transactions: {
                    page_info: {
                        start_cursor: 'opNxsLZeNMttsyh4PxLk35dmRsJyLXQwJjpVp89ip7pU2j5yzyR:0',
                        end_cursor: 'ootgGUV6dDRfBBF9wutPQZWzeetqiAUkwrkwSL1Qy6NrTNUs6Jw:0',
                        has_next_page: true,
                        has_previous_page: false,
                    },
                    edges: [
                        {
                            cursor: 'opNxsLZeNMttsyh4PxLk35dmRsJyLXQwJjpVp89ip7pU2j5yzyR:0',
                            node: {
                                hash: 'opNxsLZeNMttsyh4PxLk35dmRsJyLXQwJjpVp89ip7pU2j5yzyR',
                                batch_position: 0,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                kind: 'transaction',
                                timestamp: '2020-08-15T22:11:48Z',
                                level: 1085872,
                                block: 'BLEwgk9RsqEfhtm6u3PrLXcu7pXQ6dWMysJRVTAhNRqVqhCsF9g',
                                gas: 1285,
                                counter: 'e2d0010000000000',
                                gas_limit: '4328000000000000',
                                storage_limit: '1501000000000000',
                                amount: '50000000',
                                parameters: '{ "prim": "Unit" }',
                                entrypoint: 'default',
                                id: 26331765,
                                destination: 'tz1UovZ16T6vTuCRQhWKtE7jqeC6QaTzbc3P',
                            },
                        },
                        {
                            cursor: 'opEHTGa4n4efFQfpaWFNqoY4dbzXrBEo7Xo8v74AX5pEREqK28S:0',
                            node: {
                                hash: 'opEHTGa4n4efFQfpaWFNqoY4dbzXrBEo7Xo8v74AX5pEREqK28S',
                                batch_position: 0,
                                source: 'tz1YPCK2iRxWZYHxeUtKBKRosdQ1SAHKeACe',
                                kind: 'transaction',
                                timestamp: '2020-07-17T19:46:24Z',
                                level: 1044207,
                                block: 'BMYhcRrNnHTthRueAKoiyBxyuzJWEMqMgkxzqiVFXHtS84ALKua',
                                gas: 1420,
                                counter: '3ee8200000000000',
                                gas_limit: '6829000000000000',
                                storage_limit: '2c01000000000000',
                                amount: '192000000',
                                parameters: '{ "prim": "Unit" }',
                                entrypoint: 'default',
                                id: 25210462,
                                destination: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                            },
                        },
                        {
                            cursor: 'ootgGUV6dDRfBBF9wutPQZWzeetqiAUkwrkwSL1Qy6NrTNUs6Jw:0',
                            node: {
                                hash: 'ootgGUV6dDRfBBF9wutPQZWzeetqiAUkwrkwSL1Qy6NrTNUs6Jw',
                                batch_position: 0,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                kind: 'transaction',
                                timestamp: '2020-07-15T20:58:27Z',
                                level: 1041413,
                                block: 'BMf1K4SE3WqRJ6eD345kU74pjLkfB7ABK5u5KwZrxckNHFvUr4v',
                                gas: 1285,
                                counter: 'e1d0010000000000',
                                gas_limit: '4328000000000000',
                                storage_limit: '1501000000000000',
                                amount: '14193181',
                                parameters: '{ "prim": "Unit" }',
                                entrypoint: 'default',
                                id: 25134264,
                                destination: 'tz1cb9DMkq9ZdrDVxi3HwyLEMtbDseeZPZEa',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.transactions first and date_range gte and after', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS") {
                address
                first_seen
                activated
                transactions(
                  first: 3
                  date_range: {
                    gte: "2019-10-22T18:56:24Z"
                  }
                  after:"oo1TgCtTmss6LFW2WCuaM1RSDPLKhz2UQz6Ym9XTjiSiw4Dm9gk:0"
                ) {
                page_info {
                    start_cursor
                    end_cursor
                    has_next_page
                    has_previous_page
                }
                edges {
                        cursor
                        node {
                            hash
                            batch_position
                            source
                            kind
                            timestamp
                            level
                            block
                            gas
                            counter
                            gas_limit
                            storage_limit
                            amount
                            parameters
                            entrypoint
                            id
                            destination
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                first_seen: '2018-09-13T14:51:20Z',
                activated: '2018-09-13T14:49:20.000Z',
                transactions: {
                    page_info: {
                        start_cursor: 'ooZM6ErfewYXeUJGN4as8qiHPDT6u465tTuJKF6eQktXDkgtZKW:0',
                        end_cursor: 'oodK29zqUFb7NyMa5mRyb6DVR71FyWHh5gXF8WVgCDG6uGVuCaT:0',
                        has_next_page: true,
                        has_previous_page: true,
                    },
                    edges: [
                        {
                            cursor: 'ooZM6ErfewYXeUJGN4as8qiHPDT6u465tTuJKF6eQktXDkgtZKW:0',
                            node: {
                                hash: 'ooZM6ErfewYXeUJGN4as8qiHPDT6u465tTuJKF6eQktXDkgtZKW',
                                batch_position: 0,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                kind: 'transaction',
                                timestamp: '2020-02-27T17:55:56Z',
                                level: 842861,
                                block: 'BL7YnpqVo7tZEw3hhSVLYNf7RHLMNTRSopuu9CHmQuALvRbwVRP',
                                gas: 1283,
                                counter: 'cbd0010000000000',
                                gas_limit: '4328000000000000',
                                storage_limit: '1501000000000000',
                                amount: '10000',
                                parameters: '{ "prim": "Unit" }',
                                entrypoint: 'default',
                                id: 19920382,
                                destination: 'tz1QkeWxQgkHbmyjJuNKnTJBCNtUxtHtvbJU',
                            },
                        },
                        {
                            cursor: 'ooHN7P3X9pUeDyWWibvZqEaYEPic4xqASsZcxk4WMQ1n7hvojah:0',
                            node: {
                                hash: 'ooHN7P3X9pUeDyWWibvZqEaYEPic4xqASsZcxk4WMQ1n7hvojah',
                                batch_position: 0,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                kind: 'transaction',
                                timestamp: '2020-02-22T22:47:26Z',
                                level: 836082,
                                block: 'BM8ieLyATFHdtgvqhq6t9y9HacUMjhSpfHwSJNobRd2us13L5HR',
                                gas: 1284,
                                counter: 'cad0010000000000',
                                gas_limit: '4328000000000000',
                                storage_limit: '',
                                amount: '100000000',
                                parameters: '{ "prim": "Unit" }',
                                entrypoint: 'default',
                                id: 19731787,
                                destination: 'tz1RAWNEEFr2DSEDQVPnHXhH24GLMDF5XNjJ',
                            },
                        },
                        {
                            cursor: 'oodK29zqUFb7NyMa5mRyb6DVR71FyWHh5gXF8WVgCDG6uGVuCaT:0',
                            node: {
                                hash: 'oodK29zqUFb7NyMa5mRyb6DVR71FyWHh5gXF8WVgCDG6uGVuCaT',
                                batch_position: 0,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                kind: 'transaction',
                                timestamp: '2020-02-22T22:29:26Z',
                                level: 836064,
                                block: 'BKspGC8xE7CT8431tR5CErsn5dEVUB6eVtXLeCSpv4285z3rZa4',
                                gas: 1284,
                                counter: 'c9d0010000000000',
                                gas_limit: '4328000000000000',
                                storage_limit: '1501000000000000',
                                amount: '100000',
                                parameters: '{ "prim": "Unit" }',
                                entrypoint: 'default',
                                id: 19731302,
                                destination: 'tz1VPz5qhzTrRTNevHC33ArBTBPV6jHpwjQB',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.transactions first and date_range gte and order_by', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS") {
                address
                first_seen
                activated
                transactions(
                  first: 3
                  date_range: {
                    gte: "2019-10-22T18:56:24Z"
                  }
                  order_by: {field: ID, direction: ASC}
                ) {
                page_info {
                    start_cursor
                    end_cursor
                    has_next_page
                    has_previous_page
                }
                edges {
                        cursor
                        node {
                            hash
                            batch_position
                            source
                            kind
                            timestamp
                            level
                            block
                            gas
                            counter
                            gas_limit
                            storage_limit
                            amount
                            parameters
                            entrypoint
                            id
                            destination
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                first_seen: '2018-09-13T14:51:20Z',
                activated: '2018-09-13T14:49:20.000Z',
                transactions: {
                    page_info: {
                        start_cursor: 'ooADLykQuRbk3yUc5f465SRJhGyDtJDFit48WpCp9tipnDK399X:0',
                        end_cursor: 'ooHN7P3X9pUeDyWWibvZqEaYEPic4xqASsZcxk4WMQ1n7hvojah:0',
                        has_next_page: true,
                        has_previous_page: false,
                    },
                    edges: [
                        {
                            cursor: 'ooADLykQuRbk3yUc5f465SRJhGyDtJDFit48WpCp9tipnDK399X:0',
                            node: {
                                hash: 'ooADLykQuRbk3yUc5f465SRJhGyDtJDFit48WpCp9tipnDK399X',
                                batch_position: 0,
                                source: 'tz1KtGwriE7VuLwT3LwuvU9Nv4wAxP7XZ57d',
                                kind: 'transaction',
                                timestamp: '2019-10-24T20:38:46Z',
                                level: 664348,
                                block: 'BM8uvwRPXp7QtwUwcr2FtSsooUJcRbnN7hvDhUochXS8dpcXfQ8',
                                gas: 17920,
                                counter: 'e2fe010000000000',
                                gas_limit: 'fa58020000000000',
                                storage_limit: '2c01000000000000',
                                amount: '10009000000',
                                parameters: '{ "prim": "Unit" }',
                                entrypoint: 'default',
                                id: 15227316,
                                destination: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                            },
                        },
                        {
                            cursor: 'oodK29zqUFb7NyMa5mRyb6DVR71FyWHh5gXF8WVgCDG6uGVuCaT:0',
                            node: {
                                hash: 'oodK29zqUFb7NyMa5mRyb6DVR71FyWHh5gXF8WVgCDG6uGVuCaT',
                                batch_position: 0,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                kind: 'transaction',
                                timestamp: '2020-02-22T22:29:26Z',
                                level: 836064,
                                block: 'BKspGC8xE7CT8431tR5CErsn5dEVUB6eVtXLeCSpv4285z3rZa4',
                                gas: 1284,
                                counter: 'c9d0010000000000',
                                gas_limit: '4328000000000000',
                                storage_limit: '1501000000000000',
                                amount: '100000',
                                parameters: '{ "prim": "Unit" }',
                                entrypoint: 'default',
                                id: 19731302,
                                destination: 'tz1VPz5qhzTrRTNevHC33ArBTBPV6jHpwjQB',
                            },
                        },
                        {
                            cursor: 'ooHN7P3X9pUeDyWWibvZqEaYEPic4xqASsZcxk4WMQ1n7hvojah:0',
                            node: {
                                hash: 'ooHN7P3X9pUeDyWWibvZqEaYEPic4xqASsZcxk4WMQ1n7hvojah',
                                batch_position: 0,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                kind: 'transaction',
                                timestamp: '2020-02-22T22:47:26Z',
                                level: 836082,
                                block: 'BM8ieLyATFHdtgvqhq6t9y9HacUMjhSpfHwSJNobRd2us13L5HR',
                                gas: 1284,
                                counter: 'cad0010000000000',
                                gas_limit: '4328000000000000',
                                storage_limit: '',
                                amount: '100000000',
                                parameters: '{ "prim": "Unit" }',
                                entrypoint: 'default',
                                id: 19731787,
                                destination: 'tz1RAWNEEFr2DSEDQVPnHXhH24GLMDF5XNjJ',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.transactions first and date_range lte', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS") {
                address
                first_seen
                activated
                transactions(
                  first: 3
                  date_range: {
                    lte: "2019-10-22T18:56:24Z"
                  }
                ) {
                page_info {
                    start_cursor
                    end_cursor
                    has_next_page
                    has_previous_page
                }
                edges {
                        cursor
                        node {
                            hash
                            batch_position
                            source
                            kind
                            timestamp
                            level
                            block
                            gas
                            counter
                            gas_limit
                            storage_limit
                            amount
                            parameters
                            entrypoint
                            id
                            destination
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                first_seen: '2018-09-13T14:51:20Z',
                activated: '2018-09-13T14:49:20.000Z',
                transactions: {
                    page_info: {
                        start_cursor: 'opT4s5rWRbAEcbq6GZ272LyxNCxACqdk7c6Jm4buDcR6Fh4Zs3c:0',
                        end_cursor: 'opZSVPxNoaExjX6dCFnoPAh5yHk6g7BaJdce2AXUHuwW3c7ucNA:0',
                        has_next_page: true,
                        has_previous_page: false,
                    },
                    edges: [
                        {
                            cursor: 'opT4s5rWRbAEcbq6GZ272LyxNCxACqdk7c6Jm4buDcR6Fh4Zs3c:0',
                            node: {
                                hash: 'opT4s5rWRbAEcbq6GZ272LyxNCxACqdk7c6Jm4buDcR6Fh4Zs3c',
                                batch_position: 0,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                kind: 'transaction',
                                timestamp: '2019-06-23T00:44:47Z',
                                level: 491770,
                                block: 'BLyWtfcsLhVGAbsYJPSFpUQiPwdHkHExBrgB3WD9wesuo3PLfUV',
                                gas: 1564,
                                counter: 'c8d0010000000000',
                                gas_limit: '4533000000000000',
                                storage_limit: '',
                                amount: '0',
                                parameters: null,
                                entrypoint: null,
                                id: 10910203,
                                destination: 'KT1BRudFZEXLYANgmZTka1xCDN5nWTMWY7SZ',
                            },
                        },
                        {
                            cursor: 'oobMbc7MHSz5ttV3AcjiXqaxZSxs9ShGjcHmopoo85Jays2rniJ:0',
                            node: {
                                hash: 'oobMbc7MHSz5ttV3AcjiXqaxZSxs9ShGjcHmopoo85Jays2rniJ',
                                batch_position: 0,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                kind: 'transaction',
                                timestamp: '2019-06-13T16:31:03Z',
                                level: 478922,
                                block: 'BKsTAwJ939thYVT5gN4HGE8kPApsRUXELGySbv7CNgXZ9VkGKar',
                                gas: 1284,
                                counter: 'c7d0010000000000',
                                gas_limit: '3c28000000000000',
                                storage_limit: '',
                                amount: '21594067',
                                parameters: null,
                                entrypoint: null,
                                id: 10588192,
                                destination: 'KT1SrkUKHSf9m2yruXSMCVfrfUUsXN8yVtag',
                            },
                        },
                        {
                            cursor: 'opZSVPxNoaExjX6dCFnoPAh5yHk6g7BaJdce2AXUHuwW3c7ucNA:0',
                            node: {
                                hash: 'opZSVPxNoaExjX6dCFnoPAh5yHk6g7BaJdce2AXUHuwW3c7ucNA',
                                batch_position: 0,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                kind: 'transaction',
                                timestamp: '2019-06-13T16:29:03Z',
                                level: 478920,
                                block: 'BME5VpmuMaZfWK7sgrmqrH73yZnuiDhJGuE8dsURJTodB5LWDCe',
                                gas: 1284,
                                counter: 'c6d0010000000000',
                                gas_limit: '3c28000000000000',
                                storage_limit: '',
                                amount: '26594806',
                                parameters: null,
                                entrypoint: null,
                                id: 10588145,
                                destination: 'KT1Hw82JKNLeQUF58Tr5s8hk3Yn8XW3Hv81C',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.transactions first and date_range lte and before', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS") {
                address
                first_seen
                activated
                transactions(
                  first: 3
                  date_range: {
                    lte: "2019-10-22T18:56:24Z"
                  }
                  before: "opZSVPxNoaExjX6dCFnoPAh5yHk6g7BaJdce2AXUHuwW3c7ucNA:0"
                ) {
                page_info {
                    start_cursor
                    end_cursor
                    has_next_page
                    has_previous_page
                }
                edges {
                        cursor
                        node {
                            hash
                            batch_position
                            source
                            kind
                            timestamp
                            level
                            block
                            gas
                            counter
                            gas_limit
                            storage_limit
                            amount
                            parameters
                            entrypoint
                            id
                            destination
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                first_seen: '2018-09-13T14:51:20Z',
                activated: '2018-09-13T14:49:20.000Z',
                transactions: {
                    page_info: {
                        start_cursor: 'opT4s5rWRbAEcbq6GZ272LyxNCxACqdk7c6Jm4buDcR6Fh4Zs3c:0',
                        end_cursor: 'oobMbc7MHSz5ttV3AcjiXqaxZSxs9ShGjcHmopoo85Jays2rniJ:0',
                        has_next_page: true,
                        has_previous_page: false,
                    },
                    edges: [
                        {
                            cursor: 'opT4s5rWRbAEcbq6GZ272LyxNCxACqdk7c6Jm4buDcR6Fh4Zs3c:0',
                            node: {
                                hash: 'opT4s5rWRbAEcbq6GZ272LyxNCxACqdk7c6Jm4buDcR6Fh4Zs3c',
                                batch_position: 0,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                kind: 'transaction',
                                timestamp: '2019-06-23T00:44:47Z',
                                level: 491770,
                                block: 'BLyWtfcsLhVGAbsYJPSFpUQiPwdHkHExBrgB3WD9wesuo3PLfUV',
                                gas: 1564,
                                counter: 'c8d0010000000000',
                                gas_limit: '4533000000000000',
                                storage_limit: '',
                                amount: '0',
                                parameters: null,
                                entrypoint: null,
                                id: 10910203,
                                destination: 'KT1BRudFZEXLYANgmZTka1xCDN5nWTMWY7SZ',
                            },
                        },
                        {
                            cursor: 'oobMbc7MHSz5ttV3AcjiXqaxZSxs9ShGjcHmopoo85Jays2rniJ:0',
                            node: {
                                hash: 'oobMbc7MHSz5ttV3AcjiXqaxZSxs9ShGjcHmopoo85Jays2rniJ',
                                batch_position: 0,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                kind: 'transaction',
                                timestamp: '2019-06-13T16:31:03Z',
                                level: 478922,
                                block: 'BKsTAwJ939thYVT5gN4HGE8kPApsRUXELGySbv7CNgXZ9VkGKar',
                                gas: 1284,
                                counter: 'c7d0010000000000',
                                gas_limit: '3c28000000000000',
                                storage_limit: '',
                                amount: '21594067',
                                parameters: null,
                                entrypoint: null,
                                id: 10588192,
                                destination: 'KT1SrkUKHSf9m2yruXSMCVfrfUUsXN8yVtag',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.transactions first and date_range lte and after', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS") {
                address
                first_seen
                activated
                transactions(
                  first: 3
                  date_range: {
                    lte: "2019-10-22T18:56:24Z"
                  }
                  after: "opZSVPxNoaExjX6dCFnoPAh5yHk6g7BaJdce2AXUHuwW3c7ucNA:0"
                ) {
                page_info {
                    start_cursor
                    end_cursor
                    has_next_page
                    has_previous_page
                }
                edges {
                        cursor
                        node {
                            hash
                            batch_position
                            source
                            kind
                            timestamp
                            level
                            block
                            gas
                            counter
                            gas_limit
                            storage_limit
                            amount
                            parameters
                            entrypoint
                            id
                            destination
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                first_seen: '2018-09-13T14:51:20Z',
                activated: '2018-09-13T14:49:20.000Z',
                transactions: {
                    page_info: {
                        start_cursor: 'ooZ59b3ddyATdUsbiWwX6gco4hZ7VJeBbucCjvAbNyggrnTDKbN:0',
                        end_cursor: 'onfyzsehpe5MpHPZK1iTGkFxqpuhG2Xny3L9X24VCBXNRDD27pg:0',
                        has_next_page: true,
                        has_previous_page: true,
                    },
                    edges: [
                        {
                            cursor: 'ooZ59b3ddyATdUsbiWwX6gco4hZ7VJeBbucCjvAbNyggrnTDKbN:0',
                            node: {
                                hash: 'ooZ59b3ddyATdUsbiWwX6gco4hZ7VJeBbucCjvAbNyggrnTDKbN',
                                batch_position: 0,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                kind: 'transaction',
                                timestamp: '2019-06-13T16:27:03Z',
                                level: 478918,
                                block: 'BLhWGpWqz19bGcLYoJxotAxm17vNGjymwMbGK5yizRRMQhkxEai',
                                gas: 1284,
                                counter: 'c5d0010000000000',
                                gas_limit: '3c28000000000000',
                                storage_limit: '',
                                amount: '166379804',
                                parameters: null,
                                entrypoint: null,
                                id: 10588092,
                                destination: 'KT1FPhB9bBH6BXH7a7DsX5eBUvJRyqcZhfyn',
                            },
                        },
                        {
                            cursor: 'op2Jk1AuscHtt6NWL58ETYmqenNHK6C1KSv7Rk1rPzadSTKqpL1:0',
                            node: {
                                hash: 'op2Jk1AuscHtt6NWL58ETYmqenNHK6C1KSv7Rk1rPzadSTKqpL1',
                                batch_position: 0,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                kind: 'transaction',
                                timestamp: '2019-06-13T16:26:03Z',
                                level: 478917,
                                block: 'BLBgbfhyuDD3Q2b7mAKm2S4kroYLVDp3gbkRpU7PPaZLDodi11e',
                                gas: 1284,
                                counter: 'c4d0010000000000',
                                gas_limit: '3c28000000000000',
                                storage_limit: '',
                                amount: '171235783',
                                parameters: null,
                                entrypoint: null,
                                id: 10588066,
                                destination: 'KT1C8f3CYXEwuvvvGeG9pKMLWa2Xcyci8eUn',
                            },
                        },
                        {
                            cursor: 'onfyzsehpe5MpHPZK1iTGkFxqpuhG2Xny3L9X24VCBXNRDD27pg:0',
                            node: {
                                hash: 'onfyzsehpe5MpHPZK1iTGkFxqpuhG2Xny3L9X24VCBXNRDD27pg',
                                batch_position: 0,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                kind: 'transaction',
                                timestamp: '2019-06-13T16:25:03Z',
                                level: 478916,
                                block: 'BKn6bMx7wiB7eH6RfEHtWnrGanESHcPHzWe51gJz8zxFyUkuLbd',
                                gas: 1284,
                                counter: 'c3d0010000000000',
                                gas_limit: '3c28000000000000',
                                storage_limit: '',
                                amount: '134384524',
                                parameters: null,
                                entrypoint: null,
                                id: 10588044,
                                destination: 'KT19UEmbszRSrfedo1DcRqbsYzo6UceKSN1N',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.transactions first and date_range lte and order_by', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS") {
                address
                first_seen
                activated
                transactions(
                first: 3
                date_range: {
                    lte: "2019-10-22T18:56:24Z"
                }
                order_by: {field: ID, direction: ASC}
                ) {
                page_info {
                    start_cursor
                    end_cursor
                    has_next_page
                    has_previous_page
                }
                edges {
                        cursor
                        node {
                            hash
                            batch_position
                            source
                            kind
                            timestamp
                            level
                            block
                            gas
                            counter
                            gas_limit
                            storage_limit
                            amount
                            parameters
                            entrypoint
                            id
                            destination
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                first_seen: '2018-09-13T14:51:20Z',
                activated: '2018-09-13T14:49:20.000Z',
                transactions: {
                    page_info: {
                        start_cursor: 'opQqKKXhzznFRxMPdiLyP4V1KWfuDrrNSkqF8jQ6v4JENR3K6jP:0',
                        end_cursor: 'ooe9ypjsaQZPhowQpj69MZicQ3ZUwFGxQJyMVxdGpMRWF9SBZWr:0',
                        has_next_page: true,
                        has_previous_page: false,
                    },
                    edges: [
                        {
                            cursor: 'opQqKKXhzznFRxMPdiLyP4V1KWfuDrrNSkqF8jQ6v4JENR3K6jP:0',
                            node: {
                                hash: 'opQqKKXhzznFRxMPdiLyP4V1KWfuDrrNSkqF8jQ6v4JENR3K6jP',
                                batch_position: 0,
                                source: 'tz1LBEKXaxQbd5Gtzbc1ATCwc3pppu81aWGc',
                                kind: 'transaction',
                                timestamp: '2018-09-13T14:51:20Z',
                                level: 101957,
                                block: 'BMQzE1UWod12P5XkE15fS4CNRvKLqmWknwCEDuTP1FKEhzrVjtU',
                                gas: 10,
                                counter: '62e7000000000000',
                                gas_limit: 'c800000000000000',
                                storage_limit: '',
                                amount: '14',
                                parameters: null,
                                entrypoint: null,
                                id: 1425714,
                                destination: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                            },
                        },
                        {
                            cursor: 'oop8aPtDYg7K3xqqMadPKoXzxTLNpBukasrKtBQs9bsjXQ1UWVt:1',
                            node: {
                                hash: 'oop8aPtDYg7K3xqqMadPKoXzxTLNpBukasrKtBQs9bsjXQ1UWVt',
                                batch_position: 1,
                                source: 'tz1PECYmqmuba13rnNxyoBkvH2ChUt8BuJuG',
                                kind: 'transaction',
                                timestamp: '2018-09-13T16:12:50Z',
                                level: 102031,
                                block: 'BLowVvoAyjeoMY4BRja6rEPdX37UjfdeB4JFtgQ4ZvCiRg9MNdA',
                                gas: 0,
                                counter: '9d4d000000000000',
                                gas_limit: '',
                                storage_limit: '',
                                amount: '1000000',
                                parameters: null,
                                entrypoint: null,
                                id: 1427150,
                                destination: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                            },
                        },
                        {
                            cursor: 'ooe9ypjsaQZPhowQpj69MZicQ3ZUwFGxQJyMVxdGpMRWF9SBZWr:0',
                            node: {
                                hash: 'ooe9ypjsaQZPhowQpj69MZicQ3ZUwFGxQJyMVxdGpMRWF9SBZWr',
                                batch_position: 0,
                                source: 'tz1PECYmqmuba13rnNxyoBkvH2ChUt8BuJuG',
                                kind: 'transaction',
                                timestamp: '2018-09-13T16:17:05Z',
                                level: 102034,
                                block: 'BLFRNMJkWm9aRBgTwEdhZGcgKZ3idCWYWua8bRCfhx1BBU5xMmJ',
                                gas: 0,
                                counter: '9f4d000000000000',
                                gas_limit: 'c800000000000000',
                                storage_limit: '',
                                amount: '4199800000',
                                parameters: null,
                                entrypoint: null,
                                id: 1427201,
                                destination: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.transactions first and date_range gte/lte', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS") {
                address
                first_seen
                activated
                transactions(
                  first: 3
                  date_range: {
                    gte: "2019-06-13T16:25:03Z"
                    lte: "2020-07-15T20:58:27Z"
                  }
                ) {
                page_info {
                    start_cursor
                    end_cursor
                    has_next_page
                    has_previous_page
                }
                edges {
                        cursor
                        node {
                            hash
                            batch_position
                            source
                            kind
                            timestamp
                            level
                            block
                            gas
                            counter
                            gas_limit
                            storage_limit
                            amount
                            parameters
                            entrypoint
                            id
                            destination
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                first_seen: '2018-09-13T14:51:20Z',
                activated: '2018-09-13T14:49:20.000Z',
                transactions: {
                    page_info: {
                        start_cursor: 'ootgGUV6dDRfBBF9wutPQZWzeetqiAUkwrkwSL1Qy6NrTNUs6Jw:0',
                        end_cursor: 'oowUdQhpUKrsM9UgPBxPXQHq5ri9BVZGsEWXvFk2w48GkZZXxno:0',
                        has_next_page: true,
                        has_previous_page: false,
                    },
                    edges: [
                        {
                            cursor: 'ootgGUV6dDRfBBF9wutPQZWzeetqiAUkwrkwSL1Qy6NrTNUs6Jw:0',
                            node: {
                                hash: 'ootgGUV6dDRfBBF9wutPQZWzeetqiAUkwrkwSL1Qy6NrTNUs6Jw',
                                batch_position: 0,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                kind: 'transaction',
                                timestamp: '2020-07-15T20:58:27Z',
                                level: 1041413,
                                block: 'BMf1K4SE3WqRJ6eD345kU74pjLkfB7ABK5u5KwZrxckNHFvUr4v',
                                gas: 1285,
                                counter: 'e1d0010000000000',
                                gas_limit: '4328000000000000',
                                storage_limit: '1501000000000000',
                                amount: '14193181',
                                parameters: '{ "prim": "Unit" }',
                                entrypoint: 'default',
                                id: 25134264,
                                destination: 'tz1cb9DMkq9ZdrDVxi3HwyLEMtbDseeZPZEa',
                            },
                        },
                        {
                            cursor: 'ongxtfJMZ6tdbwxUKE46463gk6rVuWHk6WNWHcSgN1Q6obmm6q1:0',
                            node: {
                                hash: 'ongxtfJMZ6tdbwxUKE46463gk6rVuWHk6WNWHcSgN1Q6obmm6q1',
                                batch_position: 0,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                kind: 'transaction',
                                timestamp: '2020-07-15T20:57:27Z',
                                level: 1041412,
                                block: 'BLFSnPLffMKuRvDR6MQErKxLitKevszN2UkwnJTNUQiGLQ6d7cM',
                                gas: 1284,
                                counter: 'e0d0010000000000',
                                gas_limit: '4328000000000000',
                                storage_limit: '',
                                amount: '188397246',
                                parameters: '{ "prim": "Unit" }',
                                entrypoint: 'default',
                                id: 25134234,
                                destination: 'tz1Vg5uaxedKk4HYn4ZMWWZ7B59GYXSm2iyj',
                            },
                        },
                        {
                            cursor: 'oowUdQhpUKrsM9UgPBxPXQHq5ri9BVZGsEWXvFk2w48GkZZXxno:0',
                            node: {
                                hash: 'oowUdQhpUKrsM9UgPBxPXQHq5ri9BVZGsEWXvFk2w48GkZZXxno',
                                batch_position: 0,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                kind: 'transaction',
                                timestamp: '2020-07-15T20:56:27Z',
                                level: 1041411,
                                block: 'BL25QMmdBxgkic7PB2z53B38rPTMYy3kAfPevQXDDsBrzbbRy6j',
                                gas: 1284,
                                counter: 'dfd0010000000000',
                                gas_limit: '4328000000000000',
                                storage_limit: '',
                                amount: '79651872',
                                parameters: '{ "prim": "Unit" }',
                                entrypoint: 'default',
                                id: 25134203,
                                destination: 'tz1bJAQfucuxvLwCHyPXvG24PjtLT1V3ewbt',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.transactions first and date_range gte/lte and before', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS") {
                address
                first_seen
                activated
                transactions(
                  first: 3
                  date_range: {
                    gte: "2019-06-13T16:25:03Z"
                    lte: "2019-10-27T12:46:11Z"
                  }
                  before:"opZSVPxNoaExjX6dCFnoPAh5yHk6g7BaJdce2AXUHuwW3c7ucNA:0"
                ) {
                page_info {
                    start_cursor
                    end_cursor
                    has_next_page
                    has_previous_page
                }
                edges {
                        cursor
                        node {
                            hash
                            batch_position
                            source
                            kind
                            timestamp
                            level
                            block
                            gas
                            counter
                            gas_limit
                            storage_limit
                            amount
                            parameters
                            entrypoint
                            id
                            destination
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                first_seen: '2018-09-13T14:51:20Z',
                activated: '2018-09-13T14:49:20.000Z',
                transactions: {
                    page_info: {
                        start_cursor: 'ooADLykQuRbk3yUc5f465SRJhGyDtJDFit48WpCp9tipnDK399X:0',
                        end_cursor: 'oobMbc7MHSz5ttV3AcjiXqaxZSxs9ShGjcHmopoo85Jays2rniJ:0',
                        has_next_page: true,
                        has_previous_page: false,
                    },
                    edges: [
                        {
                            cursor: 'ooADLykQuRbk3yUc5f465SRJhGyDtJDFit48WpCp9tipnDK399X:0',
                            node: {
                                hash: 'ooADLykQuRbk3yUc5f465SRJhGyDtJDFit48WpCp9tipnDK399X',
                                batch_position: 0,
                                source: 'tz1KtGwriE7VuLwT3LwuvU9Nv4wAxP7XZ57d',
                                kind: 'transaction',
                                timestamp: '2019-10-24T20:38:46Z',
                                level: 664348,
                                block: 'BM8uvwRPXp7QtwUwcr2FtSsooUJcRbnN7hvDhUochXS8dpcXfQ8',
                                gas: 17920,
                                counter: 'e2fe010000000000',
                                gas_limit: 'fa58020000000000',
                                storage_limit: '2c01000000000000',
                                amount: '10009000000',
                                parameters: '{ "prim": "Unit" }',
                                entrypoint: 'default',
                                id: 15227316,
                                destination: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                            },
                        },
                        {
                            cursor: 'opT4s5rWRbAEcbq6GZ272LyxNCxACqdk7c6Jm4buDcR6Fh4Zs3c:0',
                            node: {
                                hash: 'opT4s5rWRbAEcbq6GZ272LyxNCxACqdk7c6Jm4buDcR6Fh4Zs3c',
                                batch_position: 0,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                kind: 'transaction',
                                timestamp: '2019-06-23T00:44:47Z',
                                level: 491770,
                                block: 'BLyWtfcsLhVGAbsYJPSFpUQiPwdHkHExBrgB3WD9wesuo3PLfUV',
                                gas: 1564,
                                counter: 'c8d0010000000000',
                                gas_limit: '4533000000000000',
                                storage_limit: '',
                                amount: '0',
                                parameters: null,
                                entrypoint: null,
                                id: 10910203,
                                destination: 'KT1BRudFZEXLYANgmZTka1xCDN5nWTMWY7SZ',
                            },
                        },
                        {
                            cursor: 'oobMbc7MHSz5ttV3AcjiXqaxZSxs9ShGjcHmopoo85Jays2rniJ:0',
                            node: {
                                hash: 'oobMbc7MHSz5ttV3AcjiXqaxZSxs9ShGjcHmopoo85Jays2rniJ',
                                batch_position: 0,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                kind: 'transaction',
                                timestamp: '2019-06-13T16:31:03Z',
                                level: 478922,
                                block: 'BKsTAwJ939thYVT5gN4HGE8kPApsRUXELGySbv7CNgXZ9VkGKar',
                                gas: 1284,
                                counter: 'c7d0010000000000',
                                gas_limit: '3c28000000000000',
                                storage_limit: '',
                                amount: '21594067',
                                parameters: null,
                                entrypoint: null,
                                id: 10588192,
                                destination: 'KT1SrkUKHSf9m2yruXSMCVfrfUUsXN8yVtag',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.transactions first and date_range gte/lte and after', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS") {
                address
                first_seen
                activated
                transactions(
                  first: 3
                  date_range: {
                    gte: "2019-06-13T16:25:03Z"
                    lte: "2020-07-15T20:58:27Z"
                  }
                after:"opT4s5rWRbAEcbq6GZ272LyxNCxACqdk7c6Jm4buDcR6Fh4Zs3c:0"
                ) {
                page_info {
                    start_cursor
                    end_cursor
                    has_next_page
                    has_previous_page
                }
                edges {
                        cursor
                        node {
                            hash
                            batch_position
                            source
                            kind
                            timestamp
                            level
                            block
                            gas
                            counter
                            gas_limit
                            storage_limit
                            amount
                            parameters
                            entrypoint
                            id
                            destination
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                first_seen: '2018-09-13T14:51:20Z',
                activated: '2018-09-13T14:49:20.000Z',
                transactions: {
                    page_info: {
                        start_cursor: 'oobMbc7MHSz5ttV3AcjiXqaxZSxs9ShGjcHmopoo85Jays2rniJ:0',
                        end_cursor: 'ooZ59b3ddyATdUsbiWwX6gco4hZ7VJeBbucCjvAbNyggrnTDKbN:0',
                        has_next_page: true,
                        has_previous_page: true,
                    },
                    edges: [
                        {
                            cursor: 'oobMbc7MHSz5ttV3AcjiXqaxZSxs9ShGjcHmopoo85Jays2rniJ:0',
                            node: {
                                hash: 'oobMbc7MHSz5ttV3AcjiXqaxZSxs9ShGjcHmopoo85Jays2rniJ',
                                batch_position: 0,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                kind: 'transaction',
                                timestamp: '2019-06-13T16:31:03Z',
                                level: 478922,
                                block: 'BKsTAwJ939thYVT5gN4HGE8kPApsRUXELGySbv7CNgXZ9VkGKar',
                                gas: 1284,
                                counter: 'c7d0010000000000',
                                gas_limit: '3c28000000000000',
                                storage_limit: '',
                                amount: '21594067',
                                parameters: null,
                                entrypoint: null,
                                id: 10588192,
                                destination: 'KT1SrkUKHSf9m2yruXSMCVfrfUUsXN8yVtag',
                            },
                        },
                        {
                            cursor: 'opZSVPxNoaExjX6dCFnoPAh5yHk6g7BaJdce2AXUHuwW3c7ucNA:0',
                            node: {
                                hash: 'opZSVPxNoaExjX6dCFnoPAh5yHk6g7BaJdce2AXUHuwW3c7ucNA',
                                batch_position: 0,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                kind: 'transaction',
                                timestamp: '2019-06-13T16:29:03Z',
                                level: 478920,
                                block: 'BME5VpmuMaZfWK7sgrmqrH73yZnuiDhJGuE8dsURJTodB5LWDCe',
                                gas: 1284,
                                counter: 'c6d0010000000000',
                                gas_limit: '3c28000000000000',
                                storage_limit: '',
                                amount: '26594806',
                                parameters: null,
                                entrypoint: null,
                                id: 10588145,
                                destination: 'KT1Hw82JKNLeQUF58Tr5s8hk3Yn8XW3Hv81C',
                            },
                        },
                        {
                            cursor: 'ooZ59b3ddyATdUsbiWwX6gco4hZ7VJeBbucCjvAbNyggrnTDKbN:0',
                            node: {
                                hash: 'ooZ59b3ddyATdUsbiWwX6gco4hZ7VJeBbucCjvAbNyggrnTDKbN',
                                batch_position: 0,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                kind: 'transaction',
                                timestamp: '2019-06-13T16:27:03Z',
                                level: 478918,
                                block: 'BLhWGpWqz19bGcLYoJxotAxm17vNGjymwMbGK5yizRRMQhkxEai',
                                gas: 1284,
                                counter: 'c5d0010000000000',
                                gas_limit: '3c28000000000000',
                                storage_limit: '',
                                amount: '166379804',
                                parameters: null,
                                entrypoint: null,
                                id: 10588092,
                                destination: 'KT1FPhB9bBH6BXH7a7DsX5eBUvJRyqcZhfyn',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.transactions first and date_range gte/lte and order_by', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS") {
                address
                first_seen
                activated
                transactions(
                  first: 3
                  date_range: {
                    gte: "2019-06-13T16:25:03Z"
                    lte: "2020-07-15T20:58:27Z"
                  }
                  order_by: {field: ID, direction: ASC}
                ) {
                page_info {
                    start_cursor
                    end_cursor
                    has_next_page
                    has_previous_page
                }
                edges {
                        cursor
                        node {
                            hash
                            batch_position
                            source
                            kind
                            timestamp
                            level
                            block
                            gas
                            counter
                            gas_limit
                            storage_limit
                            amount
                            parameters
                            entrypoint
                            id
                            destination
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                first_seen: '2018-09-13T14:51:20Z',
                activated: '2018-09-13T14:49:20.000Z',
                transactions: {
                    page_info: {
                        start_cursor: 'onfyzsehpe5MpHPZK1iTGkFxqpuhG2Xny3L9X24VCBXNRDD27pg:0',
                        end_cursor: 'ooZ59b3ddyATdUsbiWwX6gco4hZ7VJeBbucCjvAbNyggrnTDKbN:0',
                        has_next_page: true,
                        has_previous_page: false,
                    },
                    edges: [
                        {
                            cursor: 'onfyzsehpe5MpHPZK1iTGkFxqpuhG2Xny3L9X24VCBXNRDD27pg:0',
                            node: {
                                hash: 'onfyzsehpe5MpHPZK1iTGkFxqpuhG2Xny3L9X24VCBXNRDD27pg',
                                batch_position: 0,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                kind: 'transaction',
                                timestamp: '2019-06-13T16:25:03Z',
                                level: 478916,
                                block: 'BKn6bMx7wiB7eH6RfEHtWnrGanESHcPHzWe51gJz8zxFyUkuLbd',
                                gas: 1284,
                                counter: 'c3d0010000000000',
                                gas_limit: '3c28000000000000',
                                storage_limit: '',
                                amount: '134384524',
                                parameters: null,
                                entrypoint: null,
                                id: 10588044,
                                destination: 'KT19UEmbszRSrfedo1DcRqbsYzo6UceKSN1N',
                            },
                        },
                        {
                            cursor: 'op2Jk1AuscHtt6NWL58ETYmqenNHK6C1KSv7Rk1rPzadSTKqpL1:0',
                            node: {
                                hash: 'op2Jk1AuscHtt6NWL58ETYmqenNHK6C1KSv7Rk1rPzadSTKqpL1',
                                batch_position: 0,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                kind: 'transaction',
                                timestamp: '2019-06-13T16:26:03Z',
                                level: 478917,
                                block: 'BLBgbfhyuDD3Q2b7mAKm2S4kroYLVDp3gbkRpU7PPaZLDodi11e',
                                gas: 1284,
                                counter: 'c4d0010000000000',
                                gas_limit: '3c28000000000000',
                                storage_limit: '',
                                amount: '171235783',
                                parameters: null,
                                entrypoint: null,
                                id: 10588066,
                                destination: 'KT1C8f3CYXEwuvvvGeG9pKMLWa2Xcyci8eUn',
                            },
                        },
                        {
                            cursor: 'ooZ59b3ddyATdUsbiWwX6gco4hZ7VJeBbucCjvAbNyggrnTDKbN:0',
                            node: {
                                hash: 'ooZ59b3ddyATdUsbiWwX6gco4hZ7VJeBbucCjvAbNyggrnTDKbN',
                                batch_position: 0,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                kind: 'transaction',
                                timestamp: '2019-06-13T16:27:03Z',
                                level: 478918,
                                block: 'BLhWGpWqz19bGcLYoJxotAxm17vNGjymwMbGK5yizRRMQhkxEai',
                                gas: 1284,
                                counter: 'c5d0010000000000',
                                gas_limit: '3c28000000000000',
                                storage_limit: '',
                                amount: '166379804',
                                parameters: null,
                                entrypoint: null,
                                id: 10588092,
                                destination: 'KT1FPhB9bBH6BXH7a7DsX5eBUvJRyqcZhfyn',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.transactions first and date_range gte and order_by and before', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS") {
                address
                first_seen
                activated
                transactions(
                  first: 3
                  date_range: {
                    gte: "2020-03-14T03:09:17Z"
                  }
                  order_by: {field: ID, direction: ASC}
                  before: "oneN9h5T5KahttNrycakF98MmtwJgHwTup5X32ZjpsCpHgPx1vk:0"
                ) {
                page_info {
                    start_cursor
                    end_cursor
                    has_next_page
                    has_previous_page
                }
                edges {
                        cursor
                        node {
                            hash
                            batch_position
                            source
                            kind
                            timestamp
                            level
                            block
                            gas
                            counter
                            gas_limit
                            storage_limit
                            amount
                            parameters
                            entrypoint
                            id
                            destination
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                first_seen: '2018-09-13T14:51:20Z',
                activated: '2018-09-13T14:49:20.000Z',
                transactions: {
                    page_info: {
                        start_cursor: 'oowsKfXqnNGCebjHJMUooAGynGrV5h5EPnY8Ck6NaNEMvdVf4bN:0',
                        end_cursor: 'opNutoZNVCcEsKMYcemrxss38w2ezYjx9UwEDv3cLFAc5HLGSBB:0',
                        has_next_page: true,
                        has_previous_page: false,
                    },
                    edges: [
                        {
                            cursor: 'oowsKfXqnNGCebjHJMUooAGynGrV5h5EPnY8Ck6NaNEMvdVf4bN:0',
                            node: {
                                hash: 'oowsKfXqnNGCebjHJMUooAGynGrV5h5EPnY8Ck6NaNEMvdVf4bN',
                                batch_position: 0,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                kind: 'transaction',
                                timestamp: '2020-03-14T03:09:17Z',
                                level: 864385,
                                block: 'BLLgqAFcqFsq42je9G5YrivWksnfG5BfCCmSJSfFGCoRzSTQbTg',
                                gas: 1284,
                                counter: 'ced0010000000000',
                                gas_limit: '4328000000000000',
                                storage_limit: '1501000000000000',
                                amount: '1000000',
                                parameters: '{ "prim": "Unit" }',
                                entrypoint: 'default',
                                id: 20490954,
                                destination: 'tz1dA6Qk3L16sqUFZhYg4dmWm8raHntA8Wip',
                            },
                        },
                        {
                            cursor: 'onvTurPZ3KJYZfvbiiBnMW9tY13Bfymfrt6dSUbis5wuRNRHZEQ:0',
                            node: {
                                hash: 'onvTurPZ3KJYZfvbiiBnMW9tY13Bfymfrt6dSUbis5wuRNRHZEQ',
                                batch_position: 0,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                kind: 'transaction',
                                timestamp: '2020-03-14T03:15:17Z',
                                level: 864391,
                                block: 'BMdTg2Y1mkP6wz2yiXSUvJpDocZgm1dYW14Tc6KZyXmUng1F9gP',
                                gas: 1285,
                                counter: 'cfd0010000000000',
                                gas_limit: '4328000000000000',
                                storage_limit: '',
                                amount: '9999000000',
                                parameters: '{ "prim": "Unit" }',
                                entrypoint: 'default',
                                id: 20491117,
                                destination: 'tz1dA6Qk3L16sqUFZhYg4dmWm8raHntA8Wip',
                            },
                        },
                        {
                            cursor: 'opNutoZNVCcEsKMYcemrxss38w2ezYjx9UwEDv3cLFAc5HLGSBB:0',
                            node: {
                                hash: 'opNutoZNVCcEsKMYcemrxss38w2ezYjx9UwEDv3cLFAc5HLGSBB',
                                batch_position: 0,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                kind: 'transaction',
                                timestamp: '2020-03-14T03:30:37Z',
                                level: 864405,
                                block: 'BLPUnxUKvUf4bp5BJHLbmuGfnfjTAN7VxnzbfWQ4HVgPvFCftVV',
                                gas: 1284,
                                counter: 'd0d0010000000000',
                                gas_limit: '4328000000000000',
                                storage_limit: '',
                                amount: '199900000',
                                parameters: '{ "prim": "Unit" }',
                                entrypoint: 'default',
                                id: 20491513,
                                destination: 'tz1VPz5qhzTrRTNevHC33ArBTBPV6jHpwjQB',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.transactions first and date_range gte and order_by and after', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS") {
                address
                first_seen
                activated
                transactions(
                  first: 3
                  date_range: {
                    gte: "2020-03-14T03:09:17Z"
                  }
                  order_by: {field: ID, direction: ASC}
                  after: "oneN9h5T5KahttNrycakF98MmtwJgHwTup5X32ZjpsCpHgPx1vk:0"
                ) {
                page_info {
                    start_cursor
                    end_cursor
                    has_next_page
                    has_previous_page
                }
                edges {
                        cursor
                        node {
                            hash
                            batch_position
                            source
                            kind
                            timestamp
                            level
                            block
                            gas
                            counter
                            gas_limit
                            storage_limit
                            amount
                            parameters
                            entrypoint
                            id
                            destination
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                first_seen: '2018-09-13T14:51:20Z',
                activated: '2018-09-13T14:49:20.000Z',
                transactions: {
                    page_info: {
                        start_cursor: 'ooF1n8wiRoEmqrUatogBwVmQocZ2Me41EKBnaYBhWCySscJWQsn:0',
                        end_cursor: 'opLgb1F3FavKLT2GA3y8PV2ZTwqvJ6zjrdWSdvEVVguarGpCSQ9:0',
                        has_next_page: true,
                        has_previous_page: true,
                    },
                    edges: [
                        {
                            cursor: 'ooF1n8wiRoEmqrUatogBwVmQocZ2Me41EKBnaYBhWCySscJWQsn:0',
                            node: {
                                hash: 'ooF1n8wiRoEmqrUatogBwVmQocZ2Me41EKBnaYBhWCySscJWQsn',
                                batch_position: 0,
                                source: 'tz1YPCK2iRxWZYHxeUtKBKRosdQ1SAHKeACe',
                                kind: 'transaction',
                                timestamp: '2020-04-16T14:23:12Z',
                                level: 911974,
                                block: 'BMHzo6Stgch5vi4QQcjqKSwuRyhUFk8zNZGAu5s6VdKHmb9RGHe',
                                gas: 2500,
                                counter: '3ce8200000000000',
                                gas_limit: '5046000000000000',
                                storage_limit: '2c01000000000000',
                                amount: '1000',
                                parameters: '{ "prim": "Unit" }',
                                entrypoint: 'default',
                                id: 21724448,
                                destination: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                            },
                        },
                        {
                            cursor: 'onvmb4RUskLnMsP3Qyt1odvH1ZsEr5S8jNGptxGJskPHgRRNSuY:0',
                            node: {
                                hash: 'onvmb4RUskLnMsP3Qyt1odvH1ZsEr5S8jNGptxGJskPHgRRNSuY',
                                batch_position: 0,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                kind: 'transaction',
                                timestamp: '2020-06-20T00:53:07Z',
                                level: 1004387,
                                block: 'BM9KZPbwaUKHKMHfT7rcUafbWvQfxfEypHrDuC8qMTPz2893Qkn',
                                gas: 1283,
                                counter: 'd2d0010000000000',
                                gas_limit: '4328000000000000',
                                storage_limit: '',
                                amount: '100000',
                                parameters: '{ "prim": "Unit" }',
                                entrypoint: 'default',
                                id: 24156005,
                                destination: 'tz1YPCK2iRxWZYHxeUtKBKRosdQ1SAHKeACe',
                            },
                        },
                        {
                            cursor: 'opLgb1F3FavKLT2GA3y8PV2ZTwqvJ6zjrdWSdvEVVguarGpCSQ9:0',
                            node: {
                                hash: 'opLgb1F3FavKLT2GA3y8PV2ZTwqvJ6zjrdWSdvEVVguarGpCSQ9',
                                batch_position: 0,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                kind: 'transaction',
                                timestamp: '2020-06-20T00:55:07Z',
                                level: 1004389,
                                block: 'BLwyWkw9v38oidgRYLL9vkj8s4Wfk2tNrCQzCzNHaLES1F1r3UH',
                                gas: 1285,
                                counter: 'd3d0010000000000',
                                gas_limit: '4328000000000000',
                                storage_limit: '',
                                amount: '20000000000',
                                parameters: '{ "prim": "Unit" }',
                                entrypoint: 'default',
                                id: 24156058,
                                destination: 'tz1YPCK2iRxWZYHxeUtKBKRosdQ1SAHKeACe',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.transactions first and date_range lte and order_by and before', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS") {
                address
                first_seen
                activated
                transactions(
                  first: 3
                  date_range: {
                    lte: "2020-03-14T03:09:17Z"
                  }
                  order_by: {field: ID, direction: ASC}
                  before: "oomuawT2jcbrRCmnXBCpBsT88wCXah5fihxmiQh5iKq7Kwxr4bC:0"
                ) {
                page_info {
                    start_cursor
                    end_cursor
                    has_next_page
                    has_previous_page
                }
                edges {
                        cursor
                        node {
                            hash
                            batch_position
                            source
                            kind
                            timestamp
                            level
                            block
                            gas
                            counter
                            gas_limit
                            storage_limit
                            amount
                            parameters
                            entrypoint
                            id
                            destination
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                first_seen: '2018-09-13T14:51:20Z',
                activated: '2018-09-13T14:49:20.000Z',
                transactions: {
                    page_info: {
                        start_cursor: 'opQqKKXhzznFRxMPdiLyP4V1KWfuDrrNSkqF8jQ6v4JENR3K6jP:0',
                        end_cursor: 'ooe9ypjsaQZPhowQpj69MZicQ3ZUwFGxQJyMVxdGpMRWF9SBZWr:0',
                        has_next_page: true,
                        has_previous_page: false,
                    },
                    edges: [
                        {
                            cursor: 'opQqKKXhzznFRxMPdiLyP4V1KWfuDrrNSkqF8jQ6v4JENR3K6jP:0',
                            node: {
                                hash: 'opQqKKXhzznFRxMPdiLyP4V1KWfuDrrNSkqF8jQ6v4JENR3K6jP',
                                batch_position: 0,
                                source: 'tz1LBEKXaxQbd5Gtzbc1ATCwc3pppu81aWGc',
                                kind: 'transaction',
                                timestamp: '2018-09-13T14:51:20Z',
                                level: 101957,
                                block: 'BMQzE1UWod12P5XkE15fS4CNRvKLqmWknwCEDuTP1FKEhzrVjtU',
                                gas: 10,
                                counter: '62e7000000000000',
                                gas_limit: 'c800000000000000',
                                storage_limit: '',
                                amount: '14',
                                parameters: null,
                                entrypoint: null,
                                id: 1425714,
                                destination: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                            },
                        },
                        {
                            cursor: 'oop8aPtDYg7K3xqqMadPKoXzxTLNpBukasrKtBQs9bsjXQ1UWVt:1',
                            node: {
                                hash: 'oop8aPtDYg7K3xqqMadPKoXzxTLNpBukasrKtBQs9bsjXQ1UWVt',
                                batch_position: 1,
                                source: 'tz1PECYmqmuba13rnNxyoBkvH2ChUt8BuJuG',
                                kind: 'transaction',
                                timestamp: '2018-09-13T16:12:50Z',
                                level: 102031,
                                block: 'BLowVvoAyjeoMY4BRja6rEPdX37UjfdeB4JFtgQ4ZvCiRg9MNdA',
                                gas: 0,
                                counter: '9d4d000000000000',
                                gas_limit: '',
                                storage_limit: '',
                                amount: '1000000',
                                parameters: null,
                                entrypoint: null,
                                id: 1427150,
                                destination: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                            },
                        },
                        {
                            cursor: 'ooe9ypjsaQZPhowQpj69MZicQ3ZUwFGxQJyMVxdGpMRWF9SBZWr:0',
                            node: {
                                hash: 'ooe9ypjsaQZPhowQpj69MZicQ3ZUwFGxQJyMVxdGpMRWF9SBZWr',
                                batch_position: 0,
                                source: 'tz1PECYmqmuba13rnNxyoBkvH2ChUt8BuJuG',
                                kind: 'transaction',
                                timestamp: '2018-09-13T16:17:05Z',
                                level: 102034,
                                block: 'BLFRNMJkWm9aRBgTwEdhZGcgKZ3idCWYWua8bRCfhx1BBU5xMmJ',
                                gas: 0,
                                counter: '9f4d000000000000',
                                gas_limit: 'c800000000000000',
                                storage_limit: '',
                                amount: '4199800000',
                                parameters: null,
                                entrypoint: null,
                                id: 1427201,
                                destination: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.transactions first and date_range lte and order_by and after', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS") {
                address
                first_seen
                activated
                transactions(
                  first: 3
                  date_range: {
                    lte: "2020-03-14T03:09:17Z"
                  }
                  order_by: {field: ID, direction: ASC}
                  after: "oomuawT2jcbrRCmnXBCpBsT88wCXah5fihxmiQh5iKq7Kwxr4bC:0"
                ) {
                page_info {
                    start_cursor
                    end_cursor
                    has_next_page
                    has_previous_page
                }
                edges {
                        cursor
                        node {
                            hash
                            batch_position
                            source
                            kind
                            timestamp
                            level
                            block
                            gas
                            counter
                            gas_limit
                            storage_limit
                            amount
                            parameters
                            entrypoint
                            id
                            destination
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                first_seen: '2018-09-13T14:51:20Z',
                activated: '2018-09-13T14:49:20.000Z',
                transactions: {
                    page_info: {
                        start_cursor: 'opEzt5jFc3UMsYvKGwkpfgW972yAqWTgb7L1whmktFZti56hQfc:0',
                        end_cursor: 'opSF1rwXsJFJyfmagGC4fnXuXDotaWMjK9ZagZ4pqg8ZDYMMekM:0',
                        has_next_page: true,
                        has_previous_page: true,
                    },
                    edges: [
                        {
                            cursor: 'opEzt5jFc3UMsYvKGwkpfgW972yAqWTgb7L1whmktFZti56hQfc:0',
                            node: {
                                hash: 'opEzt5jFc3UMsYvKGwkpfgW972yAqWTgb7L1whmktFZti56hQfc',
                                batch_position: 0,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                kind: 'transaction',
                                timestamp: '2019-03-29T18:57:50Z',
                                level: 373398,
                                block: 'BLYhWKxV4sM9zbiWaFkKeZaqSk6wBrCiMU6xNFxtkDXdo3Dkzu7',
                                gas: 1275,
                                counter: 'b7d0010000000000',
                                gas_limit: 'd827000000000000',
                                storage_limit: '1501000000000000',
                                amount: '100000000',
                                parameters: null,
                                entrypoint: null,
                                id: 8011021,
                                destination: 'tz1btSFnzfkSAqxTm5qvUeNjZXiXvEjRz8VU',
                            },
                        },
                        {
                            cursor: 'opBUfBcorkPCmsjFQ4FzQpy7EX5iTZkPgeMbUmaxd6jaoxrQTqD:0',
                            node: {
                                hash: 'opBUfBcorkPCmsjFQ4FzQpy7EX5iTZkPgeMbUmaxd6jaoxrQTqD',
                                batch_position: 0,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                kind: 'transaction',
                                timestamp: '2019-05-03T21:41:13Z',
                                level: 422244,
                                block: 'BLJQYi6Rtj57Zjf15H4k9d4c6RvNcnjJui7FWJQCqqbzFpecMmT',
                                gas: 1275,
                                counter: 'b8d0010000000000',
                                gas_limit: 'd827000000000000',
                                storage_limit: '1501000000000000',
                                amount: '100000000',
                                parameters: null,
                                entrypoint: null,
                                id: 9207984,
                                destination: 'tz1g6XhzH48f85tL197oiSa2Ssxzs6Vzjiq1',
                            },
                        },
                        {
                            cursor: 'opSF1rwXsJFJyfmagGC4fnXuXDotaWMjK9ZagZ4pqg8ZDYMMekM:0',
                            node: {
                                hash: 'opSF1rwXsJFJyfmagGC4fnXuXDotaWMjK9ZagZ4pqg8ZDYMMekM',
                                batch_position: 0,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                kind: 'transaction',
                                timestamp: '2019-05-03T21:43:13Z',
                                level: 422246,
                                block: 'BMEASXTTyHERPjzYWhUfaL9BtBAPc5xvBq3kap9rLWQ3FCDzLQs',
                                gas: 1275,
                                counter: 'b9d0010000000000',
                                gas_limit: 'd827000000000000',
                                storage_limit: '1501000000000000',
                                amount: '100000000',
                                parameters: null,
                                entrypoint: null,
                                id: 9208042,
                                destination: 'tz1fMFYv7GwRdqXszP2DQRTMdFtQW3BBEzKX',
                            },
                        },
                    ],
                },
            },
        }));
    });
    test('account.transactions first and date_range gte/lte and order_by and before', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS") {
                address
                first_seen
                activated
                transactions(
                  first: 3
                  date_range: {
                    lte: "2020-03-14T03:09:17Z"
                    gte: "2019-06-13T16:26:03Z"
                  }
                  order_by: {field: ID, direction: ASC}
                  before: "oobMbc7MHSz5ttV3AcjiXqaxZSxs9ShGjcHmopoo85Jays2rniJ:0"
                ) {
                page_info {
                    start_cursor
                    end_cursor
                    has_next_page
                    has_previous_page
                }
                edges {
                        cursor
                        node {
                            hash
                            batch_position
                            source
                            kind
                            timestamp
                            level
                            block
                            gas
                            counter
                            gas_limit
                            storage_limit
                            amount
                            parameters
                            entrypoint
                            id
                            destination
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                first_seen: '2018-09-13T14:51:20Z',
                activated: '2018-09-13T14:49:20.000Z',
                transactions: {
                    page_info: {
                        start_cursor: 'op2Jk1AuscHtt6NWL58ETYmqenNHK6C1KSv7Rk1rPzadSTKqpL1:0',
                        end_cursor: 'opZSVPxNoaExjX6dCFnoPAh5yHk6g7BaJdce2AXUHuwW3c7ucNA:0',
                        has_next_page: true,
                        has_previous_page: false,
                    },
                    edges: [
                        {
                            cursor: 'op2Jk1AuscHtt6NWL58ETYmqenNHK6C1KSv7Rk1rPzadSTKqpL1:0',
                            node: {
                                hash: 'op2Jk1AuscHtt6NWL58ETYmqenNHK6C1KSv7Rk1rPzadSTKqpL1',
                                batch_position: 0,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                kind: 'transaction',
                                timestamp: '2019-06-13T16:26:03Z',
                                level: 478917,
                                block: 'BLBgbfhyuDD3Q2b7mAKm2S4kroYLVDp3gbkRpU7PPaZLDodi11e',
                                gas: 1284,
                                counter: 'c4d0010000000000',
                                gas_limit: '3c28000000000000',
                                storage_limit: '',
                                amount: '171235783',
                                parameters: null,
                                entrypoint: null,
                                id: 10588066,
                                destination: 'KT1C8f3CYXEwuvvvGeG9pKMLWa2Xcyci8eUn',
                            },
                        },
                        {
                            cursor: 'ooZ59b3ddyATdUsbiWwX6gco4hZ7VJeBbucCjvAbNyggrnTDKbN:0',
                            node: {
                                hash: 'ooZ59b3ddyATdUsbiWwX6gco4hZ7VJeBbucCjvAbNyggrnTDKbN',
                                batch_position: 0,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                kind: 'transaction',
                                timestamp: '2019-06-13T16:27:03Z',
                                level: 478918,
                                block: 'BLhWGpWqz19bGcLYoJxotAxm17vNGjymwMbGK5yizRRMQhkxEai',
                                gas: 1284,
                                counter: 'c5d0010000000000',
                                gas_limit: '3c28000000000000',
                                storage_limit: '',
                                amount: '166379804',
                                parameters: null,
                                entrypoint: null,
                                id: 10588092,
                                destination: 'KT1FPhB9bBH6BXH7a7DsX5eBUvJRyqcZhfyn',
                            },
                        },
                        {
                            cursor: 'opZSVPxNoaExjX6dCFnoPAh5yHk6g7BaJdce2AXUHuwW3c7ucNA:0',
                            node: {
                                hash: 'opZSVPxNoaExjX6dCFnoPAh5yHk6g7BaJdce2AXUHuwW3c7ucNA',
                                batch_position: 0,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                kind: 'transaction',
                                timestamp: '2019-06-13T16:29:03Z',
                                level: 478920,
                                block: 'BME5VpmuMaZfWK7sgrmqrH73yZnuiDhJGuE8dsURJTodB5LWDCe',
                                gas: 1284,
                                counter: 'c6d0010000000000',
                                gas_limit: '3c28000000000000',
                                storage_limit: '',
                                amount: '26594806',
                                parameters: null,
                                entrypoint: null,
                                id: 10588145,
                                destination: 'KT1Hw82JKNLeQUF58Tr5s8hk3Yn8XW3Hv81C',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.transactions first and date_range gte/lte and order_by and after', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS") {
                address
                first_seen
                activated
                transactions(
                  first: 3
                  date_range: {
                    lte: "2020-03-14T03:09:17Z"
                    gte: "2019-06-13T16:26:03Z"
                  }
                  order_by: {field: ID, direction: ASC}
                  after: "oobMbc7MHSz5ttV3AcjiXqaxZSxs9ShGjcHmopoo85Jays2rniJ:0"
                ) {
                page_info {
                    start_cursor
                    end_cursor
                    has_next_page
                    has_previous_page
                }
                edges {
                        cursor
                        node {
                            hash
                            batch_position
                            source
                            kind
                            timestamp
                            level
                            block
                            gas
                            counter
                            gas_limit
                            storage_limit
                            amount
                            parameters
                            entrypoint
                            id
                            destination
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                first_seen: '2018-09-13T14:51:20Z',
                activated: '2018-09-13T14:49:20.000Z',
                transactions: {
                    page_info: {
                        start_cursor: 'opT4s5rWRbAEcbq6GZ272LyxNCxACqdk7c6Jm4buDcR6Fh4Zs3c:0',
                        end_cursor: 'oodK29zqUFb7NyMa5mRyb6DVR71FyWHh5gXF8WVgCDG6uGVuCaT:0',
                        has_next_page: true,
                        has_previous_page: true,
                    },
                    edges: [
                        {
                            cursor: 'opT4s5rWRbAEcbq6GZ272LyxNCxACqdk7c6Jm4buDcR6Fh4Zs3c:0',
                            node: {
                                hash: 'opT4s5rWRbAEcbq6GZ272LyxNCxACqdk7c6Jm4buDcR6Fh4Zs3c',
                                batch_position: 0,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                kind: 'transaction',
                                timestamp: '2019-06-23T00:44:47Z',
                                level: 491770,
                                block: 'BLyWtfcsLhVGAbsYJPSFpUQiPwdHkHExBrgB3WD9wesuo3PLfUV',
                                gas: 1564,
                                counter: 'c8d0010000000000',
                                gas_limit: '4533000000000000',
                                storage_limit: '',
                                amount: '0',
                                parameters: null,
                                entrypoint: null,
                                id: 10910203,
                                destination: 'KT1BRudFZEXLYANgmZTka1xCDN5nWTMWY7SZ',
                            },
                        },
                        {
                            cursor: 'ooADLykQuRbk3yUc5f465SRJhGyDtJDFit48WpCp9tipnDK399X:0',
                            node: {
                                hash: 'ooADLykQuRbk3yUc5f465SRJhGyDtJDFit48WpCp9tipnDK399X',
                                batch_position: 0,
                                source: 'tz1KtGwriE7VuLwT3LwuvU9Nv4wAxP7XZ57d',
                                kind: 'transaction',
                                timestamp: '2019-10-24T20:38:46Z',
                                level: 664348,
                                block: 'BM8uvwRPXp7QtwUwcr2FtSsooUJcRbnN7hvDhUochXS8dpcXfQ8',
                                gas: 17920,
                                counter: 'e2fe010000000000',
                                gas_limit: 'fa58020000000000',
                                storage_limit: '2c01000000000000',
                                amount: '10009000000',
                                parameters: '{ "prim": "Unit" }',
                                entrypoint: 'default',
                                id: 15227316,
                                destination: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                            },
                        },
                        {
                            cursor: 'oodK29zqUFb7NyMa5mRyb6DVR71FyWHh5gXF8WVgCDG6uGVuCaT:0',
                            node: {
                                hash: 'oodK29zqUFb7NyMa5mRyb6DVR71FyWHh5gXF8WVgCDG6uGVuCaT',
                                batch_position: 0,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                kind: 'transaction',
                                timestamp: '2020-02-22T22:29:26Z',
                                level: 836064,
                                block: 'BKspGC8xE7CT8431tR5CErsn5dEVUB6eVtXLeCSpv4285z3rZa4',
                                gas: 1284,
                                counter: 'c9d0010000000000',
                                gas_limit: '4328000000000000',
                                storage_limit: '1501000000000000',
                                amount: '100000',
                                parameters: '{ "prim": "Unit" }',
                                entrypoint: 'default',
                                id: 19731302,
                                destination: 'tz1VPz5qhzTrRTNevHC33ArBTBPV6jHpwjQB',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.transactions last', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS") {
                address
                first_seen
                activated
                transactions(
                  last: 3
                ) {
                page_info {
                    start_cursor
                    end_cursor
                    has_next_page
                    has_previous_page
                }
                edges {
                        cursor
                        node {
                            hash
                            batch_position
                            source
                            kind
                            timestamp
                            level
                            block
                            gas
                            counter
                            gas_limit
                            storage_limit
                            amount
                            parameters
                            entrypoint
                            id
                            destination
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                first_seen: '2018-09-13T14:51:20Z',
                activated: '2018-09-13T14:49:20.000Z',
                transactions: {
                    page_info: {
                        start_cursor: 'ooe9ypjsaQZPhowQpj69MZicQ3ZUwFGxQJyMVxdGpMRWF9SBZWr:0',
                        end_cursor: 'opQqKKXhzznFRxMPdiLyP4V1KWfuDrrNSkqF8jQ6v4JENR3K6jP:0',
                        has_next_page: false,
                        has_previous_page: true,
                    },
                    edges: [
                        {
                            cursor: 'ooe9ypjsaQZPhowQpj69MZicQ3ZUwFGxQJyMVxdGpMRWF9SBZWr:0',
                            node: {
                                hash: 'ooe9ypjsaQZPhowQpj69MZicQ3ZUwFGxQJyMVxdGpMRWF9SBZWr',
                                batch_position: 0,
                                source: 'tz1PECYmqmuba13rnNxyoBkvH2ChUt8BuJuG',
                                kind: 'transaction',
                                timestamp: '2018-09-13T16:17:05Z',
                                level: 102034,
                                block: 'BLFRNMJkWm9aRBgTwEdhZGcgKZ3idCWYWua8bRCfhx1BBU5xMmJ',
                                gas: 0,
                                counter: '9f4d000000000000',
                                gas_limit: 'c800000000000000',
                                storage_limit: '',
                                amount: '4199800000',
                                parameters: null,
                                entrypoint: null,
                                id: 1427201,
                                destination: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                            },
                        },
                        {
                            cursor: 'oop8aPtDYg7K3xqqMadPKoXzxTLNpBukasrKtBQs9bsjXQ1UWVt:1',
                            node: {
                                hash: 'oop8aPtDYg7K3xqqMadPKoXzxTLNpBukasrKtBQs9bsjXQ1UWVt',
                                batch_position: 1,
                                source: 'tz1PECYmqmuba13rnNxyoBkvH2ChUt8BuJuG',
                                kind: 'transaction',
                                timestamp: '2018-09-13T16:12:50Z',
                                level: 102031,
                                block: 'BLowVvoAyjeoMY4BRja6rEPdX37UjfdeB4JFtgQ4ZvCiRg9MNdA',
                                gas: 0,
                                counter: '9d4d000000000000',
                                gas_limit: '',
                                storage_limit: '',
                                amount: '1000000',
                                parameters: null,
                                entrypoint: null,
                                id: 1427150,
                                destination: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                            },
                        },
                        {
                            cursor: 'opQqKKXhzznFRxMPdiLyP4V1KWfuDrrNSkqF8jQ6v4JENR3K6jP:0',
                            node: {
                                hash: 'opQqKKXhzznFRxMPdiLyP4V1KWfuDrrNSkqF8jQ6v4JENR3K6jP',
                                batch_position: 0,
                                source: 'tz1LBEKXaxQbd5Gtzbc1ATCwc3pppu81aWGc',
                                kind: 'transaction',
                                timestamp: '2018-09-13T14:51:20Z',
                                level: 101957,
                                block: 'BMQzE1UWod12P5XkE15fS4CNRvKLqmWknwCEDuTP1FKEhzrVjtU',
                                gas: 10,
                                counter: '62e7000000000000',
                                gas_limit: 'c800000000000000',
                                storage_limit: '',
                                amount: '14',
                                parameters: null,
                                entrypoint: null,
                                id: 1425714,
                                destination: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.transactions last and before', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS") {
                address
                first_seen
                activated
                transactions(
                  last: 3
                  before: "ooe9ypjsaQZPhowQpj69MZicQ3ZUwFGxQJyMVxdGpMRWF9SBZWr:0"
                ) {
                page_info {
                    start_cursor
                    end_cursor
                    has_next_page
                    has_previous_page
                }
                edges {
                        cursor
                        node {
                            hash
                            batch_position
                            source
                            kind
                            timestamp
                            level
                            block
                            gas
                            counter
                            gas_limit
                            storage_limit
                            amount
                            parameters
                            entrypoint
                            id
                            destination
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                first_seen: '2018-09-13T14:51:20Z',
                activated: '2018-09-13T14:49:20.000Z',
                transactions: {
                    page_info: {
                        start_cursor: 'ooXu4igMeKVc2CT7CYUa62WxJRcpXJosLM2FF1vBXmBT61FR19K:0',
                        end_cursor: 'ooeHBn4BDojRRLsNc4eWta1JrHoxGEEU8p2EjufpUcRNMcKJyNb:0',
                        has_next_page: true,
                        has_previous_page: true,
                    },
                    edges: [
                        {
                            cursor: 'ooXu4igMeKVc2CT7CYUa62WxJRcpXJosLM2FF1vBXmBT61FR19K:0',
                            node: {
                                hash: 'ooXu4igMeKVc2CT7CYUa62WxJRcpXJosLM2FF1vBXmBT61FR19K',
                                batch_position: 0,
                                source: 'tz1KtGwriE7VuLwT3LwuvU9Nv4wAxP7XZ57d',
                                kind: 'transaction',
                                timestamp: '2018-10-05T00:32:44Z',
                                level: 131767,
                                block: 'BMWGakwzmTPyXT5ncfT9HfF154AJhXi8z1o1UuGeFaMJV1o93nK',
                                gas: 0,
                                counter: '7de5010000000000',
                                gas_limit: 'c800000000000000',
                                storage_limit: '',
                                amount: '5543250000',
                                parameters: null,
                                entrypoint: null,
                                id: 2050623,
                                destination: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                            },
                        },
                        {
                            cursor: 'op4hQbsXN35JghDjC9pM3wUFQW8roEmwcncBY5KoW2hF3CQbPGH:0',
                            node: {
                                hash: 'op4hQbsXN35JghDjC9pM3wUFQW8roEmwcncBY5KoW2hF3CQbPGH',
                                batch_position: 0,
                                source: 'tz1PECYmqmuba13rnNxyoBkvH2ChUt8BuJuG',
                                kind: 'transaction',
                                timestamp: '2018-09-13T16:23:20Z',
                                level: 102039,
                                block: 'BM1CtHUump3aahzSMVHh6HVBEvmWramVCpuTm7tFFnPGVhS6xr5',
                                gas: 0,
                                counter: 'a14d000000000000',
                                gas_limit: 'c800000000000000',
                                storage_limit: '',
                                amount: '3000',
                                parameters: null,
                                entrypoint: null,
                                id: 1427300,
                                destination: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                            },
                        },
                        {
                            cursor: 'ooeHBn4BDojRRLsNc4eWta1JrHoxGEEU8p2EjufpUcRNMcKJyNb:0',
                            node: {
                                hash: 'ooeHBn4BDojRRLsNc4eWta1JrHoxGEEU8p2EjufpUcRNMcKJyNb',
                                batch_position: 0,
                                source: 'tz1PECYmqmuba13rnNxyoBkvH2ChUt8BuJuG',
                                kind: 'transaction',
                                timestamp: '2018-09-13T16:18:05Z',
                                level: 102035,
                                block: 'BLJLwRhrzz4srdUXw3FHgPazUnnMc3B1R15wpSWsBJPAYRJQCng',
                                gas: 0,
                                counter: 'a04d000000000000',
                                gas_limit: 'c800000000000000',
                                storage_limit: '',
                                amount: '20000',
                                parameters: null,
                                entrypoint: null,
                                id: 1427217,
                                destination: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.transactions last and after', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS") {
                address
                first_seen
                activated
                transactions(
                  last: 3
                  after: "ooXu4igMeKVc2CT7CYUa62WxJRcpXJosLM2FF1vBXmBT61FR19K:0"
                ) {
                page_info {
                    start_cursor
                    end_cursor
                    has_next_page
                    has_previous_page
                }
                edges {
                        cursor
                        node {
                            hash
                            batch_position
                            source
                            kind
                            timestamp
                            level
                            block
                            gas
                            counter
                            gas_limit
                            storage_limit
                            amount
                            parameters
                            entrypoint
                            id
                            destination
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                first_seen: '2018-09-13T14:51:20Z',
                activated: '2018-09-13T14:49:20.000Z',
                transactions: {
                    page_info: {
                        start_cursor: 'ooe9ypjsaQZPhowQpj69MZicQ3ZUwFGxQJyMVxdGpMRWF9SBZWr:0',
                        end_cursor: 'opQqKKXhzznFRxMPdiLyP4V1KWfuDrrNSkqF8jQ6v4JENR3K6jP:0',
                        has_next_page: false,
                        has_previous_page: true,
                    },
                    edges: [
                        {
                            cursor: 'ooe9ypjsaQZPhowQpj69MZicQ3ZUwFGxQJyMVxdGpMRWF9SBZWr:0',
                            node: {
                                hash: 'ooe9ypjsaQZPhowQpj69MZicQ3ZUwFGxQJyMVxdGpMRWF9SBZWr',
                                batch_position: 0,
                                source: 'tz1PECYmqmuba13rnNxyoBkvH2ChUt8BuJuG',
                                kind: 'transaction',
                                timestamp: '2018-09-13T16:17:05Z',
                                level: 102034,
                                block: 'BLFRNMJkWm9aRBgTwEdhZGcgKZ3idCWYWua8bRCfhx1BBU5xMmJ',
                                gas: 0,
                                counter: '9f4d000000000000',
                                gas_limit: 'c800000000000000',
                                storage_limit: '',
                                amount: '4199800000',
                                parameters: null,
                                entrypoint: null,
                                id: 1427201,
                                destination: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                            },
                        },
                        {
                            cursor: 'oop8aPtDYg7K3xqqMadPKoXzxTLNpBukasrKtBQs9bsjXQ1UWVt:1',
                            node: {
                                hash: 'oop8aPtDYg7K3xqqMadPKoXzxTLNpBukasrKtBQs9bsjXQ1UWVt',
                                batch_position: 1,
                                source: 'tz1PECYmqmuba13rnNxyoBkvH2ChUt8BuJuG',
                                kind: 'transaction',
                                timestamp: '2018-09-13T16:12:50Z',
                                level: 102031,
                                block: 'BLowVvoAyjeoMY4BRja6rEPdX37UjfdeB4JFtgQ4ZvCiRg9MNdA',
                                gas: 0,
                                counter: '9d4d000000000000',
                                gas_limit: '',
                                storage_limit: '',
                                amount: '1000000',
                                parameters: null,
                                entrypoint: null,
                                id: 1427150,
                                destination: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                            },
                        },
                        {
                            cursor: 'opQqKKXhzznFRxMPdiLyP4V1KWfuDrrNSkqF8jQ6v4JENR3K6jP:0',
                            node: {
                                hash: 'opQqKKXhzznFRxMPdiLyP4V1KWfuDrrNSkqF8jQ6v4JENR3K6jP',
                                batch_position: 0,
                                source: 'tz1LBEKXaxQbd5Gtzbc1ATCwc3pppu81aWGc',
                                kind: 'transaction',
                                timestamp: '2018-09-13T14:51:20Z',
                                level: 101957,
                                block: 'BMQzE1UWod12P5XkE15fS4CNRvKLqmWknwCEDuTP1FKEhzrVjtU',
                                gas: 10,
                                counter: '62e7000000000000',
                                gas_limit: 'c800000000000000',
                                storage_limit: '',
                                amount: '14',
                                parameters: null,
                                entrypoint: null,
                                id: 1425714,
                                destination: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.transactions last and order_by', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS") {
                address
                first_seen
                activated
                transactions(
                  last: 3
                  order_by: {field: ID, direction: ASC}
                ) {
                page_info {
                    start_cursor
                    end_cursor
                    has_next_page
                    has_previous_page
                }
                edges {
                        cursor
                        node {
                            hash
                            batch_position
                            source
                            kind
                            timestamp
                            level
                            block
                            gas
                            counter
                            gas_limit
                            storage_limit
                            amount
                            parameters
                            entrypoint
                            id
                            destination
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                first_seen: '2018-09-13T14:51:20Z',
                activated: '2018-09-13T14:49:20.000Z',
                transactions: {
                    page_info: {
                        start_cursor: 'ootgGUV6dDRfBBF9wutPQZWzeetqiAUkwrkwSL1Qy6NrTNUs6Jw:0',
                        end_cursor: 'opNxsLZeNMttsyh4PxLk35dmRsJyLXQwJjpVp89ip7pU2j5yzyR:0',
                        has_next_page: false,
                        has_previous_page: true,
                    },
                    edges: [
                        {
                            cursor: 'ootgGUV6dDRfBBF9wutPQZWzeetqiAUkwrkwSL1Qy6NrTNUs6Jw:0',
                            node: {
                                hash: 'ootgGUV6dDRfBBF9wutPQZWzeetqiAUkwrkwSL1Qy6NrTNUs6Jw',
                                batch_position: 0,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                kind: 'transaction',
                                timestamp: '2020-07-15T20:58:27Z',
                                level: 1041413,
                                block: 'BMf1K4SE3WqRJ6eD345kU74pjLkfB7ABK5u5KwZrxckNHFvUr4v',
                                gas: 1285,
                                counter: 'e1d0010000000000',
                                gas_limit: '4328000000000000',
                                storage_limit: '1501000000000000',
                                amount: '14193181',
                                parameters: '{ "prim": "Unit" }',
                                entrypoint: 'default',
                                id: 25134264,
                                destination: 'tz1cb9DMkq9ZdrDVxi3HwyLEMtbDseeZPZEa',
                            },
                        },
                        {
                            cursor: 'opEHTGa4n4efFQfpaWFNqoY4dbzXrBEo7Xo8v74AX5pEREqK28S:0',
                            node: {
                                hash: 'opEHTGa4n4efFQfpaWFNqoY4dbzXrBEo7Xo8v74AX5pEREqK28S',
                                batch_position: 0,
                                source: 'tz1YPCK2iRxWZYHxeUtKBKRosdQ1SAHKeACe',
                                kind: 'transaction',
                                timestamp: '2020-07-17T19:46:24Z',
                                level: 1044207,
                                block: 'BMYhcRrNnHTthRueAKoiyBxyuzJWEMqMgkxzqiVFXHtS84ALKua',
                                gas: 1420,
                                counter: '3ee8200000000000',
                                gas_limit: '6829000000000000',
                                storage_limit: '2c01000000000000',
                                amount: '192000000',
                                parameters: '{ "prim": "Unit" }',
                                entrypoint: 'default',
                                id: 25210462,
                                destination: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                            },
                        },
                        {
                            cursor: 'opNxsLZeNMttsyh4PxLk35dmRsJyLXQwJjpVp89ip7pU2j5yzyR:0',
                            node: {
                                hash: 'opNxsLZeNMttsyh4PxLk35dmRsJyLXQwJjpVp89ip7pU2j5yzyR',
                                batch_position: 0,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                kind: 'transaction',
                                timestamp: '2020-08-15T22:11:48Z',
                                level: 1085872,
                                block: 'BLEwgk9RsqEfhtm6u3PrLXcu7pXQ6dWMysJRVTAhNRqVqhCsF9g',
                                gas: 1285,
                                counter: 'e2d0010000000000',
                                gas_limit: '4328000000000000',
                                storage_limit: '1501000000000000',
                                amount: '50000000',
                                parameters: '{ "prim": "Unit" }',
                                entrypoint: 'default',
                                id: 26331765,
                                destination: 'tz1UovZ16T6vTuCRQhWKtE7jqeC6QaTzbc3P',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.transactions last and order_by and before', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS") {
                address
                first_seen
                activated
                transactions(
                  last: 3
                  order_by: {field: ID, direction: ASC}
                  before: "ootgGUV6dDRfBBF9wutPQZWzeetqiAUkwrkwSL1Qy6NrTNUs6Jw:0"
                ) {
                page_info {
                    start_cursor
                    end_cursor
                    has_next_page
                    has_previous_page
                }
                edges {
                        cursor
                        node {
                            hash
                            batch_position
                            source
                            kind
                            timestamp
                            level
                            block
                            gas
                            counter
                            gas_limit
                            storage_limit
                            amount
                            parameters
                            entrypoint
                            id
                            destination
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                first_seen: '2018-09-13T14:51:20Z',
                activated: '2018-09-13T14:49:20.000Z',
                transactions: {
                    page_info: {
                        start_cursor: 'ooa7UPV5iL5TeV469j9BqeXxFr7tTviyxRRizVFWoc3rU1eAH8N:0',
                        end_cursor: 'ongxtfJMZ6tdbwxUKE46463gk6rVuWHk6WNWHcSgN1Q6obmm6q1:0',
                        has_next_page: true,
                        has_previous_page: true,
                    },
                    edges: [
                        {
                            cursor: 'ooa7UPV5iL5TeV469j9BqeXxFr7tTviyxRRizVFWoc3rU1eAH8N:0',
                            node: {
                                hash: 'ooa7UPV5iL5TeV469j9BqeXxFr7tTviyxRRizVFWoc3rU1eAH8N',
                                batch_position: 0,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                kind: 'transaction',
                                timestamp: '2020-07-15T20:55:27Z',
                                level: 1041410,
                                block: 'BMUm98MH7pD2eC9s5WHiPvsSjMFGPyQiJB7zHLQY9uX3ozGpQFE',
                                gas: 1285,
                                counter: 'ded0010000000000',
                                gas_limit: '4328000000000000',
                                storage_limit: '',
                                amount: '325873000',
                                parameters: '{ "prim": "Unit" }',
                                entrypoint: 'default',
                                id: 25134171,
                                destination: 'tz1gEQe76hTA9L5MpTRtNnDhhqQ1YbzVPxnY',
                            },
                        },
                        {
                            cursor: 'oowUdQhpUKrsM9UgPBxPXQHq5ri9BVZGsEWXvFk2w48GkZZXxno:0',
                            node: {
                                hash: 'oowUdQhpUKrsM9UgPBxPXQHq5ri9BVZGsEWXvFk2w48GkZZXxno',
                                batch_position: 0,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                kind: 'transaction',
                                timestamp: '2020-07-15T20:56:27Z',
                                level: 1041411,
                                block: 'BL25QMmdBxgkic7PB2z53B38rPTMYy3kAfPevQXDDsBrzbbRy6j',
                                gas: 1284,
                                counter: 'dfd0010000000000',
                                gas_limit: '4328000000000000',
                                storage_limit: '',
                                amount: '79651872',
                                parameters: '{ "prim": "Unit" }',
                                entrypoint: 'default',
                                id: 25134203,
                                destination: 'tz1bJAQfucuxvLwCHyPXvG24PjtLT1V3ewbt',
                            },
                        },
                        {
                            cursor: 'ongxtfJMZ6tdbwxUKE46463gk6rVuWHk6WNWHcSgN1Q6obmm6q1:0',
                            node: {
                                hash: 'ongxtfJMZ6tdbwxUKE46463gk6rVuWHk6WNWHcSgN1Q6obmm6q1',
                                batch_position: 0,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                kind: 'transaction',
                                timestamp: '2020-07-15T20:57:27Z',
                                level: 1041412,
                                block: 'BLFSnPLffMKuRvDR6MQErKxLitKevszN2UkwnJTNUQiGLQ6d7cM',
                                gas: 1284,
                                counter: 'e0d0010000000000',
                                gas_limit: '4328000000000000',
                                storage_limit: '',
                                amount: '188397246',
                                parameters: '{ "prim": "Unit" }',
                                entrypoint: 'default',
                                id: 25134234,
                                destination: 'tz1Vg5uaxedKk4HYn4ZMWWZ7B59GYXSm2iyj',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.transactions last and order_by and after', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS") {
                address
                first_seen
                activated
                transactions(
                  last: 3
                  order_by: {field: ID, direction: ASC}
                  after: "ootgGUV6dDRfBBF9wutPQZWzeetqiAUkwrkwSL1Qy6NrTNUs6Jw:0"
                ) {
                page_info {
                    start_cursor
                    end_cursor
                    has_next_page
                    has_previous_page
                }
                edges {
                        cursor
                        node {
                            hash
                            batch_position
                            source
                            kind
                            timestamp
                            level
                            block
                            gas
                            counter
                            gas_limit
                            storage_limit
                            amount
                            parameters
                            entrypoint
                            id
                            destination
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                first_seen: '2018-09-13T14:51:20Z',
                activated: '2018-09-13T14:49:20.000Z',
                transactions: {
                    page_info: {
                        start_cursor: 'opEHTGa4n4efFQfpaWFNqoY4dbzXrBEo7Xo8v74AX5pEREqK28S:0',
                        end_cursor: 'opNxsLZeNMttsyh4PxLk35dmRsJyLXQwJjpVp89ip7pU2j5yzyR:0',
                        has_next_page: false,
                        has_previous_page: true,
                    },
                    edges: [
                        {
                            cursor: 'opEHTGa4n4efFQfpaWFNqoY4dbzXrBEo7Xo8v74AX5pEREqK28S:0',
                            node: {
                                hash: 'opEHTGa4n4efFQfpaWFNqoY4dbzXrBEo7Xo8v74AX5pEREqK28S',
                                batch_position: 0,
                                source: 'tz1YPCK2iRxWZYHxeUtKBKRosdQ1SAHKeACe',
                                kind: 'transaction',
                                timestamp: '2020-07-17T19:46:24Z',
                                level: 1044207,
                                block: 'BMYhcRrNnHTthRueAKoiyBxyuzJWEMqMgkxzqiVFXHtS84ALKua',
                                gas: 1420,
                                counter: '3ee8200000000000',
                                gas_limit: '6829000000000000',
                                storage_limit: '2c01000000000000',
                                amount: '192000000',
                                parameters: '{ "prim": "Unit" }',
                                entrypoint: 'default',
                                id: 25210462,
                                destination: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                            },
                        },
                        {
                            cursor: 'opNxsLZeNMttsyh4PxLk35dmRsJyLXQwJjpVp89ip7pU2j5yzyR:0',
                            node: {
                                hash: 'opNxsLZeNMttsyh4PxLk35dmRsJyLXQwJjpVp89ip7pU2j5yzyR',
                                batch_position: 0,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                kind: 'transaction',
                                timestamp: '2020-08-15T22:11:48Z',
                                level: 1085872,
                                block: 'BLEwgk9RsqEfhtm6u3PrLXcu7pXQ6dWMysJRVTAhNRqVqhCsF9g',
                                gas: 1285,
                                counter: 'e2d0010000000000',
                                gas_limit: '4328000000000000',
                                storage_limit: '1501000000000000',
                                amount: '50000000',
                                parameters: '{ "prim": "Unit" }',
                                entrypoint: 'default',
                                id: 26331765,
                                destination: 'tz1UovZ16T6vTuCRQhWKtE7jqeC6QaTzbc3P',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.transactions last and date_range gte', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS") {
                address
                first_seen
                activated
                transactions(
                  last: 3
                  date_range: {
                      gte: "2020-02-27T17:55:56Z"
                  }
                ) {
                page_info {
                    start_cursor
                    end_cursor
                    has_next_page
                    has_previous_page
                }
                edges {
                        cursor
                        node {
                            hash
                            batch_position
                            source
                            kind
                            timestamp
                            level
                            block
                            gas
                            counter
                            gas_limit
                            storage_limit
                            amount
                            parameters
                            entrypoint
                            id
                            destination
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                first_seen: '2018-09-13T14:51:20Z',
                activated: '2018-09-13T14:49:20.000Z',
                transactions: {
                    page_info: {
                        start_cursor: 'ooPvZ6ZtCdaJzzWgtD2xoLaxHQVQCZchv32sMtvp2bJeiwi5zip:0',
                        end_cursor: 'ooZM6ErfewYXeUJGN4as8qiHPDT6u465tTuJKF6eQktXDkgtZKW:0',
                        has_next_page: false,
                        has_previous_page: true,
                    },
                    edges: [
                        {
                            cursor: 'ooPvZ6ZtCdaJzzWgtD2xoLaxHQVQCZchv32sMtvp2bJeiwi5zip:0',
                            node: {
                                hash: 'ooPvZ6ZtCdaJzzWgtD2xoLaxHQVQCZchv32sMtvp2bJeiwi5zip',
                                batch_position: 0,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                kind: 'transaction',
                                timestamp: '2020-02-27T18:39:41Z',
                                level: 842902,
                                block: 'BLwfS29daVF4iXwJu8eM8Hr1ox8w1BRJaf3jJPdTHAG16VvxkLj',
                                gas: 1284,
                                counter: 'cdd0010000000000',
                                gas_limit: '4328000000000000',
                                storage_limit: '',
                                amount: '10000000',
                                parameters: '{ "prim": "Unit" }',
                                entrypoint: 'default',
                                id: 19921513,
                                destination: 'tz1QkeWxQgkHbmyjJuNKnTJBCNtUxtHtvbJU',
                            },
                        },
                        {
                            cursor: 'oo1TgCtTmss6LFW2WCuaM1RSDPLKhz2UQz6Ym9XTjiSiw4Dm9gk:0',
                            node: {
                                hash: 'oo1TgCtTmss6LFW2WCuaM1RSDPLKhz2UQz6Ym9XTjiSiw4Dm9gk',
                                batch_position: 0,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                kind: 'transaction',
                                timestamp: '2020-02-27T18:07:36Z',
                                level: 842872,
                                block: 'BLvCnxfJMnVAjpghwQagu27qAbzmTqATrartQ1BzmBQ8Nv4WULt',
                                gas: 1283,
                                counter: 'ccd0010000000000',
                                gas_limit: '4328000000000000',
                                storage_limit: '',
                                amount: '1000000',
                                parameters: '{ "prim": "Unit" }',
                                entrypoint: 'default',
                                id: 19920688,
                                destination: 'tz1QkeWxQgkHbmyjJuNKnTJBCNtUxtHtvbJU',
                            },
                        },
                        {
                            cursor: 'ooZM6ErfewYXeUJGN4as8qiHPDT6u465tTuJKF6eQktXDkgtZKW:0',
                            node: {
                                hash: 'ooZM6ErfewYXeUJGN4as8qiHPDT6u465tTuJKF6eQktXDkgtZKW',
                                batch_position: 0,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                kind: 'transaction',
                                timestamp: '2020-02-27T17:55:56Z',
                                level: 842861,
                                block: 'BL7YnpqVo7tZEw3hhSVLYNf7RHLMNTRSopuu9CHmQuALvRbwVRP',
                                gas: 1283,
                                counter: 'cbd0010000000000',
                                gas_limit: '4328000000000000',
                                storage_limit: '1501000000000000',
                                amount: '10000',
                                parameters: '{ "prim": "Unit" }',
                                entrypoint: 'default',
                                id: 19920382,
                                destination: 'tz1QkeWxQgkHbmyjJuNKnTJBCNtUxtHtvbJU',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.transactions last and date_range gte and before', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS") {
                address
                first_seen
                activated
                transactions(
                  last: 3
                  date_range: {
                      gte: "2020-02-27T17:55:56Z"
                  }
                  before: "oowsKfXqnNGCebjHJMUooAGynGrV5h5EPnY8Ck6NaNEMvdVf4bN:0"
                ) {
                page_info {
                    start_cursor
                    end_cursor
                    has_next_page
                    has_previous_page
                }
                edges {
                        cursor
                        node {
                            hash
                            batch_position
                            source
                            kind
                            timestamp
                            level
                            block
                            gas
                            counter
                            gas_limit
                            storage_limit
                            amount
                            parameters
                            entrypoint
                            id
                            destination
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                first_seen: '2018-09-13T14:51:20Z',
                activated: '2018-09-13T14:49:20.000Z',
                transactions: {
                    page_info: {
                        start_cursor: 'oneN9h5T5KahttNrycakF98MmtwJgHwTup5X32ZjpsCpHgPx1vk:0',
                        end_cursor: 'onvTurPZ3KJYZfvbiiBnMW9tY13Bfymfrt6dSUbis5wuRNRHZEQ:0',
                        has_next_page: true,
                        has_previous_page: true,
                    },
                    edges: [
                        {
                            cursor: 'oneN9h5T5KahttNrycakF98MmtwJgHwTup5X32ZjpsCpHgPx1vk:0',
                            node: {
                                hash: 'oneN9h5T5KahttNrycakF98MmtwJgHwTup5X32ZjpsCpHgPx1vk',
                                batch_position: 0,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                kind: 'transaction',
                                timestamp: '2020-04-07T18:34:56Z',
                                level: 899351,
                                block: 'BLijJ6KuiTEbFMf9HnEmbxrJFLYLYBtQ1LAtdKm9T2qA92bJUKz',
                                gas: 1284,
                                counter: 'd1d0010000000000',
                                gas_limit: '4328000000000000',
                                storage_limit: '1501000000000000',
                                amount: '2000000',
                                parameters: '{ "prim": "Unit" }',
                                entrypoint: 'default',
                                id: 21399531,
                                destination: 'tz1QwEDvWNowgs2CbzWD4jW99Cacz3FVMMgF',
                            },
                        },
                        {
                            cursor: 'opNutoZNVCcEsKMYcemrxss38w2ezYjx9UwEDv3cLFAc5HLGSBB:0',
                            node: {
                                hash: 'opNutoZNVCcEsKMYcemrxss38w2ezYjx9UwEDv3cLFAc5HLGSBB',
                                batch_position: 0,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                kind: 'transaction',
                                timestamp: '2020-03-14T03:30:37Z',
                                level: 864405,
                                block: 'BLPUnxUKvUf4bp5BJHLbmuGfnfjTAN7VxnzbfWQ4HVgPvFCftVV',
                                gas: 1284,
                                counter: 'd0d0010000000000',
                                gas_limit: '4328000000000000',
                                storage_limit: '',
                                amount: '199900000',
                                parameters: '{ "prim": "Unit" }',
                                entrypoint: 'default',
                                id: 20491513,
                                destination: 'tz1VPz5qhzTrRTNevHC33ArBTBPV6jHpwjQB',
                            },
                        },
                        {
                            cursor: 'onvTurPZ3KJYZfvbiiBnMW9tY13Bfymfrt6dSUbis5wuRNRHZEQ:0',
                            node: {
                                hash: 'onvTurPZ3KJYZfvbiiBnMW9tY13Bfymfrt6dSUbis5wuRNRHZEQ',
                                batch_position: 0,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                kind: 'transaction',
                                timestamp: '2020-03-14T03:15:17Z',
                                level: 864391,
                                block: 'BMdTg2Y1mkP6wz2yiXSUvJpDocZgm1dYW14Tc6KZyXmUng1F9gP',
                                gas: 1285,
                                counter: 'cfd0010000000000',
                                gas_limit: '4328000000000000',
                                storage_limit: '',
                                amount: '9999000000',
                                parameters: '{ "prim": "Unit" }',
                                entrypoint: 'default',
                                id: 20491117,
                                destination: 'tz1dA6Qk3L16sqUFZhYg4dmWm8raHntA8Wip',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.transactions last and date_range gte and after', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS") {
                address
                first_seen
                activated
                transactions(
                  last: 3
                  date_range: {
                      gte: "2020-02-27T17:55:56Z"
                  }
                  after: "opNutoZNVCcEsKMYcemrxss38w2ezYjx9UwEDv3cLFAc5HLGSBB:0"
                ) {
                page_info {
                    start_cursor
                    end_cursor
                    has_next_page
                    has_previous_page
                }
                edges {
                        cursor
                        node {
                            hash
                            batch_position
                            source
                            kind
                            timestamp
                            level
                            block
                            gas
                            counter
                            gas_limit
                            storage_limit
                            amount
                            parameters
                            entrypoint
                            id
                            destination
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                first_seen: '2018-09-13T14:51:20Z',
                activated: '2018-09-13T14:49:20.000Z',
                transactions: {
                    page_info: {
                        start_cursor: 'ooPvZ6ZtCdaJzzWgtD2xoLaxHQVQCZchv32sMtvp2bJeiwi5zip:0',
                        end_cursor: 'ooZM6ErfewYXeUJGN4as8qiHPDT6u465tTuJKF6eQktXDkgtZKW:0',
                        has_next_page: false,
                        has_previous_page: true,
                    },
                    edges: [
                        {
                            cursor: 'ooPvZ6ZtCdaJzzWgtD2xoLaxHQVQCZchv32sMtvp2bJeiwi5zip:0',
                            node: {
                                hash: 'ooPvZ6ZtCdaJzzWgtD2xoLaxHQVQCZchv32sMtvp2bJeiwi5zip',
                                batch_position: 0,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                kind: 'transaction',
                                timestamp: '2020-02-27T18:39:41Z',
                                level: 842902,
                                block: 'BLwfS29daVF4iXwJu8eM8Hr1ox8w1BRJaf3jJPdTHAG16VvxkLj',
                                gas: 1284,
                                counter: 'cdd0010000000000',
                                gas_limit: '4328000000000000',
                                storage_limit: '',
                                amount: '10000000',
                                parameters: '{ "prim": "Unit" }',
                                entrypoint: 'default',
                                id: 19921513,
                                destination: 'tz1QkeWxQgkHbmyjJuNKnTJBCNtUxtHtvbJU',
                            },
                        },
                        {
                            cursor: 'oo1TgCtTmss6LFW2WCuaM1RSDPLKhz2UQz6Ym9XTjiSiw4Dm9gk:0',
                            node: {
                                hash: 'oo1TgCtTmss6LFW2WCuaM1RSDPLKhz2UQz6Ym9XTjiSiw4Dm9gk',
                                batch_position: 0,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                kind: 'transaction',
                                timestamp: '2020-02-27T18:07:36Z',
                                level: 842872,
                                block: 'BLvCnxfJMnVAjpghwQagu27qAbzmTqATrartQ1BzmBQ8Nv4WULt',
                                gas: 1283,
                                counter: 'ccd0010000000000',
                                gas_limit: '4328000000000000',
                                storage_limit: '',
                                amount: '1000000',
                                parameters: '{ "prim": "Unit" }',
                                entrypoint: 'default',
                                id: 19920688,
                                destination: 'tz1QkeWxQgkHbmyjJuNKnTJBCNtUxtHtvbJU',
                            },
                        },
                        {
                            cursor: 'ooZM6ErfewYXeUJGN4as8qiHPDT6u465tTuJKF6eQktXDkgtZKW:0',
                            node: {
                                hash: 'ooZM6ErfewYXeUJGN4as8qiHPDT6u465tTuJKF6eQktXDkgtZKW',
                                batch_position: 0,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                kind: 'transaction',
                                timestamp: '2020-02-27T17:55:56Z',
                                level: 842861,
                                block: 'BL7YnpqVo7tZEw3hhSVLYNf7RHLMNTRSopuu9CHmQuALvRbwVRP',
                                gas: 1283,
                                counter: 'cbd0010000000000',
                                gas_limit: '4328000000000000',
                                storage_limit: '1501000000000000',
                                amount: '10000',
                                parameters: '{ "prim": "Unit" }',
                                entrypoint: 'default',
                                id: 19920382,
                                destination: 'tz1QkeWxQgkHbmyjJuNKnTJBCNtUxtHtvbJU',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.transactions last and date_range gte and order_by', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS") {
                address
                first_seen
                activated
                transactions(
                  last: 3
                  date_range: {
                    gte: "2019-10-22T18:56:24Z"
                  }
                  order_by: {field: ID, direction: ASC}
                ) {
                page_info {
                    start_cursor
                    end_cursor
                    has_next_page
                    has_previous_page
                }
                edges {
                        cursor
                        node {
                            hash
                            batch_position
                            source
                            kind
                            timestamp
                            level
                            block
                            gas
                            counter
                            gas_limit
                            storage_limit
                            amount
                            parameters
                            entrypoint
                            id
                            destination
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                first_seen: '2018-09-13T14:51:20Z',
                activated: '2018-09-13T14:49:20.000Z',
                transactions: {
                    page_info: {
                        start_cursor: 'ootgGUV6dDRfBBF9wutPQZWzeetqiAUkwrkwSL1Qy6NrTNUs6Jw:0',
                        end_cursor: 'opNxsLZeNMttsyh4PxLk35dmRsJyLXQwJjpVp89ip7pU2j5yzyR:0',
                        has_next_page: false,
                        has_previous_page: true,
                    },
                    edges: [
                        {
                            cursor: 'ootgGUV6dDRfBBF9wutPQZWzeetqiAUkwrkwSL1Qy6NrTNUs6Jw:0',
                            node: {
                                hash: 'ootgGUV6dDRfBBF9wutPQZWzeetqiAUkwrkwSL1Qy6NrTNUs6Jw',
                                batch_position: 0,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                kind: 'transaction',
                                timestamp: '2020-07-15T20:58:27Z',
                                level: 1041413,
                                block: 'BMf1K4SE3WqRJ6eD345kU74pjLkfB7ABK5u5KwZrxckNHFvUr4v',
                                gas: 1285,
                                counter: 'e1d0010000000000',
                                gas_limit: '4328000000000000',
                                storage_limit: '1501000000000000',
                                amount: '14193181',
                                parameters: '{ "prim": "Unit" }',
                                entrypoint: 'default',
                                id: 25134264,
                                destination: 'tz1cb9DMkq9ZdrDVxi3HwyLEMtbDseeZPZEa',
                            },
                        },
                        {
                            cursor: 'opEHTGa4n4efFQfpaWFNqoY4dbzXrBEo7Xo8v74AX5pEREqK28S:0',
                            node: {
                                hash: 'opEHTGa4n4efFQfpaWFNqoY4dbzXrBEo7Xo8v74AX5pEREqK28S',
                                batch_position: 0,
                                source: 'tz1YPCK2iRxWZYHxeUtKBKRosdQ1SAHKeACe',
                                kind: 'transaction',
                                timestamp: '2020-07-17T19:46:24Z',
                                level: 1044207,
                                block: 'BMYhcRrNnHTthRueAKoiyBxyuzJWEMqMgkxzqiVFXHtS84ALKua',
                                gas: 1420,
                                counter: '3ee8200000000000',
                                gas_limit: '6829000000000000',
                                storage_limit: '2c01000000000000',
                                amount: '192000000',
                                parameters: '{ "prim": "Unit" }',
                                entrypoint: 'default',
                                id: 25210462,
                                destination: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                            },
                        },
                        {
                            cursor: 'opNxsLZeNMttsyh4PxLk35dmRsJyLXQwJjpVp89ip7pU2j5yzyR:0',
                            node: {
                                hash: 'opNxsLZeNMttsyh4PxLk35dmRsJyLXQwJjpVp89ip7pU2j5yzyR',
                                batch_position: 0,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                kind: 'transaction',
                                timestamp: '2020-08-15T22:11:48Z',
                                level: 1085872,
                                block: 'BLEwgk9RsqEfhtm6u3PrLXcu7pXQ6dWMysJRVTAhNRqVqhCsF9g',
                                gas: 1285,
                                counter: 'e2d0010000000000',
                                gas_limit: '4328000000000000',
                                storage_limit: '1501000000000000',
                                amount: '50000000',
                                parameters: '{ "prim": "Unit" }',
                                entrypoint: 'default',
                                id: 26331765,
                                destination: 'tz1UovZ16T6vTuCRQhWKtE7jqeC6QaTzbc3P',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.transactions last and date_range lte', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS") {
                address
                first_seen
                activated
                transactions(
                  last: 3
                  date_range: {
                      lte: "2020-02-27T17:55:56Z"
                  }
                ) {
                page_info {
                    start_cursor
                    end_cursor
                    has_next_page
                    has_previous_page
                }
                edges {
                        cursor
                        node {
                            hash
                            batch_position
                            source
                            kind
                            timestamp
                            level
                            block
                            gas
                            counter
                            gas_limit
                            storage_limit
                            amount
                            parameters
                            entrypoint
                            id
                            destination
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                first_seen: '2018-09-13T14:51:20Z',
                activated: '2018-09-13T14:49:20.000Z',
                transactions: {
                    page_info: {
                        start_cursor: 'ooe9ypjsaQZPhowQpj69MZicQ3ZUwFGxQJyMVxdGpMRWF9SBZWr:0',
                        end_cursor: 'opQqKKXhzznFRxMPdiLyP4V1KWfuDrrNSkqF8jQ6v4JENR3K6jP:0',
                        has_next_page: false,
                        has_previous_page: true,
                    },
                    edges: [
                        {
                            cursor: 'ooe9ypjsaQZPhowQpj69MZicQ3ZUwFGxQJyMVxdGpMRWF9SBZWr:0',
                            node: {
                                hash: 'ooe9ypjsaQZPhowQpj69MZicQ3ZUwFGxQJyMVxdGpMRWF9SBZWr',
                                batch_position: 0,
                                source: 'tz1PECYmqmuba13rnNxyoBkvH2ChUt8BuJuG',
                                kind: 'transaction',
                                timestamp: '2018-09-13T16:17:05Z',
                                level: 102034,
                                block: 'BLFRNMJkWm9aRBgTwEdhZGcgKZ3idCWYWua8bRCfhx1BBU5xMmJ',
                                gas: 0,
                                counter: '9f4d000000000000',
                                gas_limit: 'c800000000000000',
                                storage_limit: '',
                                amount: '4199800000',
                                parameters: null,
                                entrypoint: null,
                                id: 1427201,
                                destination: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                            },
                        },
                        {
                            cursor: 'oop8aPtDYg7K3xqqMadPKoXzxTLNpBukasrKtBQs9bsjXQ1UWVt:1',
                            node: {
                                hash: 'oop8aPtDYg7K3xqqMadPKoXzxTLNpBukasrKtBQs9bsjXQ1UWVt',
                                batch_position: 1,
                                source: 'tz1PECYmqmuba13rnNxyoBkvH2ChUt8BuJuG',
                                kind: 'transaction',
                                timestamp: '2018-09-13T16:12:50Z',
                                level: 102031,
                                block: 'BLowVvoAyjeoMY4BRja6rEPdX37UjfdeB4JFtgQ4ZvCiRg9MNdA',
                                gas: 0,
                                counter: '9d4d000000000000',
                                gas_limit: '',
                                storage_limit: '',
                                amount: '1000000',
                                parameters: null,
                                entrypoint: null,
                                id: 1427150,
                                destination: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                            },
                        },
                        {
                            cursor: 'opQqKKXhzznFRxMPdiLyP4V1KWfuDrrNSkqF8jQ6v4JENR3K6jP:0',
                            node: {
                                hash: 'opQqKKXhzznFRxMPdiLyP4V1KWfuDrrNSkqF8jQ6v4JENR3K6jP',
                                batch_position: 0,
                                source: 'tz1LBEKXaxQbd5Gtzbc1ATCwc3pppu81aWGc',
                                kind: 'transaction',
                                timestamp: '2018-09-13T14:51:20Z',
                                level: 101957,
                                block: 'BMQzE1UWod12P5XkE15fS4CNRvKLqmWknwCEDuTP1FKEhzrVjtU',
                                gas: 10,
                                counter: '62e7000000000000',
                                gas_limit: 'c800000000000000',
                                storage_limit: '',
                                amount: '14',
                                parameters: null,
                                entrypoint: null,
                                id: 1425714,
                                destination: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.transactions last and date_range lte and before', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS") {
                address
                first_seen
                activated
                transactions(
                  last: 3
                  date_range: {
                      lte: "2020-02-27T17:55:56Z"
                  }
                  before: "oobMbc7MHSz5ttV3AcjiXqaxZSxs9ShGjcHmopoo85Jays2rniJ:0"
                ) {
                page_info {
                    start_cursor
                    end_cursor
                    has_next_page
                    has_previous_page
                }
                edges {
                        cursor
                        node {
                            hash
                            batch_position
                            source
                            kind
                            timestamp
                            level
                            block
                            gas
                            counter
                            gas_limit
                            storage_limit
                            amount
                            parameters
                            entrypoint
                            id
                            destination
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                first_seen: '2018-09-13T14:51:20Z',
                activated: '2018-09-13T14:49:20.000Z',
                transactions: {
                    page_info: {
                        start_cursor: 'oodK29zqUFb7NyMa5mRyb6DVR71FyWHh5gXF8WVgCDG6uGVuCaT:0',
                        end_cursor: 'opT4s5rWRbAEcbq6GZ272LyxNCxACqdk7c6Jm4buDcR6Fh4Zs3c:0',
                        has_next_page: true,
                        has_previous_page: true,
                    },
                    edges: [
                        {
                            cursor: 'oodK29zqUFb7NyMa5mRyb6DVR71FyWHh5gXF8WVgCDG6uGVuCaT:0',
                            node: {
                                hash: 'oodK29zqUFb7NyMa5mRyb6DVR71FyWHh5gXF8WVgCDG6uGVuCaT',
                                batch_position: 0,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                kind: 'transaction',
                                timestamp: '2020-02-22T22:29:26Z',
                                level: 836064,
                                block: 'BKspGC8xE7CT8431tR5CErsn5dEVUB6eVtXLeCSpv4285z3rZa4',
                                gas: 1284,
                                counter: 'c9d0010000000000',
                                gas_limit: '4328000000000000',
                                storage_limit: '1501000000000000',
                                amount: '100000',
                                parameters: '{ "prim": "Unit" }',
                                entrypoint: 'default',
                                id: 19731302,
                                destination: 'tz1VPz5qhzTrRTNevHC33ArBTBPV6jHpwjQB',
                            },
                        },
                        {
                            cursor: 'ooADLykQuRbk3yUc5f465SRJhGyDtJDFit48WpCp9tipnDK399X:0',
                            node: {
                                hash: 'ooADLykQuRbk3yUc5f465SRJhGyDtJDFit48WpCp9tipnDK399X',
                                batch_position: 0,
                                source: 'tz1KtGwriE7VuLwT3LwuvU9Nv4wAxP7XZ57d',
                                kind: 'transaction',
                                timestamp: '2019-10-24T20:38:46Z',
                                level: 664348,
                                block: 'BM8uvwRPXp7QtwUwcr2FtSsooUJcRbnN7hvDhUochXS8dpcXfQ8',
                                gas: 17920,
                                counter: 'e2fe010000000000',
                                gas_limit: 'fa58020000000000',
                                storage_limit: '2c01000000000000',
                                amount: '10009000000',
                                parameters: '{ "prim": "Unit" }',
                                entrypoint: 'default',
                                id: 15227316,
                                destination: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                            },
                        },
                        {
                            cursor: 'opT4s5rWRbAEcbq6GZ272LyxNCxACqdk7c6Jm4buDcR6Fh4Zs3c:0',
                            node: {
                                hash: 'opT4s5rWRbAEcbq6GZ272LyxNCxACqdk7c6Jm4buDcR6Fh4Zs3c',
                                batch_position: 0,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                kind: 'transaction',
                                timestamp: '2019-06-23T00:44:47Z',
                                level: 491770,
                                block: 'BLyWtfcsLhVGAbsYJPSFpUQiPwdHkHExBrgB3WD9wesuo3PLfUV',
                                gas: 1564,
                                counter: 'c8d0010000000000',
                                gas_limit: '4533000000000000',
                                storage_limit: '',
                                amount: '0',
                                parameters: null,
                                entrypoint: null,
                                id: 10910203,
                                destination: 'KT1BRudFZEXLYANgmZTka1xCDN5nWTMWY7SZ',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.transactions last and date_range lte and after', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS") {
                address
                first_seen
                activated
                transactions(
                  last: 3
                  date_range: {
                      lte: "2020-02-27T17:55:56Z"
                  }
                  after: "oobMbc7MHSz5ttV3AcjiXqaxZSxs9ShGjcHmopoo85Jays2rniJ:0"
                ) {
                page_info {
                    start_cursor
                    end_cursor
                    has_next_page
                    has_previous_page
                }
                edges {
                        cursor
                        node {
                            hash
                            batch_position
                            source
                            kind
                            timestamp
                            level
                            block
                            gas
                            counter
                            gas_limit
                            storage_limit
                            amount
                            parameters
                            entrypoint
                            id
                            destination
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                first_seen: '2018-09-13T14:51:20Z',
                activated: '2018-09-13T14:49:20.000Z',
                transactions: {
                    page_info: {
                        start_cursor: 'ooe9ypjsaQZPhowQpj69MZicQ3ZUwFGxQJyMVxdGpMRWF9SBZWr:0',
                        end_cursor: 'opQqKKXhzznFRxMPdiLyP4V1KWfuDrrNSkqF8jQ6v4JENR3K6jP:0',
                        has_next_page: false,
                        has_previous_page: true,
                    },
                    edges: [
                        {
                            cursor: 'ooe9ypjsaQZPhowQpj69MZicQ3ZUwFGxQJyMVxdGpMRWF9SBZWr:0',
                            node: {
                                hash: 'ooe9ypjsaQZPhowQpj69MZicQ3ZUwFGxQJyMVxdGpMRWF9SBZWr',
                                batch_position: 0,
                                source: 'tz1PECYmqmuba13rnNxyoBkvH2ChUt8BuJuG',
                                kind: 'transaction',
                                timestamp: '2018-09-13T16:17:05Z',
                                level: 102034,
                                block: 'BLFRNMJkWm9aRBgTwEdhZGcgKZ3idCWYWua8bRCfhx1BBU5xMmJ',
                                gas: 0,
                                counter: '9f4d000000000000',
                                gas_limit: 'c800000000000000',
                                storage_limit: '',
                                amount: '4199800000',
                                parameters: null,
                                entrypoint: null,
                                id: 1427201,
                                destination: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                            },
                        },
                        {
                            cursor: 'oop8aPtDYg7K3xqqMadPKoXzxTLNpBukasrKtBQs9bsjXQ1UWVt:1',
                            node: {
                                hash: 'oop8aPtDYg7K3xqqMadPKoXzxTLNpBukasrKtBQs9bsjXQ1UWVt',
                                batch_position: 1,
                                source: 'tz1PECYmqmuba13rnNxyoBkvH2ChUt8BuJuG',
                                kind: 'transaction',
                                timestamp: '2018-09-13T16:12:50Z',
                                level: 102031,
                                block: 'BLowVvoAyjeoMY4BRja6rEPdX37UjfdeB4JFtgQ4ZvCiRg9MNdA',
                                gas: 0,
                                counter: '9d4d000000000000',
                                gas_limit: '',
                                storage_limit: '',
                                amount: '1000000',
                                parameters: null,
                                entrypoint: null,
                                id: 1427150,
                                destination: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                            },
                        },
                        {
                            cursor: 'opQqKKXhzznFRxMPdiLyP4V1KWfuDrrNSkqF8jQ6v4JENR3K6jP:0',
                            node: {
                                hash: 'opQqKKXhzznFRxMPdiLyP4V1KWfuDrrNSkqF8jQ6v4JENR3K6jP',
                                batch_position: 0,
                                source: 'tz1LBEKXaxQbd5Gtzbc1ATCwc3pppu81aWGc',
                                kind: 'transaction',
                                timestamp: '2018-09-13T14:51:20Z',
                                level: 101957,
                                block: 'BMQzE1UWod12P5XkE15fS4CNRvKLqmWknwCEDuTP1FKEhzrVjtU',
                                gas: 10,
                                counter: '62e7000000000000',
                                gas_limit: 'c800000000000000',
                                storage_limit: '',
                                amount: '14',
                                parameters: null,
                                entrypoint: null,
                                id: 1425714,
                                destination: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.transactions last and date_range lte and order_by', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS") {
                address
                first_seen
                activated
                transactions(
                last: 3
                date_range: {
                    lte: "2019-10-22T18:56:24Z"
                }
                order_by: {field: ID, direction: ASC}
                ) {
                page_info {
                    start_cursor
                    end_cursor
                    has_next_page
                    has_previous_page
                }
                edges {
                        cursor
                        node {
                            hash
                            batch_position
                            source
                            kind
                            timestamp
                            level
                            block
                            gas
                            counter
                            gas_limit
                            storage_limit
                            amount
                            parameters
                            entrypoint
                            id
                            destination
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                first_seen: '2018-09-13T14:51:20Z',
                activated: '2018-09-13T14:49:20.000Z',
                transactions: {
                    page_info: {
                        start_cursor: 'opZSVPxNoaExjX6dCFnoPAh5yHk6g7BaJdce2AXUHuwW3c7ucNA:0',
                        end_cursor: 'opT4s5rWRbAEcbq6GZ272LyxNCxACqdk7c6Jm4buDcR6Fh4Zs3c:0',
                        has_next_page: false,
                        has_previous_page: true,
                    },
                    edges: [
                        {
                            cursor: 'opZSVPxNoaExjX6dCFnoPAh5yHk6g7BaJdce2AXUHuwW3c7ucNA:0',
                            node: {
                                hash: 'opZSVPxNoaExjX6dCFnoPAh5yHk6g7BaJdce2AXUHuwW3c7ucNA',
                                batch_position: 0,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                kind: 'transaction',
                                timestamp: '2019-06-13T16:29:03Z',
                                level: 478920,
                                block: 'BME5VpmuMaZfWK7sgrmqrH73yZnuiDhJGuE8dsURJTodB5LWDCe',
                                gas: 1284,
                                counter: 'c6d0010000000000',
                                gas_limit: '3c28000000000000',
                                storage_limit: '',
                                amount: '26594806',
                                parameters: null,
                                entrypoint: null,
                                id: 10588145,
                                destination: 'KT1Hw82JKNLeQUF58Tr5s8hk3Yn8XW3Hv81C',
                            },
                        },
                        {
                            cursor: 'oobMbc7MHSz5ttV3AcjiXqaxZSxs9ShGjcHmopoo85Jays2rniJ:0',
                            node: {
                                hash: 'oobMbc7MHSz5ttV3AcjiXqaxZSxs9ShGjcHmopoo85Jays2rniJ',
                                batch_position: 0,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                kind: 'transaction',
                                timestamp: '2019-06-13T16:31:03Z',
                                level: 478922,
                                block: 'BKsTAwJ939thYVT5gN4HGE8kPApsRUXELGySbv7CNgXZ9VkGKar',
                                gas: 1284,
                                counter: 'c7d0010000000000',
                                gas_limit: '3c28000000000000',
                                storage_limit: '',
                                amount: '21594067',
                                parameters: null,
                                entrypoint: null,
                                id: 10588192,
                                destination: 'KT1SrkUKHSf9m2yruXSMCVfrfUUsXN8yVtag',
                            },
                        },
                        {
                            cursor: 'opT4s5rWRbAEcbq6GZ272LyxNCxACqdk7c6Jm4buDcR6Fh4Zs3c:0',
                            node: {
                                hash: 'opT4s5rWRbAEcbq6GZ272LyxNCxACqdk7c6Jm4buDcR6Fh4Zs3c',
                                batch_position: 0,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                kind: 'transaction',
                                timestamp: '2019-06-23T00:44:47Z',
                                level: 491770,
                                block: 'BLyWtfcsLhVGAbsYJPSFpUQiPwdHkHExBrgB3WD9wesuo3PLfUV',
                                gas: 1564,
                                counter: 'c8d0010000000000',
                                gas_limit: '4533000000000000',
                                storage_limit: '',
                                amount: '0',
                                parameters: null,
                                entrypoint: null,
                                id: 10910203,
                                destination: 'KT1BRudFZEXLYANgmZTka1xCDN5nWTMWY7SZ',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.transactions last and date_range gte/lte', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS") {
                address
                first_seen
                activated
                transactions(
                  last: 3
                  date_range: {
                      lte: "2020-02-27T17:55:56Z"
                      gte: "2019-06-13T16:22:03Z"
                  }
                ) {
                page_info {
                    start_cursor
                    end_cursor
                    has_next_page
                    has_previous_page
                }
                edges {
                        cursor
                        node {
                            hash
                            batch_position
                            source
                            kind
                            timestamp
                            level
                            block
                            gas
                            counter
                            gas_limit
                            storage_limit
                            amount
                            parameters
                            entrypoint
                            id
                            destination
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                first_seen: '2018-09-13T14:51:20Z',
                activated: '2018-09-13T14:49:20.000Z',
                transactions: {
                    page_info: {
                        start_cursor: 'onfyzsehpe5MpHPZK1iTGkFxqpuhG2Xny3L9X24VCBXNRDD27pg:0',
                        end_cursor: 'oo1F42RgALBHR3ktYLq1yaeWzVFepNsuUGibaC5nWuGedjJJWci:0',
                        has_next_page: false,
                        has_previous_page: true,
                    },
                    edges: [
                        {
                            cursor: 'onfyzsehpe5MpHPZK1iTGkFxqpuhG2Xny3L9X24VCBXNRDD27pg:0',
                            node: {
                                hash: 'onfyzsehpe5MpHPZK1iTGkFxqpuhG2Xny3L9X24VCBXNRDD27pg',
                                batch_position: 0,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                kind: 'transaction',
                                timestamp: '2019-06-13T16:25:03Z',
                                level: 478916,
                                block: 'BKn6bMx7wiB7eH6RfEHtWnrGanESHcPHzWe51gJz8zxFyUkuLbd',
                                gas: 1284,
                                counter: 'c3d0010000000000',
                                gas_limit: '3c28000000000000',
                                storage_limit: '',
                                amount: '134384524',
                                parameters: null,
                                entrypoint: null,
                                id: 10588044,
                                destination: 'KT19UEmbszRSrfedo1DcRqbsYzo6UceKSN1N',
                            },
                        },
                        {
                            cursor: 'oov7xyb6aNBGhZmsqbvGKbpWoYEDA6535V5xLMQXxQtBLVdFTmX:0',
                            node: {
                                hash: 'oov7xyb6aNBGhZmsqbvGKbpWoYEDA6535V5xLMQXxQtBLVdFTmX',
                                batch_position: 0,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                kind: 'transaction',
                                timestamp: '2019-06-13T16:24:03Z',
                                level: 478915,
                                block: 'BM8xzgJhGS6Ti7fzDmMrM23yHTRnQMRSHnnmcdnmgnWraRLotmD',
                                gas: 1285,
                                counter: 'c2d0010000000000',
                                gas_limit: '3c28000000000000',
                                storage_limit: '',
                                amount: '277694173',
                                parameters: null,
                                entrypoint: null,
                                id: 10588016,
                                destination: 'KT1LdKDekzfeQEzTpj1yaJsXGfXUGhsexwq8',
                            },
                        },
                        {
                            cursor: 'oo1F42RgALBHR3ktYLq1yaeWzVFepNsuUGibaC5nWuGedjJJWci:0',
                            node: {
                                hash: 'oo1F42RgALBHR3ktYLq1yaeWzVFepNsuUGibaC5nWuGedjJJWci',
                                batch_position: 0,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                kind: 'transaction',
                                timestamp: '2019-06-13T16:22:03Z',
                                level: 478913,
                                block: 'BLQCC1ymavfJiSN9sW7zoz34smGtmexStE2Jq8p4qenNHhxXbua',
                                gas: 1284,
                                counter: 'c1d0010000000000',
                                gas_limit: '3c28000000000000',
                                storage_limit: '',
                                amount: '32933337',
                                parameters: null,
                                entrypoint: null,
                                id: 10587967,
                                destination: 'KT1SXkrJxbkuQ3i3XbreG1WFuG5C8RZsDkSC',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.transactions last and date_range gte/lte and before', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS") {
                address
                first_seen
                activated
                transactions(
                  last: 3
                  date_range: {
                      lte: "2020-02-27T17:55:56Z"
                    	gte: "2019-06-13T16:22:03Z"
                  }
                  before: "oov7xyb6aNBGhZmsqbvGKbpWoYEDA6535V5xLMQXxQtBLVdFTmX:0"
                ) {
                page_info {
                    start_cursor
                    end_cursor
                    has_next_page
                    has_previous_page
                }
                edges {
                        cursor
                        node {
                            hash
                            batch_position
                            source
                            kind
                            timestamp
                            level
                            block
                            gas
                            counter
                            gas_limit
                            storage_limit
                            amount
                            parameters
                            entrypoint
                            id
                            destination
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                first_seen: '2018-09-13T14:51:20Z',
                activated: '2018-09-13T14:49:20.000Z',
                transactions: {
                    page_info: {
                        start_cursor: 'ooZ59b3ddyATdUsbiWwX6gco4hZ7VJeBbucCjvAbNyggrnTDKbN:0',
                        end_cursor: 'onfyzsehpe5MpHPZK1iTGkFxqpuhG2Xny3L9X24VCBXNRDD27pg:0',
                        has_next_page: true,
                        has_previous_page: true,
                    },
                    edges: [
                        {
                            cursor: 'ooZ59b3ddyATdUsbiWwX6gco4hZ7VJeBbucCjvAbNyggrnTDKbN:0',
                            node: {
                                hash: 'ooZ59b3ddyATdUsbiWwX6gco4hZ7VJeBbucCjvAbNyggrnTDKbN',
                                batch_position: 0,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                kind: 'transaction',
                                timestamp: '2019-06-13T16:27:03Z',
                                level: 478918,
                                block: 'BLhWGpWqz19bGcLYoJxotAxm17vNGjymwMbGK5yizRRMQhkxEai',
                                gas: 1284,
                                counter: 'c5d0010000000000',
                                gas_limit: '3c28000000000000',
                                storage_limit: '',
                                amount: '166379804',
                                parameters: null,
                                entrypoint: null,
                                id: 10588092,
                                destination: 'KT1FPhB9bBH6BXH7a7DsX5eBUvJRyqcZhfyn',
                            },
                        },
                        {
                            cursor: 'op2Jk1AuscHtt6NWL58ETYmqenNHK6C1KSv7Rk1rPzadSTKqpL1:0',
                            node: {
                                hash: 'op2Jk1AuscHtt6NWL58ETYmqenNHK6C1KSv7Rk1rPzadSTKqpL1',
                                batch_position: 0,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                kind: 'transaction',
                                timestamp: '2019-06-13T16:26:03Z',
                                level: 478917,
                                block: 'BLBgbfhyuDD3Q2b7mAKm2S4kroYLVDp3gbkRpU7PPaZLDodi11e',
                                gas: 1284,
                                counter: 'c4d0010000000000',
                                gas_limit: '3c28000000000000',
                                storage_limit: '',
                                amount: '171235783',
                                parameters: null,
                                entrypoint: null,
                                id: 10588066,
                                destination: 'KT1C8f3CYXEwuvvvGeG9pKMLWa2Xcyci8eUn',
                            },
                        },
                        {
                            cursor: 'onfyzsehpe5MpHPZK1iTGkFxqpuhG2Xny3L9X24VCBXNRDD27pg:0',
                            node: {
                                hash: 'onfyzsehpe5MpHPZK1iTGkFxqpuhG2Xny3L9X24VCBXNRDD27pg',
                                batch_position: 0,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                kind: 'transaction',
                                timestamp: '2019-06-13T16:25:03Z',
                                level: 478916,
                                block: 'BKn6bMx7wiB7eH6RfEHtWnrGanESHcPHzWe51gJz8zxFyUkuLbd',
                                gas: 1284,
                                counter: 'c3d0010000000000',
                                gas_limit: '3c28000000000000',
                                storage_limit: '',
                                amount: '134384524',
                                parameters: null,
                                entrypoint: null,
                                id: 10588044,
                                destination: 'KT19UEmbszRSrfedo1DcRqbsYzo6UceKSN1N',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.transactions last and date_range gte/lte and after', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS") {
                address
                first_seen
                activated
                transactions(
                  last: 3
                  date_range: {
                      lte: "2020-02-27T17:55:56Z"
                    	gte: "2019-06-13T16:22:03Z"
                  }
                  after: "onfyzsehpe5MpHPZK1iTGkFxqpuhG2Xny3L9X24VCBXNRDD27pg:0"
                ) {
                page_info {
                    start_cursor
                    end_cursor
                    has_next_page
                    has_previous_page
                }
                edges {
                        cursor
                        node {
                            hash
                            batch_position
                            source
                            kind
                            timestamp
                            level
                            block
                            gas
                            counter
                            gas_limit
                            storage_limit
                            amount
                            parameters
                            entrypoint
                            id
                            destination
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                first_seen: '2018-09-13T14:51:20Z',
                activated: '2018-09-13T14:49:20.000Z',
                transactions: {
                    page_info: {
                        start_cursor: 'oov7xyb6aNBGhZmsqbvGKbpWoYEDA6535V5xLMQXxQtBLVdFTmX:0',
                        end_cursor: 'oo1F42RgALBHR3ktYLq1yaeWzVFepNsuUGibaC5nWuGedjJJWci:0',
                        has_next_page: false,
                        has_previous_page: true,
                    },
                    edges: [
                        {
                            cursor: 'oov7xyb6aNBGhZmsqbvGKbpWoYEDA6535V5xLMQXxQtBLVdFTmX:0',
                            node: {
                                hash: 'oov7xyb6aNBGhZmsqbvGKbpWoYEDA6535V5xLMQXxQtBLVdFTmX',
                                batch_position: 0,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                kind: 'transaction',
                                timestamp: '2019-06-13T16:24:03Z',
                                level: 478915,
                                block: 'BM8xzgJhGS6Ti7fzDmMrM23yHTRnQMRSHnnmcdnmgnWraRLotmD',
                                gas: 1285,
                                counter: 'c2d0010000000000',
                                gas_limit: '3c28000000000000',
                                storage_limit: '',
                                amount: '277694173',
                                parameters: null,
                                entrypoint: null,
                                id: 10588016,
                                destination: 'KT1LdKDekzfeQEzTpj1yaJsXGfXUGhsexwq8',
                            },
                        },
                        {
                            cursor: 'oo1F42RgALBHR3ktYLq1yaeWzVFepNsuUGibaC5nWuGedjJJWci:0',
                            node: {
                                hash: 'oo1F42RgALBHR3ktYLq1yaeWzVFepNsuUGibaC5nWuGedjJJWci',
                                batch_position: 0,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                kind: 'transaction',
                                timestamp: '2019-06-13T16:22:03Z',
                                level: 478913,
                                block: 'BLQCC1ymavfJiSN9sW7zoz34smGtmexStE2Jq8p4qenNHhxXbua',
                                gas: 1284,
                                counter: 'c1d0010000000000',
                                gas_limit: '3c28000000000000',
                                storage_limit: '',
                                amount: '32933337',
                                parameters: null,
                                entrypoint: null,
                                id: 10587967,
                                destination: 'KT1SXkrJxbkuQ3i3XbreG1WFuG5C8RZsDkSC',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.transactions last and date_range gte/lte and order_by', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS") {
                address
                first_seen
                activated
                transactions(
                  last: 3
                  date_range: {
                    gte: "2019-06-13T16:25:03Z"
                    lte: "2020-07-15T20:58:27Z"
                  }
                  order_by: {field: ID, direction: ASC}
                ) {
                page_info {
                    start_cursor
                    end_cursor
                    has_next_page
                    has_previous_page
                }
                edges {
                        cursor
                        node {
                            hash
                            batch_position
                            source
                            kind
                            timestamp
                            level
                            block
                            gas
                            counter
                            gas_limit
                            storage_limit
                            amount
                            parameters
                            entrypoint
                            id
                            destination
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                first_seen: '2018-09-13T14:51:20Z',
                activated: '2018-09-13T14:49:20.000Z',
                transactions: {
                    page_info: {
                        start_cursor: 'oowUdQhpUKrsM9UgPBxPXQHq5ri9BVZGsEWXvFk2w48GkZZXxno:0',
                        end_cursor: 'ootgGUV6dDRfBBF9wutPQZWzeetqiAUkwrkwSL1Qy6NrTNUs6Jw:0',
                        has_next_page: false,
                        has_previous_page: true,
                    },
                    edges: [
                        {
                            cursor: 'oowUdQhpUKrsM9UgPBxPXQHq5ri9BVZGsEWXvFk2w48GkZZXxno:0',
                            node: {
                                hash: 'oowUdQhpUKrsM9UgPBxPXQHq5ri9BVZGsEWXvFk2w48GkZZXxno',
                                batch_position: 0,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                kind: 'transaction',
                                timestamp: '2020-07-15T20:56:27Z',
                                level: 1041411,
                                block: 'BL25QMmdBxgkic7PB2z53B38rPTMYy3kAfPevQXDDsBrzbbRy6j',
                                gas: 1284,
                                counter: 'dfd0010000000000',
                                gas_limit: '4328000000000000',
                                storage_limit: '',
                                amount: '79651872',
                                parameters: '{ "prim": "Unit" }',
                                entrypoint: 'default',
                                id: 25134203,
                                destination: 'tz1bJAQfucuxvLwCHyPXvG24PjtLT1V3ewbt',
                            },
                        },
                        {
                            cursor: 'ongxtfJMZ6tdbwxUKE46463gk6rVuWHk6WNWHcSgN1Q6obmm6q1:0',
                            node: {
                                hash: 'ongxtfJMZ6tdbwxUKE46463gk6rVuWHk6WNWHcSgN1Q6obmm6q1',
                                batch_position: 0,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                kind: 'transaction',
                                timestamp: '2020-07-15T20:57:27Z',
                                level: 1041412,
                                block: 'BLFSnPLffMKuRvDR6MQErKxLitKevszN2UkwnJTNUQiGLQ6d7cM',
                                gas: 1284,
                                counter: 'e0d0010000000000',
                                gas_limit: '4328000000000000',
                                storage_limit: '',
                                amount: '188397246',
                                parameters: '{ "prim": "Unit" }',
                                entrypoint: 'default',
                                id: 25134234,
                                destination: 'tz1Vg5uaxedKk4HYn4ZMWWZ7B59GYXSm2iyj',
                            },
                        },
                        {
                            cursor: 'ootgGUV6dDRfBBF9wutPQZWzeetqiAUkwrkwSL1Qy6NrTNUs6Jw:0',
                            node: {
                                hash: 'ootgGUV6dDRfBBF9wutPQZWzeetqiAUkwrkwSL1Qy6NrTNUs6Jw',
                                batch_position: 0,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                kind: 'transaction',
                                timestamp: '2020-07-15T20:58:27Z',
                                level: 1041413,
                                block: 'BMf1K4SE3WqRJ6eD345kU74pjLkfB7ABK5u5KwZrxckNHFvUr4v',
                                gas: 1285,
                                counter: 'e1d0010000000000',
                                gas_limit: '4328000000000000',
                                storage_limit: '1501000000000000',
                                amount: '14193181',
                                parameters: '{ "prim": "Unit" }',
                                entrypoint: 'default',
                                id: 25134264,
                                destination: 'tz1cb9DMkq9ZdrDVxi3HwyLEMtbDseeZPZEa',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.transactions last and date_range gte and order_by and before', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS") {
                address
                first_seen
                activated
                transactions(
                  last: 3
                  date_range: {
                    	gte: "2019-06-13T16:22:03Z"
                  }
                  order_by: {field: ID, direction: ASC}
                  before: "ootgGUV6dDRfBBF9wutPQZWzeetqiAUkwrkwSL1Qy6NrTNUs6Jw:0"
                ) {
                page_info {
                    start_cursor
                    end_cursor
                    has_next_page
                    has_previous_page
                }
                edges {
                        cursor
                        node {
                            hash
                            batch_position
                            source
                            kind
                            timestamp
                            level
                            block
                            gas
                            counter
                            gas_limit
                            storage_limit
                            amount
                            parameters
                            entrypoint
                            id
                            destination
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                first_seen: '2018-09-13T14:51:20Z',
                activated: '2018-09-13T14:49:20.000Z',
                transactions: {
                    page_info: {
                        start_cursor: 'ooa7UPV5iL5TeV469j9BqeXxFr7tTviyxRRizVFWoc3rU1eAH8N:0',
                        end_cursor: 'ongxtfJMZ6tdbwxUKE46463gk6rVuWHk6WNWHcSgN1Q6obmm6q1:0',
                        has_next_page: true,
                        has_previous_page: true,
                    },
                    edges: [
                        {
                            cursor: 'ooa7UPV5iL5TeV469j9BqeXxFr7tTviyxRRizVFWoc3rU1eAH8N:0',
                            node: {
                                hash: 'ooa7UPV5iL5TeV469j9BqeXxFr7tTviyxRRizVFWoc3rU1eAH8N',
                                batch_position: 0,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                kind: 'transaction',
                                timestamp: '2020-07-15T20:55:27Z',
                                level: 1041410,
                                block: 'BMUm98MH7pD2eC9s5WHiPvsSjMFGPyQiJB7zHLQY9uX3ozGpQFE',
                                gas: 1285,
                                counter: 'ded0010000000000',
                                gas_limit: '4328000000000000',
                                storage_limit: '',
                                amount: '325873000',
                                parameters: '{ "prim": "Unit" }',
                                entrypoint: 'default',
                                id: 25134171,
                                destination: 'tz1gEQe76hTA9L5MpTRtNnDhhqQ1YbzVPxnY',
                            },
                        },
                        {
                            cursor: 'oowUdQhpUKrsM9UgPBxPXQHq5ri9BVZGsEWXvFk2w48GkZZXxno:0',
                            node: {
                                hash: 'oowUdQhpUKrsM9UgPBxPXQHq5ri9BVZGsEWXvFk2w48GkZZXxno',
                                batch_position: 0,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                kind: 'transaction',
                                timestamp: '2020-07-15T20:56:27Z',
                                level: 1041411,
                                block: 'BL25QMmdBxgkic7PB2z53B38rPTMYy3kAfPevQXDDsBrzbbRy6j',
                                gas: 1284,
                                counter: 'dfd0010000000000',
                                gas_limit: '4328000000000000',
                                storage_limit: '',
                                amount: '79651872',
                                parameters: '{ "prim": "Unit" }',
                                entrypoint: 'default',
                                id: 25134203,
                                destination: 'tz1bJAQfucuxvLwCHyPXvG24PjtLT1V3ewbt',
                            },
                        },
                        {
                            cursor: 'ongxtfJMZ6tdbwxUKE46463gk6rVuWHk6WNWHcSgN1Q6obmm6q1:0',
                            node: {
                                hash: 'ongxtfJMZ6tdbwxUKE46463gk6rVuWHk6WNWHcSgN1Q6obmm6q1',
                                batch_position: 0,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                kind: 'transaction',
                                timestamp: '2020-07-15T20:57:27Z',
                                level: 1041412,
                                block: 'BLFSnPLffMKuRvDR6MQErKxLitKevszN2UkwnJTNUQiGLQ6d7cM',
                                gas: 1284,
                                counter: 'e0d0010000000000',
                                gas_limit: '4328000000000000',
                                storage_limit: '',
                                amount: '188397246',
                                parameters: '{ "prim": "Unit" }',
                                entrypoint: 'default',
                                id: 25134234,
                                destination: 'tz1Vg5uaxedKk4HYn4ZMWWZ7B59GYXSm2iyj',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.transactions last and date_range gte and order_by and after', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS") {
                address
                first_seen
                activated
                transactions(
                  last: 3
                  date_range: {
                    	gte: "2019-06-13T16:22:03Z"
                  }
                  order_by: {field: ID, direction: ASC}
                  after: "ootgGUV6dDRfBBF9wutPQZWzeetqiAUkwrkwSL1Qy6NrTNUs6Jw:0"
                ) {
                page_info {
                    start_cursor
                    end_cursor
                    has_next_page
                    has_previous_page
                }
                edges {
                        cursor
                        node {
                            hash
                            batch_position
                            source
                            kind
                            timestamp
                            level
                            block
                            gas
                            counter
                            gas_limit
                            storage_limit
                            amount
                            parameters
                            entrypoint
                            id
                            destination
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                first_seen: '2018-09-13T14:51:20Z',
                activated: '2018-09-13T14:49:20.000Z',
                transactions: {
                    page_info: {
                        start_cursor: 'opEHTGa4n4efFQfpaWFNqoY4dbzXrBEo7Xo8v74AX5pEREqK28S:0',
                        end_cursor: 'opNxsLZeNMttsyh4PxLk35dmRsJyLXQwJjpVp89ip7pU2j5yzyR:0',
                        has_next_page: false,
                        has_previous_page: true,
                    },
                    edges: [
                        {
                            cursor: 'opEHTGa4n4efFQfpaWFNqoY4dbzXrBEo7Xo8v74AX5pEREqK28S:0',
                            node: {
                                hash: 'opEHTGa4n4efFQfpaWFNqoY4dbzXrBEo7Xo8v74AX5pEREqK28S',
                                batch_position: 0,
                                source: 'tz1YPCK2iRxWZYHxeUtKBKRosdQ1SAHKeACe',
                                kind: 'transaction',
                                timestamp: '2020-07-17T19:46:24Z',
                                level: 1044207,
                                block: 'BMYhcRrNnHTthRueAKoiyBxyuzJWEMqMgkxzqiVFXHtS84ALKua',
                                gas: 1420,
                                counter: '3ee8200000000000',
                                gas_limit: '6829000000000000',
                                storage_limit: '2c01000000000000',
                                amount: '192000000',
                                parameters: '{ "prim": "Unit" }',
                                entrypoint: 'default',
                                id: 25210462,
                                destination: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                            },
                        },
                        {
                            cursor: 'opNxsLZeNMttsyh4PxLk35dmRsJyLXQwJjpVp89ip7pU2j5yzyR:0',
                            node: {
                                hash: 'opNxsLZeNMttsyh4PxLk35dmRsJyLXQwJjpVp89ip7pU2j5yzyR',
                                batch_position: 0,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                kind: 'transaction',
                                timestamp: '2020-08-15T22:11:48Z',
                                level: 1085872,
                                block: 'BLEwgk9RsqEfhtm6u3PrLXcu7pXQ6dWMysJRVTAhNRqVqhCsF9g',
                                gas: 1285,
                                counter: 'e2d0010000000000',
                                gas_limit: '4328000000000000',
                                storage_limit: '1501000000000000',
                                amount: '50000000',
                                parameters: '{ "prim": "Unit" }',
                                entrypoint: 'default',
                                id: 26331765,
                                destination: 'tz1UovZ16T6vTuCRQhWKtE7jqeC6QaTzbc3P',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.transactions last and date_range lte and order_by and before', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS") {
                address
                first_seen
                activated
                transactions(
                  last: 3
                  date_range: {
                    	lte: "2019-06-13T16:22:03Z"
                  }
                  order_by: {field: ID, direction: ASC}
                  before: "oobxTtkqJcJEmR6N9begZXpgUm37DipMV3nrXHkG9eEAV69jnb8:0"
                ) {
                page_info {
                    start_cursor
                    end_cursor
                    has_next_page
                    has_previous_page
                }
                edges {
                        cursor
                        node {
                            hash
                            batch_position
                            source
                            kind
                            timestamp
                            level
                            block
                            gas
                            counter
                            gas_limit
                            storage_limit
                            amount
                            parameters
                            entrypoint
                            id
                            destination
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                first_seen: '2018-09-13T14:51:20Z',
                activated: '2018-09-13T14:49:20.000Z',
                transactions: {
                    page_info: {
                        start_cursor: 'onraimDTje9mjsrCJCLFtbMdCKB3heipUN2zosPxZ1tyod7Tytb:0',
                        end_cursor: 'opQ3d7f9BnQ6GfKX1os7Qy7Tagyo58Gwwbb5vfcT4fEjWZdiAk3:0',
                        has_next_page: true,
                        has_previous_page: true,
                    },
                    edges: [
                        {
                            cursor: 'onraimDTje9mjsrCJCLFtbMdCKB3heipUN2zosPxZ1tyod7Tytb:0',
                            node: {
                                hash: 'onraimDTje9mjsrCJCLFtbMdCKB3heipUN2zosPxZ1tyod7Tytb',
                                batch_position: 0,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                kind: 'transaction',
                                timestamp: '2019-06-13T16:14:48Z',
                                level: 478907,
                                block: 'BM3QPrbZGnpLYn9HJumn4Sw81Wet1M9YCTgadFPvUTc2sQPTUnh',
                                gas: 1284,
                                counter: 'bcd0010000000000',
                                gas_limit: '3c28000000000000',
                                storage_limit: '',
                                amount: '35046793',
                                parameters: null,
                                entrypoint: null,
                                id: 10587820,
                                destination: 'KT1BQo4CJb2GMeLE81grz3D4gKNWgXV2dkYs',
                            },
                        },
                        {
                            cursor: 'opQxQtBFCyJWe999Bv2JQ5RKLQAqihiVjbHCfR4KP6G7zcN9xh5:0',
                            node: {
                                hash: 'opQxQtBFCyJWe999Bv2JQ5RKLQAqihiVjbHCfR4KP6G7zcN9xh5',
                                batch_position: 0,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                kind: 'transaction',
                                timestamp: '2019-06-13T16:17:03Z',
                                level: 478908,
                                block: 'BLwJ7L6RWC1G4L2jDFqskKpQ2dmbWzdRwFSpFr5884HZsDGNawS',
                                gas: 1284,
                                counter: 'bdd0010000000000',
                                gas_limit: '3c28000000000000',
                                storage_limit: '',
                                amount: '19777030',
                                parameters: null,
                                entrypoint: null,
                                id: 10587846,
                                destination: 'KT1Hi9tU4QxgLfHyji4TfEyxY69ToYNTQyk7',
                            },
                        },
                        {
                            cursor: 'opQ3d7f9BnQ6GfKX1os7Qy7Tagyo58Gwwbb5vfcT4fEjWZdiAk3:0',
                            node: {
                                hash: 'opQ3d7f9BnQ6GfKX1os7Qy7Tagyo58Gwwbb5vfcT4fEjWZdiAk3',
                                batch_position: 0,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                kind: 'transaction',
                                timestamp: '2019-06-13T16:18:03Z',
                                level: 478909,
                                block: 'BLwgy2nrKjRKhgRfkEwQocGQ8gKYjwd5cwMcBeGhdcyq6f1SM7o',
                                gas: 1285,
                                counter: 'bed0010000000000',
                                gas_limit: '3c28000000000000',
                                storage_limit: '',
                                amount: '773670997',
                                parameters: null,
                                entrypoint: null,
                                id: 10587872,
                                destination: 'KT1JZGkiZaofbEwa6CSjssadkH5qHgEV6goV',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.transactions last and date_range lte and order_by and after', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS") {
                address
                first_seen
                activated
                transactions(
                  last: 3
                  date_range: {
                    	lte: "2019-06-13T16:22:03Z"
                  }
                  order_by: {field: ID, direction: ASC}
                  after: "oobxTtkqJcJEmR6N9begZXpgUm37DipMV3nrXHkG9eEAV69jnb8:0"
                ) {
                page_info {
                    start_cursor
                    end_cursor
                    has_next_page
                    has_previous_page
                }
                edges {
                        cursor
                        node {
                            hash
                            batch_position
                            source
                            kind
                            timestamp
                            level
                            block
                            gas
                            counter
                            gas_limit
                            storage_limit
                            amount
                            parameters
                            entrypoint
                            id
                            destination
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                first_seen: '2018-09-13T14:51:20Z',
                activated: '2018-09-13T14:49:20.000Z',
                transactions: {
                    page_info: {
                        start_cursor: 'ooYnoeueNkmyv3ZhQKkPxJw9bDjGp6RcxZ6Rz6yQghwLBJmtMaL:0',
                        end_cursor: 'oo1F42RgALBHR3ktYLq1yaeWzVFepNsuUGibaC5nWuGedjJJWci:0',
                        has_next_page: false,
                        has_previous_page: true,
                    },
                    edges: [
                        {
                            cursor: 'ooYnoeueNkmyv3ZhQKkPxJw9bDjGp6RcxZ6Rz6yQghwLBJmtMaL:0',
                            node: {
                                hash: 'ooYnoeueNkmyv3ZhQKkPxJw9bDjGp6RcxZ6Rz6yQghwLBJmtMaL',
                                batch_position: 0,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                kind: 'transaction',
                                timestamp: '2019-06-13T16:20:03Z',
                                level: 478911,
                                block: 'BKrjUjkDtf8H2ohXWddxGVqtiL6Zd6zJyPzXrvCv7gQ2tk5sBvM',
                                gas: 1283,
                                counter: 'c0d0010000000000',
                                gas_limit: '3c28000000000000',
                                storage_limit: '',
                                amount: '891716',
                                parameters: null,
                                entrypoint: null,
                                id: 10587920,
                                destination: 'KT1CAwkqpBZCJvF8F5iUTqdECN6dxfFyRHe2',
                            },
                        },
                        {
                            cursor: 'oo1F42RgALBHR3ktYLq1yaeWzVFepNsuUGibaC5nWuGedjJJWci:0',
                            node: {
                                hash: 'oo1F42RgALBHR3ktYLq1yaeWzVFepNsuUGibaC5nWuGedjJJWci',
                                batch_position: 0,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                kind: 'transaction',
                                timestamp: '2019-06-13T16:22:03Z',
                                level: 478913,
                                block: 'BLQCC1ymavfJiSN9sW7zoz34smGtmexStE2Jq8p4qenNHhxXbua',
                                gas: 1284,
                                counter: 'c1d0010000000000',
                                gas_limit: '3c28000000000000',
                                storage_limit: '',
                                amount: '32933337',
                                parameters: null,
                                entrypoint: null,
                                id: 10587967,
                                destination: 'KT1SXkrJxbkuQ3i3XbreG1WFuG5C8RZsDkSC',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.transactions last and date_range gte/lte and order_by and before', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS") {
                address
                first_seen
                activated
                transactions(
                  last: 3
                  date_range: {
                      lte: "2020-02-27T17:55:56Z"
                      gte: "2019-06-13T16:22:03Z"
                  }
                  order_by: {field: ID, direction: ASC}
                  before: "oobMbc7MHSz5ttV3AcjiXqaxZSxs9ShGjcHmopoo85Jays2rniJ:0"
                ) {
                page_info {
                    start_cursor
                    end_cursor
                    has_next_page
                    has_previous_page
                }
                edges {
                        cursor
                        node {
                            hash
                            batch_position
                            source
                            kind
                            timestamp
                            level
                            block
                            gas
                            counter
                            gas_limit
                            storage_limit
                            amount
                            parameters
                            entrypoint
                            id
                            destination
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                first_seen: '2018-09-13T14:51:20Z',
                activated: '2018-09-13T14:49:20.000Z',
                transactions: {
                    page_info: {
                        start_cursor: 'op2Jk1AuscHtt6NWL58ETYmqenNHK6C1KSv7Rk1rPzadSTKqpL1:0',
                        end_cursor: 'opZSVPxNoaExjX6dCFnoPAh5yHk6g7BaJdce2AXUHuwW3c7ucNA:0',
                        has_next_page: true,
                        has_previous_page: true,
                    },
                    edges: [
                        {
                            cursor: 'op2Jk1AuscHtt6NWL58ETYmqenNHK6C1KSv7Rk1rPzadSTKqpL1:0',
                            node: {
                                hash: 'op2Jk1AuscHtt6NWL58ETYmqenNHK6C1KSv7Rk1rPzadSTKqpL1',
                                batch_position: 0,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                kind: 'transaction',
                                timestamp: '2019-06-13T16:26:03Z',
                                level: 478917,
                                block: 'BLBgbfhyuDD3Q2b7mAKm2S4kroYLVDp3gbkRpU7PPaZLDodi11e',
                                gas: 1284,
                                counter: 'c4d0010000000000',
                                gas_limit: '3c28000000000000',
                                storage_limit: '',
                                amount: '171235783',
                                parameters: null,
                                entrypoint: null,
                                id: 10588066,
                                destination: 'KT1C8f3CYXEwuvvvGeG9pKMLWa2Xcyci8eUn',
                            },
                        },
                        {
                            cursor: 'ooZ59b3ddyATdUsbiWwX6gco4hZ7VJeBbucCjvAbNyggrnTDKbN:0',
                            node: {
                                hash: 'ooZ59b3ddyATdUsbiWwX6gco4hZ7VJeBbucCjvAbNyggrnTDKbN',
                                batch_position: 0,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                kind: 'transaction',
                                timestamp: '2019-06-13T16:27:03Z',
                                level: 478918,
                                block: 'BLhWGpWqz19bGcLYoJxotAxm17vNGjymwMbGK5yizRRMQhkxEai',
                                gas: 1284,
                                counter: 'c5d0010000000000',
                                gas_limit: '3c28000000000000',
                                storage_limit: '',
                                amount: '166379804',
                                parameters: null,
                                entrypoint: null,
                                id: 10588092,
                                destination: 'KT1FPhB9bBH6BXH7a7DsX5eBUvJRyqcZhfyn',
                            },
                        },
                        {
                            cursor: 'opZSVPxNoaExjX6dCFnoPAh5yHk6g7BaJdce2AXUHuwW3c7ucNA:0',
                            node: {
                                hash: 'opZSVPxNoaExjX6dCFnoPAh5yHk6g7BaJdce2AXUHuwW3c7ucNA',
                                batch_position: 0,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                kind: 'transaction',
                                timestamp: '2019-06-13T16:29:03Z',
                                level: 478920,
                                block: 'BME5VpmuMaZfWK7sgrmqrH73yZnuiDhJGuE8dsURJTodB5LWDCe',
                                gas: 1284,
                                counter: 'c6d0010000000000',
                                gas_limit: '3c28000000000000',
                                storage_limit: '',
                                amount: '26594806',
                                parameters: null,
                                entrypoint: null,
                                id: 10588145,
                                destination: 'KT1Hw82JKNLeQUF58Tr5s8hk3Yn8XW3Hv81C',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.transactions last and date_range gte/lte and order_by and after', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS") {
                address
                first_seen
                activated
                transactions(
                  last: 3
                  date_range: {
                      lte: "2020-02-27T17:55:56Z"
                    	gte: "2019-06-13T16:22:03Z"
                  }
                  order_by: {field: ID, direction: ASC}
                  after: "oobMbc7MHSz5ttV3AcjiXqaxZSxs9ShGjcHmopoo85Jays2rniJ:0"
                ) {
                page_info {
                    start_cursor
                    end_cursor
                    has_next_page
                    has_previous_page
                }
                edges {
                        cursor
                        node {
                            hash
                            batch_position
                            source
                            kind
                            timestamp
                            level
                            block
                            gas
                            counter
                            gas_limit
                            storage_limit
                            amount
                            parameters
                            entrypoint
                            id
                            destination
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                first_seen: '2018-09-13T14:51:20Z',
                activated: '2018-09-13T14:49:20.000Z',
                transactions: {
                    page_info: {
                        start_cursor: 'oodK29zqUFb7NyMa5mRyb6DVR71FyWHh5gXF8WVgCDG6uGVuCaT:0',
                        end_cursor: 'ooZM6ErfewYXeUJGN4as8qiHPDT6u465tTuJKF6eQktXDkgtZKW:0',
                        has_next_page: false,
                        has_previous_page: true,
                    },
                    edges: [
                        {
                            cursor: 'oodK29zqUFb7NyMa5mRyb6DVR71FyWHh5gXF8WVgCDG6uGVuCaT:0',
                            node: {
                                hash: 'oodK29zqUFb7NyMa5mRyb6DVR71FyWHh5gXF8WVgCDG6uGVuCaT',
                                batch_position: 0,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                kind: 'transaction',
                                timestamp: '2020-02-22T22:29:26Z',
                                level: 836064,
                                block: 'BKspGC8xE7CT8431tR5CErsn5dEVUB6eVtXLeCSpv4285z3rZa4',
                                gas: 1284,
                                counter: 'c9d0010000000000',
                                gas_limit: '4328000000000000',
                                storage_limit: '1501000000000000',
                                amount: '100000',
                                parameters: '{ "prim": "Unit" }',
                                entrypoint: 'default',
                                id: 19731302,
                                destination: 'tz1VPz5qhzTrRTNevHC33ArBTBPV6jHpwjQB',
                            },
                        },
                        {
                            cursor: 'ooHN7P3X9pUeDyWWibvZqEaYEPic4xqASsZcxk4WMQ1n7hvojah:0',
                            node: {
                                hash: 'ooHN7P3X9pUeDyWWibvZqEaYEPic4xqASsZcxk4WMQ1n7hvojah',
                                batch_position: 0,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                kind: 'transaction',
                                timestamp: '2020-02-22T22:47:26Z',
                                level: 836082,
                                block: 'BM8ieLyATFHdtgvqhq6t9y9HacUMjhSpfHwSJNobRd2us13L5HR',
                                gas: 1284,
                                counter: 'cad0010000000000',
                                gas_limit: '4328000000000000',
                                storage_limit: '',
                                amount: '100000000',
                                parameters: '{ "prim": "Unit" }',
                                entrypoint: 'default',
                                id: 19731787,
                                destination: 'tz1RAWNEEFr2DSEDQVPnHXhH24GLMDF5XNjJ',
                            },
                        },
                        {
                            cursor: 'ooZM6ErfewYXeUJGN4as8qiHPDT6u465tTuJKF6eQktXDkgtZKW:0',
                            node: {
                                hash: 'ooZM6ErfewYXeUJGN4as8qiHPDT6u465tTuJKF6eQktXDkgtZKW',
                                batch_position: 0,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                kind: 'transaction',
                                timestamp: '2020-02-27T17:55:56Z',
                                level: 842861,
                                block: 'BL7YnpqVo7tZEw3hhSVLYNf7RHLMNTRSopuu9CHmQuALvRbwVRP',
                                gas: 1283,
                                counter: 'cbd0010000000000',
                                gas_limit: '4328000000000000',
                                storage_limit: '1501000000000000',
                                amount: '10000',
                                parameters: '{ "prim": "Unit" }',
                                entrypoint: 'default',
                                id: 19920382,
                                destination: 'tz1QkeWxQgkHbmyjJuNKnTJBCNtUxtHtvbJU',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('account.transactions date_range gte/lte error', async () => {
        const GET_ACCOUNT = gql`
            query {
                account(address: "tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6") {
                    transactions (
                    first: 3
                    date_range: {
                        gte: "2019-10-22T18:56:24Z"
                        lte: "2019-10-21T12:46:11Z"
                    }
                    ) {
                        page_info {
                            start_cursor
                            end_cursor
                            has_next_page
                            has_previous_page
                        }
                    }
                }
            }
            `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });
        if (res.errors == undefined) {
            throw new Error('Expected an error message however res.errors is undefined.');
        }

        expect(res.errors[0].message).toMatch('Argument "date" of type "DateRange?" must have the "lte" value greater than the "gte" value.');
    });

    test('account.transactions query limit error', async () => {
        const GET_ACCOUNT = gql`
            query {
                account(address: "tz1dKRZVcmJBVNvaAueUmqX42vVEaLb2MbA6") {
                    transactions (
                    first: 201
                    ) {
                        page_info {
                            start_cursor
                            end_cursor
                            has_next_page
                            has_previous_page
                        }
                    }
                }
            }
            `;

        await startTestClient();
        const res = await testClientQuery({ query: GET_ACCOUNT });
        if (res.errors == undefined) {
            throw new Error('Expected an error message however res.errors is undefined.');
        }

        expect(res.errors[0].message).toMatch('Argument \"limit\" of type \"Float?\" cannot be greater than 200.');
    });
});
