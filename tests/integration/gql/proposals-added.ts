import { gql } from 'apollo-server';

export const proposalsAdded = gql`
subscription ($source: AddressFilter, $proposals: NullableProtocolHashArrayFilter) {
  proposalsAdded(source: $source, proposals: $proposals) {
    kind,
    source,
    period,
    proposals
  }
}
`;
