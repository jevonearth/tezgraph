import { gql } from 'apollo-server';

export const delegationAdded = gql`
subscription ($source: AddressFilter, $delegate: NullableAddressFilter, $status: OperationResultStatusFilter) {
  delegationAdded(source: $source, delegate: $delegate, status: $status) {
    kind,
    fee,
    counter,
    gas_limit,
    storage_limit,
    delegate,
    metadata {
      balance_updates {
        kind,
        delegate
      },
      internal_operation_results {
        kind,
        source,
        nonce,
        amount,
        destination
      },
      operation_result {
        status,
        consumed_gas,
        errors {
          kind,
          id
        }
      }
    }
  }
}
`;
