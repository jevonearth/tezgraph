import { gql } from 'apollo-server';

export const blockAdded = gql`
subscription {
  blockAdded {
    hash,
    protocol,
    chain_id,
    header {
      level,
      proto,
      predecessor,
      timestamp,
      signature
    }
  }
}`;
