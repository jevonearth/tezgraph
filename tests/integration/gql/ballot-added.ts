import { gql } from 'apollo-server';

export const ballotAdded = gql`
subscription ($source: AddressFilter, $proposal: ProtocolHashFilter, $ballot: BallotVoteFilter) {
  ballotAdded(source: $source, proposal: $proposal, ballot: $ballot) {
    kind,
    source,
    period,
    proposal,
    ballot
  }
}
`;
