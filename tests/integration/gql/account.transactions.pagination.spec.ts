import 'reflect-metadata';

import { gql } from 'apollo-server';
import { ApolloServer } from 'apollo-server-express';
import { ApolloServerTestClient, createTestClient } from 'apollo-server-testing';
import { buildSchema } from 'type-graphql';

import { AccountResolver } from '../../../src/resolvers/account';

async function startTestServer() {
    const testServer = new ApolloServer({
        schema: await buildSchema({
            resolvers: [AccountResolver],
        }),
        context: ({ req, res }) => ({ req, res }),
    });

    return testServer;
}

const server = startTestServer();

let testClient: ApolloServerTestClient;

async function startTestClient() {
    if (!testClient) {
        testClient = createTestClient(await server);
    }
}

describe('Account Resolvers', () => {
    test('relay pagination with desc id order - first', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS") {
                address
                first_seen
                activated
                transactions(first:3) {
                page_info {
                    start_cursor
                    end_cursor
                    has_next_page
                    has_previous_page
                }
                edges {
                        cursor
                        node {
                            hash
                            batch_position
                            source
                            kind
                            timestamp
                            level
                            block
                            gas
                            counter
                            gas_limit
                            storage_limit
                            amount
                            parameters
                            entrypoint
                            id
                            destination
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClient.query({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                first_seen: '2018-09-13T14:51:20Z',
                activated: '2018-09-13T14:49:20.000Z',
                transactions: {
                    page_info: {
                        start_cursor: 'opNxsLZeNMttsyh4PxLk35dmRsJyLXQwJjpVp89ip7pU2j5yzyR:0',
                        end_cursor: 'ootgGUV6dDRfBBF9wutPQZWzeetqiAUkwrkwSL1Qy6NrTNUs6Jw:0',
                        has_next_page: true,
                        has_previous_page: false,
                    },
                    edges: [
                        {
                            cursor: 'opNxsLZeNMttsyh4PxLk35dmRsJyLXQwJjpVp89ip7pU2j5yzyR:0',
                            node: {
                                hash: 'opNxsLZeNMttsyh4PxLk35dmRsJyLXQwJjpVp89ip7pU2j5yzyR',
                                batch_position: 0,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                kind: 'transaction',
                                timestamp: '2020-08-15T22:11:48Z',
                                level: 1085872,
                                block: 'BLEwgk9RsqEfhtm6u3PrLXcu7pXQ6dWMysJRVTAhNRqVqhCsF9g',
                                gas: 1285,
                                counter: 'e2d0010000000000',
                                gas_limit: '4328000000000000',
                                storage_limit: '1501000000000000',
                                amount: '50000000',
                                parameters: '{ "prim": "Unit" }',
                                entrypoint: 'default',
                                id: 26331765,
                                destination: 'tz1UovZ16T6vTuCRQhWKtE7jqeC6QaTzbc3P',
                            },
                        },
                        {
                            cursor: 'opEHTGa4n4efFQfpaWFNqoY4dbzXrBEo7Xo8v74AX5pEREqK28S:0',
                            node: {
                                hash: 'opEHTGa4n4efFQfpaWFNqoY4dbzXrBEo7Xo8v74AX5pEREqK28S',
                                batch_position: 0,
                                source: 'tz1YPCK2iRxWZYHxeUtKBKRosdQ1SAHKeACe',
                                kind: 'transaction',
                                timestamp: '2020-07-17T19:46:24Z',
                                level: 1044207,
                                block: 'BMYhcRrNnHTthRueAKoiyBxyuzJWEMqMgkxzqiVFXHtS84ALKua',
                                gas: 1420,
                                counter: '3ee8200000000000',
                                gas_limit: '6829000000000000',
                                storage_limit: '2c01000000000000',
                                amount: '192000000',
                                parameters: '{ "prim": "Unit" }',
                                entrypoint: 'default',
                                id: 25210462,
                                destination: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                            },
                        },
                        {
                            cursor: 'ootgGUV6dDRfBBF9wutPQZWzeetqiAUkwrkwSL1Qy6NrTNUs6Jw:0',
                            node: {
                                hash: 'ootgGUV6dDRfBBF9wutPQZWzeetqiAUkwrkwSL1Qy6NrTNUs6Jw',
                                batch_position: 0,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                kind: 'transaction',
                                timestamp: '2020-07-15T20:58:27Z',
                                level: 1041413,
                                block: 'BMf1K4SE3WqRJ6eD345kU74pjLkfB7ABK5u5KwZrxckNHFvUr4v',
                                gas: 1285,
                                counter: 'e1d0010000000000',
                                gas_limit: '4328000000000000',
                                storage_limit: '1501000000000000',
                                amount: '14193181',
                                parameters: '{ "prim": "Unit" }',
                                entrypoint: 'default',
                                id: 25134264,
                                destination: 'tz1cb9DMkq9ZdrDVxi3HwyLEMtbDseeZPZEa',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('relay pagination with desc id order - first and after', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS") {
                address
                first_seen
                activated
                transactions(first:3, after: "opNxsLZeNMttsyh4PxLk35dmRsJyLXQwJjpVp89ip7pU2j5yzyR:0") {
                page_info {
                    start_cursor
                    end_cursor
                    has_next_page
                    has_previous_page
                }
                edges {
                        cursor
                        node {
                            hash
                            batch_position
                            source
                            kind
                            timestamp
                            level
                            block
                            gas
                            counter
                            gas_limit
                            storage_limit
                            amount
                            parameters
                            entrypoint
                            id
                            destination
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClient.query({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                first_seen: '2018-09-13T14:51:20Z',
                activated: '2018-09-13T14:49:20.000Z',
                transactions: {
                    page_info: {
                        start_cursor: 'opEHTGa4n4efFQfpaWFNqoY4dbzXrBEo7Xo8v74AX5pEREqK28S:0',
                        end_cursor: 'ongxtfJMZ6tdbwxUKE46463gk6rVuWHk6WNWHcSgN1Q6obmm6q1:0',
                        has_next_page: true,
                        has_previous_page: true,
                    },
                    edges: [
                        {
                            cursor: 'opEHTGa4n4efFQfpaWFNqoY4dbzXrBEo7Xo8v74AX5pEREqK28S:0',
                            node: {
                                hash: 'opEHTGa4n4efFQfpaWFNqoY4dbzXrBEo7Xo8v74AX5pEREqK28S',
                                batch_position: 0,
                                source: 'tz1YPCK2iRxWZYHxeUtKBKRosdQ1SAHKeACe',
                                kind: 'transaction',
                                timestamp: '2020-07-17T19:46:24Z',
                                level: 1044207,
                                block: 'BMYhcRrNnHTthRueAKoiyBxyuzJWEMqMgkxzqiVFXHtS84ALKua',
                                gas: 1420,
                                counter: '3ee8200000000000',
                                gas_limit: '6829000000000000',
                                storage_limit: '2c01000000000000',
                                amount: '192000000',
                                parameters: '{ "prim": "Unit" }',
                                entrypoint: 'default',
                                id: 25210462,
                                destination: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                            },
                        },
                        {
                            cursor: 'ootgGUV6dDRfBBF9wutPQZWzeetqiAUkwrkwSL1Qy6NrTNUs6Jw:0',
                            node: {
                                hash: 'ootgGUV6dDRfBBF9wutPQZWzeetqiAUkwrkwSL1Qy6NrTNUs6Jw',
                                batch_position: 0,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                kind: 'transaction',
                                timestamp: '2020-07-15T20:58:27Z',
                                level: 1041413,
                                block: 'BMf1K4SE3WqRJ6eD345kU74pjLkfB7ABK5u5KwZrxckNHFvUr4v',
                                gas: 1285,
                                counter: 'e1d0010000000000',
                                gas_limit: '4328000000000000',
                                storage_limit: '1501000000000000',
                                amount: '14193181',
                                parameters: '{ "prim": "Unit" }',
                                entrypoint: 'default',
                                id: 25134264,
                                destination: 'tz1cb9DMkq9ZdrDVxi3HwyLEMtbDseeZPZEa',
                            },
                        },
                        {
                            cursor: 'ongxtfJMZ6tdbwxUKE46463gk6rVuWHk6WNWHcSgN1Q6obmm6q1:0',
                            node: {
                                hash: 'ongxtfJMZ6tdbwxUKE46463gk6rVuWHk6WNWHcSgN1Q6obmm6q1',
                                batch_position: 0,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                kind: 'transaction',
                                timestamp: '2020-07-15T20:57:27Z',
                                level: 1041412,
                                block: 'BLFSnPLffMKuRvDR6MQErKxLitKevszN2UkwnJTNUQiGLQ6d7cM',
                                gas: 1284,
                                counter: 'e0d0010000000000',
                                gas_limit: '4328000000000000',
                                storage_limit: '',
                                amount: '188397246',
                                parameters: '{ "prim": "Unit" }',
                                entrypoint: 'default',
                                id: 25134234,
                                destination: 'tz1Vg5uaxedKk4HYn4ZMWWZ7B59GYXSm2iyj',
                            },
                        },
                    ],
                },
            },
        }));
    });
    test('relay pagination with desc id order - first and before', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS") {
                address
                first_seen
                activated
                transactions(first:3, before: "ootgGUV6dDRfBBF9wutPQZWzeetqiAUkwrkwSL1Qy6NrTNUs6Jw:0") {
                page_info {
                    start_cursor
                    end_cursor
                    has_next_page
                    has_previous_page
                }
                edges {
                        cursor
                        node {
                            hash
                            batch_position
                            source
                            kind
                            timestamp
                            level
                            block
                            gas
                            counter
                            gas_limit
                            storage_limit
                            amount
                            parameters
                            entrypoint
                            id
                            destination
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClient.query({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                first_seen: '2018-09-13T14:51:20Z',
                activated: '2018-09-13T14:49:20.000Z',
                transactions: {
                    page_info: {
                        start_cursor: 'opNxsLZeNMttsyh4PxLk35dmRsJyLXQwJjpVp89ip7pU2j5yzyR:0',
                        end_cursor: 'opEHTGa4n4efFQfpaWFNqoY4dbzXrBEo7Xo8v74AX5pEREqK28S:0',
                        has_next_page: true,
                        has_previous_page: false,
                    },
                    edges: [
                        {
                            cursor: 'opNxsLZeNMttsyh4PxLk35dmRsJyLXQwJjpVp89ip7pU2j5yzyR:0',
                            node: {
                                hash: 'opNxsLZeNMttsyh4PxLk35dmRsJyLXQwJjpVp89ip7pU2j5yzyR',
                                batch_position: 0,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                kind: 'transaction',
                                timestamp: '2020-08-15T22:11:48Z',
                                level: 1085872,
                                block: 'BLEwgk9RsqEfhtm6u3PrLXcu7pXQ6dWMysJRVTAhNRqVqhCsF9g',
                                gas: 1285,
                                counter: 'e2d0010000000000',
                                gas_limit: '4328000000000000',
                                storage_limit: '1501000000000000',
                                amount: '50000000',
                                parameters: '{ "prim": "Unit" }',
                                entrypoint: 'default',
                                id: 26331765,
                                destination: 'tz1UovZ16T6vTuCRQhWKtE7jqeC6QaTzbc3P',
                            },
                        },
                        {
                            cursor: 'opEHTGa4n4efFQfpaWFNqoY4dbzXrBEo7Xo8v74AX5pEREqK28S:0',
                            node: {
                                hash: 'opEHTGa4n4efFQfpaWFNqoY4dbzXrBEo7Xo8v74AX5pEREqK28S',
                                batch_position: 0,
                                source: 'tz1YPCK2iRxWZYHxeUtKBKRosdQ1SAHKeACe',
                                kind: 'transaction',
                                timestamp: '2020-07-17T19:46:24Z',
                                level: 1044207,
                                block: 'BMYhcRrNnHTthRueAKoiyBxyuzJWEMqMgkxzqiVFXHtS84ALKua',
                                gas: 1420,
                                counter: '3ee8200000000000',
                                gas_limit: '6829000000000000',
                                storage_limit: '2c01000000000000',
                                amount: '192000000',
                                parameters: '{ "prim": "Unit" }',
                                entrypoint: 'default',
                                id: 25210462,
                                destination: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('relay pagination with asc id order - first', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS") {
                address
                first_seen
                activated
                transactions(
                  first:3
                  order_by:{field: ID, direction: ASC}
                ) {
                page_info {
                    start_cursor
                    end_cursor
                    has_next_page
                    has_previous_page
                }
                edges {
                        cursor
                        node {
                            hash
                            batch_position
                            source
                            kind
                            timestamp
                            level
                            block
                            gas
                            counter
                            gas_limit
                            storage_limit
                            amount
                            parameters
                            entrypoint
                            id
                            destination
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClient.query({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                first_seen: '2018-09-13T14:51:20Z',
                activated: '2018-09-13T14:49:20.000Z',
                transactions: {
                    page_info: {
                        start_cursor: 'opQqKKXhzznFRxMPdiLyP4V1KWfuDrrNSkqF8jQ6v4JENR3K6jP:0',
                        end_cursor: 'ooe9ypjsaQZPhowQpj69MZicQ3ZUwFGxQJyMVxdGpMRWF9SBZWr:0',
                        has_next_page: true,
                        has_previous_page: false,
                    },
                    edges: [
                        {
                            cursor: 'opQqKKXhzznFRxMPdiLyP4V1KWfuDrrNSkqF8jQ6v4JENR3K6jP:0',
                            node: {
                                hash: 'opQqKKXhzznFRxMPdiLyP4V1KWfuDrrNSkqF8jQ6v4JENR3K6jP',
                                batch_position: 0,
                                source: 'tz1LBEKXaxQbd5Gtzbc1ATCwc3pppu81aWGc',
                                kind: 'transaction',
                                timestamp: '2018-09-13T14:51:20Z',
                                level: 101957,
                                block: 'BMQzE1UWod12P5XkE15fS4CNRvKLqmWknwCEDuTP1FKEhzrVjtU',
                                gas: 10,
                                counter: '62e7000000000000',
                                gas_limit: 'c800000000000000',
                                storage_limit: '',
                                amount: '14',
                                parameters: null,
                                entrypoint: null,
                                id: 1425714,
                                destination: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                            },
                        },
                        {
                            cursor: 'oop8aPtDYg7K3xqqMadPKoXzxTLNpBukasrKtBQs9bsjXQ1UWVt:1',
                            node: {
                                hash: 'oop8aPtDYg7K3xqqMadPKoXzxTLNpBukasrKtBQs9bsjXQ1UWVt',
                                batch_position: 1,
                                source: 'tz1PECYmqmuba13rnNxyoBkvH2ChUt8BuJuG',
                                kind: 'transaction',
                                timestamp: '2018-09-13T16:12:50Z',
                                level: 102031,
                                block: 'BLowVvoAyjeoMY4BRja6rEPdX37UjfdeB4JFtgQ4ZvCiRg9MNdA',
                                gas: 0,
                                counter: '9d4d000000000000',
                                gas_limit: '',
                                storage_limit: '',
                                amount: '1000000',
                                parameters: null,
                                entrypoint: null,
                                id: 1427150,
                                destination: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                            },
                        },
                        {
                            cursor: 'ooe9ypjsaQZPhowQpj69MZicQ3ZUwFGxQJyMVxdGpMRWF9SBZWr:0',
                            node: {
                                hash: 'ooe9ypjsaQZPhowQpj69MZicQ3ZUwFGxQJyMVxdGpMRWF9SBZWr',
                                batch_position: 0,
                                source: 'tz1PECYmqmuba13rnNxyoBkvH2ChUt8BuJuG',
                                kind: 'transaction',
                                timestamp: '2018-09-13T16:17:05Z',
                                level: 102034,
                                block: 'BLFRNMJkWm9aRBgTwEdhZGcgKZ3idCWYWua8bRCfhx1BBU5xMmJ',
                                gas: 0,
                                counter: '9f4d000000000000',
                                gas_limit: 'c800000000000000',
                                storage_limit: '',
                                amount: '4199800000',
                                parameters: null,
                                entrypoint: null,
                                id: 1427201,
                                destination: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('relay pagination with asc id order - first and after', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS") {
                address
                first_seen
                activated
                transactions(
                  first:3
                  after: "opQqKKXhzznFRxMPdiLyP4V1KWfuDrrNSkqF8jQ6v4JENR3K6jP:0"
                  order_by:{field: ID, direction: ASC}
                ) {
                page_info {
                    start_cursor
                    end_cursor
                    has_next_page
                    has_previous_page
                }
                edges {
                        cursor
                        node {
                            hash
                            batch_position
                            source
                            kind
                            timestamp
                            level
                            block
                            gas
                            counter
                            gas_limit
                            storage_limit
                            amount
                            parameters
                            entrypoint
                            id
                            destination
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClient.query({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                first_seen: '2018-09-13T14:51:20Z',
                activated: '2018-09-13T14:49:20.000Z',
                transactions: {
                    page_info: {
                        start_cursor: 'oop8aPtDYg7K3xqqMadPKoXzxTLNpBukasrKtBQs9bsjXQ1UWVt:1',
                        end_cursor: 'ooeHBn4BDojRRLsNc4eWta1JrHoxGEEU8p2EjufpUcRNMcKJyNb:0',
                        has_next_page: true,
                        has_previous_page: true,
                    },
                    edges: [
                        {
                            cursor: 'oop8aPtDYg7K3xqqMadPKoXzxTLNpBukasrKtBQs9bsjXQ1UWVt:1',
                            node: {
                                hash: 'oop8aPtDYg7K3xqqMadPKoXzxTLNpBukasrKtBQs9bsjXQ1UWVt',
                                batch_position: 1,
                                source: 'tz1PECYmqmuba13rnNxyoBkvH2ChUt8BuJuG',
                                kind: 'transaction',
                                timestamp: '2018-09-13T16:12:50Z',
                                level: 102031,
                                block: 'BLowVvoAyjeoMY4BRja6rEPdX37UjfdeB4JFtgQ4ZvCiRg9MNdA',
                                gas: 0,
                                counter: '9d4d000000000000',
                                gas_limit: '',
                                storage_limit: '',
                                amount: '1000000',
                                parameters: null,
                                entrypoint: null,
                                id: 1427150,
                                destination: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                            },
                        },
                        {
                            cursor: 'ooe9ypjsaQZPhowQpj69MZicQ3ZUwFGxQJyMVxdGpMRWF9SBZWr:0',
                            node: {
                                hash: 'ooe9ypjsaQZPhowQpj69MZicQ3ZUwFGxQJyMVxdGpMRWF9SBZWr',
                                batch_position: 0,
                                source: 'tz1PECYmqmuba13rnNxyoBkvH2ChUt8BuJuG',
                                kind: 'transaction',
                                timestamp: '2018-09-13T16:17:05Z',
                                level: 102034,
                                block: 'BLFRNMJkWm9aRBgTwEdhZGcgKZ3idCWYWua8bRCfhx1BBU5xMmJ',
                                gas: 0,
                                counter: '9f4d000000000000',
                                gas_limit: 'c800000000000000',
                                storage_limit: '',
                                amount: '4199800000',
                                parameters: null,
                                entrypoint: null,
                                id: 1427201,
                                destination: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                            },
                        },
                        {
                            cursor: 'ooeHBn4BDojRRLsNc4eWta1JrHoxGEEU8p2EjufpUcRNMcKJyNb:0',
                            node: {
                                hash: 'ooeHBn4BDojRRLsNc4eWta1JrHoxGEEU8p2EjufpUcRNMcKJyNb',
                                batch_position: 0,
                                source: 'tz1PECYmqmuba13rnNxyoBkvH2ChUt8BuJuG',
                                kind: 'transaction',
                                timestamp: '2018-09-13T16:18:05Z',
                                level: 102035,
                                block: 'BLJLwRhrzz4srdUXw3FHgPazUnnMc3B1R15wpSWsBJPAYRJQCng',
                                gas: 0,
                                counter: 'a04d000000000000',
                                gas_limit: 'c800000000000000',
                                storage_limit: '',
                                amount: '20000',
                                parameters: null,
                                entrypoint: null,
                                id: 1427217,
                                destination: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                            },
                        },
                    ],
                },
            },
        }));
    });
    test('relay pagination with asc id order - first and before', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS") {
                address
                first_seen
                activated
                transactions(
                  first:3
                  before: "ooe9ypjsaQZPhowQpj69MZicQ3ZUwFGxQJyMVxdGpMRWF9SBZWr:0"
                  order_by:{field: ID, direction: ASC}
                ) {
                page_info {
                    start_cursor
                    end_cursor
                    has_next_page
                    has_previous_page
                }
                edges {
                        cursor
                        node {
                            hash
                            batch_position
                            source
                            kind
                            timestamp
                            level
                            block
                            gas
                            counter
                            gas_limit
                            storage_limit
                            amount
                            parameters
                            entrypoint
                            id
                            destination
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClient.query({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                first_seen: '2018-09-13T14:51:20Z',
                activated: '2018-09-13T14:49:20.000Z',
                transactions: {
                    page_info: {
                        start_cursor: 'opQqKKXhzznFRxMPdiLyP4V1KWfuDrrNSkqF8jQ6v4JENR3K6jP:0',
                        end_cursor: 'oop8aPtDYg7K3xqqMadPKoXzxTLNpBukasrKtBQs9bsjXQ1UWVt:1',
                        has_next_page: true,
                        has_previous_page: false,
                    },
                    edges: [
                        {
                            cursor: 'opQqKKXhzznFRxMPdiLyP4V1KWfuDrrNSkqF8jQ6v4JENR3K6jP:0',
                            node: {
                                hash: 'opQqKKXhzznFRxMPdiLyP4V1KWfuDrrNSkqF8jQ6v4JENR3K6jP',
                                batch_position: 0,
                                source: 'tz1LBEKXaxQbd5Gtzbc1ATCwc3pppu81aWGc',
                                kind: 'transaction',
                                timestamp: '2018-09-13T14:51:20Z',
                                level: 101957,
                                block: 'BMQzE1UWod12P5XkE15fS4CNRvKLqmWknwCEDuTP1FKEhzrVjtU',
                                gas: 10,
                                counter: '62e7000000000000',
                                gas_limit: 'c800000000000000',
                                storage_limit: '',
                                amount: '14',
                                parameters: null,
                                entrypoint: null,
                                id: 1425714,
                                destination: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                            },
                        },
                        {
                            cursor: 'oop8aPtDYg7K3xqqMadPKoXzxTLNpBukasrKtBQs9bsjXQ1UWVt:1',
                            node: {
                                hash: 'oop8aPtDYg7K3xqqMadPKoXzxTLNpBukasrKtBQs9bsjXQ1UWVt',
                                batch_position: 1,
                                source: 'tz1PECYmqmuba13rnNxyoBkvH2ChUt8BuJuG',
                                kind: 'transaction',
                                timestamp: '2018-09-13T16:12:50Z',
                                level: 102031,
                                block: 'BLowVvoAyjeoMY4BRja6rEPdX37UjfdeB4JFtgQ4ZvCiRg9MNdA',
                                gas: 0,
                                counter: '9d4d000000000000',
                                gas_limit: '',
                                storage_limit: '',
                                amount: '1000000',
                                parameters: null,
                                entrypoint: null,
                                id: 1427150,
                                destination: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('relay pagination with desc id order - last', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS") {
                address
                first_seen
                activated
                transactions(
                	last: 3
                ) {
                page_info {
                    start_cursor
                    end_cursor
                    has_next_page
                    has_previous_page
                }
                edges {
                        cursor
                        node {
                            hash
                            batch_position
                            source
                            kind
                            timestamp
                            level
                            block
                            gas
                            counter
                            gas_limit
                            storage_limit
                            amount
                            parameters
                            entrypoint
                            id
                            destination
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClient.query({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                first_seen: '2018-09-13T14:51:20Z',
                activated: '2018-09-13T14:49:20.000Z',
                transactions: {
                    page_info: {
                        start_cursor: 'ooe9ypjsaQZPhowQpj69MZicQ3ZUwFGxQJyMVxdGpMRWF9SBZWr:0',
                        end_cursor: 'opQqKKXhzznFRxMPdiLyP4V1KWfuDrrNSkqF8jQ6v4JENR3K6jP:0',
                        has_next_page: false,
                        has_previous_page: true,
                    },
                    edges: [
                        {
                            cursor: 'ooe9ypjsaQZPhowQpj69MZicQ3ZUwFGxQJyMVxdGpMRWF9SBZWr:0',
                            node: {
                                hash: 'ooe9ypjsaQZPhowQpj69MZicQ3ZUwFGxQJyMVxdGpMRWF9SBZWr',
                                batch_position: 0,
                                source: 'tz1PECYmqmuba13rnNxyoBkvH2ChUt8BuJuG',
                                kind: 'transaction',
                                timestamp: '2018-09-13T16:17:05Z',
                                level: 102034,
                                block: 'BLFRNMJkWm9aRBgTwEdhZGcgKZ3idCWYWua8bRCfhx1BBU5xMmJ',
                                gas: 0,
                                counter: '9f4d000000000000',
                                gas_limit: 'c800000000000000',
                                storage_limit: '',
                                amount: '4199800000',
                                parameters: null,
                                entrypoint: null,
                                id: 1427201,
                                destination: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                            },
                        },
                        {
                            cursor: 'oop8aPtDYg7K3xqqMadPKoXzxTLNpBukasrKtBQs9bsjXQ1UWVt:1',
                            node: {
                                hash: 'oop8aPtDYg7K3xqqMadPKoXzxTLNpBukasrKtBQs9bsjXQ1UWVt',
                                batch_position: 1,
                                source: 'tz1PECYmqmuba13rnNxyoBkvH2ChUt8BuJuG',
                                kind: 'transaction',
                                timestamp: '2018-09-13T16:12:50Z',
                                level: 102031,
                                block: 'BLowVvoAyjeoMY4BRja6rEPdX37UjfdeB4JFtgQ4ZvCiRg9MNdA',
                                gas: 0,
                                counter: '9d4d000000000000',
                                gas_limit: '',
                                storage_limit: '',
                                amount: '1000000',
                                parameters: null,
                                entrypoint: null,
                                id: 1427150,
                                destination: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                            },
                        },
                        {
                            cursor: 'opQqKKXhzznFRxMPdiLyP4V1KWfuDrrNSkqF8jQ6v4JENR3K6jP:0',
                            node: {
                                hash: 'opQqKKXhzznFRxMPdiLyP4V1KWfuDrrNSkqF8jQ6v4JENR3K6jP',
                                batch_position: 0,
                                source: 'tz1LBEKXaxQbd5Gtzbc1ATCwc3pppu81aWGc',
                                kind: 'transaction',
                                timestamp: '2018-09-13T14:51:20Z',
                                level: 101957,
                                block: 'BMQzE1UWod12P5XkE15fS4CNRvKLqmWknwCEDuTP1FKEhzrVjtU',
                                gas: 10,
                                counter: '62e7000000000000',
                                gas_limit: 'c800000000000000',
                                storage_limit: '',
                                amount: '14',
                                parameters: null,
                                entrypoint: null,
                                id: 1425714,
                                destination: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('relay pagination with desc id order - last and after', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS") {
                address
                first_seen
                activated
                transactions(
                	last: 3
              		after: "ooe9ypjsaQZPhowQpj69MZicQ3ZUwFGxQJyMVxdGpMRWF9SBZWr:0"
                ) {
                page_info {
                    start_cursor
                    end_cursor
                    has_next_page
                    has_previous_page
                }
                edges {
                        cursor
                        node {
                            hash
                            batch_position
                            source
                            kind
                            timestamp
                            level
                            block
                            gas
                            counter
                            gas_limit
                            storage_limit
                            amount
                            parameters
                            entrypoint
                            id
                            destination
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClient.query({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                first_seen: '2018-09-13T14:51:20Z',
                activated: '2018-09-13T14:49:20.000Z',
                transactions: {
                    page_info: {
                        start_cursor: 'oop8aPtDYg7K3xqqMadPKoXzxTLNpBukasrKtBQs9bsjXQ1UWVt:1',
                        end_cursor: 'opQqKKXhzznFRxMPdiLyP4V1KWfuDrrNSkqF8jQ6v4JENR3K6jP:0',
                        has_next_page: false,
                        has_previous_page: true,
                    },
                    edges: [
                        {
                            cursor: 'oop8aPtDYg7K3xqqMadPKoXzxTLNpBukasrKtBQs9bsjXQ1UWVt:1',
                            node: {
                                hash: 'oop8aPtDYg7K3xqqMadPKoXzxTLNpBukasrKtBQs9bsjXQ1UWVt',
                                batch_position: 1,
                                source: 'tz1PECYmqmuba13rnNxyoBkvH2ChUt8BuJuG',
                                kind: 'transaction',
                                timestamp: '2018-09-13T16:12:50Z',
                                level: 102031,
                                block: 'BLowVvoAyjeoMY4BRja6rEPdX37UjfdeB4JFtgQ4ZvCiRg9MNdA',
                                gas: 0,
                                counter: '9d4d000000000000',
                                gas_limit: '',
                                storage_limit: '',
                                amount: '1000000',
                                parameters: null,
                                entrypoint: null,
                                id: 1427150,
                                destination: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                            },
                        },
                        {
                            cursor: 'opQqKKXhzznFRxMPdiLyP4V1KWfuDrrNSkqF8jQ6v4JENR3K6jP:0',
                            node: {
                                hash: 'opQqKKXhzznFRxMPdiLyP4V1KWfuDrrNSkqF8jQ6v4JENR3K6jP',
                                batch_position: 0,
                                source: 'tz1LBEKXaxQbd5Gtzbc1ATCwc3pppu81aWGc',
                                kind: 'transaction',
                                timestamp: '2018-09-13T14:51:20Z',
                                level: 101957,
                                block: 'BMQzE1UWod12P5XkE15fS4CNRvKLqmWknwCEDuTP1FKEhzrVjtU',
                                gas: 10,
                                counter: '62e7000000000000',
                                gas_limit: 'c800000000000000',
                                storage_limit: '',
                                amount: '14',
                                parameters: null,
                                entrypoint: null,
                                id: 1425714,
                                destination: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('relay pagination with desc id order - last and before', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS") {
                address
                first_seen
                activated
                transactions(
                	last: 3
              		before: "ooe9ypjsaQZPhowQpj69MZicQ3ZUwFGxQJyMVxdGpMRWF9SBZWr:0"
                ) {
                page_info {
                    start_cursor
                    end_cursor
                    has_next_page
                    has_previous_page
                }
                edges {
                        cursor
                        node {
                            hash
                            batch_position
                            source
                            kind
                            timestamp
                            level
                            block
                            gas
                            counter
                            gas_limit
                            storage_limit
                            amount
                            parameters
                            entrypoint
                            id
                            destination
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClient.query({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                first_seen: '2018-09-13T14:51:20Z',
                activated: '2018-09-13T14:49:20.000Z',
                transactions: {
                    page_info: {
                        start_cursor: 'ooXu4igMeKVc2CT7CYUa62WxJRcpXJosLM2FF1vBXmBT61FR19K:0',
                        end_cursor: 'ooeHBn4BDojRRLsNc4eWta1JrHoxGEEU8p2EjufpUcRNMcKJyNb:0',
                        has_next_page: true,
                        has_previous_page: true,
                    },
                    edges: [
                        {
                            cursor: 'ooXu4igMeKVc2CT7CYUa62WxJRcpXJosLM2FF1vBXmBT61FR19K:0',
                            node: {
                                hash: 'ooXu4igMeKVc2CT7CYUa62WxJRcpXJosLM2FF1vBXmBT61FR19K',
                                batch_position: 0,
                                source: 'tz1KtGwriE7VuLwT3LwuvU9Nv4wAxP7XZ57d',
                                kind: 'transaction',
                                timestamp: '2018-10-05T00:32:44Z',
                                level: 131767,
                                block: 'BMWGakwzmTPyXT5ncfT9HfF154AJhXi8z1o1UuGeFaMJV1o93nK',
                                gas: 0,
                                counter: '7de5010000000000',
                                gas_limit: 'c800000000000000',
                                storage_limit: '',
                                amount: '5543250000',
                                parameters: null,
                                entrypoint: null,
                                id: 2050623,
                                destination: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                            },
                        },
                        {
                            cursor: 'op4hQbsXN35JghDjC9pM3wUFQW8roEmwcncBY5KoW2hF3CQbPGH:0',
                            node: {
                                hash: 'op4hQbsXN35JghDjC9pM3wUFQW8roEmwcncBY5KoW2hF3CQbPGH',
                                batch_position: 0,
                                source: 'tz1PECYmqmuba13rnNxyoBkvH2ChUt8BuJuG',
                                kind: 'transaction',
                                timestamp: '2018-09-13T16:23:20Z',
                                level: 102039,
                                block: 'BM1CtHUump3aahzSMVHh6HVBEvmWramVCpuTm7tFFnPGVhS6xr5',
                                gas: 0,
                                counter: 'a14d000000000000',
                                gas_limit: 'c800000000000000',
                                storage_limit: '',
                                amount: '3000',
                                parameters: null,
                                entrypoint: null,
                                id: 1427300,
                                destination: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                            },
                        },
                        {
                            cursor: 'ooeHBn4BDojRRLsNc4eWta1JrHoxGEEU8p2EjufpUcRNMcKJyNb:0',
                            node: {
                                hash: 'ooeHBn4BDojRRLsNc4eWta1JrHoxGEEU8p2EjufpUcRNMcKJyNb',
                                batch_position: 0,
                                source: 'tz1PECYmqmuba13rnNxyoBkvH2ChUt8BuJuG',
                                kind: 'transaction',
                                timestamp: '2018-09-13T16:18:05Z',
                                level: 102035,
                                block: 'BLJLwRhrzz4srdUXw3FHgPazUnnMc3B1R15wpSWsBJPAYRJQCng',
                                gas: 0,
                                counter: 'a04d000000000000',
                                gas_limit: 'c800000000000000',
                                storage_limit: '',
                                amount: '20000',
                                parameters: null,
                                entrypoint: null,
                                id: 1427217,
                                destination: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('relay pagination with asc id order - last', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS") {
                address
                first_seen
                activated
                transactions(
                  last: 3
                  order_by:{field: ID, direction: ASC}
                ) {
                page_info {
                    start_cursor
                    end_cursor
                    has_next_page
                    has_previous_page
                }
                edges {
                        cursor
                        node {
                            hash
                            batch_position
                            source
                            kind
                            timestamp
                            level
                            block
                            gas
                            counter
                            gas_limit
                            storage_limit
                            amount
                            parameters
                            entrypoint
                            id
                            destination
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClient.query({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                first_seen: '2018-09-13T14:51:20Z',
                activated: '2018-09-13T14:49:20.000Z',
                transactions: {
                    page_info: {
                        start_cursor: 'ootgGUV6dDRfBBF9wutPQZWzeetqiAUkwrkwSL1Qy6NrTNUs6Jw:0',
                        end_cursor: 'opNxsLZeNMttsyh4PxLk35dmRsJyLXQwJjpVp89ip7pU2j5yzyR:0',
                        has_next_page: false,
                        has_previous_page: true,
                    },
                    edges: [
                        {
                            cursor: 'ootgGUV6dDRfBBF9wutPQZWzeetqiAUkwrkwSL1Qy6NrTNUs6Jw:0',
                            node: {
                                hash: 'ootgGUV6dDRfBBF9wutPQZWzeetqiAUkwrkwSL1Qy6NrTNUs6Jw',
                                batch_position: 0,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                kind: 'transaction',
                                timestamp: '2020-07-15T20:58:27Z',
                                level: 1041413,
                                block: 'BMf1K4SE3WqRJ6eD345kU74pjLkfB7ABK5u5KwZrxckNHFvUr4v',
                                gas: 1285,
                                counter: 'e1d0010000000000',
                                gas_limit: '4328000000000000',
                                storage_limit: '1501000000000000',
                                amount: '14193181',
                                parameters: '{ "prim": "Unit" }',
                                entrypoint: 'default',
                                id: 25134264,
                                destination: 'tz1cb9DMkq9ZdrDVxi3HwyLEMtbDseeZPZEa',
                            },
                        },
                        {
                            cursor: 'opEHTGa4n4efFQfpaWFNqoY4dbzXrBEo7Xo8v74AX5pEREqK28S:0',
                            node: {
                                hash: 'opEHTGa4n4efFQfpaWFNqoY4dbzXrBEo7Xo8v74AX5pEREqK28S',
                                batch_position: 0,
                                source: 'tz1YPCK2iRxWZYHxeUtKBKRosdQ1SAHKeACe',
                                kind: 'transaction',
                                timestamp: '2020-07-17T19:46:24Z',
                                level: 1044207,
                                block: 'BMYhcRrNnHTthRueAKoiyBxyuzJWEMqMgkxzqiVFXHtS84ALKua',
                                gas: 1420,
                                counter: '3ee8200000000000',
                                gas_limit: '6829000000000000',
                                storage_limit: '2c01000000000000',
                                amount: '192000000',
                                parameters: '{ "prim": "Unit" }',
                                entrypoint: 'default',
                                id: 25210462,
                                destination: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                            },
                        },
                        {
                            cursor: 'opNxsLZeNMttsyh4PxLk35dmRsJyLXQwJjpVp89ip7pU2j5yzyR:0',
                            node: {
                                hash: 'opNxsLZeNMttsyh4PxLk35dmRsJyLXQwJjpVp89ip7pU2j5yzyR',
                                batch_position: 0,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                kind: 'transaction',
                                timestamp: '2020-08-15T22:11:48Z',
                                level: 1085872,
                                block: 'BLEwgk9RsqEfhtm6u3PrLXcu7pXQ6dWMysJRVTAhNRqVqhCsF9g',
                                gas: 1285,
                                counter: 'e2d0010000000000',
                                gas_limit: '4328000000000000',
                                storage_limit: '1501000000000000',
                                amount: '50000000',
                                parameters: '{ "prim": "Unit" }',
                                entrypoint: 'default',
                                id: 26331765,
                                destination: 'tz1UovZ16T6vTuCRQhWKtE7jqeC6QaTzbc3P',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('relay pagination with asc id order - last and after', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS") {
                address
                first_seen
                activated
                transactions(
                  last: 3
                  order_by:{field: ID, direction: ASC}
                  after:"ootgGUV6dDRfBBF9wutPQZWzeetqiAUkwrkwSL1Qy6NrTNUs6Jw:0"
                ) {
                page_info {
                    start_cursor
                    end_cursor
                    has_next_page
                    has_previous_page
                }
                edges {
                        cursor
                        node {
                            hash
                            batch_position
                            source
                            kind
                            timestamp
                            level
                            block
                            gas
                            counter
                            gas_limit
                            storage_limit
                            amount
                            parameters
                            entrypoint
                            id
                            destination
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClient.query({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                first_seen: '2018-09-13T14:51:20Z',
                activated: '2018-09-13T14:49:20.000Z',
                transactions: {
                    page_info: {
                        start_cursor: 'opEHTGa4n4efFQfpaWFNqoY4dbzXrBEo7Xo8v74AX5pEREqK28S:0',
                        end_cursor: 'opNxsLZeNMttsyh4PxLk35dmRsJyLXQwJjpVp89ip7pU2j5yzyR:0',
                        has_next_page: false,
                        has_previous_page: true,
                    },
                    edges: [
                        {
                            cursor: 'opEHTGa4n4efFQfpaWFNqoY4dbzXrBEo7Xo8v74AX5pEREqK28S:0',
                            node: {
                                hash: 'opEHTGa4n4efFQfpaWFNqoY4dbzXrBEo7Xo8v74AX5pEREqK28S',
                                batch_position: 0,
                                source: 'tz1YPCK2iRxWZYHxeUtKBKRosdQ1SAHKeACe',
                                kind: 'transaction',
                                timestamp: '2020-07-17T19:46:24Z',
                                level: 1044207,
                                block: 'BMYhcRrNnHTthRueAKoiyBxyuzJWEMqMgkxzqiVFXHtS84ALKua',
                                gas: 1420,
                                counter: '3ee8200000000000',
                                gas_limit: '6829000000000000',
                                storage_limit: '2c01000000000000',
                                amount: '192000000',
                                parameters: '{ "prim": "Unit" }',
                                entrypoint: 'default',
                                id: 25210462,
                                destination: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                            },
                        },
                        {
                            cursor: 'opNxsLZeNMttsyh4PxLk35dmRsJyLXQwJjpVp89ip7pU2j5yzyR:0',
                            node: {
                                hash: 'opNxsLZeNMttsyh4PxLk35dmRsJyLXQwJjpVp89ip7pU2j5yzyR',
                                batch_position: 0,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                kind: 'transaction',
                                timestamp: '2020-08-15T22:11:48Z',
                                level: 1085872,
                                block: 'BLEwgk9RsqEfhtm6u3PrLXcu7pXQ6dWMysJRVTAhNRqVqhCsF9g',
                                gas: 1285,
                                counter: 'e2d0010000000000',
                                gas_limit: '4328000000000000',
                                storage_limit: '1501000000000000',
                                amount: '50000000',
                                parameters: '{ "prim": "Unit" }',
                                entrypoint: 'default',
                                id: 26331765,
                                destination: 'tz1UovZ16T6vTuCRQhWKtE7jqeC6QaTzbc3P',
                            },
                        },
                    ],
                },
            },
        }));
    });

    test('relay pagination with asc id order - last and before', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS") {
                address
                first_seen
                activated
                transactions(
                  last:3
                  before: "ooa7UPV5iL5TeV469j9BqeXxFr7tTviyxRRizVFWoc3rU1eAH8N:0"
                  order_by:{field: ID, direction: ASC}
                ) {
                page_info {
                    start_cursor
                    end_cursor
                    has_next_page
                    has_previous_page
                }
                edges {
                        cursor
                        node {
                            hash
                            batch_position
                            source
                            kind
                            timestamp
                            level
                            block
                            gas
                            counter
                            gas_limit
                            storage_limit
                            amount
                            parameters
                            entrypoint
                            id
                            destination
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClient.query({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                first_seen: '2018-09-13T14:51:20Z',
                activated: '2018-09-13T14:49:20.000Z',
                transactions: {
                    page_info: {
                        start_cursor: 'opB8Yp3kyXctnsv1PnSxeHcempXtuvJ8y4ttKhReust8iBXj9t8:0',
                        end_cursor: 'ooZ68VKGkWdnLEY3nRGkaZZAMvMHvdf5nrnssmmhc7eXc7LEQGD:0',
                        has_next_page: true,
                        has_previous_page: true,
                    },
                    edges: [
                        {
                            cursor: 'opB8Yp3kyXctnsv1PnSxeHcempXtuvJ8y4ttKhReust8iBXj9t8:0',
                            node: {
                                hash: 'opB8Yp3kyXctnsv1PnSxeHcempXtuvJ8y4ttKhReust8iBXj9t8',
                                batch_position: 0,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                kind: 'transaction',
                                timestamp: '2020-07-15T20:50:27Z',
                                level: 1041405,
                                block: 'BKvd9oBBiWLok31cP9ftX6Nftcu9DHq1GVPhXb5KAnuzB2zS5GJ',
                                gas: 1285,
                                counter: 'dbd0010000000000',
                                gas_limit: '4328000000000000',
                                storage_limit: '1501000000000000',
                                amount: '50217319',
                                parameters: '{ "prim": "Unit" }',
                                entrypoint: 'default',
                                id: 25134017,
                                destination: 'tz1X3xMBb3XzbpK4cehfQRoDgTVFJ2mBQBwt',
                            },
                        },
                        {
                            cursor: 'oo8sVjaRzoFHaoxgtVdAbqbPujVJzt1ToknGwapTjHJM5jRhyMx:0',
                            node: {
                                hash: 'oo8sVjaRzoFHaoxgtVdAbqbPujVJzt1ToknGwapTjHJM5jRhyMx',
                                batch_position: 0,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                kind: 'transaction',
                                timestamp: '2020-07-15T20:52:27Z',
                                level: 1041407,
                                block: 'BKj2RNeeJg6ZUQMr3MYWaDv9t6zJE1j9QFTvW6MhMLDRTjtM5Dp',
                                gas: 1284,
                                counter: 'dcd0010000000000',
                                gas_limit: '4328000000000000',
                                storage_limit: '',
                                amount: '223081138',
                                parameters: '{ "prim": "Unit" }',
                                entrypoint: 'default',
                                id: 25134075,
                                destination: 'tz1RR8ZLwjNNma9JkohknkPCWfiuxKxc5y9W',
                            },
                        },
                        {
                            cursor: 'ooZ68VKGkWdnLEY3nRGkaZZAMvMHvdf5nrnssmmhc7eXc7LEQGD:0',
                            node: {
                                hash: 'ooZ68VKGkWdnLEY3nRGkaZZAMvMHvdf5nrnssmmhc7eXc7LEQGD',
                                batch_position: 0,
                                source: 'tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS',
                                kind: 'transaction',
                                timestamp: '2020-07-15T20:54:27Z',
                                level: 1041409,
                                block: 'BLAuF4FoKqgzDt1rY4NB1VPWmNtNzsN9tJowi9f64KLNofU3McH',
                                gas: 1286,
                                counter: 'ddd0010000000000',
                                gas_limit: '4328000000000000',
                                storage_limit: '1501000000000000',
                                amount: '286268648',
                                parameters: '{ "prim": "Unit" }',
                                entrypoint: 'default',
                                id: 25134138,
                                destination: 'tz1gEQe76hTA9L5MpTRtNnDhhqQ1YbzVPxnY',
                            },
                        },
                    ],
                },
            },
        }));
    });
});
