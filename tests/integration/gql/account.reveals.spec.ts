import 'reflect-metadata';

import { gql } from 'apollo-server';
import { ApolloServer } from 'apollo-server-express';
import { ApolloServerTestClient, createTestClient } from 'apollo-server-testing';
import { buildSchema } from 'type-graphql';

import { AccountResolver } from '../../../src/resolvers/account';

async function startTestServer() {
    const testServer = new ApolloServer({
        schema: await buildSchema({
            resolvers: [AccountResolver],
        }),
        context: ({ req, res }) => ({ req, res }),
    });

    return testServer;
}

const server = startTestServer();

let testClient: ApolloServerTestClient;

async function startTestClient() {
    if (!testClient) {
        testClient = createTestClient(await server);
    }
}

describe('Account Resolvers', () => {
    test('account.delegations first', async () => {
        const GET_ACCOUNT = gql`
        query {
            account(address: "tz1a3Gs9whDXUAVzKSbJ3abYaEUxhQm1wdb1") {
                address
                first_seen
                activated
                public_key
                reveals (first: 3) {
                    page_info {
                        start_cursor
                        end_cursor
                        has_next_page
                        has_previous_page
                    }
                    edges {
                        cursor
                        node {
                            hash
                            batch_position
                            source
                            kind
                            timestamp
                            level
                            block
                            gas
                            counter
                            gas_limit
                            public_key
                            id
                        }
                    }
                }
            }
        }
        `;

        await startTestClient();
        const res = await testClient.query({ query: GET_ACCOUNT });

        expect(JSON.stringify(res.data)).toMatch(JSON.stringify({
            account: {
                address: 'tz1a3Gs9whDXUAVzKSbJ3abYaEUxhQm1wdb1',
                first_seen: '2019-10-02T02:55:48Z',
                activated: null,
                public_key: 'edpktwAyqsfbYuGgtcstjLh2tRUtfTxwfWUdhqTwJmcBbWs1uSTEB5',
                reveals: {
                    page_info: {
                        start_cursor: 'onwbonv722FrR2m8isDPgCz4XzSmkC4oaQEHcFsYw3SLiPgKE7y:0',
                        end_cursor: 'onwbonv722FrR2m8isDPgCz4XzSmkC4oaQEHcFsYw3SLiPgKE7y:0',
                        has_next_page: false,
                        has_previous_page: false,
                    },
                    edges: [
                        {
                            cursor: 'onwbonv722FrR2m8isDPgCz4XzSmkC4oaQEHcFsYw3SLiPgKE7y:0',
                            node: {
                                hash: 'onwbonv722FrR2m8isDPgCz4XzSmkC4oaQEHcFsYw3SLiPgKE7y',
                                batch_position: 0,
                                source: 'tz1a3Gs9whDXUAVzKSbJ3abYaEUxhQm1wdb1',
                                kind: 'reveal',
                                timestamp: '2019-11-28T11:14:13Z',
                                level: 712767,
                                block: 'BLD1denfmAXrnMZBk5VcLNiV6YB3ybLuai42hiLKGHUNAFMT1n9',
                                gas: 1269,
                                counter: '6e36250000000000',
                                gas_limit: '1027000000000000',
                                public_key: 'edpktwAyqsfbYuGgtcstjLh2tRUtfTxwfWUdhqTwJmcBbWs1uSTEB5',
                                id: 16442188,
                            },
                        },
                    ],
                },
            },
        }));
    });
});
