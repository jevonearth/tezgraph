import chai from 'chai';
import chaiAsPromised from 'chai-as-promised';
import chaiString from 'chai-string';

chai.use(chaiString);
chai.use(chaiAsPromised);

export const expect = chai.expect;
