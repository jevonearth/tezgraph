import { BlockResponse } from '@taquito/rpc';
import { UserInputError } from 'apollo-server';
import { expect } from 'chai';
import { anything, capture, instance, mock, verify, when } from 'ts-mockito';

import { BlockNotification } from '../../../src/entity/subscriptions/block-notification';
import { EndorsementNotification } from '../../../src/entity/subscriptions/endorsement-notification';
import { OperationKind, OperationNotification } from '../../../src/entity/subscriptions/operation-notification';
import { TransactionNotification } from '../../../src/entity/subscriptions/transaction-notification';
import { RpcMonitorBlockHeader } from '../../../src/rpc/rpc-monitor-block-header';
import { TezosRpcClient } from '../../../src/rpc/tezos-rpc-client';
import { BlockConverter } from '../../../src/subscriptions/converters/block-converter';
import { HeadBlockProvider } from '../../../src/subscriptions/head-block-provider';
import { OperationsProvider } from '../../../src/subscriptions/operations-provider';
import { Triggers, TypedPubSub } from '../../../src/subscriptions/typed-pub-sub';
import { arrayToIterator, iteratorToArray } from '../../../src/utils/async-iterator-utils';
import { EnvConfig } from '../../../src/utils/env-config';
import { MockedLogger, mockLogger } from '../mocks';

describe(OperationsProvider.name, () => {
    let target: OperationsProvider;
    let pubSub: TypedPubSub;
    let rpcClient: TezosRpcClient;
    let blockConverter: BlockConverter;
    let headBlockProvider: HeadBlockProvider;
    let config: EnvConfig;
    let logger: MockedLogger;

    const opType = EndorsementNotification;
    const opKind = OperationKind.endorsement;
    let ops: OperationNotification[];

    const act = async (level?: number) => iteratorToArray(target.iterate(opType, level));

    beforeEach(() => {
        pubSub = mock(TypedPubSub);
        rpcClient = mock(TezosRpcClient);
        blockConverter = mock(BlockConverter);
        config = mock(EnvConfig);
        headBlockProvider = mock(HeadBlockProvider);
        logger = mockLogger(OperationsProvider);
        target = new OperationsProvider(
            instance(pubSub),
            instance(rpcClient),
            instance(blockConverter),
            instance(headBlockProvider),
            instance(config),
            logger.factory,
        );

        ops = [...Array(7).keys()].map(i => Object.assign(new EndorsementNotification(), { id: i }));
        ops[2] = Object.assign(new TransactionNotification(), { id: 2 });

        when(pubSub.iterate(Triggers.operations[opKind])).thenReturn(arrayToIterator([ops[5], ops[6]]));
        when(config.startBlockLevelMaxFromHead).thenReturn(7);
        when(headBlockProvider.getHeader())
            .thenResolve(<RpcMonitorBlockHeader>{ level: 600 })
            .thenResolve(<RpcMonitorBlockHeader>{ level: 601 });
    });

    it('should iterate fresh operations if no replay block specified', async () => {
        const operations = await act();

        expect(operations).have.ordered.members([ops[5], ops[6]]);
    });

    it('should replay from specified block', async () => {
        mockRpcBlock(599, [ops[0], ops[1]]); // Past
        mockRpcBlock(600, [ops[2], ops[3]]); // Head
        mockRpcBlock(601, [ops[4]]); // Head in the meantime

        const operations = await act(599);

        // Ops[2] should be filtered out b/c different kind
        expect(operations).have.ordered.members([ops[0], ops[1], ops[3], ops[4], ops[5], ops[6]]);
        verify(logger.mock.error(anything())).never();
    });

    it('should proceed to next if a block processing failed', async () => {
        when(rpcClient.getBlock(600)).thenReject(new Error('Oups'));
        mockRpcBlock(601, [ops[1]]);

        const operations = await act(600);

        expect(operations).have.ordered.members([ops[1], ops[5], ops[6]]);
        expect(capture(logger.mock.error).first()[0]).contains('Oups'); /* eslint-disable-line @typescript-eslint/unbound-method */
    });

    it('should fail if specified block is in far past', async () => {
        let receivedError: any;
        try {
            await act(550);
        } catch (e) {
            receivedError = e;
        }

        expect(receivedError).instanceof(UserInputError);
        expect(receivedError.message).contain('#550').and.contain('#600').and.contain('7');
    });

    function mockRpcBlock(level: number, operations: readonly OperationNotification[]) {
        const rpcBlock = <BlockResponse>{};
        when(rpcClient.getBlock(level)).thenResolve(rpcBlock);
        when(blockConverter.convert(rpcBlock)).thenReturn(<BlockNotification>{ operations });
    }
});
