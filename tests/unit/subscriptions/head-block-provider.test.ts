import { BlockHeaderResponse } from '@taquito/rpc';
import { expect } from 'chai';
import { capture, instance, mock, verify, when } from 'ts-mockito';

import { RpcMonitorBlockHeader } from '../../../src/rpc/rpc-monitor-block-header';
import { TezosRpcClient } from '../../../src/rpc/tezos-rpc-client';
import { HeadBlockProvider } from '../../../src/subscriptions/head-block-provider';
import { Triggers, TypedPubSub } from '../../../src/subscriptions/typed-pub-sub';

describe(HeadBlockProvider.name, () => {
    let target: HeadBlockProvider;
    let pubSub: TypedPubSub;
    let rpcClient: TezosRpcClient;

    beforeEach(() => {
        pubSub = mock(TypedPubSub);
        rpcClient = mock(TezosRpcClient);
        target = new HeadBlockProvider(instance(pubSub), instance(rpcClient));
    });

    it('should provide last monitor header', async () => {
        const monitorBlock = <RpcMonitorBlockHeader>{};
        const onNewBlock = <any>capture((x: any) => pubSub.subscribe(Triggers.monitorBlockHeaders, x)).last()[1];

        onNewBlock(monitorBlock);
        const block = await target.getHeader();

        expect(block).equals(monitorBlock);
        verify(rpcClient.getHeadBlockHeader()).never();
    });

    it('should get block from RPC if nothing from monitor yet', async () => {
        const rpcBlock = <BlockHeaderResponse>{};
        when(rpcClient.getHeadBlockHeader()).thenResolve(rpcBlock);

        const block = await target.getHeader();

        expect(block).equals(rpcBlock);
        expect(await target.getHeader()).equals(block); // Should be cached.
        verify(rpcClient.getHeadBlockHeader()).once();
    });
});
