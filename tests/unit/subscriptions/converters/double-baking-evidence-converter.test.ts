import * as rpc from '@taquito/rpc';
import { expect } from 'chai';
import { instance, mock, when } from 'ts-mockito';

import { DoubleBakingEvidenceNotification } from '../../../../src/entity/subscriptions/double-baking-evidence-notification';
import { OperationKind } from '../../../../src/entity/subscriptions/operation-notification';
import { OperationMetadataConverter } from '../../../../src/subscriptions/converters/common/operation-metadata-converter';
import { DoubleBakingEvidenceConverter } from '../../../../src/subscriptions/converters/double-baking-evidence-converter';
import { mockBaseProperties, mockOperationMetadata } from './mocks';

describe(DoubleBakingEvidenceConverter.name, () => {
    let target: DoubleBakingEvidenceConverter;
    let rpcOperation: rpc.OperationContentsAndResultDoubleBaking;
    let metadataConverter: OperationMetadataConverter;

    const act = () => target.convert(rpcOperation, mockBaseProperties());

    beforeEach(() => {
        metadataConverter = mock(OperationMetadataConverter);
        target = new DoubleBakingEvidenceConverter(instance(metadataConverter));
    });

    it('should convert values correctly', () => {
        rpcOperation = {
            kind: rpc.OpKind.DOUBLE_BAKING_EVIDENCE,
            bh1: {
                level: 1111,
                proto: 2221,
                predecessor: 'pred1',
                timestamp: 'time1',
                validation_pass: 3331,
                operations_hash: 'hash1',
                fitness: ['fit1'],
                context: 'ctx1',
                priority: 4441,
                proof_of_work_nonce: 'pown1',
                seed_nonce_hash: 'snh1',
                signature: 'sig1',
            },
            bh2: {
                level: 1112,
                proto: 2222,
                predecessor: 'pred2',
                timestamp: 'time2',
                validation_pass: 3332,
                operations_hash: 'hash2',
                fitness: ['fit2'],
                context: 'ctx2',
                priority: 4442,
                proof_of_work_nonce: 'pown2',
                seed_nonce_hash: 'snh2',
                signature: 'sig2',
            },
            metadata: null!,
        };
        const metadata = mockOperationMetadata();
        when(metadataConverter.convertSimple(rpcOperation)).thenReturn(metadata);

        const operation = act();

        const expected: DoubleBakingEvidenceNotification = {
            kind: OperationKind.double_baking_evidence,
            bh1: {
                level: 1111,
                proto: 2221,
                predecessor: 'pred1',
                timestamp: 'time1',
                validation_pass: 3331,
                operations_hash: 'hash1',
                fitness: ['fit1'],
                context: 'ctx1',
                priority: 4441,
                proof_of_work_nonce: 'pown1',
                seed_nonce_hash: 'snh1',
                signature: 'sig1',
            },
            bh2: {
                level: 1112,
                proto: 2222,
                predecessor: 'pred2',
                timestamp: 'time2',
                validation_pass: 3332,
                operations_hash: 'hash2',
                fitness: ['fit2'],
                context: 'ctx2',
                priority: 4442,
                proof_of_work_nonce: 'pown2',
                seed_nonce_hash: 'snh2',
                signature: 'sig2',
            },
            ...mockBaseProperties(),
            metadata,
        };
        expect(operation).deep.equals(expected)
            .and.instanceof(DoubleBakingEvidenceNotification);
    });
});
