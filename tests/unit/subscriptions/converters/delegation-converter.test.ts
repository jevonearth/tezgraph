import * as rpc from '@taquito/rpc';
import { BigNumber } from 'bignumber.js';
import { expect } from 'chai';
import { instance, mock, when } from 'ts-mockito';

import { DelegationMetadata, DelegationNotification } from '../../../../src/entity/subscriptions/delegation-notification';
import { OperationKind } from '../../../../src/entity/subscriptions/operation-notification';
import { OperationMetadataConverter } from '../../../../src/subscriptions/converters/common/operation-metadata-converter';
import { DelegationConverter } from '../../../../src/subscriptions/converters/delegation-converter';
import {
    DelegationResultConverter,
} from '../../../../src/subscriptions/converters/operation-results/delegation-result-converter';
import { mockBaseProperties, mockOperationMetadata } from './mocks';

describe(DelegationConverter.name, () => {
    let target: DelegationConverter;
    let metadataConverter: OperationMetadataConverter;
    let resultConverter: DelegationResultConverter;
    let rpcOperation: rpc.OperationContentsAndResultDelegation;

    const act = () => target.convert(rpcOperation, mockBaseProperties());

    beforeEach(() => {
        metadataConverter = mock(OperationMetadataConverter);
        resultConverter = mock(DelegationResultConverter);
        target = new DelegationConverter(instance(metadataConverter), resultConverter);
    });

    it('should convert values correctly', () => {
        rpcOperation = {
            kind: rpc.OpKind.DELEGATION,
            source: 'sss',
            fee: '111',
            counter: '222',
            gas_limit: '333',
            storage_limit: '444',
            delegate: 'ddd',
            metadata: null!,
        };
        const metadata = <DelegationMetadata>mockOperationMetadata();
        when(metadataConverter.convert(rpcOperation, resultConverter)).thenReturn(metadata);

        const operation = act();

        const expected: DelegationNotification = {
            kind: OperationKind.delegation,
            source: 'sss',
            fee: new BigNumber(111),
            counter: new BigNumber(222),
            gas_limit: new BigNumber(333),
            storage_limit: new BigNumber(444),
            delegate: 'ddd',
            ...mockBaseProperties(),
            metadata,
        };
        expect(operation).deep.equals(expected)
            .and.instanceof(DelegationNotification);
    });
});
