import * as rpc from '@taquito/rpc';
import { expect } from 'chai';
import { instance, mock, when } from 'ts-mockito';

import { EndorsementNotification } from '../../../../src/entity/subscriptions/endorsement-notification';
import { OperationKind } from '../../../../src/entity/subscriptions/operation-notification';
import { BalanceUpdateConverter } from '../../../../src/subscriptions/converters/common/balance-update-converter';
import { EndorsementConverter } from '../../../../src/subscriptions/converters/endorsement-converter';
import { mockBalanceUpdate, mockBaseProperties } from './mocks';

describe(EndorsementConverter.name, () => {
    let target: EndorsementConverter;
    let rpcOperation: rpc.OperationContentsAndResultEndorsement;
    let balanceUpdateConverter: BalanceUpdateConverter;

    const act = () => target.convert(rpcOperation, mockBaseProperties());

    beforeEach(() => {
        balanceUpdateConverter = mock(BalanceUpdateConverter);
        target = new EndorsementConverter(instance(balanceUpdateConverter));
    });

    it('should convert values correctly', () => {
        rpcOperation = {
            kind: rpc.OpKind.ENDORSEMENT,
            level: 111,
            metadata: {
                balance_updates: [<rpc.OperationMetadataBalanceUpdates>{ contract: 'balance-input' }],
                delegate: 'ddd',
                slots: [222],
            },
        };
        const convertedBalanceUpdates = [mockBalanceUpdate()];
        when(balanceUpdateConverter.convert(rpcOperation.metadata.balance_updates)).thenReturn(convertedBalanceUpdates);

        const operation = act();

        const expected: EndorsementNotification = {
            kind: OperationKind.endorsement,
            level: 111,
            metadata: {
                balance_updates: convertedBalanceUpdates,
                delegate: 'ddd',
                slots: [222],
            },
            ...mockBaseProperties(),
        };
        expect(operation).deep.equals(expected)
            .and.instanceof(EndorsementNotification);
    });
});
