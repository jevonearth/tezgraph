import * as rpc from '@taquito/rpc';
import { expect } from 'chai';
import { instance, mock, when } from 'ts-mockito';

import { ActivateAccountNotification } from '../../../../src/entity/subscriptions/activate-account-notification';
import { OperationKind } from '../../../../src/entity/subscriptions/operation-notification';
import { ActivateAccountConverter } from '../../../../src/subscriptions/converters/activate-account-converter';
import { OperationMetadataConverter } from '../../../../src/subscriptions/converters/common/operation-metadata-converter';
import { mockBaseProperties, mockOperationMetadata } from './mocks';

describe(ActivateAccountConverter.name, () => {
    let target: ActivateAccountConverter;
    let rpcOperation: rpc.OperationContentsAndResultActivateAccount;
    let metadataConverter: OperationMetadataConverter;

    const act = () => target.convert(rpcOperation, mockBaseProperties());

    beforeEach(() => {
        metadataConverter = mock(OperationMetadataConverter);
        target = new ActivateAccountConverter(instance(metadataConverter));
    });

    it('should convert values correctly', () => {
        rpcOperation = {
            kind: rpc.OpKind.ACTIVATION,
            pkh: 'ppp',
            secret: 'sss',
            metadata: null!,
        };
        const metadata = mockOperationMetadata();
        when(metadataConverter.convertSimple(rpcOperation)).thenReturn(metadata);

        const operation = act();

        const expected: ActivateAccountNotification = {
            kind: OperationKind.activate_account,
            pkh: 'ppp',
            secret: 'sss',
            ...mockBaseProperties(),
            metadata,
        };
        expect(operation).deep.equals(expected)
            .and.instanceof(ActivateAccountNotification);
    });
});
