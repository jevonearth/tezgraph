import { BalanceUpdate } from '../../../../src/entity/subscriptions/balance-update';
import { BlockNotification } from '../../../../src/entity/subscriptions/block-notification';
import { OperationNotificationInfo } from '../../../../src/entity/subscriptions/operation-notification';
import { BaseOperationProperties } from '../../../../src/subscriptions/converters/block-converter';
import { asReadonly } from '../../../../src/utils/conversion';

export function mockBaseProperties(): BaseOperationProperties {
    return {
        info: <OperationNotificationInfo>{ protocol: 'ppp' },
        block: <BlockNotification>{ hash: 'haha' },
    };
}

export function mockOperationMetadata() {
    return {
        balance_updates: asReadonly([mockBalanceUpdate()]),
    };
}

export function mockBalanceUpdate(): BalanceUpdate {
    return <BalanceUpdate>{ contract: 'ccc' };
}
