import { BigNumber } from 'bignumber.js';
import { expect } from 'chai';

import { convertBigNumber } from '../../../../../src/subscriptions/converters/common/big-number-converter';

describe(convertBigNumber.name, () => {
    const act = (s: string | undefined) => convertBigNumber(s);

    it('should return converted number', () => {
        const num = act('123');

        expect(num).deep.equals(new BigNumber(123));
    });

    for (const invalid of [undefined, null, '']) {
        it(`should return undefined if ${JSON.stringify(invalid)} input`, () => {
            const num = act(invalid!);

            expect(num).equals(undefined);
        });
    }

    it('should throw if invalid input', () => {
        expect(() => act('wtf'))
            .throws().which.property('message').contains('"wtf"');
    });
});
