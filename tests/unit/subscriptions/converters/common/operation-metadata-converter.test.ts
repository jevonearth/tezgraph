import * as rpc from '@taquito/rpc';
import { expect } from 'chai';
import { instance, mock, when } from 'ts-mockito';

import { InternalOperationResult } from '../../../../../src/entity/subscriptions/operation-result';
import { BalanceUpdateConverter } from '../../../../../src/subscriptions/converters/common/balance-update-converter';
import { OperationMetadataConverter } from '../../../../../src/subscriptions/converters/common/operation-metadata-converter';
import {
    InternalOperationResultConverter,
} from '../../../../../src/subscriptions/converters/operation-results/internal-operation-result-converter';
import { nameof } from '../../../../../src/utils/reflection';
import { mockBalanceUpdate } from '../mocks';

describe(OperationMetadataConverter.name, () => {
    let target: OperationMetadataConverter;
    let balanceUpdateConverter: BalanceUpdateConverter;
    let internalOperationResultConverter: InternalOperationResultConverter;

    beforeEach(() => {
        balanceUpdateConverter = mock(BalanceUpdateConverter);
        internalOperationResultConverter = mock(InternalOperationResultConverter);
        target = new OperationMetadataConverter(instance(balanceUpdateConverter), instance(internalOperationResultConverter));
    });

    it(`${nameof<OperationMetadataConverter>('convertSimple')}() should convert values correctly`, () => {
        const rpcMetadata = { balance_updates: [<rpc.OperationMetadataBalanceUpdates>{ contract: 'balance-input' }] };
        const convertedBalanceUpdates = [mockBalanceUpdate()];
        when(balanceUpdateConverter.convert(rpcMetadata.balance_updates)).thenReturn(convertedBalanceUpdates);

        const metadata = target.convertSimple({ metadata: rpcMetadata });

        expect(metadata).deep.equals({ balance_updates: convertedBalanceUpdates });
    });

    it(`${nameof<OperationMetadataConverter>('convert')}() should convert values correctly`, () => {
        const rpcMetadata = {
            balance_updates: [<rpc.OperationMetadataBalanceUpdates>{ contract: 'balance-input' }],
            internal_operation_results: [<rpc.InternalOperationResult>{ source: 'src-input' }],
            operation_result: 123,
        };
        const convertedBalanceUpdates = [mockBalanceUpdate()];
        const convertedInternalResult = <InternalOperationResult>{ source: 'src-converted' };

        when(balanceUpdateConverter.convert(rpcMetadata.balance_updates)).thenReturn(convertedBalanceUpdates);
        when(internalOperationResultConverter.convert(rpcMetadata.internal_operation_results[0])).thenReturn(convertedInternalResult);
        const resultConverter = { convert: (r: number) => `result-${r}` };

        const metadata = target.convert({ metadata: rpcMetadata }, resultConverter);

        expect(metadata).deep.equals({
            balance_updates: convertedBalanceUpdates,
            internal_operation_results: [convertedInternalResult],
            operation_result: 'result-123',
        });
    });
});
