import * as rpc from '@taquito/rpc';
import { BigNumber } from 'bignumber.js';
import { expect } from 'chai';

import {
    BalanceUpdate,
    BalanceUpdateCategory,
    BalanceUpdateKind,
} from '../../../../../src/entity/subscriptions/balance-update';
import { BalanceUpdateConverter } from '../../../../../src/subscriptions/converters/common/balance-update-converter';

describe(BalanceUpdateConverter.name, () => {
    let rpcUpdates: rpc.OperationBalanceUpdatesItem[];

    const act = () => new BalanceUpdateConverter().convert(rpcUpdates);

    beforeEach(() => {
        rpcUpdates = [
            {
                kind: 'contract',
                category: 'rewards',
                contract: 'ccc',
                delegate: 'ddd',
                cycle: 111,
                change: '222',
            },
        ];
    });

    it('should convert values correctly', () => {
        const updates = act();

        const expected: BalanceUpdate = {
            kind: BalanceUpdateKind.contract,
            category: BalanceUpdateCategory.rewards,
            contract: 'ccc',
            delegate: 'ddd',
            cycle: 111,
            change: new BigNumber(222),
        };
        expect(updates).deep.equals([expected]);
    });

    it('should support undefined "category"', () => {
        rpcUpdates[0].category = undefined;

        const updates = act();

        expect(updates[0].category).equals(undefined);
    });

    [undefined, null].forEach(input => {
        it(`should return undefined if ${input} input`, () => {
            rpcUpdates = input!;

            const updates = act();

            expect(updates).equals(undefined);
        });
    });
});
