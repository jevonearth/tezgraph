import * as rpc from '@taquito/rpc';
import { expect } from 'chai';

import { DelegationResult } from '../../../../../src/entity/subscriptions/delegation-notification';
import {
    DelegationResultConverter,
} from '../../../../../src/subscriptions/converters/operation-results/delegation-result-converter';
import { baseOperationResultProperties } from './base-operation-result.mock';

describe(DelegationResultConverter.name, () => {
    const target = new DelegationResultConverter();

    const act = (r: rpc.OperationResultDelegation) => target.convert(r);

    it('should convert values correctly', () => {
        const rpcResult: rpc.OperationResultDelegation = baseOperationResultProperties.getRpc();

        const result = act(rpcResult);

        const expected: DelegationResult = baseOperationResultProperties.getExpected();
        expect(result).deep.equals(expected)
            .and.instanceof(DelegationResult);
    });
});
