import * as rpc from '@taquito/rpc';
import { BigNumber } from 'bignumber.js';

import { AbstractOperationResult, OperationResultStatus } from '../../../../../src/entity/subscriptions/operation-result';

export const baseOperationResultProperties = {
    getRpc() {
        return {
            status: <rpc.OperationResultStatusEnum>'applied',
            consumed_gas: '111',
            errors: [{ id: 'xx', kind: 'yy' }],
        };
    },
    getExpected(): AbstractOperationResult {
        return {
            status: OperationResultStatus.applied,
            consumed_gas: new BigNumber(111),
            errors: [{ id: 'xx', kind: 'yy' }],
        };
    },
};
