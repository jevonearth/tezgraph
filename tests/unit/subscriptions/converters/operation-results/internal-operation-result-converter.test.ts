import * as rpc from '@taquito/rpc';
import { BigNumber } from 'bignumber.js';
import { expect } from 'chai';
import { instance, mock, when } from 'ts-mockito';

import {
    InternalOperationResult,
    InternalOperationResultKind,
} from '../../../../../src/entity/subscriptions/operation-result';
import {
    DelegationResultConverter,
} from '../../../../../src/subscriptions/converters/operation-results/delegation-result-converter';
import {
    InternalOperationResultConverter,
} from '../../../../../src/subscriptions/converters/operation-results/internal-operation-result-converter';
import {
    OriginationResultConverter,
} from '../../../../../src/subscriptions/converters/operation-results/origination-result-converter';
import {
    RevealResultConverter,
} from '../../../../../src/subscriptions/converters/operation-results/reveal-result-converter';
import {
    TransactionResultConverter,
} from '../../../../../src/subscriptions/converters/operation-results/transaction-result-converter';

describe(InternalOperationResultConverter.name, () => {
    let target: InternalOperationResultConverter;
    let delegationResultConverter: DelegationResultConverter;
    let originationResultConverter: OriginationResultConverter;
    let revealResultConverter: RevealResultConverter;
    let transactionResultConverter: TransactionResultConverter;
    let rpcResult: rpc.InternalOperationResult;

    const act = () => target.convert(rpcResult);

    beforeEach(() => {
        delegationResultConverter = mock(DelegationResultConverter);
        originationResultConverter = mock(OriginationResultConverter);
        revealResultConverter = mock(RevealResultConverter);
        transactionResultConverter = mock(TransactionResultConverter);
        target = new InternalOperationResultConverter(
            instance(delegationResultConverter),
            instance(originationResultConverter),
            instance(revealResultConverter),
            instance(transactionResultConverter),
        );

        rpcResult = {
            kind: rpc.OpKind.TRANSACTION,
            source: 'src',
            nonce: 111,
            amount: '222',
            destination: 'dst',
            parameters: {
                entrypoint: 'smart',
                value: { prim: 'michelson-param' },
            },
            public_key: 'kkk',
            balance: '333',
            delegate: 'ddd',
            script: {
                code: [],
                storage: { prim: 'michelson-script' },
            },
            result: <rpc.InternalOperationResultEnum>{ consumed_gas: '123' },
        };
    });

    it('should convert values correctly', () => {
        const internalResult = act();

        const expected: InternalOperationResult = {
            kind: InternalOperationResultKind.transaction,
            source: 'src',
            nonce: 111,
            amount: new BigNumber(222),
            destination: 'dst',
            parameters: {
                entrypoint: 'smart',
                value: { prim: 'michelson-param' },
            },
            public_key: 'kkk',
            balance: new BigNumber(333),
            delegate: 'ddd',
            script: {
                code: [],
                storage: { prim: 'michelson-script' },
            },
            result: null!,
        };
        expect(internalResult).deep.equals(expected);
    });

    testResultType(rpc.OpKind.TRANSACTION, () => transactionResultConverter);
    testResultType(rpc.OpKind.REVEAL, () => revealResultConverter);
    testResultType(rpc.OpKind.DELEGATION, () => delegationResultConverter);
    testResultType(rpc.OpKind.ORIGINATION, () => originationResultConverter);

    function testResultType<TResult>(
        kind: rpc.InternalOperationResultKindEnum,
        getConverter: () => { convert(r: rpc.InternalOperationResultEnum): TResult },
    ) {
        it(`should convert ${kind} result`, () => {
            rpcResult.kind = kind;
            const testResult = <TResult>{};
            when(getConverter().convert(rpcResult.result)).thenReturn(testResult);

            const internalResult = act();

            expect(internalResult.result).equals(testResult);
        });
    }
});
