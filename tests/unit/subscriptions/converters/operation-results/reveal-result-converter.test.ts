import * as rpc from '@taquito/rpc';
import { expect } from 'chai';

import { RevealResult } from '../../../../../src/entity/subscriptions/reveal-notification';
import {
    RevealResultConverter,
} from '../../../../../src/subscriptions/converters/operation-results/reveal-result-converter';
import { baseOperationResultProperties } from './base-operation-result.mock';

describe(RevealResultConverter.name, () => {
    const target = new RevealResultConverter();

    const act = (r: rpc.OperationResultReveal) => target.convert(r);

    it('should convert values correctly', () => {
        const rpcResult: rpc.OperationResultReveal = baseOperationResultProperties.getRpc();

        const result = act(rpcResult);

        const expected: RevealResult = baseOperationResultProperties.getExpected();
        expect(result).deep.equals(expected)
            .and.instanceof(RevealResult);
    });
});
