import * as rpc from '@taquito/rpc';
import { BigNumber } from 'bignumber.js';
import { expect } from 'chai';
import { instance, mock, when } from 'ts-mockito';

import { OriginationResult } from '../../../../../src/entity/subscriptions/origination-notification';
import { BalanceUpdateConverter } from '../../../../../src/subscriptions/converters/common/balance-update-converter';
import {
    OriginationResultConverter,
} from '../../../../../src/subscriptions/converters/operation-results/origination-result-converter';
import { mockBalanceUpdate } from '../mocks';
import { baseOperationResultProperties } from './base-operation-result.mock';

describe(OriginationResultConverter.name, () => {
    let target: OriginationResultConverter;
    let balanceUpdateConverter: BalanceUpdateConverter;

    const act = (r: rpc.OperationResultOrigination) => target.convert(r);

    beforeEach(() => {
        balanceUpdateConverter = mock(BalanceUpdateConverter);
        target = new OriginationResultConverter(instance(balanceUpdateConverter));
    });

    it('should convert values correctly', () => {
        const rpcResult: rpc.OperationResultOrigination = {
            ...baseOperationResultProperties.getRpc(),
            balance_updates: [<rpc.OperationBalanceUpdatesItem>{ contract: 'balance-input' }],
            originated_contracts: ['c1', 'c2'],
            storage_size: '222',
            paid_storage_size_diff: '333',
        };
        const convertedBalanceUpdates = [mockBalanceUpdate()];
        when(balanceUpdateConverter.convert(rpcResult.balance_updates)).thenReturn(convertedBalanceUpdates);

        const result = act(rpcResult);

        const expected: OriginationResult = {
            ...baseOperationResultProperties.getExpected(),
            balance_updates: convertedBalanceUpdates,
            originated_contracts: ['c1', 'c2'],
            storage_size: new BigNumber(222),
            paid_storage_size_diff: new BigNumber(333),
        };
        expect(result).deep.equals(expected)
            .and.instanceof(OriginationResult);
    });
});
