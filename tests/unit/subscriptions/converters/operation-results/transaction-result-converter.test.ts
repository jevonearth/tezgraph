import * as rpc from '@taquito/rpc';
import { BigNumber } from 'bignumber.js';
import { expect } from 'chai';
import { instance, mock, when } from 'ts-mockito';

import { BigMapDiff } from '../../../../../src/entity/subscriptions/big-map-diff';
import { TransactionResult } from '../../../../../src/entity/subscriptions/transaction-notification';
import { BalanceUpdateConverter } from '../../../../../src/subscriptions/converters/common/balance-update-converter';
import { BigMapDiffConverter } from '../../../../../src/subscriptions/converters/common/big-map-diff-converter';
import {
    TransactionResultConverter,
} from '../../../../../src/subscriptions/converters/operation-results/transaction-result-converter';
import { mockBalanceUpdate } from '../mocks';
import { baseOperationResultProperties } from './base-operation-result.mock';

describe(TransactionResultConverter.name, () => {
    let target: TransactionResultConverter;
    let balanceUpdateConverter: BalanceUpdateConverter;
    let bigMapDiffConverter: BigMapDiffConverter;

    const act = (r: rpc.OperationResultTransaction) => target.convert(r);

    beforeEach(() => {
        balanceUpdateConverter = mock(BalanceUpdateConverter);
        bigMapDiffConverter = mock(BigMapDiffConverter);
        target = new TransactionResultConverter(instance(balanceUpdateConverter), instance(bigMapDiffConverter));
    });

    it('should convert values correctly', () => {
        const rpcResult: rpc.OperationResultTransaction = {
            ...baseOperationResultProperties.getRpc(),
            storage: { prim: 'storage-michelson' },
            big_map_diff: [<rpc.ContractBigMapDiffItem>{ key_hash: 'diff-input' }],
            balance_updates: [<rpc.OperationBalanceUpdatesItem>{ contract: 'balance-input' }],
            originated_contracts: ['c1', 'c2'],
            storage_size: '222',
            paid_storage_size_diff: '333',
            allocated_destination_contract: true,
        };
        const convertedBigMapDiff = [<BigMapDiff>{ key_hash: 'diff-converted' }];
        const convertedBalanceUpdates = [mockBalanceUpdate()];
        when(bigMapDiffConverter.convert(rpcResult.big_map_diff)).thenReturn(convertedBigMapDiff);
        when(balanceUpdateConverter.convert(rpcResult.balance_updates)).thenReturn(convertedBalanceUpdates);

        const result = act(rpcResult);

        const expected: TransactionResult = {
            ...baseOperationResultProperties.getExpected(),
            storage: { prim: 'storage-michelson' },
            big_map_diff: convertedBigMapDiff,
            balance_updates: convertedBalanceUpdates,
            originated_contracts: ['c1', 'c2'],
            storage_size: new BigNumber(222),
            paid_storage_size_diff: new BigNumber(333),
            allocated_destination_contract: true,
        };
        expect(result).deep.equals(expected)
            .and.instanceof(TransactionResult);
    });
});
