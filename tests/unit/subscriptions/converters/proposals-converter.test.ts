import * as rpc from '@taquito/rpc';
import { expect } from 'chai';

import { OperationKind } from '../../../../src/entity/subscriptions/operation-notification';
import { ProposalsNotification } from '../../../../src/entity/subscriptions/proposals-notification';
import { ProposalsConverter } from '../../../../src/subscriptions/converters/proposals-converter';
import { mockBaseProperties } from './mocks';

describe(ProposalsConverter.name, () => {
    const target = new ProposalsConverter();
    let rpcOperation: rpc.OperationContentsAndResultProposals;

    const act = () => target.convert(rpcOperation, mockBaseProperties());

    it('should convert values correctly', () => {
        rpcOperation = {
            kind: rpc.OpKind.PROPOSALS,
            source: 'sss',
            period: 111,
            proposals: ['ppp'],
        };

        const operation = act();

        const expected: ProposalsNotification = {
            kind: OperationKind.proposals,
            source: 'sss',
            period: 111,
            proposals: ['ppp'],
            ...mockBaseProperties(),
        };
        expect(operation).deep.equals(expected)
            .and.instanceof(ProposalsNotification);
    });
});
