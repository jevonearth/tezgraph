import * as rpc from '@taquito/rpc';
import { expect } from 'chai';
import { instance, mock, when } from 'ts-mockito';

import { OperationKind } from '../../../../src/entity/subscriptions/operation-notification';
import { SeedNonceRevelationNotification } from '../../../../src/entity/subscriptions/seed-nonce-revelation-notification';
import { OperationMetadataConverter } from '../../../../src/subscriptions/converters/common/operation-metadata-converter';
import { SeedNonceRevelationConverter } from '../../../../src/subscriptions/converters/seed-nonce-revelation-converter';
import { mockBaseProperties, mockOperationMetadata } from './mocks';

describe(SeedNonceRevelationConverter.name, () => {
    let target: SeedNonceRevelationConverter;
    let rpcOperation: rpc.OperationContentsAndResultRevelation;
    let metadataConverter: OperationMetadataConverter;

    const act = () => target.convert(rpcOperation, mockBaseProperties());

    beforeEach(() => {
        metadataConverter = mock(OperationMetadataConverter);
        target = new SeedNonceRevelationConverter(instance(metadataConverter));
    });

    it('should convert values correctly', () => {
        rpcOperation = {
            kind: rpc.OpKind.SEED_NONCE_REVELATION,
            level: 111,
            nonce: 'nnn',
            metadata: null!,
        };
        const metadata = mockOperationMetadata();
        when(metadataConverter.convertSimple(rpcOperation)).thenReturn(metadata);

        const operation = act();

        const expected: SeedNonceRevelationNotification = {
            kind: OperationKind.seed_nonce_revelation,
            level: 111,
            nonce: 'nnn',
            ...mockBaseProperties(),
            metadata,
        };
        expect(operation).deep.equals(expected)
            .and.instanceof(SeedNonceRevelationNotification);
    });
});
