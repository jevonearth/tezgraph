import * as rpc from '@taquito/rpc';
import { BigNumber } from 'bignumber.js';
import { expect } from 'chai';
import { instance, mock, when } from 'ts-mockito';

import { OperationKind } from '../../../../src/entity/subscriptions/operation-notification';
import { TransactionMetadata, TransactionNotification } from '../../../../src/entity/subscriptions/transaction-notification';
import { OperationMetadataConverter } from '../../../../src/subscriptions/converters/common/operation-metadata-converter';
import {
    TransactionResultConverter,
} from '../../../../src/subscriptions/converters/operation-results/transaction-result-converter';
import { TransactionConverter } from '../../../../src/subscriptions/converters/transaction-converter';
import { mockBaseProperties, mockOperationMetadata } from './mocks';

describe(TransactionConverter.name, () => {
    let target: TransactionConverter;
    let metadataConverter: OperationMetadataConverter;
    let resultConverter: TransactionResultConverter;
    let rpcOperation: rpc.OperationContentsAndResultTransaction;

    const act = () => target.convert(rpcOperation, mockBaseProperties());

    beforeEach(() => {
        metadataConverter = mock(OperationMetadataConverter);
        resultConverter = mock(TransactionResultConverter);
        target = new TransactionConverter(instance(metadataConverter), resultConverter);
    });

    it('should convert values correctly', () => {
        rpcOperation = {
            kind: rpc.OpKind.TRANSACTION,
            source: 'sss',
            fee: '111',
            counter: '222',
            gas_limit: '333',
            storage_limit: '444',
            amount: '555',
            destination: 'ddd',
            parameters: {
                entrypoint: 'eee',
                value: { prim: 'michelson' },
            },
            metadata: null!,
        };
        const metadata = <TransactionMetadata>mockOperationMetadata();
        when(metadataConverter.convert(rpcOperation, resultConverter)).thenReturn(metadata);

        const operation = act();

        const expected: TransactionNotification = {
            kind: OperationKind.transaction,
            source: 'sss',
            fee: new BigNumber(111),
            counter: new BigNumber(222),
            gas_limit: new BigNumber(333),
            storage_limit: new BigNumber(444),
            amount: new BigNumber(555),
            destination: 'ddd',
            parameters: {
                entrypoint: 'eee',
                value: { prim: 'michelson' },
            },
            ...mockBaseProperties(),
            metadata,
        };
        expect(operation).deep.equals(expected)
            .and.instanceof(TransactionNotification);
    });
});
