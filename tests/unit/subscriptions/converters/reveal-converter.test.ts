import * as rpc from '@taquito/rpc';
import { BigNumber } from 'bignumber.js';
import { expect } from 'chai';
import { instance, mock, when } from 'ts-mockito';

import { OperationKind } from '../../../../src/entity/subscriptions/operation-notification';
import { RevealMetadata, RevealNotification } from '../../../../src/entity/subscriptions/reveal-notification';
import { OperationMetadataConverter } from '../../../../src/subscriptions/converters/common/operation-metadata-converter';
import { RevealResultConverter } from '../../../../src/subscriptions/converters/operation-results/reveal-result-converter';
import { RevealConverter } from '../../../../src/subscriptions/converters/reveal-converter';
import { mockBaseProperties, mockOperationMetadata } from './mocks';

describe(RevealConverter.name, () => {
    let target: RevealConverter;
    let metadataConverter: OperationMetadataConverter;
    let resultConverter: RevealResultConverter;
    let rpcOperation: rpc.OperationContentsAndResultReveal;

    const act = () => target.convert(rpcOperation, mockBaseProperties());

    beforeEach(() => {
        metadataConverter = mock(OperationMetadataConverter);
        resultConverter = mock(RevealResultConverter);
        target = new RevealConverter(instance(metadataConverter), resultConverter);
    });

    it('should convert values correctly', () => {
        rpcOperation = {
            kind: rpc.OpKind.REVEAL,
            source: 'sss',
            fee: '111',
            counter: '222',
            gas_limit: '333',
            storage_limit: '444',
            public_key: 'kkk',
            metadata: null!,
        };
        const metadata = <RevealMetadata>mockOperationMetadata();
        when(metadataConverter.convert(rpcOperation, resultConverter)).thenReturn(metadata);

        const operation = act();

        const expected: RevealNotification = {
            kind: OperationKind.reveal,
            source: 'sss',
            fee: new BigNumber(111),
            counter: new BigNumber(222),
            gas_limit: new BigNumber(333),
            storage_limit: new BigNumber(444),
            public_key: 'kkk',
            ...mockBaseProperties(),
            metadata,
        };
        expect(operation).deep.equals(expected)
            .and.instanceof(RevealNotification);
    });
});
