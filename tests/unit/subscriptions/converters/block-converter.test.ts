import * as rpc from '@taquito/rpc';
import { expect } from 'chai';
import { anyString, anything, capture, instance, mock, verify, when } from 'ts-mockito';

import { OperationNotification } from '../../../../src/entity/subscriptions/operation-notification';
import { BlockConverter } from '../../../../src/subscriptions/converters/block-converter';
import { OperationConverter } from '../../../../src/subscriptions/converters/operation-converter';
import { MockedLogger, mockLogger } from '../../mocks';

describe(BlockConverter.name, () => {
    let target: BlockConverter;
    let operationConverter: OperationConverter;
    let logger: MockedLogger;

    beforeEach(() => {
        operationConverter = mock(OperationConverter);
        logger = mockLogger(BlockConverter);
        target = new BlockConverter(instance(operationConverter), logger.factory);
    });

    it('should convert values correctly', () => {
        const rpcOpInfo1 = { protocol: 'p1', chain_id: 'c1', hash: 'h1', branch: 'b1', signature: 's1' };
        const rpcOpInfo2 = { protocol: 'p2', chain_id: 'c2', hash: 'h2', branch: 'b2', signature: 's2' };
        const header = { level: 11, proto: 22, predecessor: 'rr', timestamp: 'tt', signature: 'ss' };
        const blockProps = { hash: 'hh', protocol: 'pp', chain_id: 'cc', header };

        const rpcOp1 = <rpc.OperationContentsAndResult>{ kind: rpc.OpKind.ENDORSEMENT };
        const rpcOp2 = <rpc.OperationContentsAndResult>{ kind: rpc.OpKind.TRANSACTION };
        const rpcOp3 = <rpc.OperationContentsAndResult>{ kind: rpc.OpKind.BALLOT };
        const rpcBlock = <rpc.BlockResponse>{
            ...blockProps,
            operations: [
                [{ ...rpcOpInfo1, contents: [rpcOp1, rpcOp2] }],
                [{ ...rpcOpInfo2, contents: [rpcOp3] }],
            ],
        };
        const op1 = <OperationNotification>{};
        const op2 = <OperationNotification>{};
        const op3 = <OperationNotification>{};
        when(operationConverter.convert(rpcOp1, anything())).thenReturn(op1);
        when(operationConverter.convert(rpcOp2, anything())).thenReturn(op2);
        when(operationConverter.convert(rpcOp3, anything())).thenReturn(op3);

        const block = target.convert(rpcBlock);

        expect(block).deep.equals({
            ...blockProps,
            operations: [op1, op2, op3],
        });
        verifyOp(0, rpcOpInfo1);
        verifyOp(1, rpcOpInfo1);
        verifyOp(2, rpcOpInfo2);

        function verifyOp(opIndex: number, info: any) {
            const baseProps = capture(operationConverter.convert).byCallIndex(opIndex)[1]; /* eslint-disable-line @typescript-eslint/unbound-method */
            expect(baseProps.info).deep.equals(info);
            expect(baseProps.block).equals(block);

            verify(logger.mock.debug(anyString()));
        }
    });
});
