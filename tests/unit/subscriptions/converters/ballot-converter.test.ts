import * as rpc from '@taquito/rpc';
import { expect } from 'chai';

import { BallotNotification, BallotVote } from '../../../../src/entity/subscriptions/ballot-notification';
import { OperationKind } from '../../../../src/entity/subscriptions/operation-notification';
import { BallotConverter } from '../../../../src/subscriptions/converters/ballot-converter';
import { mockBaseProperties } from './mocks';

describe(BallotConverter.name, () => {
    const target = new BallotConverter();
    let rpcOperation: rpc.OperationContentsAndResultBallot;

    const act = () => target.convert(rpcOperation, mockBaseProperties());

    it('should convert values correctly', () => {
        rpcOperation = {
            kind: rpc.OpKind.BALLOT,
            source: 'sss',
            period: 111,
            proposal: 'ppp',
            ballot: 'yay',
        };

        const operation = act();

        const expected: BallotNotification = {
            kind: OperationKind.ballot,
            source: 'sss',
            period: 111,
            proposal: 'ppp',
            ballot: BallotVote.yay,
            ...mockBaseProperties(),
        };
        expect(operation).deep.equals(expected)
            .and.instanceof(BallotNotification);
    });
});
