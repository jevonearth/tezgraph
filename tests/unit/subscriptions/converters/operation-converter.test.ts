import * as rpc from '@taquito/rpc';
import { expect } from 'chai';
import { instance, mock, when } from 'ts-mockito';

import { ActivateAccountConverter } from '../../../../src/subscriptions/converters/activate-account-converter';
import { BallotConverter } from '../../../../src/subscriptions/converters/ballot-converter';
import { BaseOperationProperties } from '../../../../src/subscriptions/converters/block-converter';
import { DelegationConverter } from '../../../../src/subscriptions/converters/delegation-converter';
import { DoubleBakingEvidenceConverter } from '../../../../src/subscriptions/converters/double-baking-evidence-converter';
import {
    DoubleEndorsementEvidenceConverter,
} from '../../../../src/subscriptions/converters/double-endorsement-evidence-converter';
import { EndorsementConverter } from '../../../../src/subscriptions/converters/endorsement-converter';
import { OperationConverter } from '../../../../src/subscriptions/converters/operation-converter';
import { OriginationConverter } from '../../../../src/subscriptions/converters/origination-converter';
import { ProposalsConverter } from '../../../../src/subscriptions/converters/proposals-converter';
import { RevealConverter } from '../../../../src/subscriptions/converters/reveal-converter';
import { SeedNonceRevelationConverter } from '../../../../src/subscriptions/converters/seed-nonce-revelation-converter';
import { TransactionConverter } from '../../../../src/subscriptions/converters/transaction-converter';
import { mockBaseProperties } from './mocks';

describe(OperationConverter.name, () => {
    let target: OperationConverter;
    let activateAccountConverter: ActivateAccountConverter;
    let ballotConverter: BallotConverter;
    let delegationConverter: DelegationConverter;
    let doubleBakingEvidenceConverter: DoubleBakingEvidenceConverter;
    let doubleEndorsementEvidenceConverter: DoubleEndorsementEvidenceConverter;
    let endorsementConverter: EndorsementConverter;
    let originationConverter: OriginationConverter;
    let proposalsConverter: ProposalsConverter;
    let revealConverter: RevealConverter;
    let seedNonceRevelationConverter: SeedNonceRevelationConverter;
    let transactionConverter: TransactionConverter;

    beforeEach(() => {
        activateAccountConverter = mock(ActivateAccountConverter);
        ballotConverter = mock(BallotConverter);
        delegationConverter = mock(DelegationConverter);
        doubleBakingEvidenceConverter = mock(DoubleBakingEvidenceConverter);
        doubleEndorsementEvidenceConverter = mock(DoubleEndorsementEvidenceConverter);
        endorsementConverter = mock(EndorsementConverter);
        originationConverter = mock(OriginationConverter);
        proposalsConverter = mock(ProposalsConverter);
        revealConverter = mock(RevealConverter);
        seedNonceRevelationConverter = mock(SeedNonceRevelationConverter);
        transactionConverter = mock(TransactionConverter);
        target = new OperationConverter(
            instance(activateAccountConverter),
            instance(ballotConverter),
            instance(delegationConverter),
            instance(doubleBakingEvidenceConverter),
            instance(doubleEndorsementEvidenceConverter),
            instance(endorsementConverter),
            instance(originationConverter),
            instance(proposalsConverter),
            instance(revealConverter),
            instance(seedNonceRevelationConverter),
            instance(transactionConverter),
        );
    });

    test(rpc.OpKind.ACTIVATION, () => activateAccountConverter);
    test(rpc.OpKind.BALLOT, () => ballotConverter);
    test(rpc.OpKind.DELEGATION, () => delegationConverter);
    test(rpc.OpKind.DOUBLE_BAKING_EVIDENCE, () => doubleBakingEvidenceConverter);
    test(rpc.OpKind.DOUBLE_ENDORSEMENT_EVIDENCE, () => doubleEndorsementEvidenceConverter);
    test(rpc.OpKind.ENDORSEMENT, () => endorsementConverter);
    test(rpc.OpKind.ORIGINATION, () => originationConverter);
    test(rpc.OpKind.PROPOSALS, () => proposalsConverter);
    test(rpc.OpKind.REVEAL, () => revealConverter);
    test(rpc.OpKind.SEED_NONCE_REVELATION, () => seedNonceRevelationConverter);
    test(rpc.OpKind.TRANSACTION, () => transactionConverter);

    function test<TRpc extends rpc.OperationContentsAndResult, TOperation>(
        kind: rpc.OpKind,
        getConverter: () => { convert(r: TRpc, b: BaseOperationProperties): TOperation },
    ) {
        it(`should delegate to correct converter if ${kind} operation`, () => {
            const rpcOperation = <TRpc>{ kind };
            const convertedResult = <TOperation>{};
            const baseProps = mockBaseProperties();
            when(getConverter().convert(rpcOperation, baseProps)).thenReturn(convertedResult);

            const result = target.convert(rpcOperation, baseProps);

            expect(result).equals(convertedResult);
        });
    }
});
