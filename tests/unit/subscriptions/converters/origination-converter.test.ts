import * as rpc from '@taquito/rpc';
import { BigNumber } from 'bignumber.js';
import { expect } from 'chai';
import { instance, mock, when } from 'ts-mockito';

import { OperationKind } from '../../../../src/entity/subscriptions/operation-notification';
import { OriginationMetadata, OriginationNotification } from '../../../../src/entity/subscriptions/origination-notification';
import { OperationMetadataConverter } from '../../../../src/subscriptions/converters/common/operation-metadata-converter';
import {
    OriginationResultConverter,
} from '../../../../src/subscriptions/converters/operation-results/origination-result-converter';
import { OriginationConverter } from '../../../../src/subscriptions/converters/origination-converter';
import { mockBaseProperties, mockOperationMetadata } from './mocks';

describe(OriginationConverter.name, () => {
    let target: OriginationConverter;
    let metadataConverter: OperationMetadataConverter;
    let resultConverter: OriginationResultConverter;
    let rpcOperation: rpc.OperationContentsAndResultOrigination;

    const act = () => target.convert(rpcOperation, mockBaseProperties());

    beforeEach(() => {
        metadataConverter = mock(OperationMetadataConverter);
        resultConverter = mock(OriginationResultConverter);
        target = new OriginationConverter(instance(metadataConverter), resultConverter);
    });

    it('should convert values correctly', () => {
        rpcOperation = {
            kind: rpc.OpKind.ORIGINATION,
            source: 'sss',
            fee: '111',
            counter: '222',
            gas_limit: '333',
            storage_limit: '444',
            balance: '555',
            delegate: 'ddd',
            script: {
                code: [],
                storage: { prim: 'michelson-script' },
            },
            metadata: null!,
        };
        const metadata = <OriginationMetadata>mockOperationMetadata();
        when(metadataConverter.convert(rpcOperation, resultConverter)).thenReturn(metadata);

        const operation = act();

        const expected: OriginationNotification = {
            kind: OperationKind.origination,
            source: 'sss',
            fee: new BigNumber(111),
            counter: new BigNumber(222),
            gas_limit: new BigNumber(333),
            storage_limit: new BigNumber(444),
            balance: new BigNumber(555),
            delegate: 'ddd',
            script: {
                code: [],
                storage: { prim: 'michelson-script' },
            },
            ...mockBaseProperties(),
            metadata,
        };
        expect(operation).deep.equals(expected)
            .and.instanceof(OriginationNotification);
    });
});
