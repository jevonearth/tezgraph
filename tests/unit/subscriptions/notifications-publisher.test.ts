import { BlockResponse } from '@taquito/rpc';
import { instance, mock, verify, when } from 'ts-mockito';

import { BlockNotification } from '../../../src/entity/subscriptions/block-notification';
import { OperationKind, OperationNotification } from '../../../src/entity/subscriptions/operation-notification';
import { RpcMonitorBlockHeader } from '../../../src/rpc/rpc-monitor-block-header';
import { TezosRpcClient } from '../../../src/rpc/tezos-rpc-client';
import { BlockConverter } from '../../../src/subscriptions/converters/block-converter';
import { NotificationsPublisher } from '../../../src/subscriptions/notifications-publisher';
import { Triggers, TypedPubSub } from '../../../src/subscriptions/typed-pub-sub';
import { asReadonly } from '../../../src/utils/conversion';
import { MockedLogger, mockLogger } from '../mocks';

describe(NotificationsPublisher.name, () => {
    let target: NotificationsPublisher;
    let rpcClient: TezosRpcClient;
    let pubSub: TypedPubSub;
    let blockConverter: BlockConverter;
    let logger: MockedLogger;

    beforeEach(() => {
        rpcClient = mock(TezosRpcClient);
        pubSub = mock(TypedPubSub);
        blockConverter = mock(BlockConverter);
        logger = mockLogger(NotificationsPublisher);
        target = new NotificationsPublisher(instance(rpcClient), instance(pubSub), instance(blockConverter), logger.factory);
    });

    it('should publish converted block and operations from a new RPC block', async () => {
        const rpcBlock = <BlockResponse>{ hash: 'rpc-hash' };
        const operation1 = <OperationNotification>{ kind: OperationKind.endorsement };
        const operation2 = <OperationNotification>{ kind: OperationKind.transaction };
        const block = <BlockNotification>{ operations: asReadonly([operation1, operation2]) };

        let onIteratorFinished: () => void;
        const iteratorFinished = new Promise<void>(r => {
            onIteratorFinished = r;
        });

        when(pubSub.iterate(Triggers.monitorBlockHeaders)).thenReturn(mockIterator([<RpcMonitorBlockHeader>{ hash: 'monitor-hash' }]));
        when(rpcClient.getBlock('monitor-hash')).thenResolve(rpcBlock);
        when(blockConverter.convert(rpcBlock)).thenReturn(block);

        target.start();
        await iteratorFinished;

        verify(pubSub.publish(Triggers.blocks, block)).once();
        verify(pubSub.publish(Triggers.operations[OperationKind.endorsement], operation1)).once();
        verify(pubSub.publish(Triggers.operations[OperationKind.transaction], operation2)).once();

        async function *mockIterator(items: RpcMonitorBlockHeader[]): AsyncIterator<RpcMonitorBlockHeader> {
            await Promise.resolve();
            yield* items;
            onIteratorFinished();
        }
    });
});
