import { expect } from 'chai';
import { PubSub } from 'graphql-subscriptions';
import { anyString, deepEqual, instance, mock, verify, when } from 'ts-mockito';

import { BlockNotification } from '../../../src/entity/subscriptions/block-notification';
import { OperationKind } from '../../../src/entity/subscriptions/operation-notification';
import { Triggers, TypedPubSub } from '../../../src/subscriptions/typed-pub-sub';
import { nameof } from '../../../src/utils/reflection';
import { MockedLogger, mockLogger } from '../mocks';

describe('Triggers', () => {
    testTrigger('monitorBlockHeaders', 'MONITOR_BLOCK_HEADERS');
    testTrigger('blocks', 'BLOCKS');

    function testTrigger(triggerKey: keyof typeof Triggers, expectedName: string) {
        it(`${triggerKey} should have correct name ${expectedName}`, () => {
            const trigger: any = Triggers[triggerKey];

            expect(trigger.name).equals(expectedName);
        });
    }

    describe(nameof<typeof Triggers>('operations'), () => {
        testOperations(OperationKind.ballot, 'OPERATIONS:ballot');
        testOperations(OperationKind.transaction, 'OPERATIONS:transaction');

        function testOperations(kind: OperationKind, expectedName: string) {
            it(`should expose correct name ${expectedName} for ${kind} kind`, () => {
                const trigger = Triggers.operations[kind];

                expect(trigger.name).equals(expectedName);
            });
        }
    });
});

describe(TypedPubSub.name, () => {
    let target: TypedPubSub;
    let pubSub: PubSub;
    let logger: MockedLogger;
    const trigger = Triggers.blocks;

    beforeEach(() => {
        pubSub = mock(PubSub);
        logger = mockLogger(TypedPubSub);
        target = new TypedPubSub(instance(pubSub), logger.factory);
    });

    it(`${nameof<TypedPubSub>('publish')}() should publish given payload for given trigger`, () => {
        const payload = <BlockNotification>{ hash: 'hhh' };

        target.publish(trigger, payload);

        verify(pubSub.publish(trigger.name, payload)).once();
        verify(logger.mock.debug(anyString(), deepEqual({ payload }))).once();
    });

    it(`${nameof<TypedPubSub>('subscribe')}() should subscribe to given trigger`, () => {
        const handler = () => { /* Nothing*/ };

        target.subscribe(trigger, handler);

        verify(pubSub.subscribe(trigger.name, handler)).once();
    });

    it(`${nameof<TypedPubSub>('iterate')}() should iterate payloads for given trigger`, () => {
        const testIterator = <AsyncIterator<BlockNotification>>{};
        when(pubSub.asyncIterator(trigger.name)).thenReturn(testIterator);

        const iterator = target.iterate(trigger);

        expect(iterator).equals(testIterator);
    });
});
