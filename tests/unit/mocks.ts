import { instance, mock, when } from 'ts-mockito';

import { Logger, LoggerFactory } from '../../src/utils/logger';
import { Constructor } from '../../src/utils/reflection';

export interface MockedLogger {
    mock: Logger;
    factory: LoggerFactory;
}

export function mockLogger(categoryType: Constructor<any>): MockedLogger {
    const logger = mock(Logger);
    const factory = mock(LoggerFactory);

    when(factory.get(categoryType)).thenReturn(instance(logger));

    return {
        mock: logger,
        factory: instance(factory),
    };
}
