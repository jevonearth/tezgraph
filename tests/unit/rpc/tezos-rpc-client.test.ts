import { RpcClient } from '@taquito/rpc';
import { expect } from 'chai';
import { anyString, deepEqual, instance, mock, verify, when } from 'ts-mockito';

import { RpcRetryHelper } from '../../../src/rpc/rpc-retry-helper';
import { TezosRpcClient } from '../../../src/rpc/tezos-rpc-client';
import { nameof } from '../../../src/utils/reflection';
import { MockedLogger, mockLogger } from '../mocks';

describe(TezosRpcClient.name, () => {
    let target: TezosRpcClient;
    let rpcClient: RpcClient;
    let logger: MockedLogger;

    beforeEach(() => {
        rpcClient = mock(RpcClient);
        logger = mockLogger(TezosRpcClient);
        const retryHelper = <RpcRetryHelper>{};
        target = new TezosRpcClient(instance(rpcClient), retryHelper, logger.factory);

        retryHelper.retry = async (func: () => Promise<any>) => {
            const result = await func();
            return Object.assign(result ?? {}, { wasRetried: true });
        };
    });

    it(`${nameof<TezosRpcClient>('getBlock')}() should call RpcClient with retry`, async () => {
        when(rpcClient.getBlock(deepEqual({ block: 'haha' }))).thenResolve(<any>{ blockResponse: 'bbb' });

        const block = await target.getBlock('haha');

        expect(block).deep.equals({
            blockResponse: 'bbb',
            wasRetried: true,
        });
        verify(logger.mock.debug(anyString())).once();
    });

    it(`${nameof<TezosRpcClient>('getHeadBlockHeader')}() should call RpcClient with retry`, async () => {
        when(rpcClient.getBlockHeader()).thenResolve(<any>{ headerResponse: 'hhh' });

        const header = await target.getHeadBlockHeader();

        expect(header).deep.equals({
            headerResponse: 'hhh',
            wasRetried: true,
        });
        verify(logger.mock.debug(anyString())).once();
    });
});
