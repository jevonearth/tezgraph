import { anything, instance, mock, verify, when } from 'ts-mockito';

import { RpcRetryHelper } from '../../../src/rpc/rpc-retry-helper';
import { EnvConfig } from '../../../src/utils/env-config';
import { expect } from '../../chai-with-plugins';

interface Foo {
    bar(): Promise<string>;
    sleep(millis: number): Promise<void>;
}

describe(RpcRetryHelper.name, () => {
    let target: RpcRetryHelper;
    let foo: Foo;

    const act = async () => target.retry(async () => instance(foo).bar());

    beforeEach(() => {
        const config = mock(EnvConfig);
        target = new RpcRetryHelper(instance(config));

        foo = mock<Foo>();
        when(config.rpcRetryDelaysMillis).thenReturn([1, 2]);

        when(foo.sleep(anything())).thenResolve();
        (<any>target).sleep = async (t: number) => instance(foo).sleep(t);
    });

    it('should call function once if no failure', async () => {
        when(foo.bar()).thenResolve('lol');

        const result = await act();

        expect(result).equal('lol');
        verify(foo.bar()).once();
        verify(foo.sleep(anything())).never();
    });

    it('should retry with configured delays', async () => {
        when(foo.bar())
            .thenReject(new Error('First failure.'))
            .thenReject(new Error('Second failure.'))
            .thenResolve('omg');

        const result = await act();

        expect(result).equal('omg');
        verify(foo.bar()).times(3);
        verify(foo.sleep(anything())).times(2);
        verify(foo.sleep(1)).calledBefore(foo.sleep(2));
    });

    it('should fail if more failures then configred retries', () => {
        when(foo.bar())
            .thenReject(new Error('First failure.'))
            .thenReject(new Error('Second failure.'))
            .thenReject(new Error('Final failure.'));

        expect(act()).rejectedWith('Final failure.');
    });
});
