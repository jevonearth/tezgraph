import 'reflect-metadata';

import { expect } from 'chai';

import { ActivateAccountNotification } from '../../../../../src/entity/subscriptions/activate-account-notification';
import { ActivateAccountArgs } from '../../../../../src/entity/subscriptions/args/activate-account-args';
import { AddressFilter } from '../../../../../src/entity/subscriptions/filters/address-filters';
import { nameof } from '../../../../../src/utils/reflection';
import { filterStates, mockFilter, shouldPass } from './mocks';

describe(`${ActivateAccountArgs.name}.${nameof<ActivateAccountArgs>('passesInternal')}()`, () => {
    for (const pkhState of filterStates) {
        const expectedPassed = shouldPass(pkhState);

        it(`should return ${expectedPassed} if filters are pkh=${pkhState}`, () => {
            const operation = <ActivateAccountNotification>{
                pkh: 'ppp',
            };
            const target = new ActivateAccountArgs();
            target.pkh = mockFilter(AddressFilter, pkhState, operation.pkh);

            const passed = target.passesInternal(operation);

            expect(passed).equals(expectedPassed);
        });
    }
});
