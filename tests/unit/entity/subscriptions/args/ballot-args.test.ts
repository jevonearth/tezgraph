import { expect } from 'chai';

import { BallotArgs, BallotVoteFilter } from '../../../../../src/entity/subscriptions/args/ballot-args';
import { BallotNotification } from '../../../../../src/entity/subscriptions/ballot-notification';
import { AddressFilter } from '../../../../../src/entity/subscriptions/filters/address-filters';
import { ProtocolHashFilter } from '../../../../../src/entity/subscriptions/filters/hash-filters';
import { nameof } from '../../../../../src/utils/reflection';
import { filterStates, mockFilter, shouldPass } from './mocks';

describe(`${BallotArgs.name}.${nameof<BallotArgs>('passesInternal')}()`, () => {
    for (const sourceState of filterStates) {
        for (const proposalState of filterStates) {
            for (const ballotState of filterStates) {
                const expectedPassed = shouldPass(sourceState, proposalState, ballotState);

                it(`should return ${expectedPassed} if filters are source=${sourceState}, proposal=${proposalState}, ballot=${ballotState}`, () => {
                    const operation = <BallotNotification>{
                        source: 'sss',
                        proposal: 'ppp',
                        ballot: 'nay',
                    };
                    const target = new BallotArgs();
                    target.source = mockFilter(AddressFilter, sourceState, operation.source);
                    target.proposal = mockFilter(ProtocolHashFilter, proposalState, operation.proposal);
                    target.ballot = mockFilter(BallotVoteFilter, ballotState, operation.ballot);

                    const passed = target.passesInternal(operation);

                    expect(passed).equals(expectedPassed);
                });
            }
        }
    }
});
