import { expect } from 'chai';

import { OperationArgs } from '../../../../../src/entity/subscriptions/args/operation-args';
import {
    BlockHashFilter,
    OperationHashFilter,
    ProtocolHashFilter,
} from '../../../../../src/entity/subscriptions/filters/hash-filters';
import { RevealNotification } from '../../../../../src/entity/subscriptions/reveal-notification';
import { nameof } from '../../../../../src/utils/reflection';
import { FilterState, filterStates, mockFilter, shouldPass } from './mocks';

class TestArgs extends OperationArgs<RevealNotification> {
    passesInternal!: (operation: RevealNotification) => boolean;
}

describe(`${OperationArgs.name}.${nameof<OperationArgs<any>>('passes')}()`, () => {
    for (const hashState of filterStates) {
        for (const protocolState of filterStates) {
            for (const branchState of filterStates) {
                for (const passesInternalState of [FilterState.Passed, FilterState.NotPassed]) {
                    const expectedPassed = shouldPass(hashState, protocolState, passesInternalState);

                    it(`should return ${expectedPassed} if filters are hash=${hashState}, protocol=${protocolState},`
                        + ` branch=${branchState}, passesInternal()=${passesInternalState}`, () => {
                        const operation = <RevealNotification>{
                            info: {
                                hash: 'hhh',
                                protocol: 'ppp',
                                branch: 'bbb',
                            },
                        };
                        const target = new TestArgs();
                        target.hash = mockFilter(OperationHashFilter, hashState, operation.info.hash);
                        target.protocol = mockFilter(ProtocolHashFilter, protocolState, operation.info.protocol);
                        target.branch = mockFilter(BlockHashFilter, passesInternalState, operation.info.branch);

                        let receivedOp: { value: RevealNotification } | undefined;
                        target.passesInternal = op => {
                            receivedOp = { value: op };
                            return passesInternalState === FilterState.Passed;
                        };

                        const passed = target.passes(operation);

                        expect(passed).equals(expectedPassed);
                        if (receivedOp) {
                            expect(receivedOp.value).equals(operation);
                        }
                    });
                }
            }
        }
    }
});
