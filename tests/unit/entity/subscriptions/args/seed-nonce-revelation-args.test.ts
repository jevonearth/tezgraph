import { expect } from 'chai';

import { SeedNonceRevelationArgs } from '../../../../../src/entity/subscriptions/args/seed-nonce-revelation-args';
import { nameof } from '../../../../../src/utils/reflection';

describe(`${SeedNonceRevelationArgs.name}.${nameof<SeedNonceRevelationArgs>('passesInternal')}()`, () => {
    it('should return true', () => {
        const target = new SeedNonceRevelationArgs();

        const passed = target.passesInternal();

        expect(passed).equals(true);
    });
});
