import { expect } from 'chai';

import { RevealArgs } from '../../../../../src/entity/subscriptions/args/reveal-args';
import { AddressFilter } from '../../../../../src/entity/subscriptions/filters/address-filters';
import { OperationResultStatusFilter } from '../../../../../src/entity/subscriptions/filters/operation-result-status-filter';
import { OperationResultStatus } from '../../../../../src/entity/subscriptions/operation-result';
import { RevealNotification } from '../../../../../src/entity/subscriptions/reveal-notification';
import { nameof } from '../../../../../src/utils/reflection';
import { filterStates, mockFilter, shouldPass } from './mocks';

describe(`${RevealArgs.name}.${nameof<RevealArgs>('passesInternal')}()`, () => {
    for (const sourceState of filterStates) {
        for (const statusState of filterStates) {
            const expectedPassed = shouldPass(sourceState, statusState);

            it(`should return ${expectedPassed} if filters are source=${sourceState}, status=${statusState}`, () => {
                const operation = <RevealNotification>{
                    source: 'sss',
                    metadata: { operation_result: { status: OperationResultStatus.backtracked } },
                };
                const target = new RevealArgs();
                target.source = mockFilter(AddressFilter, sourceState, operation.source);
                target.status = mockFilter(OperationResultStatusFilter, statusState, operation.metadata.operation_result.status);

                const passed = target.passesInternal(operation);

                expect(passed).equals(expectedPassed);
            });
        }
    }
});
