import { expect } from 'chai';

import { DelegationArgs } from '../../../../../src/entity/subscriptions/args/delegation-args';
import { DelegationNotification } from '../../../../../src/entity/subscriptions/delegation-notification';
import { AddressFilter, NullableAddressFilter } from '../../../../../src/entity/subscriptions/filters/address-filters';
import { OperationResultStatusFilter } from '../../../../../src/entity/subscriptions/filters/operation-result-status-filter';
import { OperationResultStatus } from '../../../../../src/entity/subscriptions/operation-result';
import { nameof } from '../../../../../src/utils/reflection';
import { filterStates, mockFilter, shouldPass } from './mocks';

describe(`${DelegationArgs.name}.${nameof<DelegationArgs>('passesInternal')}()`, () => {
    for (const sourceState of filterStates) {
        for (const delegateState of filterStates) {
            for (const statusState of filterStates) {
                const expectedPassed = shouldPass(sourceState, delegateState, statusState);

                it(`should return ${expectedPassed} if filters are source=${sourceState}, delegate=${delegateState}, status=${statusState}`, () => {
                    const operation = <DelegationNotification>{
                        source: 'sss',
                        delegate: 'ddd',
                        metadata: { operation_result: { status: OperationResultStatus.backtracked } },
                    };
                    const target = new DelegationArgs();
                    target.source = mockFilter(AddressFilter, sourceState, operation.source);
                    target.delegate = mockFilter(NullableAddressFilter, delegateState, operation.delegate);
                    target.status = mockFilter(OperationResultStatusFilter, statusState, operation.metadata.operation_result.status);

                    const passed = target.passesInternal(operation);

                    expect(passed).equals(expectedPassed);
                });
            }
        }
    }
});
