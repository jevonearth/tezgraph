import { expect } from 'chai';

import { TransactionArgs } from '../../../../../src/entity/subscriptions/args/transaction-args';
import { AddressFilter } from '../../../../../src/entity/subscriptions/filters/address-filters';
import { OperationResultStatusFilter } from '../../../../../src/entity/subscriptions/filters/operation-result-status-filter';
import { OperationResultStatus } from '../../../../../src/entity/subscriptions/operation-result';
import { TransactionNotification } from '../../../../../src/entity/subscriptions/transaction-notification';
import { nameof } from '../../../../../src/utils/reflection';
import { filterStates, mockFilter, shouldPass } from './mocks';

describe(`${TransactionArgs.name}.${nameof<TransactionArgs>('passesInternal')}()`, () => {
    for (const sourceState of filterStates) {
        for (const destinationState of filterStates) {
            for (const statusState of filterStates) {
                const expectedPassed = shouldPass(sourceState, destinationState, statusState);

                it(`should return ${expectedPassed} if filters are source=${sourceState},`
                    + ` destination=${destinationState}, status=${statusState}`, () => {
                    const operation = <TransactionNotification>{
                        source: 'sss',
                        destination: 'ddd',
                        metadata: { operation_result: { status: OperationResultStatus.backtracked } },
                    };
                    const target = new TransactionArgs();
                    target.source = mockFilter(AddressFilter, sourceState, operation.source);
                    target.destination = mockFilter(AddressFilter, destinationState, operation.destination);
                    target.status = mockFilter(OperationResultStatusFilter, statusState, operation.metadata.operation_result.status);

                    const passed = target.passesInternal(operation);

                    expect(passed).equals(expectedPassed);
                });
            }
        }
    }
});
