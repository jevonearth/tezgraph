import { deepEqual, instance, mock, when } from 'ts-mockito';

import { Filter } from '../../../../../src/entity/subscriptions/filters/filter';
import { Constructor } from '../../../../../src/utils/reflection';

export enum FilterState {
    Undefined = 'Undefined',
    Passed = 'Passed',
    NotPassed = 'NotPassed',
}

export const filterStates: readonly FilterState[] = [FilterState.Undefined, FilterState.Passed, FilterState.NotPassed];

export function shouldPass(...states: FilterState[]): boolean {
    return !states.some(s => s === FilterState.NotPassed);
}

export function mockFilter<TValue, TFilter extends Filter<TValue>>(
    filterType: Constructor<TFilter>,
    state: FilterState,
    expectedValue: TValue,
): TFilter | undefined {
    if (state === FilterState.Undefined) {
        return undefined;
    }
    const filter = mock<TFilter>(filterType);
    when(filter.passes(deepEqual(expectedValue))).thenReturn(state === FilterState.Passed);
    return instance(filter);
}
