import { expect } from 'chai';

import {
    DoubleEndorsementEvidenceArgs,
} from '../../../../../src/entity/subscriptions/args/double-endorsement-evidence-args';
import {
    DoubleEndorsementEvidenceNotification,
} from '../../../../../src/entity/subscriptions/double-endorsement-evidence-notification';
import { AddressArrayFilter } from '../../../../../src/entity/subscriptions/filters/address-filters';
import { asReadonly } from '../../../../../src/utils/conversion';
import { nameof } from '../../../../../src/utils/reflection';
import { filterStates, mockFilter, shouldPass } from './mocks';

describe(`${DoubleEndorsementEvidenceArgs.name}.${nameof<DoubleEndorsementEvidenceArgs>('passesInternal')}()`, () => {
    for (const delegateState of filterStates) {
        const expectedPassed = shouldPass(delegateState);

        it(`should return ${expectedPassed} if filters are delegate=${delegateState}`, () => {
            const operation = <DoubleEndorsementEvidenceNotification>{
                metadata: {
                    balance_updates: asReadonly([
                        { delegate: 'd1' },
                        { delegate: null },
                        {},
                        { delegate: 'd2' },
                    ]),
                },
            };
            const target = new DoubleEndorsementEvidenceArgs();
            target.delegate = mockFilter(AddressArrayFilter, delegateState, ['d1', 'd2']);

            const passed = target.passesInternal(operation);

            expect(passed).equals(expectedPassed);
        });
    }
});
