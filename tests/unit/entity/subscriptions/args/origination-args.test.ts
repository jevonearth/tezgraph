import { expect } from 'chai';

import { OriginationArgs } from '../../../../../src/entity/subscriptions/args/origination-args';
import {
    AddressFilter,
    NullableAddressArrayFilter,
    NullableAddressFilter,
} from '../../../../../src/entity/subscriptions/filters/address-filters';
import { OperationResultStatusFilter } from '../../../../../src/entity/subscriptions/filters/operation-result-status-filter';
import { OperationResultStatus } from '../../../../../src/entity/subscriptions/operation-result';
import { OriginationNotification } from '../../../../../src/entity/subscriptions/origination-notification';
import { asReadonly } from '../../../../../src/utils/conversion';
import { nameof } from '../../../../../src/utils/reflection';
import { filterStates, mockFilter, shouldPass } from './mocks';

describe(`${OriginationArgs.name}.${nameof<OriginationArgs>('passesInternal')}()`, () => {
    for (const sourceState of filterStates) {
        for (const delegateState of filterStates) {
            for (const originated_contractState of filterStates) {
                for (const statusState of filterStates) {
                    const expectedPassed = shouldPass(sourceState, delegateState, originated_contractState, statusState);

                    it(`should return ${expectedPassed} if filters are source=${sourceState}, delegate=${delegateState},`
                        + ` originated_contract=${originated_contractState}, status=${statusState}`, () => {
                        const contracts = asReadonly(['c1', 'c2']);
                        const operation = <OriginationNotification>{
                            source: 'sss',
                            delegate: 'ddd',
                            metadata: {
                                operation_result: {
                                    originated_contracts: contracts,
                                    status: OperationResultStatus.backtracked,
                                },
                            },
                        };
                        const target = new OriginationArgs();
                        target.source = mockFilter(AddressFilter, sourceState, operation.source);
                        target.delegate = mockFilter(NullableAddressFilter, delegateState, operation.delegate);
                        target.originated_contract = mockFilter(NullableAddressArrayFilter, originated_contractState, contracts);
                        target.status = mockFilter(OperationResultStatusFilter, statusState, operation.metadata.operation_result.status);

                        const passed = target.passesInternal(operation);

                        expect(passed).equals(expectedPassed);
                    });
                }
            }
        }
    }
});
