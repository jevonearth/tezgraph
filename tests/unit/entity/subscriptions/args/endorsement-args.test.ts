import { expect } from 'chai';

import { EndorsementArgs } from '../../../../../src/entity/subscriptions/args/endorsement-args';
import { EndorsementNotification } from '../../../../../src/entity/subscriptions/endorsement-notification';
import { AddressFilter } from '../../../../../src/entity/subscriptions/filters/address-filters';
import { nameof } from '../../../../../src/utils/reflection';
import { filterStates, mockFilter, shouldPass } from './mocks';

describe(`${EndorsementArgs.name}.${nameof<EndorsementArgs>('passes')}()`, () => {
    for (const delegateState of filterStates) {
        const expectedPassed = shouldPass(delegateState);

        it(`should return ${expectedPassed} if filters are delegate=${delegateState}`, () => {
            const operation = <EndorsementNotification>{
                metadata: { delegate: 'ddd' },
            };
            const target = new EndorsementArgs();
            target.delegate = mockFilter(AddressFilter, delegateState, operation.metadata.delegate);

            const passed = target.passesInternal(operation);

            expect(passed).equals(expectedPassed);
        });
    }
});
