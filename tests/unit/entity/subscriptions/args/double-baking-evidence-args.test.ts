import { expect } from 'chai';

import { DoubleBakingEvidenceArgs } from '../../../../../src/entity/subscriptions/args/double-baking-evidence-args';
import {
    DoubleBakingEvidenceNotification,
} from '../../../../../src/entity/subscriptions/double-baking-evidence-notification';
import { AddressArrayFilter } from '../../../../../src/entity/subscriptions/filters/address-filters';
import { asReadonly } from '../../../../../src/utils/conversion';
import { nameof } from '../../../../../src/utils/reflection';
import { filterStates, mockFilter, shouldPass } from './mocks';

describe(`${DoubleBakingEvidenceArgs.name}.${nameof<DoubleBakingEvidenceArgs>('passesInternal')}()`, () => {
    for (const delegateState of filterStates) {
        const expectedPassed = shouldPass(delegateState);

        it(`should return ${expectedPassed} if filters are delegate=${delegateState}`, () => {
            const operation = <DoubleBakingEvidenceNotification>{
                metadata: {
                    balance_updates: asReadonly([
                        { delegate: 'd1' },
                        { delegate: null },
                        {},
                        { delegate: 'd2' },
                    ]),
                },
            };
            const target = new DoubleBakingEvidenceArgs();
            target.delegate = mockFilter(AddressArrayFilter, delegateState, ['d1', 'd2']);

            const passed = target.passesInternal(operation);

            expect(passed).equals(expectedPassed);
        });
    }
});
