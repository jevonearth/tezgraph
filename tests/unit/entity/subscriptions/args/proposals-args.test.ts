import { expect } from 'chai';

import { ProposalsArgs } from '../../../../../src/entity/subscriptions/args/proposals-args';
import { AddressFilter } from '../../../../../src/entity/subscriptions/filters/address-filters';
import { NullableProtocolHashArrayFilter } from '../../../../../src/entity/subscriptions/filters/hash-filters';
import { ProposalsNotification } from '../../../../../src/entity/subscriptions/proposals-notification';
import { asReadonly } from '../../../../../src/utils/conversion';
import { nameof } from '../../../../../src/utils/reflection';
import { filterStates, mockFilter, shouldPass } from './mocks';

describe(`${ProposalsArgs.name}.${nameof<ProposalsArgs>('passesInternal')}()`, () => {
    for (const sourceState of filterStates) {
        for (const proposalsState of filterStates) {
            const expectedPassed = shouldPass(sourceState, proposalsState);

            it(`should return ${expectedPassed} if filters are source=${sourceState}, proposals=${proposalsState}`, () => {
                const operation = <ProposalsNotification>{
                    source: 'sss',
                    proposals: asReadonly(['p1', 'p2']),
                };
                const target = new ProposalsArgs();
                target.source = mockFilter(AddressFilter, sourceState, operation.source);
                target.proposals = mockFilter(NullableProtocolHashArrayFilter, proposalsState, operation.proposals);

                const passed = target.passesInternal(operation);

                expect(passed).equals(expectedPassed);
            });
        }
    }
});
