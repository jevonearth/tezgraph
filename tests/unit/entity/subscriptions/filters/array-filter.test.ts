import { expect } from 'chai';

import { ArrayFilter } from '../../../../../src/entity/subscriptions/filters/array-filter';
import { nameof } from '../../../../../src/utils/reflection';

class TargetFilter extends ArrayFilter<string>(String) {}

describe(`${ArrayFilter.name}.${nameof<TargetFilter>('passes')}()`, () => {
    const tests = [
        {
            value: ['tz1X7fu4GXBXp9A8fchu1px3zzMDKtagDBk3'],
            filter: undefined,
            expected: true,
            condition: 'filter is undefined',
        }, {
            value: ['tz1X7fu4GXBXp9A8fchu1px3zzMDKtagDBk3'],
            filter: {},
            expected: true,
            condition: 'filter is empty',
        }, {
            value: undefined,
            filter: {},
            expected: true,
            condition: 'value is undefined without filter',
        }, {
            value: ['tz1J8fu4GXBXp9A8fchu1px3zzMDKtagDDi6', 'tz2AX9pq2oia17L3cfNhUHugM3SjPhjW3rJd'],
            filter: { includes: 'tz2AX9pq2oia17L3cfNhUHugM3SjPhjW3rJd' },
            expected: true,
            condition: '"includes" matches',
        }, {
            value: ['tz1J8fu4GXBXp9A8fchu1px3zzMDKtagDDi6', 'tz2AX9pq2oia17L3cfNhUHugM3SjPhjW3rJd'],
            filter: { includes: 'tz3SYyWM9sq9eWTxiA8KHb36SAieVYQPeZZm' },
            expected: false,
            condition: '"includes" does not match',
        }, {
            value: undefined,
            filter: { includes: 'tz3SYyWM9sq9eWTxiA8KHb36SAieVYQPeZZm' },
            expected: false,
            condition: '"includes" does not match because value is undefined',
        }, {
            value: null,
            filter: { includes: 'tz3SYyWM9sq9eWTxiA8KHb36SAieVYQPeZZm' },
            expected: false,
            condition: '"includes" does not match because value is undefined',
        }, {
            value: ['tz1J8fu4GXBXp9A8fchu1px3zzMDKtagDDi6', 'tz2AX9pq2oia17L3cfNhUHugM3SjPhjW3rJd'],
            filter: { notIncludes: 'tz3SYyWM9sq9eWTxiA8KHb36SAieVYQPeZZm' },
            expected: true,
            condition: '"notIncludes" matches',
        }, {
            value: ['tz1J8fu4GXBXp9A8fchu1px3zzMDKtagDDi6', 'tz2AX9pq2oia17L3cfNhUHugM3SjPhjW3rJd'],
            filter: { notIncludes: 'tz2AX9pq2oia17L3cfNhUHugM3SjPhjW3rJd' },
            expected: false,
            condition: '"notIncludes" does not match',
        }, {
            value: undefined,
            filter: { notIncludes: 'tz3SYyWM9sq9eWTxiA8KHb36SAieVYQPeZZm' },
            expected: true,
            condition: '"notIncludes" matches because value is undefined',
        }, {
            value: null,
            filter: { notIncludes: 'tz3SYyWM9sq9eWTxiA8KHb36SAieVYQPeZZm' },
            expected: true,
            condition: '"notIncludes" matches because value is null',
        }, {
            value: ['tz1J8fu4GXBXp9A8fchu1px3zzMDKtagDDi6', 'tz2AX9pq2oia17L3cfNhUHugM3SjPhjW3rJd'],
            filter: {
                includes: 'tz2AX9pq2oia17L3cfNhUHugM3SjPhjW3rJd',
                notIncludes: 'tz3SYyWM9sq9eWTxiA8KHb36SAieVYQPeZZm',
            },
            expected: true,
            condition: 'combined filters match',
        }, {
            value: ['tz1J8fu4GXBXp9A8fchu1px3zzMDKtagDDi6', 'tz2AX9pq2oia17L3cfNhUHugM3SjPhjW3rJd'],
            filter: {
                includes: 'tz3SYyWM9sq9eWTxiA8KHb36SAieVYQPeZZm',
                notIncludes: 'tz1J8fu4GXBXp9A8fchu1px3zzMDKtagDDi6',
            },
            expected: false,
            condition: 'combined filters do not match',
        },
    ];

    for (const test of tests) {
        it(`${nameof<TargetFilter>('passes')}() should return ${test.expected} if ${test.condition}`, () => {
            const target = Object.assign(new TargetFilter(), test.filter);

            const actual = target.passes(test.value!);

            expect(actual).equals(test.expected);
        });
    }
});
