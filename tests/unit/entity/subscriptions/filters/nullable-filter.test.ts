import { expect } from 'chai';

import { Filter } from '../../../../../src/entity/subscriptions/filters/filter';
import { Nullable } from '../../../../../src/entity/subscriptions/filters/nullable-filter';
import { nameof } from '../../../../../src/utils/reflection';

class TargetFilter extends Nullable(class DummyFilter implements Filter<string> {
    shouldNotPass = false;

    passes(_value: string): boolean {
        return !this.shouldNotPass;
    }
}) {}

describe(`${Nullable.name}Filter.${nameof<TargetFilter>('passes')}()`, () => {
    const tests = [
        {
            value: 'omg',
            filter: undefined,
            expected: true,
            condition: 'filter is undefined',
        }, {
            value: 'omg',
            filter: {},
            expected: true,
            condition: 'filter is empty',
        }, {
            value: undefined,
            filter: {},
            expected: true,
            condition: 'value is undefined without filter',
        }, {
            value: undefined,
            filter: { isNull: true },
            expected: true,
            condition: '"isNull = true" matches because value is undefined',
        }, {
            value: null,
            filter: { isNull: true },
            expected: true,
            condition: '"isNull = true" matches because value is null',
        }, {
            value: 'omg',
            filter: { isNull: true },
            expected: false,
            condition: '"isNull = true" does not match',
        }, {
            value: 'omg',
            filter: { isNull: false },
            expected: true,
            condition: '"isNull = false" matches',
        }, {
            value: undefined,
            filter: { isNull: false },
            expected: false,
            condition: '"isNull = false" does not match because value is undefined',
        }, {
            value: null,
            filter: { isNull: false },
            expected: false,
            condition: '"isNull = false" does not match because value is null',
        }, {
            value: 'omg',
            filter: { shouldNotPass: false },
            expected: true,
            condition: 'parent filter matches',
        }, {
            value: 'omg',
            filter: { shouldNotPass: true },
            expected: false,
            condition: 'parent filter does not match',
        },
    ];

    for (const test of tests) {
        it(`should return ${test.expected} if ${test.condition}`, () => {
            const target = Object.assign(new TargetFilter(), test.filter);

            const actual = target.passes(test.value);

            expect(actual).equals(test.expected);
        });
    }
});
