import { expect } from 'chai';

import { EqualityFilter } from '../../../../../src/entity/subscriptions/filters/equality-filter';
import { nameof } from '../../../../../src/utils/reflection';

class TargetFilter extends EqualityFilter<string | undefined>(String) {}

describe(`${EqualityFilter.name}.${nameof<TargetFilter>('passes')}()`, () => {
    const tests = [
        {
            value: 'tz1X7fu4GXBXp9A8fchu1px3zzMDKtagDBk3',
            filter: undefined,
            expected: true,
            condition: 'filter is undefined',
        }, {
            value: 'tz1X7fu4GXBXp9A8fchu1px3zzMDKtagDBk3',
            filter: {},
            expected: true,
            condition: 'filter is empty',
        }, {
            value: undefined,
            filter: {},
            expected: true,
            condition: 'value is undefined without filter',
        }, {
            value: undefined,
            filter: { equalTo: 'tz1X7fu4GXBXp9A8fchu1px3zzMDKtagDBk3' },
            expected: false,
            condition: 'value is undefined with some filter',
        }, {
            value: undefined,
            filter: { equalTo: '' },
            expected: false,
            condition: 'value is undefined with empty string filter',
        }, {
            value: 'tz1X7fu4GXBXp9A8fchu1px3zzMDKtagDBk3',
            filter: { equalTo: 'tz1X7fu4GXBXp9A8fchu1px3zzMDKtagDBk3' },
            expected: true,
            condition: '"equalTo" matches',
        }, {
            value: 'tz1X7fu4GXBXp9A8fchu1px3zzMDKtagDBk3',
            filter: { equalTo: 'tz1K3fu4GXBXp9A8fchu1px3zzMDKtagDBh8' },
            expected: false,
            condition: '"equalTo" does not match',
        }, {
            value: 'tz1X7fu4GXBXp9A8fchu1px3zzMDKtagDBk3',
            filter: { notEqualTo: 'tz1X7fu4GXBXp9A8fchu1px3zzMDKtagDBk3' },
            expected: false,
            condition: '"notEqualTo" matches',
        }, {
            value: 'tz1X7fu4GXBXp9A8fchu1px3zzMDKtagDBk3',
            filter: { notEqualTo: 'tz1F5fu4GXBXp9A8fchu1px3zzMDKtagDBj8' },
            expected: true,
            condition: 'notEqualTo does not match',
        }, {
            value: 'tz1X7fu4GXBXp9A8fchu1px3zzMDKtagDBk3',
            filter: { in: ['tz1X7fu4GXBXp9A8fchu1px3zzMDKtagDBk3', 'tz1F5fu4GXBXp9A8fchu1px3zzMDKtagDBj8'] },
            expected: true,
            condition: '"in" matches',
        }, {
            value: 'tz1X7fu4GXBXp9A8fchu1px3zzMDKtagDBk3',
            filter: { in: ['tz1F5fu4GXBXp9A8fchu1px3zzMDKtagDBj8', 'tz1R9fu4GXBXp9A8fchu1px3zzMDKtagDBj8'] },
            expected: false,
            condition: '"in" does not match',
        }, {
            value: 'tz1X7fu4GXBXp9A8fchu1px3zzMDKtagDBk3',
            filter: { notIn: ['tz1F5fu4GXBXp9A8fchu1px3zzMDKtagDBj8', 'tz1R9fu4GXBXp9A8fchu1px3zzMDKtagDBj8'] },
            expected: true,
            condition: '"notIn" matches',
        }, {
            value: 'tz1X7fu4GXBXp9A8fchu1px3zzMDKtagDBk3',
            filter: { notIn: ['tz1X7fu4GXBXp9A8fchu1px3zzMDKtagDBk3', 'tz1R9fu4GXBXp9A8fchu1px3zzMDKtagDBj8'] },
            expected: false,
            condition: '"notIn" does not match',
        }, {
            value: 'tz1X7fu4GXBXp9A8fchu1px3zzMDKtagDBk3',
            filter: {
                notEqualTo: 'tz1F8fu4GXBXp9A8fchu1px3zzMDKtagDBh8',
                in: ['tz1X7fu4GXBXp9A8fchu1px3zzMDKtagDBk3', 'tz1R9fu4GXBXp9A8fchu1px3zzMDKtagDBj8'],
                notIn: ['tz1R8fu4GXBXp9A8fchu1px3zzMDKtagDBh1', 'tz1R9fu4GXBXp9A8fchu1px3zzMDKtagDBj8'],
            },
            expected: true,
            condition: 'combined filters match',
        }, {
            value: 'tz1X7fu4GXBXp9A8fchu1px3zzMDKtagDBk3',
            filter: {
                equalTo: 'tz1X7fu4GXBXp9A8fchu1px3zzMDKtagDBk3',
                notEqualTo: 'tz1F8fu4GXBXp9A8fchu1px3zzMDKtagDBh8',
                in: ['tz1R8fu4GXBXp9A8fchu1px3zzMDKtagDBh1', 'tz1R9fu4GXBXp9A8fchu1px3zzMDKtagDBj8'],
                notIn: ['tz1X7fu4GXBXp9A8fchu1px3zzMDKtagDBk3', 'tz2Y7fu4GXBXp9A8fchu1px3zzMDKtagDBg4'],
            },
            expected: false,
            condition: 'combined filters do not match',
        },
    ];

    for (const test of tests) {
        it(`should return ${test.expected} if ${test.condition}`, () => {
            const target = Object.assign(new TargetFilter(), test.filter);

            const actual = target.passes(test.value);

            expect(actual).equals(test.expected);
        });
    }
});
