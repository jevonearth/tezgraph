import { expect } from 'chai';

import { create } from '../../../src/utils/creation';

class TestData {
    value!: string;
}

describe('Creation utils', () => {
    it(`${create.name}() should create instance and assign values`, () => {
        const data = create(TestData, { value: 'foo' });

        expect(data).deep.equals({ value: 'foo' })
            .and.instanceof(TestData);
    });
});
