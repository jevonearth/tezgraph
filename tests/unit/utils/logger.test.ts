import { expect } from 'chai';
import { instance, mock, verify, when } from 'ts-mockito';
import winston from 'winston';

import { Logger, LoggerFactory, LogLevel } from '../../../src/utils/logger';
import { Constructor, KeyOfType, nameof } from '../../../src/utils/reflection';

describe(Logger.name, () => {
    let target: Logger;
    let rootLogger: winston.Logger;

    beforeEach(() => {
        rootLogger = mock<winston.Logger>();
        target = new Logger('Foo', instance(rootLogger));
    });

    describe(`${nameof<Logger>('isEnabled')}()`, () => {
        testIsEnabled(t => t.isEnabled(LogLevel.Debug), LogLevel.Debug);
        testIsEnabled(t => t.isEnabled(LogLevel.Error), LogLevel.Error);
    });

    testIsEnabledProperty('isErrorEnabled', LogLevel.Error);
    testIsEnabledProperty('isWarnEnabled', LogLevel.Warn);
    testIsEnabledProperty('isInfoEnabled', LogLevel.Info);
    testIsEnabledProperty('isDebugEnabled', LogLevel.Debug);

    function testIsEnabledProperty(property: KeyOfType<Logger, boolean>, expectedLevel: LogLevel) {
        describe(property, () => testIsEnabled(t => t[property], expectedLevel));
    }

    function testIsEnabled(act: (l: Logger) => boolean, expectedLevel: LogLevel) {
        [true, false].forEach(expectedReturn => {
            it(`should check ${expectedLevel} level and return ${expectedReturn}`, () => {
                when(rootLogger.isLevelEnabled(expectedLevel)).thenReturn(expectedReturn);

                const actual = act(target);

                expect(actual).equals(expectedReturn);
            });
        });
    }

    describe(`${nameof<Logger>('log')}()`, () => {
        testLog((t, m, a) => t.log(LogLevel.Info, m, ...a), LogLevel.Info);
        testLog((t, m, a) => t.log(LogLevel.Warn, m, ...a), LogLevel.Warn);
    });

    testLogMethod('error', LogLevel.Error);
    testLogMethod('warn', LogLevel.Warn);
    testLogMethod('info', LogLevel.Info);
    testLogMethod('debug', LogLevel.Debug);

    function testLogMethod(method: KeyOfType<Logger, (m: string, ...a: any[]) => void>, expectedLevel: LogLevel) {
        describe(`${method}()`, () => testLog((t, m, a) => t[method](m, ...a), expectedLevel));
    }

    function testLog(act: (t: Logger, m: string, a: any[]) => void, expectedLevel: LogLevel) {
        it(`should log ${expectedLevel} level`, () => {
            act(target, 'test msg', [123, 'omg']);

            verify(rootLogger.log(expectedLevel, '[Foo] test msg', 123, 'omg')).called();
        });
    }
});

class MyService {}

describe(LoggerFactory.name, () => {
    let target: LoggerFactory;
    let rootLogger: winston.Logger;

    beforeEach(() => {
        rootLogger = mock<winston.Logger>();
        target = new LoggerFactory(rootLogger);
    });

    testGet('string', 'FooBar', 'FooBar');
    testGet('type', MyService, 'MyService');

    function testGet(argType: string, inputCategory: string | Constructor<any>, expectedCategory: string) {
        it(`${nameof<LoggerFactory>('get')}(${argType}) should create logger correctly`, () => {
            const logger = <any>target.get(inputCategory);

            expect(logger.category).equals(expectedCategory);
            expect(logger.rootLogger).equals(rootLogger);
        });
    }
});
