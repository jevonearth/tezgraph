import { EnvConfig, Names } from '../../../src/utils/env-config';
import { expect } from '../../chai-with-plugins';

describe(EnvConfig.name, () => {
    let env: Record<string, string | undefined>;
    const act = () => new EnvConfig(env);

    beforeEach(() => {
        env = {
            [Names.Host]: '1.2.3.4',
            [Names.Port]: '3000',
            [Names.TezosNode]: 'https://api.tezos.org.ua/',
            [Names.GraphQL]: 'all',
            [Names.Env]: 'development',
            [Names.StartBlockLevelMaxFromHead]: '66',
            [Names.RpcRetryDelaysMillis]: '500,1000,2000,4000,8000,16000',
            [Names.SubscriptionKeepAliveMillis]: '4',
        };
    });

    it('should be created correctly', () => {
        const config = act();

        const expectedConfig: EnvConfig = {
            host: '1.2.3.4',
            port: 3000,
            tezosNodeUrl: 'https://api.tezos.org.ua/',
            graphQL: {
                queriesEnabled: true,
                subscriptionsEnabled: true,
            },
            environment: 'development',
            startBlockLevelMaxFromHead: 66,
            rpcRetryDelaysMillis: [500, 1000, 2000, 4000, 8000, 16000],
            subscriptionKeepAliveMillis: 4,
        };
        expect(config).deep.equals(expectedConfig);
    });

    describeProperty('host', () => {
        testMissingValue(Names.Host);
    });

    describeProperty('port', () => {
        testMissingValue(Names.Port);
        testNotPositiveInteger(Names.Port);
    });

    describeProperty('tezosNodeUrl', () => {
        testMissingValue(Names.TezosNode);

        testFailed({
            valueDesc: 'invalid URL',
            varName: Names.TezosNode,
            value: 'wtf',
            expectedError: ['absolute URL'],
        });
    });

    describeProperty('environment', () => {
        testMissingValue(Names.Env);
    });

    describeProperty('graphQL', () => {
        test('all', { queriesEnabled: true, subscriptionsEnabled: true });
        test('queries', { queriesEnabled: true, subscriptionsEnabled: false });
        test('subscriptions', { queriesEnabled: false, subscriptionsEnabled: true });

        function test(mode: string, expected: { queriesEnabled: boolean; subscriptionsEnabled: boolean }): void {
            it(`should support "${mode}" mode`, () => {
                env[Names.GraphQL] = mode;

                const config = act();

                expect(config.graphQL).deep.equals(expected);
            });
        }

        testMissingValue(Names.GraphQL);

        testFailed({
            valueDesc: 'not supported',
            varName: Names.GraphQL,
            value: 'wtf',
            expectedError: ['supported', 'all', 'queries', 'subscriptions'],
        });
    });

    describeProperty('startBlockLevelMaxFromHead', () => {
        testMissingValue(Names.StartBlockLevelMaxFromHead);
        testNotPositiveInteger(Names.StartBlockLevelMaxFromHead);
    });

    describeProperty('rpcRetryDelaysMillis', () => {
        testMissingValue(Names.RpcRetryDelaysMillis);
        testNotListOfPositiveIntegers(Names.RpcRetryDelaysMillis);
    });

    describeProperty('subscriptionKeepAliveMillis', () => {
        testMissingValue(Names.SubscriptionKeepAliveMillis);
        testNotPositiveInteger(Names.SubscriptionKeepAliveMillis);
    });

    function describeProperty(property: keyof EnvConfig, tests: () => void): void {
        describe(property, tests);
    }

    function testMissingValue(varName: string): void {
        for (const value of [null, undefined, '']) {
            testFailed({ valueDesc: JSON.stringify(value), varName, value, expectedError: ['missing'] });
        }
    }

    function testNotPositiveInteger(varName: string): void {
        for (const [value, valueDesc] of [['0', 'zero'], ['-123', 'negative']]) {
            testFailed({ valueDesc, varName, value, expectedError: ['integer', 'greater than zero'] });
        }
    }

    function testNotListOfPositiveIntegers(varName: string) {
        for (const [value, valueDesc] of [['1,0', 'zero'], ['1,-123', 'negative']]) {
            testFailed({ valueDesc, varName, value, expectedError: ['integer', 'greater than zero'] });
        }
    }

    function testFailed(options: { valueDesc: string; varName: string; value: string | undefined | null; expectedError: string[] }) {
        it(`should fail if value is ${options.valueDesc}`, () => {
            env[options.varName] = options.value!;

            const assertion = expect(act).throws().which.property('message');
            options.expectedError.concat('failed parsing', `"${options.varName}"`, `"${options.value ?? ''}"`)
                .forEach(e => assertion.containIgnoreCase(e));
        });
    }
});
