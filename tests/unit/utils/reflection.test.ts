import { expect } from 'chai';

import { nameof, isNotNullish, isNullish } from '../../../src/utils/reflection';

class TestData {
    value!: string;
}

describe('Reflection utils', () => {
    it(`${nameof.name}() should return member name`, () => {
        const name = nameof<TestData>('value');

        expect(name).equals('value');
    });

    const nullishTestCases = [
        { desc: 'undefined', value: undefined, expected: true },
        { desc: 'null', value: null, expected: true },
        { desc: 'empty string', value: '', expected: false },
        { desc: 'zero', value: 0, expected: false },
        { desc: 'string', value: 'abc', expected: false },
        { desc: 'number', value: 123, expected: false },
    ];

    testNullish(isNullish, expected => expected);
    testNullish(isNotNullish, expected => !expected);

    function testNullish(act: (v: any) => boolean, transformExpected: (v: boolean) => boolean) {
        describe(`${act.name}()`, () => {
            nullishTestCases.forEach(test => {
                it(`should return ${transformExpected(test.expected)} if value is ${test.desc}`, () => {
                    const actual = act(test.value);

                    expect(actual).equals(transformExpected(test.expected));
                });
            });
        });
    }
});
