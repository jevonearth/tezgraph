import { expect } from 'chai';

import { errorToString } from '../../../src/utils/conversion';

describe('Conversion utils', () => {
    describe(`${errorToString.name}()`, () => {
        const testCases = [
            { desc: 'error', error: new Error('Oups'), expected: 'Error: Oups' },
            { desc: 'string', error: 'OMG', expected: 'OMG' },
            { desc: 'number', error: 123, expected: '123' },
            { desc: 'object literal', error: { meme: 'lol' }, expected: '{"meme":"lol"}' },
            { desc: 'empty string', error: '', expected: '' },
            { desc: 'undefined', error: undefined, expected: 'undefined' },
            { desc: 'null', error: null, expected: 'null' },
        ];

        testCases.forEach(test => {
            it(`should correctly convert to string if value is ${test.desc}`, () => {
                const result = errorToString(test.error);

                expect(result).equals(test.expected);
            });
        });
    });
});
