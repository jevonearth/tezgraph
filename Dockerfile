FROM node:12.19.0-alpine3.12

# Default Env variables
ENV PORT 3000
ENV HOST 0.0.0.0
ENV TEZOS_NODE https://api.tezos.org.ua

RUN apk add --no-cache git
WORKDIR /usr/src/app
ADD . /usr/src/app
RUN npm ci
EXPOSE 3000
CMD ["npm", "run", "dev"]
